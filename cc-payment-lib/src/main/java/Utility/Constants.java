package Utility;


public class Constants {
    public static final String PARAMETER_SEP = "&";
    public static final String PARAMETER_EQUALS = "=";
    public static final String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans";

    public static final String GET_RSA_URL = "https://vamosys.com/ccavenue/GetRSA.php";
    public static final String REDIRECT_URL = "https://vamosys.com/ccavenue/ccavResponseHandler.php";
    public static final String CANCEL_URL = "http://vamosys.com/ccavenue/ccavResponseHandler.php";

    public static final String INR = "INR";

//    public static final String ACCESS_CODE = "AVMG79FG17BB15GMBB";
//    public static final String MERCHANT_ID = "182384";

    public static final String ACCESS_CODE = "AVNR78FG98AH09RNHA";
    public static final String MERCHANT_ID = "182384";

}
