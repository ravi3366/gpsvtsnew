package Utility;


public class AvenuesParams {
    public static final String COMMAND = "command";
    public static final String ACCESS_CODE = "access_code";
    public static final String MERCHANT_ID = "merchant_id";
    public static final String ORDER_ID = "order_id";
    public static final String AMOUNT = "amount";
    public static final String CURRENCY = "currency";
    public static final String ENC_VAL = "enc_val";
    public static final String REDIRECT_URL = "redirect_url";
    public static final String CANCEL_URL = "cancel_url";
    public static final String RSA_KEY_URL = "rsa_key_url";


    public static final String F_CODE = "merchant_param1";
    public static final String USER_ID = "merchant_param2";

    //billing address related info

    public static final String BILLING_NAME = "billing_name";
    public static final String BILLING_ADDRESS = "billing_address";

    public static final String BILLING_CITY = "billing_city";
    public static final String BILLING_STATE = "billing_state";
    public static final String BILLING_COUNTRY = "billing_country";
    public static final String BILLING_ZIPCODE = "billing_zip";
}
