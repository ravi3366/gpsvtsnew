package com.avenues.lib.testotpappnew;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import Utility.AvenuesParams;


public class PaymentActivity extends AppCompatActivity {

    //    private EditText accessCode, merchantId, currency, amount, orderId, rsaKeyUrl, redirectUrl, cancelUrl;
    long orderId = 0;
    private EditText mEdtAmount;
    private Button mBtnPaynow;
    Toolbar mToolbar;

    String mUserId,mFCode,mCustomerName;


    private void init() {
//        accessCode = (EditText) findViewById(R.id.accessCode);
//        merchantId = (EditText) findViewById(R.id.merchantId);
//        orderId = (EditText) findViewById(R.id.orderId);
//        currency = (EditText) findViewById(R.id.currency);
//        amount = (EditText) findViewById(R.id.amount);
//        rsaKeyUrl = (EditText) findViewById(R.id.rsaUrl);
//        redirectUrl = (EditText) findViewById(R.id.redirectUrl);
//        cancelUrl = (EditText) findViewById(R.id.cancelUrl);

        mEdtAmount = (EditText) findViewById(R.id.edt_amount);
        mBtnPaynow = (Button) findViewById(R.id.btn_paynow);

        mBtnPaynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderId = System.currentTimeMillis();
                if (mEdtAmount.getText() != null && mEdtAmount.getText().toString().trim().length() > 0 && Double.parseDouble(mEdtAmount.getText().toString().trim()) > 0) {
                    Intent intent = new Intent(PaymentActivity.this, WebViewActivityNew.class);

//                    System.out.println("Hi amount " + mEdtAmount.getText().toString().trim() + " order id " + orderId);

                    intent.putExtra(AvenuesParams.AMOUNT, mEdtAmount.getText().toString().trim());
                    intent.putExtra(AvenuesParams.ORDER_ID, ""+orderId);

                    intent.putExtra(AvenuesParams.F_CODE, mFCode);
                    intent.putExtra(AvenuesParams.USER_ID, mUserId);

                    System.out.println();

                    intent.putExtra(AvenuesParams.BILLING_NAME, mCustomerName);

                    startActivity(intent);
                    // finish();
                } else {
                    mEdtAmount.setError("Please enter amount");
                }
            }
        });

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.payment));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    Intent myIntent = new Intent(PaymentActivity.this, Class.forName("com.vamosys.vamos.VehicleListActivity"));
                    startActivity(myIntent);
                    finish();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }


            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);
        init();
        Intent mainIntent = getIntent();
        mUserId=mainIntent.getStringExtra(AvenuesParams.USER_ID);

       mFCode= mainIntent.getStringExtra(AvenuesParams.F_CODE);

        mEdtAmount.setText(mainIntent.getStringExtra(AvenuesParams.AMOUNT));

        mCustomerName= mainIntent.getStringExtra(AvenuesParams.BILLING_NAME);


    }


//    public void onClick(View view) {
//        String vAccessCode = ServiceUtility.chkNull(accessCode.getText()).toString().trim();
//        String vMerchantId = ServiceUtility.chkNull(merchantId.getText()).toString().trim();
//        String vCurrency = ServiceUtility.chkNull(currency.getText()).toString().trim();
//        String vAmount = ServiceUtility.chkNull(amount.getText()).toString().trim();
//        if (!vAccessCode.equals("") && !vMerchantId.equals("") && !vCurrency.equals("") && !vAmount.equals("")) {
//            Intent intent = new Intent(this, WebViewActivity.class);
//            intent.putExtra(AvenuesParams.ACCESS_CODE, ServiceUtility.chkNull(accessCode.getText()).toString().trim());
//            intent.putExtra(AvenuesParams.MERCHANT_ID, ServiceUtility.chkNull(merchantId.getText()).toString().trim());
//            intent.putExtra(AvenuesParams.ORDER_ID, ServiceUtility.chkNull(orderId.getText()).toString().trim());
//            intent.putExtra(AvenuesParams.CURRENCY, ServiceUtility.chkNull(currency.getText()).toString().trim());
//            intent.putExtra(AvenuesParams.AMOUNT, ServiceUtility.chkNull(amount.getText()).toString().trim());
//
//            intent.putExtra(AvenuesParams.REDIRECT_URL, ServiceUtility.chkNull(redirectUrl.getText()).toString().trim());
//            intent.putExtra(AvenuesParams.CANCEL_URL, ServiceUtility.chkNull(cancelUrl.getText()).toString().trim());
//            intent.putExtra(AvenuesParams.RSA_KEY_URL, ServiceUtility.chkNull(rsaKeyUrl.getText()).toString().trim());
//
//            startActivity(intent);
//        } else {
//            showToast("All parameters are mandatory.");
//        }
//    }


    public void showToast(String msg) {
        Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //generating order number
        //  Integer randomNum = ServiceUtility.randInt(0, 9999999);
        //  orderId.setText(randomNum.toString());
    }
}
