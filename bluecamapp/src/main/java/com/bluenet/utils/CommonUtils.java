package com.bluenet.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Vibrator;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextUtils;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

/**
 * Created by Administrator on 2017/7/6.
 */

public class CommonUtils
{
    public static final String APP_SETTING_4_G_TOAST = "4g_toast";
    public static final String APP_SETTING_ALARM_TOAST = "alarm_toast";
    public static final String APP_SETTING_LANGUAGE_TOAST = "language_set";

    public static final String WIFI_KEY = "0123456789012345";
    private static long lastClickTime;
    /**
     * 防止快速点击
     *
     * @return 是否是快速点击
     */
    public static boolean isFastClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < 1000) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    /**
     * 定位到输入的末尾
     */
    public static void toTextEnd(EditText editText) {
        CharSequence text = editText.getText();
        if (!TextUtils.isEmpty(text)) {
            Spannable spanText = (Spannable) text;
            Selection.setSelection(spanText, text.length());
        }
    }

    public static String getFond(String text,String color)
    {
        return "<font color=\""+color+"\">"+text+"</font>";
    }

    public static void virbate(Context mContext){
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(200);
    }

    public static String formatNum(int a){
        if(a>9){
            return String.valueOf(a);
        }
        else {
            return "0"+String.valueOf(a);
        }
    }

    public static boolean isNumeric(String str){
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(str).matches();
    }

    public static  boolean isChar(String str){
        Pattern pattern = Pattern.compile("[a-zA-Z]+");
        return pattern.matcher(str).matches();
    }

    public static int getPrecent(int a,int total){
        //DecimalFormat formatter = new DecimalFormat("0.00");
        float precent = (float)a/(float)total;
        //LogUtil.printLog("a == "+a+",--------precent == "+precent);
        int c = (int)(precent*100);
        //LogUtil.printLog("c == "+c+",--------precent == "+precent);
        return c;
    }

    public static int getDp(Context context,int dimensResID){
        int result = context.getResources().getDimensionPixelSize(dimensResID);
        return result;
    }

    public static boolean isJson(String json){
        try {
            new JSONObject(json);
            return true;
        } catch (JSONException e) {
            System.out.println("bad json: " + json);
            return false;
        }

    }

    //生成圆角图片
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        try {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(),
                    bitmap.getHeight());
            final RectF rectF = new RectF(new Rect(0, 0, bitmap.getWidth(),
                    bitmap.getHeight()));
            final float roundPx = 20;
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(Color.BLACK);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

            final Rect src = new Rect(0, 0, bitmap.getWidth(),
                    bitmap.getHeight());

            canvas.drawBitmap(bitmap, src, rect, paint);
            return output;
        } catch (Exception e) {
            return bitmap;
        }
    }

    public  static Bitmap generateBitmap(String content,int width, int height) {
//        QRCodeWriter qrCodeWriter = new QRCodeWriter();
//        Map<EncodeHintType, String> hints = new HashMap<>();
//        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
//        try {
//            BitMatrix encode = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, width, height, hints);
//            int[] pixels = new int[width * height];
//            for (int i = 0; i < height; i++) {
//                for (int j = 0; j < width; j++) {
//                    if (encode.get(j, i)) {
//                        pixels[i * width + j] = 0x00000000;
//                    } else {
//                        pixels[i * width + j] = 0xffffffff;
//                    }
//                }
//            }
//            return Bitmap.createBitmap(pixels, 0, width, width, height, Bitmap.Config.RGB_565);
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }
        return null;
    }
}
