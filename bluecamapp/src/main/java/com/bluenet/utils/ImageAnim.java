package com.bluenet.utils;


import android.os.Handler;
import android.widget.ImageView;

/**
 * Created by Administrator on 2017/7/7.
 */

public class ImageAnim implements Runnable
{
    private Handler mHandler = new Handler();
    private ImageView animView;
    private int[] resId ;
    private int duration =200;
    private int frameLen;

    private boolean isStop = false;

    public void startAnim(ImageView view,int[] resId,int duration)
    {
        this.animView = view;
        this.resId = resId;
        this.duration = duration;
        frameLen = resId.length;
        mHandler.postDelayed(this,duration);
    }
    public void stopAnim()
    {
        isStop = true;
    }


    @Override
    public void run() {
        if(isStop)
        {
            mHandler.removeCallbacks(this);
            return;
        }
        if(frameLen == 0)
        {
            frameLen = resId.length;
        }
        frameLen--;
        if(animView != null)
        {
            int position = (resId.length-frameLen)-1;
           // LogUtil.printLog("ImageAnim position == "+position);
           // Bitmap bitmap = BitmapFactory.decodeResource()
            animView.setImageResource(resId[position]);
        }
        mHandler.postDelayed(this,duration);

    }
}
