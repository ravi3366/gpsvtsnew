package com.bluenet.camManager.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public final class DatabaseHelper extends SQLiteOpenHelper
{
	private static final String DB_NAME = "bluecam.db";
	private static final int DB_VERSION = 1;

	public interface TABLE 
	{
		public static final String DEVICE_TABLE = "camera";
		public static final String MESSAGE_TABLE = "message";
	}

	private static final String CREATE_DEVICE_TABLE_SQL = 
            "CREATE TABLE " + TABLE.DEVICE_TABLE + "(" +
    		CameraColumn.ID + " INTEGER PRIMARY KEY," +
    		CameraColumn.DID + " TEXT,"+
    		CameraColumn.NAME + " TEXT,"+
    		CameraColumn.PASSWD + " TEXT,"+
    		CameraColumn.USERNAME + " TEXT,"+
    		CameraColumn.IP + " TEXT,"+
    		CameraColumn.PORT + " TEXT,"+
    		CameraColumn.TYPE + " TEXT,"+
    		CameraColumn.MAC + " TEXT )";

	private static final String CREATE_MESSAGE_TABLE_SQL = 
            "CREATE TABLE " + TABLE.MESSAGE_TABLE + "(" +
            MessageColumn.ID + " INTEGER PRIMARY KEY," +
            MessageColumn.DID + " TEXT,"+
            MessageColumn.MESSAGE + " TEXT,"+
            MessageColumn.MSG_TYPE + " TEXT,"+
            MessageColumn.PATH + " TEXT,"+
            MessageColumn.DEVICE_NAME + " TEXT,"+
            MessageColumn.ALARM_TYPE + " TEXT,"+
			MessageColumn.READ + " TEXT,"+
    		MessageColumn.TIME + " TEXT )";

	private static DatabaseHelper mInstance;
	public static synchronized DatabaseHelper getInstance(Context context) 
	{
		if (mInstance == null) {
			mInstance = new DatabaseHelper(context);
		}
		return mInstance;
	}

	private DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		db.execSQL(CREATE_DEVICE_TABLE_SQL);
		db.execSQL(CREATE_MESSAGE_TABLE_SQL);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		
	}
}
