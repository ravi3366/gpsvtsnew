package com.bluenet.camManager.db;

public interface CameraColumn
{
	public static final String ID = "_id";
	//名称
	public static final String NAME = "name";
	//设备ID
	public static final String DID = "did";
	//mac地址
	public static final String MAC = "mac";
	//ip地址
	public static final String IP = "ip";
	//端口
	public static final String PORT = "port";
	//用户名
	public static final String USERNAME = "username";
	//密码
	public static final String PASSWD = "passwd";
	//设备类型
	public static final String TYPE = "type";
	
}
