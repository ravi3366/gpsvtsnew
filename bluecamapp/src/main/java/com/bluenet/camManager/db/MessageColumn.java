/**
 * 
 */
package com.bluenet.camManager.db;

/**
 * @author wang.jingui
 *
 */
public interface MessageColumn
{
	public static final String ID = "_id";
	public static final String DID = "did";
	public static final String TIME = "time";
	public static final String MSG_TYPE ="msg_type";
	public static final String MESSAGE = "message";
	public static final String PATH = "path";
	public static final String ALARM_TYPE = "alarm_type";
	public static final String DEVICE_NAME = "device_name";
	public static final String READ        = "read";
}
