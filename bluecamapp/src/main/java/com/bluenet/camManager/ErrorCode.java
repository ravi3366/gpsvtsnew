package com.bluenet.camManager;

/*
* Error Code
*/
public final class ErrorCode {
	private ErrorCode() {}

	public static final int ERR_SUCCESSFUL				= 0;	// 操作成功	
	public static final int ERR_NOT_INITIALIZED			= -1;
	public static final int ERR_ALREADY_INITIALIZED		= -2;
	public static final int ERR_TIMEOUT					= -3;
	public static final int ERR_INVALID_ID				= -4;
	public static final int ERR_INVALID_PARAMETERS		= -5;
	public static final int ERR_DEVICE_OFFLINE			= -6;
	public static final int ERR_ID_OUT_OF_DATE			= -7;	// ID失效
	public static final int ERR_MAX_SESSION				= -8;
	public static final int ERR_CREATE_THREAD_FAILED	= -9;	
	public static final int ERR_APPLY_RESOURCE_FAIL		= -10;
	public static final int ERR_INIT_SOCKET_FAIL		= -11;
	public static final int ERR_NOT_IMPLEMENTED			= -12;	// 未实现
	public static final int ERR_CREATE_DEVICE_FAIL		= -13;
	public static final int ERR_UNSUPPORTED_LICENSE		= -14;
	public static final int ERR_CONNECT_FAILED			= -15;
	public static final int ERR_USER_ABORT				= -16;
	public static final int ERR_SOCKET_BIND_FAILED		= -17;
	public static final int ERR_WRITE_FAILED			= -18;
	public static final int ERR_READ_FAILED				= -19;
	public static final int ERR_NOT_LOGIN				= -20;
	public static final int ERR_GET_SECRET_KEY_FAILED	= -21;
	public static final int ERR_OUT_OF_BUFFER			= -22;
	public static final int ERR_CHECK_USER_FAILED		= -23;
	public static final int ERR_DATA_INCORRECT			= -24;
	public static final int ERR_GET_SOCKET_BUFFER_FAILED= -25;
	public static final int ERR_AUDIO_NOT_OPENED		= -26;
	public static final int ERR_VIDEO_NOT_OPENED		= -27;
	public static final int ERR_PLAYBACK_NOT_OPENED		= -28;	
	public static final int ERR_JNI_REFERENCE_OBJECT	= -29;		// 引用失败
	public static final int ERR_JNI_GET_OBJECT_CLASS	= -30;		// 检索class失败
	public static final int ERR_JNI_GET_METHOD			= -31;		// 检索method失败
	public static final int ERR_TRANSMIT_FILE_INVALID_RESULT	= -35;	// 收到前端数据不可用
	public static final int ERR_TRANSMIT_FILE_IN_PROGRESS		= -36;	// 处理中
	public static final int ERR_TRANSMIT_FILE_VER_MISMATCH		= -37;	// 前端版本无法匹配
	public static final int ERR_TRANSMIT_FILE_TRANSFER_CANCEL	= -38;	// 传输取消
	public static final int ERR_TRANSMIT_FILE_TRANSFER_ERROR	= -39;	// 传输发生错误
	public static final int ERR_TRANSMIT_FILE_OPEN_FILE_ERROR	= -40;	
	public static final int ERR_TRANSMIT_FILE_MD5_CHECK_ERROR	= -41;	// 校验错误
	public static final int ERR_TRANSMIT_FILE_CONFIRM_ERROR		= -42;	// 确认文件错误
	public static final int ERR_TRANSMIT_FILE_DATA_ERROR		= -43;	// 数据错误
	public static final int ERR_TRANSMIT_FILE_PARAM_ERROR		= -44;	// 接口参数错误
	public static final int ERR_UNKOWN							= -99;
};