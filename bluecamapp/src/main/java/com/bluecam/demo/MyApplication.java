package com.bluecam.demo;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Environment;

import com.bluenet.utils.UncaughtException;

import java.io.File;

/*
 
5.	搜索结果显示不出来，重启APP搜索就可以了。

*/
public class MyApplication extends Application{
	public static Context mContext;
	private static MyApplication mInstance = null;
	
	private static final String TAG = MyApplication.class.getSimpleName();
	
	public static int Add_DEV_TYPE = 1;
	
	public int screenWidth;
	public int screenHeight;
	

	public static String appUrl = "";
	
	public static String[] list_sensor;
	
	public static MyApplication getInstance() {
		return mInstance;
	}
	public static Context getContext(){
		return mContext;
	}
	
	public static File getSDPath(){
		  File sdDir = null;
		  boolean sdCardExist = Environment.getExternalStorageState()
		  .equals(Environment.MEDIA_MOUNTED); //判断sd卡是否存在
		  if (sdCardExist)
		  {
		  sdDir = Environment.getExternalStorageDirectory();//获取跟目录
		  }
		  return null;
	 }
	
	@Override
	public void onCreate() {
		super.onCreate();
		//异常捕捉
		UncaughtException mUncaughtException = UncaughtException.getInstance();
		mUncaughtException.init();
		        
		mInstance = this;
		mContext=  this;


	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
//		Log.i(TAG, "application onConfigurationChanged()");
	}
}
