package com.bluecam.demo;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.bluenet.camManager.BCamera;

import java.util.Locale;

/**
 * Created by Administrator on 2017/5/22.
 */

public class BaseFragment extends Fragment {
    protected static final int DELETE_CAM = 1;
    protected static final int SET_PWD_CAM = 2;
    protected static final int TOAST_4G = 3;
    protected static final String USERID = "camHandler";
    protected Context context;
   // protected HeaderBar header_bar;
    protected ProgressDialog progressDialog;
    protected TextView tvMessage;
    protected View progressView;
   // protected EventManager eventManager;
    public static boolean isAdd = false;
    //protected static List<Alarm433Model> disListData = new ArrayList<Alarm433Model>();
    protected int lanIndex;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
       // eventManager = EventManager.getEventManager();
       // lanIndex = PreferencesUtil.getInt(context, CommonUtils.APP_SETTING_LANGUAGE_TOAST, 0);
        setLan(lanIndex);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    protected void initProgressDialogView() {
        //progressView = LayoutInflater.from(context).inflate(R.layout.toast_view, null);
        //tvMessage = (TextView) progressView.findViewById(R.id.tv_message_toast);
    }

    protected void showCustomProgressDialog(String msg) {
        progressDialog = null;
        progressDialog = ProgressDialog.show(context, "", "", true, false);
        progressDialog.setContentView(progressView);
        progressDialog.setCancelable(true);
        tvMessage.setText(msg);

    }

    protected void showProgressDialog(String msg) {
        hideProgressDialog();
        progressDialog = ProgressDialog.show(context, "", msg, true, true);
    }

    protected void showProgressDialog1(String msg) {
        hideProgressDialog();
        progressDialog = ProgressDialog.show(context, "", msg, true, false);
    }

    protected void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    //把音量设为60%
    protected void autoSetAudioVolumn(float voice) {
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) (max * voice), 0);
    }

    protected void openActicity(long userid, Class clazz) {
        Intent intent = new Intent(context, clazz);
        intent.putExtra(USERID, userid);
        startActivity(intent);
        //((Activity)context).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    protected void openActicity(Class clazz, String fragmentparam) {
        Intent intent = new Intent(context, clazz);
        intent.putExtra(CommonActivity.FRAGMENT_PATH, fragmentparam);
        startActivity(intent);
        //((Activity)context).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    protected void openActicity(String camID, Class clazz, String fragmentparam) {
        Intent intent = new Intent(context, clazz);
        intent.putExtra(CommonActivity.FRAGMENT_PATH, fragmentparam);
        intent.putExtra(CommonActivity.FRAGMENT_PARAM, camID);
        startActivity(intent);
        //((Activity)context).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    protected void openActivityForResult(Class clazz, String fragmentparam, int requestCode) {
        Intent intent = new Intent(context, clazz);
        intent.putExtra(CommonActivity.FRAGMENT_PATH, fragmentparam);
        startActivityForResult(intent, requestCode);
        //((Activity)context).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    protected void showDialog(String msg, final BCamera camera, final int optTag) {
        View view = LayoutInflater.from(context).inflate(R.layout.common_dialog, null);

        TextView content_txt = (TextView) view.findViewById(R.id.content_txt);
        TextView ok_btn = (TextView) view.findViewById(R.id.ok_btn);
        TextView cancle_btn = (TextView) view.findViewById(R.id.cancle_btn);

        content_txt.setText(msg);

        final Dialog dialog = new Dialog(context, R.style.dialog_sty);
        dialog.setCancelable(true);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setCanceledOnTouchOutside(true);
        dialog.addContentView(view, new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT));
        dialog.show();

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogOK(camera, optTag);
                dialog.dismiss();
            }
        });

        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    protected void showDialog(String msg, final BCamera camera, final int optTag, boolean isHideCancel) {
        View view = LayoutInflater.from(context).inflate(R.layout.common_dialog, null);
        TextView content_txt = (TextView) view.findViewById(R.id.content_txt);
        TextView ok_btn = (TextView) view.findViewById(R.id.ok_btn);
        TextView cancle_btn = (TextView) view.findViewById(R.id.cancle_btn);
        if (isHideCancel) {
            cancle_btn.setVisibility(View.GONE);
        }
        content_txt.setText(msg);
        final Dialog dialog = new Dialog(context, R.style.dialog_sty);
        dialog.setCancelable(true);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setCanceledOnTouchOutside(true);
        dialog.addContentView(view, new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT));
        dialog.show();
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogOK(camera, optTag);
                dialog.dismiss();
            }
        });
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    protected void showDialog(String msg, final Object obj, final int optTag) {
        View view = LayoutInflater.from(context).inflate(R.layout.common_dialog, null);
        TextView content_txt = (TextView) view.findViewById(R.id.content_txt);
        TextView ok_btn = (TextView) view.findViewById(R.id.ok_btn);
        TextView cancle_btn = (TextView) view.findViewById(R.id.cancle_btn);
        content_txt.setText(msg);
        final Dialog dialog = new Dialog(context, R.style.dialog_sty);
        dialog.setCancelable(true);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setCanceledOnTouchOutside(true);
        dialog.addContentView(view, new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT));
        dialog.show();
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogOK(obj, optTag);
                dialog.dismiss();
            }
        });
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    protected void onDialogOK(BCamera camera, int optTag) {

    }

    protected void onDialogOK(Object obj, int optTag) {

    }

    protected String getTextString(int resId) {
        return context.getResources().getString(resId);
    }

    protected void showToast(String msg) {
        //ToastUtils.showToast(context, msg);
    }

    protected void backFinish() {
        if (context != null)
            ((Activity) context).finish();
    }

    public void setLan(int lanIndex) {
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (lanIndex == 0) {
            config.locale = Locale.getDefault();
        } else if (lanIndex == 1) {
            config.locale = Locale.CHINESE;
        } else if (lanIndex == 2) {
            config.locale = Locale.ENGLISH;
        } else if (lanIndex == 3) {
            config.locale = new Locale("IT", "it", "");
        } else if (lanIndex == 4) {
            config.locale = new Locale("DE", "de", "");
        } else if (lanIndex == 5) {
            config.locale = new Locale("FR", "fr", "");
        } else if (lanIndex == 6) {
            config.locale = new Locale("NL", "nl", "");
        } else if (lanIndex == 7) {
            config.locale = new Locale("PT", "pt", "");
        } else if (lanIndex == 8) {
            config.locale = Locale.JAPAN;
        } else if (lanIndex == 9) {
            config.locale = new Locale("ES", "es", "");
        } else if (lanIndex == 10) {
            config.locale = new Locale("RU", "ru", "");
        }else if(lanIndex == 11){
            config.locale = new Locale("VI","vn","");
        }
        //LogUtil.printLog("local ==" + config.locale.getDisplayLanguage());
        resources.updateConfiguration(config, dm);
    }
}
