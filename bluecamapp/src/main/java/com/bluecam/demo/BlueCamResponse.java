package com.bluecam.demo;


import com.google.gson.annotations.SerializedName;

public class BlueCamResponse {

	@SerializedName("camName")
	private String camName;

	@SerializedName("camId")
	private String camId;

	@SerializedName("userName")
	private String userName;

	@SerializedName("pwd")
	private String pwd;

	@SerializedName("shortName")
	private String shortName;

	@SerializedName("cameraType")
	private String cameraType;

	public void setCamName(String camName){
		this.camName = camName;
	}

	public String getCamName(){
		return camName;
	}

	public void setCamId(String camId){
		this.camId = camId;
	}

	public String getCamId(){
		return camId;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setPwd(String pwd){
		this.pwd = pwd;
	}

	public String getPwd(){
		return pwd;
	}

	public void setShortName(String shortName){
		this.shortName = shortName;
	}

	public String getShortName(){
		return shortName;
	}

	public void setCameraType(String cameraType){
		this.cameraType = cameraType;
	}

	public String getCameraType(){
		return cameraType;
	}

	@Override
 	public String toString(){
		return 
			"BlueCamResponse{" + 
			"camName = '" + camName + '\'' + 
			",camId = '" + camId + '\'' + 
			",userName = '" + userName + '\'' + 
			",pwd = '" + pwd + '\'' + 
			",shortName = '" + shortName + '\'' + 
			",cameraType = '" + cameraType + '\'' + 
			"}";
		}
}