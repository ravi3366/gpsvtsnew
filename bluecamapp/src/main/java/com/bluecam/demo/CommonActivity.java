package com.bluecam.demo;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;

import com.bluenet.utils.LogUtil;

/**
 * Created by Administrator on 2017/7/1.
 */

public class CommonActivity extends BaseActivity
{
    public static final String FRAGMENT_PATH  = "fragment_path";
    public static final String FRAGMENT_PARAM = "fragment_param";
    private String path;
    private String data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_screen);
        path = getIntent().getStringExtra(FRAGMENT_PATH);
        data = getIntent().getStringExtra(FRAGMENT_PARAM);
        LogUtil.printLog("path-------------->"+path);
        try {
            Class clazz = Class.forName(path);
            BaseFragment fragment = (BaseFragment) clazz.newInstance();
            Bundle b = new Bundle();
            b.putString(FRAGMENT_PARAM, data);
            fragment.setArguments(b);

//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .add(R.id.comment_frame, fragment)
//                    .commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(TextUtils.isEmpty(path)){
                return true;
            }
            if(path.equals("com.camera.fragment.Add_SearchFragment")){
                //eventManager.addEvent();
            }
//            if(eventManager != null){
//                eventManager.backEvent();
//            }
        }
        return super.onKeyDown(keyCode,event);
    }
}
