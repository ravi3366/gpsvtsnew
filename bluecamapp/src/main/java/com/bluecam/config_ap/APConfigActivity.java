package com.bluecam.config_ap;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;

import com.bluecam.adapter.APWifiListAdapter;
import com.bluecam.demo.BaseFragment;
import com.bluecam.demo.MvpBaseActivity;
import com.bluecam.demo.R;
import com.bluecam.presenter.CameraAPConfigPresenter;
import com.bluecam.presenter.ICameraAPConfigView;
import com.bluenet.camManager.BCamera;
import com.bluenet.camManager.CameraContants;
import com.bluenet.camManager.CameraManager;
import com.bluenet.utils.HeaderBar;
import com.bluenet.utils.LogUtil;
import com.bluenet.utils.WifiUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Administrator on 2017/11/27.
 */

public class APConfigActivity extends MvpBaseActivity<ICameraAPConfigView,CameraAPConfigPresenter> implements APWifiListAdapter.OnWlanItemClickListener,SwipeRefreshLayout.OnRefreshListener,ICameraAPConfigView {
    private SwipeRefreshLayout refresh_view;
    private RecyclerView wlan_list_view;
    private APWifiListAdapter apWifiListAdapter;
    private List<ScanResult>  wlanList = new ArrayList<ScanResult>();
    private boolean isConnected = false;
    private boolean isExit = false;

    private WifiUtils wifiUtils ;
    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == 0){
                if(isExit){
                    return;
                }
                initNetWifi();
                //mHandler.sendEmptyMessageDelayed(0,4000);
            }
            else if(msg.what ==1){
                refresh_view.setRefreshing(false);
            }
            else if(msg.what == 2){

                final String camID  = (String) msg.obj;
                LogUtil.printLog("camID == "+camID);

                mPresenter.connectCam(camID);
                BaseFragment.isAdd = true;

                Message  msg1 = conHandler.obtainMessage(0,camID);
                conHandler.sendMessageDelayed(msg1,1000);

                //if(eventManager != null){
                 //   eventManager.addEvent();
                //}

                //finish();

            }
            else if(msg.what == 3){
                if(isConnected){
                    hideProgressDialog();
                    LogUtil.printLog("Connected suc++++++++++++++++++++++++++++");
                    return;
                }
                String camID  = (String) msg.obj;
                LogUtil.printLog("camID == "+camID);
                mPresenter.connectCam(camID);
                isConnected = false;
                mHandler.sendEmptyMessageDelayed(4,6000);
            }
            else if(msg.what == 4){
                if(!isConnected){
                    hideProgressDialog();
                    //showToast(getTextString(R.string.camera_status_connect_failed));
                }
            }
        }
    };

    private int count = 0;
    private Handler conHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String camID = (String) msg.obj;
            if(TextUtils.isEmpty(camID)){
                return;
            }
            BCamera camera = CameraManager.getDeviceManager(APConfigActivity.this.getApplicationContext()).getCamera(camID);

            if(camera == null){
                hideProgressDialog();
                finish();
                return;
            }

            if(camera.getStatus() == CameraContants.DeviceStatus.DEVICE_STATUS_ON_LINE){
                hideProgressDialog();
                finish();
                return;
            }

            if(count == 10){
                hideProgressDialog();
                finish();
                return;
            }

            camera.destoryCamera();
          //  camera.createCamera(camera.getCameraManager());

            Message  msg1 = conHandler.obtainMessage(0,camID);
            conHandler.sendMessageDelayed(msg1,5000);

            count = count+1;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ap_wlan_list_screen);
        wifiUtils = new WifiUtils(this.getApplicationContext());
        initView();

        mHandler.sendEmptyMessageDelayed(0,1000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideProgressDialog();
        mHandler.removeMessages(0);
        isExit = true;
    }

    @Override
    protected CameraAPConfigPresenter createPresenter() {
        return new CameraAPConfigPresenter();
    }


    private void initNetWifi(){
        wlanList.clear();
        if(wifiUtils.isWifiEnabled()){
            wifiUtils.WifiStartScan();
        }
        else{
            wifiUtils.WifiOpen();
            wifiUtils.WifiStartScan();
            //0正在关闭,1WIFi不可用,2正在打开,3可用,4状态不可zhi
            while(wifiUtils.WifiCheckState() != WifiManager.WIFI_STATE_ENABLED){//等待Wifi开启
                LogUtil.printLog("WifiState======"+String.valueOf(wifiUtils.WifiCheckState()));
            }
            try {
                Thread.sleep(1000);//休眠3s，不休眠则会在程序首次开启WIFI时候，处理getScanResults结果，wifiResultList.size()发生异常
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        //List<ScanResult> result = wifiUtils.getScanResults();
        List<ScanResult> result = wifiUtils.getScanResults();
        int len = result.size();
        for(int i = 0;i < len;i++){
            ScanResult r = result.get(i);
            LogUtil.printLog("SSID == "+r.SSID);
            if(!TextUtils.isEmpty(r.SSID)){
                if(r.SSID.startsWith("HOW-")){
                    wlanList.add(r);
                }
            }
        }
        sort(wlanList);
        LogUtil.printLog("wlanList size =="+result.size());
        wifiUtils.getConfiguration();
        apWifiListAdapter.notifyDataSetChanged();
    }

    private void sort(List<ScanResult> listData)
    {
        Collections.sort(listData, new Comparator<ScanResult>() {
            @Override
            public int compare(ScanResult arg0, ScanResult arg1) {

                Integer l0 = WifiManager.calculateSignalLevel(arg0.level, 100);
                Integer l1 = WifiManager.calculateSignalLevel(arg1.level, 100);

                return l1.compareTo(l0);
            }
        });
    }


    private void initView(){

//        header_bar = (HeaderBar) findViewById(R.id.header_bar);
//        header_bar.setTitleText(getTextString(R.string.main_add_ap_wifi_title));
//        header_bar.setClickListener(new HeaderBar.OnHeaderBarClickListener() {
//            @Override
//            public void back() {
//                finish();
//            }
//        });
        refresh_view = (SwipeRefreshLayout)findViewById(R.id.refresh_view);
        refresh_view.setOnRefreshListener(this);
        refresh_view.setColorSchemeColors(getResources().getColor(android.R.color.holo_blue_bright), getResources().getColor(android.R.color.holo_green_light), getResources().getColor(android.R.color.holo_orange_light), getResources().getColor( android.R.color.holo_red_light));
        refresh_view.setRefreshing(true);
        mHandler.sendEmptyMessageDelayed(1,2000);
        wlan_list_view = (RecyclerView)findViewById(R.id.wlan_list_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        wlan_list_view.setLayoutManager(layoutManager);

        apWifiListAdapter = new APWifiListAdapter(this,wlanList,this);
        wlan_list_view.setAdapter(apWifiListAdapter);
        apWifiListAdapter.setWifiUtils(wifiUtils);
    }

    @Override
    public void onItemClick(ScanResult result) {
        //showInputPwdDialog(result);
        connectAp(result);
    }

    @Override
    public void onRefresh() {
        initNetWifi();
        mHandler.sendEmptyMessageDelayed(1,3000);
    }

    private void connectAp(ScanResult result){
        isConnected = false;
        //showProgressDialog(getTextString(R.string.camera_status_connecting)+"...");
        int wifiItemId = wifiUtils.IsConfiguration("\""+result.SSID+"\"");
        LogUtil.printLog("+++++++++++wifiItemId == "+wifiItemId);
        if(wifiItemId != -1){
            if(wifiUtils.ConnectWifi(wifiItemId)){//连接指定WIFI
                //连接成功
                //hideProgressDialog();
               // showToast("连接成功");
                Message msg = mHandler.obtainMessage(2,result.SSID);
                mHandler.sendMessageDelayed(msg,2000);
            }
        }else{
            String pwd = "168168168";
            int netId = wifiUtils.AddWifiConfig(wlanList,result.SSID, pwd);
            LogUtil.printLog("+++++++++++netId == "+netId);
            if(netId != -1){
                wifiUtils.getConfiguration();//添加了配置信息，要重新得到配置信息
                if(wifiUtils.ConnectWifi(netId)){
                    //连接成功
                    //hideProgressDialog();
                   // showToast("连接成功");
                    Message msg = mHandler.obtainMessage(2,result.SSID);
                    mHandler.sendMessageDelayed(msg,2000);
                }
            }
            else{
               // showToast(getTextString(R.string.main_add_ap_connect_err));
            }
        }
        Message msg = mHandler.obtainMessage(3,result.SSID);
        //mHandler.sendMessageDelayed(msg,10000);
    }

    /*private void showInputPwdDialog(final ScanResult result){
        View view = LayoutInflater.from(this).inflate(R.layout.ap_input_pwd_dialog, null);
        final EditText content_txt = (EditText)view.findViewById(R.id.content_et);
        TextView ok_btn      = (TextView)view.findViewById(R.id.ok_btn);
        TextView cancle_btn  = (TextView)view.findViewById(R.id.cancle_btn);

        final Dialog dialog = new Dialog(this,R.style.dialog_sty);
        dialog.setCancelable(true);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setCanceledOnTouchOutside(true);
        dialog.addContentView(view, new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT));
        dialog.show();
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pwd = content_txt.getText().toString().trim();
                if(TextUtils.isEmpty(pwd)){
                    showToast(getTextString(R.string.dialog_setting_hint_wifi_pwd));
                    return;
                }
                showProgressDialog(getTextString(R.string.camera_status_connecting));
                int wifiItemId = wifiUtils.IsConfiguration("\""+result.SSID+"\"");
                if(wifiItemId != -1){
                    if(wifiUtils.ConnectWifi(wifiItemId)){//连接指定WIFI
                        //连接成功
                        //hideProgressDialog();
                        //showToast("连接成功");
                        Message msg = mHandler.obtainMessage(2,result.SSID);
                        mHandler.sendMessageDelayed(msg,2000);
                    }
                }else{

                    int netId = wifiUtils.AddWifiConfig(wlanList,result.SSID, pwd);
                    if(netId != -1){
                        wifiUtils.getConfiguration();//添加了配置信息，要重新得到配置信息
                        if(wifiUtils.ConnectWifi(netId)){
                            //连接成功
                            //hideProgressDialog();
                            //showToast("连接成功");
                            Message msg = mHandler.obtainMessage(2,result.SSID);
                            mHandler.sendMessageDelayed(msg,2000);
                        }
                    }
                    else{
                        showToast(getTextString(R.string.main_add_ap_connect_err));
                    }
                }
                dialog.dismiss();
            }
        });
        cancle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }*/

    @Override
    public void cameraStatusChange(final BCamera camera, int status) {
        /*if(isConnected){
            return;
        }
        if(status == 100){
            if(camera != null){

                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isConnected = true;
                        hideProgressDialog();
                        Intent intent = new Intent(APConfigActivity.this,APConfigWifiActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("cam",camera);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }*/
    }

    @Override
    public void onGetParamResult(long ParamNewKey, String param) {

    }

    @Override
    public void onSetParamResult(int result) {

    }
}
