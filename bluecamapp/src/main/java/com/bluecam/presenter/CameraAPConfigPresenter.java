package com.bluecam.presenter;

import com.bluenet.camManager.BCamera;
import com.bluenet.camManager.Camera;
import com.bluenet.camManager.CameraBean;
import com.bluenet.camManager.CameraContants;

/**
 * Created by Administrator on 2017/11/30.
 */

public class CameraAPConfigPresenter extends BasePresenter<ICameraAPConfigView> {
    private BCamera camera;

    public void connectCam(String camID){
        camera = new Camera();
        CameraBean bean = new CameraBean();
        bean.setDevID(camID);
        bean.setUsername("admin");
        bean.setPassword("123");
        camera.setCameraBean(bean);
        addCamera(camera);
    }
    public void addCamera(final BCamera camera)
    {
        poolExecutor.execute(new Runnable() {
            @Override
            public void run() {
                cameraManager.addCamera(camera);
            }
        });
    }
    public void getWifiStatus(BCamera camera)
    {
        if(camera == null){
            return;
        }
        this.camera = camera;
//        camera.GetCameraParam(CameraContants.ParamNewKey.GET_CGI_WIFI_PARAM_KEY);
//        camera.GetCameraParam(CameraContants.ParamNewKey.GET_CGI_WIFI_LIST_PARAM_KEY);
    }

    public void setWifiParam(String param){
        if(camera == null){
            return;
        }
     //   camera.SetCameraParam(CameraContants.ParamNewKey.SET_CGI_WIFI_PARAM_KEY,param);
    }

    @Override
    public void onDestory() {

    }
/*
    @Override
    public void onCameraStatusChangeEvent(long camHandler, int status) {
        if(camera != null){
            if(camera.getCamHandler() == camHandler){
                LogUtil.printLog("cam status =="+status);
                if(getIView() != null){
                    getIView().cameraStatusChange(camera,status);
                }
            }
        }
    }*/

//    @Override
//    public void onSetParamsEvent(long camHandler, long ParamNewKey, int nResult) {
//        if(camera != null){
//            if(camera.getCamHandler() == camHandler) {
//                if (getIView() != null) {
//                    getIView().onSetParamResult(nResult);
//                }
//            }
//        }
//    }
//
//    @Override
//    public void onGetParamsEvent(long camHandler, long ParamNewKey, String params) {
//        if(camera != null){
//            if(camera.getCamHandler() == camHandler) {
//                if (getIView() != null) {
//                    getIView().onGetParamResult(ParamNewKey, params);
//                }
//            }
//        }
//    }
}
