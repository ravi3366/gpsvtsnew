package com.bluecam.presenter;

import com.bluenet.camManager.BCamera;

/**
 * Created by Administrator on 2017/11/30.
 */

public interface ICameraAPConfigView {
    void cameraStatusChange(BCamera camera, int status);
    void onGetParamResult(long ParamNewKey, String param);
    void onSetParamResult(int result);
}
