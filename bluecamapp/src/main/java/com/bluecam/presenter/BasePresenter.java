package com.bluecam.presenter;

import android.content.Context;

import com.bluenet.camManager.CameraManager;
import com.threadpool.ThreadPoolExecutor;

/**
 * Created by Administrator on 2017/5/27.
 */

public abstract class BasePresenter<T>
{
    protected Context context;
    protected CameraManager cameraManager;
    protected ThreadPoolExecutor poolExecutor;
    //protected PushManager pushManager;

    /**
     * 当内存不足可释放内存
     */
    protected T iViewRef;

    /**
     * 绑定IView
     * @param iView
     */
    public void bindView(T iView,Context context)
    {
        iViewRef = iView;

        this.context = context;
      //  pushManager = PushManager.getPushManager();
        cameraManager = CameraManager.getDeviceManager(context);
     //   cameraManager.registerEventListener(this);
        poolExecutor = ThreadPoolExecutor.getThreadPool();
    }

    /**
     * 解除管理
     */
    public void unBindView()
    {
        if(iViewRef != null)
        {
            iViewRef = null;
        }
        onDestory();
       // cameraManager.unRegisterEventListener(this);
    }

    public T getIView()
    {
        return iViewRef;
    }



    public abstract void onDestory();


}
