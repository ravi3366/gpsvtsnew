//package com.bluecam.config_wifi;
//
//import android.annotation.SuppressLint;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.media.MediaPlayer;
//import android.net.NetworkInfo;
//import android.net.wifi.WifiInfo;
//import android.net.wifi.WifiManager;
//import android.os.Bundle;
//import android.text.method.HideReturnsTransformationMethod;
//import android.text.method.PasswordTransformationMethod;
//import android.view.KeyEvent;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.bluecam.demo.BaseActivity;
//import com.bluecam.demo.R;
//import com.bluecam.permission.PermissionUtils;
//import com.bluenet.utils.CommonUtils;
//import com.bluenet.utils.HeaderBar;
//import com.bluenet.utils.NetworkUtils;
//
//import java.util.Locale;
//
///**
// * Created by Administrator on 2017/7/1.
// */
//
//public class WifiConfigSetup2Activity extends BaseActivity
//{
//    private LinearLayout select_ssid_view;
//    private TextView wifi_ssid_et;
//    private EditText wifi_pwd_et;
//    private ImageView see_pwd_iv;
//    private Button done_btn;
//
//    private WifiManager mWifiManager;
//    private String mConnectedSsid;
//    private SharedPreferences sPre;
//    private SharedPreferences.Editor edit;
//    private int mLocalIp;
//    private boolean isSeePwd = true;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.wifi_config_setup2_screen);
//        //lanIndex = PreferencesUtil.getInt(this, CommonUtils.APP_SETTING_LANGUAGE_TOAST,0);
//        addWifiConfigActvity(this);
//        sPre = getSharedPreferences("wlan",MODE_MULTI_PROCESS);
//        edit = sPre.edit();
//        autoSetAudioVolumn(0.6f);
//        initView();
//        initNetWorkParam();
//        PermissionUtils.requestWifiServicePermission(this,this);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//    }
//
//    private void initView()
//    {
//        header_bar = (HeaderBar)findViewById(R.id.header_bar);
//        header_bar.setTitleText(getTextString(R.string.wifi_config_setup2_title));
//        header_bar.setClickListener(new HeaderBar.OnHeaderBarClickListener() {
//            @Override
//            public void back() {
//                stopAudio();
//                finish();
//            }
//        });
//
//        select_ssid_view = (LinearLayout)findViewById(R.id.select_ssid_view);
//        wifi_ssid_et = (TextView)findViewById(R.id.wifi_ssid_et);
//        wifi_pwd_et = (EditText)findViewById(R.id.wifi_pwd_et);
//        see_pwd_iv = (ImageView)findViewById(R.id.see_pwd_iv);
//
//        done_btn = (Button)findViewById(R.id.done_btn);
//        done_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                stopAudio();
//                next();
//            }
//        });
//
//        see_pwd_iv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showPwd();
//            }
//        });
//        playAudio();
//
//    }
//    MediaPlayer mediaPlayer = null;
//
//    public void stopAudio() {
//        if (mediaPlayer != null /*&& mediaPlayer.isPlaying()*/) {
//            mediaPlayer.stop();
//            //mediaPlayer.release();
//        }
//    }
//
//    public void playAudio() {
//        try {
//            if(lanIndex >1){
//                mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup2_en);
//                mediaPlayer.start();
//            }
//            else{
//                if (Locale.getDefault().getLanguage().toString().toUpperCase().equals("ZH") || Locale.getDefault().getLanguage().toString().equals("zh_CN")) {
//                    mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup2_cn);
//                    mediaPlayer.start();
//                } else {
//                    mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup2_en);
//                    mediaPlayer.start();
//                }
//            }
//
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        }
//    }
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            stopAudio();
//            finish();
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//    public void next()
//    {
//        Intent intent = new Intent(this,WifiConfigSetup3Activity.class);
//        intent.putExtra("wifi_name", wifi_ssid_et.getText().toString());
//        intent.putExtra("wifi_pwd", wifi_pwd_et.getText().toString());
//        intent.putExtra("key", CommonUtils.WIFI_KEY);
//        intent.putExtra("mLocalIp", mLocalIp);
//        edit.putString(wifi_ssid_et.getText().toString().trim(), wifi_pwd_et.getText().toString().trim());
//        edit.commit();
//        startActivity(intent);
//    }
//
//    private void showPwd()
//    {
//        if(isSeePwd)
//        {
//            isSeePwd = false;
//            wifi_pwd_et.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
//            see_pwd_iv.setImageResource(R.mipmap.setting_see_passwd);
//        }
//        else
//        {
//            isSeePwd = true;
//            wifi_pwd_et.setTransformationMethod(PasswordTransformationMethod.getInstance());
//            see_pwd_iv.setImageResource(R.mipmap.setting_nosee_passwd);
//        }
//        CommonUtils.toTextEnd(wifi_pwd_et);
//    }
//
//    @SuppressLint("WifiManagerLeak")
//    private void initNetWorkParam()
//    {
//        mWifiManager = (WifiManager) getSystemService (WIFI_SERVICE);
//        if(mWifiManager.isWifiEnabled())
//        {
//            WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
//
//            mConnectedSsid = wifiInfo.getSSID();
//            int iLen = mConnectedSsid.length();
//
//            if ("".equals(mConnectedSsid) || "<unknown ssid>".equals(mConnectedSsid))
//            {
//                NetworkInfo info =  NetworkUtils.getActiveNetworkInfo(this);
//                if(info != null)
//                    mConnectedSsid = info.getExtraInfo();
//
//                if(mConnectedSsid == null){
//                    return;
//                }
//            }
//
//            if (mConnectedSsid.startsWith("\"") && mConnectedSsid.endsWith("\""))
//            {
//                mConnectedSsid = mConnectedSsid.substring(1, iLen - 1);
//            }
//
//            mLocalIp = wifiInfo.getIpAddress();
//            wifi_ssid_et.setText(mConnectedSsid);//设置wifi名称
//
//            String passwd = sPre.getString(mConnectedSsid, "");
//            wifi_pwd_et.setText(passwd);
//        }
//        else
//        {
//            showDialog(getTextString(R.string.wifi_config_wifi_enable),true);
//        }
//    }
//
//    @Override
//    protected void onDialogOK() {
//        finish();
//        removeWifiConfigActivity();
//    }
//}
