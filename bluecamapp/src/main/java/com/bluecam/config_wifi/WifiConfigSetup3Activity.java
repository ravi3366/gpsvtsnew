//package com.bluecam.config_wifi;
//
//import android.app.Dialog;
//import android.graphics.drawable.ColorDrawable;
//import android.media.MediaPlayer;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.text.Html;
//import android.view.Gravity;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.PopupWindow;
//import android.widget.TextView;
//
//import com.bluecam.demo.BaseActivity;
//import com.bluecam.demo.CommonActivity;
//import com.bluecam.demo.R;
//import com.bluenet.api.DeviceAPI;
//import com.bluenet.utils.CommonUtils;
//import com.bluenet.utils.HeaderBar;
//import com.bluenet.utils.ImageAnim;
//import com.bluenet.utils.LogUtil;
//import com.mediatek.elian.ElianNative;
//
//import java.util.Locale;
//
//import voice.encoder.DataEncoder;
//import voice.encoder.VoicePlayer;
//import voice.encoder.VoicePlayerListener;
//
//
///**
// * Created by Administrator on 2017/7/1.
// */
//
//public class WifiConfigSetup3Activity extends BaseActivity {
//    private TextView done_btn;
//    private TextView toast1_tv;
//    private TextView toast2_tv;
//    private ImageView cam_view;
//
//    private String wifi_name;
//    private String wifi_pwd;
//    private String key;
//    private int mLocalIp;
//    private VoicePlayer player;
//    private boolean isExit = false;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.wifi_config_setup3_screen);
//
//        wifi_name = getIntent().getStringExtra("wifi_name");
//        wifi_pwd = getIntent().getStringExtra("wifi_pwd");
//        key = getIntent().getStringExtra("key");
//        mLocalIp = getIntent().getIntExtra("mLocalIp", 0);
//        LogUtil.printLog("wifi_name == " + wifi_name + " ,wifi_pwd == " + wifi_pwd + " ,key == " + key + " , mLocalIp == " + mLocalIp);
//        addWifiConfigActvity(this);
//        initView();
//        autoSetAudioVolumn(0.6f);
//
//        player = DeviceAPI.startVoicePlayer();
//        player.setListener(new VoicePlayerListener() {
//            @Override
//            public void onPlayEnd(VoicePlayer arg0) {
//
//            }
//
//            @Override
//            public void onPlayStart(VoicePlayer arg0) {
//            }
//        });
//
//        int libVersion = ElianNative.GetLibVersion();
//        int protoVersion = ElianNative.GetProtoVersion();
//        LogUtil.printLog("WifiConfigSetup3Activity elian libVersion==" + libVersion + " protoVersion==" + protoVersion);
//    }
//
//
//    private void startSendVoice() {
//        poolExecutor.execute(new Runnable() {
//            @Override
//            public void run() {
//                if (player == null) {
//                    return;
//                }
//                int count = 0;
//                do {
//                    if (isExit) {
//                        player.stop();
//                        return;
//                    }
//                    player.play(DataEncoder.encodeSSIDWiFi(wifi_name, wifi_pwd), 1, 200);
//                    try {
//                        Thread.sleep(5000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    count = count + 1;
//                    //第1次声波是150频率的递增
//                    //为了适配旧设备，在后面俩次的声波 改回+200 频率
//
//                    if(count>0){
//                        int freqs[] = {4000, 4200, 4400, 4600, 4800, 5000, 5200, 5400, 5600, 5800, 6000, 6200, 6400, 6600, 6800, 7000, 7200, 7400, 7600};
//                        player.setFreqs(freqs);
//                    }
//
//
//                } while (count <= 2);
//            }
//        });
//    }
//
//    private void startSendElian() {
//        poolExecutor.execute(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                int libVersion = ElianNative.GetLibVersion();
//                int protoVersion = ElianNative.GetProtoVersion();
//                LogUtil.printLog("WifiConfigSetup3Activity send Elian ssid==" + wifi_name + " pwd==" + wifi_pwd + " key==" + key);
//                ElianNative.InitSmartConnection(null, 0, 1);
//                int ret = ElianNative.StartSmartConnection(wifi_name, wifi_pwd, key);
//                LogUtil.printLog("StartSmartConnection ret == " + ret);
//            }
//        });
//    }
//
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        stopConnect();
//    }
//
//    private void stopConnect() {
//        isExit = true;
//        ElianNative.StopSmartConnection();
//        if (player != null) {
//            LogUtil.printLog("Voice send is stop ==" + player.isStopped());
//            player.stop();
//        }
//    }
//
//    private void initView() {
//        header_bar = (HeaderBar) findViewById(R.id.header_bar);
//        header_bar.setTitleText(getTextString(R.string.wifi_config_setup3_title));
//        header_bar.setClickListener(new HeaderBar.OnHeaderBarClickListener() {
//            @Override
//            public void back() {
//                stopAudio();
//                stopConnect();
//                finish();
//            }
//        });
//
//
//        toast1_tv = (TextView) findViewById(R.id.toast1_tv);
//        toast2_tv = (TextView) findViewById(R.id.toast2_tv);
//        StringBuffer toast1 = new StringBuffer();
//        StringBuffer toast2 = new StringBuffer();
//
//        if (Locale.getDefault().getLanguage().toString().toUpperCase().equals("ZH") || Locale.getDefault().getLanguage().toString().equals("zh_CN")) {
//            toast1.append(getTextString(R.string.wifi_config_setup3_toast1)).append(CommonUtils.getFond(getTextString(R.string.wifi_config_setup3_toast2), "#EB7373"));
//            toast2.append(getTextString(R.string.wifi_config_setup3_toast3)).append(CommonUtils.getFond(getTextString(R.string.wifi_config_setup3_toast4), "#EB7373"));
//            toast1_tv.setText(Html.fromHtml(toast1.toString()));
//            toast2_tv.setText(Html.fromHtml(toast2.toString()));
//        } else {
//            toast1.append(getTextString(R.string.wifi_config_setup3_toast1)).append("\n");
//            toast2.append(getTextString(R.string.wifi_config_setup3_toast3)).append("\n");
//            toast1_tv.setText(toast1.toString());
//            toast1_tv.append(Html.fromHtml(CommonUtils.getFond(getTextString(R.string.wifi_config_setup3_toast2), "#EB7373")));
//            toast2_tv.setText(toast2.toString());
//            toast2_tv.append(Html.fromHtml(CommonUtils.getFond(getTextString(R.string.wifi_config_setup3_toast4), "#EB7373")));
//        }
//
//        cam_view = (ImageView) findViewById(R.id.cam_view);
//        //AnimationUtils.translateLeftInAnim(cam_view);
//
//        done_btn = (TextView) findViewById(R.id.done_btn);
//        done_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showSendVoiceDialog();
//                startSendVoice();
//                startSendElian();
//                stopAudio();
//            }
//        });
//        playAudio();
//
//    }
//
//    public void stopAudio() {
//        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
//            mediaPlayer.stop();
//            mediaPlayer.release();
//        }
//    }
//
//    MediaPlayer mediaPlayer = null;
//
//    public void playAudio() {
//        try {
//            if (lanIndex > 1) {
//                mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup3_en);
//                mediaPlayer.start();
//            } else {
//
//
//                if (Locale.getDefault().getLanguage().toString().toUpperCase().equals("ZH") ||
//                        Locale.getDefault().getLanguage().toString().equals("zh_CN")) {
//                    mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup3_cn);
//                    mediaPlayer.start();
//                } else {
//                    mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup3_en);
//                    mediaPlayer.start();
//                }
//            }
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//    //private AnimationDrawable sendAnimationDrawable;
//    private ImageAnim anim;
//    private Dialog dialog;
//    private PopupWindow dialogWin;
//    private ImageView send_iv;
//    private TextView timer_tv;
//    private int timer = 20;
//    private Handler timerHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            if (timer == 1) {
//                stopAnimation();
//                openActivity("wifi_config", CommonActivity.class, "com.camera.fragment.Add_SearchFragment");
//
//                stopConnect();
//                finish();
//                removeWifiConfigActivity();
//                try {
//                    if (dialogWin != null && dialogWin.isShowing()) {
//                        dialogWin.dismiss();
//                    }
//                } catch (Exception e) {
//                }
//                return;
//            }
//            timer = timer - 1;
//            timer_tv.setText(timer + "s");
//            timerHandler.sendEmptyMessageDelayed(0, 1000);
//        }
//    };
//
//    private void showSendVoiceDialog() {
//        View view = LayoutInflater.from(this).inflate(R.layout.wifi_send_voice_dialog, null);
//        send_iv = (ImageView) view.findViewById(R.id.send_iv);
//        timer_tv = (TextView) view.findViewById(R.id.timer_tv);
//        timer_tv.setText(timer + "s");
//        startAnimation();
//        dialogWin = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        dialogWin.setFocusable(false);
//        dialogWin.setOutsideTouchable(false);
//        dialogWin.setBackgroundDrawable(new ColorDrawable(0));
//        dialogWin.showAtLocation(cam_view, Gravity.CENTER, 0, 0);
//        timerHandler.sendEmptyMessageDelayed(0, 1000);
//    }
//
//    private int imgResId[] = {R.mipmap.voice_anim1, R.mipmap.voice_anim2, R.mipmap.voice_anim3, R.mipmap.voice_anim4, R.mipmap.voice_anim5, R.mipmap.voice_anim6,
//            R.mipmap.voice_anim7, R.mipmap.voice_anim8, R.mipmap.voice_anim9, R.mipmap.voice_anim10};
//
//    private void startAnimation() {
//        if (send_iv == null)
//            return;
//        anim = new ImageAnim();
//        anim.startAnim(send_iv, imgResId, 200);
//
//
//    }
//
//    private void stopAnimation() {
//        if (anim != null) {
//            anim.stopAnim();
//        }
//    }
//
//}
