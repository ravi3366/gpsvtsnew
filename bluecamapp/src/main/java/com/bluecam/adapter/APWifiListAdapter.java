package com.bluecam.adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.bluecam.demo.R;
import com.bluenet.utils.WifiUtils;

import java.util.List;

public class APWifiListAdapter extends RecyclerView.Adapter<APWifiListAdapter.ApWlanViewHolder>{

    private List<ScanResult> mDatas;
    private Context mContext;
    private LayoutInflater inflater;
    private OnWlanItemClickListener onItemClickListener;
    private WifiUtils wifiUtils;

    public void setWifiUtils(WifiUtils wifiUtils) {
        this.wifiUtils = wifiUtils;
    }

    public APWifiListAdapter(Context context, List<ScanResult> datas, OnWlanItemClickListener onItemClickListener){
        this. mContext=context;
        this. mDatas=datas;
        this.onItemClickListener = onItemClickListener;
        inflater=LayoutInflater. from(mContext);
    }

    @Override
    public ApWlanViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.ap_wlan_list_item,viewGroup, false);
        ApWlanViewHolder viewHolder = new ApWlanViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ApWlanViewHolder viewHolder, int position) {
        ScanResult result = mDatas.get(position);
        viewHolder.status_txt.setVisibility(View.GONE);
        viewHolder.wifi_item_fy.setOnClickListener(new OnWlanItemListener(result));
        viewHolder.ssid_txt.setText(result.SSID);
        viewHolder.sign_txt.setText(WifiManager.calculateSignalLevel(result.level, 100)+"%");
        if(wifiUtils != null){
            WifiInfo wifiInfo = wifiUtils.getWifiConnectedInfo();
            String ssid = "\""+result.SSID+"\"";
            if(wifiInfo.getSSID().equals(ssid)){
                viewHolder.status_txt.setVisibility(View.VISIBLE);
                viewHolder.status_txt.setText(mContext.getResources().getString(R.string.setting_wifi_connected));
            }
        }
        //LogUtil.printLog("result.SSID ====="+result.SSID);

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }


    class ApWlanViewHolder extends RecyclerView.ViewHolder {
        FrameLayout wifi_item_fy;
        TextView    ssid_txt;
        TextView   sign_txt;
        TextView   status_txt;



        public ApWlanViewHolder(View view) {
            super(view);
            wifi_item_fy  = (FrameLayout)view.findViewById(R.id.wifi_item_fy);
            ssid_txt      = (TextView)view.findViewById(R.id.ssid_txt);
            sign_txt      = (TextView)view.findViewById(R.id.sign_txt);
            status_txt    = (TextView)view.findViewById(R.id.status_txt);
        }
    }

    private class OnWlanItemListener implements View.OnClickListener
    {
        ScanResult result;

        public OnWlanItemListener(ScanResult result) {
            this.result = result;
        }


        @Override
        public void onClick(View v) {
            if(onItemClickListener != null)
                onItemClickListener.onItemClick(result);
        }
    }

    public  interface OnWlanItemClickListener
    {
        void onItemClick(ScanResult result);

    }
}
