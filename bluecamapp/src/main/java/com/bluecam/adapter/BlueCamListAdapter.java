package com.bluecam.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bluecam.demo.BlueCamResponse;
import com.bluecam.demo.R;
import com.bluenet.utils.WifiUtils;

import java.util.List;

public class BlueCamListAdapter extends RecyclerView.Adapter<BlueCamListAdapter.ViewHolder>{

    private List<BlueCamResponse> mDatas;
    private Context mContext;
    private LayoutInflater inflater;
    private WifiUtils wifiUtils;



    public BlueCamListAdapter(Context context, List<BlueCamResponse> datas){
        this. mContext=context;
        this. mDatas=datas;
        inflater=LayoutInflater. from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.item_camera,viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        
        String camName = mDatas.get(position).getCamName();
        String shortName = mDatas.get(position).getShortName();
        String userName = mDatas.get(position).getUserName();
        String pwd = mDatas.get(position).getPwd();
        String camId = mDatas.get(position).getCamId();
        String camType = mDatas.get(position).getCameraType();
        viewHolder.textViewCamStatus.setText("Online");
        viewHolder.textViewCamName.setText(camName);
        viewHolder.textViewCamShortName.setText(shortName);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction("appendChatScreenMsg");
                i.putExtra("position",position);
                mContext.sendBroadcast(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView = null;
        TextView    textViewCamShortName;
        TextView   textViewCamName;
        TextView   textViewCamStatus;


        public ViewHolder(View view) {
            super(view);
            imageView  = (ImageView) view.findViewById(R.id.imageView);
            textViewCamShortName      = (TextView)view.findViewById(R.id.textViewCamShortName);
            textViewCamName      = (TextView)view.findViewById(R.id.textViewCamName);
            textViewCamStatus    = (TextView)view.findViewById(R.id.textViewCamStatus);
        }


    }


}
