package com.bluecam.permission;

import android.Manifest;
import android.content.Context;

import com.bluenet.utils.LogUtil;


/**
 * Created by apple on 16/12/28.
 * 具体权限处理
 */

public class PermissionUtils {
    /**
     * 请求相机权限
     *
     * @param context 当前context，activity或fragment
     * @param ctx     getContext
     */
    public static void requestCameraPermission(final Object context, final Context ctx, final String activityId) {
        PermissionHelper.getInstance().setPermissionListener(new OnPermissionCallback() {
            @Override
            public void onSuccess() {
//                ActivityUtil.startCaptureActivity(ctx, activityId);
            }

            @Override
            public void onFailure() {
                PermissionHelper.getInstance().showPermissionDialog(ctx,
                        "为了正常使用扫码功能，请在设置中开启相机权限");
            }
        });
        PermissionHelper.getInstance().requestPermission(context, Manifest.permission.CAMERA);
    }

    public static void requestWifiServicePermission(final Object context,final Context ctx)
    {
        PermissionHelper.getInstance().setPermissionListener(new OnPermissionCallback() {
            @Override
            public void onSuccess() {
                LogUtil.printLog("requestWifiServicePermission onSuccess");
            }

            @Override
            public void onFailure() {
                PermissionHelper.getInstance().showPermissionDialog(ctx,
                        "为了正常使用WIFI网络功能，请在设置中开启WIFI权限");
            }
        });
        PermissionHelper.getInstance().requestPermission(context, Manifest.permission.ACCESS_WIFI_STATE);
    }

    public static void requestAudioServicePermission(final Object context,final Context ctx)
    {
        PermissionHelper.getInstance().setPermissionListener(new OnPermissionCallback() {
            @Override
            public void onSuccess() {
                LogUtil.printLog("requestAudioServicePermission onSuccess");
            }

            @Override
            public void onFailure() {
                PermissionHelper.getInstance().showPermissionDialog(ctx,
                        "为了正常使用录音功能，请在设置中开启录音权限");
            }
        });
        PermissionHelper.getInstance().requestPermission(context, Manifest.permission.RECORD_AUDIO);
    }

    /**
     * 存储权限
     */
    public interface OnSdCardListener {
        void onSuccess();
    }

    public static void requestSdCardPermission(final Object context, final Context ctx, final OnSdCardListener listener) {
        PermissionHelper.getInstance().setPermissionListener(new OnPermissionCallback() {
            @Override
            public void onSuccess() {
                if (listener != null) {
                    listener.onSuccess();
                }
            }

            @Override
            public void onFailure() {
                PermissionHelper.getInstance().showPermissionDialog(ctx,
                        "为了正常使用App，请在设置中开启存储权限");
            }
        });
        PermissionHelper.getInstance().requestPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    /**
     * 上传头像时，请求相机和内存卡权限
     *
     * @param context  当前context，activity或fragment
     * @param ctx      getContext
     */
    public static void requestCameraAndSdCardPermission(final Object context, final Context ctx) {
        PermissionHelper.getInstance().setPermissionListener(new OnPermissionCallback() {
            @Override
            public void onSuccess() {
                //DialogUtils.getInstance().showSelectImgDialog(ctx, listener);
            }

            @Override
            public void onFailure() {
                PermissionHelper.getInstance().showPermissionDialog(ctx,
                        "为了更换头像，请在设置中开启相机,存储空间和录音权限");
            }
        });
        String[] permissions = {Manifest.permission.ACCESS_WIFI_STATE,Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.MODIFY_AUDIO_SETTINGS,Manifest.permission.WAKE_LOCK,Manifest.permission.READ_PHONE_STATE};
        PermissionHelper.getInstance().requestPermissions(context, permissions);
    }

    public static void onRequestPermissionsResult(int requestCode, int[] grantResults) {
        PermissionHelper.getInstance().onRequestPermissionsResult(requestCode, grantResults);
    }


    public static void requestGpsPermission(final Object context, final Context ctx){
        PermissionHelper.getInstance().setPermissionListener(new OnPermissionCallback() {
            @Override
            public void onSuccess() {
                //DialogUtils.getInstance().showSelectImgDialog(ctx, listener);
            }

            @Override
            public void onFailure() {
                PermissionHelper.getInstance().showPermissionDialog(ctx,
                        "Gps right!");
            }
        });

        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        PermissionHelper.getInstance().requestPermissions(context, permissions);
    }

    /*public interface OnBleListener{
        void onSuccess();
    }

    public static void requestBlePermission(final Object context, final Context ctx, OnBleListener listener) {
        PermissionHelper.getInstance().setPermissionListener(new OnPermissionCallback() {
            @Override
            public void onSuccess() {
                listener.onSuccess();
            }

            @Override
            public void onFailure() {
                PermissionHelper.getInstance().showPermissionDialog(ctx,
                        "为了搜索蓝牙,请开启定位权限");
            }
        });
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        PermissionHelper.getInstance().requestPermissions(context, permissions);
    }*/


}
