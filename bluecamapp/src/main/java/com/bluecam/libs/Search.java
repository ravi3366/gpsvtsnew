package com.bluecam.libs;

import android.util.Log;

public class Search {
	private static final String TAG = "Search";
	
	static {
		System.loadLibrary("PPCS_API");
		System.loadLibrary("BCNetSDK");
	}

	private static class SearchHodler {
		static Search instance = new Search();
	}

	public static Search getInstance() {
		return SearchHodler.instance;
	}

	public void SearchCallBack(String data)
	{
		Log.i(TAG, data);
	}
	
	public static native int InitSearchEnv();
	public static native void CleanupSearchEnv();
	public static native int SearchDevice();
	// 含有void SearchCallBack(String data)方法的类
	public static native int SetSearchDeviceCallBack(Object callbacker);
};