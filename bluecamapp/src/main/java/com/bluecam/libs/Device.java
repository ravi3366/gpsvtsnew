package com.bluecam.libs;

import android.util.Log;

/**
 * Created by Administrator on 2017/7/17.
 */

public class Device {

    private static final String TAG = "Device";

    static{
        System.loadLibrary("PPCS_API");
        System.loadLibrary("BCNetSDK");
    }

    private static class DeviceHodler {
        static Device instance = new Device();
    }

    public static Device getInstance() {
        return DeviceHodler.instance;
    }

    public void SDKEventCallBack(int uid, int eventid){
        Log.i(TAG,  "uid:" + uid + " SDKEvent:" + eventid);
    }

    public void AlarmCallBack(int uid, int eventid, String data){
        Log.i(TAG, "uid:" + uid + " eventid:" + eventid + " data:" + data);
    }

    public void ConfigCallBack(int uid, int cmd, byte[] data, int len){
        //Log.i(TAG, "uid:" + uid + " cmd:0x" + Integer.toHexString(cmd) + " len:" + len + " data:" + data);
    }

    public void RealDataCallBack(int uid, int type, byte[] data, int size){
        Log.i(TAG, "uid:" + uid + " RealData type:" + type + " size:" + size);
    }
    public void PlayBackCallBack(int uid, int type, byte[] data, int size){
        Log.i(TAG, "uid:" + uid + " PlayBack type:" + type + " size:" + size);
    }

    public void TransmitFileCallBack(int uid, int eventid) {
        Log.i(TAG,  "uid:" + uid + " TransmitFile eventid:" + eventid);
    }




    public static native int Init();
    public static native int InitEx(Object assetManager, String filename);
    public static native void Cleanup();
    public static native int NetDetect();

    public static native int CreateDevice(String deviceid, String username, String password);
    public static native int DeleteDevice(int uid);
    public static native int SetAutoReconnect(int uid, int recon, int trytimes);
    public static native int Login(int uid, int timeout);
    public static native int Logout(int uid);
    public static native String SDKVersion();
    public static native int PPCSVersion();
    public static native int GetLinkMode(int uid);
    public static native int SetSDKCallBack(int uid, Object callbacker);




    public static native int OpenStream(int uid, int streamid);
    public static native int CloseStream(int uid);

    public static native int OpenSound(int uid,int streamid);
    public static native int CloseSound(int uid);
    public static native int OpenTalk(int uid);
    public static native int CloseTalk(int uid);
    public static native int SendTalkData(int uid, byte[] data, int len);

    public static native int OpenPlayBack(int uid, String filename, int pos);
    public static native int OpenPlayBackByTime(int uid, int second);
    public static native int ClosePlayBack(int uid);
    public static native int ConfigData(int uid, int cmd, String in);
    /*
     * 传输文件到前端
     * transInfo: Json数据格式,{"filename":"", "filetype":"", "filever":"", "filesize":0,"filemd5":"","forceflag":0}
     *	filetype: 1-system fireware, 2-application fireware
     *	foreflag: 0-前端判断更新 1-强制更新文件
     * filePath: like /root/x.suffix
     */
    public static native int TransmitFile(int uid, String transInfo, String filePath);

}
