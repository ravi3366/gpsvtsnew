package com.decode.h264;

import com.bluecam.libs.Adpcm;
import com.decode.utils.CustomBufferData;
import com.decode.utils.CustomBufferHead;

public class AdpcmToPcm {

    private boolean bAudioAac = false;
    private Thread aacThread = null;

    private CameraHelper mObject;

    public AdpcmToPcm(CameraHelper Object) {
        this.mObject = Object;
    }

    public boolean AudioAACStart(){
        synchronized (this) {
            if (bAudioAac) {
                return true;
            }
            bAudioAac = true;
            aacThread = new Thread(new AdpcmToPcm.aacThread());
            aacThread.start();
        }
        return true;
    }
    public void AudioAACStop(){
        synchronized (this) {
            if (!bAudioAac || aacThread == null) {
                return ;
            }
            bAudioAac = false;
            try {
                aacThread.join();
            } catch (Exception e) {
            }
            aacThread = null;
        }
    }


    byte buffer[] = new byte[1024*4];
    class aacThread implements Runnable {
        @Override
        public void run() {
            while (bAudioAac)
            {
                CustomBufferData data = null;
                int nRet = Adpcm.getInstance().adpcm_readdata(buffer,1024);

                if(nRet>0){
                    CustomBufferHead head = new CustomBufferHead();
                    CustomBufferData bufferdata = new CustomBufferData();

                    head.length = nRet;
                    head.startcode = 0;

                    bufferdata.head = head;
                    bufferdata.data = buffer;
                    data = bufferdata;
                }else{
                    data = null;
                }

                if (data == null)
                {
                    try {
                        Thread.sleep(10);
                        continue;
                    } catch (Exception e) {

                        return;
                    }
                }

                mObject.setAudioBuffer(data);
            }
        }
    }
}
