package com.decode.h264;

import android.media.MediaCodecInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class CodecThread extends Thread {
    private String TAG = "CodecThread";

    private Handler codecHandler;

    private final int MSG_DECODE = 1;
    private final int MSG_RESET = 2;//复位解码

    private Object mDecEncLock;

    private Queue<PreviewBufferInfo> mDecodeBuffers_clean;//开辟的固定大小5*DECODE_UNI_SIZE 的QUENU
    private Queue<PreviewBufferInfo> mDecodeBuffers_dirty;//编码后的数据

    private int PREVIEW_POOL_CAPACITY = 5;
    private final int DECODE_UNI_SIZE = 1024 * 3 * 1024;

    private byte[] mReceveBuf    = new byte[DECODE_UNI_SIZE];//需要优化

    private Decoder mDecoder;
    //private Object mRender = null;

    private String path;

    private int FRAME_RATE = 15;

    //这个值用于找到第一个帧头后，继续寻找第二个帧头，如果解码失败可以尝试缩小这个值
    private final int FRAME_MIN_LEN = 512;//20;
    //一般H264帧大小不超过200k,如果解码失败可以尝试增大这个值
    private final int FRAME_MAX_LEN =  1024 * 4 * 1024;
    //根据帧率获取的解码每帧需要休眠的时间,根据实际帧率进行操作
    private int PRE_FRAME_TIME = 1000 / FRAME_RATE;

    private int mWidth = 1920;//352;
    private int mHeight = 1080;//288;


    private OnRenderListener mRender = null;

    private class PreviewBufferInfo {
        public byte[] buffer;
        public int size;
        public long timestamp;
    }

    public CodecThread(){
        this.path = null;
        this.mRender = null;
        mDecEncLock = new Object();
        initQueues();
    }

    public void SetStreamCallBack(OnRenderListener render){
        if(render != null){
            if(this.mRender != render){
                this.mRender = render;
            }
        }else{
            this.mRender = null;
        }
    }

    public boolean checkDeCodeAlive(){
        if(mDecoder != null){
            return mDecoder.isAlive();
        }
        return false;
    }

    /**
     * 初始化各种队列
     */
    private void initQueues() {

        if (mDecodeBuffers_clean == null)
            mDecodeBuffers_clean = new LinkedList<>();
        if (mDecodeBuffers_dirty == null)
            mDecodeBuffers_dirty = new LinkedList<>();

        for (int i = 0; i < PREVIEW_POOL_CAPACITY; i++) {
            PreviewBufferInfo info = new PreviewBufferInfo();
            info.buffer = new byte[DECODE_UNI_SIZE];
            info.size = 0;
            info.timestamp = 0;
            mDecodeBuffers_clean.add(info);
        }
    }

    @Override
    public void run() {
        Looper.prepare();

        codecHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {

                    case MSG_RESET:
                        synchronized (mDecEncLock) {

                            if (mDecodeBuffers_dirty != null && mDecodeBuffers_clean != null) {
                                Iterator<PreviewBufferInfo> ite = mDecodeBuffers_dirty.iterator();
                                while (ite.hasNext()) {
                                    PreviewBufferInfo info = ite.next();
                                    ite.remove();
                                    mDecodeBuffers_clean.add(info);
                                }

                                if(codecHandler!=null && mDecoder!=null && mDecoder.isAlive()){
                                    codecHandler.sendEmptyMessage(MSG_DECODE);
                                }
                            }

                        }break;
                    case MSG_DECODE:
                        int result = Decoder.BUFFER_OK;

                        synchronized (mDecEncLock) {
                            //STEP 1: handle input buffer
                            if (mDecodeBuffers_dirty != null && mDecodeBuffers_clean != null) {
                                Iterator<PreviewBufferInfo> ite = mDecodeBuffers_dirty.iterator();//获取待解码数据
                                while (ite.hasNext()) {
                                    PreviewBufferInfo info = ite.next();

                                    if(mDecoder != null){
                                        result = mDecoder.input(info.buffer, info.size, info.timestamp);//输入解码数据
                                    }

                                    if (result != Decoder.BUFFER_OK) {
                                        break;     //the rest buffers shouldn't go into encoder, if the previous one get problem
                                    } else {
                                        ite.remove();
                                        mDecodeBuffers_clean.add(info);//PreviewBufferInfo 重新回到 mDecodeBuffers_clean中
                                    }

                                }
                            }
                        }

                        int[] len = new int[1];
                        long[] ts = new long[1];

                        int[]ww = new int[1];
                        int[]hh = new int[1];
                        int[]fmt= new int[1];

                        while (result == Decoder.BUFFER_OK) {

                            if(mDecoder != null){
                                byte[] data = new byte[DECODE_UNI_SIZE];

                                result = mDecoder.output(data, len, ts,ww,hh,fmt);//输出解码数据，传入数据空间大小，返回解码后的数据。
                                if(result == Decoder.BUFFER_OK){
                                    DrawerYuvData(data,len[0],ww[0],hh[0],fmt[0]);
                                }
                            }
                        }

                        if(codecHandler != null ){
                            codecHandler.sendEmptyMessageDelayed(MSG_DECODE, 30);//延迟30，继续下次解码工作。
                        }

                        break;
                }
            }
        };
        Looper.loop();

    }

    private void DrawerYuvData(byte []data,int len,int w,int h,int fmt){

        if(mRender != null ){

            //ok
            //AvcUtils.swapNV12toYV12(mDecodeNV12Buf,mDecodeYUVBuf,w,h);
            //shaderRender.writeYuvDate(mDecodeNV12Buf,mDecodeYUVBuf,w,h);

            switch (fmt) {
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:{
                    byte[] nv12Buffer = new byte[DECODE_UNI_SIZE];
                    AvcUtils.YV12toNV21(data,nv12Buffer,w,h);

                    byte[] i420Buffer = new byte[DECODE_UNI_SIZE];
                    AvcUtils.swapYV12toI420(data,i420Buffer,w,h);

                    writeYuvData(nv12Buffer,i420Buffer,w,h);
                } break;

                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV411Planar:
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV411PackedPlanar:
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedPlanar:
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedSemiPlanar:
                    {
                    //data = AvcUtils.yuv420spToYuv420P(data, w[0],h[0]);
                    //常见颜色格式映射如下：
                    //原始数据 --->编码器
                    //NV12(YUV420sp) ———> COLOR_FormatYUV420PackedSemiPlanar
                    //NV21 ———-> COLOR_FormatYUV420SemiPlanar
                    //YV12(I420) ———-> COLOR_FormatYUV420Planar

                    byte[] yuvBuffer = new byte[DECODE_UNI_SIZE];
                    AvcUtils.swapNV12toYV12(data,len,yuvBuffer,w,h);
                    writeYuvData(data,yuvBuffer,w,h);
                }break;
                default:
                    byte[] yuvBuffer = new byte[DECODE_UNI_SIZE];
                    AvcUtils.swapNV12toYV12(data,len,yuvBuffer,w,h);
                    writeYuvData(data,yuvBuffer,w,h);
                    break;
            }
        }
    }

    private void writeYuvData(byte[] NV12OfByte, byte[] yuvOfByte, int width, int height){

        if(mRender != null){
            mRender.writeYuvData(NV12OfByte,yuvOfByte,width,height);
        }
    }

    private boolean initDecoder(byte[]data, int len) {

        if(mDecoder == null){
            mDecoder = new Decoder();
            try {
                mDecoder.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(!mDecoder.isAlive())
        {

            byte[] sps_nal = null;
            int sps_len = 0;
            byte[] pps_nal = null;
            int pps_len = 0;

            ByteBuffer byteb = ByteBuffer.wrap(data, 0, len);
            //SPS
            if (true == AvcUtils.goToPrefix(byteb)) {
                int sps_position = 0;
                int pps_position = 0;
                int nal_type = AvcUtils.getNalType(byteb);

                if (AvcUtils.NAL_TYPE_SPS == nal_type) {
                    Log.d(TAG, "OutputAvcBuffer, AVC NAL type: SPS");
                    int pos = byteb.position();
                    sps_position = pos - AvcUtils.START_PREFIX_LENGTH - AvcUtils.NAL_UNIT_HEADER_LENGTH;
                    //PPS
                    if (true == AvcUtils.goToPrefix(byteb)) {
                        nal_type = AvcUtils.getNalType(byteb);
                        if (AvcUtils.NAL_TYPE_PPS == nal_type) {
                            int pos1= byteb.position();
                            //byteb.get();byteb.get();
                            pps_position = byteb.position() - AvcUtils.START_PREFIX_LENGTH - AvcUtils.NAL_UNIT_HEADER_LENGTH;
                            sps_len = pps_position - sps_position;
                            sps_nal = new byte[sps_len];
                            int cur_pos = byteb.position();
                            byteb.position(sps_position);
                            byteb.get(sps_nal, 0, sps_len);
                            byteb.position(cur_pos);
                            //slice
                            if (true == AvcUtils.goToPrefix(byteb)) {
                                nal_type = AvcUtils.getNalType(byteb);
                                int pps_end_position = byteb.position() - AvcUtils.START_PREFIX_LENGTH - AvcUtils.NAL_UNIT_HEADER_LENGTH;
                                pps_len = pps_end_position - pps_position;
                            } else {
                                pps_len = byteb.position() - pps_position;
                                //pps_len = byteb.limit() - pps_position + 1;
                            }
                            if (pps_len > 0) {
                                pps_nal = new byte[pps_len];
                                cur_pos = byteb.position();
                                byteb.position(pps_position);
                                byteb.get(pps_nal, 0, pps_len);
                                byteb.position(cur_pos);
                            }

                        } else {
                            //Log.d(log_tag, "OutputAvcBuffer, AVC NAL type: "+nal_type);
                            throw new UnsupportedOperationException("SPS is not followed by PPS, nal type :" + nal_type);
                        }
                    }

                }

                //2. configure AVC decoder with SPS/PPS
                if (sps_nal != null && pps_nal != null) {
                    try {
                        int[] width = new int[1];
                        int[] height = new int[1];

                        if (AvcUtils.parseSPS(sps_nal, width, height)) {
                            mWidth = width[0];
                            mHeight = height[0];

                            mDecoder.configure(sps_nal, pps_nal, null);
                        }
                        else {
                            //byte[] tmp = new byte[15];
                            //把data中索引0开始的15个数字复制到tmp中索引为0的位置上
                            // System.arraycopy(data, 0, tmp, 0, 15);
                            //mDecoder.configure(tmp,mWidth,mHeight,surface);

                            mDecoder.configure(sps_nal, mWidth, mHeight, null);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    mDecoder.start();
                    if (codecHandler != null) {
                        codecHandler.sendEmptyMessage(MSG_DECODE);//发起解码
                    }
                }
            }
        }

        return mDecoder.isAlive();
    }

    //视频数据转换Frame
    int frameLen = 0;//当前帧长度
    public void writeDate(byte[]readData, int readLen){

        try {
            //当前长度小于最大值
            if (frameLen + readLen < FRAME_MAX_LEN) {
                //将readData拷贝到frame
                System.arraycopy(readData, 0, mReceveBuf, frameLen, readLen);
                //修改frameLen
                frameLen += readLen;
                //寻找第一个帧头
                int headFirstIndex = AvcUtils.findHead(mReceveBuf, 0, frameLen);
                while (headFirstIndex >= 0 && AvcUtils.isHead(mReceveBuf, headFirstIndex)) {
                    //寻找第二个帧头
                    int headSecondIndex = AvcUtils.findHead(mReceveBuf, headFirstIndex + FRAME_MIN_LEN, frameLen);
                    //如果第二个帧头存在，则两个帧头之间的就是一帧完整的数据
                    if (headSecondIndex >= 0 && AvcUtils.isHead(mReceveBuf, headSecondIndex)) {
                        //                                    Log.e("TAG", "headSecondIndex:" + headSecondIndex);
                        //加入缓存
                        AddFrame(mReceveBuf, headSecondIndex - headFirstIndex, 0);

                        //截取headSecondIndex之后到frame的有效数据,并放到frame最前面
                        //if(headSecondIndex <= frameLen)
                        {
                            byte[] temp = Arrays.copyOfRange(mReceveBuf, headSecondIndex, frameLen);
                            System.arraycopy(temp, 0, mReceveBuf, 0, temp.length);

                            //修改frameLen的值
                            frameLen = temp.length;
                            //线程休眠
                            sleepThread(startTime, System.currentTimeMillis());
                            //重置开始时间
                            startTime = System.currentTimeMillis();
                        }

                        //继续寻找数据帧
                        headFirstIndex = AvcUtils.findHead(mReceveBuf, 0, frameLen);
                    } else {
                        //找不到第二个帧头
                        headFirstIndex = -1;
                    }
                }
            } else {
                //如果长度超过最大值，frameLen置0
                frameLen = 0;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AddFrame(byte[] receDataBuf,int len,long ts) throws InterruptedException {

        synchronized(mDecEncLock) {

            if(this.mRender == null)return;

            if(initDecoder(receDataBuf,len))//初始化解码器
            {
                PreviewBufferInfo info = mDecodeBuffers_clean.poll();    //remove the head of queue

                if(info != null){
                    try{
                        System.arraycopy(receDataBuf, 0, info.buffer, 0, len);

                        info.size = len;
                        info.timestamp = ts;
                        mDecodeBuffers_dirty.add(info);

                    }catch (Exception e){
                        e.printStackTrace();
                        mDecodeBuffers_clean.add(info);
                    }
                }
            }
        }
    }

    /**
     * 释放队列资源
     */
    private void releaseQueue() {

        if (mDecodeBuffers_clean != null){
            mDecodeBuffers_clean.clear();
            mDecodeBuffers_clean = null;
        }
        if (mDecodeBuffers_dirty != null){
            mDecodeBuffers_dirty.clear();
            mDecodeBuffers_dirty = null;
        }
    }

    private void releseDecoder() {
        if(mDecoder != null){
            mDecoder.release();
            mDecoder = null;
        }
    }

    private void releaseCodecThread() {
        if(codecHandler != null){
            codecHandler.getLooper().quit();
            codecHandler = null;
            mRender = null;
        }
    }

    public void stopCodecThread(){
        releaseCodecThread();
        releseDecoder();
        releaseQueue();
    }

    public void reSetDecoder(){
        releseDecoder();
    }

    //修眠
    long startTime = System.currentTimeMillis();
    private void sleepThread(long startTime, long endTime) {
        //根据读文件和解码耗时，计算需要休眠的时间
        long time = PRE_FRAME_TIME - (endTime - startTime);
        if (time > 0) {
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public interface OnRenderListener{
        public void writeYuvData(byte[] NV12OfByte, byte[] yuvOfByte, int width, int height);
    }
}
