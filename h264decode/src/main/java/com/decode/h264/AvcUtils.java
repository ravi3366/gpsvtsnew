package com.decode.h264;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;


public class AvcUtils {
	public static final int START_PREFIX_CODE = 0x00000001;
	public static final int START_PREFIX_LENGTH = 4;
	public static final int NAL_UNIT_HEADER_LENGTH = 1;
	public static final int NAL_TYPE_CODED_SLICE 			= 0x01;
	public static final int NAL_TYPE_CODED_SLICE_IDR 		= 0x05;
	public static final int NAL_TYPE_SEI 					= 0x06;
	public static final int NAL_TYPE_SPS 					= 0x07;
	public static final int NAL_TYPE_PPS 					= 0x08;
	public static final int NAL_TYPE_SUBSET_SPS 			= 0x0f;

	//YUV 格式：
	//YUV420分为两种：YUV420p和YUV420sp。
	//YUV420p:
	//I420:又叫YU12 , 存储顺序是先存Y，再存U，最后存V。YYYYUUUVVV
	//YV12:存储顺序是先存Y，再存V，最后存U。YYYVVVUUU
	//也是一种Plane模式，将Y、U、V分量分别打包，依次存储

	//YUV420sp:
	//NV12:IOS只有这一种模式。存储顺序是先存Y，再UV交替存储。YYYYUVUVUV
	//NV21:安卓的模式。存储顺序是先存Y，再存U，再VU交替存储。YYYYVUVUVU
	//是一种two-plane模式，即Y和UV分为两个Plane，但是UV（CbCr）为交错存储

	//I420: YYYYYYYY UU VV    =>YUV420P
	//YV12: YYYYYYYY VV UU    =>YUV420P
	//NV12: YYYYYYYY UVUV     =>YUV420SP
	//NV21: YYYYYYYY VUVU     =>YUV420SP

	
	public static boolean goToPrefix(final ByteBuffer buffer)
	{
		int presudo_prefix = 0xffffffff;
		while(buffer.hasRemaining())
		{
			presudo_prefix = (presudo_prefix << 8) | (buffer.get() & 0xff);
			if (presudo_prefix == START_PREFIX_CODE)
			{
				return true;
			}
		}
		return false;
	}
	
	public static int getNalType(final ByteBuffer buffer)
	{
		//return buffer.get() & 0x1f;
		byte d = buffer.get();
		return d & 0x1f;
	}
	
	public static int getGolombUE(final BitBufferLite bitb) {
		int leadingZeroBits = 0;
		while (!bitb.getBit()) {
			leadingZeroBits++;
		}
		final int suffix = bitb.getBits(leadingZeroBits);
		final int minimum = (1 << leadingZeroBits) - 1;
		return minimum + suffix;
	}
	
	//TODO: need support extra profile_idc and pic_order_cnt_type
	//usage: int[] width = new int[1];
	//sps should contains 00 00 00 01 67 ......
	public static boolean parseSPS(/*in*/byte[] sps, /*out*/int[] width, /*out*/int[] height)	//sps buffer doesn't include nal-type byte
	{
		try{
			ByteBuffer byteb = ByteBuffer.wrap(sps);
			if (false == goToPrefix(byteb) || NAL_TYPE_SPS != getNalType(byteb))
				return false;

			BitBufferLite bitb = new BitBufferLite(byteb);

			int profile_idc = bitb.getBits(8);				//profile idc  //比特流所遵守的配置和级别level_idc
			bitb.getBits(16);								//constraint_set0...,
			getGolombUE(bitb);

			//当profile_idc等于100、110、122或144时，constraint_set0_flag、constraint_set1_flag和constraint_set2_flag都应等于0。
			if (profile_idc == 100 || profile_idc == 110 || profile_idc == 122
					|| profile_idc == 244 || profile_idc == 44 || profile_idc == 83
					|| profile_idc == 86 || profile_idc == 118 || profile_idc == 128)
			{
				Log.e("AvcUtils", "SPS parsing do not support such profile idc, "+profile_idc);
				throw new UnsupportedOperationException("Profile idc NOT supported yet.");
			}

			int log2_max_frame_num_minus4 = getGolombUE(bitb);
			int pic_order_cnt_type = getGolombUE(bitb);
			if (pic_order_cnt_type == 0)
			{
				int log2_max_pic_order_cnt_lsb_minus4 = getGolombUE(bitb);
			}
			else if (pic_order_cnt_type == 1)
			{
				Log.e("AvcUtils", "SPS parsing do not support such pic_order_cnt_type, "+pic_order_cnt_type);
				throw new UnsupportedOperationException("pic_order_cnt_type NOT supported yet.");
			}
			else
			{
				//pic_order_cnt_type shall be "2", do nothing
			}

			int num_ref_frames = getGolombUE(bitb);//规定了可能在视频序列中任何图像帧间预测的解码过程中用到的短期参考帧和长期参考帧、互补参考场对以及不成对的参考场的最大数量。num_ref_frames 的取值范围应该在0到MaxDpbSize
			int gaps_in_frame_num_value_allowed_flag = bitb.getBits(1);	//1 bit

			//KEY POINT
			int pic_width_in_mbs_minus1 = getGolombUE(bitb);
			width[0] = (pic_width_in_mbs_minus1 + 1) * 16;
			int pic_height_in_map_units_minus1 = getGolombUE(bitb);
			height[0] = (pic_height_in_map_units_minus1 + 1) * 16;

		}catch (Exception e){
			return false;
		}
		return true;
	}

	/**
	 * 寻找指定 buffer 中 h264 头的开始位置
	 *
	 * @param data   数据
	 * @param offset 偏移量
	 * @param max    需要检测的最大值
	 * @return h264头的开始位置 ,-1表示未发现
	 */
	public static int findHead(byte[] data, int offset, int max) {
		int i;
		for (i = offset; i <= max; i++) {
			//发现帧头
			if (isHead(data, i))
				break;
		}
		//检测到最大值，未发现帧头
		if (i == max) {
			i = -1;
		}
		return i;
	}

	/**
	 * 判断是否是I帧/P帧头:
	 * 00 00 00 01 65    (I帧)
	 * 00 00 00 01 61 / 41   (P帧)
	 * 00 00 00 01 67    (SPS)
	 * 00 00 00 01 68    (PPS)
	 *
	 * @param data   解码数据
	 * @param offset 偏移量
	 * @return 是否是帧头
	 */
	public static boolean isHead(byte[] data, int offset) {
		boolean result = false;
		// 00 00 00 01 x
		if (data[offset] == 0x00 && data[offset + 1] == 0x00
				&& data[offset + 2] == 0x00 && data[3] == 0x01 && isVideoFrameHeadType(data[offset + 4])) {
			result = true;
		}
		// 00 00 01 x
		if (data[offset] == 0x00 && data[offset + 1] == 0x00
				&& data[offset + 2] == 0x01 && isVideoFrameHeadType(data[offset + 3])) {
			result = true;
		}
		return result;
	}

	/**
	 * I帧或者P帧
	 */
	public static boolean isVideoFrameHeadType(byte head) {
		return head == (byte) 0x65 || head == (byte) 0x61 || head == (byte) 0x41
				|| head == (byte) 0x67 || head == (byte) 0x68;
	}

	//I420（YU12） To NV21
	public static byte[] I420ToNV21(final byte[] input, final int width, final int height) {
		byte[] nv21 = new byte[input.length];

		int total = width * height;

		ByteBuffer bufferY = ByteBuffer.wrap(nv21, 0, total);
		ByteBuffer bufferV = ByteBuffer.wrap(nv21, total, total /4);
		ByteBuffer bufferU = ByteBuffer.wrap(nv21, total + total / 4, total /4);

		bufferY.put(input, 0, total);
		for (int i=0; i<total/4; i+=1) {
			bufferV.put(input[total+i]);
			bufferU.put(input[i+total+total/4]);
		}

		return nv21;
	}
	// YV12 To NV21
	public static void YV12toNV21(final byte[] input, final byte[] output, final int width, final int height) {
		//long startMs = System.currentTimeMillis();
		final int frameSize = width * height;
		final int qFrameSize = frameSize / 4;
		final int tempFrameSize = frameSize * 5 / 4;

		System.arraycopy(input, 0, output, 0, frameSize); // Y

		for (int i = 0; i < qFrameSize; i++) {
			output[frameSize + i * 2] = input[frameSize + i]; // Cb (U)
			output[frameSize + i * 2 + 1] = input[tempFrameSize + i]; // Cr (V)
		}
	}

	//YV12 To NV12
	public static void swapYV12toNV12(byte[] yv12bytes, byte[] nv12bytes, int width,int height)
	{
		int nLenY = width * height;
		int nLenU = nLenY / 4;

		System.arraycopy(yv12bytes, 0, nv12bytes, 0, width * height);
		for (int i = 0; i < nLenU; i++) {
			nv12bytes[nLenY + 2 * i + 1] = yv12bytes[nLenY + i];
			nv12bytes[nLenY + 2 * i] = yv12bytes[nLenY + nLenU + i];
		}
	}

	// NV21 To NV12
	public static void swapNV21ToNV12(byte[] nv21,byte[] nv12,int width,int height){
		if(nv21 == null || nv12 == null)return;
		int framesize = width*height;
		int i = 0,j = 0;
		System.arraycopy(nv21, 0, nv12, 0, framesize);
		for (j = 0; j < framesize/2; j+=2)
		{
			nv12[framesize + j + 1] = nv21[j + framesize];
		}

		for (j = 0; j < framesize/2; j += 2)
		{
			nv12[framesize + j] = nv21[j + framesize + 1];
		}
	}

	public static void swapYV12toI420(byte[] yv12bytes, byte[] i420bytes, int width, int height) {
		System.arraycopy(yv12bytes, 0, i420bytes, 0, width * height);//y

		System.arraycopy(yv12bytes, width * height + width * height / 4, i420bytes, width * height, width * height / 4);//u
		System.arraycopy(yv12bytes, width * height, i420bytes, width * height + width * height / 4, width * height / 4);//v
	}

	//NV12是YUV420格式，排列是(Y), (UV)，是2 plane
	//NV21是YUV420格式，排列是(Y), (VU)，是2 plane
	public static void swapNV12toYV12(byte[] nv12bytes, int len,byte[] i420bytes, int width,int height) {
		int nLenY = width * height;
		int nLenU = nLenY / 4;

		System.arraycopy(nv12bytes, 0, i420bytes, 0, len);//width * height);

		for (int i = 0; i < nLenU; i++) {
			i420bytes[nLenY + i]         = nv12bytes[nLenY + 2 * i + 1]; // u - >v
			i420bytes[nLenY + nLenU + i] = nv12bytes[nLenY + 2 * i];     // v - >u
		}
	}

	//yuv420SP(NV12) - >RGB565
	public static int NV12ToRGB565(byte[] yuv420, int[] rgb565, int width,int height) {
			final int frameSize = width * height;

			for (int j = 0, yp = 0; j < height; j++) {

				int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;

				for (int i = 0; i < width; i++, yp++) {
					int y = (0xff & ((int) yuv420[yp])) - 16;
					if (y < 0) y = 0;
					if ((i & 1) == 0) {
						v = (0xff & yuv420[uvp++]) - 128;
						u = (0xff & yuv420[uvp++]) - 128;
					}

					int y1192 = 1192 * y;
					int r = (y1192 + 1634 * u);
					int g = (y1192 - 833 * u - 400 * v);
					int b = (y1192 + 2066 * v);

					if (r < 0) r = 0; else if (r > 262143) r = 262143;
					if (g < 0) g = 0; else if (g > 262143) g = 262143;
					if (b < 0) b = 0; else if (b > 262143) b = 262143;

					int n = (0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff));
					rgb565[yp] = n;
				}
			}

		return 0;
	}
	//yuv420SP(NV21) - >RGB565
	public static int NV21ToRGB565(byte[] yuv420, int[] rgb565, int width,int height) {
		final int frameSize = width * height;

		for (int j = 0, yp = 0; j < height; j++) {

			int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;

			for (int i = 0; i < width; i++, yp++) {
				int y = (0xff & ((int) yuv420[yp])) - 16;
				if (y < 0) y = 0;
				if ((i & 1) == 0) {
					v = (0xff & yuv420[uvp++]) - 128;
					u = (0xff & yuv420[uvp++]) - 128;
				}

				int y1192 = 1192 * y;
				int r = (y1192 + 1634 * v);
				int g = (y1192 - 833 * v - 400 * u);
				int b = (y1192 + 2066 * u);

				if (r < 0) r = 0; else if (r > 262143) r = 262143;
				if (g < 0) g = 0; else if (g > 262143) g = 262143;
				if (b < 0) b = 0; else if (b > 262143) b = 262143;

				int n = (0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff));
				rgb565[yp] = n;
			}
		}

		return 0;
	}

	public static int YUV420SPToRGB565(byte[] yuv420sp, int[] rgb, int width,int height,int type){
		if(type == 0){//NV12,YYYYYYYY UVUV
			NV12ToRGB565(yuv420sp,rgb,width,height);
		}else{ //NV21,YYYYYYYY VUVU
			NV21ToRGB565(yuv420sp,rgb,width,height);
		}
		return 0;
	}

	public static Bitmap NV12ToBitmap(byte[] yuv420sp, int width,int height){
		Bitmap bitmap = null;

		try{
			int[] rgb = new int[width * height];

			NV12ToRGB565(yuv420sp,rgb,width,height);

			bitmap = Bitmap.createBitmap(rgb, width, height, Bitmap.Config.RGB_565);

		}catch (Exception e){
			e.printStackTrace();
		}

		return bitmap;
	}

	public static Bitmap NV21ToBitmap(byte[] data, int previewWidth, int previewHeight) {
		YuvImage yuvimage = new YuvImage(
				data,
				ImageFormat.NV21,
				previewWidth,
				previewHeight,
				null);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(new Rect(0, 0, previewWidth, previewHeight), 100, baos);// 80--JPG图片的质量[0-100],100最高
		byte[] rawImage = baos.toByteArray();
		//将rawImage转换成bitmap
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		Bitmap bitmap = BitmapFactory.decodeByteArray(rawImage, 0, rawImage.length, options);
		return bitmap;
	}

	public static int byteArrayToInt(byte[] b) {
		return   b[0] & 0xFF |
				(b[1] & 0xFF) << 8 |
				(b[2] & 0xFF) << 16 |
				(b[3] & 0xFF) << 24;
	}

	public static byte[] intToByteArray(int a) {
		return new byte[] {
				(byte) ((a >> 24) & 0xFF),
				(byte) ((a >> 16) & 0xFF),
				(byte) ((a >> 8) & 0xFF),
				(byte) (a & 0xFF)
		};
	}

	//I420是yuv420格式，是3个plane，排列方式为(Y)(U)(V)
	public static int[] I420ToRGB(byte[] src, int width, int height){
		int R = 0,G = 1,B = 2;

		int numOfPixel = width * height;
		int positionOfV = numOfPixel;
		int positionOfU = numOfPixel/4 + numOfPixel;
		int[] rgb = new int[numOfPixel*3];

		for(int i=0; i<height; i++){
			int startY = i*width;
			int step = (i/2)*(width/2);
			int startU = positionOfV + step;
			int startV = positionOfU + step;

			for(int j = 0; j < width; j++){
				int Y = startY + j;
				int U = startU + j/2;
				int V = startV + j/2;
				int index = Y*3;

				RGB tmp = yuvTorgb(src[Y],src[U],src[V]);
				rgb[index+R] = tmp.r;
				rgb[index+G] = tmp.g;
				rgb[index+B] = tmp.b;
			}
		}
		return rgb;
	}

		//NV12是YUV420格式，排列是(Y), (UV)，是2 plane
	public static int[]NV12ToRGB(byte[] src, int width, int height){
		int R = 0,G = 1,B = 2;

		int numOfPixel = width*height;
		int positionOfU = numOfPixel;
		int[] rgb = new int[numOfPixel*3];
		for(int i=0; i < height; i++){
			int startY = i*width;
			int step = i/2*width;
			int startU = positionOfU + step;

			for(int j = 0; j < width; j++){
				int Y = startY + j;
				int U = startU + j/2;
				int V = U + 1;
				int index = Y*3;

				RGB tmp = yuvTorgb(src[Y],src[U],src[V]);
				rgb[index+R] = tmp.r;
				rgb[index+G] = tmp.g;
				rgb[index+B] = tmp.b;
			}
		}
		return rgb;
	}

	//NV21是YUV420格式，排列是(Y), (VU)，是2 plane
	public static int[]NV21ToRGB(byte[] src, int width, int height){
		int R = 0,G = 1,B = 2;

		int numOfPixel = width*height;
		int positionOfV = numOfPixel;

		int[] rgb = new int[numOfPixel*3];

		for(int i=0; i < height; i++){
			int startY = i*width;
			int step = i/2*width;
			int startV = positionOfV + step;

			for(int j = 0; j < width; j++){
				int Y = startY + j;
				int V = startV + j/2;
				int U = V + 1;
				int index = Y*3;

				RGB tmp = yuvTorgb(src[Y],src[U],src[V]);
				rgb[index+R] = tmp.r;
				rgb[index+G] = tmp.g;
				rgb[index+B] = tmp.b;
			}
		}
		return rgb;
	}

	//YV12是yuv420格式，是3个plane，排列方式为(Y)(V)(U)
	public static int[]YV12ToRGB(byte[] src,int width,int height){
		int R = 0,G = 1,B = 2;

		int numOfPixel = width * height;
		int positionOfV = numOfPixel;
		int positionOfU = numOfPixel/4 + numOfPixel;
		int[] rgb = new int[numOfPixel*3];

		for(int i=0; i<height; i++){
			int startY = i*width;
			int step =(i/2)*(width/2);
			int startV = positionOfV + step;
			int startU = positionOfU + step;
			for(int j = 0; j < width; j++){
				int Y = startY + j;
				int V = startV + j/2;
				int U = startU + j/2;
				int index = Y*3;

				//rgb[index+R]=(int)((src[Y]&0xff)+1.4075*((src[V]&0xff)-128));
				//rgb[index+G]=(int)((src[Y]&0xff)-0.3455*((src[U]&0xff)-128)-0.7169*((src[V]&0xff)-128));
				//rgb[index+B]=(int)((src[Y]&0xff)+1.779*((src[U]&0xff)-128));
				RGB tmp = yuvTorgb(src[Y], src[U], src[V]);
				rgb[index+R] = tmp.r;
				rgb[index+G] = tmp.g;
				rgb[index+B] = tmp.b;
			}
	}
		return rgb;
	}

	private static RGB yuvTorgb(byte Y, byte U, byte V){
		RGB rgb = new RGB();
		rgb.r = (int)((Y&0xff) + 1.4075 * ((V&0xff)-128));
		rgb.g = (int)((Y&0xff) - 0.3455 * ((U&0xff)-128) - 0.7169*((V&0xff)-128));
		rgb.b = (int)((Y&0xff) + 1.779 * ((U&0xff)-128));

		rgb.r =(rgb.r<0? 0: rgb.r>255? 255 : rgb.r);
		rgb.g =(rgb.g<0? 0: rgb.g>255? 255 : rgb.g);
		rgb.b =(rgb.b<0? 0: rgb.b>255? 255 : rgb.b);

	return rgb;
	}

	private static class RGB{
		public int r, g, b;
	}
}