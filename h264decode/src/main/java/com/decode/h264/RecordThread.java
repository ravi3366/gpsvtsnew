package com.decode.h264;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.bluecam.libs.Mp4v2Helper;
import com.decode.utils.LogUtil;
import com.decode.utils.CustomBufferData;

import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class RecordThread extends Thread{

    private FileOutputStream output;
    private String filePath;

    private Queue<PreviewBufferInfo> mRecordBuffers_clean;//开辟的固定大小5*DECODE_UNI_SIZE 的QUENU
    private Queue<PreviewBufferInfo> mRecordBuffers_dirty;//编码后的数据

    private int PREVIEW_POOL_CAPACITY = 5;
    private final int DECODE_UNI_SIZE = 1024 * 300;

    private byte[] mReceveBuf    = new byte[DECODE_UNI_SIZE];
    private int nReceveSize = 0;
    private int nAVType = 0;//视频，还是音频

    private final int MSG_RECORD =  0;
    private Object mRecordLock;
    Handler recordHandler;

    public int RecordType = 0;//0本地，1远程

    public RecordThread(String filePath,int nRecordType){
        this.filePath = filePath;
        output = null;
        mRecordLock = new Object();
        initQueues();
        this.RecordType = nRecordType;
    }

    private class PreviewBufferInfo {
        public byte[] buffer;
        public int size;
        public long timestamp;
        public int type;
    }

    /**
     * 初始化各种队列
     */
    private void initQueues() {

        if (mRecordBuffers_clean == null)
            mRecordBuffers_clean = new LinkedList<>();
        if (mRecordBuffers_dirty == null)
            mRecordBuffers_dirty = new LinkedList<>();

        for (int i = 0; i < PREVIEW_POOL_CAPACITY; i++) {
            PreviewBufferInfo info = new PreviewBufferInfo();
            info.buffer = new byte[DECODE_UNI_SIZE];
            info.size = 0;
            info.timestamp = 0;
            mRecordBuffers_clean.add(info);
        }
    }

    @Override
    public void run() {

        Looper.prepare();

        recordHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MSG_RECORD:

                        synchronized (mRecordLock) {

                            if (mRecordBuffers_dirty != null && mRecordBuffers_clean != null) {
                                Iterator<PreviewBufferInfo> ite = mRecordBuffers_dirty.iterator();//获取待解码数据

                                while (ite.hasNext()) {
                                    PreviewBufferInfo info = ite.next();

                                    byte[] data = info.buffer;
                                    nReceveSize = info.size;
                                    nAVType = info.type;

                                    System.arraycopy(data, 0, mReceveBuf, 0, nReceveSize);

                                    {
                                        ite.remove();
                                        mRecordBuffers_clean.add(info);//PreviewBufferInfo 重新回到 mDecodeBuffers_clean中
                                    }

                                    if(nReceveSize >0 ){
                                        try{

                                            if(RecordType == 0){//本地
                                                if(findSpsPssIDR(mReceveBuf,nReceveSize) == 0){

                                                    if(nAVType == 6){//音频
                                                        //int nRet = Adpcm.getInstance().adpcm_readdata(mReceveBuf,1024);
                                                        CustomBufferData audioData = AACEncoder.getInstance().outAudioBuffer();
                                                        if(audioData != null){
                                                            //Mp4v2Helper.mp4AEncode(audioData.data,audioData.head.length);
                                                        }
                                                    }else{//视频
                                                        try{
                                                            Mp4v2Helper.mp4VEncode(mReceveBuf,nReceveSize);
                                                        }catch (Exception e){
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            }else{
                                                output.write(mReceveBuf,0,nReceveSize);
                                            }

                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                        }

                        if(recordHandler != null){
                            recordHandler.sendEmptyMessageDelayed(MSG_RECORD, 10);//延迟30，继续下次解码工作
                        }

                        break;

                }
            }
        };
        Looper.loop();
    }

    //将视频数据添加到缓存（TF卡录像）
    public void AddRecordFrame(byte[]receDataBuf,int len) throws InterruptedException {

        synchronized(mRecordLock) {

            if(output == null){
                try {
                    output = new FileOutputStream(filePath, true);

                    if(recordHandler != null)
                        recordHandler.sendEmptyMessageDelayed(MSG_RECORD, 10);//延迟30，继续下次解码工作

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            PreviewBufferInfo info = mRecordBuffers_clean.poll();    //remove the head of queue
            if(info != null){
                System.arraycopy(receDataBuf, 0, info.buffer, 0, len);
                info.size = len;
                info.timestamp = 0;
                mRecordBuffers_dirty.add(info);
            }
        }
    }

    //本地录像
    public void LocalRecordAVideo(byte[]receDataBuf,int len,int type){
        synchronized(mRecordLock) {
            PreviewBufferInfo info = mRecordBuffers_clean.poll();    //remove the head of queue
            if(info != null){
                System.arraycopy(receDataBuf, 0, info.buffer, 0, len);
                info.size = len;
                info.timestamp = 0;
                info.type = type;
                mRecordBuffers_dirty.add(info);
            }
        }
    }

    private boolean checkFrameHead(byte[]receDataBuf,int frameLen){
        int headFirstIndex = AvcUtils.findHead(mReceveBuf, 0, frameLen);
        return headFirstIndex >= 0 ;
    }

    public void initRecordFirstFrame(byte[]receDataBuf,int len){

        if(Mp4v2Helper.checkMp4()>0){
            if(Mp4v2Helper.findMp4VideoTrack() < 0) {
                if (findSpsPssIDR(receDataBuf, len) >= 0) {
                    if(recordHandler != null){
                        recordHandler.sendEmptyMessageDelayed(MSG_RECORD, 1);
                    }else{
                        LogUtil.printLog("------------initRecordFirstFrame-------recordHandler = null");
                    }
                }
            }
        }
    }

    public int findSpsPssIDR(byte[]data,int len){
        byte[] sps_nal = null;
        int sps_len = 0;
        byte[] pps_nal = null;
        int pps_len = 0;

        byte[] idr_nal = null;
        int idr_len = 0;

        if(Mp4v2Helper.checkMp4()<= 0){
            return -1;
        }

        ByteBuffer byteb = ByteBuffer.wrap(data, 0, len);

        //SPS
        if (true == AvcUtils.goToPrefix(byteb)) {
            int sps_position = 0;
            int pps_position = 0;
            int idr_position = 0;
            int nal_type = AvcUtils.getNalType(byteb);

            if (AvcUtils.NAL_TYPE_SPS == nal_type) {
                int pos = byteb.position();
                sps_position = pos - AvcUtils.START_PREFIX_LENGTH - AvcUtils.NAL_UNIT_HEADER_LENGTH;

                //PPS
                if (true == AvcUtils.goToPrefix(byteb)) {
                    nal_type = AvcUtils.getNalType(byteb);
                    if (AvcUtils.NAL_TYPE_PPS == nal_type) {
                        int pos1= byteb.position();

                        pps_position = byteb.position() - AvcUtils.START_PREFIX_LENGTH - AvcUtils.NAL_UNIT_HEADER_LENGTH;
                        sps_len = pps_position - sps_position;

                        sps_nal = new byte[sps_len];
                        int cur_pos = byteb.position();
                        byteb.position(sps_position);
                        byteb.get(sps_nal, 0, sps_len);
                        byteb.position(cur_pos);

                        Mp4v2Helper.mp4VEncode(sps_nal,sps_len);

                        //slice
                        if (true == AvcUtils.goToPrefix(byteb)) {
                            nal_type = AvcUtils.getNalType(byteb);

                            idr_position = byteb.position() - AvcUtils.START_PREFIX_LENGTH - AvcUtils.NAL_UNIT_HEADER_LENGTH;
                            pps_len = idr_position - pps_position;

                        } else {
                            pps_len = byteb.position() - pps_position;
                        }

                        if (pps_len > 0) {
                            pps_nal = new byte[pps_len];
                            cur_pos = byteb.position();
                            byteb.position(pps_position);
                            byteb.get(pps_nal, 0, pps_len);
                            byteb.position(cur_pos);

                            Mp4v2Helper.mp4VEncode(pps_nal,pps_len);

                            if (AvcUtils.NAL_TYPE_CODED_SLICE_IDR == nal_type) {
                                idr_len = len - idr_position;

                                if(idr_len>0){

                                    idr_nal = new byte[idr_len];
                                    cur_pos = byteb.position();
                                    byteb.position(idr_position);
                                    byteb.get(idr_nal, 0, idr_len);
                                    byteb.position(cur_pos);

                                    Mp4v2Helper.mp4VEncode(idr_nal,idr_len);

                                    return 1;
                                }
                            }
                        }
                    }
                } else
                    {
                        //Log.d(log_tag, "OutputAvcBuffer, AVC NAL type: "+nal_type);
                        throw new UnsupportedOperationException("SPS is not followed by PPS, nal type :" + nal_type);
                    }
            }
        }

        return 0;
    }

    /**
     * 释放队列资源
     */
    private void releaseQueue() {

        if (mRecordBuffers_clean != null){
            mRecordBuffers_clean.clear();
            mRecordBuffers_clean = null;
        }
        if (mRecordBuffers_dirty != null){
            mRecordBuffers_dirty.clear();
            mRecordBuffers_dirty = null;
        }
    }

    private void releaseRecodeThread() {
        if(recordHandler!=null){
            recordHandler.getLooper().quit();
            recordHandler = null;
        }
    }
    private void releaseFileStream(){
        if(output != null){
            try{
                output.close();
                output = null;
            }catch (Exception e){
                e.printStackTrace();
                output = null;
            }
        }
    }

    public void stopRecord(){
        releaseRecodeThread();
        releaseFileStream();
        releaseQueue();
    }
}