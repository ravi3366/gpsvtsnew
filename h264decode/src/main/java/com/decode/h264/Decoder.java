package com.decode.h264;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Build;
import android.util.Log;
import android.view.Surface;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by andi on 2018/10/15 />
 */

/*
● "video/x-vnd.on2.vp8” - VP8 video (i.e. video in .webm)
● “video/x-vnd.on2.vp9” - VP9 video (i.e. video in .webm)
● “video/avc” - H.264/AVC video
● “video/mp4v-es” - MPEG4 video
● “video/3gpp” - H.263 video
● “audio/3gpp” - AMR narrowband audio
● “audio/amr-wb” - AMR wideband audio
● “audio/mpeg” - MPEG1/2 audio layer III
● “audio/mp4a-latm” - AAC audio (note, this is raw AAC packets, not packaged in LATM!)
● “audio/vorbis” - vorbis audio
● “audio/g711-alaw” - G.711 alaw audio
● “audio/g711-mlaw” - G.711 ulaw audio
*/

public class Decoder {

    public static final int TRY_AGAIN_LATER = -1;
    public static final int BUFFER_OK = 0;
    public static final int BUFFER_TOO_SMALL = 1;
    public static final int OUTPUT_UPDATE = 2;

    private final String MIME_TYPE = "video/avc";
    private MediaCodec mMCodec = null;
    private MediaFormat mMFomat;

    private long BUFFER_TIMEOUT = -1;

    private boolean bShowRender = false;//surface 是否显示视频；
    private boolean isAlive = false;

    /**
     * 初始化编码器
     * @throws IOException 创建编码器失败会抛出异常
     */
    public void init() throws IOException {
        mMCodec = MediaCodec.createDecoderByType(MIME_TYPE);
    }

    public void init(MediaCodec mediaCodec){
        mMCodec = mediaCodec;
    }

    /**
     * 配置解码器
     * @param sps 用于配置的sps参数
     * @param pps 用于配置的pps参数
     * @param surface 用于解码显示的Surface
     */
    public void configure(byte[] sps, byte[] pps, Surface surface){
        int[] width = new int[1];
        int[] height = new int[1];
        AvcUtils.parseSPS(sps, width, height);//从sps中解析出视频宽高
        mMFomat = MediaFormat.createVideoFormat(MIME_TYPE, width[0], height[0]);
        mMFomat.setByteBuffer("csd-0", ByteBuffer.wrap(sps));
        mMFomat.setByteBuffer("csd-1", ByteBuffer.wrap(pps));

        mMFomat.setInteger(MediaFormat.KEY_FRAME_RATE, 15);

        try{
            mMCodec.configure(mMFomat, null, null, MediaCodec.CRYPTO_MODE_UNENCRYPTED);
        }catch (Exception e){
            e.printStackTrace();
        }

        Log.v("configure","----------------width:"+width[0]+"height:"+height[0]);
    }

    public void configure(byte[] sps,int width,int height,Surface surface){

        boolean isVGA = true;
        //使用sps数据格式判断是否是VGA
        byte[] video_sps = {0, 0, 0, 1, 103, 100, 64, 41, -84, 44, -88, 10, 2, -1, -107};
        for (int i = 0; i < sps.length; i++) {
            if (video_sps[i] != sps[i]) {
                //判断是否是VGA视频传输标准
                isVGA = false;
                break;
            }
        }
        //获取h264中的pps及sps数据
        if (isVGA) {
            //初始化MediaFormat
            mMFomat = MediaFormat.createVideoFormat(MIME_TYPE, width, height);
            byte[] header_pps = {0, 0, 0, 1, 104, -18, 56, -128};
            mMFomat.setByteBuffer("csd-0", ByteBuffer.wrap(video_sps));
            mMFomat.setByteBuffer("csd-1", ByteBuffer.wrap(header_pps));
            //mMFomat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, width * height);

        } else {
            mMFomat = MediaFormat.createVideoFormat(MIME_TYPE,1920, 1080);

            byte[] header_sps = sps;//{0, 0, 0, 1, 103, 100, 0, 22, -84, 59, 80, 80, 30, -48, -128,0,0,3,0,-128,0,0,10,66,0,0};

            byte[] header_pps =  {0, 0, 0, 1, 104, -18, 60,-31, 0, 66,66, 0, -124, -124, 4,76,82,27,-109,-59,124,
                    -97,-109,-7,63,39,-55,-26,-28,-55,36,44,34,66,-112,-100,-98,79,-81,-55,-3,126,79,-81,39,38,-92,-64};

            mMFomat.setByteBuffer("csd-0", ByteBuffer.wrap(header_sps));
            mMFomat.setByteBuffer("csd-1", ByteBuffer.wrap(header_pps));
        }

        try{
            mMCodec.configure(mMFomat, null, null, MediaCodec.CRYPTO_MODE_UNENCRYPTED);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 开启解码器，
     */
    public void start(){
        mMCodec.start();
        isAlive = true;
    }

    public boolean  isAlive(){
        return isAlive;
    }

    /**
     * 输入数据
     * @param data 输入的数据
     * @param len 数据有效长度
     * @param timestamp 时间戳
     * @return 成功则返回{@link #BUFFER_OK} 否则返回{@link #TRY_AGAIN_LATER}
     */
    public int input(byte[] data,int len,long timestamp){
        try{
            ByteBuffer[] mInputBuffers = mMCodec.getInputBuffers();

            int inputBufferIndex = mMCodec.dequeueInputBuffer(BUFFER_TIMEOUT);
            if(inputBufferIndex >= 0){
                ByteBuffer inputBuffer = mInputBuffers[inputBufferIndex];
                inputBuffer.clear();
                inputBuffer.put(data, 0, len);
                mMCodec.queueInputBuffer(inputBufferIndex, 0, len, timestamp, 0);
            }else {
                return TRY_AGAIN_LATER;
            }

        }catch (Exception e){
            return TRY_AGAIN_LATER;
        }
        return BUFFER_OK;
    }

    public int output(byte[] data,int[] len,long[] ts,int[]w,int[]h,int[]fmt){
        try{
            // 获取输出buffer index
            MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
            int outputBufferIndex = mMCodec.dequeueOutputBuffer(bufferInfo, 200);

            if(outputBufferIndex >= 0){

                ByteBuffer[] mOutputBuffers = mMCodec.getOutputBuffers();

                ByteBuffer outputBuffer;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    outputBuffer = mMCodec.getOutputBuffer(outputBufferIndex);
                } else {
                    outputBuffer = mOutputBuffers[outputBufferIndex];
                }

                if (outputBuffer != null) {
                    outputBuffer.position(bufferInfo.offset);
                    outputBuffer.limit(bufferInfo.offset + bufferInfo.size);

                    len[0] = bufferInfo.size;
                    ts[0] = bufferInfo.presentationTimeUs;

                    if (data != null) {
                        if(data.length > len[0]){
                            outputBuffer.get(data, 0, bufferInfo.size);
                        }
                    }

                    MediaFormat mediaFormat = mMCodec.getOutputFormat();

                    fmt[0] = mediaFormat.getInteger(MediaFormat.KEY_COLOR_FORMAT);

                    w[0] = mediaFormat.getInteger(MediaFormat.KEY_WIDTH);
                    h[0] = mediaFormat.getInteger(MediaFormat.KEY_HEIGHT);

                    //FormatYuv(fmt[0],srcYuvBuffer,desData,w[0],h[0]);

                    mMCodec.releaseOutputBuffer(outputBufferIndex, bShowRender);
                }
            }else{
                return TRY_AGAIN_LATER;
            }

        }catch(Exception e){
            return TRY_AGAIN_LATER;
        }

        return BUFFER_OK;
    }

    public void flish(){
        mMCodec.flush();
    }

    public void release() {
        mMCodec.stop();
        mMCodec.release();
        mMCodec = null;

        isAlive = false;
    }
}
