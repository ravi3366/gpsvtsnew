package com.decode.h264;

import com.bluecam.libs.Adpcm;
import com.bluecam.libs.Mp4v2Helper;

import com.decode.utils.AudioPlayer;
import com.decode.utils.CustomBuffer;
import com.decode.utils.CustomBufferData;

import java.util.Arrays;

public class CameraHelper implements CodecThread.OnRenderListener {

    public static final String TAG = "CameraHelper";

    public static int Video_A_RealData = 0x01;
    public static int Video_A_PlayBackData = 0x02;

    public int Video_Audio_Data_Type = 0;//视频类型

    private byte[] FirstFrame = new byte[1024*100];
    private int firstSize = 0;

    private int nBitstream = 0;

    private CodecThread decodeThread            = null;//实时播放解码线程
    private CodecThread decodePlayBackThread    = null;//回放解码线程
    private AudioPlayer audioPlayer             = null;//音频播放线程
    private AdpcmToPcm adpcmToPcm               = null;//音频转码
    private RecordThread recordThread           = null;//录像线程

    private CustomBuffer audioPcmBuffer; //音频裸流

    private OnDataListener onDataListener = null;

    private boolean isRecordLoading;//是否录像下载
    private boolean isRecordAudio;//是否开启声音

    public void setRecordLoading(boolean openRecord){
        isRecordLoading = openRecord;
    }
    public void setRecordAudio(boolean openAudio){
        isRecordAudio = openAudio;
    }

    @Override
    public void writeYuvData(byte[] NV12OfByte, byte[] yuvOfByte, int width, int height) {
        if(onDataListener!=null){
            onDataListener.writeYuvData(NV12OfByte,yuvOfByte,width,height);
        }
    }

    public interface OnDataListener{
        void onRecordProgressEvent(int uid, int size);
        void onRecordLoading(int uid, int size);
        void onRecordFinish(int uid, int srcSize);
        void writeYuvData(byte[] NV12OfByte, byte[] yuvOfByte, int width, int height);
    }

    public CameraHelper(OnDataListener updataUi){
        this.audioPcmBuffer = new CustomBuffer();
        this.onDataListener = updataUi;
    }

    //视频播放线程
    private void initPlayer(){

        if(decodeThread == null){
            decodeThread = new CodecThread();
            decodeThread.start();
        }

        if(decodeThread != null){
            decodeThread.SetStreamCallBack(this);
        }
    }

    private void unInitPlayer(){

        if(decodeThread != null){
            decodeThread.stopCodecThread();
            decodeThread = null;
        }
    }

    //视频回放播放线程
    private void initRePlayer(){
        if(decodePlayBackThread == null){
            //firstSize = 0;
            decodePlayBackThread = new CodecThread();
            decodePlayBackThread.start();
        }

        if(decodePlayBackThread != null){
            decodePlayBackThread.SetStreamCallBack(this);
        }
    }

    private void unInitRePlayer(){
        if(decodePlayBackThread != null){
            decodePlayBackThread.stopCodecThread();
            decodePlayBackThread = null;
        }
    }

    //Audio Player
    //音频播放线程
    public void initAudioPlayer(){

        if(audioPlayer == null)
        {
            audioPlayer = new AudioPlayer(this);
        }
        audioPlayer.AudioPlayStart();
    }

    public void unInitAudioPlayer(){

        if(audioPlayer != null)
        {
            audioPlayer.AudioPlayStop();
            audioPlayer = null;
        }
    }

    //AAC
    private void initAdpcmToPCM(){

        if(adpcmToPcm == null)
        {
            audioPcmBuffer.ClearAll();
            adpcmToPcm = new AdpcmToPcm(this);

            adpcmToPcm.AudioAACStart();
        }
    }

    private void unInitAdpcmToPCM(){
        audioPcmBuffer.ClearAll();
        if(adpcmToPcm != null)
        {
            adpcmToPcm.AudioAACStop();
            adpcmToPcm = null;
        }
    }

    //record
    public void initRecord(String savepath,int nRecordType){

        int i = Mp4v2Helper.initMp4Encoder(savepath, 1920, 1080);

        if(recordThread == null){
            recordThread = new RecordThread(savepath,nRecordType);//0-本地录像；1-远程录像
            recordThread.start();
        }
    }

    public void unInitRecord(){
        if(recordThread != null){
            recordThread.stopRecord();
            recordThread = null;
        }
        Mp4v2Helper.closeMp4Encoder();
    }

    public void destoryCameraDecoder(){
        unInitPlayer();
        unInitRePlayer();
        unInitRecord();
        unInitAdpcmToPCM();
    }

    public CustomBufferData getAudioBuffer(){
        CustomBufferData data = audioPcmBuffer.RemoveData();
        return data;
    }

    //aac音频数据
    public void setAudioBuffer(CustomBufferData data){
        AACEncoder.getInstance().putPCMData(data);
    }

    //实时视频
    public void Audio_Video_Player(int uid, byte[] data, int srcSize){
        Video_Audio_Data_Type = Video_A_RealData;

        byte[] tempHead = Arrays.copyOfRange(data, 0, 4);
        int startcode =  AvcUtils.byteArrayToInt(tempHead);
        if(startcode == 0xa815aa55){
            initPlayer();

            byte[] tempLen = Arrays.copyOfRange(data, 16, 20);
            int decSize =  AvcUtils.byteArrayToInt(tempLen);
            byte[] Video_Audio_Buffer = Arrays.copyOfRange(data, 32, srcSize);

            if(srcSize <= FirstFrame.length && srcSize >= 1024*40 ){//录制关键帧
                System.arraycopy(Video_Audio_Buffer, 0, FirstFrame, 0, decSize);
                firstSize = decSize;

                //Log.v("FirstFrame","--------FirstFrame--------");
            }else{
                if(decodeThread != null && !decodeThread.checkDeCodeAlive()){

                    if(firstSize >0) {
                        video_Decoder(uid, FirstFrame, firstSize);

                        //Log.v("FirstFrame", "--------默认解码器数据--------");
                    }
                }
            }

            if(data[4] == 1 || data[4] == 0){
                int nBitstream = data[21];//用来切换判断码流，3 主码流，0 子码流

                if(checkBitstream(nBitstream)){
                    unInitPlayer();
                    initPlayer();

                    if(decodeThread != null){
                        decodeThread.SetStreamCallBack(this);
                    }

                    if(firstSize >0) {
                        video_Decoder(uid, FirstFrame, firstSize);
                        //Log.v("FirstFrame", "--------默认解码器数据--------");
                    }
                }

                video_Decoder(uid,Video_Audio_Buffer,decSize);
            }else{
                audio_Decoder(uid,Video_Audio_Buffer,decSize);
            }

            //本地录像
            Record_local_Video(uid,Video_Audio_Buffer,decSize,srcSize);
        }
    }

    //视频回放
    public void Audio_Video_rePlayer(int uid, byte[] data, int srcSize){
        Video_Audio_Data_Type = Video_A_PlayBackData;

        byte[] tempHead = Arrays.copyOfRange(data, 0, 4);
        int startcode =  AvcUtils.byteArrayToInt(tempHead);

        if(startcode == 0xa815aa55){

            initRePlayer();

            byte[] tempLen = Arrays.copyOfRange(data, 16, 20);
            int decSize =  AvcUtils.byteArrayToInt(tempLen);
            byte[] Video_Audio_Buffer = Arrays.copyOfRange(data, 32, srcSize);

            if(srcSize <= FirstFrame.length && srcSize >= 1024*30 && firstSize == 0){//录制关键帧
                System.arraycopy(Video_Audio_Buffer, 0, FirstFrame, 0, decSize);
                firstSize = decSize;
            }

            if(!isRecordLoading){
                if(data[4] == 1 || data[4] == 0)
                {
                    video_Decoder(uid,Video_Audio_Buffer,decSize);
                }else{
                    audio_Decoder(uid,Video_Audio_Buffer,decSize);
                }

            }else{
                DownLoad_Record_TF_Video(uid,data[4],Video_Audio_Buffer,decSize,srcSize);
            }

            if(onDataListener != null && decodePlayBackThread != null){
                onDataListener.onRecordFinish(uid,srcSize);
            }
        }
    }

    //视频解码
    private void video_Decoder(int uid,byte[] data, int size){
        if(Video_Audio_Data_Type == Video_A_RealData){

            if(decodeThread != null){// && decodeThread.isAlive()){
                decodeThread.writeDate(data,size);
            }

        }else{

            if(decodePlayBackThread != null ){
                decodePlayBackThread.writeDate(data,size);
                onDataListener.onRecordProgressEvent(uid,size+32);
            }
        }
    }
    //音频解码
    private void audio_Decoder(int uid,byte[] data, int size){
        if(Video_Audio_Data_Type == Video_A_RealData){
            int nRet = Adpcm.getInstance().adpcm_decoder(data,size);
        }else{
            if(isRecordAudio){
                int nRet = Adpcm.getInstance().adpcm_decoder(data,size);
            }
        }
    }

    //录像(本地录像)
    private void Record_local_Video(int uid,byte[] data, int decSize,int srcSize){
        try{
            if(recordThread != null && recordThread.isAlive()) {//本地录像

                recordThread.initRecordFirstFrame(FirstFrame, firstSize);
                recordThread.LocalRecordAVideo(data, decSize, 0);// 0,1-视频；6-音频

               // if (audioPlayer != null)
                   // audioPlayer.setLoadRecordingWithNoVoice(false);

            }
        }catch (Exception e){
            e.printStackTrace();
            recordThread = null;
        }
    }

    //录像(TF卡录像下载)
    private void DownLoad_Record_TF_Video(int uid,int data_type,byte[] data, int decSize,int srcSize){
        try{
            if(recordThread != null && recordThread.isAlive()){//TF 卡下载
                initAdpcmToPCM();

                recordThread.initRecordFirstFrame(FirstFrame,firstSize);

               // if (audioPlayer != null)
                  //  audioPlayer.setLoadRecordingWithNoVoice(true);

                if(data_type == 6){
                    audio_Decoder(uid,data,decSize);
                }else{
                    recordThread.LocalRecordAVideo(data,decSize,0);// 0,1-视频；6-音频
                }

                onDataListener.onRecordLoading(uid,srcSize);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean checkBitstream(int nBitstream){
        if(this.nBitstream != nBitstream){
            this.nBitstream = nBitstream;
            return true;
        }
        return false;
    }
}