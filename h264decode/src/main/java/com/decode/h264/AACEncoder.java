package com.decode.h264;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Build;
import android.util.Log;

import com.bluecam.libs.Mp4v2Helper;
import com.decode.utils.CustomBuffer;
import com.decode.utils.CustomBufferData;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * @author andi
 * @file AACEncoder.java
 * @brief aac音频编码器
 * @date 2018/12/7*/
public class AACEncoder {

    public String MIME_TYPE="audio/mp4a-latm";
    public int KEY_CHANNEL_COUNT = 1;
    public int KEY_SAMPLE_RATE = 8000;
    public int KEY_BIT_RATE = 16000;
    public int KEY_AAC_PROFILE = MediaCodecInfo.CodecProfileLevel.AACObjectLC;

    public static final int TRY_AGAIN_LATER = -1;
    public static final int BUFFER_OK = 0;

    private Worker mWorker;
    private final String TAG="AACEncoder";
    private byte[] mFrameByte;

    private static AACEncoder instance = null;

    private CustomBuffer  inPCMBuffer = null ;
    private CustomBuffer outAACBuffer;

    //private FileOutputStream output = null;

    public static AACEncoder getInstance(){
        if(instance == null)
            instance = new AACEncoder();
        return instance;
    }

    public AACEncoder() {
        inPCMBuffer = new CustomBuffer();
        outAACBuffer = new CustomBuffer();

        start();
    }

    public boolean putPCMData(CustomBufferData audioData){
        //录像
        if(Mp4v2Helper.findMp4VideoTrack() > 0)
        {
            if (inPCMBuffer != null)
                inPCMBuffer.addData(audioData);

            return true;
        }

        return false;
    }

    public CustomBufferData getPCMData(){
        CustomBufferData data = null;
        if(inPCMBuffer != null){
            data = inPCMBuffer.RemoveData();
        }
        return data;
    }

    public CustomBufferData outAudioBuffer(){
        if(outAACBuffer != null)
            return outAACBuffer.RemoveData();
        return null;
    }

    public void start(){
        if(mWorker==null){
            mWorker=new Worker();
            mWorker.setRunning(true);
            mWorker.start();
        }
    }
    public void stop(){
        synchronized (AACEncoder.this) {
            if(mWorker!=null){
                mWorker.setRunning(false);
                mWorker=null;
            }

            if(inPCMBuffer != null){
                inPCMBuffer.ClearAll();
                inPCMBuffer = null;
            }

//            if(output != null){
//                try{
//                    output.close();
//                    output = null;
//                }catch (Exception e){
//                    e.printStackTrace();
//                    output = null;
//                }
//            }

            instance = null;
        }
    }

    private class Worker extends Thread{
        private final int mFrameSize = 2048;
        //private byte[] mBuffer;
        private boolean isRunning=false;
        private MediaCodec mEncoder;

        @Override
        public void run() {

            if(!initAACMediaEncode()){
                Log.d(TAG,"音频编码器初始化失败");
                isRunning=false;
            }

            while(isRunning){

                CustomBufferData data = getPCMData();
                if(data != null){
                    int result = input(data.data,data.head.length);

                    byte[] aacBuffer = new byte[1024*10];
                    int[]len = new int[1];

                    while (result == BUFFER_OK) {
                        result = output(aacBuffer,len);

                        if(result == BUFFER_OK) {
                            //outAACBuffer.addData(bufferdata);

                            Mp4v2Helper.mp4AEncode(aacBuffer,len[0]);

//                            if (output != null) {
//                                try {
//                                    output.write(aacBuffer, 0, len[0]);
//                                } catch (Exception e) {
//
//                                }
//                            }
                        }
                    }
                }
            }

            release();
        }

        public void setRunning(boolean run){
            isRunning = run;
        }

        public boolean isRunning(){
            return this.isRunning;
        }

        /**
         * 释放资源
         */
        private void release() {
            if(mEncoder!=null){
                mEncoder.stop();
                mEncoder.release();
            }
        }

        /**
         * @return true配置成功，false配置失败
         */
        private boolean initAACMediaEncode() {
            try {

                MediaFormat mediaFormat = MediaFormat.createAudioFormat(MIME_TYPE,KEY_SAMPLE_RATE, KEY_CHANNEL_COUNT);
                mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, KEY_BIT_RATE);
                mediaFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, KEY_AAC_PROFILE);
                mediaFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, 1000*1024);

                mEncoder = MediaCodec.createEncoderByType(MIME_TYPE);
                mEncoder.configure(mediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            if(mEncoder == null){
                return false;
            }

//            try {
//                SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd_HHmmss");
//                String strDate = f.format(new Date());
//                String fileName = FileHelper.VIDEO_PATH + strDate+"_"+"HOW_127411_MYGBX"+ ".aac";
//
//                output = new FileOutputStream(fileName, true);
//
//            }catch (Exception e){
//                e.printStackTrace();
//            }

            mEncoder.start();

            return true;
        }

        private int input(byte[] data,int len){
            synchronized (AACEncoder.this) {
                int inputBufferIndex = mEncoder.dequeueInputBuffer(-1);
                if (inputBufferIndex >= 0) {

                    ByteBuffer inputBuffer = mEncoder.getInputBuffer(inputBufferIndex);
                    inputBuffer.clear();
                    inputBuffer.put(data);
                    inputBuffer.limit(len);

                    mEncoder.queueInputBuffer(inputBufferIndex, 0, len,
                            System.nanoTime(), 0);
                } else {
                    return TRY_AGAIN_LATER;
                }
                return BUFFER_OK;
            }
        }

        private int output(byte[] aacBuffer,int[] len) {

           try{
               synchronized (AACEncoder.this) {

                   MediaCodec.BufferInfo encodeBufferInfo = new MediaCodec.BufferInfo();
                   int outputBufferIndex = mEncoder.dequeueOutputBuffer(encodeBufferInfo, 0);
                   if (outputBufferIndex >= 0) {

                       ByteBuffer[] mOutputBuffers  = mEncoder.getOutputBuffers();
                       ByteBuffer outputBuffer = null;

                       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                           outputBuffer = mEncoder.getOutputBuffer(outputBufferIndex);
                       } else {
                           outputBuffer = mOutputBuffers[outputBufferIndex];
                       }

                       if (outputBuffer != null){

                           outputBuffer.position(encodeBufferInfo.offset);
                           outputBuffer.limit(encodeBufferInfo.offset + encodeBufferInfo.size);

                           int outBitSize = encodeBufferInfo.size;

                           addADTStoPacket(aacBuffer,outBitSize + 7);
                           outputBuffer.get(aacBuffer,7,outBitSize);

                           len[0] = outBitSize + 7;
                       }

                       outputBuffer.position(encodeBufferInfo.offset);
                       mEncoder.releaseOutputBuffer(outputBufferIndex, false);

                   }else{
                       return TRY_AGAIN_LATER;
                   }
               }
           }catch (Exception e){
               e.printStackTrace();

               return TRY_AGAIN_LATER;
           }

           return BUFFER_OK;
        }

        /**
         * 给编码出的aac裸流添加adts头字段
         * @param packet 要空出前7个字节，否则会搞乱数据
         * @param packetLen
         */
        private void addADTStoPacket(byte[] packet, int packetLen) {
            int profile = 2;  //AAC LC
            int freqIdx = 11;  //8KHz
            int chanCfg = 1;  //CPE  通道数

            packet[0] = (byte)0xFF;
            packet[1] = (byte)0xF9;
            packet[2] = (byte)(((profile-1)<<6) + (freqIdx<<2) +(chanCfg>>2));
            packet[3] = (byte)(((chanCfg&3)<<6) + (packetLen>>11));
            packet[4] = (byte)((packetLen&0x7FF) >> 3);
            packet[5] = (byte)(((packetLen&7)<<5) + 0x1F);
            packet[6] = (byte)0xFC;
        }

        int GetSRIndex(int sampleRate)
        {
            if (92017 <= sampleRate) return 0;
            if (75132 <= sampleRate) return 1;
            if (55426 <= sampleRate) return 2;
            if (46009 <= sampleRate) return 3;
            if (37566 <= sampleRate) return 4;
            if (27713 <= sampleRate) return 5;
            if (23004 <= sampleRate) return 6;
            if (18783 <= sampleRate) return 7;
            if (13856 <= sampleRate) return 8;
            if (11502 <= sampleRate) return 9;
            if (9391 <= sampleRate) return 10;
            return 11;

            //00010 1011 0001 000
            //第一位：00010
            //第二位：0100
            //第三位：0010
            //第四位：000
            //合起來： 00010010 00010000 ＝＞ 0x12 0x10
        }
    }


}