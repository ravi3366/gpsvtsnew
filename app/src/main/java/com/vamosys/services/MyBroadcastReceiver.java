package com.vamosys.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.Toast;

import com.vamosys.vamos.R;
import com.vamosys.vamos.ShowDialog;


/**
 * Created by vamo on 10/01/18.
 */

public class MyBroadcastReceiver extends BroadcastReceiver  {
    MediaPlayer mediaPlayer;
    String vehiclename,safetyname,fuelname,fueldropname;
    String FUELMSG="FuelFilldata";
    String FUELDROPMSG = "Fueldropped";
    String SAFETYMSG = "safetyparking";
    @Override
    public void onReceive(Context context, Intent intent) {
//        mediaPlayer=MediaPlayer.create(context, android.provider.Settings.System.DEFAULT_RINGTONE_URI);
      //  mediaPlayer= MediaPlayer.create(context,R.raw.warning);

        vehiclename=intent.getStringExtra("vehiclename");

        Log.d("safetyname",""+safetyname);

        if (vehiclename !=null){
            //mediaPlayer= MediaPlayer.create(context,R.raw.warning);

          //  if (SAFETYMSG.equalsIgnoreCase(safetyname)) {
                mediaPlayer = MediaPlayer.create(context, android.provider.Settings.System.DEFAULT_RINGTONE_URI);
                mediaPlayer.start();
                Intent i = new Intent(context.getApplicationContext(), ShowDialog.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("vehicle", vehiclename);
                i.putExtra("safetyname","safetyparking");
                context.startActivity(i);
                Toast.makeText(context, "Safe Alarm on....", Toast.LENGTH_LONG).show();
         //   }
        }
//        if (vehiclename !=null ){
//            //mediaPlayer= MediaPlayer.create(context,R.raw.warning);
//            if (FUELMSG.equalsIgnoreCase(fuelname)) {
//                mediaPlayer = MediaPlayer.create(context, android.provider.Settings.System.DEFAULT_RINGTONE_URI);
//                mediaPlayer.start();
//                Intent i = new Intent(context.getApplicationContext(), ShowDialog.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                i.putExtra("vehicle", vehiclename);
//                i.putExtra("fuelname", fuelname);
//                context.startActivity(i);
//                Toast.makeText(context, "Fuel Alarm on....", Toast.LENGTH_LONG).show();
//            }
//        }

//        if (vehiclename !=null && fueldropname !=null){
//          //  mediaPlayer= MediaPlayer.create(context,R.raw.warning);
//            if (FUELDROPMSG.equalsIgnoreCase(fueldropname)) {
//                mediaPlayer = MediaPlayer.create(context, android.provider.Settings.System.DEFAULT_RINGTONE_URI);
//                mediaPlayer.start();
//                Intent i = new Intent(context.getApplicationContext(), ShowDialog.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                i.putExtra("vehicle", vehiclename);
//                i.putExtra("fueldropname", fueldropname);
//                context.startActivity(i);
//                Toast.makeText(context, "Fuel Dropped Alarm on....", Toast.LENGTH_LONG).show();
//            }
//        }



//        if (mediaPlayer.isPlaying()){
//            stopPlaying();
//            mediaPlayer.stop();
//        }else {
//            stopPlaying();
//            mediaPlayer=MediaPlayer.create(context, android.provider.Settings.System.DEFAULT_RINGTONE_URI);
//            mediaPlayer.start();
//            vehiclename=intent.getStringExtra("vehiclename");
//            Intent i=new Intent(context.getApplicationContext(),ShowDialog.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            i.putExtra("vehicle",vehiclename);
//            context.startActivity(i);
//            Toast.makeText(context, "Safe Alarm on....", Toast.LENGTH_LONG).show();
//        }

        
        
    }

    private void stopPlaying() {
        if (mediaPlayer !=null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }


}
