package com.vamosys.services;


import android.os.AsyncTask;

import com.vamosys.utils.HttpConfig;
import com.vamosys.vamos.Const;

public class UploadAddressTask extends AsyncTask<String, String, String> {
//    private final Context mContext;

//    public UploadAddressTask(Context context) {
//        mContext = context;
//    }

    @Override
    protected String doInBackground(String... params) {

        HttpConfig ht = new HttpConfig();
        String mUrl = Const.SAVE_ADDRESS_API + params[0];
        try {

//            System.out.println("Hi address upload to server url " + mUrl);

            ht.httpGet(mUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
