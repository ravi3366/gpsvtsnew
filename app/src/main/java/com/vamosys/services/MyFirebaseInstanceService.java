//package com.vamosys.services;
//
//
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.preference.PreferenceManager;
//import android.util.Log;
//
//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.FirebaseInstanceIdService;
//import com.vamosys.utils.ConnectionDetector;
//import com.vamosys.utils.Constant;
//import com.vamosys.utils.HttpConfig;
//import com.vamosys.vamos.Const;
//
//public class MyFirebaseInstanceService extends FirebaseInstanceIdService {
//    private static final String REG_TOKEN="REG_TOKEN";
//    ConnectionDetector cd;
//    String mGcmToken;
//
//    public MyFirebaseInstanceService(){
//
//    }
//    @Override
//    public void onTokenRefresh() {
//        cd = new ConnectionDetector(getApplicationContext());
//
//        String token = FirebaseInstanceId.getInstance().getToken();
//        Log.d(REG_TOKEN,token);
//        Log.d("firebasetoken",""+token);
//        saveGcmToken(getApplicationContext(), token);
//
//
//    }
//
//    private void saveGcmToken(Context mContext, String token) {
//        Log.d("gsmfirebase","savetoken");
//        try {
//            Constant.gcm_count++;
//            if (token != null) {
//                if (token.length() > 0 && !token.equalsIgnoreCase("SERVICE_NOT_AVAILABLE")) {
//                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
//                    editor.putString("gcmToken", token);
//                    editor.commit();
//
//                    if (Constant.is_logged_in) {
//                        Log.d("gsmfirebase","success");
//                       // sendGcmData(token);
//                    }
//
//                } else {
//                    Log.d("gsmfirebase","failure");
//
//
//                    if (Constant.gcm_count < 6) {
//                        onTokenRefresh();
//                    } else {
////                        System.out.println("Count over 1111111111::::::::::::::");
//                        stopService(new Intent(MyFirebaseInstanceService.this, MyFirebaseInstanceService.class));
//                    }
//
//
//                }
//            } else {
//
//                Log.d("gsmfirebase","failure1");
//                if (Constant.gcm_count < 6) {
//                    onTokenRefresh();
//                } else {
//
////                    System.out.println("Count over 222222222::::::::::::::");
//                    stopService(new Intent(MyFirebaseInstanceService.this, MyFirebaseInstanceService.class));
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.d("exceptionhand",""+e);
//        } finally {
//            stopService(new Intent(MyFirebaseInstanceService.this, MyFirebaseInstanceService.class));
//        }
//    }
//
//    public String sendGcmData(String token) {
//
//        mGcmToken = token;
//        String res = "";
//        try {
//
//            if (cd.isConnectingToInternet()) {
//                Log.d("gsmfirebase","success1");
//                HttpConfig config = new HttpConfig();
//                res = config.httpGet(Const.API_URL + "mobile/pushGcm?userId=" + Constant.SELECTED_EMP_ID + "&password=" + Constant.SELECTED_EMP_PWD + "&gcmId="
//                        + mGcmToken + "&notifyEnable=" + Constant.SELECTED_EMP_NOTI_ENABLE_STATUS + "&type=android");
//                // res = config.doPostEcpl(data, WebConfig.GET_TM_PATCH_LIST);
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }
//
//        if (res != null) {
//
//            if (res.equalsIgnoreCase("Success")) {
//
//            } else {
//                sendGcmData(mGcmToken);
//            }
//
//        }else {
//            sendGcmData(mGcmToken);
//        }
//
//        //
//        return res;
//    }
//}
