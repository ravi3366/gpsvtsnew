package com.vamosys.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.widget.Toast;

import com.vamosys.vamos.ShowDialog;

public class FuelFillBroadcast extends BroadcastReceiver {
    MediaPlayer mediaPlayer;
    String vehiclename;
    @Override
    public void onReceive(Context context, Intent intent) {
        vehiclename=intent.getStringExtra("vehiclename");

        if (vehiclename !=null){
            //mediaPlayer= MediaPlayer.create(context,R.raw.warning);

                mediaPlayer = MediaPlayer.create(context, android.provider.Settings.System.DEFAULT_RINGTONE_URI);
                mediaPlayer.start();
                Intent i = new Intent(context.getApplicationContext(), ShowDialog.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("vehicle", vehiclename);
                i.putExtra("fuelname", "FuelFilldata");
                context.startActivity(i);
                Toast.makeText(context, "Fuel Filled Alarm on....", Toast.LENGTH_LONG).show();

        }

    }
}
