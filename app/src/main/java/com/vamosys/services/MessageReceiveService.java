//package com.vamosys.services;
//
//import android.app.AlarmManager;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.os.Bundle;
//import android.preference.PreferenceManager;
//import android.support.v4.app.NotificationCompat;
//import android.util.Log;
//
//import com.google.android.gms.gcm.GcmListenerService;
//import com.vamosys.notification.NotificationUtils;
//import com.vamosys.utils.DaoHandler;
//import com.vamosys.utils.UnCaughtException;
//import com.vamosys.vamos.DBHelper;
//import com.vamosys.vamos.NotificationListActivity;
//import com.vamosys.vamos.NotificationViewerActivity;
//import com.vamosys.vamos.R;
//
///**
// * Created by Smart Team.
// */
//public class  MessageReceiveService extends GcmListenerService {
//    private static final String TAG = "GCM";
//
//    /**
//     * Called when message is received.
//     *
//     * @param from SenderID of the sender.
//     * @param data Data bundle containing message data as key/value pairs.
//     * For Set of keys use data.keySet().
//     */
//
//    DBHelper dbhelper;
//    SharedPreferences sp;
//
//    String parkingdata;
//    String safetymessage;
//    String safemsg = "SAFETY PARKING:";
//    String message;
//
//
//    private NotificationUtils notificationUtils;
//
//    // [START receive_message]
//    @Override
//    public void onMessageReceived(String from, Bundle data) {
//        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(
//                getApplicationContext()));
//        message = data.getString("message");
//        Log.d(TAG, "onMessageReceived() called with: " + "from = [" + from + "], data = [" + data + "]");
//        // checkPushList();
//        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        Log.d("message", "" + data.getString("message"));
//        parkingdata = message.substring(0, 15);
//        Log.d("parkingdata", "" + parkingdata);
//
//        boolean isLoggedIn = sp.getBoolean("is_logged_in", false);
//        if (isLoggedIn) {
//
//            try {
//                DaoHandler da = new DaoHandler(getApplicationContext(), true);
//                da.insertPushMsg(message);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            if (parkingdata.equals(safemsg)) {
//                safetymessage = data.getString("message");
//                Log.d("safetymessage", "" + safetymessage);
//                String[] msg = message.split("vehicle");
//                String part1 = msg[1].split("is")[0];
//                Intent intent = new Intent(getApplicationContext(), MyBroadcastReceiver.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra("vehiclename", part1);
//                PendingIntent pendingIntent = PendingIntent.getBroadcast(
//                        getApplicationContext(), 234324243, intent, 0);
//                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//                alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
//                        + (1000), pendingIntent);
//               //startForeground(1000,notificationUtils);
//            } else {
//                Log.d("safetymessage", "failure");
//            }
//
//            Intent intent = new Intent(getApplicationContext(), NotificationListActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* Request code */, intent,
//                    PendingIntent.FLAG_ONE_SHOT);
//            showNotificationMessage(getApplicationContext(), getApplicationContext().getResources().getString(R.string.app_name), message, intent);
//
//
//        }
////        if (isLoggedIn) {
////            DaoHandler da = new DaoHandler(getApplicationContext(), true);
////            da.insertPushMsg(message);
////            if (message.trim().length() > 16) {
////                parkingdata = message.substring(0, 15);
////                if (parkingdata.equals(safemsg)) {
////                    safetymessage = data.getString("message");
////                    String[] msg = message.split("vehicle");
////                    String part1 = msg[1].split("is")[0];
////                    Log.d("safetymessage",""+data.getString("message"));
////                    Intent intent = new Intent(getApplicationContext(), MyBroadcastReceiver.class);
////                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                    intent.putExtra("vehiclename", part1);
////                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
////                            getApplicationContext(), 234324243, intent, 0);
////                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
////                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
////                            + (1000), pendingIntent);
////                } else {
////                    Log.d("message", "failure");
////                }
////            }
////
//////        insertPushMsg(message);
////            showNotification(getApplicationContext(), message);
////        }
////        }
//    }
//
//    void dbSetup(Context context) {
//        dbhelper = new DBHelper(context);
//        try {
//            dbhelper.createDataBase();
//            dbhelper.openDataBase();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
////    public static void showNotification(Context context, String message) {
////        try {
////
////
////
////
////            Intent intent = new Intent(context, NotificationListActivity.class);
////            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
////                    PendingIntent.FLAG_ONE_SHOT);
////            NotificationViewerActivity.mMsgContent = message;
////            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
////            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
////                    .setSmallIcon(R.mipmap.ic_launcher)
////                    .setContentTitle(context.getResources().getString(R.string.app_name))
////                    .setContentText(message)
////                    .setAutoCancel(true)
////                    .setSound(defaultSoundUri)
////                    .setContentIntent(pendingIntent);
////            NotificationManager notificationManager =
////                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
////            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////    }
//
//    private void showNotificationMessage(Context context, String title, String message, Intent intent) {
//        notificationUtils = new NotificationUtils(context);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        notificationUtils.showNotificationMessage(title, message, intent);
//    }
//
////    public void insertPushMsg(String msg) {
////        Date d = new Date();
////        String time = d.getDate() + "-" + d.getMonth() + 1 + "-" + d.getYear()
////                + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
////
////        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
////
////
////        String DateToStr = sdf.format(d);
////
////
////        ContentValues cv = new ContentValues();
////        cv.put("msg_data", msg);
////        cv.put("time_stamp", DateToStr);
////        DataBaseHandler db = new DataBaseHandler(getBaseContext());
////        try {
////            int a = (int) db.open().getDatabaseObj()
////                    .insert(DataBaseHandler.TABLE_NOTIFICATIONS, null, cv);
////
////
////        } catch (SQLException e) {
////
////        } finally {
////
////            db.close();
////        }
////
////    }
//
////    public void check PushList() {
////        DataBaseHandler db = new DataBaseHandler(getBaseContext());
////        String qry = "DELETE FROM " + DataBaseHandler.TABLE_NOTIFICATIONS + " WHERE id NOT IN (SELECT id FROM " + DataBaseHandler.TABLE_NOTIFICATIONS + " ORDER BY id DESC LIMIT 1000 ) ";
////        try {
////            db.open().getDatabaseObj().rawQuery(qry, null);
////
////
////        } catch (SQLException e) {
////
////        } finally {
////            db.close();
////        }
////    }
//
//    public interface receiver {
//        void call();
//   }
//}
