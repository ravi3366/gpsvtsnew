package com.vamosys.services;


import android.Manifest;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.vamosys.model.LatLngDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.UnCaughtException;
import com.vamosys.vamos.Const;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class LatLngLiveTrackingService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    Timer timer;

    List<LatLngDto> mExistingLatLngList;
    ConnectionDetector cd;
    Context contextinfo;
    long timeInterval = 10;
    SharedPreferences sp;


    double latitude;
    double longitude;


    String mImeiNoValue, mLatValue, mLongValue, mDateTimeValue;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private static final String LOGSERVICE = "Maps";

    private TcpClient mTcpClient;
    String mDomainName = null;
    boolean mIsInternetErrroToastShown = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        contextinfo = getApplicationContext();
        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(
                contextinfo));
        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        buildGoogleApiClient();
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }


        if (sp.getString("ip_adds", "") != null && sp.getString("ip_adds", "").trim().length() > 0) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                String mDomain = sp.getString("ip_adds", "");
//                System.out.println("Hi domain 00 " + mDomain);

                mDomainName = mDomain.substring((Integer.valueOf(mDomain.indexOf("://")) + 3)).replace("/", "");
//                System.out.println("Hi domain 11 " + mDomainName);
            }
        } else {
            String mDomain = Const.API_URL;
//            System.out.println("Hi domain else 00 " + mDomain);

            mDomainName = mDomain.substring((Integer.valueOf(mDomain.indexOf("://")) + 3)).replace("/", "");
//            System.out.println("Hi domain else 11 " + mDomainName);
        }


        if (cd.isConnectingToInternet()) {
//            new ConnectTask().execute("");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                        my_task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);
                new ConnectTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new ConnectTask().execute();
            }

//            connectTcp();

        }


//        System.out.println("Hi Time interval " + sp.getString("live_tracking_interval", "3"));

        timeInterval = Long.parseLong(sp.getString("live_tracking_interval", "3"));
//        timeInterval = 10;
        startTimer();
//
        if (checkPhonePermission(contextinfo)) {
            TelephonyManager tma = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//
            mImeiNoValue = tma.getDeviceId();
        }
//
////        String formatted = String.format("%0" + String.valueOf(16 - mImeiNoValue.length()) + "d", mImeiNoValue);
//        System.out.println("Hi LatLngLiveTrackingService service created " + mImeiNoValue);

    }

//    @Override
//    public void onStart(Intent intent, int startId) {
//        super.onStart(intent, startId);
//
//        if (!mGoogleApiClient.isConnected())
//            mGoogleApiClient.connect();
//
//        startTimer();
//
//        if (checkPhonePermission(contextinfo)) {
//            TelephonyManager tma = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//
//            mImeiNoValue = tma.getDeviceId();
//        }
//
////        String formatted = String.format("%0" + String.valueOf(16 - mImeiNoValue.length()) + "d", mImeiNoValue);
//        System.out.println("Hi LatLngLiveTrackingService service created " + mImeiNoValue);
//
//    }

    public static boolean checkPhonePermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        showErrorText("Live Tracking is disabled");

        if (timer != null) {
            timer.cancel();
        }
        stopLocationUpdate();
        if (mTcpClient != null) {
            mTcpClient.stopClient();
        }
    }


    private void startTimer() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {

            public void run() {
                System.out.println("Hi timer running" + timeInterval);
//                LocationDataService lo = new LocationDataService(getApplicationContext());
                if (checkPermission(contextinfo)) {
//                    System.out.println("Hi LatLngTrackingService is running 000 " + latitude + " : " + longitude);
                    startLocationUpdate();
//                    System.out.println("Hi LatLngTrackingService is running " + latitude + " : " + longitude);
                }

            }
        }, 1000, timeInterval * 1000);
    }

    private void formData() {
        try {
            long dateTimeUtc = System.currentTimeMillis();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String utcTime = sdf.format(new Date());
            try {
                Date d = sdf.parse(utcTime);
                dateTimeUtc = d.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (cd.isConnectingToInternet()) {

                mIsInternetErrroToastShown = false;
                if (mTcpClient != null && !mTcpClient.isConnectedWithTCP()) {
                    DaoHandler da = new DaoHandler(contextinfo, false);
//                List<LatLngDto> lalgList = new ArrayList<LatLngDto>();
//                lalgList = da.getOfflineStoredLatLng();
                    System.out.println("Hi existing database data is 000:::::" + da.getOfflineStoredLatLng().size());
                    if (da.getOfflineStoredLatLng().size() > 0) {
//                    mExistingLatLngList = new ArrayList<LatLngDto>();
//                    mExistingLatLngList = lalgList;
                        da.insertLiveTrackingData(String.valueOf(latitude), String.valueOf(longitude), String.valueOf(dateTimeUtc));
                        sendExistingData();
                        System.out.println("Hii ");

                    } else {
                        formByteData(latitude, longitude, dateTimeUtc, false);
                    }


                } else {
//                    System.out.println("Hi tcp client null 222");
                    showErrorText("Live tracking Failure");

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                        my_task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);
                        new ConnectTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new ConnectTask().execute();
                    }
//                    new ConnectTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

//                    connectTcp();

                    DaoHandler da = new DaoHandler(contextinfo, false);
                    da.insertLiveTrackingData(String.valueOf(latitude), String.valueOf(longitude), String.valueOf(dateTimeUtc));
//                List<LatLngDto> lalgList = new ArrayList<LatLngDto>();
//                lalgList = da.getOfflineStoredLatLng();
//
//                if (lalgList.size() > 0) {
//                    mExistingLatLngList = new ArrayList<LatLngDto>();
//                    mExistingLatLngList = lalgList;
//
//                    sendExistingData();
//                }
//
//                formByteData(latitude, longitude, System.currentTimeMillis());
                }
            } else {


                //store in db
                DaoHandler da = new DaoHandler(contextinfo, true);
                da.insertLiveTrackingData(String.valueOf(latitude), String.valueOf(longitude), String.valueOf(dateTimeUtc));
                if (!mIsInternetErrroToastShown) {
                    showErrorText("Offline tracking mode enabled.Due to bad internet connection");
                    mIsInternetErrroToastShown = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showErrorText(final String mText) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),
                        mText,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendExistingData() {


        DaoHandler da = new DaoHandler(contextinfo, false);
        List<LatLngDto> lalgList = new ArrayList<LatLngDto>();
        lalgList = da.getOfflineStoredLatLng();

        boolean mIsSent = false;
        for (int i = 0; i < lalgList.size(); i++) {
            mIsSent = formByteData(lalgList.get(i).getLat(), lalgList.get(i).getLng(), lalgList.get(i).getmDateTime(), true);

            if (mIsSent) {
                da.deleteLocationData(lalgList.get(i).getId());
            } else {
                mIsSent = false;
//                if (mTcpClient != null) {
//                    mTcpClient.stopClient();
//                }

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
////                        my_task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);
//                    new ConnectTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                } else {
//                    new ConnectTask().execute();
//                }

                break;
            }
        }

        System.out.println("Hi offline sent success " + mIsSent);

        if (mIsSent) {
            if (da.getOfflineStoredLatLng().size() > 0) {

                System.out.println("Hi offline sent success next data called ");

                sendExistingData();
            }
        }
    }

    private boolean formByteData(double mLatitudeData, double mLongitudeData, long mDateTimeData, boolean isExisting) {
        byte[] byteArray = new byte[42];
        if (checkPhonePermission(contextinfo)) {
            TelephonyManager tma = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//
            mImeiNoValue = tma.getDeviceId();
        }
        System.out.println("Hi the imei number is ::: " + mImeiNoValue);
        if (mImeiNoValue != null) {


            byteArray[0] = 0x68; //login
            byteArray[1] = 0x68;

            byteArray[2] = 0x25;  // packetLength

            byteArray[3] = 0x26;  // LAC
            byteArray[4] = 0x6A;


            byte[] terminalArray = convertImei(mImeiNoValue);

//            865374020885383

            if (terminalArray.length >= 8) {
                byteArray[5] = terminalArray[0];   //TerminalId
                byteArray[6] = terminalArray[1];
                byteArray[7] = terminalArray[2];
                byteArray[8] = terminalArray[3];
                byteArray[9] = terminalArray[4];
                byteArray[10] = terminalArray[5];
                byteArray[11] = terminalArray[6];
                byteArray[12] = terminalArray[7];
            }
//double in=80;
//            byteArray[5] = 8;   //TerminalId
//            byteArray[6] = 65;
//            byteArray[7] = 37;
//            byteArray[8] = 40;
//            byteArray[9] = 20;
//            byteArray[10] = 88;
//            byteArray[11] = 53;
//            byteArray[12] = 83;


            byteArray[13] = 0x00; //Information serial number
            byteArray[14] = 0x01;
//            int protocolNumber = 10;
            byteArray[15] = 0x10; // protocol No
//            byteArray[15] = (byte) 10; // protocol No


            byte[] dateTimeArray = convertDateTime(mDateTimeData);
            if (dateTimeArray.length >= 6) {
                byteArray[16] = dateTimeArray[0];   // Date time
                byteArray[17] = dateTimeArray[1];
                byteArray[18] = dateTimeArray[2];
                byteArray[19] = dateTimeArray[3];
                byteArray[20] = dateTimeArray[4];
                byteArray[21] = dateTimeArray[5];
            }


            byte[] latArray = convertLatLng(mLatitudeData, true);
            if (latArray.length >= 4) {
                byteArray[22] = latArray[0];   // lat
                byteArray[23] = latArray[1];
                byteArray[24] = latArray[2];
                byteArray[25] = latArray[3];
            }


//            byte[] latArray = convertLatLng(mLatitudeData, true);
//            if (latArray.length >= 4) {
//                byteArray[22] = latArray[0];   // lat
//                byteArray[23] = latArray[0];
//                byteArray[24] = latArray[0];
//                byteArray[25] = latArray[0];
//            }


            byte[] lngArray = convertLatLng(mLongitudeData, false);
            if (lngArray.length >= 4) {
                byteArray[26] = lngArray[0];   // lng
                byteArray[27] = lngArray[1];
                byteArray[28] = lngArray[2];
                byteArray[29] = lngArray[3];

            }
            byteArray[30] = 0x00; // speed

            byteArray[31] = 0x01; // Course
            byteArray[32] = 0x56;

            byteArray[33] = 0x00; // MNC

            byteArray[34] = 0x1D; // Cell ID
            byteArray[35] = (byte) 0xF1;


            byteArray[36] = 0x00;   // Status
            byteArray[37] = 0x00;
            byteArray[38] = 0x00;
            byteArray[39] = 0x06;

            byteArray[40] = 0x0D; // Stop bit
            byteArray[41] = 0x0A;
        }

        boolean mIsSent = mTcpClient.sendMessage(byteArray);



//
        if (!mIsSent && !isExisting) {
            DaoHandler da = new DaoHandler(contextinfo, true);

            da.insertLiveTrackingData(String.valueOf(mLatitudeData), String.valueOf(mLongitudeData), String.valueOf(mDateTimeData));

            showErrorText("Live tracking failure.Data stored in device");

        }

        if(!mIsSent){
            showErrorText("Live tracking connection got failure");

            if (mTcpClient != null) {
                mTcpClient.stopClient();
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                        my_task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);
                new ConnectTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new ConnectTask().execute();
            }
        }

        System.out.println("Hi data sent suc " + mIsSent);

        return mIsSent;
    }


    private String convert(double latitude, double longitude) {

        String hex = Integer.toHexString(23306055);

//        System.out.println("Hi hex value is :::" + hex.toUpperCase());


        StringBuilder builder = new StringBuilder();

        if (latitude < 0) {
            builder.append("S ");
        } else {
            builder.append("N ");
        }

        String latitudeDegrees = Location.convert(Math.abs(latitude), Location.FORMAT_MINUTES);
//        System.out.println("Hi lat DM " + latitudeDegrees);
        String[] latitudeSplit = latitudeDegrees.split(":");
        builder.append(latitudeSplit[0]);
        builder.append("°");
        builder.append(latitudeSplit[1]);

//        System.out.println("Hi latitude before decimal value ::: " + Integer.valueOf(latitudeSplit[0]) + ": " + Double.parseDouble(latitudeSplit[1]));

        int latDecimalValue = (int) (((Integer.valueOf(latitudeSplit[0])) * 60 + (Double.parseDouble(latitudeSplit[1]))) * 30000);
//        int latDecimalValue = (int) ((22 * 60 + 32.7658) * 3000);

//        System.out.println("Hi latitude decimal value ::: " + latDecimalValue);


//        builder.append("'");
//        builder.append(latitudeSplit[2]);
//        builder.append("\"");

        builder.append(" ");

        if (longitude < 0) {
            builder.append("W ");
        } else {
            builder.append("E ");
        }

        String longitudeDegrees = Location.convert(Math.abs(longitude), Location.FORMAT_MINUTES);

//        System.out.println("Hi lng DM " + longitudeDegrees);


        String[] longitudeSplit = longitudeDegrees.split(":");
        builder.append(longitudeSplit[0]);
        builder.append("°");
        builder.append(longitudeSplit[1]);
//        builder.append("'");
//        builder.append(longitudeSplit[2]);
//        builder.append("\"");
//        System.out.println("Hi longitude before decimal value ::: " + Integer.valueOf(longitudeSplit[0]) + ": " + Double.parseDouble(longitudeSplit[1]) + " : " + (Integer.valueOf(longitudeSplit[0])) * 60 + (Double.parseDouble(longitudeSplit[1])));
        int lngDecimalValue = (int) (((Integer.valueOf(longitudeSplit[0])) * 60 + (Double.parseDouble(longitudeSplit[1]))) * 30000);

//        System.out.println("Hi longitude decimal value ::: " + lngDecimalValue);

        return builder.toString();
    }

    private byte[] convertLatLng(double latLongitude, boolean mIsLatitude) {

        int latLngDecimalValue;

        if (mIsLatitude) {


            String latitudeDegrees = Location.convert(Math.abs(latLongitude), Location.FORMAT_MINUTES);
//            System.out.println("Hi lat DM " + latitudeDegrees);
            String[] latitudeSplit = latitudeDegrees.split(":");
//            System.out.println("Hi latitude before decimal value ::: " + Integer.valueOf(latitudeSplit[0]) + ": " + Double.parseDouble(latitudeSplit[1]));

            latLngDecimalValue = (int) (((Integer.valueOf(latitudeSplit[0])) * 60 + (Double.parseDouble(latitudeSplit[1]))) * 30000);

//            latLngDecimalValue = (int) ((((Integer.valueOf(latitudeSplit[0])) + (Double.parseDouble(latitudeSplit[1]))))*(60* 30000));


//            System.out.println("Hi latitude decimal value ::: " + latLngDecimalValue);


        } else {


            String longitudeDegrees = Location.convert(Math.abs(latLongitude), Location.FORMAT_MINUTES);
            String[] longitudeSplit = longitudeDegrees.split(":");

//            latLngDecimalValue = (int) (((Integer.valueOf(longitudeSplit[0])) * 60 + (Double.parseDouble(longitudeSplit[1]))) * 30000);
            latLngDecimalValue = (int) (((Integer.valueOf(longitudeSplit[0])) * 60 + (Double.parseDouble(longitudeSplit[1]))) * 30000);

        }


        String strLatLngDecimalValue = Integer.toHexString(latLngDecimalValue);
//        System.out.println("Hi latlng hex value is :::" + strLatLngDecimalValue.length() + " ::hex to integer  " + convertLatLngtoHexInteger(strLatLngDecimalValue));
//        String strLatLngDecimalValue = String.valueOf(latLngDecimalValue);
        if (strLatLngDecimalValue.length() < 8 && strLatLngDecimalValue.length() > 6) {
            strLatLngDecimalValue = "0" + strLatLngDecimalValue;
        } else if (strLatLngDecimalValue.length() < 7 && strLatLngDecimalValue.length() > 5) {
            strLatLngDecimalValue = "00" + strLatLngDecimalValue;
        } else if (strLatLngDecimalValue.length() < 6 && strLatLngDecimalValue.length() > 4) {
            strLatLngDecimalValue = "000" + strLatLngDecimalValue;
        } else if (strLatLngDecimalValue.length() < 5 && strLatLngDecimalValue.length() > 3) {
            strLatLngDecimalValue = "0000" + strLatLngDecimalValue;
        } else if (strLatLngDecimalValue.length() < 4 && strLatLngDecimalValue.length() > 2) {
            strLatLngDecimalValue = "00000" + strLatLngDecimalValue;
        } else if (strLatLngDecimalValue.length() < 3 && strLatLngDecimalValue.length() > 1) {
            strLatLngDecimalValue = "000000" + strLatLngDecimalValue;
        } else if (strLatLngDecimalValue.length() == 1) {
            strLatLngDecimalValue = "0000000" + strLatLngDecimalValue;
        } else if (strLatLngDecimalValue.length() < 1) {
            strLatLngDecimalValue = "00000000" + strLatLngDecimalValue;
        }
        int len = strLatLngDecimalValue.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {

//            System.out.println("data brfore :" + strLatLngDecimalValue + ":" + strLatLngDecimalValue.substring(i, i + 2));


//            System.out.println("Hi the latlng convert integer is " + convertLatLngtoHexInteger(strLatLngDecimalValue.substring(i, i + 2)) + " original text :: " + strLatLngDecimalValue.substring(i, i + 2));
//            System.out.println("Hi the decode lat lng " + convertToInteger(strLatLngDecimalValue.substring(i, i + 2)));
//            System.out.println("Hi the decode lat lng " + convertLatLngtoHexInteger(strLatLngDecimalValue.substring(i, i + 2)));
//            data[i / 2] = (byte) convertToInteger(strLatLngDecimalValue.substring(i, i + 2));

            data[i / 2] = (byte) convertLatLngtoHexInteger(strLatLngDecimalValue.substring(i, i + 2));

        }

//        byte[] data = new byte[1];
//        System.out.println("Hi the decode lat lng " + convertLatLngtoHexInteger(strLatLngDecimalValue));
//        data[0] = (byte) convertLatLngtoHexInteger(strLatLngDecimalValue);


        return data;
    }

    private byte[] convertImei(String mImeiNoData) {
        if (mImeiNoData.length() > 5) {


            if (mImeiNoData.length() < 16 && mImeiNoData.length() > 14) {
                mImeiNoData = "0" + mImeiNoData;
            } else if (mImeiNoData.length() < 15 && mImeiNoData.length() > 13) {
                mImeiNoData = "00" + mImeiNoData;
            } else if (mImeiNoData.length() < 14 && mImeiNoData.length() > 12) {
                mImeiNoData = "000" + mImeiNoData;
            } else if (mImeiNoData.length() < 13 && mImeiNoData.length() > 11) {
                mImeiNoData = "0000" + mImeiNoData;
            } else if (mImeiNoData.length() < 12 && mImeiNoData.length() > 10) {
                mImeiNoData = "00000" + mImeiNoData;
            } else if (mImeiNoData.length() < 11 && mImeiNoData.length() > 9) {
                mImeiNoData = "000000" + mImeiNoData;
            } else if (mImeiNoData.length() < 10 && mImeiNoData.length() > 8) {
                mImeiNoData = "0000000" + mImeiNoData;
            } else if (mImeiNoData.length() < 9 && mImeiNoData.length() > 7) {
                mImeiNoData = "00000000" + mImeiNoData;
            }

        }

        int len = mImeiNoData.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            String hexValue = "0x" + mImeiNoData.substring(i, i + 2);

            data[i / 2] = (byte) convertToInteger(mImeiNoData.substring(i, i + 2));

        }


        return data;

    }

//    public static byte[] ToByteArray(String HexString) {
//        int NumberChars = HexString.length();
//        byte[] bytes = new byte[NumberChars / 2];
//        for (int i = 0; i < NumberChars; i += 2) {
//
//            bytes[i / 2] = Convert.ToByte(HexString.substring(i, 2), 16);
//        }
//        return bytes;
//    }


    private byte[] convertDateTime(long mDateTimeData) {
//        mDateTimeData = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(mDateTimeData);
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
        int day = now.get(Calendar.DAY_OF_MONTH);
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        int second = now.get(Calendar.SECOND);
        int millis = now.get(Calendar.MILLISECOND);

        System.out.printf("%d-%02d-%02d %02d:%02d:%02d.%03d", year, month, day, hour, minute, second, millis);

        String yearData = String.valueOf(year);
//        System.out.println("Hi Year Value " + yearData + " : " + yearData.charAt(2) + yearData.charAt(3));
        int yearValue = Integer.valueOf(yearData.substring(2));
        String yearHex = Integer.toHexString(yearValue);
//        System.out.println("Hi Year hex Value " + yearHex + " : " + yearValue);
        if (yearHex.length() == 1) {
            yearHex = "0" + yearHex;
        }

        String monthHex = Integer.toHexString(month);
        if (monthHex.length() == 1) {
            monthHex = "0" + monthHex;
        }

        String dayHex = Integer.toHexString(day);
        if (dayHex.length() == 1) {
            dayHex = "0" + dayHex;
        }

        String hourHex = Integer.toHexString(hour);
        if (hourHex.length() == 1) {
            hourHex = "0" + hourHex;
        }

        String minuteHex = Integer.toHexString(minute);
        if (minuteHex.length() == 1) {
            minuteHex = "0" + minuteHex;
        }

        String secondhex = Integer.toHexString(second);
        if (secondhex.length() == 1) {
            secondhex = "0" + secondhex;
        }


        byte[] data = new byte[6];
//        data[0] = (byte) ((yearHex.charAt(0))
//                + (yearHex.charAt(1)));
//        data[1] = (byte) ((monthHex.charAt(0))
//                + (monthHex.charAt(1)));
//        data[2] = (byte) ((dayHex.charAt(0))
//                + (dayHex.charAt(1)));
//        data[3] = (byte) ((hourHex.charAt(0))
//                + (hourHex.charAt(1)));
//        data[4] = (byte) ((minuteHex.charAt(0))
//                + (minuteHex.charAt(1)));
//        data[5] = (byte) ((secondhex.charAt(0))
//                + (secondhex.charAt(1)));


//        System.out.println("Hi datetime decode yr value " + convertDateTimetoHexInteger(yearHex));
//        System.out.println("Hi datetime decode month value " + convertDateTimetoHexInteger(monthHex));
//        System.out.println("Hi datetime decode date value " + convertDateTimetoHexInteger(dayHex));
//        System.out.println("Hi datetime decode hour value " + convertDateTimetoHexInteger(hourHex));
//        System.out.println("Hi datetime decode minutes value " + convertDateTimetoHexInteger(minuteHex));
//        System.out.println("Hi datetime decode seconds value " + convertDateTimetoHexInteger(secondhex));

        data[0] = (byte) (convertDateTimetoHexInteger(yearHex));
        data[1] = (byte) (convertDateTimetoHexInteger(monthHex));
        data[2] = (byte) (convertDateTimetoHexInteger(dayHex));
        data[3] = (byte) (convertDateTimetoHexInteger(hourHex));
        data[4] = (byte) (convertDateTimetoHexInteger(minuteHex));
        data[5] = (byte) (convertDateTimetoHexInteger(secondhex));


//        System.out.println("Hi datetime byte array length" + data.length);
        //byte[] byteArray = new byte[4];

        return data;

    }

    private int convertDateTimetoHexInteger(String str) {

//        System.out.println("Hi the date time original text is " + str);

        return Integer.decode("0x" + str);
    }

    private int convertLatLngtoHexInteger(String str) {

        return Integer.decode("0x" + str);
    }


    public static int convertToInteger(String n) {
        return Integer.valueOf(n, 16);
    }

    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location l = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (l != null) {
            // Log.i(LOGSERVICE, "lat " + l.getLatitude());
            // Log.i(LOGSERVICE, "lng " + l.getLongitude());

        }

        startLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(LOGSERVICE, "" + location.getLatitude());
        Log.i(LOGSERVICE, "" + location.getLongitude());
        LatLng mLocation = (new LatLng(location.getLatitude(), location.getLongitude()));
        if (location.getLatitude() != 0
                && location.getLongitude() != 0) {
            ContentValues cv = new ContentValues();
            cv.put("laa", location.getLatitude());
            cv.put("loo", location.getLongitude());
            try {
                System.out.println("Hi inserting location values are 000000:: " + cv);
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                formData();

//                DaoHandler db = new DaoHandler(getApplicationContext());
//                db.insertLocData(
//                        cv, String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
            } catch (Exception sqle) {
                sqle.printStackTrace();
            }
        }
//        stopLocationUpdate();
    }

    private void initLocationRequest() {
        mLocationRequest = new LocationRequest();
//        mLocationRequest.setInterval(5 * 60 * 1000);
//        mLocationRequest.setFastestInterval(2 * 60 * 1000);

        mLocationRequest.setInterval(timeInterval * 1000);
        mLocationRequest.setFastestInterval(timeInterval * 1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

    }

    private void startLocationUpdate() {
        initLocationRequest();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void stopLocationUpdate() {

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

        Intent in = new Intent(getApplicationContext(), LatLngLiveTrackingService.class);
        stopService(in);

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void connectTcp() {
        System.out.println("Hi tcp client connect called ::: ");
        mTcpClient = new TcpClient(new TcpClient.OnMessageReceived() {
            @Override
            //here the messageReceived method is implemented
            public void messageReceived(String message) {
                //this method calls the onProgressUpdate

                Log.i("TCP", "Msg received " + message);

//                publishProgress(message);
            }
        });
        mTcpClient.run(mDomainName);
    }

//    String s = "{"
//    mapvalue":"
//
//    {\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[78.43890317213665,17.464572502763748]}}", "time":
//        "17-Aug-2017  12:45", "photo_string":"str_image", "incident":"Hijackings", "desc":
//        "terrific", "detail":"Bdjbdjbdjbd", "dummy":"dummy"
//    }}";
//
//    869087021618902

    public class ConnectTask extends AsyncTask<String, String, TcpClient> {

        @Override
        protected TcpClient doInBackground(String... message) {

            //we create a TCPClient object and

//            System.out.println("Hi tcp client connect called ::: ");
            mTcpClient = new TcpClient(new TcpClient.OnMessageReceived() {
                @Override
                //here the messageReceived method is implemented
                public void messageReceived(String message) {
                    //this method calls the onProgressUpdate

                    Log.i("TCP", "Msg received " + message);

                    publishProgress(message);
                }
            });

            if (mTcpClient != null) {
                mTcpClient.run(mDomainName);
            }


            return null;
        }

        @Override
        protected void onPostExecute(TcpClient tcpClient) {
            super.onPostExecute(tcpClient);
//            System.out.println("Hi tcp client connect called :::onPostExecute ");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            System.out.println("Hi tcp client connect called :::onPreExecute ");
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);

            //in the arrayList we add the messaged received from server
//            arrayList.add(values[0]);
            // notify the adapter that the data set has changed. This means that new message received
            // from server was added to the list
//            mAdapter.notifyDataSetChanged();
        }
    }
}
