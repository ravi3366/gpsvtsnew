package com.vamosys.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vamosys.model.PaymentDueDto;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.List;

public class PaymentDueAdapter extends RecyclerView.Adapter<PaymentDueAdapter.ViewHolder> {
    private List<PaymentDueDto> mArrayList;
    //    private ArrayList<AndroidVersion> mFilteredList;
    private Activity myActivity;

    public PaymentDueAdapter(List<PaymentDueDto> arrayList, Activity act) {
        mArrayList = arrayList;
        //  mFilteredList = arrayList;
        myActivity = act;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.payment_overdue_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {

        if (mArrayList.get(i).getDueDate() != null && Const.daysDiff(mArrayList.get(i).getDueDate().trim()) > 0) {
            viewHolder.tv_payment_due_alert.setText("Payment overdue for " + Const.daysDiff(mArrayList.get(i).getDueDate().trim()) + " days");
        }
        viewHolder.tv_payment_due_amount.setText("Amount : " + mArrayList.get(i).getBalanceAmount());


        viewHolder.mImg_invoice_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mArrayList.get(i).getInvoiceLink() != null) {
                    //open
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mArrayList.get(i).getInvoiceLink()));
                    myActivity.startActivity(browserIntent);
//                    myActivity.finish();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_payment_due_alert, tv_payment_due_amount;
        private ImageView mImg_invoice_download;

        public ViewHolder(View view) {
            super(view);


            tv_payment_due_alert = (TextView) view.findViewById(R.id.txt_payment_due_alert);
            tv_payment_due_amount = (TextView) view.findViewById(R.id.txt_payment_due_amount);
            mImg_invoice_download = (ImageView) view.findViewById(R.id.img_invoice_download);


        }
    }

}