package com.vamosys.adapter;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vamosys.interfaces.ShowMapInterface;
import com.vamosys.model.FuelFillDto;
import com.vamosys.model.FuelTheftDto;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class SensorTheftAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    Context mContext;
    ArrayList<FuelTheftDto> mFuelTheftReportData = new ArrayList<FuelTheftDto>();
    ShowMapInterface showMap;
    public SensorTheftAdapter(Context context, ArrayList<FuelTheftDto> mFuelTheftReportData, ShowMapInterface showMapInterface) {
        mContext=context;
        this.mFuelTheftReportData=mFuelTheftReportData;
        showMap=showMapInterface;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("fueldata","oncreate");
        if (viewType == TYPE_ITEM) {
            // Here Inflating your recyclerview item layout
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fuel_theft_report_list_row, parent, false);
            return new RowViewHolder(view);
        } else if (viewType == TYPE_HEADER) {
            // Here Inflating your header view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fuel_theft_report_header, parent, false);
            return new HeaderViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.d("fueldata","onBind");

        try {
            if (holder instanceof HeaderViewHolder){
                Log.d("fueldata","onBind1");
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            }
            else if (holder instanceof RowViewHolder){
                Log.d("fueldata","onBind2");
                final RowViewHolder holder1 = (RowViewHolder) holder;
                // Following code give a row of header and decrease the one position from listview items
                FuelTheftDto f=getItem(position);
                holder1.txt_date.setText(Const.getTimefromserver(f.getStartdate()));
                holder1.txt_todate.setText(Const.getTimefromserver(f.getDate()));
                holder1.txt_pLtr.setText(f.getPrevLitr() );
                holder1.txt_cLtr.setText(f.getCurrLitr());
                double i2 = f.getTheftLitr();
                holder1.txt_tLtr.setText((String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(i2)))));
                Log.d("theftltr",""+i2);
                Log.d("theftltr1",""+Double.parseDouble(new DecimalFormat("##.##").format(i2)));
                // col5.setText(data.getTheftLoc());
                double i3 = f.getOdoLitr();
                holder1.txt_odo.setText((String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(i3)))));

                SpannableString content = new SpannableString("G-Map");
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//            col3.setText(content);
                holder1.txt_gmap.setText(content);
                holder1. txt_gmap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        double mLatitude,mLongitude;
                        mLatitude = Double.valueOf(f.getLatitude());
                        mLongitude = Double.valueOf(f.longitude);
                        Log.d("fueldata","address "+ mLatitude+" "+ mLongitude);
                        showMap.showLatLng(mLatitude,mLongitude);
                    }
                });
            }
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return mFuelTheftReportData.size()+1;
    }

    private FuelTheftDto getItem(int position) {
        return mFuelTheftReportData.get(position-1);
    }
    private boolean isPositionHeader(int position) {
        return position == 0;
    }
    public class RowViewHolder extends RecyclerView.ViewHolder{
        TextView txt_date,txt_todate,txt_pLtr,txt_cLtr,txt_tLtr,txt_odo,txt_gmap;
        public RowViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_date=itemView.findViewById(R.id.ft_report_datetime_textView);
            txt_todate=itemView.findViewById(R.id.ft_report_todatetime_textView);
            txt_pLtr=itemView.findViewById(R.id.ft_report_prevltrs_textView);
            txt_cLtr=itemView.findViewById(R.id.ft_report_curltrs_textView);
            txt_tLtr=itemView.findViewById(R.id.ft_report_theftltrs_textView);
            txt_odo=itemView.findViewById(R.id.ft_report_odo_textView);
            txt_gmap=itemView.findViewById(R.id.ft_report_gmap_textView);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView Htxt_date,Htxt_todate,Htxt_pLtr,Htxt_cLtr,Htxt_tLtr,Htxt_odo,Htxt_gmap;
        public HeaderViewHolder(View view) {
            super(view);
            Htxt_date = view.findViewById(R.id.ft_report_datetime_textView_header);
            Htxt_todate = view.findViewById(R.id.ft_report_todatetime_textView_header);
            Htxt_pLtr = view.findViewById(R.id.ft_report_prevltrs_textView_header);
            Htxt_cLtr = view.findViewById(R.id.ft_report_curltrs_textView_header);
            Htxt_tLtr= view.findViewById(R.id.ft_report_theftltrs_textView_header);
            Htxt_odo = view.findViewById(R.id.ft_report_odo_textView_header);
            Htxt_gmap =  view.findViewById(R.id.ac_report_gmap_textView_header);
        }
    }
}
