package com.vamosys.adapter;


import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vamosys.model.Site;
import com.vamosys.vamos.R;

import java.util.List;

public class SiteListAdapterNew extends RecyclerView.Adapter<SiteListAdapterNew.ViewHolder> {

    private List<Site> mSiteList;
    Activity myAct;
//    private OnSiteClickedListener onSiteClickedListener;

    private final OnItemClickListener listener;

    public SiteListAdapterNew(Activity act, List<Site> arrayList, OnItemClickListener listener) {
        mSiteList = arrayList;
        myAct = act;
        this.listener = listener;
    }

//    public interface OnSiteClickedListener {
//        void onClicked(int i);
//
//        void onLongClicked(int i);
//    }

    public interface OnItemClickListener {
        void onClicked(int i);

        void onLongClicked(int i);
    }

    //
//    public void setOnSiteClickedListener(OnSiteClickedListener onSiteClickedListener) {
//        this.onSiteClickedListener = onSiteClickedListener;
//    }

    @Override
    public SiteListAdapterNew.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {

        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.site_list_layout, viewGroup, false);


        final ViewHolder holder = new ViewHolder(view);


        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                final int position = holder.getAdapterPosition();
                System.out.println("Hi view onclick " + position);

                listener.onLongClicked(position);

//                if (SiteListAdapterNew.this.onSiteClickedListener != null) {
//                    System.out.println("Hi view onclick 222 " + i);
//                    SiteListAdapterNew.this.onSiteClickedListener.onLongClicked(i);
//                }
            }
        });
//        return new ViewHolder(view);

        return holder;

    }

    @Override
    public void onBindViewHolder(SiteListAdapterNew.ViewHolder holder, final int position) {


        if (mSiteList.size() > 0 && mSiteList.size() > position) {


            holder.mTxtSiteName.setText(mSiteList.get(position).getSiteName());
            holder.mTxtSiteType.setText(mSiteList.get(position).getSiteType());


            holder.mImgClose.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    System.out.println("Hi img close onclick " + position);
                    listener.onClicked(position);
//                    if (SiteListAdapterNew.this.onSiteClickedListener != null) {
//                        System.out.println("Hi img close onclick 2222 " + position);
//                        SiteListAdapterNew.this.onSiteClickedListener.onClicked(position);
//                    }
                }
            });

        }


    }

    @Override
    public int getItemCount() {
        return mSiteList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTxtSiteName, mTxtSiteType;
        ImageView mImgClose;

        public ViewHolder(View vi) {
            super(vi);

            mTxtSiteName = (TextView) vi.findViewById(R.id.txt_site_name);
            mTxtSiteType = (TextView) vi.findViewById(R.id.txt_site_type);
            mImgClose = (ImageView) vi.findViewById(R.id.img_site_close);

        }
    }

    public void updateList(List<Site> list) {
        mSiteList = list;
        notifyDataSetChanged();
    }

}
