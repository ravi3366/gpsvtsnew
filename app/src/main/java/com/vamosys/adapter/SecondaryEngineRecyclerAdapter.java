package com.vamosys.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vamosys.model.EngineReportDto;
import com.vamosys.utils.Constant;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.List;

public class SecondaryEngineRecyclerAdapter extends RecyclerView.Adapter<SecondaryEngineRecyclerAdapter.ViewHolder> {
    private List<EngineReportDto> mPrimaryEngineReportData;
    //    private ArrayList<AndroidVersion> mFilteredList;
    private Activity myActivity;
    //int width, height;

    public SecondaryEngineRecyclerAdapter(List<EngineReportDto> arrayList, Activity act) {
        mPrimaryEngineReportData = arrayList;
        //  mFilteredList = arrayList;
        myActivity = act;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.secondary_engine_report_list_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        vehicleNameParams.width = Constant.ScreenWidth * 35 / 100;
        vehicleNameParams.height = Constant.ScreenHeight * 11 / 100;
        holder.mTxtStartDateTime.setLayoutParams(vehicleNameParams);
        holder.mTxtStartDateTime.setPadding(Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100, Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100);
        holder.mTxtStartDateTime.setGravity(Gravity.CENTER);

        holder.mTxtEndDateTime.setLayoutParams(vehicleNameParams);
        holder.mTxtEndDateTime.setPadding(Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100, Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100);
        holder.mTxtEndDateTime.setGravity(Gravity.CENTER);


        LinearLayout.LayoutParams text3Params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        text3Params.width = Constant.ScreenWidth * 30 / 100;
        text3Params.height = Constant.ScreenHeight * 11 / 100;

        holder.mTxtStartStaus.setLayoutParams(text3Params);
        //   col3.setWidth(width * 75 / 100);
        holder.mTxtStartStaus.setPadding(Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100, Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100);
        holder.mTxtStartStaus.setGravity(Gravity.CENTER);

        holder.mTxtEndStatus.setLayoutParams(text3Params);
        //   col3.setWidth(width * 75 / 100);
        holder.mTxtEndStatus.setPadding(Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100, Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100);
        holder.mTxtEndStatus.setGravity(Gravity.CENTER);

        holder.mTxtDuration.setLayoutParams(text3Params);
        //  col3.setWidth(width * 75 / 100);
        holder.mTxtDuration.setPadding(Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100, Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100);
        holder.mTxtDuration.setGravity(Gravity.CENTER);

        holder.mTxtStartMap.setLayoutParams(text3Params);
        //   col3.setWidth(width * 75 / 100);
        holder.mTxtStartMap.setPadding(Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100, Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100);
        holder.mTxtStartMap.setGravity(Gravity.CENTER);

        holder.mTxtEndMap.setLayoutParams(text3Params);
        //   col3.setWidth(width * 75 / 100);
        holder.mTxtEndMap.setPadding(Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100, Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100);
        holder.mTxtEndMap.setGravity(Gravity.CENTER);


        LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        text2Params.width = Constant.ScreenWidth * 60 / 100;
        text2Params.height = Constant.ScreenHeight * 11 / 100;
        holder.mTxtStartAdds.setLayoutParams(text2Params);
        holder.mTxtStartAdds.setPadding(Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100, Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100);
        holder.mTxtStartAdds.setGravity(Gravity.CENTER | Gravity.LEFT);

        holder.mTxtEndAdds.setLayoutParams(text2Params);
        holder.mTxtEndAdds.setPadding(Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100, Constant.ScreenWidth * 2 / 100, Constant.ScreenHeight * (int) 0.5 / 100);
        holder.mTxtEndAdds.setGravity(Gravity.CENTER | Gravity.LEFT);

//        viewHolder.tv_datetime.setText(Const.getAcReportTime(mArrayList.get(i).getTimeStamp()));
//
//        if (mArrayList.get(i).getAddress() != null && !mArrayList.get(i).getAddress().equalsIgnoreCase("null")) {
//            viewHolder.tv_address.setVisibility(View.VISIBLE);
//            viewHolder.tv_address.setText(mArrayList.get(i).getAddress());
//        } else {
//            viewHolder.tv_address.setVisibility(View.GONE);
//        }

//        Glide.with(myActivity).load(Const.API_URL + mArrayList.get(i).getImageName()).into(viewHolder.mImg_Camera);
//        Glide.with(myActivity).load(Const.CAMERA_IMAGE_IP + mArrayList.get(i).getImageName()).into(viewHolder.mImg_Camera);


        if (mPrimaryEngineReportData.get(position).getEndAddress() != null) {
            holder.mTxtEndDateTime.setVisibility(View.VISIBLE);
            holder.mTxtEndStatus.setVisibility(View.VISIBLE);
            holder.mTxtEndAdds.setVisibility(View.VISIBLE);

            holder.view1.setVisibility(View.VISIBLE);
            holder.view2.setVisibility(View.VISIBLE);
            holder.view3.setVisibility(View.VISIBLE);
//                view4.setVisibility(View.VISIBLE);
        } else {
            holder.mTxtEndDateTime.setVisibility(View.GONE);
            holder.mTxtEndStatus.setVisibility(View.GONE);
            holder.mTxtEndAdds.setVisibility(View.GONE);

            holder.view1.setVisibility(View.GONE);
            holder.view2.setVisibility(View.GONE);
            holder.view3.setVisibility(View.GONE);
        }

        if (mPrimaryEngineReportData.get(position).getEndLatitude() != null && mPrimaryEngineReportData.get(position).getEndLongitude() != null) {
            holder.mTxtEndMap.setVisibility(View.VISIBLE);
            holder.view4.setVisibility(View.VISIBLE);
        } else {
            holder.mTxtEndMap.setVisibility(View.GONE);
            holder.view4.setVisibility(View.GONE);
        }

        EngineReportDto data = mPrimaryEngineReportData.get(position);

        holder.mTxtStartDateTime.setText(Const.getAcReportTime((data.getStartTime())));
        holder.mTxtEndDateTime.setText(Const.getAcReportTime((data.getEndTime())));
        holder.mTxtStartStaus.setText(data.getStartStatus());
        holder.mTxtEndStatus.setText(data.getEndStatus());

        long timevalue = 0;
        if (data.getEndTime() > 0 && data.getStartTime() > 0) {
            if (data.getEndTime() > data.getStartTime()) {
                timevalue = data.getEndTime() - data.getStartTime();
            }
        } else {
            timevalue = System.currentTimeMillis() - data.getStartTime();
        }
        holder.mTxtDuration.setText(Const.getreportTimeValue(timevalue));

        holder.mTxtStartAdds.setText(data.getStartingAddress());
        holder.mTxtEndAdds.setText(data.getEndAddress());


        SpannableString content = new SpannableString("G-Map");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        holder.mTxtStartMap.setText(content);
        holder.mTxtEndMap.setText(content);

        holder.mTxtStartMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                MenuItem item = menu.findItem(R.id.action_map_close);
//                item.setVisible(true);
//
//                //  mImgMapViewClose.setVisibility(View.VISIBLE);
//                mAcReportDataMapLayout.setVisibility(View.VISIBLE);
////                    if (isMapPresent) {
//                isMapPresent = true;
//                mMainScrollView.setVisibility(View.GONE);
//                lv.setVisibility(View.GONE);
////                    }
//
////                    lv.setVisibility(View.GONE);
//                mLatitude = Double.valueOf(mPrimaryEngineReportData.get(position).getStartLatitude());
//                mLongitude = Double.valueOf(mPrimaryEngineReportData.get(position).getStartLongitude());
////                    setupMap(mAcReportData.get(position).getStartingAddress());
//
//                if (isOsmEnabled) {
//                    setupOsmMap(mPrimaryEngineReportData.get(position).getStartingAddress());
//                } else {
//                    setupGoogleMap(mPrimaryEngineReportData.get(position).getStartingAddress());
//                }

            }
        });

        holder.mTxtEndMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    mImgMapViewClose.setVisibility(View.VISIBLE);

//                MenuItem item = menu.findItem(R.id.action_map_close);
//                item.setVisible(true);
//
//                mAcReportDataMapLayout.setVisibility(View.VISIBLE);
////                    if (isMapPresent) {
//                isMapPresent = true;
//                lv.setVisibility(View.GONE);
//                mMainScrollView.setVisibility(View.GONE);
////                    }
//
////                    lv.setVisibility(View.GONE);
//                mLatitude = Double.valueOf(mPrimaryEngineReportData.get(position).getEndLatitude());
//                mLongitude = Double.valueOf(mPrimaryEngineReportData.get(position).getEndLongitude());
//
//                if (isOsmEnabled) {
//                    setupOsmMap(mPrimaryEngineReportData.get(position).getEndAddress());
//                } else {
//                    setupGoogleMap(mPrimaryEngineReportData.get(position).getEndAddress());
//                }

//                    setupMap(mAcReportData.get(position).getEndAddress());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mPrimaryEngineReportData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        //  private TextView tv_name, tv_version, tv_api_level;
//        private TextView tv_datetime, tv_address;
//        private ImageView mImg_Camera;

        private TextView mTxtStartDateTime = null, mTxtEndDateTime = null, mTxtStartMap, mTxtEndMap, mTxtStartAdds, mTxtEndAdds, mTxtDuration, mTxtStartStaus, mTxtEndStatus;
        private View view1, view2, view3, view4;

        public ViewHolder(View vi) {
            super(vi);

            mTxtStartDateTime = (TextView) vi.findViewById(R.id.engine_report_datetime_textView);
            mTxtEndDateTime = (TextView) vi.findViewById(R.id.engine_report_end_datetime_textView);
            mTxtStartStaus = (TextView) vi.findViewById(R.id.engine_report_status_textView);
            mTxtEndStatus = (TextView) vi.findViewById(R.id.engine_report_end_status_textView);
            mTxtDuration = (TextView) vi.findViewById(R.id.engine_report_duration_textView);
            mTxtStartAdds = (TextView) vi.findViewById(R.id.engine_report_nearest_location_textView);
            mTxtEndAdds = (TextView) vi.findViewById(R.id.engine_report_nearest_end_location_textView);
            mTxtStartMap = (TextView) vi.findViewById(R.id.engine_report_gmap_textView);
            mTxtEndMap = (TextView) vi.findViewById(R.id.engine_report_end_gmap_textView);


            view1 = (View) vi.findViewById(R.id.view1);
             view2 = (View) vi.findViewById(R.id.view2);
             view3 = (View) vi.findViewById(R.id.view3);
             view4 = (View) vi.findViewById(R.id.view4);


        }
    }

}