package com.vamosys.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vamosys.model.NotificationFilterDto;
import com.vamosys.utils.Constant;
import com.vamosys.vamos.R;

import java.util.List;

public class NotificationFilterAdapter extends BaseAdapter {

    private List<NotificationFilterDto> mFilterList;
    private LayoutInflater inflater;
    int height, width;
    Activity myacActivity;

    public NotificationFilterAdapter(Activity context, List<NotificationFilterDto> mList) {
        this.mFilterList = mList;
        myacActivity = context;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mFilterList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mFilterList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.notification_filter_adapter_layout, null);
            holder = new ViewHolder();
            holder.filterText = (TextView) vi
                    .findViewById(R.id.adap_noti_filter_txtview);
            holder.$Check = (CheckBox) vi.findViewById(R.id.adap_noti_filter_checkBox);

            height = Constant.ScreenHeight;
            width = Constant.ScreenWidth;

            LinearLayout.LayoutParams txtname = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            txtname.width = width * 80 / 100;
            txtname.gravity = Gravity.CENTER;
            txtname.topMargin = height * 1 / 100;
            txtname.bottomMargin = height * 1 / 100;
            txtname.leftMargin = width * 4 / 100;
            holder.filterText.setLayoutParams(txtname);
            holder.filterText.setGravity(Gravity.CENTER | Gravity.LEFT);

            LinearLayout.LayoutParams txtCheck = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            txtCheck.height = height * 9 / 100;
            txtCheck.width = width * 14 / 100;
            txtCheck.gravity = Gravity.CENTER;
            txtCheck.topMargin = height * 1 / 100;
            txtCheck.bottomMargin = height * 1 / 100;
            txtCheck.leftMargin = width * 1 / 100;
            holder.$Check.setLayoutParams(txtCheck);
            holder.$Check.setGravity(Gravity.CENTER);

            if (width >= 600) {
                holder.filterText.setTextSize(16);

            } else if (width > 501 && width < 600) {
                holder.filterText.setTextSize(15);

            } else if (width > 260 && width < 500) {
                holder.filterText.setTextSize(14);

            } else if (width <= 260) {
                holder.filterText.setTextSize(13);

            }

            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }


        holder.filterText.setText(mFilterList.get(position).getFilterName()
                .toString().trim());


        holder.$Check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final boolean isChecked = holder.$Check.isChecked();
                // Do something here.


                NotificationFilterDto n = new NotificationFilterDto();
                n.setFilterName(mFilterList.get(position).getFilterName().trim());
                n.setSelected(isChecked);

                insertData(n, isChecked);
            }
        });
        holder.$Check.setChecked(Constant.mNotificationFilterList.get(position)
                .isSelected());

        return vi;
    }

    public static class ViewHolder {
        public TextView filterText;
        public CheckBox $Check;

    }

    public void insertData(NotificationFilterDto a, boolean isChecked) {

        for (int i = 0; i < Constant.mNotificationFilterList.size(); i++) {
            if (Constant.mNotificationFilterList.get(i).getFilterName().trim().equalsIgnoreCase(a
                    .getFilterName().trim())) {
                Constant.mNotificationFilterList.set(i, a);
            }
        }

    }
}
