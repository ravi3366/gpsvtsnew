package com.vamosys.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vamosys.model.Site;
import com.vamosys.utils.Constant;
import com.vamosys.utils.TypefaceUtil;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.List;

public class GeoFencesAdapter extends BaseAdapter {

    Context mContext;
    int layoutResourceId;
    List<Site> mSiteList = new ArrayList<Site>();
    private static LayoutInflater inflater = null;
    Activity myacActivity;
    int width, height;
    ViewHolder holder;

    public GeoFencesAdapter(Activity act, List<Site> mList) {

        myacActivity = act;
        mSiteList = mList;
        inflater = (LayoutInflater) myacActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mSiteList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mSiteList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;

        convertView = null;
        if (convertView == null) {

            vi = inflater.inflate(R.layout.geo_fences_adapter, null);
            holder = new ViewHolder();
            holder.mTxtSiteName = (TextView) vi.findViewById(R.id.txt_site_name);
            holder.mTxtSiteNameValue = (TextView) vi
                    .findViewById(R.id.txt_site_name_value);
            holder.mTxtSiteType = (TextView) vi.findViewById(R.id.txt_site_type);
            holder.mTxtSiteTypeValue = (TextView) vi
                    .findViewById(R.id.txt_site_type_value);


            holder.mColon1 = (TextView) vi.findViewById(R.id.txt_site_name_colon);
            holder.mColon2 = (TextView) vi
                    .findViewById(R.id.txt_site_type_colon);

            holder.mTxtSiteName.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.mTxtSiteNameValue.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.mTxtSiteType.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.mTxtSiteTypeValue.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));

            screenArrange();
            vi.setTag(holder);
            holder.mTxtSiteNameValue.setText(String.valueOf(mSiteList.get(
                    position).getSiteName()));
            holder.mTxtSiteTypeValue.setText(String.valueOf(mSiteList.get(
                    position).getSiteType()));


        } else {
            holder = (ViewHolder) vi.getTag();
        }
        /*
         * if the doctor is already present in the dcp list.then it should not
		 * again checked
		 */
        return vi;
    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        width = Constant.ScreenWidth;
        height = Constant.ScreenHeight;

//        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        headTxtParams.width = width * 95 / 100;
//        headTxtParams.height = height * 6 / 100;
//        headTxtParams.topMargin = height * 1 / 100;
//        headTxtParams.gravity = Gravity.CENTER;
//        holder.mTxtAdapHeader.setLayoutParams(headTxtParams);
//        holder.mTxtAdapHeader.setPadding(width * 2 / 100, 0, 0, 0);
//        holder.mTxtAdapHeader.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams dayParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        dayParams.width = width * 30 / 100;
        dayParams.height = height * 6 / 100;
        dayParams.topMargin = height * 1 / 100;
        dayParams.gravity = Gravity.CENTER;
        holder.mTxtSiteName.setLayoutParams(dayParams);
        holder.mTxtSiteType.setLayoutParams(dayParams);
//        holder.mTxtMonth.setLayoutParams(dayParams);
//        holder.mTxtWeek.setLayoutParams(dayParams);

        holder.mTxtSiteName.setPadding(width * 2 / 100, 0, 0, 0);
        holder.mTxtSiteType.setPadding(width * 2 / 100, 0, 0, 0);
       // holder.mTxtMonth.setPadding(width * 2 / 100, 0, 0, 0);

        holder.mTxtSiteName.setGravity(Gravity.CENTER | Gravity.LEFT);
        holder.mTxtSiteType.setGravity(Gravity.CENTER | Gravity.LEFT);
       // holder.mTxtMonth.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams headTxtColonParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtColonParams.height = height * 6 / 100;
        headTxtColonParams.topMargin = height * 1 / 100;
        headTxtColonParams.gravity = Gravity.CENTER;
        holder.mColon1.setLayoutParams(headTxtColonParams);
        holder.mColon2.setLayoutParams(headTxtColonParams);
//        holder.mColon3.setLayoutParams(headTxtColonParams);
//        holder.mColon4.setLayoutParams(headTxtColonParams);

        holder.mColon1.setGravity(Gravity.CENTER | Gravity.LEFT);
        holder.mColon2.setGravity(Gravity.CENTER | Gravity.LEFT);
//        holder.mColon3.setGravity(Gravity.CENTER | Gravity.LEFT);
//        holder.mColon4.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams dayValueParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        dayValueParams.height = height * 6 / 100;
        dayValueParams.topMargin = height * 1 / 100;
        dayValueParams.gravity = Gravity.CENTER;
        holder.mTxtSiteNameValue.setLayoutParams(dayValueParams);
        holder.mTxtSiteTypeValue.setLayoutParams(dayValueParams);
//        holder.mTxtYesterdayValue.setLayoutParams(dayValueParams);
//        holder.mTxtWeekValue.setLayoutParams(dayValueParams);

        holder.mTxtSiteTypeValue.setPadding(width * 2 / 100, 0, 0, 0);
        holder.mTxtSiteNameValue.setPadding(width * 2 / 100, 0, 0, 0);
//        holder.mTxtYesterdayValue.setPadding(width * 2 / 100, 0, 0, 0);
//        holder.mTxtWeekValue.setPadding(width * 2 / 100, 0, 0, 0);

        holder.mTxtSiteTypeValue.setGravity(Gravity.CENTER | Gravity.LEFT);
        holder.mTxtSiteNameValue.setGravity(Gravity.CENTER | Gravity.LEFT);
//        holder.mTxtYesterdayValue.setGravity(Gravity.CENTER | Gravity.LEFT);
//        holder.mTxtWeekValue.setGravity(Gravity.CENTER | Gravity.LEFT);

        if (width >= 600) {
            holder.mTxtSiteTypeValue.setTextSize(16);
            holder.mTxtSiteType.setTextSize(16);
            holder.mTxtSiteNameValue.setTextSize(16);
            holder.mTxtSiteName.setTextSize(16);

            holder.mColon1.setTextSize(16);
            holder.mColon2.setTextSize(16);


        } else if (width > 501 && width < 600) {
            holder.mTxtSiteTypeValue.setTextSize(15);
            holder.mTxtSiteType.setTextSize(15);
            holder.mTxtSiteNameValue.setTextSize(15);
            holder.mTxtSiteName.setTextSize(15);

            holder.mColon1.setTextSize(15);
            holder.mColon2.setTextSize(15);

        } else if (width > 260 && width < 500) {
            holder.mTxtSiteTypeValue.setTextSize(14);
            holder.mTxtSiteType.setTextSize(14);
            holder.mTxtSiteNameValue.setTextSize(14);
            holder.mTxtSiteName.setTextSize(14);

            holder.mColon1.setTextSize(14);
            holder.mColon2.setTextSize(14);

        } else if (width <= 260) {
            holder.mTxtSiteTypeValue.setTextSize(13);
            holder.mTxtSiteType.setTextSize(13);
            holder.mTxtSiteNameValue.setTextSize(13);
            holder.mTxtSiteName.setTextSize(13);

            holder.mColon1.setTextSize(13);
            holder.mColon2.setTextSize(13);

        }
    }

    public static class ViewHolder {
        public TextView mTxtSiteName, mTxtSiteNameValue, mTxtSiteType,
                mTxtSiteTypeValue,
                mColon1, mColon2;
        //public TextView mTxtWeek,mTxtWeekValue,mColon4;
    }
}