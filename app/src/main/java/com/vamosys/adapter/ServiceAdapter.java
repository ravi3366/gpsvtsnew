package com.vamosys.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vamosys.model.FmsDto;
import com.vamosys.vamos.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {
    private List<FmsDto> mArrayList;
    private Activity myActivity;
   String date,date_after;
    Date currentdate;
    String Tagg="check serivces";

    public ServiceAdapter(List<FmsDto> arrayList, Activity act) {
        mArrayList = arrayList;
        myActivity = act;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.service_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Log.d("serviceodometer",""+mArrayList.get(i).getOdometerDue());
        Log.d("servicenotify",""+mArrayList.get(i).getNotifyInterval());
       if (mArrayList.get(i).getOdometerDue().equalsIgnoreCase("null")){

           Log.d("serviceadapter","success");
           Log.d(Tagg,"odonull");

       }
       else{
           Log.d("serviceadapter","failure");
           Log.d(Tagg,"odo not null");
           Log.d(Tagg,"documenttype: "+mArrayList.get(i).getDocumentType());
           Log.d(Tagg,"duedate: "+mArrayList.get(i).getDueDate());
           Log.d(Tagg,"amount: "+mArrayList.get(i).getAmount());
           Log.d(Tagg,"company name: "+mArrayList.get(i).getCompanyName());
           viewHolder.tv_document_type.setText(mArrayList.get(i).getDocumentType());
           // viewHolder.tv_due_date.setText(mArrayList.get(i).getDueDate());
           date = mArrayList.get(i).getDueDate();
           date_after = formateDateFromstring("yyyy-MM-dd","dd-MM-yyyy", date);
           viewHolder.tv_due_date.setText(date_after);

           // viewHolder.tv_due_date.setText(Constant.currentdate);
           viewHolder.tv_amount.setText(mArrayList.get(i).getAmount()+".00");
           viewHolder.tv_company.setText(mArrayList.get(i).getCompanyName());
           viewHolder.tv_description.setText(mArrayList.get(i).getDescription());
           if(mArrayList.get(i).getDocumentType().contains("OTHERS")) {
               viewHolder.odometer.setVisibility(View.GONE);
               viewHolder.overdue.setVisibility(View.GONE);
           }
           else{
               viewHolder.odometer.setText(mArrayList.get(i).getOdometerDue() + " Kms");
           }
       }


    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        //  private TextView tv_name, tv_version, tv_api_level;
        private TextView tv_document_type, tv_due_date, tv_amount, tv_company, tv_description,odometer,overdue;
        public RelativeLayout viewBackground;
        public LinearLayout viewForeground;

        public ViewHolder(View view) {
            super(view);

            tv_document_type = (TextView) view.findViewById(R.id.txt_document_type);
            tv_due_date = (TextView) view.findViewById(R.id.txt_date);
            tv_amount = (TextView) view.findViewById(R.id.txt_amount);
            tv_company = (TextView) view.findViewById(R.id.txt_company_name);
            tv_description = (TextView) view.findViewById(R.id.txt_description);
            odometer = (TextView) view.findViewById(R.id.over_due);
            overdue = (TextView)view.findViewById(R.id.overdueTxt);
           viewBackground = (RelativeLayout)view.findViewById(R.id.view_background);
            viewForeground = (LinearLayout)view.findViewById(R.id.view_foreground);

        }
    }
    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {

        }

        return outputDate;

    }

}