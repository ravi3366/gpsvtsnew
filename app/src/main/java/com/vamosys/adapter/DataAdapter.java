package com.vamosys.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vamosys.model.CameraReportDto;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private List<CameraReportDto> mArrayList;
    //    private ArrayList<AndroidVersion> mFilteredList;
    private Activity myActivity;

    public DataAdapter(List<CameraReportDto> arrayList, Activity act) {
        mArrayList = arrayList;
        //  mFilteredList = arrayList;
        myActivity = act;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {


        viewHolder.tv_datetime.setText(Const.getAcReportTime(mArrayList.get(i).getTimeStamp()));

        if (mArrayList.get(i).getAddress() != null && !mArrayList.get(i).getAddress().equalsIgnoreCase("null")) {
            viewHolder.tv_address.setVisibility(View.VISIBLE);
            viewHolder.tv_address.setText(mArrayList.get(i).getAddress());
        } else {
            viewHolder.tv_address.setVisibility(View.GONE);
        }

//        Glide.with(myActivity).load(Const.API_URL + mArrayList.get(i).getImageName()).into(viewHolder.mImg_Camera);
        Glide.with(myActivity).load(Const.CAMERA_IMAGE_IP + mArrayList.get(i).getImageName()).into(viewHolder.mImg_Camera);

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        //  private TextView tv_name, tv_version, tv_api_level;
        private TextView tv_datetime, tv_address;
        private ImageView mImg_Camera;

        public ViewHolder(View view) {
            super(view);


            tv_datetime = (TextView) view.findViewById(R.id.txt_camera_adap_datetime);
            tv_address = (TextView) view.findViewById(R.id.txt_camera_adap_address);
            mImg_Camera = (ImageView) view.findViewById(R.id.img_camera_adapter);


        }
    }

}