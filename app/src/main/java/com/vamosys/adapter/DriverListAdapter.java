package com.vamosys.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vamosys.model.VehicleData;
import com.vamosys.utils.Constant;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.List;

public class DriverListAdapter extends BaseAdapter {

    Context mContext;
    int layoutResourceId;
    List<VehicleData> mVehicleLocationList = new ArrayList<VehicleData>();
    private static LayoutInflater inflater = null;
    Activity myacActivity;
    int width, height;
    ViewHolder holder;

    public DriverListAdapter(Activity act, List<VehicleData> mList) {

        myacActivity = act;
        mVehicleLocationList = mList;
        inflater = (LayoutInflater) myacActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mVehicleLocationList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mVehicleLocationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;

        convertView = null;
        if (convertView == null) {

            vi = inflater.inflate(R.layout.driver_list_adapter, null);
            holder = new ViewHolder();
            holder.mHeaderTxt = (TextView) vi.findViewById(R.id.driver_list_header_text);
            holder.mDriverNameTxt = (TextView) vi
                    .findViewById(R.id.driver_name_text);
            holder.mMobileNoTxt = (TextView) vi.findViewById(R.id.driver_mobile_no_text);

            holder.mDriverIcon = (ImageView) vi
                    .findViewById(R.id.driver_adap_icon);
            holder.mCallIcon = (ImageView) vi.findViewById(R.id.call_adap_icon);


            screenArrange();
            vi.setTag(holder);

//            holder.mTxtWeekValue.setText(String.valueOf(mKmsSummaryList
//                    .get(position).getDistanceWeek()));
//            holder.mTxtAdapHeader.setText(mKmsSummaryList.get(position)
//                    .getVehicleId());

        } else {
            holder = (ViewHolder) vi.getTag();
        }
        /*
         * if the doctor is already present in the dcp list.then it should not
		 * again checked
		 */

        holder.mHeaderTxt.setText(String.valueOf(mVehicleLocationList.get(
                position).getShortName()));
        holder.mDriverNameTxt.setText(String.valueOf(mVehicleLocationList.get(
                position).getDriverName()));
        holder.mMobileNoTxt.setText(String.valueOf(mVehicleLocationList
                .get(position).getDriverMobile()));

        return vi;
    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        width = Constant.ScreenWidth;
        height = Constant.ScreenHeight;

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 95 / 100;
        headTxtParams.height = height * 6 / 100;
        headTxtParams.topMargin = height * 1 / 100;
        headTxtParams.gravity = Gravity.CENTER;
        holder.mHeaderTxt.setLayoutParams(headTxtParams);
        holder.mHeaderTxt.setPadding(width * 2 / 100, 0, 0, 0);
        holder.mHeaderTxt.setGravity(Gravity.CENTER | Gravity.LEFT);


        LinearLayout.LayoutParams driverCallIconParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        driverCallIconParams.width = height * 7 / 100;
        driverCallIconParams.height = height * 7 / 100;
        //driverCallIconParams.topMargin = height * 1 / 100;
        driverCallIconParams.rightMargin = width * 1 / 100;
        driverCallIconParams.leftMargin = width * 1 / 100;
        driverCallIconParams.gravity = Gravity.CENTER;
        holder.mDriverIcon.setLayoutParams(driverCallIconParams);
        holder.mCallIcon.setLayoutParams(driverCallIconParams);


        LinearLayout.LayoutParams dayParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        dayParams.width = width * 90 / 100;
        dayParams.height = height * 6 / 100;
       // dayParams.topMargin = height * 1 / 100;
        driverCallIconParams.rightMargin = width * 1 / 100;
        driverCallIconParams.leftMargin = width * 1 / 100;
        dayParams.gravity = Gravity.CENTER;
        holder.mDriverNameTxt.setLayoutParams(dayParams);
        holder.mMobileNoTxt.setLayoutParams(dayParams);
//        holder.mTxtMonth.setLayoutParams(dayParams);
//        holder.mTxtWeek.setLayoutParams(dayParams);

        holder.mDriverNameTxt.setPadding(width * 2 / 100, 0, 0, 0);
        holder.mMobileNoTxt.setPadding(width * 2 / 100, 0, 0, 0);
//        holder.mTxtMonth.setPadding(width * 2 / 100, 0, 0, 0);
//        holder.mTxtWeek.setPadding(width * 2 / 100, 0, 0, 0);

        holder.mDriverNameTxt.setGravity(Gravity.CENTER | Gravity.LEFT);
        holder.mMobileNoTxt.setGravity(Gravity.CENTER | Gravity.LEFT);


        if (width >= 600) {
            holder.mHeaderTxt.setTextSize(16);
            holder.mDriverNameTxt.setTextSize(16);
            holder.mMobileNoTxt.setTextSize(16);


        } else if (width > 501 && width < 600) {
            holder.mHeaderTxt.setTextSize(15);
            holder.mDriverNameTxt.setTextSize(15);
            holder.mMobileNoTxt.setTextSize(15);

        } else if (width > 260 && width < 500) {
            holder.mHeaderTxt.setTextSize(14);
            holder.mDriverNameTxt.setTextSize(14);
            holder.mMobileNoTxt.setTextSize(14);


        } else if (width <= 260) {
            holder.mHeaderTxt.setTextSize(13);
            holder.mDriverNameTxt.setTextSize(13);
            holder.mMobileNoTxt.setTextSize(13);

        }
    }

    public static class ViewHolder {
        //        public TextView mTxtToday, mTxtTodayValue, mTxtYesterday,
//                mTxtYesterdayValue, mTxtMonth, mTxtMonthValue, mTxtAdapHeader,
//                mColon1, mColon2, mColon3;
        public TextView mHeaderTxt, mDriverNameTxt, mMobileNoTxt;
        public ImageView mDriverIcon, mCallIcon;
    }
}