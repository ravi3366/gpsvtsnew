package com.vamosys.interfaces;

import com.vamosys.model.FuelSumReportDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
//    http://209.97.163.4:9000/getConsolidatedFuelReport?userId=DHARIKAZEEM&groupName=DHARIKAZEEM:VAM&date=2020-09-04
    @GET("getConsolidatedFuelReport")
    Call<List<FuelSumReportDto>> getConsolidatedFuelReport(
          @Query("userId") String userId,
          @Query("groupName") String groupName,
          @Query("date") String date
    );
}
