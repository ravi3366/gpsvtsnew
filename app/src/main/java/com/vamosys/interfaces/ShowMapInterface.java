package com.vamosys.interfaces;

public interface ShowMapInterface {
    void showLatLng(double lat, double lng);
}
