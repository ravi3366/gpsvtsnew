package com.vamosys.imageLoader;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

import com.vamosys.vamos.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ImageLoader {

    MemoryCache memoryCache = new MemoryCache();
    FileCache fileCache;
    Context con;
    private Map<ImageView, String> imageViews = Collections
            .synchronizedMap(new WeakHashMap<ImageView, String>());
    ExecutorService executorService;
    static SharedPreferences pref;

    public ImageLoader(Context context) {
        pref = PreferenceManager.getDefaultSharedPreferences(context);
        con = context;
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(5);
    }


    final int stub_id = R.drawable.bg_logo1;

    public void DisplayImage(String url, ImageView imageView) {

        System.out.println("the url for slider image is ::::" + url);

        imageViews.put(imageView, url);
        Bitmap bitmap = memoryCache.get(url);
        if (bitmap != null) {
            System.out.println("bitmap111 is :::::::::::::::::::" + bitmap);
            imageView.setImageBitmap(bitmap);
            // setImage(imageView, bitmap);
            // imageView.setImageResource(stub_id);
        } else {
            System.out.println("bitmap2222 is :elseee::::::::::::::::::");
            queuePhoto(url, imageView);
            imageView.setImageResource(stub_id);
        }
    }

    public void setImage(ImageView view1, Bitmap img_byte) {
        ImageView view = view1;
        //	Constant.prtMsg("The image view is :::::::" + view);

        Bitmap bitmapOrg = img_byte;

        int width = bitmapOrg.getWidth();
        int height = bitmapOrg.getHeight();
        int newHeight = view.getLayoutParams().height;
        int newWidth = view.getLayoutParams().width;
        // int newWidth = 300;
        // int newHeight = 470;

        // calculate the scale - in this case = 0.4f
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        // createa matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // rotate the Bitmap
        // matrix.postRotate(45);

        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width,
                height, matrix, true);

        // make a Drawable from Bitmap to allow to set the BitMap
        // to the ImageView, ImageButton or what ever
        BitmapDrawable bmd = new BitmapDrawable(resizedBitmap);

        // ImageView imageView = new ImageView(this);

        // set the Drawable on the ImageView
        view.setImageDrawable(bmd);

        // center the Image
        view.setScaleType(ScaleType.CENTER);

        // Now change ImageView's dimensions to match the scaled image
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view
                .getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);

    }

    public void DisplayImage1(String url, String f) {
        // imageViews.put(imageView, url);
        Bitmap bitmap = memoryCache.get1(url);
        //	Constant.prtMsg("bitmap is :::::::::::::::::::" + bitmap);
        if (bitmap != null) {
            // imageView.setImageBitmap(bitmap);
            if (f.equalsIgnoreCase("home")) {
                // should update this bitmap into shared pref before that pls
                // convert bitmap into string

                // ByteArrayOutputStream baos = new ByteArrayOutputStream();
                // bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                // byte[] b = baos.toByteArray();
                // String temp = Base64.encodeToString(b, Base64.DEFAULT);
                // updateHomeDetails(temp);
            }
        }
    }

    private void queuePhoto(String url, ImageView imageView) {
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    private Bitmap getBitmap(String url) {
        File f = fileCache.getFile(url);

        // from SD cache
        Bitmap b = decodeFile(f);
        if (b != null)
            return b;

        // from web
        try {
            Bitmap bitmap = null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            bitmap = decodeFile(f);
            return bitmap;
        } catch (Throwable ex) {
            ex.printStackTrace();
            if (ex instanceof OutOfMemoryError)
                memoryCache.clear();
            return null;
        }
    }

    // decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f) {
        try {
            // decode image size

            //	CommonManager co=new CommonManager(con);

            //BitmapFactory.Options o = co.getBitmap();

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 200;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
//				if (width_tmp / 2 < REQUIRED_SIZE
//						|| height_tmp / 2 < REQUIRED_SIZE)
//					break;
//				width_tmp /= 2;
//				height_tmp /= 2;
//				scale *= 2;

                if (width_tmp < REQUIRED_SIZE
                        || height_tmp < REQUIRED_SIZE)
                    break;
                width_tmp = width_tmp;
                height_tmp = height_tmp;
                scale *= 1;

            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();

//			if (scale > 3) {
//				scale = 3;
//			}

            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    // Task for the queue
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            Bitmap bmp = getBitmap(photoToLoad.url);
            memoryCache.put(photoToLoad.url, bmp);
            if (imageViewReused(photoToLoad))
                return;
            BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
            Activity a = (Activity) photoToLoad.imageView.getContext();
            a.runOnUiThread(bd);
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            if (bitmap != null)
                photoToLoad.imageView.setImageBitmap(bitmap);
            else
                photoToLoad.imageView.setImageResource(stub_id);
        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    public static void updateHomeDetails(String bit) {
        //Constant.prtMsg("pref update called in image loader ::::::::");
        Editor e = pref.edit();
        e.putString("image", bit);

        e.commit();
    }

}
