package com.vamosys.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.text.TextUtils;

import com.vamosys.app.MyApplication;
import com.vamosys.vamos.R;

import java.util.Arrays;
import java.util.List;


public class NotificationUtils {

    private static String TAG = NotificationUtils.class.getSimpleName();

    private Context mContext;

    public NotificationUtils() {
    }

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }

    public void showNotificationMessage(String title, String message, Intent intent) {
        showNotificationContent(title, message, intent);
    }

    public void showNotificationContent(final String title, final String message, Intent intent) {
        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;


        // notification icon
        final int icon = R.mipmap.ic_launcher;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mContext,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext);

//        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                + "://" + mContext.getPackageName() + "/raw/notification");

//        if (!TextUtils.isEmpty(imageUrl)) {
//
//            if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {
//
//                Bitmap bitmap = getBitmapFromURL(imageUrl);
//
//                if (bitmap != null) {
//                    showBigNotification(bitmap, mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
//                } else {
//                    showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
//                }
//            }
//        } else {
        showSmallNotification(mBuilder, icon, title, message, resultPendingIntent);
//            playNotificationSound();
//        }
    }


    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, PendingIntent resultPendingIntent) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

//        if (Config.appendNotificationMessages) {
        // store the notification in shared pref first
        MyApplication.getInstance().getPrefManager().addNotification(message);

        // get the notifications from shared preferences
        String oldNotification = MyApplication.getInstance().getPrefManager().getNotifications();

        List<String> messages = Arrays.asList(oldNotification.split("\\|"));
        //   int msgSize = messages.size();


        for (int i = messages.size() - 1; i >= 0; i--) {
            //  if (i < 7) {
            inboxStyle.addLine(messages.get(i));
            //}
        }

        if (messages.size() > 7) {
            inboxStyle.setSummaryText(String.valueOf(messages.size() - 7) + " more Notifications");
        }
//        } else {
//            inboxStyle.addLine(message);
//        }

            try {

                NotificationManager notificationManager;
              //  NotificationCompat.Builder mBuilder;
                String NOTIFICATION_CHANNEL_ID = "10001";



//                Intent intent = new Intent(context, NotificationListActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
//                        PendingIntent.FLAG_ONE_SHOT);
//                NotificationViewerActivity.mMsgContent = message;
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                mBuilder = new NotificationCompat.Builder(mContext)
//                        .setSmallIcon(R.mipmap.ic_launcher_fleetpoint)
//                        .setContentTitle(context.getResources().getString(R.string.app_name))
//                        .setContentText(message)
//                        .setAutoCancel(true)
//                        .setSound(defaultSoundUri)
//                        .setPriority(NotificationCompat.PRIORITY_HIGH)
//                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//                        .setChannelId(NOTIFICATION_CHANNEL_ID)
//                        .setContentIntent(pendingIntent);

                                .setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(defaultSoundUri)
                .setStyle(inboxStyle)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setChannelId(NOTIFICATION_CHANNEL_ID)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(messages.size() + " more Notifications");

//         notificationManager =
//                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(100 /* ID of notification */, mBuilder.build());




                notificationManager =
                        (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
                    notificationChannel.enableLights(true);
                    notificationChannel.setLightColor(Color.RED);
                    notificationChannel.enableVibration(true);
                    notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    assert notificationManager != null;
                    mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
                    notificationManager.createNotificationChannel(notificationChannel);
                }
                //   notificationManager.createNotificationChannel(mChannel);
//            (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//                notificationManager.createNotificationChannel(mChannel);
//            }
                // assert notificationManager != null;
                notificationManager.notify(100 /* ID of notification */, mBuilder.build());
               // notificationManager.notify(0 /* ID of notification */, mBuilder.build());

            } catch (Exception e) {
                e.printStackTrace();
            }




//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
//                .setSmallIcon(icon).setTicker(title).setWhen(0)
//                .setAutoCancel(true)
//                .setContentTitle(title)
//                .setContentIntent(resultPendingIntent)
//                .setSound(defaultSoundUri)
//                .setStyle(inboxStyle)
//                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
//                .setContentText(messages.size() + " more Notifications");
//
//        NotificationManager notificationManager =
//                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(100 /* ID of notification */, notificationBuilder.build());



//
//        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(100, notification);

        if (messages.size() > 500) {
            MyApplication.getInstance().getPrefManager().updateNotificationsNull();
        }
    }


    // Clears notification tray messages
    public static void clearNotifications() {
        NotificationManager notificationManager = (NotificationManager) MyApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }


}
