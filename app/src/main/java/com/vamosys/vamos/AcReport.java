package com.vamosys.vamos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.vamosys.model.AcReportDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HorizontalScrollView;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.TypefaceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;

import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AcReport extends AppCompatActivity implements View.OnClickListener {
    Toolbar mToolbar;
    ImageView mCalendarImage, mImgMapViewClose;
    ConnectionDetector cd;
    SharedPreferences sp;
    private static ArrayList<AcReportDto> mStoppageData = new ArrayList<AcReportDto>();
    TableAdapter adapter;
    static ListView lv;
    static int width;
    static int height;
    static Const cons;
    View bottomSheet;
    BottomSheetBehavior behavior;
    DBHelper dbhelper;
    boolean bottomSheetEnabled = false;

    private int year, month, day;
    static final int TIME_DIALOG_ID = 1111;
    static final int DATE_DIALOG_ID = 2222;
    private int hour;
    private int minute;
    boolean isHeaderPresent = false;
    Spinner spinnerInterval;
    Button mBtnSubmit;
    TextView mTxtFromDate, mTxtEndDate, mTxtFromTime, mTxtEndTime, mTxtSelectedVehicle, mTxtNoRecord;

    SimpleDateFormat dftime = new SimpleDateFormat("kk:mm");
    String mStartDate, mEndDate, mStartTime, mStartTimeValue, mEndDateValue, mEndTime, mEndTimeValue, mIntervalSpinnerSelectedValue = "";
    int mIntervalSpinnerSelectedPos = 0;
    ArrayList<String> mIntervalList = new ArrayList<String>();
    boolean isFromTime = false, isFromDate = false, isMapPresent = false;
    //    private static GoogleMap map;
    static float vehicle_zoom_level = 15.5F;
    String mSELECTED_MAP_TYPE = "Normal";
    static double mLatitude = 0;
    static double mLongitude = 0;
    ImageView $ChangeView;
    static LinearLayout mAcReportDataMapLayout;
    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

    //OSM related
    MapView mapview;
    private IMapController mapController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        //important! set your user agent to prevent getting banned from the osm servers
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.activity_ac_report);
        init();
        screenArrange();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.ac_report));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);
        mCalendarImage = (ImageView) mToolbar.findViewById(R.id.img_calendar);
        mImgMapViewClose = (ImageView) mToolbar.findViewById(R.id.img_map_close);
        mImgMapViewClose.setOnClickListener(this);
        mCalendarImage.setOnClickListener(this);

        mStartDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));

        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);

//        SimpleDateFormat dateFormat = new SimpleDateFormat(
//                "yyyy-MM-dd hh:mm:ss aa");


        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        System.out.println("Hi date is :::" + timeFormat.format(cal.getTime()));

//        mStartTimeValue = "12:00 AM";
        mStartTimeValue = timeFormatShow.format(cal.getTime());
//        mStartTime = "12:00:00 am";
        mStartTime = timeFormat.format(cal.getTime());
        final Calendar c = Calendar.getInstance();
        // Current Hour
        hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        minute = c.get(Calendar.MINUTE);
        isFromTime = false;
        updateTime(hour, minute);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(AcReport.this, MapMenuActivity.class));
                finish();


            }
        });
        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        dbSetup();
        setBottomLayoutData();
        adapter = new TableAdapter(this);

        if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();

            System.out.println("Hi data get method calling");

//            mStoppageData.clear();
            new getStoppageReportdata().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }

        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });


        spinnerInterval
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mIntervalSpinnerSelectedPos = position;
                        if (position != 0) {
                            mIntervalSpinnerSelectedValue = String.valueOf(mIntervalList.get(position));
                        } else {
                            mIntervalSpinnerSelectedValue = "";
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
    }

    void dbSetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
//        FragmentManager myFragmentManager = getSupportFragmentManager();
//        SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
//                .findFragmentById(R.id.temperature_data_map);
//        myMapFragment.getMapAsync(this);

        mapview = (MapView) findViewById(R.id.osm_ac_report_map);
        mapview.setTileSource(TileSourceFactory.MAPNIK);
        mapview.setBuiltInZoomControls(false);
        mapview.setMultiTouchControls(true);
//        mapview.til
        mapController = mapview.getController();
        mapController.setZoom(6);


        $ChangeView = (ImageView) findViewById(R.id.temperature_data_map_ViewIcon);
        $ChangeView.setOnClickListener(this);
        mAcReportDataMapLayout = (LinearLayout) findViewById(R.id.temperature_data_map_view_layout);
        mAcReportDataMapLayout.setVisibility(View.GONE);
        lv = (ListView) findViewById(R.id.tstoppage_report_listView1);
        lv.setVisibility(View.VISIBLE);
        mTxtNoRecord = (TextView) findViewById(R.id.temperature_report_no_record_txt);
        mTxtNoRecord.setVisibility(View.GONE);
        spinnerInterval = (Spinner) findViewById(R.id.spinner_interval);

        mTxtFromDate = (TextView) findViewById(R.id.txt_start_date_value);
        mTxtFromTime = (TextView) findViewById(R.id.txt_start_time_value);
        mTxtEndDate = (TextView) findViewById(R.id.txt_end_date_value);
        mTxtEndTime = (TextView) findViewById(R.id.txt_end_time_value);
        mBtnSubmit = (Button) findViewById(R.id.btn_done);
        mTxtSelectedVehicle = (TextView) findViewById(R.id.selected_vehicle_txt);
        mTxtSelectedVehicle.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
        mTxtFromTime.setOnClickListener(this);
        mTxtFromDate.setOnClickListener(this);
        mTxtEndDate.setOnClickListener(this);
        mTxtEndTime.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
    }

    private void opptionPopUp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(AcReport.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                mapview.setTileSource(TileSourceFactory.MAPNIK);

//                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
//                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                mapview.setTileSource(TileSourceFactory.USGS_SAT);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
//                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                mapview.setTileSource(TileSourceFactory.HIKEBIKEMAP);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
//                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                mapview.setTileSource(TileSourceFactory.USGS_TOPO);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }

    public void setupMap() {

//        if (map != null) {
//            map.clear();
//        }

        if (mapview != null) {
            mapview.getOverlays().clear();
            mapview.invalidate();
        }


//        marker = ((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//        id_vehicle_in_marker.setVisibility(View.GONE);

//        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
//        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//
//
//        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//            // Use default InfoWindow frame
//            @Override
//            public View getInfoWindow(Marker arg0) {
//                return null;
//            }
//
//            // Defines the contents of the InfoWindow
//            @Override
//            public View getInfoContents(Marker arg0) {
//
//                // Getting view from the layout file info_window_layout
//                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);
//
//                // Getting the position from the marker
//                LatLng latLng = arg0.getPosition();
//
//                DisplayMetrics displayMetrics = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//                height = displayMetrics.heightPixels;
//                width = displayMetrics.widthPixels;
//                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
//                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT);
//                backImageParams.width = width * 80 / 100;
//                //backImageParams.height = height * 10 / 100;
//                backImageParams.gravity = Gravity.CENTER;
//                layout.setLayoutParams(backImageParams);
//
//                // Getting reference to the TextView to set latitude
//                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
//                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//
////                new GetAddressTask(txtContent).execute(mLatitude, mLongitude, 1.0);
//
//
//                StringBuffer addr = new StringBuffer();
//                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//                List<Address> addresses = null;
//                try {
//                    addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                } catch (IllegalArgumentException e2) {
//                    // Error message to post in the log
////                        String errorString = "Illegal arguments " +
////                                Double.toString(params[0]) +
////                                " , " +
////                                Double.toString(params[1]) +
////                                " passed to address service";
////                        e2.printStackTrace();
////                        return errorString;
//                } catch (NullPointerException np) {
//                    // TODO Auto-generated catch block
//                    np.printStackTrace();
//                }
//                // If the reverse geocode returned an address
//                if (addresses != null && addresses.size() > 0) {
//                    // Get the first address
//                    Address address = addresses.get(0);
//                /*
//                 * Format the first line of address (if available),
//                 * city, and country name.
//                 */
//                    String addressText = null;
//
//                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                        addressText = address.getAddressLine(i);
//                        addr.append(addressText + ",");
//                    }
//                    // Return the text
//                    // return addr.toString();
//                } else {
//                    addr.append("No address found");
//                }
//
//                // txtContent.setText(mSelectedMsg);
//                txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " " + addr);
//                //  txtContent.setVisibility(View.GONE);
//
//                return v;
//
//            }
//        });


//        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
        // markerOption.title(mSelectedMsg);
        // markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NotificationListActivity.this, marker)));


//        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//        Marker currentMarker = map.addMarker(markerOption);
//        currentMarker.showInfoWindow();


        View markerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_image_view, null);
        ImageView mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);
        mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
//        animateMarker.setIcon(createDrawableFromViewNew(AcReport.this, markerView));
        Marker osmMarker = new Marker(mapview);
        osmMarker.setPosition(new GeoPoint(mLatitude, mLongitude));
        osmMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        osmMarker.setTitle(Constant.SELECTED_VEHICLE_SHORT_NAME);
        osmMarker.setIcon(createDrawableFromViewNew(AcReport.this, markerView));
        InfoWindow infoWindow = new MyInfoWindow(R.layout.map_info_txt_layout, mapview);
        osmMarker.setInfoWindow(infoWindow);
        osmMarker.showInfoWindow();

        GeoPoint newPos = new GeoPoint(mLatitude, mLongitude);
        mapController.setCenter(newPos);
        mapview.getOverlays().add(osmMarker);
        mapview.invalidate();


//        Marker marker = map.addMarker(new MarkerOptions()
//                .position(new LatLng(mLatitude, mLongitude))
//                .title(mSelectedMsg)
//                        // .snippet("Snippet")
//                .icon(BitmapDescriptorFactory
//                        .fromResource(R.drawable.grey_custom_marker_icon)));
//
//        marker.showInfoWindow();


    }

    public static Drawable createDrawableFromViewNew(Context context, View view) {


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        System.out.println("Hi view width " + displayMetrics.widthPixels + " " + view.getMeasuredWidth() + " height " + displayMetrics.heightPixels + " " + view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.createBitmap(250, 250, Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.crea(96, 96, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
//        return bitmap;


        return new BitmapDrawable(bitmap);
    }

    private void setBottomLayoutData() {
        mTxtFromDate.setText(mStartDate);
        mTxtEndDate.setText(mEndDate);
        mTxtFromTime.setText(mStartTimeValue);
        mTxtEndTime.setText(mEndTimeValue);
        setIntervalSpinnerValue();
        mIntervalList = intervalSpinnerDataValue();

    }

    private void setIntervalSpinnerValue() {
        ArrayAdapter dataAdapter = new ArrayAdapter(this, R.layout.spinner_row, intervalSpinnerValue());

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        // spinnerFromHour.setPrompt("Select From Hour");
        spinnerInterval.setAdapter(dataAdapter);
        spinnerInterval.setSelection((mIntervalSpinnerSelectedPos));
    }

    private ArrayList<String> intervalSpinnerValue() {
        ArrayList<String> mHrList = new ArrayList<String>();
        mHrList.add("Interval");
        mHrList.add("1 mins");
        mHrList.add("2 mins");
        mHrList.add("5 mins");
        mHrList.add("10 mins");
        mHrList.add("15 mins");
        mHrList.add("30 mins");

        return mHrList;
    }

    private ArrayList<String> intervalSpinnerDataValue() {
        ArrayList<String> mHrList = new ArrayList<String>();
        mHrList.add("0");
        mHrList.add("1");
        mHrList.add("2");
        mHrList.add("5");
        mHrList.add("10");
        mHrList.add("15");
        mHrList.add("30");

        return mHrList;
    }

    private ArrayList<String> filterSpinnerValue() {
        ArrayList<String> mHrList = new ArrayList<String>();
        mHrList.add("All");
        mHrList.add(">1 mins");
        mHrList.add(">2 mins");
        mHrList.add(">5 mins");
        mHrList.add(">10 mins");
        mHrList.add(">15 mins");
        mHrList.add(">30 mins");
        mHrList.add(">1 hr");

        return mHrList;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            if (isFromDate) {
                mStartDate = String.valueOf(mDate2);
                //  mStartTimeValue=String.valueOf(mDate2);
                mTxtFromDate.setText(mDate2);
            } else {
                mEndDate = String.valueOf(mDate2);

                mTxtEndDate.setText(mDate2);
            }
//            mFromDate = String.valueOf(mDate);
//            mFromDateTxtValue = String.valueOf(mDate2);
//            mTxtDate.setText(mDate2);
            // }
        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:

                // set time picker as current time
                return new TimePickerDialog(this, timePickerListener, hour, minute,
                        false);

            case DATE_DIALOG_ID:
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                /** set date picker as current date */
                DatePickerDialog dialog = new DatePickerDialog(this,
                        datePickerListener, year, month, day);
                // set time picker as current time
                return dialog;

        }
        return null;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);

        }

    };

    private String utilTime(int value) {

        if (value < 10)
            return "0" + String.valueOf(value);
        else
            return String.valueOf(value);
    }


    private void updateTime(int hours, int mins) {

//        String timeSet = "", timeSet2 = "";
//        if (hours > 12) {
//            hours -= 12;
//            timeSet = "PM";
//            timeSet2 = "p.m.";
//        } else if (hours == 0) {
//            hours += 12;
//            timeSet = "AM";
//            timeSet2 = "a.m.";
//        } else if (hours == 12) {
//            timeSet = "PM";
//            timeSet2 = "p.m.";
//        } else {
//            timeSet = "AM";
//            timeSet2 = "a.m.";
//        }
//
//
//        String minutes = "";
//        if (mins < 10)
//            minutes = "0" + mins;
//        else
//            minutes = String.valueOf(mins);

// Append in a StringBuilder


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, mins);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        System.out.println("Hi date is 222:::" + timeFormat.format(cal.getTime()));

//        String aTime = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
        String aTime = timeFormatShow.format(cal.getTime());
//        String aTime2 = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(":00").append(minutes).append(" ").append(timeSet2).toString();

        String aTime2 = timeFormat.format(cal.getTime());


        if (isFromTime) {
            mStartTime = aTime2;
            mStartTimeValue = aTime;
            mTxtFromTime.setText(mStartTimeValue);
        } else {
            mEndTime = aTime2;
            mEndTimeValue = aTime;
            mTxtEndTime.setText(mEndTimeValue);
        }
    }

    private void screenArrange() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        height = metrics.heightPixels;
        width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;
    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        map = googleMap;
//        map.setPadding(1, 1, 1, 150);
//        map.getUiSettings().setZoomControlsEnabled(true);
//
//    }

    private class getStoppageReportdata extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {

                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
//                System.out.println("Hi doInBackground called ::::" + string_from_date);
                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                Date d = f.parse(string_from_date);

                long fromMilliSeconds = d.getTime();

                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
//                System.out.println("Date " + string_from_date + " utc " + fromMilliSeconds);

                HttpConfig ht = new HttpConfig();
//                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDropFillReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=1&fromDate=" + mStrFromDate + "&fromTime=" + mStrFromTime + "&toDate=" + mStrToDate + "&toTime=" + mStrToTime + "&fuelDrop=true&fuelFill=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);

//                http://gpsvts.net/mobile/getVehicleHistory?vehicleId=paaus1&interval=&fromDateUTC=1496773800000&toDateUTC=1496815840000&userId=PAAUS
//                result = ht.httpGet(Const.API_URL + "/mobile/getStoppageReport?fromDateUTC=1495391400000&toDateUTC=1495477799000&groupName=vantec:SMP&userId=vantec");
                result = ht.httpGet(Const.API_URL + "/mobile/getVehicleHistory?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=" + mIntervalSpinnerSelectedValue + "&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {
                e.printStackTrace();
                // System.out.println("Hi exception is ::::" + e.toString());
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null && result.length() > 0) {
//                System.out.println("The Fuel fill result is ::::" + result);
                boolean vehicleBusySelected = false;

                JSONArray jArrayFuel = null, jArrResult = null;
                try {


                    JSONObject jsonObject = new JSONObject(result.trim());

                    if (jsonObject.has("vehicleLocations")) {
                        jArrayFuel = jsonObject.getJSONArray("vehicleLocations");
                        ArrayList<AcReportDto> mSData = new ArrayList<AcReportDto>();
                        for (int i = 0; i < jArrayFuel.length(); i++) {
                            JSONObject jsonStoppageObject = jArrayFuel.getJSONObject(i);
                            AcReportDto f = new AcReportDto();

                            if (jsonStoppageObject.has("vehicleBusy")) {

                                if (!vehicleBusySelected) {
                                    if ((jsonStoppageObject.getString("vehicleBusy") != null) && (jsonStoppageObject.getString("vehicleBusy").equalsIgnoreCase("yes"))) {
                                        System.out.println("Vehicle busy yess:::" + jsonStoppageObject.getString("vehicleBusy") + " : " + i);


                                        if (jsonStoppageObject.has("latitude")) {
                                            f.setStartLatitude(jsonStoppageObject.getString("latitude"));
                                        }
                                        if (jsonStoppageObject.has("longitude")) {
                                            f.setStartLongitude(jsonStoppageObject.getString("longitude"));
                                        }
//                                    if (jsonStoppageObject.has("endStoppageTime")) {
//                                        f.setEndTime(jsonStoppageObject.getLong("endStoppageTime"));
//                                    }
                                        if (jsonStoppageObject.has("date")) {
                                            f.setStartTime(jsonStoppageObject.getLong("date"));
                                        }
                                        if (jsonStoppageObject.has("vehicleBusy")) {
                                            f.setStartStatus(jsonStoppageObject.getString("vehicleBusy"));
                                        }
                                        if (jsonStoppageObject.has("address")) {
//                                            System.out.println("Hi adds st ::" + jsonStoppageObject.getString("address"));
                                            f.setStartingAddress(jsonStoppageObject.getString("address"));

                                        }
                                        vehicleBusySelected = true;
//                                        System.out.println("Hi adds st 222::" + f.getStartingAddress());
                                        mSData.add(f);

                                    }
                                } else {
                                    if ((jsonStoppageObject.getString("vehicleBusy") != null) && (jsonStoppageObject.getString("vehicleBusy").equalsIgnoreCase("no"))) {
                                        System.out.println("Vehicle busy noo :::" + jsonStoppageObject.getString("vehicleBusy") + " : " + i);


                                        if (jsonStoppageObject.has("latitude")) {
                                            f.setEndLatitude(jsonStoppageObject.getString("latitude"));
                                        }
                                        if (jsonStoppageObject.has("longitude")) {
                                            f.setEndLongitude(jsonStoppageObject.getString("longitude"));
                                        }
//                                    if (jsonStoppageObject.has("endStoppageTime")) {
//                                        f.setEndTime(jsonStoppageObject.getLong("endStoppageTime"));
//                                    }
                                        if (jsonStoppageObject.has("date")) {
                                            f.setEndTime(jsonStoppageObject.getLong("date"));
                                        }
                                        if (jsonStoppageObject.has("vehicleBusy")) {
                                            f.setEndStatus(jsonStoppageObject.getString("vehicleBusy"));
                                        }
                                        if (jsonStoppageObject.has("address")) {
//                                            System.out.println("Hi adds ed ::" + jsonStoppageObject.getString("address"));
                                            f.setEndAddress(jsonStoppageObject.getString("address"));

                                        }
                                        vehicleBusySelected = false;
//                                        System.out.println("Hi adds ed 222::" + f.getEndAddress());
                                        mSData.add(f);

                                    }
                                }
                            }

                        }

                        ArrayList<AcReportDto> mSData2 = new ArrayList<AcReportDto>();
//                        System.out.println("Hi ac on off list size is :::" + mSData.size());
                        for (int i = 0; i < mSData.size(); i = i + 2) {
                            AcReportDto ac = new AcReportDto();
                            if (i < mSData.size()) {

//                                System.out.println("Hi status 00 " + mSData.get(i).getStartStatus());

                                if (mSData.get(i).getStartStatus() != null && mSData.get(i).getStartStatus().equalsIgnoreCase("yes")) {
//                                    System.out.println("Hi adds st i ::" + mSData.get(i).getStartStatus() + " " + mSData.get(i).getStartLatitude());
                                    ac.setStartingAddress(mSData.get(i).getStartingAddress());
                                    ac.setStartTime(mSData.get(i).getStartTime());
                                    ac.setStartLatitude(mSData.get(i).getStartLatitude());
                                    ac.setStartLongitude(mSData.get(i).getStartLongitude());
                                    ac.setStartStatus("ON");
                                }
                            }

                            if (i + 1 < mSData.size()) {
//                                System.out.println("Hi status 00 " + mSData.get(i + 1).getEndStatus());
                                if (mSData.get(i + 1).getEndStatus() != null && mSData.get(i + 1).getEndStatus().equalsIgnoreCase("no")) {
//                                    System.out.println("Hi adds ed i+1::" + mSData.get(i).getEndAddress());
                                    ac.setEndAddress(mSData.get(i + 1).getEndAddress());
                                    ac.setEndTime(mSData.get(i + 1).getEndTime());
                                    ac.setEndLatitude(mSData.get(i + 1).getEndLatitude());
                                    ac.setEndLongitude(mSData.get(i + 1).getEndLongitude());
                                    ac.setEndStatus("OFF");
                                }
                            }

                            System.out.println("Hi time 000 st ::" + ac.getStartTime() + "  ed " + ac.getEndTime());
//                            System.out.println("Hi adds st 000 ::" + ac.getStartingAddress() + "  ed " + ac.getEndAddress());
                            mSData2.add(ac);

                        }

                        mStoppageData = mSData2;
                    }

//                    if (jsonObject.has("totalFuel")) {
//                        mTotalFuelValue = String.valueOf(jsonObject.getDouble("totalFuel"));
//                    }

                    // }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                System.out.println("Hi ac on off list size is 22222:::" + mStoppageData.size());
                if (mStoppageData.size() > 0) {
                    lv.setVisibility(View.VISIBLE);
                    mTxtNoRecord.setVisibility(View.GONE);
                    setTableLayoutData();
                } else {
                    lv.setAdapter(null);
                    lv.setVisibility(View.GONE);
                    mTxtNoRecord.setVisibility(View.VISIBLE);
                }
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(AcReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    public void setTableLayoutData() {
//        System.out.println("Hi set table data called :::" + mStoppageData.size());
//        adapter.setHeader(mHeader);
//        ListView listView = (ListView) findViewById(R.id.executive_report_listView1);

        if (isHeaderPresent) {

        } else {
            lv.addHeaderView(adapter.getHeaderView(lv));
            isHeaderPresent = true;
        }


        lv.setAdapter(adapter);
        adapter.setData(mStoppageData);

        adapter.notifyDataSetChanged();

//        mTxtCumulativeFuelValue.setText(mTotalFuelValue + " ltrs");

//        parseVehicleKms();

    }

    public class TableAdapter extends BaseAdapter {

        // private static final String TAG = "TableAdapter";

        private Context mContext;

        private String[] mHeader;


        private int mCurrentScroll;

        private int[] mColResources = {R.id.ac_report_datetime_textView, R.id.ac_report_status_textView,
                R.id.ac_report_duration_textView, R.id.ac_report_nearest_location_textView, R.id.ac_report_gmap_textView, R.id.ac_report_end_datetime_textView,
                R.id.ac_report_end_status_textView, R.id.ac_report_nearest_end_location_textView, R.id.ac_report_end_gmap_textView};

        public TableAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return mStoppageData != null ? mStoppageData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mStoppageData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            HorizontalScrollView view = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                view = (HorizontalScrollView) inflater.inflate(
                        R.layout.ac_report_list_item_table_row, parent, false);

//                view.setOnScrollChangeListener(new OnScrollListener() {
//                    @Override
//                    public void onScrollChanged(View view, int scrollX) {
//
//                    }
//                });


                view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                    @Override
                    public void onScrollChanged(View scrollView, int scrollX) {

                        mCurrentScroll = scrollX;
                        ListView listView = (ListView) scrollView.getParent();
                        if (listView == null)
                            return;

                        for (int i = 0; i < listView.getChildCount(); i++) {
                            View child = listView.getChildAt(i);
                            if (child instanceof HorizontalScrollView
                                    && child != scrollView) {
                                HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                                if (scrollView2.getScrollX() != mCurrentScroll) {
                                    scrollView2.setScrollX(mCurrentScroll);
                                }
                            }
                        }
                    }
                });

            } else {
                view = (HorizontalScrollView) convertView;
            }

            view.setScrollX(mCurrentScroll);

            if (position % 2 == 0) {
                view.setBackgroundColor(Color.WHITE);
            } else {
                view.setBackgroundColor(Color.LTGRAY);
            }

            AcReportDto data = mStoppageData.get(position);

//            for (int i = 0; i < mColResources.length; i++) {
            TextView col1 = (TextView) view.findViewById(mColResources[0]);
            if (data.getStartTime() > 0) {
                col1.setText(cons.getReport24HourTimefromserver(String.valueOf(data.getStartTime())));
            }
            TextView col2 = (TextView) view.findViewById(mColResources[1]);
            if (data.getStartStatus() != null) {
                col2.setText(data.getStartStatus());
            }
            TextView col3 = (TextView) view.findViewById(mColResources[2]);
            long timevalue = 0;
            if (data.getEndTime() > data.getStartTime()) {
                timevalue = data.getEndTime() - data.getStartTime();
            }
//            System.out.println("Hi time  ::" + timevalue);
            col3.setText(cons.getreportTimeValue(timevalue));
            TextView col4 = (TextView) view.findViewById(mColResources[3]);
            if (data.getStartingAddress() != null) {
                col4.setText(data.getStartingAddress());
            }
            TextView col5 = (TextView) view.findViewById(mColResources[4]);
//            col5.setText(String.valueOf(data.getStartingAddress()));

            TextView col6 = (TextView) view.findViewById(mColResources[5]);
            if (data.getEndTime() > 0) {
                col6.setText(String.valueOf(cons.getReport24HourTimefromserver(String.valueOf(data.getEndTime()))));
            }
            TextView col7 = (TextView) view.findViewById(mColResources[6]);
            if (data.getEndStatus() != null) {
                col7.setText(String.valueOf(data.getEndStatus()));
            }
            TextView col8 = (TextView) view.findViewById(mColResources[7]);
            if (data.getEndAddress() != null) {
                col8.setText(String.valueOf(data.getEndAddress()));
            }

            TextView col9 = (TextView) view.findViewById(mColResources[8]);
//            col9.setText(String.valueOf(data.get()));

//            TextView col6 = (TextView) view.findViewById(mColResources[5]);
//            col6.setText(cons.getreportTimeValue((data.getStoppageTime())));
            // col4.setText(String.valueOf(data.getFuelTo()));
            // TextView col5 = (TextView) view.findViewById(mColResources[4]);
//            col5.setText("Location");

            SpannableString content = new SpannableString("G-Map");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            col5.setText(content);

            col9.setText(content);

            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 35 / 100;
//            vehicleNameParams.height = height * 9 / 100;
            col1.setLayoutParams(vehicleNameParams);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            col6.setLayoutParams(vehicleNameParams);
            col6.setPadding(width * 2 / 100, 0, 0, 0);
            col6.setGravity(Gravity.CENTER | Gravity.LEFT);

            LinearLayout.LayoutParams vehicleNameParams2 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams2.width = width * 15 / 100;
//            vehicleNameParams2.width = width * 15 / 100;

            col2.setLayoutParams(vehicleNameParams2);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);

            col7.setLayoutParams(vehicleNameParams2);
            col7.setPadding(width * 2 / 100, 0, 0, 0);
            col7.setGravity(Gravity.CENTER | Gravity.LEFT);

//            col6.setLayoutParams(vehicleNameParams);
//            col6.setPadding(width * 2 / 100, 0, 0, 0);
//            col6.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text3Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text3Params.width = width * 30 / 100;
//            text3Params.height = height * 9 / 100;

            col3.setLayoutParams(text3Params);
            //   col3.setWidth(width * 75 / 100);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(text3Params);
            //   col3.setWidth(width * 75 / 100);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);

            col9.setLayoutParams(text3Params);
            //  col3.setWidth(width * 75 / 100);
            col9.setPadding(width * 2 / 100, 0, 0, 0);
            col9.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 60 / 100;
//            text2Params.height = height * 9 / 100;
            col4.setLayoutParams(text2Params);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

            col8.setLayoutParams(text2Params);
            col8.setPadding(width * 2 / 100, 0, 0, 0);
            col8.setGravity(Gravity.CENTER | Gravity.LEFT);


            col5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mImgMapViewClose.setVisibility(View.VISIBLE);
                    mAcReportDataMapLayout.setVisibility(View.VISIBLE);
//                    if (isMapPresent) {
                    isMapPresent = true;
                    lv.setVisibility(View.GONE);
//                    }

                    lv.setVisibility(View.GONE);
                    mLatitude = Double.valueOf(mStoppageData.get(position).getStartLatitude());
                    mLongitude = Double.valueOf(mStoppageData.get(position).getStartLongitude());
                    setupMap();
                }
            });

            col9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mImgMapViewClose.setVisibility(View.VISIBLE);
                    mAcReportDataMapLayout.setVisibility(View.VISIBLE);
//                    if (isMapPresent) {
                    isMapPresent = true;
                    lv.setVisibility(View.GONE);
//                    }

                    lv.setVisibility(View.GONE);
                    mLatitude = Double.valueOf(mStoppageData.get(position).getEndLatitude());
                    mLongitude = Double.valueOf(mStoppageData.get(position).getEndLongitude());
                    setupMap();
                }
            });

//            }

            return view;
        }

        public View getHeaderView(ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            HorizontalScrollView view = (HorizontalScrollView) inflater
                    .inflate(R.layout.ac_report_list_item_table_header, parent, false);

//            for (int i = 0; i < mColResources.length; i++) {
//                TextView col = (TextView) view.findViewById(mColResources[i]);
//                col.setText(mHeader[i]);
//            }


            view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                @Override
                public void onScrollChanged(View scrollView, int scrollX) {

                    mCurrentScroll = scrollX;
                    ListView listView = (ListView) scrollView.getParent();
                    if (listView == null)
                        return;

                    for (int i = 0; i < listView.getChildCount(); i++) {
                        View child = listView.getChildAt(i);
                        if (child instanceof HorizontalScrollView
                                && child != scrollView) {
                            HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                            if (scrollView2.getScrollX() != mCurrentScroll) {
                                scrollView2.setScrollX(mCurrentScroll);
                            }
                        }
                    }
                }
            });


            TextView col1 = (TextView) view.findViewById(R.id.ac_report_datetime_textView_header);
            TextView col2 = (TextView) view.findViewById(R.id.ac_report_status_textView_header);
            TextView col3 = (TextView) view.findViewById(R.id.ac_report_duration_textView_header);
            TextView col4 = (TextView) view.findViewById(R.id.ac_report_nearest_location_textView_header);
            TextView col5 = (TextView) view.findViewById(R.id.ac_report_gmap_textView_header);
//            TextView col6 = (TextView) view.findViewById(R.id.temperature_link_textView_header);
            //  TextView col5 = (TextView) view.findViewById(R.id.link_textView_header);


            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 35 / 100;
            vehicleNameParams.height = height * 7 / 100;
            col1.setLayoutParams(vehicleNameParams);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            LinearLayout.LayoutParams vehicleNameParams2 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams2.width = width * 15 / 100;
            vehicleNameParams2.height = height * 7 / 100;

            col2.setLayoutParams(vehicleNameParams2);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text3Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text3Params.width = width * 30 / 100;
            text3Params.height = height * 7 / 100;

            col3.setLayoutParams(text3Params);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(text3Params);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 60 / 100;
            text2Params.height = height * 7 / 100;
            col4.setLayoutParams(text2Params);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);


            return view;
        }

//        public void setHeader(String[] header) {
//            mHeader = header;
//        }

        public void setData(ArrayList<AcReportDto> data) {
            mStoppageData = data;
            notifyDataSetChanged();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(AcReport.this, MapMenuActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_start_date_value:

                isFromDate = true;
                showDialog(DATE_DIALOG_ID);
                break;
            case R.id.txt_start_time_value:
//                showDialog(DATE_DIALOG_FROMID);
                isFromTime = true;
                showDialog(TIME_DIALOG_ID);
                break;
            case R.id.txt_end_date_value:
//                createNumberDialog(1);
                isFromDate = false;
                showDialog(DATE_DIALOG_ID);
//                show(1);

                break;
            case R.id.txt_end_time_value:
//                createNumberDialog(0);
                isFromTime = false;
                showDialog(TIME_DIALOG_ID);
//                show(0);
                break;

            case R.id.img_calendar:
                if (!bottomSheetEnabled) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    // setData();
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }
                break;

            case R.id.img_map_close:


                mAcReportDataMapLayout.setVisibility(View.GONE);
//                if (isMapPresent) {
                isMapPresent = false;
                lv.setVisibility(View.VISIBLE);
                mImgMapViewClose.setVisibility(View.GONE);
//                }


                break;
            case R.id.temperature_data_map_ViewIcon:

                opptionPopUp();

                break;
            case R.id.btn_done:
//                createNumberDialog(0);
//                show(0);
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();
//                    mStoppageData = new ArrayList<StoppageReportDto>();
                    mStoppageData.clear();
                    lv.setAdapter(null);
                    adapter.notifyDataSetChanged();
                    new getStoppageReportdata().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }

//                new CameraReportIamgeListActivtiy.getCameraReport().execute();
                break;
        }

    }

    private class MyInfoWindow extends InfoWindow {
        public MyInfoWindow(int layoutResId, MapView mapView) {
            super(layoutResId, mapView);
        }

        public void onClose() {
        }

        public void onOpen(Object arg0) {


            LinearLayout layout = (LinearLayout) mView.findViewById(R.id.map_info_layout);
//            Button btnMoreInfo = (Button) mView.findViewById(R.id.tv_txt_content);
            TextView txtContent = (TextView) mView.findViewById(R.id.tv_txt_content);
//            TextView txtDescription = (TextView) mView.findViewById(R.id.bubble_description);
//            TextView txtSubdescription = (TextView) mView.findViewById(R.id.bubble_subdescription);

//            txtTitle.setText("Title of my marker");
//            txtDescription.setText("Click here to view details!");
//            txtSubdescription.setText("You can also edit the subdescription");
//            layout.setOnClickListener(new OnClickListener() {
//                public void onClick(View v) {
//                    // Override Marker's onClick behaviour here
//                }
//            });

//            View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

            // Getting the position from the marker
//            LatLng latLng = arg0.getPosition();

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            height = displayMetrics.heightPixels;
            width = displayMetrics.widthPixels;
//            LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
            LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            backImageParams.width = width * 80 / 100;
            //backImageParams.height = height * 10 / 100;
            backImageParams.gravity = Gravity.CENTER;
            layout.setLayoutParams(backImageParams);

            // Getting reference to the TextView to set latitude
//            TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
            txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

//                new GetAddressTask(txtContent).execute(mLatitude, mLongitude, 1.0);


//            StringBuffer addr = new StringBuffer();
//            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//            List<Address> addresses = null;
//            try {
//                addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            } catch (IllegalArgumentException e2) {
//                // Error message to post in the log
////                        String errorString = "Illegal arguments " +
////                                Double.toString(params[0]) +
////                                " , " +
////                                Double.toString(params[1]) +
////                                " passed to address service";
////                        e2.printStackTrace();
////                        return errorString;
//            } catch (NullPointerException np) {
//                // TODO Auto-generated catch block
//                np.printStackTrace();
//            }
//            // If the reverse geocode returned an address
//            if (addresses != null && addresses.size() > 0) {
//                // Get the first address
//                Address address = addresses.get(0);
//                /*
//                 * Format the first line of address (if available),
//                 * city, and country name.
//                 */
//                String addressText = null;
//
//                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                    addressText = address.getAddressLine(i);
//                    addr.append(addressText + ",");
//                }
//                // Return the text
//                // return addr.toString();
//            } else {
//                addr.append("No address found");
//            }

            new GetAddressTaskNew(txtContent).execute(mLatitude, mLongitude, 4.0);

            // txtContent.setText(mSelectedMsg);
            txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);


        }
    }

    int mPopupStartCountNew = 1;

    private class GetAddressTaskNew extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTaskNew(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {
            getlat = params[0];
            getlng = params[1];

            String addr = null;
            try {
                addr = Constant.getAddress(String.valueOf(params[0]), String.valueOf(params[1]),getStdTableValue("appid"),getString(R.string.address_api));
            } catch (Exception e1) {
                e1.printStackTrace();
            }


            if (addr != null && addr.length() > 0) {
                return addr.toString();
            } else {
                return "No address found";
            }

        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);

            System.out.println("Hi google address address22 " + address_type + " " + result);

            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {


                    currenta_adddress_view.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " " + (result.equals("null") ? "No Data" : result));


                    if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                        if (mPopupStartCountNew < 3) {
//                                new GetAddressTask(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);
//
//                            } else {
                            new GetAddressTaskNew(currenta_adddress_view).execute(getlat, getlng, 4.0);
                        }
                        mPopupStartCountNew++;
                    }


                } else {
//                    Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
//                    empty_fav.show();
                }
            }
        }


    }

    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }
}
