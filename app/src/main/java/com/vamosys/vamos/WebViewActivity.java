package com.vamosys.vamos;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.webkit.WebView;
import android.widget.ImageView;

public class WebViewActivity extends AppCompatActivity {
    WebView webView;
    ImageView webback;
    SharedPreferences sp;
    boolean login_status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        webView = (WebView) findViewById(R.id.webView1);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
       login_status = sp.getBoolean("login_status",false);
//        webback = (ImageView)findViewById(R.id.web_back);
//        webback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(WebViewActivity.this,RegistrationPage.class));
//            }
//        });
        webView.getSettings().setJavaScriptEnabled(true);
        String s = getResources().getString(R.string.app_name);
        webView.loadUrl("https://vamotest.sgp1.digitaloceanspaces.com/Privacy_Policy/"+s+".html");

        }

    @Override
    public void onBackPressed() {
        if(login_status) {
            startActivity(new Intent(WebViewActivity.this, VehicleListActivity.class));
        }
        else {
            startActivity(new Intent(WebViewActivity.this, RegistrationPage.class));
        }
        super.onBackPressed();
    }
}
