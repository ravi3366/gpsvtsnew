package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vamosys.model.FuelFillDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HorizontalScrollView;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.TypefaceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FuelFillReport extends FragmentActivity implements OnMapReadyCallback {
    static ListView lv;
    TextView mHeadTitle, mTxtNoRecord, mTxtVehicleName, mTxtVehicleNameValue, mTxtCumulativeFuel, mTxtCumulativeFuelValue;
    static LinearLayout mFuelDataMapLayout;
    View v1, v2;
    ImageView mBackArrow, mImgChangeDate, mImgDataViewChange;
    ConnectionDetector cd;
    SharedPreferences sp;
    static Const cons;

    static int width;
    static int height;
    boolean isHeaderPresent = false;
    DBHelper dbhelper;
    private List<FuelFillDto> mFuelFillData = new ArrayList<FuelFillDto>();

    TableAdapter adapter;

    Dialog marker_info_dialog;
    TextView id_from_date, id_to_date, id_from_time, id_to_time;
    String mStrFromDate, mStrToDate, mStrFromTime, mStrToTime;
    String mTotalFuelValue = null;
    private static GoogleMap map;
    ImageView $ChangeView;
    boolean mIsMapPresent = false;
    String mStrUtcFromDate, mStrUtcToDate;
    String mSELECTED_MAP_TYPE = "Normal";
    static double mLatitude = 0;
    static double mLongitude = 0;
    static float vehicle_zoom_level = 15.5F;
    static Context mFuelContext;

    LinearLayout mGoogleMapLayout;

    MapView mapview;
    private IMapController mapController;
    boolean isOsmEnabled = true, mIsFuelChartEnabled = false;
    String sensor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_fill_report);

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cons = new Const();
        dbSetup();
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        if (sp.getString("enabled_map", "") != null) {
            if (sp.getString("enabled_map", "").trim().length() > 0) {

//                System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                    isOsmEnabled = true;
                } else {
                    isOsmEnabled = false;
                }
            }
        }

        init();
        screenArrange();

        cd = new ConnectionDetector(getApplicationContext());
        mFuelContext = getApplicationContext();
        adapter = new TableAdapter(this);


//        Calendar cal = Calendar.getInstance();
////        cal.add(Calendar.MONTH, -1);
//        cal.add(Calendar.DATE, -7);
//        Date result = cal.getTime();
//
//        SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
//
//        SimpleDateFormat timeDate = new SimpleDateFormat("HH:mm:ss");
//
//
//        String mCurrentDate = dfDate.format(new Date());
//        final String mFromDate = dfDate.format(result);
////        mStrFromDate = mCurrentDate;
//        mStrFromDate = mFromDate;
//        mStrToDate = mCurrentDate;
//
//        String mCurrentTime = timeDate.format(new Date());
//        mStrFromTime = "00:00:00";
//        mStrToTime = mCurrentTime;


        Bundle extras = getIntent().getExtras();
        if (extras == null) {
//            return;
            //  startActivity(new Intent(FuelFillReport.this, FuelFillNewReport.class));
            finish();
        } else {
            mStrFromDate = extras.getString("start_date");
            mStrFromTime = extras.getString("start_time");
            mStrToDate = extras.getString("end_date");
            mStrToTime = extras.getString("end_time");
            mIsFuelChartEnabled = extras.getBoolean("is_chart_enabled", false);
            sensor=extras.getString("sensor","1");
        }


        if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();

            new getKmsSummaryData().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }

        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(FuelFillReport.this, MapMenuActivity.class));
//                finish();


//                startActivity(new Intent(FuelFillReport.this, FuelFillNewReport.class));
//                finish();

//                Intent in = new Intent(FuelFillReport.this, FuelFillNewReport.class);
//                in.putExtra("start_date", mStrFromDate);
//                in.putExtra("start_time", mStrFromTime);
//                in.putExtra("end_date", mStrToDate);
//                in.putExtra("end_time", mStrToTime);
//                in.putExtra("is_chart_enabled", mIsFuelChartEnabled);
//                startActivity(in);
                finish();

            }
        });

        mImgChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVehicleInfoDialog();
            }
        });

        $ChangeView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                opptionPopUp();
            }
        });

        mImgDataViewChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//               mIsMapPresent=false;
                setListData();


            }
        });


    }

    void dbSetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {

//        FragmentManager myFragmentManager = getSupportFragmentManager();
//        SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
//                .findFragmentById(R.id.fuel_fill_data_map);
//        myMapFragment.getMapAsync(this);


        $ChangeView = (ImageView) findViewById(R.id.fuel_fill_data_map_ViewIcon);
        mFuelDataMapLayout = (LinearLayout) findViewById(R.id.fuel_fill_data_map_view_layout);

        lv = (ListView) findViewById(R.id.fuelFill_report_listView1);
        mImgDataViewChange = (ImageView) findViewById(R.id.fuelFill_report_change_data_view);
        mImgDataViewChange.setVisibility(View.GONE);
//        lv.setVisibility(View.GONE);
//        chart = (BarChart) findViewById(R.id.chart);
//        fromdate = (TextView) findViewById(R.id.executive_report_from_date_text);
//        todate = (TextView) findViewById(R.id.executive_report_to_date_text);
//        fromdatevalue = (TextView) findViewById(R.id.executive_report_from_date_value);
//        todatevalue = (TextView) findViewById(R.id.executive_report_to_date_value);
//        groupSpinner = (Spinner) findViewById(R.id.executive_report_groupspinner);


        v1 = (View) findViewById(R.id.fuelFill_vehicle_name_view);
        v2 = (View) findViewById(R.id.fuelFill_cumulative_fuel_view);
//        v3 = (View) findViewById(R.id.executive_report_view_horizontal);
        //  mTxtNoRecord = (TextView) findViewById(R.id.fuelFill_report_tvTitle);
        mTxtVehicleName = (TextView) findViewById(R.id.fuelFill_vehicle_name_txt);
        mTxtVehicleNameValue = (TextView) findViewById(R.id.fuelFill_vehicle_name_value_txt);
        mTxtVehicleNameValue.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
        mTxtCumulativeFuel = (TextView) findViewById(R.id.fuelFill_cumulative_fuel_txt);
        mTxtCumulativeFuelValue = (TextView) findViewById(R.id.fuelFill_cumulative_fuel_value_txt);
        mTxtNoRecord = (TextView) findViewById(R.id.fuelFill_report_no_record_txt);
        mBackArrow = (ImageView) findViewById(R.id.fuelFill_report_view_Back);
        mHeadTitle = (TextView) findViewById(R.id.fuelFill_report_tvTitle);
        mImgChangeDate = (ImageView) findViewById(R.id.fuelFill_report_change_date);


        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);

        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);
        if (isOsmEnabled) {
            $ChangeView.setVisibility(View.GONE);
            mGoogleMapLayout.setVisibility(View.GONE);
            rl.setVisibility(View.VISIBLE);

            mapview = new MapView(this);
            mapview.setTilesScaledToDpi(true);
            rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                    RelativeLayout.LayoutParams.FILL_PARENT));
//        mapview.setTileSource(TileSourceFactory.MAPNIK);
            mapview.setBuiltInZoomControls(false);
            mapview.setMultiTouchControls(true);
            mapController = mapview.getController();
            mapController.setZoom(8);
            mapview.getOverlays().clear();
            mapview.invalidate();
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
        } else {
            $ChangeView.setVisibility(View.VISIBLE);
            mGoogleMapLayout.setVisibility(View.VISIBLE);
            rl.setVisibility(View.GONE);

            FragmentManager myFragmentManager = getSupportFragmentManager();
            SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                    .findFragmentById(R.id.fuel_fill_data_map);
            myMapFragment.getMapAsync(this);
        }
    }


    private void opptionPopUp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(FuelFillReport.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }


    public void setupOsmMap() {

//        if (map != null) {
//            map.clear();
//        }

        if (mapview != null) {
            InfoWindow.closeAllInfoWindowsOn(mapview);
            mapview.getOverlays().clear();
            mapview.invalidate();
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
        }

//        View marker = null;
//
//        marker = ((LayoutInflater) mFuelContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//        id_vehicle_in_marker.setVisibility(View.GONE);
//
//        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
//        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//
//
//        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//            // Use default InfoWindow frame
//            @Override
//            public View getInfoWindow(Marker arg0) {
//                return null;
//            }
//
//            // Defines the contents of the InfoWindow
//            @Override
//            public View getInfoContents(Marker arg0) {
//
//                // Getting view from the layout file info_window_layout
//                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);
//
//                // Getting the position from the marker
//                LatLng latLng = arg0.getPosition();
//
//                DisplayMetrics displayMetrics = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//                height = displayMetrics.heightPixels;
//                width = displayMetrics.widthPixels;
//                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
//                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT);
//                backImageParams.width = width * 80 / 100;
//                //backImageParams.height = height * 10 / 100;
//                backImageParams.gravity = Gravity.CENTER;
//                layout.setLayoutParams(backImageParams);
//
//                // Getting reference to the TextView to set latitude
//                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
//                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//
////                new GetAddressTask(txtContent).execute(mLatitude, mLongitude, 1.0);
//
//
//                StringBuffer addr = new StringBuffer();
//                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//                List<Address> addresses = null;
//                try {
//                    addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                } catch (IllegalArgumentException e2) {
//                    // Error message to post in the log
////                        String errorString = "Illegal arguments " +
////                                Double.toString(params[0]) +
////                                " , " +
////                                Double.toString(params[1]) +
////                                " passed to address service";
////                        e2.printStackTrace();
////                        return errorString;
//                } catch (NullPointerException np) {
//                    // TODO Auto-generated catch block
//                    np.printStackTrace();
//                }
//                // If the reverse geocode returned an address
//                if (addresses != null && addresses.size() > 0) {
//                    // Get the first address
//                    Address address = addresses.get(0);
//                /*
//                 * Format the first line of address (if available),
//                 * city, and country name.
//                 */
//                    String addressText = null;
//
//                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                        addressText = address.getAddressLine(i);
//                        addr.append(addressText + ",");
//                    }
//                    // Return the text
//                    // return addr.toString();
//                } else {
//                    addr.append("No address found");
//                }
//
//                // txtContent.setText(mSelectedMsg);
//                txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " " + addr);
//                //  txtContent.setVisibility(View.GONE);
//
//                return v;
//
//            }
//        });


//        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
        // markerOption.title(mSelectedMsg);
        // markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NotificationListActivity.this, marker)));

//
//        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//        Marker currentMarker = map.addMarker(markerOption);
//        currentMarker.showInfoWindow();

//        Marker marker = map.addMarker(new MarkerOptions()
//                .position(new LatLng(mLatitude, mLongitude))
//                .title(mSelectedMsg)
//                        // .snippet("Snippet")
//                .icon(BitmapDescriptorFactory
//                        .fromResource(R.drawable.grey_custom_marker_icon)));
//
//        marker.showInfoWindow();

        View markerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_image_view, null);
        ImageView mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);
        mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
//        animateMarker.setIcon(createDrawableFromViewNew(AcReport.this, markerView));
        org.osmdroid.views.overlay.Marker osmMarker = new org.osmdroid.views.overlay.Marker(mapview);
        osmMarker.setPosition(new GeoPoint(mLatitude, mLongitude));
        osmMarker.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
//        osmMarker.setTitle(Constant.SELECTED_VEHICLE_SHORT_NAME);
        osmMarker.setIcon(Constant.createDrawableFromViewNew(FuelFillReport.this, markerView));
        InfoWindow infoWindow = new MyInfoWindow(R.layout.map_info_txt_layout, mapview);
        osmMarker.setInfoWindow(infoWindow);
        osmMarker.showInfoWindow();

        final GeoPoint newPos = new GeoPoint(mLatitude, mLongitude);
//        mapController.setCenter(newPos);

        new Handler(Looper.getMainLooper()).post(
                new Runnable() {
                    public void run() {
                        mapview.getController().setCenter(newPos);
                    }
                }
        );

        mapview.getOverlays().add(osmMarker);
        mapview.invalidate();


    }

    private class MyInfoWindow extends InfoWindow {
        public MyInfoWindow(int layoutResId, MapView mapView) {
            super(layoutResId, mapView);
        }

        public void onClose() {
        }

        public void onOpen(Object arg0) {


            LinearLayout layout = (LinearLayout) mView.findViewById(R.id.map_info_layout);
//            Button btnMoreInfo = (Button) mView.findViewById(R.id.tv_txt_content);
            TextView txtContent = (TextView) mView.findViewById(R.id.tv_txt_content);
//            TextView txtDescription = (TextView) mView.findViewById(R.id.bubble_description);
//            TextView txtSubdescription = (TextView) mView.findViewById(R.id.bubble_subdescription);

//            txtTitle.setText("Title of my marker");
//            txtDescription.setText("Click here to view details!");
//            txtSubdescription.setText("You can also edit the subdescription");
//            layout.setOnClickListener(new OnClickListener() {
//                public void onClick(View v) {
//                    // Override Marker's onClick behaviour here
//                }
//            });

//            View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

            // Getting the position from the marker
//            LatLng latLng = arg0.getPosition();

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            height = displayMetrics.heightPixels;
            width = displayMetrics.widthPixels;
//            LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
            LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            backImageParams.width = width * 80 / 100;
            //backImageParams.height = height * 10 / 100;
            backImageParams.gravity = Gravity.CENTER;
            layout.setLayoutParams(backImageParams);

            // Getting reference to the TextView to set latitude
//            TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
            txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

//                new GetAddressTask(txtContent).execute(mLatitude, mLongitude, 1.0);


//            StringBuffer addr = new StringBuffer();
//            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//            List<Address> addresses = null;
//            try {
//                addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            } catch (IllegalArgumentException e2) {
//                // Error message to post in the log
////                        String errorString = "Illegal arguments " +
////                                Double.toString(params[0]) +
////                                " , " +
////                                Double.toString(params[1]) +
////                                " passed to address service";
////                        e2.printStackTrace();
////                        return errorString;
//            } catch (NullPointerException np) {
//                // TODO Auto-generated catch block
//                np.printStackTrace();
//            }
//            // If the reverse geocode returned an address
//            if (addresses != null && addresses.size() > 0) {
//                // Get the first address
//                Address address = addresses.get(0);
//                /*
//                 * Format the first line of address (if available),
//                 * city, and country name.
//                 */
//                String addressText = null;
//
//                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                    addressText = address.getAddressLine(i);
//                    addr.append(addressText + ",");
//                }
//                // Return the text
//                // return addr.toString();
//            } else {
//                addr.append("No address found");
//            }

            new GetAddressTaskNew(txtContent).execute(mLatitude, mLongitude, 4.0);

            // txtContent.setText(mSelectedMsg);
            txtContent.setText("");


        }
    }

    int mPopupStartCountNew = 1;

    private class GetAddressTaskNew extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTaskNew(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {
            getlat = params[0];
            getlng = params[1];

            String addr = null;
            try {
                addr = Constant.getAddress(String.valueOf(params[0]), String.valueOf(params[1]),getStdTableValue("appid"),getString(R.string.address_api));
            } catch (Exception e1) {
                e1.printStackTrace();
            }


            if (addr != null && addr.length() > 0) {
                return addr.toString();
            } else {
                return "No address found";
            }

        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);

            System.out.println("Hi google address address22 " + address_type + " " + result);

            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {


                    currenta_adddress_view.setText((result.equals("null") ? "No Data" : result));


                    if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {


                        if (mPopupStartCountNew < 3) {
//                                new GetAddressTask(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);
//
//                            } else {
                            new GetAddressTaskNew(currenta_adddress_view).execute(getlat, getlng, 4.0);
                        }
                        mPopupStartCountNew++;
                    }


                } else {
//                    Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
//                    empty_fav.show();

                    if (mPopupStartCountNew < 3) {
//                                new GetAddressTask(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);
//
//                            } else {
                        new GetAddressTaskNew(currenta_adddress_view).execute(getlat, getlng, 4.0);
                    }
                    mPopupStartCountNew++;

                }
            }
        }


    }

    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }
    public void setupGoogleMap() {

        if (map != null) {
            map.clear();
        }

        View marker = null;

        marker = ((LayoutInflater) mFuelContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
        id_vehicle_in_marker.setVisibility(View.GONE);

        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

//
//        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//            // Use default InfoWindow frame
//            @Override
//            public View getInfoWindow(Marker arg0) {
//                return null;
//            }
//
//            // Defines the contents of the InfoWindow
//            @Override
//            public View getInfoContents(Marker arg0) {
//
//
//                // View v=new MyInfoWindow(R.layout.map_info_txt_layout);
//
//                // Getting view from the layout file info_window_layout
//                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);
//
//                // Getting the position from the marker
//                LatLng latLng = arg0.getPosition();
//
//                DisplayMetrics displayMetrics = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//                height = displayMetrics.heightPixels;
//                width = displayMetrics.widthPixels;
//                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
//                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT);
//                backImageParams.width = width * 80 / 100;
//                //backImageParams.height = height * 10 / 100;
//                backImageParams.gravity = Gravity.CENTER;
//                layout.setLayoutParams(backImageParams);
//
//                // Getting reference to the TextView to set latitude
//                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
//                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//
////                new GetAddressTask(txtContent).execute(mLatitude, mLongitude, 1.0);
//
//
//                StringBuffer addr = new StringBuffer();
//                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//                List<Address> addresses = null;
//                try {
//                    addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                } catch (IllegalArgumentException e2) {
//                    // Error message to post in the log
////                        String errorString = "Illegal arguments " +
////                                Double.toString(params[0]) +
////                                " , " +
////                                Double.toString(params[1]) +
////                                " passed to address service";
////                        e2.printStackTrace();
////                        return errorString;
//                } catch (NullPointerException np) {
//                    // TODO Auto-generated catch block
//                    np.printStackTrace();
//                }
//                // If the reverse geocode returned an address
//                if (addresses != null && addresses.size() > 0) {
//                    // Get the first address
//                    Address address = addresses.get(0);
//                /*
//                 * Format the first line of address (if available),
//                 * city, and country name.
//                 */
//                    String addressText = null;
//
//                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                        addressText = address.getAddressLine(i);
//                        addr.append(addressText + ",");
//                    }
//                    // Return the text
//                    // return addr.toString();
//                } else {
//                    addr.append("No address found");
//                }
//
//                // txtContent.setText(mSelectedMsg);
//                txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " " + addr);
//
//                new GetAddressTaskNew(txtContent).execute(arg0.getPosition().latitude, arg0.getPosition().longitude, 4.0);
//
//                //  txtContent.setVisibility(View.GONE);
//
//                return v;
//
//            }
//        });


        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
        // markerOption.title(mSelectedMsg);
        // markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NotificationListActivity.this, marker)));


        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        Marker currentMarker = map.addMarker(markerOption);
        currentMarker.showInfoWindow();

//        Marker marker = map.addMarker(new MarkerOptions()
//                .position(new LatLng(mLatitude, mLongitude))
//                .title(mSelectedMsg)
//                        // .snippet("Snippet")
//                .icon(BitmapDescriptorFactory
//                        .fromResource(R.drawable.grey_custom_marker_icon)));
//
//        marker.showInfoWindow();


    }


    private boolean showVehicleInfoDialog() {

        TextView
                id__popupto_date_label,
                id_popup_from_label, id__popupto_time_label,
                id_popup_from_time_label,
                id_txt_divider1, id_txt_divider2, id_txt_divider3, id_histroy_vehicleid;
        Button id_history_selection_done, id_history_selection_cancel;
//        if (isFromList) {
//            marker_info_dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
//            marker_info_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        } else {
        marker_info_dialog = new Dialog(FuelFillReport.this);
//        }
        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        marker_info_dialog.setContentView(R.layout.fuel_popup_layout);
        marker_info_dialog.setCancelable(false);
        //LinearLayout id_date_time_picker_layout;
        LinearLayout id_from_datetime_layout;
//        ImageView id_history_vehicle_info_icon;
//        final TextView id_histroy_vehicleid, id_history_type_value, id_from_label, id_from_date, id_from_time, id_to_date, id_to_time;
        // Button id_history_selection_done, id_history_selection_cancel;

        id_from_datetime_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id_popup_date_time_from_datetime_layout);


        // id_history_vehicle_info_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_popup_history_vehicle_info_icon);
        id_histroy_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_date_time_vehicleid);

        id_txt_divider1 = (TextView) marker_info_dialog.findViewById(R.id.date_time_divider_txt_1);
        id_txt_divider2 = (TextView) marker_info_dialog.findViewById(R.id.date_time_divider_txt_2);
        id_txt_divider2 = (TextView) marker_info_dialog.findViewById(R.id.date_time_divider_txt_3);


        // id_popup_history_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_labelid);
        //   id_popup_history_type_value = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_type_value);

        id_popup_from_label = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_from_label);

        id_from_date = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_from_date);
        id__popupto_date_label = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_to_date_label);
        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_to_date);


        id_popup_from_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_from_time_label);

        id_from_time = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_from_time);
        id__popupto_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_to_time_label);
        id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_to_time);


//        id__popupto_date_label = (TextView) marker_info_dialog.findViewById(R.id.id__popupto_date_label);
//        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_date);
//        id_popup_to_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time_label);
//        id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time);

        id_history_selection_done = (Button) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_selection_done);
        id_history_selection_cancel = (Button) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_selection_cancel);


//        id_from_date.setText(cons.GetCurrentDate(Calendar.getInstance()));
//        id_to_date.setText(cons.GetCurrentDate(Calendar.getInstance()));
//
//        SimpleDateFormat timeDate = new SimpleDateFormat("HH:mm:ss");
//
//        String mCurrentTime = timeDate.format(new Date());
//
//        id_from_time.setText("00:00:00");
//        id_to_time.setText(mCurrentTime);

        id_from_date.setText(mStrFromDate.trim());
        id_to_date.setText(mStrToDate.trim());


        id_from_time.setText(mStrFromTime.trim());
        id_to_time.setText(mStrToTime.trim());


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        // int height = metrics.heightPixels;
        //  int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

//        LinearLayout.LayoutParams history_for_layout = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        history_for_layout.width = width * 90 / 100;
//        history_for_layout.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        history_for_layout.gravity = Gravity.CENTER;
//        history_for_layout.setMargins(width * 5 / 100, 0, width * 5 / 100, 0);
//        id_history_type_layout.setLayoutParams(history_for_layout);

//        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        backImageParams.width = width * 80 / 100;
//        backImageParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        backImageParams.gravity = Gravity.CENTER;
//
//        id_popup_history_labelid.setLayoutParams(backImageParams);
//        id_popup_history_type_value.setLayoutParams(backImageParams);

        LinearLayout.LayoutParams backImageParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams1.width = width * 40 / 100;
        backImageParams1.height = LinearLayout.LayoutParams.WRAP_CONTENT;

        id_popup_from_label.setLayoutParams(backImageParams1);
        id_from_date.setLayoutParams(backImageParams1);
        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);

        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);
        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);


        id_popup_from_time_label.setLayoutParams(backImageParams1);
        id_from_time.setLayoutParams(backImageParams1);
        id__popupto_time_label.setLayoutParams(backImageParams1);
        id_to_time.setLayoutParams(backImageParams1);


        if (width >= 600) {
            // id_popup_history_labelid.setTextSize(18);
            // id_popup_history_type_value.setTextSize(17);
            id_from_date.setTextSize(17);
            //  id_from_time.setTextSize(17);
            id_to_date.setTextSize(17);

            id_from_time.setTextSize(17);
            id_to_time.setTextSize(17);

            // id_to_time.setTextSize(17);
            //  $TxtOfflineVehicleValue.setTextSize(16);
        } else if (width > 501 && width < 600) {
            // id_popup_history_labelid.setTextSize(17);
            // id_popup_history_type_value.setTextSize(16);
            id_from_date.setTextSize(16);
            //id_from_time.setTextSize(16);
            id_to_date.setTextSize(16);

            id_from_time.setTextSize(16);
            id_to_time.setTextSize(16);
            // id_to_time.setTextSize(16);
            // $TxtOfflineVehicleValue.setTextSize(15);
        } else if (width > 260 && width < 500) {
            //id_popup_history_labelid.setTextSize(16);
            // id_popup_history_type_value.setTextSize(15);
            id_from_date.setTextSize(15);
            // id_from_time.setTextSize(15);
            id_to_date.setTextSize(15);

            id_from_time.setTextSize(15);
            id_to_time.setTextSize(15);
            //  id_to_time.setTextSize(15);
            // $TxtOfflineVehicleValue.setTextSize(14);
        } else if (width <= 260) {
            //  id_popup_history_labelid.setTextSize(15);
            //  id_popup_history_type_value.setTextSize(14);
            id_from_date.setTextSize(14);
            //  id_from_time.setTextSize(14);
            id_to_date.setTextSize(14);

            id_from_time.setTextSize(14);
            id_to_time.setTextSize(14);
            // id_to_time.setTextSize(14);
            //  $TxtOfflineVehicleValue.setTextSize(13);
        }


        id_histroy_vehicleid.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);


        /** listener to handle when history is canceled */
        id_history_selection_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                opptionPopUp();
                marker_info_dialog.hide();
            }
        });

        /** listener to handle when history is selected */
        id_history_selection_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    marker_info_dialog.hide();
//                    id_date_time_picker_layout.setVisibility(View.GONE);
//                    id_map_info_layout.setVisibility(View.GONE);
//
                    adapter = null;
                    adapter = new TableAdapter(FuelFillReport.this);
//     String url = Const.API_URL + "/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    mFuelFillData.clear();
                    lv.setAdapter(null);

                    mStrFromDate = id_from_date.getText().toString().trim();
                    mStrToDate = id_to_date.getText().toString().trim();
                    mStrFromTime = id_from_time.getText().toString().trim();
                    mStrToTime = id_to_time.getText().toString().trim();
//                    String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    // String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&fromTime=" + id_from_time.getText().toString() + "&toDate=" + id_to_date.getText().toString() + "&toTime=" + id_to_time.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                    // String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                    new getKmsSummaryData().execute();
                    //  new GetPOIInformation().execute(par);
                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
                    internet_toast.show();
                }
            }
        });

        id_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new DatePickerDialog(FuelFillReport.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        id_from_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

//        /** listener to handle when From date is clicked in history selection*/
//        id_from_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(TripSummaryActivity.this, "Hi from date clicked ::::", Toast.LENGTH_SHORT).show();
//                from_date = true;
//                to_date = false;
//                //   from_time = false;
//                //   to_time = false;
//                showDialog(DATE_PICKER_ID);
//            }
//        });
        /** listener to handle when From Time is clicked in history selection*/
//        id_from_time.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = false;
//                from_time = true;
//                to_time = false;
//                showDialog(TIME_DIALOG_ID);
//            }
//        });


        id_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new DatePickerDialog(FuelFillReport.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        id_to_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        id_from_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new TimePickerDialog(FuelFillReport.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        // TODO Auto-generated method stub
                        id_from_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
                    }
                }, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false).show();
            }
        });

        id_to_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new TimePickerDialog(FuelFillReport.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        // TODO Auto-generated method stub
                        id_to_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
                    }
                }, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false).show();
            }
        });


        /** listener to handle when To date is clicked in history selection*/
//        id_to_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = true;
//                // from_time = false;
//                // to_time = false;
//                showDialog(DATE_PICKER_ID);
//            }
//        });
        /** listener to handle when To Time is clicked in history selection*/
//        id_to_time.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = false;
//                from_time = false;
//                to_time = true;
//                showDialog(TIME_DIALOG_ID);
//            }
//        });


        marker_info_dialog.show();
        return true;
    }

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }


    private class getKmsSummaryData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                //  Const constaccess = new Const();

                // System.out.println("Helllooooooooooooo testttttt");
                String string_from_date = mStrFromDate.trim() + " " + mStrFromTime.trim();
                String string_to_date = mStrToDate.trim() + " " + mStrToTime.trim();

                //   System.out.println("The from date  :::::" + string_from_date);

                //  System.out.println("The to date  :::::" + string_to_date);

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date d = f.parse(string_from_date);

                //  System.out.println("The from date after converted  :::::" + d);

                long fromMilliSeconds = d.getTime();
                // System.out.println("The from time in millis :::::" + fromMilliSeconds);
                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
//                long fromMilliSeconds =1470767400000;
//                long toMilliSeconds=1471685033000;


                //  System.out.println("The to time in millis :::::" + toMilliSeconds);


//                result = constaccess
//                        .sendGet(Const.API_URL + "/mobile/getExecutiveReport?groupId=" + mSelectedGroup + "&fromDate=" + mStrFromDate + "&toDate=" + mStrToDate + "&userId=" + mUserId, 30000);
                HttpConfig ht = new HttpConfig();
//                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDropFillReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=1&fromDate=" + mStrFromDate.trim() + "&fromTime=" + mStrFromTime.trim() + "&toDate=" + mStrToDate.trim() + "&toTime=" + mStrToTime.trim() + "&fuelDrop=false&fuelFill=false&fuelFillV2=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDetailForMachinery?vehicleId=" + Constant.SELECTED_VEHICLE_ID  + "&fromDateTime=" + fromMilliSeconds + "&toDateTime=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID+"&sensor="+sensor);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null && result.length() > 0) {
                // System.out.println("The Fuel fill result is ::::" + result);
                JSONArray jsonArray = null;
                try {
                    JSONObject jsonObject = new JSONObject(result.trim());
                    if (jsonObject.has("fuelFills")){
                        jsonArray = new JSONArray();
                        jsonArray = jsonObject.getJSONArray("fuelFills");
                        for (int i = 0;i<jsonArray.length();i++){
                            JSONObject jsonFuelObject = jsonArray.getJSONObject(i);
                            FuelFillDto f = new FuelFillDto();

                            if (jsonFuelObject.has("startTime")) {
                                f.setStartTime(jsonFuelObject.getLong("startTime"));
                            }
                            if (jsonFuelObject.has("time")) {
                                f.setEndTime(jsonFuelObject.getLong("time"));
                            }
                            if (jsonFuelObject.has("lat")) {
                                f.setLatitude(jsonFuelObject.getDouble("lat"));
                            }
                            if (jsonFuelObject.has("lng")) {
                                f.setLongitude(jsonFuelObject.getDouble("lng"));
                            }
                            if (jsonFuelObject.has("lt")) {
                                f.setFuelConsume(jsonFuelObject.getDouble("lt"));
                            }
                            if (jsonFuelObject.has("pFuel")) {
                                f.setFuelFrom(jsonFuelObject.getDouble("pFuel"));
                            }
                            if (jsonFuelObject.has("odo")) {
                                f.setOdo(jsonFuelObject.getDouble("odo"));
                            }
                            if (jsonFuelObject.has("cFuel")) {
                                f.setFuelTo(jsonFuelObject.getDouble("cFuel"));
                            }
                            mFuelFillData.add(f);
                        }

                    }
                    if (jsonObject.has("totalFuelFills")) {
                        mTotalFuelValue = String.valueOf(jsonObject.getDouble("totalFuelFills"));
                    }

                    double i2 = jsonObject.getDouble("totalFuelFills");
                    if (mTotalFuelValue != null) {
                        mTxtCumulativeFuelValue.setText((String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(i2)))) +" ltrs");
                        //mTxtCumulativeFuelValue.setText(mTotalFuelValue + " ltrs");
                    } else {
                        mTxtCumulativeFuelValue.setText("0 ltr");
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

//                JSONArray jsonArray = null, jArrayFuel = null;
//                try {
//                    jsonArray = new JSONArray(result);
//
//                    // for (int i = 0; i < jsonArray.length(); i++) {
//                    JSONObject jsonObject = jsonArray.getJSONObject(1);
//
//                    if (jsonObject.has("fuelFill")) {
//                        jArrayFuel = jsonObject.getJSONArray("fuelFill");
//                        for (int i = 0; i < jArrayFuel.length(); i++) {
//                            JSONObject jsonFuelObject = jArrayFuel.getJSONObject(i);
//                            FuelFillDto f = new FuelFillDto();
//
//                            if (jsonFuelObject.has("startTime")) {
//                                f.setStartTime(jsonFuelObject.getLong("startTime"));
//                            }
//                            if (jsonFuelObject.has("latitude")) {
//                                f.setLatitude(jsonFuelObject.getDouble("latitude"));
//                            }
//                            if (jsonFuelObject.has("longitude")) {
//                                f.setLongitude(jsonFuelObject.getDouble("longitude"));
//                            }
//                            if (jsonFuelObject.has("fuelConsume")) {
//                                f.setFuelConsume(jsonFuelObject.getDouble("fuelConsume"));
//                            }
//                            if (jsonFuelObject.has("fuelFrom")) {
//                                f.setFuelFrom(jsonFuelObject.getDouble("fuelFrom"));
//                            }
//                            if (jsonFuelObject.has("fuelTo")) {
//                                f.setFuelTo(jsonFuelObject.getDouble("fuelTo"));
//                            }
//                            mFuelFillData.add(f);
//
//                        }
//                    }
//
//                    if (jsonObject.has("totalFuel")) {
//                        mTotalFuelValue = String.valueOf(jsonObject.getDouble("totalFuel"));
//                    }
//
//                    // }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }


                setTableLayoutData();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(FuelFillReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    public void setTableLayoutData() {

        if (isHeaderPresent) {

        } else {
            lv.addHeaderView(adapter.getHeaderView(lv));
            isHeaderPresent = true;
        }

        lv.setAdapter(adapter);
        adapter.setData(mFuelFillData);
        adapter.notifyDataSetChanged();

//        if (mTotalFuelValue != null) {
//            mTxtCumulativeFuelValue.setText(mTotalFuelValue + " ltrs");
//        } else {
//            mTxtCumulativeFuelValue.setText("0 ltr");
//        }

    }

    public class TableAdapter extends BaseAdapter {

        // private static final String TAG = "TableAdapter";

        private Context mContext;

        private String[] mHeader;


        private int mCurrentScroll;

        private int[] mColResources = {R.id.fuelfill_date_textView,R.id.fuelfill_todate_textView,
                R.id.fuelfill_beginning_ltr_textView, R.id.fuelfill_ending_ltr_textView,R.id.fuelfill_filling_ltr_textView,R.id.fuelfill_odo_textView, R.id.link_textView};

        public TableAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return mFuelFillData != null ? mFuelFillData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mFuelFillData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            HorizontalScrollView view = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                view = (HorizontalScrollView) inflater.inflate(
                        R.layout.fuelfill_list_item_table_row, parent, false);

//                view.setOnScrollChangeListener(new OnScrollListener() {
//                    @Override
//                    public void onScrollChanged(View view, int scrollX) {
//
//                    }
//                });


                view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                    @Override
                    public void onScrollChanged(View scrollView, int scrollX) {

                        mCurrentScroll = scrollX;
                        ListView listView = (ListView) scrollView.getParent();
                        if (listView == null)
                            return;

                        for (int i = 0; i < listView.getChildCount(); i++) {
                            View child = listView.getChildAt(i);
                            if (child instanceof HorizontalScrollView
                                    && child != scrollView) {
                                HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                                if (scrollView2.getScrollX() != mCurrentScroll) {
                                    scrollView2.setScrollX(mCurrentScroll);
                                }
                            }
                        }
                    }
                });

            } else {
                view = (HorizontalScrollView) convertView;
            }

            view.setScrollX(mCurrentScroll);

            if (position % 2 == 0) {
                view.setBackgroundColor(Color.WHITE);
            } else {
                view.setBackgroundColor(Color.LTGRAY);
            }

            FuelFillDto data = mFuelFillData.get(position);

//            for (int i = 0; i < mColResources.length; i++) {
            TextView col1 = (TextView) view.findViewById(mColResources[0]);

            col1.setText(cons.getReportTimefromserver(String.valueOf(data.getStartTime())));
            TextView col2 = (TextView) view.findViewById(mColResources[1]);
            col2.setText(cons.getReportTimefromserver(String.valueOf(data.getEndTime())));

            //col2.setText(String.valueOf(data.getFuelConsume()));
            TextView col3 = (TextView) view.findViewById(mColResources[2]);
            col3.setText(String.valueOf(data.getFuelFrom()));
            TextView col4 = (TextView) view.findViewById(mColResources[3]);
            col4.setText(String.valueOf(data.getFuelTo()));
            TextView col5 = (TextView) view.findViewById(mColResources[4]);
            double i2 = data.getFuelConsume();
            col5.setText((String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(i2)))));
            TextView col6 = (TextView) view.findViewById(mColResources[5]);
            double i3 = data.getOdo();
            col6.setText((String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(i3)))));
            TextView col7 = (TextView) view.findViewById(mColResources[6]);
//            col5.setText("Location");

            SpannableString content = new SpannableString("Location");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            col7.setText(content);


            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 35 / 100;
            vehicleNameParams.height = height * 7 / 100;
            col1.setLayoutParams(vehicleNameParams);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            col3.setLayoutParams(vehicleNameParams);

            col3.setPadding(width * 4 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 28 / 100;
            text2Params.height = height * 7 / 100;
            col2.setLayoutParams(text2Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);


            col4.setLayoutParams(text2Params);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(text2Params);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);


            col7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mImgDataViewChange.setVisibility(View.VISIBLE);
                    mIsMapPresent = true;
                    mFuelDataMapLayout.setVisibility(View.VISIBLE);
                    lv.setVisibility(View.GONE);
                    mLatitude = mFuelFillData.get(position).getLatitude();
                    mLongitude = mFuelFillData.get(position).getLongitude();

                    if (isOsmEnabled) {
                        setupOsmMap();
                    } else {
                        setupGoogleMap();
                    }

//                    setupMap();
                }
            });

//            }

            return view;
        }

        public View getHeaderView(ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            HorizontalScrollView view = (HorizontalScrollView) inflater
                    .inflate(R.layout.fuelfill_list_item_table_header, parent, false);

//            for (int i = 0; i < mColResources.length; i++) {
//                TextView col = (TextView) view.findViewById(mColResources[i]);
//                col.setText(mHeader[i]);
//            }


            view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                @Override
                public void onScrollChanged(View scrollView, int scrollX) {

                    mCurrentScroll = scrollX;
                    ListView listView = (ListView) scrollView.getParent();
                    if (listView == null)
                        return;

                    for (int i = 0; i < listView.getChildCount(); i++) {
                        View child = listView.getChildAt(i);
                        if (child instanceof HorizontalScrollView
                                && child != scrollView) {
                            HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                            if (scrollView2.getScrollX() != mCurrentScroll) {
                                scrollView2.setScrollX(mCurrentScroll);
                            }
                        }
                    }
                }
            });


            TextView col1 = (TextView) view.findViewById(R.id.fuelfill_date_textView_header);
            TextView col2 = (TextView) view.findViewById(R.id.fuelfill_todate_textView_header);
            TextView col3 = (TextView) view.findViewById(R.id.fuelfill_filling_ltr_textView_header);
            TextView col4 = (TextView) view.findViewById(R.id.fuelfill_beginning_ltr_textView_header);
            TextView col5 = (TextView) view.findViewById(R.id.fuelfill_ending_ltr_textView_header);
            TextView col6 = (TextView) view.findViewById(R.id.fuelfill_odo_textView_header);
            TextView col7 = (TextView) view.findViewById(R.id.link_textView_header);


            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 35 / 100;
            vehicleNameParams.height = height * 7 / 100;
            col1.setLayoutParams(vehicleNameParams);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            col3.setLayoutParams(vehicleNameParams);

            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 28 / 100;
            text2Params.height = height * 7 / 100;
            col2.setLayoutParams(text2Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);


            col4.setLayoutParams(text2Params);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(text2Params);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);

            col6.setLayoutParams(text2Params);
            col6.setPadding(width * 2 / 100, 0, 0, 0);
            col6.setGravity(Gravity.CENTER | Gravity.LEFT);

            col7.setLayoutParams(text2Params);
            col7.setPadding(width * 2 / 100, 0, 0, 0);
            col7.setGravity(Gravity.CENTER | Gravity.LEFT);


            return view;
        }

//        public void setHeader(String[] header) {
//            mHeader = header;
//        }

        public void setData(List<FuelFillDto> data) {
            mFuelFillData = data;
            notifyDataSetChanged();
        }

    }

    public void setListData() {
        mImgDataViewChange.setVisibility(View.GONE);
        mIsMapPresent = false;
        lv.setVisibility(View.VISIBLE);
        mFuelDataMapLayout.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

//
//        if (mIsMapPresent) {
//            System.out.println("The map is present ::::::" + mIsMapPresent);
//            setListData();
//
//           // new getKmsSummaryData().execute();
//        } else {
//        startActivity(new Intent(FuelFillReport.this, MapMenuActivity.class));
//        finish();
//        startActivity(new Intent(FuelFillReport.this, FuelFillNewReport.class));
//        finish();

//        Intent in = new Intent(FuelFillReport.this, FuelFillNewReport.class);
//        in.putExtra("is_chart_enabled", mIsFuelChartEnabled);
//        startActivity(in);
        finish();

//        }
    }

    private void screenArrange() {
        // TODO Auto-generated method stub

        /* screen arrangements */
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        height = metrics.heightPixels;
        width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 7 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mBackArrow.setLayoutParams(backImageParams);
        mBackArrow.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 62 / 100;
        headTxtParams.height = height * 7 / 100;
        mHeadTitle.setLayoutParams(headTxtParams);
        mHeadTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mHeadTitle.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams changeDataImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        changeDataImageParams.width = width * 9 / 100;
        changeDataImageParams.height = height * 7 / 100;
        changeDataImageParams.gravity = Gravity.CENTER;
        mImgDataViewChange.setLayoutParams(changeDataImageParams);
        mImgDataViewChange.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);


        LinearLayout.LayoutParams changeDateImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        changeDateImageParams.width = width * 9 / 100;
        changeDateImageParams.height = height * 7 / 100;
        changeDateImageParams.gravity = Gravity.CENTER;
        mImgChangeDate.setLayoutParams(changeDateImageParams);
        mImgChangeDate.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textParams.width = width * 45 / 100;
        textParams.height = height * 9 / 2 / 100;
        textParams.topMargin = (int) (height * 0.25 / 100);
        textParams.leftMargin = width * 2 / 100;
        textParams.rightMargin = width * 2 / 100;
        mTxtVehicleName.setLayoutParams(textParams);
        mTxtCumulativeFuel.setLayoutParams(textParams);
        mTxtVehicleName.setGravity(Gravity.CENTER);
        mTxtCumulativeFuel.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams textValueParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textValueParams.width = width * 45 / 100;
        textValueParams.height = height * 9 / 2 / 100;
        textValueParams.leftMargin = width * 2 / 100;
        textValueParams.rightMargin = width * 2 / 100;
        textValueParams.bottomMargin = (int) (height * 0.25 / 100);
        mTxtVehicleNameValue.setLayoutParams(textValueParams);
        mTxtCumulativeFuelValue.setLayoutParams(textValueParams);
        mTxtVehicleNameValue.setGravity(Gravity.CENTER);
        mTxtCumulativeFuelValue.setGravity(Gravity.CENTER);

//        LinearLayout.LayoutParams ViewParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        ViewParams.width = width * 1 / 100;
//        ViewParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
//        v1.setLayoutParams(ViewParams);
//        v2.setLayoutParams(ViewParams);
//
        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        viewParams.width = width;
        viewParams.height = height * 1 / 2 / 100;
        v1.setLayoutParams(viewParams);
        v2.setLayoutParams(viewParams);

//        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        spinnerParams.width = width * 95 / 100;
//        spinnerParams.height = height * 6 / 100;
//        spinnerParams.topMargin = (int) (height * 1.25 / 100);
//        spinnerParams.leftMargin = width * 2 / 100;
//        spinnerParams.rightMargin = width * 2 / 100;
//        spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
//        groupSpinner.setLayoutParams(spinnerParams);

        LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lineParams.width = width * 98 / 100;
        lineParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lineParams.topMargin = height * 2 / 100;
        lineParams.bottomMargin = height * 1 / 100;
        lineParams.leftMargin = width * 1 / 100;
        lineParams.rightMargin = width * 1 / 100;
        lv.setLayoutParams(lineParams);

//        LinearLayout.LayoutParams headTxtParams1 = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        headTxtParams1.width = width * 76 / 100;
//        headTxtParams1.height = height * 8 / 100;
//        headTxtParams1.setMargins(width * 4 / 100, width * 2 / 100, 1, width * 1 / 100);
////        headTxtParams1.setMargins();
////        $TxtTitle.setLayoutParams(headTxtParams);
////        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
////        $TxtTitle.setGravity(Gravity.CENTER);
//        mEdtSearch.setLayoutParams(headTxtParams1);
////        mEdtSearch.setm
//        mEdtSearch.setPadding(width * 2 / 100, 0, width * 4 / 100, 0);
//        mEdtSearch.setGravity(Gravity.CENTER | Gravity.CENTER);

        FrameLayout.LayoutParams imgeChangeView = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgeChangeView.width = width * 10 / 100;
        imgeChangeView.height = height * 15 / 100;
        imgeChangeView.gravity = Gravity.TOP | Gravity.RIGHT;
        imgeChangeView.rightMargin = (int) (width * 0.5 / 100);
        $ChangeView.setLayoutParams(imgeChangeView);


        if (width >= 600) {
            mTxtVehicleNameValue.setTextSize(16);
            mTxtVehicleName.setTextSize(16);
            mTxtCumulativeFuelValue.setTextSize(16);
            mTxtCumulativeFuel.setTextSize(16);
            mTxtNoRecord.setTextSize(18);
            mHeadTitle.setTextSize(18);
            //mEdtSearch.setTextSize(18);
        } else if (width > 501 && width < 600) {
            mTxtVehicleNameValue.setTextSize(15);
            mTxtVehicleName.setTextSize(15);
            mTxtCumulativeFuelValue.setTextSize(15);
            mTxtCumulativeFuel.setTextSize(15);
            mTxtNoRecord.setTextSize(17);
            mHeadTitle.setTextSize(17);
            //  mEdtSearch.setTextSize(17);
        } else if (width > 260 && width < 500) {
            mTxtVehicleNameValue.setTextSize(14);
            mTxtVehicleName.setTextSize(14);
            mTxtCumulativeFuelValue.setTextSize(14);
            mTxtCumulativeFuel.setTextSize(14);
            mTxtNoRecord.setTextSize(16);
            mHeadTitle.setTextSize(16);
            //  mEdtSearch.setTextSize(16);
        } else if (width <= 260) {
            mTxtVehicleNameValue.setTextSize(13);
            mTxtVehicleName.setTextSize(13);
            mTxtCumulativeFuelValue.setTextSize(13);
            mTxtCumulativeFuel.setTextSize(13);
            mTxtNoRecord.setTextSize(15);
            mHeadTitle.setTextSize(15);
            //  mEdtSearch.setTextSize(15);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);
    }
}
