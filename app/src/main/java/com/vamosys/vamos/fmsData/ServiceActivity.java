package com.vamosys.vamos.fmsData;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fabtransitionactivity.SheetLayout;
import com.vamosys.adapter.FmsDataAdapter;
import com.vamosys.adapter.ServiceAdapter;
import com.vamosys.model.FmsDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.RecyclerItemClickListener;
import com.vamosys.utils.SingleTonClass;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

public class ServiceActivity extends AppCompatActivity implements SheetLayout.OnFabAnimationEndListener {
    LinearLayout remainderlayou,fleetlayout,servicelayout,deletelayout;
    TextView remaindertxt,fleettxt,delete;
    ImageView remainderimg,fleetimg,back,backarrow;
    FloatingActionButton fab;
    SheetLayout mSheetLayout;
    public static int posittion;
    JSONObject jsonCameraObject;

    RecyclerView mRecyclerView;
    TextView mTxtNoRecord;
    ConnectionDetector cd;
    String odometer,notifinterval;
    String Tagg="check serivces";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        fab = (FloatingActionButton) findViewById(R.id.fab_service);
        mSheetLayout = (SheetLayout)findViewById(R.id.bottom_sheetlayot);
        mSheetLayout.setFab(fab);
        mSheetLayout.setFabAnimationEndListener(this);
        remainderlayou = (LinearLayout)findViewById(R.id.remain_layout);
        fleetlayout = (LinearLayout)findViewById(R.id.fleetmanag_layout);
        remaindertxt = (TextView)findViewById(R.id.remain_txt);
        fleettxt = (TextView)findViewById(R.id.fleetmanag_txt);
        remainderimg = (ImageView) findViewById(R.id.remain_image);
        fleetimg = (ImageView)findViewById(R.id.fleetmanag_image);
        back = (ImageView)findViewById(R.id.back_arrow);
        backarrow = (ImageView)findViewById(R.id.back_delete);
        servicelayout = (LinearLayout)findViewById(R.id.service_layout);
        deletelayout = (LinearLayout)findViewById(R.id.layout_delete);
        delete = (TextView)findViewById(R.id.delete_service);
        cd = new ConnectionDetector(ServiceActivity.this);
        mTxtNoRecord = (TextView) findViewById(R.id.txt_norecord);
        // Replace 'android.R.id.list' with the 'id' of your RecyclerView
        mRecyclerView = (RecyclerView)findViewById(R.id.service_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ServiceActivity.this);
        mRecyclerView.setLayoutManager(layoutManager);
        remainderlayou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remainderimg.setColorFilter(getResources().getColor(R.color.yellow_dark));
                remaindertxt.setTextColor(getResources().getColor(R.color.yellow_dark));
                fleetimg.setColorFilter(getResources().getColor(R.color.black));
                fleettxt.setTextColor(getResources().getColor(R.color.black));
                startActivity(new Intent(ServiceActivity.this,RemainderActivity.class));
            }
        });
        fleetlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remainderimg.setColorFilter(getResources().getColor(R.color.black));
                remaindertxt.setTextColor(getResources().getColor(R.color.black));
                fleetimg.setColorFilter(getResources().getColor(R.color.yellow_dark));
                fleettxt.setTextColor(getResources().getColor(R.color.yellow_dark));
                startActivity(new Intent(ServiceActivity.this,FmsActivity.class));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ServiceActivity.this,FmsActivity.class));
            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        System.out.println("Hi selected recycler pos is " + position);

                        // do whatever
                        Intent in = new Intent(ServiceActivity.this, AllServiceActivity.class);
                        in.putExtra("select_pos", position);
                        startActivity(in);
                        finish();

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                        deletelayout.setVisibility(View.VISIBLE);
                        servicelayout.setVisibility(View.GONE);
                        posittion = position;

                        Log.d("longposition",""+position);




                    }
                })
        );

        loadFmsData();
//        if (SingleTonClass.getInstance().getFmsData().size() > 0) {
//            mRecyclerView.setVisibility(View.VISIBLE);
//            mTxtNoRecord.setVisibility(View.GONE);
//            ServiceAdapter mAadp = new ServiceAdapter(SingleTonClass.getInstance().getFmsData(),ServiceActivity.this);
//            mRecyclerView.setAdapter(mAadp);
//            mAadp.notifyDataSetChanged();
//
//        } else {
//            mRecyclerView.setVisibility(View.GONE);
//            mTxtNoRecord.setVisibility(View.VISIBLE);
//        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SingleTonClass.getInstance().getmServiceList() != null && SingleTonClass.getInstance().getmServiceList().size() > 0) {

                    mSheetLayout.expandFab();
                } else {
                    Toast.makeText(ServiceActivity.this, "All Service Data Entered", Toast.LENGTH_SHORT).show();
                }
            }
        });
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ServiceActivity.this,FmsActivity.class));
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    startActivity(new Intent(ServiceActivity.this,ServiceActivity.class));
                    new postFmsDeleteData().execute(SingleTonClass.getInstance().getmServicedata().get(posittion).getDocumentType());
                }
                else {
                    FmsDataAdapter mAadp = new FmsDataAdapter(SingleTonClass.getInstance().getmServicedata(), ServiceActivity.this);

                    mRecyclerView.setAdapter(mAadp);
                    mAadp.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(), "Please check your network connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onFabAnimationEnd() {
        if (SingleTonClass.getInstance().getmServiceList() != null && SingleTonClass.getInstance().getmServiceList().size() > 0) {
            Intent in = new Intent(ServiceActivity.this, AllServiceActivity.class);
//                    in.putExtra("selected_pos", position);
            startActivity(in);
            finish();
        } else{
            Toast.makeText(this, "All Service Data Entered", Toast.LENGTH_SHORT).show();
        }
    }

    public void loadFmsData() {
        if (cd.isConnectingToInternet()) {
            Log.d(Tagg,"load fms data");
            new getFmsData().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }
    }


    private class getFmsData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();
                result = ht.httpGet(Const.API_URL + "/mobile/getFmsDetails?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID);
                Log.d(Tagg,"do in background");
//                result = ht.httpGet("http://gpsvts.net/mobile/getFmsDetails?userId=DEMO&vehicleId=TVS-HR47C2298");

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            SingleTonClass.getInstance().setFmsReminderEmptyList(new ArrayList<FmsDto>());
            SingleTonClass.getInstance().setRemainingEmptyList(new ArrayList<String>());
            SingleTonClass.getInstance().setmServiceList(new ArrayList<String>());
            SingleTonClass.getInstance().setmServicedata(new ArrayList<FmsDto>());
            SingleTonClass.getInstance().setFmsDataEmptyList(new ArrayList<FmsDto>());
            if (result != null && result.length() > 0) {

                try {

                    JSONObject fmsObj = new JSONObject(result.trim());
                    try {
                        if (fmsObj.has("serviceList")) {
                            if (fmsObj.get("serviceList") != null) {
                                JSONArray fmsArray = fmsObj.getJSONArray("serviceList");
                                for (int i = 0; i < fmsArray.length(); i++) {
                                     jsonCameraObject = fmsArray.getJSONObject(i);
                                    FmsDto f = new FmsDto();

                                    if (jsonCameraObject.has("odometerDue")) {
                                        if (jsonCameraObject.getString("odometerDue") != null) {
                                            if (jsonCameraObject.has("odometerDue")) {
                                                Log.d("odometerdue", "success");
                                                f.setOdometerDue(jsonCameraObject.getString("odometerDue"));
                                              odometer = jsonCameraObject.getString("odometerDue");
                                              Log.d("odometervalue",""+jsonCameraObject.getString("odometerDue"));
                                            }


                                            if (jsonCameraObject.has("dueDate")) {
                                                Log.d(Tagg,"duedate"+jsonCameraObject.getString("dueDate"));
                                                f.setDueDate(jsonCameraObject.getString("dueDate"));
                                            }
                                            if (jsonCameraObject.has("amount")) {
                                                f.setAmount(jsonCameraObject.getString("amount"));
                                            }
                                            if (jsonCameraObject.has("companyName")) {
                                                f.setCompanyName(jsonCameraObject.getString("companyName"));
                                            }
                                            if (jsonCameraObject.has("description")) {
                                                f.setDescription(jsonCameraObject.getString("description"));
                                            }

                                            if (jsonCameraObject.has("notifyInterval")) {
                                                f.setNotifyInterval(jsonCameraObject.getString("notifyInterval"));
                                            }
                                            if (jsonCameraObject.has("monthInterval")) {
                                                Log.d(Tagg,"month"+jsonCameraObject.getString("monthInterval"));
                                                f.setMonthInterval(jsonCameraObject.getString("monthInterval"));
                                            }
                                            if (jsonCameraObject.has("documentType")) {
                                                f.setDocumentType(jsonCameraObject.getString("documentType"));
                                                Log.d(Tagg,"documentype: "+jsonCameraObject.has("documentType"));
                                                if(jsonCameraObject.getString("documentType").contains("OTHERS")){
                                                    String documentName[] =jsonCameraObject.getString("documentType").split(":");
                                                    Log.d("Tagg","other document "+documentName[1]);
                                                    f.setDocumentName(documentName[1]);
                                                }
                                                else{
                                                    Log.d(Tagg,"Normal document type");
                                                    f.setDocumentName(jsonCameraObject.getString("documentType"));
                                                }

                                            }
                                            notifinterval = jsonCameraObject.getString("notifyInterval");
                                       }
                                    }

                                    SingleTonClass.getInstance().addToServicedata(f);
                                    Log.d(Tagg,"add data in fmsdto");
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                    try {
                        if (fmsObj.has("getServiceList")) {
                            if (fmsObj.get("getServiceList") != null) {
                                JSONArray remainingList = fmsObj.getJSONArray("getServiceList");
                                Log.d("getservicelist", "" + fmsObj.getJSONArray("getServiceList"));
                                for (int i = 0; i < remainingList.length(); i++) {
                                    SingleTonClass.getInstance().addToServiceList(remainingList.get(i).toString().trim());
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            Log.d("odometer",""+odometer);
            Log.d("notify",""+notifinterval);

//                if ("null".equalsIgnoreCase(odometer)){
//                    Log.d("odometer","success");
//                } else if (odometer !=null) {
//
//                    Log.d("odometer", "enter");
//                    mRecyclerView.setVisibility(View.VISIBLE);
//                    mTxtNoRecord.setVisibility(View.GONE);
//                    ServiceAdapter mAadp = new ServiceAdapter(SingleTonClass.getInstance().getmServicedata(), ServiceActivity.this);
//                    mRecyclerView.setAdapter(mAadp);
//                    mAadp.notifyDataSetChanged();
//                }
//
//                else {
//                    Log.d("odometer","failure");
//                            mRecyclerView.setVisibility(View.GONE);
//                            mTxtNoRecord.setVisibility(View.VISIBLE);
//                        }
            if ("null".equalsIgnoreCase(odometer)){
                Log.d("odometer","success");

            } else{
                if (SingleTonClass.getInstance().getmServicedata().size() > 0) {

                    Log.d("odometer", "enter");
                    Log.d(Tagg,"view recycler view");
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mTxtNoRecord.setVisibility(View.GONE);
                    ServiceAdapter mAadp = new ServiceAdapter(SingleTonClass.getInstance().getmServicedata(), ServiceActivity.this);
                    mRecyclerView.setAdapter(mAadp);
                    mAadp.notifyDataSetChanged();
                }

                     else {
                        Log.d("odometer", "empty");
                        mRecyclerView.setVisibility(View.GONE);
                        mTxtNoRecord.setVisibility(View.VISIBLE);
                    }

            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(ServiceActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private class postFmsDeleteData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();
                String docType = params[0];
                String sEncoded = URLEncoder.encode(docType,"UTF-8");
//               http://188.166.244.126:9000/getFmsDetails?userId=DEMO&vehicleId=TVS-HR47C2298&documentType=FC

                result = ht.httpGet("http://188.166.244.126:9000/getFmsDetails?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&documentType="+sEncoded);
              //  result = ht.httpGet("http://puccagps.com:9000/getFmsDetails?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&documentType="+sEncoded);
                // result = ht.httpGet("http://puccagps.com:9000/getFmsDetails?userId=DEMO&vehicleId=TVS-HR47C2298&documentType="+sEncoded);


            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null && result.length() > 0) {
//                try {
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }

                if (cd.isConnectingToInternet()) {
                    new getFmsData().execute();

                } else {
                    FmsDataAdapter mAadp = new FmsDataAdapter(SingleTonClass.getInstance().getmServicedata(), ServiceActivity.this);
                    mRecyclerView.setAdapter(mAadp);
                    mAadp.notifyDataSetChanged();
                }

            }


        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(ServiceActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ServiceActivity.this, FmsActivity.class));
        finish();
    }

}
