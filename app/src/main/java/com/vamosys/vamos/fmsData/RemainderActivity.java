package com.vamosys.vamos.fmsData;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vamosys.adapter.RemainderAdapter;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.SingleTonClass;
import com.vamosys.vamos.R;

public class RemainderActivity extends AppCompatActivity {
    LinearLayout remainderlayou,servicelayout;
    TextView remaindertxt,servicetxt,vehicletxt;
    ImageView remainderimg,serviceimg,back;

    RecyclerView mRecyclerView;
    TextView mTxtNoRecord;
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remainder);
        remainderlayou = (LinearLayout)findViewById(R.id.fleet_layout);
        servicelayout = (LinearLayout)findViewById(R.id.servi_layout);
        remaindertxt = (TextView)findViewById(R.id.fleet_txt);
        vehicletxt = (TextView)findViewById(R.id.vehicle_txt);
        servicetxt = (TextView)findViewById(R.id.servi_txt);
        remainderimg = (ImageView) findViewById(R.id.fleet_image);
        serviceimg = (ImageView)findViewById(R.id.servi_image);
        back = (ImageView)findViewById(R.id.back);
        cd = new ConnectionDetector(RemainderActivity.this);
        mTxtNoRecord = (TextView) findViewById(R.id.txt_norecord);
        // Replace 'android.R.id.list' with the 'id' of your RecyclerView
        mRecyclerView = (RecyclerView)findViewById(R.id.remainder_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(RemainderActivity.this);
        mRecyclerView.setLayoutManager(layoutManager);
        vehicletxt.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);

        if (SingleTonClass.getInstance().getFmsReminderList().size() > 0) {
            Log.d("fmsremainder","success");
            mRecyclerView.setVisibility(View.VISIBLE);
            mTxtNoRecord.setVisibility(View.GONE);
            RemainderAdapter mAadp = new RemainderAdapter(SingleTonClass.getInstance().getFmsReminderList(), RemainderActivity.this);
            mRecyclerView.setAdapter(mAadp);
            mAadp.notifyDataSetChanged();
        } else {
            Log.d("fmsremainder","failure");
            mRecyclerView.setVisibility(View.GONE);
            mTxtNoRecord.setVisibility(View.VISIBLE);
        }


        remainderlayou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remainderimg.setColorFilter(getResources().getColor(R.color.yellow_dark));
                remaindertxt.setTextColor(getResources().getColor(R.color.yellow_dark));
                serviceimg.setColorFilter(getResources().getColor(R.color.black));
                servicetxt.setTextColor(getResources().getColor(R.color.black));
                startActivity(new Intent(RemainderActivity.this,FmsActivity.class));

            }
        });
        servicelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceimg.setColorFilter(getResources().getColor(R.color.yellow_dark));
                servicetxt.setTextColor(getResources().getColor(R.color.yellow_dark));
                remainderimg.setColorFilter(getResources().getColor(R.color.black));
                remaindertxt.setTextColor(getResources().getColor(R.color.black));
                startActivity(new Intent(RemainderActivity.this,ServiceActivity.class));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RemainderActivity.this,FmsActivity.class));
            }
        });

    }
}
