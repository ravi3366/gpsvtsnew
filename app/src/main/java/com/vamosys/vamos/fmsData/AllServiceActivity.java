package com.vamosys.vamos.fmsData;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.adapter.spinnerAdapter;
import com.vamosys.model.FmsDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.SingleTonClass;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AllServiceActivity extends AppCompatActivity {
    Spinner mSpinnerDocumentType;
    EditText mEdtCompanyName, mEdtDescription, mEdtNotifyInterval, mEdtMonthInterval, mEdtAmount,mEdtDocName;
    TextView mTxtDocumentType, mTxtDueDate,save,addservice,odometer_due,Document_name;
    ImageView back;
    TextView spinner_txt;
    String date,savedata,document,documentName;
    boolean firsttime = true;
    String Tagg = "check serivces";

    String mDocumentType, mCompanyName, mDescription, mNotifyInterval, mDueDate,DueDate, mAmount;
    private int year, month, day;
    ConnectionDetector cd;
    SharedPreferences sp;
    TextInputLayout companytxt,amountxt,descriptiontext;
    ArrayList<String> spinnerarraylist;
    String value;
    String[] splitedvalue;
    List<String> stringList;
    String type1,type2,date_after,date_select;
    String mMonthInterval = "8";
    String defaultTextForSpinner = "Select Type";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_service);
        initViews();

        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }


        Bundle b = getIntent().getExtras();

        if (b != null) {
            int mPosValue = b.getInt("select_pos");

            savedata = "editdata";
//
            mSpinnerDocumentType.setVisibility(View.VISIBLE);
            mTxtDocumentType.setVisibility(View.GONE);
            addservice.setText("Edit Service");
            odometer_due.setVisibility(View.VISIBLE);
            mEdtNotifyInterval.setVisibility(View.VISIBLE);
            Document_name.setVisibility(View.GONE);
            mEdtDocName.setVisibility(View.GONE);
            mEdtDocName.setEnabled(false);
            //setSpinnerData();
            setFmsData(mPosValue);



        } else {
            //set spinner list value and spinner enabled
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyy");

            Date date = new Date(); // *1000 is to convert
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, 1);
            Date currentDatePlusOne = c.getTime();
            String formattedDate =sdf.format(currentDatePlusOne);
            mTxtDueDate.setText(formattedDate);
           // mTxtDueDate.setText(Const.getDate());
            mSpinnerDocumentType.setVisibility(View.VISIBLE);
            mTxtDocumentType.setVisibility(View.GONE);
            setSpinnerData();
        }
    }

    private void initViews() {
//
//        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        mToolbar.setTitle(getResources().getString(R.string.add_fms_report));
//        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
//        setSupportActionBar(mToolbar);
//
//        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(AddFmsActivity.this, FmsActivity.class));
//                finish();
//            }
//        });
//
        mTxtDocumentType = (TextView) findViewById(R.id.txt_document);
        mSpinnerDocumentType = (Spinner) findViewById(R.id.spinner_document_type);
        mEdtCompanyName = (EditText) findViewById(R.id.edt_company_name);
        mEdtAmount = (EditText) findViewById(R.id.edt_amount);
        mTxtDueDate = (TextView) findViewById(R.id.txt_due_date);
        addservice = (TextView) findViewById(R.id.add_service);
        odometer_due = (TextView)findViewById(R.id.odometer_due);
        Document_name=(TextView)findViewById(R.id.Document_Name);
        mEdtNotifyInterval = (EditText) findViewById(R.id.edt_notify_interval);
       // mEdtMonthInterval = (EditText) findViewById(R.id.edt_month_interval);
        mEdtDescription = (EditText) findViewById(R.id.edt_description);
        mEdtDocName=(EditText)findViewById(R.id.edt_Document_Name);
       save = (TextView)findViewById(R.id.save);
        back = (ImageView)findViewById(R.id.back);
        companytxt = (TextInputLayout) findViewById(R.id.textinput_company);
        amountxt = (TextInputLayout) findViewById(R.id.textinput_amount);
        descriptiontext = (TextInputLayout) findViewById(R.id.textinput_description);


        mSpinnerDocumentType
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub
//                        mDocumentType = SingleTonClass.getInstance().getmServiceList().get(pos);
//                        Log.d("mDocumentType",""+mDocumentType);
                        if(mSpinnerDocumentType.getSelectedItem().toString().contains("OTHERS")){
                            Log.d("check","others1");
                            mDocumentType="OTHERS";
                        }
                        else if(savedata!=null && savedata.equals("editdata")){
                            if(mSpinnerDocumentType.getSelectedItem().toString().contains("FULL SERVICE")){
                                Log.d("check","Full service");
                                mDocumentType =mSpinnerDocumentType.getSelectedItem().toString();
                            }
                            else {
                                Log.d("check","others2");
                                mDocumentType = "OTHERS";
                            }
                        }
                        else{
                            mDocumentType =mSpinnerDocumentType.getSelectedItem().toString();
                        }
                        Log.d("mDocumentType",""+mDocumentType);

                        if( savedata!=null && savedata.contains("editdata")){
                            switch (mDocumentType){
                                case "OTHERS":
                                    odometer_due.setVisibility(View.GONE);
                                    mEdtNotifyInterval.setVisibility(View.GONE);
                                    Document_name.setVisibility(View.VISIBLE);
                                    mEdtDocName.setVisibility(View.VISIBLE);
                                    break;

                                case "FULL SERVICE":
                                    odometer_due.setVisibility(View.VISIBLE);
                                    mEdtNotifyInterval.setVisibility(View.VISIBLE);
                                    Document_name.setVisibility(View.GONE);
                                    mEdtDocName.setVisibility(View.GONE);
                                    break;

                                    default:
                                        odometer_due.setVisibility(View.VISIBLE);
                                        mEdtNotifyInterval.setVisibility(View.VISIBLE);
                                        Document_name.setVisibility(View.GONE);
                                        mEdtDocName.setVisibility(View.GONE);
                                        break;
                            }
                            }
                        else if(mDocumentType.contains("OTHERS")){
                            odometer_due.setVisibility(View.GONE);
                            mEdtNotifyInterval.setVisibility(View.GONE);
                            Document_name.setVisibility(View.VISIBLE);
                            mEdtDocName.setVisibility(View.VISIBLE);
                        }
                        else{
                            odometer_due.setVisibility(View.VISIBLE);
                            mEdtNotifyInterval.setVisibility(View.VISIBLE);
                            Document_name.setVisibility(View.GONE);
                            mEdtDocName.setVisibility(View.GONE);
                        }








                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });

        mTxtDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  validateAllTheFields();
                if (validAllTheFields()) {

                    mCompanyName = mEdtCompanyName.getText().toString().trim();
                    mDescription = mEdtDescription.getText().toString().trim();
                    mAmount = mEdtAmount.getText().toString().trim();
                    mDueDate = mTxtDueDate.getText().toString().trim();
                    documentName=mEdtDocName.getText().toString().trim();


                    date_after = formateDateFromstring("dd-MM-yyyy", "yyyy-MM-dd", mDueDate);
                    Log.d("mDueDate",""+mDueDate);
                    Log.d("date_after",""+date_after);
                    mNotifyInterval = mEdtNotifyInterval.getText().toString().trim();
                   // mMonthInterval = mEdtMonthInterval.getText().toString().trim();

                    if (cd.isConnectingToInternet()) {
                        if (savedata!=null){
                            Log.d(Tagg,"Edited data");
                            new postEditData().execute();
                        } else{
                            Log.d(Tagg,"save data");
                            new postFmsData().execute();
                        }
                    }

                } else {
                    Toast.makeText(AllServiceActivity.this, "All the fields are mandatory", Toast.LENGTH_SHORT).show();
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AllServiceActivity.this, FmsActivity.class));
                finish();
            }
        });


    }
    private void setFmsData(int pos) {

        System.out.println("Hi fms data list size is " + SingleTonClass.getInstance().getmServicedata().size() + " and pos " + pos);

        FmsDto fm = SingleTonClass.getInstance().getmServicedata().get(pos);
        if(fm.getDocumentType().contains("OTHERS:")){
            document = "OTHERS";
        }
        else{
            document = fm.getDocumentType();
        }
        mTxtDocumentType.setText(fm.getDocumentType());
        mEdtCompanyName.setText(fm.getCompanyName());
        mEdtDescription.setText(fm.getDescription());
        mEdtNotifyInterval.setText(fm.getOdometerDue());
       // mEdtMonthInterval.setText(fm.getMonthInterval());
        mEdtAmount.setText(fm.getAmount());
        mEdtDocName.setText(fm.getDocumentName());
        // mTxtDueDate.setText(fm.getDueDate());
        date = fm.getDueDate();
        date_after = formateDateFromstring("yyyy-MM-dd","dd-MM-yyyy", date);
        mTxtDueDate.setText(date_after);

        String[] list = new String[] {document};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spiner_adapter);
        adapter.addAll(list);
        adapter.add("Select");
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerDocumentType.setAdapter(adapter);
    }

    private void setSpinnerData() {

//
//        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
//                AllServiceActivity.this, R.layout.spiner_adapter,SingleTonClass.getInstance().getmServiceList()) {
//
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//                 spinner_txt = (TextView)v.findViewById(R.id.cust);
//                if (firsttime){
//                firsttime = false;
//                        spinner_txt.setText("Select Type");
//                        Log.d("firstime",""+SingleTonClass.getInstance().getmServiceList().get(position));
//
//                } else{
//                    Log.d("secondtime",""+SingleTonClass.getInstance().getmServiceList().get(position));
//                    spinner_txt.setText(SingleTonClass.getInstance().getmServiceList().get(position));
//
//                }
//
//
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView,
//                                        ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                return v;
//            }
//        };
//        spinnerArrayAdapter
//                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        mSpinnerDocumentType.setAdapter(spinnerArrayAdapter);

        spinnerAdapter adapter = new spinnerAdapter(AllServiceActivity.this, R.layout.spiner_adapter);
        adapter.addAll(SingleTonClass.getInstance().getmServiceList());
        adapter.add("Select Type");
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerDocumentType.setAdapter(adapter);
        mSpinnerDocumentType.setSelection(adapter.getCount());

    }


    private void showDateDialog() {

//        if (mTxtDueDate.getText() != null && mTxtDueDate.getText().toString().trim().length() > 0) {
//            String[] dateArray = mTxtDueDate.getText().toString().trim().split("-");
//            year = Integer.parseInt(dateArray[0].trim());
//            month = Integer.parseInt(dateArray[1].trim()) - 1;
//            day = Integer.parseInt(dateArray[2].trim());
//        } else {
            Calendar c = Calendar.getInstance();

            // Current Hour
            year = c.get(Calendar.YEAR);
            // Current Minute
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DATE);


       // }
        /** set date picker as current date */
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
        c.add(Calendar.DATE, 1);
        dialog.getDatePicker().setMinDate(c.getTimeInMillis());
        dialog.show();
//        DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//                mTxtDueDate.setText(dayOfMonth + "/" + (month + 1) + "/"+ year);
//            }
//        },year,month,day);
//        dialog.show();

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            day = selectedDay;
            month = selectedMonth;
            year = selectedYear;

            StringBuilder mDate = new StringBuilder().append(day).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(year))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");


            // mDueDate = String.valueOf(mDate2);

            mTxtDueDate.setText(selectedDay + "-" + (selectedMonth + 1) + "-"+ selectedYear);
            mDueDate = selectedDay + "/" + (selectedMonth + 1) + "/"+ selectedYear;
           // mTxtDueDate.setText(date_select);

            date_after = formateDateFromstring("dd-MM-yyyy", "yyyy-MM-dd", mDueDate);
            // mTxtDueDate.setText(mDate2);\
            Log.d("mDueDate",""+mDueDate);


        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }
    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {

        }

        return outputDate;

    }

    private boolean validAllTheFields() {

        if (mDocumentType == "Select Type") {
            Toast.makeText(this, "Please select Document type", Toast.LENGTH_SHORT).show();

//            boolean check = true;
//
//            if (null != this.mEdtAmount.getText().toString() &&
//                    !this.mEdtAmount.getText().toString().trim().equals("")) {
//                int amount = Integer.parseInt(this.mEdtAmount.getText().toString());
//                if(amount == 0){
//                    this.mEdtAmount.setError("Value should be greater than zero");
//                    check = false;
//                }
//            }else{
//                this.mEdtAmount.setError("Please enter the amount");
//                check = false;
//            }
//            if(this.mEdtCompanyName.getText().toString().trim().length() == 0){
//                this.mEdtCompanyName.setError("Please enter the company name");
//                check = false;
//            }
//            if(this.mTxtDueDate.getText().toString().trim().length() == 0){
//                this.mTxtDueDate.setError("Please selecct the date");
//                check = false;
//            }
//
//            if (null != this.mEdtNotifyInterval.getText().toString() &&
//                    !this.mEdtNotifyInterval.getText().toString().trim().equals("")) {
//                int notifyInterval = Integer.parseInt(this.mEdtNotifyInterval.getText().toString());
//                if(notifyInterval == 0){
//                    this.mEdtNotifyInterval.setError("Value should be greater than zero");
//                    check = false;
//                }
//            }else{
//                this.mEdtNotifyInterval.setError("Please enter the value");
//                check = false;
//            }
//
//            if(this.mEdtDescription.getText().toString().trim().length() == 0){
//                this.mEdtDescription.setError("Please enter the text");
//                check = false;
//            }
//            return check ;


        } else {
            boolean check = true;

            if (null != this.mEdtAmount.getText().toString() &&
                    !this.mEdtAmount.getText().toString().trim().equals("")) {
                int amount = Integer.parseInt(this.mEdtAmount.getText().toString());
                if(amount == 0){
                    this.mEdtAmount.setError("Value should be greater than zero");
                    check = false;
                }
            }else{
                this.mEdtAmount.setError("Please enter the amount");
                check = false;
            }
            if(this.mEdtCompanyName.getText().toString().trim().length() == 0){
                this.mEdtCompanyName.setError("Please enter the company name");
                check = false;
            }
            if(this.mTxtDueDate.getText().toString().trim().length() == 0){
                this.mTxtDueDate.setError("Please selecct the date");
                check = false;
            }

            if(mEdtDocName.getText().toString()!=null){

            }
            else{
                if (null != this.mEdtNotifyInterval.getText().toString() &&
                        !this.mEdtNotifyInterval.getText().toString().trim().equals("")) {
                    int notifyInterval = Integer.parseInt(this.mEdtNotifyInterval.getText().toString());
                    if(notifyInterval == 0){
                        this.mEdtNotifyInterval.setError("Value should be greater than zero");
                        check = false;
                    }
                }else{
                    this.mEdtNotifyInterval.setError("Please enter the value");
                    check = false;
                }
            }


            if(this.mEdtDescription.getText().toString().trim().length() == 0){
                this.mEdtDescription.setError("Please enter the text");
                check = false;
            }
            return check ;
//            Toast.makeText(this, "Please select Document type", Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    private class postFmsData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String mUrl;
            String result = null;
            try {
                HttpConfig ht = new HttpConfig();

                if(mDocumentType.contains("OTHERS")){
//                    http://188.166.244.126:9000/getFmsDetails?vehicleId=PAMUSA20&documentType=OTHERS:BALA&dueDate=2019-10-20
//                    &notifyInterval=&monthInterval=&amount=92902&issuingCompany=BALA09&note=DMHVDHJDVKJDB&userId=MSS?
                    Log.d(Tagg,"OTHERS");
                    mUrl = Const.API_URL + "/mobile/getFmsDetails?vehicleId=" + Constant.SELECTED_VEHICLE_ID
                            + "&documentType=" + mDocumentType+":"+documentName + "&dueDate=" + date_after + "&notifyInterval=" + mNotifyInterval
                            + "&monthInterval=" + mMonthInterval + "&amount="
                            + mAmount + "&issuingCompany=" + mCompanyName + "&note=" + mDescription+"&userId="+Constant.SELECTED_USER_ID;
                }
                else {
                    Log.d(Tagg,"services");

                   mUrl  = Const.API_URL + "/mobile/getFmsDetails?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID
                            + "&documentType=" + mDocumentType + "&dueDate=" + date_after + "&notifyInterval=" + mNotifyInterval + "&monthInterval=" + mMonthInterval + "&amount="
                            + mAmount + "&issuingCompany=" + mCompanyName + "&note=" + mDescription;

                }

//                String mUrl = Const.API_URL + "/mobile/getFmsDetails?userId=DEMO&vehicleId=TVS-HR47C2298&documentType=" + mDocumentType + "&dueDate=" + mDueDate + "&notifyInterval=" + mNotifyInterval + "&monthInterval=" + mMonthInterval + "&amount="
//                        + mAmount + "&issuingCompany=" + mCompanyName + "&note=" + mDescription;

                result = ht.httpGet(mUrl.replaceAll(" ", "%20"));


            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();

//            System.out.println("Hi add fms resule is " + result);

            if (result != null && result.length() > 0) {
                try {
                    JSONObject jObj = new JSONObject(result.trim());

                    if (jObj.has("response")) {
                        try {
                            if (jObj.getString("response") != null) {
                                if (jObj.getString("response").trim().equalsIgnoreCase("success")) {

                                    startActivity(new Intent(AllServiceActivity.this, ServiceActivity.class));
                                    finish();


                                } else {

                                    Toast.makeText(AllServiceActivity.this, jObj.getString("error"), Toast.LENGTH_SHORT).show();


                                }
                            } else {
                                Toast.makeText(AllServiceActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(AllServiceActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(AllServiceActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(AllServiceActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    private class postEditData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            String mUrl;
            try {
                HttpConfig ht = new HttpConfig();
                if(mDocumentType.contains("OTHERS")){
//                    http://188.166.244.126:9000/getFmsDetails?vehicleId=PAMUSA20&documentType=OTHERS:BALA&dueDate=2019-10-20
//                    &notifyInterval=&monthInterval=&amount=92902&issuingCompany=BALA09&note=DMHVDHJDVKJDB&userId=MSS?
                    Log.d("documentName","OTHERS");
                    mUrl = Const.API_URL + "/mobile/getFmsDetails?vehicleId=" + Constant.SELECTED_VEHICLE_ID
                            + "&documentType=" + mDocumentType+":"+documentName + "&dueDate=" + date_after + "&notifyInterval=" + mNotifyInterval
                            + "&monthInterval=" + mMonthInterval + "&amount="
                            + mAmount + "&issuingCompany=" + mCompanyName + "&note=" + mDescription+"&userId="+Constant.SELECTED_USER_ID;
                }
                else {
                    Log.d("documentName","slellkkjk");
                    mUrl = Const.API_URL + "/mobile/getFmsDetails?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID
                            + "&documentType=" + document + "&dueDate=" + date_after + "&notifyInterval=" + mNotifyInterval + "&monthInterval=" + mMonthInterval + "&amount="
                            + mAmount + "&issuingCompany=" + mCompanyName + "&note=" + mDescription;
                }

//                String mUrl = Const.API_URL + "/mobile/getFmsDetails?userId=DEMO&vehicleId=TVS-HR47C2298&documentType=" + mDocumentType + "&dueDate=" + mDueDate + "&notifyInterval=" + mNotifyInterval + "&monthInterval=" + mMonthInterval + "&amount="
//                        + mAmount + "&issuingCompany=" + mCompanyName + "&note=" + mDescription;

                result = ht.httpGet(mUrl.replaceAll(" ", "%20"));


            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();

//            System.out.println("Hi add fms resule is " + result);

            if (result != null && result.length() > 0) {
                try {
                    JSONObject jObj = new JSONObject(result.trim());

                    if (jObj.has("response")) {
                        try {
                            if (jObj.getString("response") != null) {
                                if (jObj.getString("response").trim().equalsIgnoreCase("success")) {

                                    startActivity(new Intent(AllServiceActivity.this, ServiceActivity.class));
                                    finish();


                                } else {

                                    Toast.makeText(AllServiceActivity.this, jObj.getString("error"), Toast.LENGTH_SHORT).show();


                                }
                            } else {
                                Toast.makeText(AllServiceActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(AllServiceActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(AllServiceActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(AllServiceActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(AllServiceActivity.this, ServiceActivity.class));
        finish();
    }

}
