package com.vamosys.vamos.fmsData;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.adapter.spinnerAdapter;
import com.vamosys.model.FmsDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.SingleTonClass;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddFmsActivity extends AppCompatActivity {
    Toolbar mToolbar;
    Spinner mSpinnerDocumentType;
    EditText mEdtCompanyName, mEdtDescription, mEdtNotifyInterval, mEdtAmount;
    TextView mTxtDocumentType, mTxtDueDate,save,addfms,mEdtMonthInterval;
    ImageView back;
    String date,document, notifyday;
    long diffDays;
    int editvalue;


    String mDocumentType, mCompanyName, mDescription, mNotifyInterval, mMonthInterval, mDueDate,DueDate, mAmount;
    private int year, month, day;
    ConnectionDetector cd;
    SharedPreferences sp;
    TextInputLayout companytxt,amountxt,descriptiontext;
    ArrayList<String> spinnerarraylist;
    String value;
    String[] splitedvalue;
    List<String> stringList;
    String type1,type2,date_after,formattedDate;
    int mPosValue ,editval ;
    String savedata;

//    ArrayList<String> mRemainingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_fms_layout);
        initViews();

        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }


        Bundle b = getIntent().getExtras();

        if (b != null) {
             mPosValue = b.getInt("selected_pos");
              savedata = "editdata";


//
          mSpinnerDocumentType.setVisibility(View.VISIBLE);
            mTxtDocumentType.setVisibility(View.GONE);
            addfms.setText("Edit FMS");


            //setSpinnerData();
            setFmsData(mPosValue);



        } else {
            //set spinner list value and spinner enabled
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyy");

            Date date = new Date(); // *1000 is to convert
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, 2);
            Date currentDatePlusOne = c.getTime();
             formattedDate =sdf.format(currentDatePlusOne);
            mTxtDueDate.setText(formattedDate);

           // mTxtDueDate.setText(Const.getDate());
          mSpinnerDocumentType.setVisibility(View.VISIBLE);
           mTxtDocumentType.setVisibility(View.GONE);
           setSpinnerData();
        }

    }



    private void initViews() {
//
//        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        mToolbar.setTitle(getResources().getString(R.string.add_fms_report));
//        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
//        setSupportActionBar(mToolbar);
//
//        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(AddFmsActivity.this, FmsActivity.class));
//                finish();
//            }
//        });
//
       mTxtDocumentType = (TextView) findViewById(R.id.txt_document);
       mSpinnerDocumentType = (Spinner) findViewById(R.id.spinner_document_type);
        mEdtCompanyName = (EditText) findViewById(R.id.edt_company_name);
        mEdtAmount = (EditText) findViewById(R.id.edt_amount);
        mTxtDueDate = (TextView) findViewById(R.id.txt_due_date);
        mEdtNotifyInterval = (EditText) findViewById(R.id.edt_notify_interval);
        mEdtMonthInterval = (TextView) findViewById(R.id.edt_month_interval);
        mEdtDescription = (EditText) findViewById(R.id.edt_description);
        save = (TextView)findViewById(R.id.save);
        addfms = (TextView)findViewById(R.id.add_fms);
        back = (ImageView)findViewById(R.id.back);
        companytxt = (TextInputLayout) findViewById(R.id.textinput_company);
        amountxt = (TextInputLayout) findViewById(R.id.textinput_amount);
        descriptiontext = (TextInputLayout) findViewById(R.id.textinput_description);



        mSpinnerDocumentType
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub

                        if (mSpinnerDocumentType.getSelectedItem().toString() == "Select Type"){

                        } else {

                               if(SingleTonClass.getInstance().getRemainingList().size()!=0){
                                   Log.d("mDocumentType","success");
                                   mDocumentType = SingleTonClass.getInstance().getRemainingList().get(pos);
                                Log.d("mDocumentlist",""+mDocumentType);
                                String[] separate = mDocumentType.split(":");
                                type1 = separate[0];
                                type2 = separate[1];
                                Log.d("type2",""+type2);
                                Bundle b = getIntent().getExtras();
                                if (b!=null){

                                } else{
                                    mEdtMonthInterval.setText(type2);
                                }
                               } else {
                                   Log.d("mDocumentType","failre");
                               }
//





                                // mEdtMonthInterval.setText(type2);

                            }







                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });

        mTxtDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                              //  validateAllTheFields();

                if (validAllTheFields()) {



                    mCompanyName = mEdtCompanyName.getText().toString().trim();
                    mDescription = mEdtDescription.getText().toString().trim();
                    mAmount = mEdtAmount.getText().toString().trim();
                    mDueDate = mTxtDueDate.getText().toString().trim();
                     date_after = formateDateFromstring("dd-MM-yyyy", "yyyy-MM-dd", mDueDate);
                    mNotifyInterval = mEdtNotifyInterval.getText().toString().trim();
                    mMonthInterval = mEdtMonthInterval.getText().toString().trim();
//
//                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
//
//                    Date d1 = null;
//                    Date d2 = null;
//
//                    try {
//                        d1 = simpleDateFormat.parse(formattedDate);
//                        d2 = simpleDateFormat.parse(mTxtDueDate.getText().toString().trim());
//                        Log.d("date2",""+mTxtDueDate.getText().toString().trim());
//                        Log.d("date1",""+formattedDate);
//
//
//                        //in milliseconds
//                        long diff = d2.getTime() - d1.getTime();
//                         diffDays = diff / (24 * 60 * 60 * 1000);
//                        Log.d("diffDays",""+diffDays);
//
//
//                    } catch (Exception e) {
//                        Log.d("dateformt","failure");
//                        e.printStackTrace();
//                    }

                    if (cd.isConnectingToInternet()) {
                        if (savedata!=null){
                            new postEditData().execute();
                        } else{
                            new postFmsData().execute();
                        }

                    }

                } else {
                    Toast.makeText(AddFmsActivity.this, "All the fields are mandatory", Toast.LENGTH_SHORT).show();
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddFmsActivity.this, FmsActivity.class));
                finish();
            }
        });


    }

    private void setFmsData(int pos) {

        System.out.println("Hi fms data list size is " + SingleTonClass.getInstance().getFmsData().size() + " and pos " + pos);

        FmsDto fm = SingleTonClass.getInstance().getFmsData().get(pos);
        document = fm.getDocumentType();
        Log.d("document",""+document);
       mTxtDocumentType.setText(fm.getDocumentType());
        mEdtCompanyName.setText(fm.getCompanyName());
        mEdtDescription.setText(fm.getDescription());
        mEdtNotifyInterval.setText(fm.getNotifyInterval());
        mEdtMonthInterval.setText(fm.getMonthInterval());
        Log.d("monthinterval",""+fm.getMonthInterval());
        Log.d("interval",""+type2);
        mEdtAmount.setText(fm.getAmount());
       // mTxtDueDate.setText(fm.getDueDate());
        date = fm.getDueDate();
        date_after = formateDateFromstring("yyyy-MM-dd","dd-MM-yyyy", date);
        mTxtDueDate.setText(date_after);

                String[] list = new String[] {document};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spiner_adapter, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerDocumentType.setAdapter(adapter);

    }

    private void setSpinnerData() {
     spinnerarraylist = new ArrayList<String>();
        spinnerarraylist = SingleTonClass.getInstance().getRemainingList();
        stringList = new ArrayList<String>();
        for (int i=0;i<spinnerarraylist.size();i++){
            splitedvalue = spinnerarraylist.get(i).toString().split(":");

             value = splitedvalue[0];
            stringList.add(value);

            String valuetext = splitedvalue[1];
        }

//        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
//                AddFmsActivity.this, R.layout.spiner_adapter,stringList) {
//
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView,
//                                        ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                return v;
//            }
//        };
//        spinnerArrayAdapter
//                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        mSpinnerDocumentType.setAdapter(spinnerArrayAdapter);

//
//        String[] list = new String[] {document};
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                R.layout.spiner_adapter, list);
//        mSpinnerDocumentType.setAdapter(adapter);

        spinnerAdapter adapter = new spinnerAdapter(AddFmsActivity.this, R.layout.spiner_adapter);
        adapter.addAll(stringList);
        adapter.add("Select Type");
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerDocumentType.setAdapter(adapter);
        mSpinnerDocumentType.setSelection(adapter.getCount());
    }


    private void showDateDialog() {
//
//        if (mTxtDueDate.getText() != null && mTxtDueDate.getText().toString().trim().length() > 0) {
//            String[] dateArray = mTxtDueDate.getText().toString().trim().split("-");
//            year = Integer.parseInt(dateArray[0].trim());
//            month = Integer.parseInt(dateArray[1].trim()) - 1;
//            day = Integer.parseInt(dateArray[2].trim());
//        } else {
            Calendar c = Calendar.getInstance();

            // Current Hour
            year = c.get(Calendar.YEAR);
            // Current Minute
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DATE);


      //  }
        /** set date picker as current date */
//        DatePickerDialog dialog = new DatePickerDialog(this,
//                datePickerListener, year, month, day);
//        dialog.show();
//        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
//                new DatePickerDialog.OnDateSetListener() {
//
//                    @Override
//                    public void onDateSet(DatePicker view, int year,
//                                          int monthOfYear, int dayOfMonth) {
////
////                                    mTxtDueDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-"+ year);
////            mDueDate = dayOfMonth + "/" + (monthOfYear + 1) + "/"+ year;
////            date_after = formateDateFromstring("dd-MM-yyyy", "yyyy-MM-dd", mDueDate);
////           // mTxtDueDate.setText(mDate2);
////            Log.d("mDueDate",""+mDueDate);
//
//                    }
//                }, year, month, day);
//        datePickerDialog.show();

        DatePickerDialog  datePickerDialog = new DatePickerDialog(AddFmsActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text

                                    mTxtDueDate.setText((dayOfMonth) + "-" + (monthOfYear + 1) + "-"+ year);
                                     notifyday = (dayOfMonth) + "-" + (monthOfYear + 1) + "-"+ year;
            mDueDate = dayOfMonth + "/" + (monthOfYear + 1) + "/"+ year;
            date_after = formateDateFromstring("dd-MM-yyyy", "yyyy-MM-dd", mDueDate);
            //mTxtDueDate.setText(date_after);
            Log.d("mDueDate",""+mDueDate);

                    }
                }, year, month, day);
        c.add(Calendar.DATE, 2);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());

        datePickerDialog.show();


    }

//    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
//
//        // when dialog box is closed, below method will be called.
//        public void onDateSet(DatePicker view, int selectedYear,
//                              int selectedMonth, int selectedDay) {
//
//            year = selectedYear;
//            month = selectedMonth;
//            day = selectedDay;
//
//
//
//            StringBuilder mDate = new StringBuilder().append(day).append(":")
//                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(year))
//                    .append("");
//
//            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
//                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
//                    .append(" ");
//
//
//           // mDueDate = String.valueOf(mDate2);
//
//            mTxtDueDate.setText(selectedDay + "-" + (selectedMonth + 1) + "-"+ selectedYear);
//            mDueDate = selectedDay + "/" + (selectedMonth + 1) + "/"+ selectedYear;
//            date_after = formateDateFromstring("dd-MM-yyyy", "yyyy-MM-dd", mDueDate);
//           // mTxtDueDate.setText(mDate2);
//            Log.d("mDueDate",""+mDueDate);
//
//
//        }
//    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            // action with ID action_refresh was selected
//            case R.id.action_done:
//
////                validateAllTheFields();
//
//                if (validAllTheFields()) {
//
//                    mCompanyName = mEdtCompanyName.getText().toString().trim();
//                    mDescription = mEdtDescription.getText().toString().trim();
//                    mAmount = mEdtAmount.getText().toString().trim();
//                    mDueDate = mTxtDueDate.getText().toString().trim();
//                    mNotifyInterval = mEdtNotifyInterval.getText().toString().trim();
//                    mMonthInterval = mEdtMonthInterval.getText().toString().trim();
//
//                    if (cd.isConnectingToInternet()) {
//                        new postFmsData().execute();
//                    }
//
//                } else {
//                    Toast.makeText(this, "All the fields are mandatory", Toast.LENGTH_SHORT).show();
//                }
//
//                break;
//
//
//            default:
//                break;
//        }

        return true;
    }
    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {

        }

        return outputDate;

    }

    private boolean validAllTheFields() {

        if (mDocumentType != null || SingleTonClass.getInstance().getRemainingList().size() == 0) {
            boolean check = true;

            if (null != this.mEdtAmount.getText().toString() &&
                    !this.mEdtAmount.getText().toString().trim().equals("")) {
                int amount = Integer.parseInt(this.mEdtAmount.getText().toString());
                if(amount == 0){
                    this.mEdtAmount.setError("Value should be greater than zero");
                    check = false;
                }
            }else{
                this.mEdtAmount.setError("Please enter the amount");
                check = false;
            }
            if(this.mEdtCompanyName.getText().toString().trim().length() == 0){
                this.mEdtCompanyName.setError("Please enter the company name");
                check = false;
            }
            if(this.mTxtDueDate.getText().toString().trim().length() == 0){
                this.mTxtDueDate.setError("Please select the date");
                check = false;
            }

//            Calendar calendar = Calendar.getInstance();
//            SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm");
//            String strDate = mdformat.format(calendar.getTime());



            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

            Date d1 = null;
            Date d2 = null;

            try {
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String formatted = df.format(c);
                Log.d("formatted",""+formatted);
//                String mystring = mTxtDueDate.getText().toString().trim() +strDate;
//                String duetatevalue = df.format(mystring);
//
//                d1 = simpleDateFormat.parse(formatted);
//                d2 = simpleDateFormat.parse(duetatevalue);
//                Log.d("date2",""+duetatevalue);
//                Log.d("date1",""+formatted);
//
//
//                //in milliseconds
//                long diff = d2.getTime() - d1.getTime();
//                diffDays = diff / (24 * 60 * 60 * 1000);
//                Log.d("diffDays",""+diffDays);

                d1 = simpleDateFormat.parse(formatted);
                d2 = simpleDateFormat.parse(mTxtDueDate.getText().toString().trim());
                Log.d("date2",""+mTxtDueDate.getText().toString().trim());
                Log.d("date1",""+formatted);


                //in milliseconds
                long diff = d2.getTime() - d1.getTime();
                diffDays = diff / (24 * 60 * 60 * 1000);
                Log.d("diffDays",""+diffDays);


            } catch (Exception e) {
                Log.d("dateformt","failure");
                e.printStackTrace();
            }

            String notifyvalue = mEdtNotifyInterval.getText().toString();
            int number = (int)diffDays;
            int notifyva = Integer.parseInt(notifyvalue);
            Log.d("numbbeers",""+number);
            Log.d("notifyva",""+notifyva);
//
            if (number < notifyva || number == notifyva){
    Log.d("notifffy","success");
                this.mEdtNotifyInterval.setError("Notify Interval should be less than two days of Due date");
    check = false;
            }

//            else {
//                this.mEdtNotifyInterval.setError("notivydays greater than date");
//                check = false;
//                Log.d("notifffy","failure");
//            }




            if (null != this.mEdtNotifyInterval.getText().toString() &&
                    !this.mEdtNotifyInterval.getText().toString().trim().equals("")) {
                int notifyInterval = Integer.parseInt(this.mEdtNotifyInterval.getText().toString());
                if(notifyInterval == 0){
                    this.mEdtNotifyInterval.setError("Value should be greater than zero");
                    check = false;
                }
            }else{
                this.mEdtNotifyInterval.setError("Please enter the days");
                check = false;
            }

            if(this.mEdtMonthInterval.getText().toString().trim().length() == 0){
                this.mEdtMonthInterval.setError("");
                check = false;
            }

            if(this.mEdtDescription.getText().toString().trim().length() == 0){
                this.mEdtDescription.setError("Please enter the text");
                check = false;
            }

         return check  ;
        } else {
            Toast.makeText(this, "Please select Document type", Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    private class postFmsData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();


                String mUrl = Const.API_URL + "/mobile/getFmsDetails?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID
                        + "&documentType=" + type1 + "&dueDate=" + date_after + "&notifyInterval=" + mNotifyInterval + "&monthInterval=" + mMonthInterval + "&amount="
                        + mAmount + "&issuingCompany=" + mCompanyName + "&note=" + mDescription;

//                String mUrl = Const.API_URL + "/mobile/getFmsDetails?userId=DEMO&vehicleId=TVS-HR47C2298&documentType=" + mDocumentType + "&dueDate=" + mDueDate + "&notifyInterval=" + mNotifyInterval + "&monthInterval=" + mMonthInterval + "&amount="
//                        + mAmount + "&issuingCompany=" + mCompanyName + "&note=" + mDescription;

                result = ht.httpGet(mUrl.replaceAll(" ", "%20"));


            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();

//            System.out.println("Hi add fms resule is " + result);

            if (result != null && result.length() > 0) {
                try {
                    JSONObject jObj = new JSONObject(result.trim());

                    if (jObj.has("response")) {
                        try {
                            if (jObj.getString("response") != null) {
                                if (jObj.getString("response").trim().equalsIgnoreCase("success")) {

                                    startActivity(new Intent(AddFmsActivity.this, FmsActivity.class));
                                    finish();


                                } else {

                                    Toast.makeText(AddFmsActivity.this, jObj.getString("error"), Toast.LENGTH_SHORT).show();


                                }
                            } else {
                                Toast.makeText(AddFmsActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(AddFmsActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(AddFmsActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddFmsActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private class postEditData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();


                String mUrl = Const.API_URL + "/mobile/getFmsDetails?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID
                        + "&documentType=" + document + "&dueDate=" + date_after + "&notifyInterval=" + mNotifyInterval + "&monthInterval=" + mMonthInterval + "&amount="
                        + mAmount + "&issuingCompany=" + mCompanyName + "&note=" + mDescription;

//                String mUrl = Const.API_URL + "/mobile/getFmsDetails?userId=DEMO&vehicleId=TVS-HR47C2298&documentType=" + mDocumentType + "&dueDate=" + mDueDate + "&notifyInterval=" + mNotifyInterval + "&monthInterval=" + mMonthInterval + "&amount="
//                        + mAmount + "&issuingCompany=" + mCompanyName + "&note=" + mDescription;

                result = ht.httpGet(mUrl.replaceAll(" ", "%20"));


            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();

//            System.out.println("Hi add fms resule is " + result);

            if (result != null && result.length() > 0) {
                try {
                    JSONObject jObj = new JSONObject(result.trim());

                    if (jObj.has("response")) {
                        try {
                            if (jObj.getString("response") != null) {
                                if (jObj.getString("response").trim().equalsIgnoreCase("success")) {

                                    startActivity(new Intent(AddFmsActivity.this, FmsActivity.class));
                                    finish();


                                } else {

                                    Toast.makeText(AddFmsActivity.this, jObj.getString("error"), Toast.LENGTH_SHORT).show();


                                }
                            } else {
                                Toast.makeText(AddFmsActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(AddFmsActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(AddFmsActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddFmsActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(AddFmsActivity.this, FmsActivity.class));
        finish();
    }
}
