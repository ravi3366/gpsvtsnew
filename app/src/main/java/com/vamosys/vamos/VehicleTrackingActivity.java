package com.vamosys.vamos;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.model.DataBaseHandler;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polyline;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import at.markushi.ui.CircleButton;

/**
 * Created by wifin on 1/23/2016.
 */
public class VehicleTrackingActivity extends FragmentActivity implements View.OnClickListener {

    //    GoogleMap map;
    TextView id_refresh_countdowntimer;

    int width, height;
    ImageView $BackPress, $Refresh, $HeadImage, $PoiClose, $ChangeView;
    TextView $HeadTxt, $SpeedLable, $DistanceLable, $SpeedValue,
            $DistanceValue, $Address, $AddressLable, $TxtView1, $TxtView2, $PoiLable;
    Button $AddPoiButton, $SavePoi;
    CircleButton history_tracking;
    ImageView $HeadButton;
    LinearLayout $HeadLayout, $SpeedLableLayout, $SpeedValueLayout, $HiddenLayout, $POILayout, $AddresswLAyout;
    //Fragment $Fragment;
    EditText $AddressEdit;
    String mSELECTED_MAP_TYPE = "Normal";
    LinearLayout mGoogleMapLayout;
    private long startTime;
    private long trackServiceTimeInterval;

    private long refresh_rate;
    private long timeElapsed;
    private final long interval = 1000;
    private boolean timerHasStarted = false;
    private RefreshCountDownTimer countDownTimer;
    DBHelper dbhelper;
    SharedPreferences sp;
    Handler handler = new Handler();
    double lat, lng;
    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");
    double currlat,currlng,startlat,startlng;


    // float vehicle_zoom_level = 15.5F;
    //float group_zoom_level = 13.5F;
    PendingIntent vehicletrackpendingIntent, cancel_pendingIntent;

    final ArrayList<Marker> current_users_location_point = new ArrayList<Marker>();
    final ArrayList<Marker> current_vehicle_location_marker = new ArrayList<Marker>();
    /**
     * Hardware access  declarations
     */
    String appID, macid;

    ProgressDialog progressDialog;
    AlarmManager vehicletrackalarm;
    String mSelectedVehicleId;
    ConnectionDetector cd;
//    private Marker trackingMarker;

    static Timer timer;


    /**
     * For Live tracking initialize variables
     */
//    LatLng mStartLocation = null, mEndLocation = null;
    private final Handler mHandler = new Handler();
    private static int ANIMATE_SPEEED = 10000;
    private static int ANIMATE_SPEEED_TURN = 10000;
    private static int BEARING_OFFSET = 20;
    float history_zoom_level = 14.5F;
    int mCursorIndexSize = 0;
    boolean mCursorAlreadyInitialized = false;
    View mIconMarker = null;
    String mPrevMarkerColor = null, mVehicleShortName = null;
    String mStartDate,mEndDate,mStartTime,mEndTime,shorttname;
    private boolean isdoubleclick = false;


    /**
     * Runnable to update the map with the seleced Vehicle and track the vehicle
     */
    Runnable runable = new Runnable() {
        @Override
        public void run() {
            Cursor getcurrent_vehicle_info = null;
            try {
                //  vehicle_info_layout.setVisibility(View.GONE);
                // id_refresh_tracking_icon.setVisibility(View.VISIBLE);
                // id_refresh_countdowntimer.setVisibility(View.VISIBLE);
                if (!timerHasStarted) {
                    countDownTimer.start();
                    timerHasStarted = true;
                }
//                ArrayList<LatLng> points = null;
//                PolylineOptions lineOptions = null;
//                MarkerOptions markerOptions = new MarkerOptions();
                getcurrent_vehicle_info = dbhelper.get_current_vehicle_details();

//                System.out.println("The Live vehicle lat and lng count is ::::::" + getcurrent_vehicle_info.getCount());

                if (getcurrent_vehicle_info.moveToFirst()) {
//                    PolylineOptions options = new PolylineOptions().width(8).color(getResources().getColor(R.color.history_polyline_color)).geodesic(true);
                    mCursorIndexSize = getcurrent_vehicle_info.getCount();
                    for (int z = 0; z < getcurrent_vehicle_info.getCount(); z++) {
                        ParsingClass parse = new ParsingClass();
                        parse.getCurrentVehicleInfo(getcurrent_vehicle_info.getString(0));
                        double lat = parse.getCurr_lat();
                        double lng = parse.getCurr_long();
//                        parse.get
                        GeoPoint position = new GeoPoint(lat, lng);
//                        options.add(position);

//                        System.out.println("The live next lat and lng is :::" + lat + " " + lng);

                        if (getcurrent_vehicle_info.getCount() != 1 && getcurrent_vehicle_info.getCount() > 1) {

                            if (mStartLocation == null) {
                                if (z == 0) {
                                    mStartLocation = position;
                                }

                            }

                            if (z == getcurrent_vehicle_info.getCount() - 1) {
                                mEndLocation = position;

                                if (parse.getAddress() != null) {
                                    $Address.setText(parse.getAddress());
                                } else {
                                    new GetAddressTask($Address).execute(lat, lng, 4.0);
                                }

                                //  current_vehicle_info_layout.setVisibility(View.VISIBLE);
                                $HeadTxt.setText(parse.getshortname());
                                $SpeedValue.setText(parse.getSpeed() + " KMS / HR");
                                // id_curr_odo_value.setText(parse.getodoDistance() + " KMS");
                                $DistanceValue.setText(parse.getDistanCovered() + " KMS");

//                            System.out.println("The vehicle id ::::" + parse.getVehicleId() + " ::: " + parse.getSpeed() + " ::: " + parse.getDistanCovered() + " :::: " + lat + " :: " + lng);

                                if (parse.getvehicleType().equals("Bus")) {
                                    Log.d("buss","bussssssssssss");
                                    $HeadImage.setImageResource(R.drawable.bus_east);
                                } else if (parse.getvehicleType().equals("Car")) {
                                    $HeadImage.setImageResource(R.drawable.car_east);
                                } else if (parse.getvehicleType().equals("Truck")) {
                                    $HeadImage.setImageResource(R.drawable.truck_east);
                                } else if (parse.getvehicleType().equals("Cycle")) {
                                    $HeadImage.setImageResource(R.drawable.cycle_east);
                                } else if (parse.getvehicleType().equalsIgnoreCase("Jcb")) {
                                    $HeadImage.setImageResource(R.drawable.jcb_icon);
                                }  else if (parse.getvehicleType().equalsIgnoreCase("BIke")) {
                                    $HeadImage.setImageResource(R.drawable.vehicle_bike);
                                } else if (parse.getvehicleType().equalsIgnoreCase("Ambulance")) {
                                    $HeadImage.setImageResource(R.drawable.ambulance);
                                }else if (parse.getvehicleType().equalsIgnoreCase("Van")) {
                                    $HeadImage.setImageResource(R.drawable.van1);
                                }


                                View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
                                ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
                                ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
                                if (!parse.getDirection().equals("")) {
                                    Constant.vehicle_direction_in_map(parse.getvehicleType(), parse.getDirection(), id_vehicle_in_marker);
                                    id_vehicle_in_marker.setColorFilter(Color.WHITE);

                                    }

                                if (parse.getOverSpeed().equalsIgnoreCase("Y")) {
                                    id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);

                                    $HeadImage.setImageDrawable(Constant.setVehicleImage("U", getApplicationContext(), parse.getvehicleType()));
                                } else if (parse.getGeoFence().equalsIgnoreCase("Y")) {
                                    $HeadImage.setImageDrawable(Constant.setVehicleImage("U", getApplicationContext(), parse.getvehicleType()));
                                    id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
                                } else if (parse.getPosition().equalsIgnoreCase("M")) {
                                    $HeadImage.setImageDrawable(Constant.setVehicleImage("M", getApplicationContext(), parse.getvehicleType()));
                                    id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
                                } else if (parse.getPosition().equalsIgnoreCase("P")) {
                                    $HeadImage.setImageDrawable(Constant.setVehicleImage("P", getApplicationContext(), parse.getvehicleType()));
                                    id_custom_marker_icon.setImageResource(R.drawable.black_custom_marker_icon);
                                } else if (parse.getPosition().equalsIgnoreCase("S")) {
                                    $HeadImage.setImageDrawable(Constant.setVehicleImage("S", getApplicationContext(), parse.getvehicleType()));
                                    id_custom_marker_icon.setImageResource(R.drawable.yellow_custom_marker_icon);
                                } else if (parse.getPosition().equalsIgnoreCase("N")) {
                                    $HeadImage.setImageDrawable(Constant.setVehicleImage("N", getApplicationContext(), parse.getvehicleType()));
                                    id_custom_marker_icon.setImageResource(R.drawable.yellow_custom_marker_icon);
                                } else if (parse.getPosition().equalsIgnoreCase("U")) {
                                    $HeadImage.setImageDrawable(Constant.setVehicleImage("U", getApplicationContext(), parse.getvehicleType()));
                                    id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
                                }
                                //  if(mPrevMarkerColor.equalsIgnoreCase())
                                mIconMarker = marker;
                                mVehicleShortName = parse.getshortname();
                            }
                        } else {
                            mStartLocation = position;
                            mEndLocation = mStartLocation;
                        }

                        getcurrent_vehicle_info.moveToNext();
                    }
                    if (mCursorAlreadyInitialized) {
                        animator.moveVehicleWithoutInit();
                    } else {
                        animator.startAnimation(true);
                    }

                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } else {
                    if (sp.getBoolean("tracking_exception", false)) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putBoolean("tracking_exception", false);
                        editor.commit();
                        cancelVehicleTracking();
                        animator.stopAnimation();
                        // home_button_functionality();
                        Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_LONG).show();
                        return;
                    }
                    Toast.makeText(getApplicationContext(), "Tracking... Please wait a moment", Toast.LENGTH_LONG).show();
                }
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(this, startTime);
            } catch (Exception e) {
                // TODO: handle exception
                // progressDialog.dismiss();
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } finally {
                //also call the same runnable
                if (getcurrent_vehicle_info != null) {
                    getcurrent_vehicle_info.close();
                }
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(this, startTime);
            }
        }
    };


    boolean m3Dmap = true;

    MapView mapview;
    private IMapController mapController;
    private Marker trackingMarker;
    private Marker historymarker;
    GeoPoint mStartLocation = null, mEndLocation = null;
    ArrayList<GeoPoint> polylinePoints = new ArrayList<GeoPoint>();
    ArrayList<GeoPoint> historypolyPoint = new ArrayList<GeoPoint>();
    View markerView;
    ImageView mImgMarkerIcon;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        //important! set your user agent to prevent getting banned from the osm servers
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.vehicle_tracking_layout);
        init();
        screenArrange();
        dbSetup();
        // progressDialog = MyCustomProgressDialog.ctor(VehicleTrackingActivity.this);
//        queryUserDB();
        dbhelper.deletecurrent_vehicleinfo();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        appID = getApplicationContext().getPackageName();
        WifiManager m_wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        macid = m_wm.getConnectionInfo().getMacAddress();
        if (appID != null && !appID.equals("")) {
            dbhelper.update_Settings_StdTableValues("appid", appID);
        }
        if (macid != null && !macid.equals("")) {
            dbhelper.update_Settings_StdTableValues("macid", macid);
        }

        m3Dmap = sp.getBoolean("animate_map_enabled", true);

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        mStartDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));

        Log.d("mStartDate",""+mStartDate);
        Log.d("mEndDate",""+mEndDate);
        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        mStartTime = timeFormat.format(cal.getTime());
        mEndTime = timeFormat.format(Calendar.getInstance().getTime());
        Log.d("mStartTime",""+mStartTime);
        Log.d("mEndTime",""+mEndTime);

        vehicletracking();
            if (mapview != null) {
                mapview.getOverlays().clear();
                mapview.invalidate();
                mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
            }
        history_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdoubleclick){
                    isdoubleclick = true;

                    history_tracking.setColor(getResources().getColor(R.color.bg_row_background));

                    Toast.makeText(VehicleTrackingActivity.this, "Tracking... Please wait a moment", Toast.LENGTH_SHORT).show();
                   // vehiclehistracking();
                    vehicletracking();
                } else {
                    history_tracking.setColor(getResources().getColor(R.color.colorPrimaryDark));
                    startActivity(new Intent(VehicleTrackingActivity.this,VehicleTrackingActivity.class));
                    //vehicletracking();
                    isdoubleclick=false;

                }
            }
        });

        $ChangeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeMapView();
            }
        });

    }

    void dbSetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
        mCursorAlreadyInitialized = false;
        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);
        mGoogleMapLayout.setVisibility(View.GONE);
//        mapview = (MapView) findViewById(R.id.osm_tracking_map);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);
        rl.setVisibility(View.VISIBLE);
        mapview = new MapView(this);
        mapview.setTilesScaledToDpi(true);
        rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                RelativeLayout.LayoutParams.FILL_PARENT));
//        mapview.setTileSource(TileSourceFactory.MAPNIK);
//        mapview.setBuiltInZoomControls(false);
//        mapview.setMultiTouchControls(true);
//        mapController = mapview.getController();
//        mapController.setZoom(10);


//        mapview = (MapView) findViewById(R.id.vehicles_osm_home_map);
//        mapview.setTileSource(TileSourceFactory.MAPNIK);
        mapview.setBuiltInZoomControls(false);
        mapview.setMultiTouchControls(true);
        mapController = mapview.getController();
        mapController.setZoom(12);
        mapview.getOverlays().clear();
        mapview.invalidate();
        mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));

        trackingMarker = new Marker(mapview);
        historymarker = new Marker(mapview);

        trackingMarker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker, MapView mapView) {
                return true;
            }
        });
        markerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_image_view, null);
        mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);

        id_refresh_countdowntimer = (TextView) findViewById(R.id.tracking_refresh_timer);

        $HeadImage = (ImageView) findViewById(R.id.id_curr_vehicle_info_icon);
        $BackPress = (ImageView) findViewById(R.id.popUp_view_Back);
        $Refresh = (ImageView) findViewById(R.id.popUP_refresh);
        $PoiClose = (ImageView) findViewById(R.id.curr_poi_img_cancel);

//        $TxtTitle = (TextView) findViewById(R.id.popUp_title);
        //    $TxtTitle = (TextView) findViewById(R.id.tracking_refresh_timer);
        $HeadTxt = (TextView) findViewById(R.id.id_curr_vehicle_id_value);
        $SpeedLable = (TextView) findViewById(R.id.id_curr_speed_label);
        $DistanceLable = (TextView) findViewById(R.id.id_curr_todaydistance_label);
        $SpeedValue = (TextView) findViewById(R.id.id_curr_speed_value);
        $DistanceValue = (TextView) findViewById(R.id.id_curr_todaydistance_value);
        $Address = (TextView) findViewById(R.id.curr_vehicle_address);
        $AddressLable = (TextView) findViewById(R.id.id_curr_vehicle_address_label);
        $TxtView1 = (TextView) findViewById(R.id.curr_view1);
        $TxtView2 = (TextView) findViewById(R.id.curr_view2);
        $PoiLable = (TextView) findViewById(R.id.curr_poi_lable);

        $HeadLayout = (LinearLayout) findViewById(R.id.id_vehicle_id_layout);
        $SpeedLableLayout = (LinearLayout) findViewById(R.id.id_speed_id_layout);
        $SpeedValueLayout = (LinearLayout) findViewById(R.id.id_speed_value_layout);
        $HiddenLayout = (LinearLayout) findViewById(R.id.id_infoHiddenLayout);
        $HiddenLayout.setVisibility(View.GONE);
        $POILayout = (LinearLayout) findViewById(R.id.curr_poiLayout);
        $AddresswLAyout = (LinearLayout) findViewById(R.id.curr_addressLayout);

        $HeadButton = (ImageView) findViewById(R.id.id_curr_vehicle_info_min);
        $AddPoiButton = (Button) findViewById(R.id.id_add_poi_btn_new);
        $AddPoiButton.setText("SAVE POI");
        $SavePoi = (Button) findViewById(R.id.id_save_poi_btn);
        history_tracking = (CircleButton) findViewById(R.id.history_tracking);

        $AddressEdit = (EditText) findViewById(R.id.curr_address_edit);
        $HeadLayout.setOnClickListener(this);
        $HeadButton.setOnClickListener(this);
        $AddPoiButton.setOnClickListener(this);
        $BackPress.setOnClickListener(this);
        $Refresh.setOnClickListener(this);
        $PoiClose.setOnClickListener(this);
        $SavePoi.setOnClickListener(this);
        refresh_rate = Long.parseLong(queryUserDB());

//        System.out.println("The refresh rate is oooooo:::::" + refresh_rate);

        //refresh_rate = 10;

        startTime = (refresh_rate * 1000);
//        trackServiceTimeInterval = startTime;
        ANIMATE_SPEEED = (int) ((refresh_rate * 1000) - 100);
        ANIMATE_SPEEED_TURN = (int) ((refresh_rate * 1000) - 100);
        //  id_refresh_rate_value.setText(getStdTableValue("refresh_rate") + " Seconds");
        countDownTimer = new RefreshCountDownTimer(startTime, interval - 500);
        $ChangeView = (ImageView) findViewById(R.id.track_changeViewIcon);
        $ChangeView.setVisibility(View.GONE);
        FrameLayout.LayoutParams imgeChangeView = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgeChangeView.width = width * 10 / 100;
        imgeChangeView.height = height * 15 / 100;
        imgeChangeView.gravity = Gravity.TOP | Gravity.RIGHT;
        imgeChangeView.rightMargin = (int) (width * 0.5 / 100);
        $ChangeView.setLayoutParams(imgeChangeView);


    }

    public void refreshTrackingData() {
//        id_refresh_tracking_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (navigation_status.equals("tracking")) {
        if (cd.isConnectingToInternet()) {
            Thread splashThread = new Thread() {
                @Override
                public void run() {
                    String response_from_server = null;
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog = MyCustomProgressDialog.ctor(VehicleTrackingActivity.this);
                                progressDialog.show();
                            }
                        });
                        String urlstr = Const.API_URL + "mobile/getSelectedVehicleLocation?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                        HttpConfig ht = new HttpConfig();
                        response_from_server = ht.httpGet(urlstr);
                        sleep(2000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (response_from_server != null && !response_from_server.isEmpty()) {
                            dbhelper.insert_current_vehicle_details(response_from_server);
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                    Toast internet_toast = Toast.makeText(getApplicationContext(), "Refreshed", Toast.LENGTH_LONG);
                                    internet_toast.show();
                                }
                            }
                        });
                    }
                }
            };
            splashThread.start();
        } else {
            Toast internet_toast = Toast.makeText(getApplicationContext(), "Internet not connected", Toast.LENGTH_LONG);
            internet_toast.show();
//                        home_button_functionality();
        }
//                } else {
//                    home_button_functionality();
//                    Toast internet_toast = Toast.makeText(getApplicationContext(), "Refreshed", Toast.LENGTH_LONG);
//                    internet_toast.show();
//                }
//            }
//        });
    }

    public String queryUserDB() {
        String mRefreshRate = null;

        DataBaseHandler db = new DataBaseHandler(this);
//        Cursor c = null;
        String qry = "SELECT * FROM "
                + DataBaseHandler.TABLE_USER + " WHERE user_id='" + Constant.SELECTED_USER_ID + "'";
        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        mRefreshRate = da.queryUserDB2(qry);

        if (mRefreshRate != null) {

        } else {
            mRefreshRate = "10";
        }
        return mRefreshRate;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.id_vehicle_id_layout:
                if ($HiddenLayout.getVisibility() == View.VISIBLE) {
                    $HeadButton.setImageResource(android.R.color.transparent);
                    $HiddenLayout.setVisibility(View.GONE);
                    $HeadButton.setBackgroundResource(R.drawable.uparrow);
                } else {
                    $HiddenLayout.setVisibility(View.VISIBLE);
                    $HeadButton.setImageResource(android.R.color.transparent);
                    // $HiddenLayout.setVisibility(View.VISIBLE);
//                    $HeadButton.setText("--");
                    $HeadButton.setBackgroundResource(R.drawable.minus1);
                }
                break;
            case R.id.id_curr_vehicle_info_min:
                if ($HiddenLayout.getVisibility() == View.VISIBLE) {
                    $HeadButton.setImageResource(android.R.color.transparent);
                    $HiddenLayout.setVisibility(View.GONE);
                    $HeadButton.setBackgroundResource(R.drawable.uparrow);
                } else {
                    $HiddenLayout.setVisibility(View.VISIBLE);
                    $HeadButton.setImageResource(android.R.color.transparent);
                    // $HiddenLayout.setVisibility(View.VISIBLE);
//                    $HeadButton.setText("--");
                    $HeadButton.setBackgroundResource(R.drawable.minus1);
                }
                break;
            case R.id.id_add_poi_btn_new:
                $AddPoiButton.setVisibility(View.GONE);
                $AddresswLAyout.setVisibility(View.GONE);
                $POILayout.setVisibility(View.VISIBLE);
                $SavePoi.setVisibility(View.VISIBLE);
                break;
            case R.id.popUp_view_Back:
                if (timer != null) {
                    timer.cancel();
                }
                cancelVehicleTracking();
                animator.stopAnimation();
                startActivity(new Intent(VehicleTrackingActivity.this, VehicleListActivity.class));
                finish();
                break;
            case R.id.popUP_refresh:
                // Toast.makeText(VehicleTrackingActivity.this, "Refresh clicked ::::::", Toast.LENGTH_SHORT).show();
                refreshTrackingData();
                break;
            case R.id.curr_poi_img_cancel:
                $POILayout.setVisibility(View.GONE);
                $SavePoi.setVisibility(View.GONE);
                $AddPoiButton.setVisibility(View.VISIBLE);
                $AddresswLAyout.setVisibility(View.VISIBLE);
                break;
            case R.id.id_save_poi_btn:
//                System.out.println("Hi save poi called");


                if ($AddressEdit.getText().toString().trim().length() > 0) {
                    if (cd.isConnectingToInternet()) {
                        $AddPoiButton.setVisibility(View.VISIBLE);
                        $AddresswLAyout.setVisibility(View.VISIBLE);
                        $POILayout.setVisibility(View.GONE);
                        $SavePoi.setVisibility(View.GONE);

                        String url = Const.API_URL + "mobile/setPOIName?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&poiName=" + $AddressEdit.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");

                        url = url.replaceAll(" ", "%20");
                        new SendPOIInformation().execute(url);
                    } else {
                        Toast internet_toast = Toast.makeText(VehicleTrackingActivity.this, "Please Check your Internet Connection", Toast.LENGTH_LONG);
                        internet_toast.show();
                    }
                } else {
                    $AddressEdit.setError("Enter POI Name");
                }

                break;
        }
    }

    public  void vehiclehistracking(){
        try{

            String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
            String string_to_date = mEndDate.trim() + " " + mEndTime.trim();
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
            Date d = f.parse(string_from_date);
            long fromMilliSeconds = d.getTime();
            Date d1 = f.parse(string_to_date);
            long toMilliSeconds = d1.getTime();
//            if (mapview != null) {
//                mapview.getOverlays().clear();
//                mapview.invalidate();
//                mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//            }

            if (Constant.SELECTED_VEHICLE_ID !=null && Constant.SELECTED_USER_ID !=null){
                String par[] = {Const.API_URL + "mobile/getVehicleHistoryForOnlyLatLng?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds};
//                progressDialog = MyCustomProgressDialog.ctor(VehicleTrackingGoogleMap.this);
//                progressDialog.setCancelable(false);
//                progressDialog.show();
                new GetHistorylatlng().execute(par);


            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void vehicletracking() {
        //System.out.println("Hi vehicle short name is 22222" + Constant.SELECTED_VEHICLE_ID + " :: " + Constant.SELECTED_USER_ID);
        // navigation_status = "tracking";
        try {
//            if (map != null) {
//                map.clear();
//            }

//            if (mapview != null) {
//                mapview.getOverlays().clear();
//                mapview.invalidate();
//                mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//            }

            if (Constant.SELECTED_VEHICLE_ID != null && Constant.SELECTED_USER_ID != null) {
                Toast.makeText(getApplicationContext(), "Tracking... Please wait a moment", Toast.LENGTH_LONG).show();
//        String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                String par[] = {Const.API_URL + "mobile/getSelectedVehicleLocation1?vehicleId=" + Constant.SELECTED_VEHICLE_ID.trim() + "&userId=" + Constant.SELECTED_USER_ID.trim().trim() + "&macid=" + getStdTableValue("macid").trim() + "&appid=" + getStdTableValue("appid").trim()};
                progressDialog = MyCustomProgressDialog.ctor(VehicleTrackingActivity.this);
                progressDialog.show();
                new GetPOIInformation().execute(par);

                cancelVehicleTracking();
                vehicletrackalarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), VehicleTrackingService.class);
                vehicletrackpendingIntent = PendingIntent.getService(getApplicationContext(), sp.getInt("notif_id", 0), intent, 0);

                if (!(refresh_rate < 3)) {
                    trackServiceTimeInterval = ((refresh_rate - 3) * 1000);
                } else {
                    trackServiceTimeInterval = refresh_rate * 1000;
                }

//        vehicletrackalarm.setRepeating(AlarmManager.RTC_WAKEUP, 0, startTime, vehicletrackpendingIntent);
                // vehicletrackalarm.setRepeating(AlarmManager.RTC_WAKEUP, 0, trackServiceTimeInterval, vehicletrackpendingIntent);
                handler.postDelayed(runable, 3000);

                loadDatas(trackServiceTimeInterval);
            } else {
                Toast.makeText(VehicleTrackingActivity.this, "Something went wrong.Please try again", Toast.LENGTH_SHORT).show();
                if (timer != null) {
                    timer.cancel();
                }
                cancelVehicleTracking();
                animator.stopAnimation();
                startActivity(new Intent(VehicleTrackingActivity.this, VehicleListActivity.class));
                finish();
            }
        } catch (Exception e) {
            Toast.makeText(VehicleTrackingActivity.this, "Something went wrong please try again", Toast.LENGTH_SHORT).show();
        }
    }

    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }

    private void loadDatas(long timeInterval) {
        // TODO Auto-generated method stub
        // preloaderStart();
        // Handler h = new Handler();
        // h.postDelayed(new Runnable() {
        // @Override
        // public void run() {
        // // TODO Auto-generated method stub
        // // homeProgressDialog.show();
        // new loadScreenData().execute();
        //
        // }
        // }, 200);

        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {

            public void run() {


                String urlstr = Const.API_URL + "mobile/getSelectedVehicleLocation?userId=" + Constant.SELECTED_USER_ID.trim() + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID.trim() + "&macid=" + getStdTableValue("macid").trim() + "&appid=" + getStdTableValue("appid").trim();
                new getLiveTrackingDatas().execute(urlstr);
            }
        }, 0, timeInterval);

    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//
//        map = googleMap;
//        map.setPadding(1, 1, 1, 150);
//        map.getUiSettings().setZoomControlsEnabled(true);
//    }

    public class getLiveTrackingDatas extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        public String doInBackground(String... urls) {
            String response_from_server = "";
            try {
                //  String qry="";
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(urls[0]);

                // System.out.println("The live track url is ::::" + urls[0]);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        public void onPostExecute(String result) {

//            System.out.println("The Selected vehicle detail is ::::" + result);

            //   System.out.println("The Live Activity2 vehicle live track datas :::" + result);
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//            SharedPreferences.Editor editor = sp.edit();
//            editor.putString("poidata", result);
//            editor.commit();
//            drawPoiPoints(result);

            if (result != null && !result.isEmpty()) {
                dbhelper.insert_current_vehicle_details(result);
            }

        }
    }

    public class GetHistorylatlng extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String response_from_server = "";
            try {
                HttpConfig httpConfig = new HttpConfig();
                response_from_server = httpConfig.httpGet(urls[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        protected void onPostExecute(String result) {

//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
            drawlatlngPoints(result);
            super.onPostExecute(result);
        }
    }
    public class GetPOIInformation extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        public String doInBackground(String... urls) {
            String response_from_server = "";
            try {
                //  String qry="";
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(urls[0]);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        public void onPostExecute(String result) {

//            System.out.println("The Selected vehicle detail is ::::" + result);

            // System.out.println("The Live Activity vehicle live track datas :::" + result);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("poidata", result);
            editor.commit();
            drawPoiPoints(result);
            if (!isdoubleclick){
                Log.d("doubleclick","failure");
            }else {
                vehiclehistracking();
                Log.d("doubleclick","success");
            }
        }
    }



    public class SendPOIInformation extends AsyncTask<String, Void, String> {
        @Override
        public String doInBackground(String... urls) {
            String response_from_server = "";
            try {
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(urls[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        public void onPostExecute(String result) {
            if (result != null) {
                $AddressEdit.setText("");
                //  id_poi_layout.setVisibility(View.GONE);
            } else {
                //  id_poi_layout.setVisibility(View.GONE);
                Toast internet_toast = Toast.makeText(VehicleTrackingActivity.this, "Something went wrong. Please add poi again", Toast.LENGTH_LONG);
                internet_toast.show();
            }
        }
    }

    public void drawlatlngPoints(String poidata){
        Log.d("drawpoidata",""+poidata);

        if (poidata !=null && poidata.length() !=0){
//            if (mapview != null) {
//                mapview.getOverlays().clear();
//                mapview.invalidate();
//                mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//            }
            try{
                Polyline myPath = null;
                //  View marker = null;
                // MarkerOptions markerOption = null;
               // PolylineOptions options = new PolylineOptions().width(8).color(getResources().getColor(R.color.black)).geodesic(true);
                JSONArray vehicles_array = new JSONArray(poidata);
                for (int j = 0; j < vehicles_array.length(); j++) {
                    JSONObject vehicleinfo = vehicles_array.getJSONObject(j);

                    currlat = Double.parseDouble(vehicleinfo.getString("lat"));
                    currlng = Double.parseDouble(vehicleinfo.getString("lng"));
                    shorttname = vehicleinfo.getString("shortName");
                    Log.d("currlatti",""+currlat);
                    Log.d("currlongii",""+currlng);


//
//                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currlat, currlng), 14.0f));
//                    LatLng position =new LatLng(currlat,currlng);
//                    options.add(position);

                    GeoPoint newPos = new GeoPoint(currlat, currlng);
                    historypolyPoint.add(newPos);
                     myPath = new Polyline();
                    myPath.setColor(getResources().getColor(R.color.black));
                    myPath.setPoints(historypolyPoint);

//                    mapview.getOverlays().add(myPath);
////
//                    mapview.invalidate();


                }

                //   for (int i = 0;i<vehicles_array.length();i++){
                JSONObject jsonObject = vehicles_array.getJSONObject(0);
                startlat = Double.parseDouble(jsonObject.getString("lat"));
                startlng = Double.parseDouble(jsonObject.getString("lng"));
                Log.d("startlat",""+startlat);
                Log.d("startlng",""+startlng);
//                View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.trackingmarker, null);
//                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(startlat, startlng));
//                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleTrackingActivity.this, marker)));
//                historymarker = map.addMarker(markerOption);


                View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_history_marker, null);
                GeoPoint loc = new GeoPoint(startlat, startlng);
                mapController.setCenter(loc);

                historymarker.setPosition(loc);
                historymarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                historymarker.setTitle(mVehicleShortName);
                historymarker.setIcon(createDrawableFromViewNew(VehicleTrackingActivity.this, marker));

                mapview.getOverlays().add(historymarker);
                mapview.invalidate();


                mapview.getOverlays().add(myPath);

                mapview.invalidate();
            }catch (Exception e){
                e.printStackTrace();
            }
//         vehicletracking();
        }

    }

    public void drawPoiPoints(String poi_data) {

        System.out.println("The vehicle tracking result is ::::" + poi_data);

        if (poi_data != null && poi_data.length() != 0) {
            try {
                JSONObject parentobject = new JSONObject(poi_data);
                if (!parentobject.isNull("geoFence")) {
                    int vehicles_history_length;
                    final ArrayList<GeoPoint> points = new ArrayList<>();
                    JSONArray vehiclearray = parentobject.getJSONArray("geoFence");
                    vehicles_history_length = vehiclearray.length();
                    for (int z = 0; z < vehicles_history_length; z++) {
                        if (!vehiclearray.get(z).equals(null)) {
                            ParsingClass parse = new ParsingClass();
                            parse.getPoiInformation(vehiclearray.getJSONObject(z).toString());
                            double lat = parse.getCurr_lat();
                            double lng = parse.getCurr_long();
                            GeoPoint position = new GeoPoint(lat, lng);
                            mStartLocation = position;
                            mEndLocation = mStartLocation;

                            //  System.out.println("the start and end location in draw poi is :::::::" + mStartLocation + " end  is ::::" + mEndLocation);

                            points.add(position);
                            if (parse.getAddress() != null) {
                                $Address.setText(parse.getAddress());
                            } else {
                                new GetAddressTask($Address).execute(lat, lng, 4.0);
                            }
                            $HeadTxt.setText(parse.getshortname());
                            $SpeedValue.setText(parse.getSpeed() + " KMS / HR");
                            // id_curr_odo_value.setText(parse.getodoDistance() + " KMS");
                            $DistanceValue.setText(parse.getDistanCovered() + " KMS");

                            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custompoimarker, null);
//                            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
//                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleTrackingActivity.this, marker)));
//                            Marker currentMarker = map.addMarker(markerOption);


                            Marker poiMarker = new Marker(mapview);
                            poiMarker.setPosition(new GeoPoint(lat, lng));
                            poiMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                            poiMarker.setTitle(mVehicleShortName);
                            poiMarker.setIcon(createDrawableFromViewNew(VehicleTrackingActivity.this, marker));
                            mapview.getOverlays().add(poiMarker);

                            // animateMarker.setTitle("Moving");
                            mapview.invalidate();

                        }
                    }
                } else {
                    // System.out.println("You are in poi else condition ");
                }

                if (poi_data != null && poi_data.length() > 0) {
//                    if (map != null) {
//                        map.clear();
//                    }

//                    if (mapview != null) {
//                        mapview.getOverlays().clear();
//                        mapview.invalidate();
//                        mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//                    }

                    try {
                        JSONObject vehicleinfo = new JSONObject(poi_data);

                        if ((vehicleinfo.has("latitude")) && (vehicleinfo.has("longitude")) && (vehicleinfo.has("shortName")) && (vehicleinfo.has("vehicleType"))) {


                            final ArrayList<GeoPoint> points = new ArrayList<>();
                            ParsingClass parse = new ParsingClass();
                            // System.out.println("The result Poi data is :::::" + poi_data);
                            parse.getCurrentVehicleInfo(poi_data);
                            double lat = parse.getCurr_lat();
                            double lng = parse.getCurr_long();
                            GeoPoint position = new GeoPoint(lat, lng);
                            // System.out.println("The lat is :::" + lat + " and longi is ::::" + lng);
                            mStartLocation = position;
                            mEndLocation = mStartLocation;

                            createDefaultPolyLine(parse.getLatLngOld());


//                            if (parse.getvehicleType().equals("Bus")) {
//                                $HeadImage.setImageResource(R.drawable.bus_east);
//                            } else if (parse.getvehicleType().equals("Car")) {
//                                $HeadImage.setImageResource(R.drawable.car_east);
//                            } else if (parse.getvehicleType().equals("Truck")) {
//                                $HeadImage.setImageResource(R.drawable.truck_east);
//                            } else if (parse.getvehicleType().equals("Cycle")) {
//                                $HeadImage.setImageResource(R.drawable.cycle_east);
//                            } else if (parse.getvehicleType().equalsIgnoreCase("Jcb")) {
//                                $HeadImage.setImageResource(R.drawable.jcb_icon);
//                            } else if (parse.getvehicleType().equalsIgnoreCase("Bike")) {
//                                $HeadImage.setImageResource(R.drawable.vehicle_bike);
//                            }

                            if (parse.getAddress() != null) {
                                $Address.setText(parse.getAddress());
                            } else {
                                new GetAddressTask($Address).execute(lat, lng, 4.0);
                            }
                            $HeadTxt.setText(parse.getshortname());
                            $SpeedValue.setText(parse.getSpeed() + " KMS / HR");
                            // id_curr_odo_value.setText(parse.getodoDistance() + " KMS");
                            $DistanceValue.setText(parse.getDistanCovered() + " KMS");

//                            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);

                            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker_osm, null);

                            ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
                            ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
                            if (!parse.getDirection().equals("")) {
                                Constant.vehicle_direction_in_map(parse.getvehicleType(), parse.getDirection(), id_vehicle_in_marker);
                                id_vehicle_in_marker.setColorFilter(Color.WHITE);

                            }
//                            if (parse.getOverSpeed().equalsIgnoreCase("Y")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
//                            } else if (parse.getGeoFence().equalsIgnoreCase("Y")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
//                            } else if (parse.getPosition().equalsIgnoreCase("M")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
//                            } else if (parse.getPosition().equalsIgnoreCase("P")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//                            } else if (parse.getPosition().equalsIgnoreCase("S")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//                            } else if (parse.getPosition().equalsIgnoreCase("N")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//                            } else if (parse.getPosition().equalsIgnoreCase("U")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//                            }


                            if (parse.getOverSpeed().equalsIgnoreCase("Y")) {
                                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);

                                $HeadImage.setImageDrawable(Constant.setVehicleImage("U", getApplicationContext(), parse.getvehicleType()));
                            } else if (parse.getGeoFence().equalsIgnoreCase("Y")) {
                                $HeadImage.setImageDrawable(Constant.setVehicleImage("U", getApplicationContext(), parse.getvehicleType()));
                                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
                            } else if (parse.getPosition().equalsIgnoreCase("M")) {
                                $HeadImage.setImageDrawable(Constant.setVehicleImage("M", getApplicationContext(), parse.getvehicleType()));
                                id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
                            } else if (parse.getPosition().equalsIgnoreCase("P")) {
                                $HeadImage.setImageDrawable(Constant.setVehicleImage("P", getApplicationContext(), parse.getvehicleType()));
                                id_custom_marker_icon.setImageResource(R.drawable.black_custom_marker_icon);
                            } else if (parse.getPosition().equalsIgnoreCase("S")) {
                                $HeadImage.setImageDrawable(Constant.setVehicleImage("S", getApplicationContext(), parse.getvehicleType()));
                                id_custom_marker_icon.setImageResource(R.drawable.yellow_custom_marker_icon);
                            } else if (parse.getPosition().equalsIgnoreCase("N")) {
                                $HeadImage.setImageDrawable(Constant.setVehicleImage("N", getApplicationContext(), parse.getvehicleType()));
                                id_custom_marker_icon.setImageResource(R.drawable.yellow_custom_marker_icon);
                            } else if (parse.getPosition().equalsIgnoreCase("U")) {
                                $HeadImage.setImageDrawable(Constant.setVehicleImage("U", getApplicationContext(), parse.getvehicleType()));
                                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
                            }


                            if (lat != 0.0 && lng != 0.0) {
//                        System.out.println("You are in map set method ::::");


//                                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
//                                markerOption.title(mVehicleShortName);
//                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleTrackingActivity.this, marker)));
////                                if (current_vehicle_location_marker.isEmpty()) {
//                                trackingMarker = map.addMarker(markerOption);
//                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14.0f));

                                GeoPoint loc = new GeoPoint(lat, lng);
                                mapController.setCenter(loc);

                                trackingMarker.setPosition(loc);
                                trackingMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                                trackingMarker.setTitle(mVehicleShortName);
//                                mMarkersHashMap.put(osmMarker, vehiclejson);
                                trackingMarker.setIcon(createDrawableFromViewNew(VehicleTrackingActivity.this, marker));
//                                mMarkerList.add(osmMarker);

                                mapview.getOverlays().add(trackingMarker);
                                mapview.invalidate();


                            }
                        } else {
                            Toast.makeText(VehicleTrackingActivity.this, "Something went wrong.Please try again", Toast.LENGTH_SHORT).show();
                            if (timer != null) {
                                timer.cancel();
                            }
                            cancelVehicleTracking();
                            animator.stopAnimation();
                            startActivity(new Intent(VehicleTrackingActivity.this, VehicleListActivity.class));
                            finish();
                        }
                    } catch (Exception e) {
                        Toast.makeText(VehicleTrackingActivity.this, "Something went wrong.Please try again", Toast.LENGTH_SHORT).show();
                        if (timer != null) {
                            timer.cancel();
                        }
                        cancelVehicleTracking();
                        animator.stopAnimation();
                        startActivity(new Intent(VehicleTrackingActivity.this, VehicleListActivity.class));
                        finish();
                    }
                }

            } catch (JSONException je) {
                je.printStackTrace();
            }
        }
    }

    public static Drawable createDrawableFromViewNew(Context context, View view) {


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        System.out.println("Hi view width " + displayMetrics.widthPixels + " " + view.getMeasuredWidth() + " height " + displayMetrics.heightPixels + " " + view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.createBitmap(250, 250, Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.crea(96, 96, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
//        return bitmap;


        return new BitmapDrawable(bitmap);
    }


    public void cancelVehicleTracking() {
        stopService(new Intent(VehicleTrackingActivity.this, VehicleTrackingService.class));
        Intent cancel_intent = new Intent(getApplicationContext(), VehicleTrackingService.class);
        cancel_pendingIntent = PendingIntent.getService(getApplicationContext(), sp.getInt("notif_id", 0), cancel_intent, 0);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(cancel_pendingIntent);
        SharedPreferences.Editor editor1 = sp.edit();
        editor1.putInt("notif_id", sp.getInt("notif_id", 0) + 1);
        editor1.putBoolean("vehicle_track_show_progress", true);
        editor1.putBoolean("tracking_exception", false);
        editor1.commit();
        stopService(cancel_intent);
        countDownTimer.cancel();
        timerHasStarted = false;
        dbhelper.deletecurrent_vehicleinfo();
        handler.removeCallbacksAndMessages(null);
//        handler.removeCallbacksAndMessages(runable);
    }

    public class RefreshCountDownTimer extends CountDownTimer {
        public RefreshCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            timeElapsed = startTime - millisUntilFinished;
            if ((millisUntilFinished / 1000) == 0) {
                id_refresh_countdowntimer.setText(Long.toString(refresh_rate));
            } else {
                id_refresh_countdowntimer.setText(String.valueOf(millisUntilFinished / 1000));
            }
        }

        @Override
        public void onFinish() {
            Toast.makeText(getApplicationContext(), "Refreshed", Toast.LENGTH_SHORT).show();
            countDownTimer.cancel();
            timerHasStarted = false;
        }
    }

    private class GetAddressTask extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTask(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {
            getlat = params[0];
            getlng = params[1];
            address_type = params[2];
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(params[0], params[1], 1);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(params[0]) +
                        " , " +
                        Double.toString(params[1]) +
                        " passed to address service";
                e2.printStackTrace();
                return errorString;
            } catch (NullPointerException np) {
                // TODO Auto-generated catch block
                np.printStackTrace();
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                String addressText = null;
                StringBuffer addr = new StringBuffer();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressText = address.getAddressLine(i);
                    addr.append(addressText + ",");
                }
                // Return the text
                return addr.toString();
            } else {
                return "No address found";
            }
        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);
            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {
                    /***
                     * 1.0 - vehicles current address when a vehicle is clicked
                     * 2.0 - vehicles origin address in history selection
                     * 3.0 - vehicles destination address in history selection
                     * 6.0 - vehicles current address when pause button is clicked in history selection
                     */

//                    System.out.println("The address is ::::" + result);

                    if (address_type == 1.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("address", result);
                        editor.commit();
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 2.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("vehicle_origin_address", result);
                        editor.commit();
                        // history_start_location_value.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 3.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("vehicle_destination_address", result);
                        editor.commit();
                        // history_end_location_value.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 4.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("current_vehicle_address", result);
                        editor.commit();
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 5.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 6.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    }
                }
            } else {
                Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
                empty_fav.show();
            }
        }
    }

   /* save_poi.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (checkNetworkSettings()) {
                String url = Const.API_URL + "mobile/setPOIName?userId=" + username + "&vehicleId=" + sp.getString("current_vehicleID", "") + "&poiName=" + store_poi.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                new SendPOIInformation().execute(url);
            } else {
                Toast internet_toast = Toast.makeText(context, "Please Check your Internet Connection", Toast.LENGTH_LONG);
                internet_toast.show();
            }
        }
    });*/

    /**
     * Code to make the Marker icon adjustable for multiple screen
     */
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }

    private void changeMapView() {

        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(VehicleTrackingActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                mapview.setTileSource(TileSourceFactory.MAPNIK);

//                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
//                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                mapview.setTileSource(TileSourceFactory.USGS_SAT);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
//                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                mapview.setTileSource(TileSourceFactory.HIKEBIKEMAP);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
//                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                mapview.setTileSource(TileSourceFactory.USGS_TOPO);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        //Log.i("onPause", "The onPause() event");
        cancelVehicleTracking();
        // handler.removeCallbacksAndMessages(null);
        animator.stopAnimation();
        if (timer != null) {
            timer.cancel();
        }
        finish();
//        System.exit(0);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        startActivity(new Intent(VehicleTrackingActivity.this, VehicleListActivity.class));
//        finish();

    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
        cancelVehicleTracking();
        animator.stopAnimation();
        if (timer != null) {
            timer.cancel();
        }
        //   handler.removeCallbacksAndMessages(null);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("tracking_exception", false);
        editor.commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (timer != null) {
            timer.cancel();
        }
        cancelVehicleTracking();
        animator.stopAnimation();
        startActivity(new Intent(VehicleTrackingActivity.this, VehicleListActivity.class));
        finish();
    }

    //    @Override
//    public void onInfoWindowClick(Marker marker) {
//
//    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;


        LinearLayout.LayoutParams backParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backParams.width = width * 14 / 100;
        backParams.height = height * 10 / 100;
        backParams.gravity = Gravity.CENTER;
        $BackPress.setLayoutParams(backParams);
        $BackPress.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);


        LinearLayout.LayoutParams refreshParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        refreshParams.width = width * 13 / 100;
        refreshParams.height = height * 8 / 100;
        refreshParams.gravity = Gravity.CENTER;
        $Refresh.setLayoutParams(refreshParams);
        $Refresh.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);
        $ChangeView.setLayoutParams(refreshParams);
        $ChangeView.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);


        LinearLayout.LayoutParams txtTitleParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtTitleParams.width = width * 60 / 100;
        txtTitleParams.height = height * 10 / 100;
        id_refresh_countdowntimer.setLayoutParams(txtTitleParams);
        id_refresh_countdowntimer.setPadding(width * 2 / 100, 0, 0, 0);
        id_refresh_countdowntimer.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams headLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headLayoutParams.width = width;
        headLayoutParams.height = height * 6 / 100;
        $HeadLayout.setLayoutParams(headLayoutParams);


        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 70 / 100;
        headTxtParams.height = height * 6 / 100;
        headTxtParams.gravity = Gravity.CENTER;
        $HeadTxt.setLayoutParams(headTxtParams);
        $HeadTxt.setPadding(width * 2 / 100, 0, 0, 0);
        $HeadTxt.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams headImgParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headImgParama.width = width * 12 / 100;
        headImgParama.gravity = Gravity.CENTER;
        headImgParama.height = (int) (height * 6 / 100);
        $HeadImage.setLayoutParams(headImgParama);

        LinearLayout.LayoutParams txtrefreshParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtrefreshParama.width = height * 6 / 100;
        txtrefreshParama.height = (int) (height * 4 / 100);
        txtrefreshParama.gravity = Gravity.CENTER;
        txtrefreshParama.setMargins(width * 2 / 100, 0, 0, 0);
        $HeadButton.setLayoutParams(txtrefreshParama);
        // $HeadButton.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams txtLableSpeedParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtLableSpeedParama.width = width * 50 / 100;
        txtLableSpeedParama.height = (int) (height * 4 / 100);
        $SpeedLable.setLayoutParams(txtLableSpeedParama);
        $DistanceLable.setLayoutParams(txtLableSpeedParama);
        $SpeedValue.setLayoutParams(txtLableSpeedParama);
        $DistanceValue.setLayoutParams(txtLableSpeedParama);
        $AddressLable.setLayoutParams(txtLableSpeedParama);

        $SpeedLable.setGravity(Gravity.CENTER | Gravity.LEFT);
        $DistanceLable.setGravity(Gravity.CENTER | Gravity.LEFT);
        $SpeedValue.setGravity(Gravity.CENTER | Gravity.LEFT);
        $DistanceValue.setGravity(Gravity.CENTER | Gravity.LEFT);
        $AddressLable.setGravity(Gravity.CENTER | Gravity.LEFT);

        $SpeedLable.setPadding(width * 1 / 100, 0, 0, 0);
        $DistanceLable.setPadding(width * 1 / 100, 0, 0, 0);
        $SpeedValue.setPadding(width * 1 / 100, 0, 0, 0);
        $DistanceValue.setPadding(width * 1 / 100, 0, 0, 0);
        $AddressLable.setPadding(width * 1 / 100, 0, 0, 0);


        LinearLayout.LayoutParams txtrefreshValueParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtrefreshValueParama.width = width * 80 / 100;
        $Address.setLayoutParams(txtrefreshValueParama);
        $Address.setGravity(Gravity.CENTER | Gravity.LEFT);
        $Address.setPadding(width * 1 / 100, 0, 0, 0);

        LinearLayout.LayoutParams txtAddPoiBtnValueParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtAddPoiBtnValueParama.width = width * 20 / 100;
        $AddPoiButton.setLayoutParams(txtAddPoiBtnValueParama);
        $AddPoiButton.setGravity(Gravity.CENTER);
        $AddPoiButton.setPadding(width * 1 / 100, height * 1 / 100, 0, height * 2 / 100);


        LinearLayout.LayoutParams addPob = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        addPob.width = width;
        addPob.height = (int) (height * 5 / 100);
        // $AddPoiButton.setLayoutParams(addPob);
        $SavePoi.setLayoutParams(addPob);
        //  $AddPoiButton.setGravity(Gravity.CENTER);
        $SavePoi.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams viewParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        viewParam.width = width;
        viewParam.height = (int) (height * 0.3 / 100);
        $TxtView1.setLayoutParams(viewParam);
        $TxtView2.setLayoutParams(viewParam);

        LinearLayout.LayoutParams txtPoiAddress = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtPoiAddress.width = width * 85 / 100;
        txtPoiAddress.height = (int) (height * 5 / 100);
        $PoiLable.setLayoutParams(txtPoiAddress);
        $PoiLable.setPadding(width * 1 / 100, 0, 0, 0);
        $PoiLable.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtImagClose = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtImagClose.width = width * 12 / 100;
        txtImagClose.height = (int) (height * 5 / 100);
        $PoiClose.setLayoutParams(txtImagClose);

        LinearLayout.LayoutParams txtpoedit = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtpoedit.width = width;
        txtpoedit.height = (int) (height * 8 / 100);
        txtpoedit.topMargin = height * 1 / 100;
        txtpoedit.bottomMargin = height * 2 / 100;
        $AddressEdit.setLayoutParams(txtpoedit);
        $AddressEdit.setGravity(Gravity.CENTER | Gravity.LEFT);

        if (width >= 600) {
            $HeadTxt.setTextSize(16);
            $SpeedLable.setTextSize(14);
            $DistanceLable.setTextSize(14);
            $SpeedValue.setTextSize(13);
            $DistanceValue.setTextSize(13);
            $Address.setTextSize(13);
            $AddressLable.setTextSize(14);
            $TxtView1.setTextSize(12);
            $TxtView2.setTextSize(12);
            // $HeadButton.setTextSize(16);
            $AddPoiButton.setTextSize(12);
            id_refresh_countdowntimer.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $HeadTxt.setTextSize(15);
            $SpeedLable.setTextSize(13);
            $DistanceLable.setTextSize(13);
            $SpeedValue.setTextSize(12);
            $DistanceValue.setTextSize(12);
            $Address.setTextSize(12);
            $AddressLable.setTextSize(13);
            $TxtView1.setTextSize(11);
            $TxtView2.setTextSize(11);
            //$HeadButton.setTextSize(15);
            $AddPoiButton.setTextSize(12);
            id_refresh_countdowntimer.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $HeadTxt.setTextSize(14);
            $SpeedLable.setTextSize(12);
            $DistanceLable.setTextSize(12);
            $SpeedValue.setTextSize(11);
            $DistanceValue.setTextSize(11);
            $Address.setTextSize(11);
            $AddressLable.setTextSize(12);
            $TxtView1.setTextSize(10);
            $TxtView2.setTextSize(10);
            // $HeadButton.setTextSize(14);
            $AddPoiButton.setTextSize(11);
            id_refresh_countdowntimer.setTextSize(14);
        } else if (width <= 260) {
            $HeadTxt.setTextSize(13);
            $SpeedLable.setTextSize(11);
            $DistanceLable.setTextSize(11);
            $SpeedValue.setTextSize(10);
            $DistanceValue.setTextSize(10);
            $Address.setTextSize(10);
            $AddressLable.setTextSize(11);
            $TxtView1.setTextSize(9);
            $TxtView2.setTextSize(9);
            // $HeadButton.setTextSize(13);
            $AddPoiButton.setTextSize(10);
            id_refresh_countdowntimer.setTextSize(13);
        }
    }


    /**
     * For Live Tracking animation created by prabhakaran on 27-02-2016
     */


    private Animator animator = new Animator();


    int currentPt;
//
//    GoogleMap.CancelableCallback MyCancelableCallback =
//            new GoogleMap.CancelableCallback() {
//
//                @Override
//                public void onCancel() {
//                    // System.out.println("onCancelled called");
//                }
//
//                @Override
//                public void onFinish() {


//                    if (++currentPt < geoPoints.size()) {
//
//
//                        float targetBearing = bearingBetweenLatLngs(map.getCameraPosition().target, geoPoints.get(currentPt));
//
//                        LatLng targetLatLng = geoPoints.get(currentPt);
//
//                        System.out.println("Prabha CancelableCallback called :::::::::::::");
//                        // System.out.println("currentPt  = " + currentPt);
//                        //  System.out.println("size  = " + geoPoints.size());
//                        //Create a new CameraPosition
//                        CameraPosition cameraPosition =
//                                new CameraPosition.Builder()
//                                        .target(targetLatLng)
//                                        .tilt(currentPt < geoPoints.size() - 1 ? 90 : 0)
//                                        .bearing(targetBearing)
//                                        .zoom(history_zoom_level)
//                                        .build();
//
//
//                        map.animateCamera(
//                                CameraUpdateFactory.newCameraPosition(cameraPosition),
//                                3000,
//                                MyCancelableCallback);
//                        System.out.println("Animate to: " + geoPoints.get(currentPt) + "\n" +
//                                "Bearing: " + targetBearing);
//
//                        geoPoints.get(currentPt);
//
//                    } else {
//                        //info.setText("onFinish()");
//                    }

//                }
//
//            };


    public class Animator implements Runnable {


        private final Interpolator interpolator = new LinearInterpolator();


        float tilt = 90;
        float zoom = 15.5f;
        boolean upward = true;

        long start = SystemClock.uptimeMillis();

        GeoPoint endLatLng = null;
        GeoPoint beginLatLng = null;

        boolean showPolyline = false;


//        public void reset() {
//            System.out.println("Prabha reset called :::::::::::::");
//            //    resetMarkers();
//            start = SystemClock.uptimeMillis();
//            if (currentIndex == geoPoints.size()) {
//                currentIndex = 0;
//            } else {
//
//            }
//            // System.out.println("the current index in reset method is ::::" + currentIndex);
//            endLatLng = getEndLatLng();
//            beginLatLng = getBeginLatLng();
//
//        }

        public void stop() {
//            if (trackingMarker != null) {
//                trackingMarker.remove();
//            }

            if (trackingMarker != null) {

                trackingMarker.remove(mapview);
            }

            mHandler.removeCallbacks(animator);

        }

        public void initialize(boolean showPolyLine) {
            mCursorAlreadyInitialized = true;
            //    System.out.println("Prabha initialize called :::::::::::::");
            reset();
            this.showPolyline = showPolyLine;

            // highLightMarker(0);

            if (showPolyLine) {
//                polyLine = initializePolyLine();

                updatePolyLine(mStartLocation.getLatitude(), mStartLocation.getLongitude());

            }
            // System.out.println("Initia lize method called ::::" + currentIndex);
            // We first need to put the camera in the correct position for the first run (we need 2 markers for this).....


            GeoPoint markerPos = mStartLocation;
            GeoPoint secondPos = mEndLocation;

            // System.out.println("The start location is ::::::" + mStartLocation + " end location is :::::" + mEndLocation);


            setupCameraPositionForMovement(markerPos, secondPos);

        }

        private void setupCameraPositionForMovement(GeoPoint markerPos,
                                                    GeoPoint secondPos) {
            //System.out.println("setup camera position called ::::");
            float bearing = bearingBetweenLatLngs(markerPos, secondPos);

//            trackingMarker = map.addMarker(new MarkerOptions().position(markerPos)
//                    .title("title")
//                    .snippet("snippet"));


            trackingMarker.setPosition(markerPos);
            trackingMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            trackingMarker.setTitle(mVehicleShortName);
            mapview.getOverlays().add(trackingMarker);
            //  trackingMarker.setIcon(getResources().getDrawable(R.drawable.green_custom_marker_icon));


            //  mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
            // trackingMarker.setIcon(createDrawableFromViewNew(VehicleTrackingActivity.this, markerView));


            // animateMarker.setTitle("Moving");
            mapview.invalidate();
            mapController.setCenter(markerPos);

            try {
                animator.reset();
                Handler handler = new Handler();
                handler.post(animator);
            } catch (Exception e) {
                e.printStackTrace();
            }


//            if (!(trackingMarker != null)) {
//                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
//                markerOption.title(mVehicleShortName);
//                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleTrackingActivity.this, mIconMarker)));
////                                if (current_vehicle_location_marker.isEmpty()) {
//                trackingMarker = map.addMarker(markerOption);
//            }


//            CameraPosition cameraPosition =
//                    new CameraPosition.Builder()
//                            .target(markerPos)
//                            .bearing(bearing + BEARING_OFFSET)
//                            .tilt(90)
//                            .zoom(map.getCameraPosition().zoom)
//                            .build();

//            CameraPosition cameraPosition;
//
//            if (m3Dmap) {
//                cameraPosition =
//                        new CameraPosition.Builder()
//                                .target(markerPos)
//                                .bearing(bearing + BEARING_OFFSET)
//                                .tilt(90)
//                                .zoom(map.getCameraPosition().zoom)
//                                .build();
//            } else {
//                cameraPosition =
//                        new CameraPosition.Builder()
//                                .target(markerPos)
//
//                                .tilt(90)
//                                .zoom(map.getCameraPosition().zoom)
//                                .build();
//            }


//            .zoom(history_zoom_level)

//            map.animateCamera(
//                    CameraUpdateFactory.newCameraPosition(cameraPosition),
//                    ANIMATE_SPEEED_TURN,
//                    new GoogleMap.CancelableCallback() {
//
//                        @Override
//                        public void onFinish() {
//                            //System.out.println("finished camera");
//                            animator.reset();
//                            Handler handler = new Handler();
//                            handler.post(animator);
//                        }
//
//                        @Override
//                        public void onCancel() {
//                            //System.out.println("cancelling camera");
//                        }
//                    }
//            );
        }

        private Polyline polyLine;
//        private PolylineOptions rectOptions = new PolylineOptions();

        public void reset() {
            //System.out.println("Prabha reset called :::::::::::::");
            //    resetMarkers();
            start = SystemClock.uptimeMillis();

            // System.out.println("the current index in reset method is ::::" + currentIndex);
            endLatLng = getEndLatLng();
            beginLatLng = getBeginLatLng();

        }

//        private Polyline initializePolyLine() {
//            //System.out.println("Prabha initializePolyLine called :::::::::::::");
//            //polyLinePoints = new ArrayList<LatLng>();
//            PolylineOptions rectOptions = new PolylineOptions();
//            //  System.out.println("The current index in polyline is :::" + currentIndex);
//
//            rectOptions.add(mStartLocation);
//
//            // System.out.println("The polyline start location is ::::is " + mStartLocation + " and end position is ::::::" + mEndLocation);
//
//            //options.add(position);
//            // System.out.println("The poly called ");
//            rectOptions.color(getResources().getColor(R.color.history_polyline_color));
//
//            return map.addPolyline(rectOptions);
//        }

        /**
         * Add the marker to the polyline.
         */
//        private void updatePolyLine(LatLng latLng) {
//            // System.out.println("Prabha updatePolyLine called :::::::::::::");
//            List<LatLng> points = polyLine.getPoints();
//            points.add(latLng);
//            //rectOptions.color(getResources().getColor(R.color.history_polyline_color));
//            polyLine.setPoints(points);
//        }
        private void updatePolyLine(double lat, double lng) {

            GeoPoint newPos = new GeoPoint(lat, lng);
            polylinePoints.add(newPos);
//            System.out.println("Hi polyline points list is " + polylinePoints.size() + " latlng to add in list lat " + lat + " lng " + lng);
            Polyline myPath = new Polyline();
            myPath.setColor(getResources().getColor(R.color.history_polyline_color));
            myPath.setPoints(polylinePoints);

            mapview.getOverlays().add(myPath);
//
            mapview.invalidate();


        }


        public void stopAnimation() {
            //  isStopCalled = false;
            animator.stop();
        }

        public void startAnimation(boolean showPolyLine) {
            // System.out.println("Start animation called :::::" + mStartLocation + " end location is ::::" + mEndLocation);
            if (mStartLocation != null && mEndLocation != null) {

                //System.out.println("Start animation geo points size " + geoPoints.size());
                // System.out.println("Start animation called :::::123454");
                if (mStartLocation != mEndLocation) {
                    // System.out.println("Prabha HIIIIIIIIIII");
//                    if (mCursorIndexSize == 0 || mCursorIndexSize == 1) {
                    if (!mCursorAlreadyInitialized) {
                        animator.initialize(showPolyLine);
                    } else {
                        moveVehicleWithoutInit();
                    }

//                    }
//                    else {
//
//                    }
                }
            } else if (mStartLocation != null && mEndLocation == null) {

            } else {
//                if (map != null) {
//                    map.clear();
//                }
//
//                if (mapview != null) {
//                    mapview.getOverlays().clear();
//                    mapview.invalidate();
//                    mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//                }

            }
        }


        public void moveVehicleWithoutInit() {
            endLatLng = getEndLatLng();
            beginLatLng = getBeginLatLng();


            start = SystemClock.uptimeMillis();

            GeoPoint begin = getBeginLatLng();
            GeoPoint end = getEndLatLng();

            float bearingL = bearingBetweenLatLngs(begin, end);

//            CameraPosition cameraPosition;
//            if (m3Dmap) {
//                cameraPosition =
//                        new CameraPosition.Builder()
//                                .target(end) // changed this...
//                                .bearing(bearingL + BEARING_OFFSET)
//                                .tilt(tilt)
//                                .zoom(map.getCameraPosition().zoom)
//                                .build();
//            } else {
//                cameraPosition =
//                        new CameraPosition.Builder()
//                                .target(end) // changed this...
//
//                                .tilt(tilt)
//                                .zoom(map.getCameraPosition().zoom)
//                                .build();
//            }
//
//
//            map.animateCamera(
//                    CameraUpdateFactory.newCameraPosition(cameraPosition),
//                    ANIMATE_SPEEED_TURN,
//                    null
//            );

            start = SystemClock.uptimeMillis();
            mHandler.postDelayed(animator, 16);
        }


        @Override
        public void run() {
            // System.out.println("Run called :::::");
            // System.out.println("Prabha run called :::::::::::::");
            long elapsed = SystemClock.uptimeMillis() - start;
            double t = interpolator.getInterpolation((float) elapsed / ANIMATE_SPEEED);

            //	LatLng endLatLng = getEndLatLng();
            //LatLng beginLatLng = getBeginLatLng();

            //System.out.println("The start latlng is :::::" + beginLatLng + " end latlng is :::::" + endLatLng);

//            double lat = t * endLatLng.latitude + (1 - t) * beginLatLng.latitude;
//            double lng = t * endLatLng.longitude + (1 - t) * beginLatLng.longitude;

            double lat = t * endLatLng.getLatitude() + (1 - t) * beginLatLng.getLatitude();
            double lng = t * endLatLng.getLongitude() + (1 - t) * beginLatLng.getLongitude();

            GeoPoint newPosition = new GeoPoint(lat, lng);
            // System.out.println("the new position is :::::" + newPosition);
            trackingMarker.setPosition(newPosition);

            if (showPolyline) {
                // System.out.println("Prabha updatePolyLine called :::::::::::::neew lat lng is ::::" + newPosition);
//                updatePolyLine(newPosition);

                updatePolyLine(lat, lng);

            }

            // It's not possible to move the marker + center it through a cameraposition update while another camerapostioning was already happening.
            //navigateToPoint(newPosition,tilt,bearing,currentZoom,false);
            //navigateToPoint(newPosition,false);

            if (t < 1) {
                //System.out.println("Prabha t < 1 true called :::::::::::::");
                mHandler.postDelayed(this, 16);
            } else {

                // System.out.println("The previous start and end location in t>?>>>1 is :::" + mStartLocation + " :::" + mEndLocation);
                // System.out.println("Prabha t  >>>>>>> 1 else called :::::::::::::");
                mStartLocation = mEndLocation;
                // System.out.println("Move to next marker.... current = " + currentIndex + " and size = " + geoPoints.size());
                // imagine 5 elements -  0|1|2|3|4 currentindex must be smaller than 4
//                if (currentIndex < geoPoints.size() - 2) {
//
//                    System.out.println("Prabha currentIndex < geoPoints.size() - 2 called :::::::::::::");
//
//                    currentIndex++;
//
//                    endLatLng = getEndLatLng();
//                    beginLatLng = getBeginLatLng();
//
//
//                    start = SystemClock.uptimeMillis();
//
//                    LatLng begin = getBeginLatLng();
//                    LatLng end = getEndLatLng();
//
//                    float bearingL = bearingBetweenLatLngs(begin, end);
//
//                    highLightMarker(currentIndex);
//
//                    CameraPosition cameraPosition =
//                            new CameraPosition.Builder()
//                                    .target(end) // changed this...
//                                    .bearing(bearingL + BEARING_OFFSET)
//                                    .tilt(tilt)
//                                    .zoom(history_zoom_level)
//                                    .build();
//
//
//                    map.animateCamera(
//                            CameraUpdateFactory.newCameraPosition(cameraPosition),
//                            ANIMATE_SPEEED_TURN,
//                            null
//                    );
//
//                    start = SystemClock.uptimeMillis();
//                    mHandler.postDelayed(animator, 16);

//                } else {

                // System.out.println("the current index in anim else method is :::::" + currentIndex);

//                    currentIndex++;
//                    highLightMarker(currentIndex);
//                stopAnimation();
//                }

            }
        }


        private GeoPoint getEndLatLng() {


            return mEndLocation;
        }

        private GeoPoint getBeginLatLng() {


            return mStartLocation;
        }

        private void adjustCameraPosition() {
            //System.out.println("tilt = " + tilt);
            //System.out.println("upward = " + upward);
            //System.out.println("zoom = " + zoom);
            if (upward) {

                if (tilt < 90) {
                    tilt++;
                    zoom -= 0.01f;
                } else {
                    upward = false;
                }

            } else {
                if (tilt > 0) {
                    tilt--;
                    zoom += 0.01f;
                } else {
                    upward = true;
                }
            }
        }
    }


    private float bearingBetweenLatLngs(GeoPoint beginLatLng, GeoPoint endLatLng) {
        Location beginLocation = convertLatLngToLocation(beginLatLng);
        Location endLocation = convertLatLngToLocation(endLatLng);
        return beginLocation.bearingTo(endLocation);
    }

    private Location convertLatLngToLocation(GeoPoint latLng) {
        Location location = new Location("someLoc");
        location.setLatitude(latLng.getLatitude());
        location.setLongitude(latLng.getLongitude());
        return location;
    }

    /**
     * Highlight the marker by index.
     */
    private void highLightMarker(int index) {
//        highLightMarker(markers.get(index));
    }

    /**
     * Highlight the marker by marker.
     */
//    private void highLightMarker(Marker marker) {

		/*
        for (Marker foundMarker : this.markers) {
			if (!foundMarker.equals(marker)) {
				foundMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
			} else {
				foundMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
				foundMarker.showInfoWindow();
			}
		}
		*/
//        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
    // marker.showInfoWindow();


    //Utils.bounceMarker(googleMap, marker);

//        this.selectedMarker = marker;
//    }
    public void createDefaultPolyLine(JSONArray jArray) {
//        PolylineOptions options = new PolylineOptions().width(8).color(getResources().getColor(R.color.history_polyline_color)).geodesic(true);
        try {

            if (jArray != null) {
                for (int i = 0; i < jArray.length(); i++) {
                    String latLngData = jArray.get(i).toString();
                    String[] str_msg_data_array = latLngData.split(",");

                    double mLat = Double.valueOf(str_msg_data_array[0]);
                    double mLng = Double.valueOf(str_msg_data_array[1]);
//                    LatLng position = new LatLng(mLat, mLng);
//                    options.add(position);

                    GeoPoint newPos = new GeoPoint(mLat, mLng);
                    polylinePoints.add(newPos);
                    Polyline myPath = new Polyline();
                    myPath.setColor(getResources().getColor(R.color.history_polyline_color));
                    myPath.setPoints(polylinePoints);

                    mapview.getOverlays().add(myPath);
//
                    mapview.invalidate();


                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


//        map.addPolyline(options);


    }

}
