package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vamosys.model.FuelFillDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HorizontalScrollView;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyMarkerFuelView;
import com.vamosys.utils.TypefaceUtil;
import com.vamosys.utils.UnCaughtException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.views.MapView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FuelFillNewReport extends AppCompatActivity implements View.OnClickListener, OnChartGestureListener,
        OnChartValueSelectedListener, OnMapReadyCallback {
    static ListView lv;
    TextView mTxtNoRecord, mTxtVehicleName, mTxtTotalFuelConsume, mTxtTripDistance, mTxtTotalFuelFill,mTxtstartfuel,mTxtendfuel,mTxtTotalMileage, mTxtOdoStart, mTxtOdoEnd;
    ImageView mCalendarImage, mImgMapViewClose;
    static ConstraintLayout mFuelDataMapLayout;
    LinearLayout mGoogleMapLayout;
    boolean isOsmEnabled = true;
    Toolbar mToolbar;
    ImageView $ChangeView;
    String intervalue,spinnervalue;

    //    ImageView mImgDataViewChange;
    ConnectionDetector cd;
    SharedPreferences sp;
    static Const cons;

    static int width;
    static int height;
    boolean isHeaderPresent = false, mIsLineChartEnabled = false;
    private List<FuelFillDto> mFuelFillData = new ArrayList<FuelFillDto>();

    ScrollView mMainScrollView;

    TableAdapter adapter;

    Dialog marker_info_dialog;
    Boolean iserror;

    //    String mTotalFuelValue = null;
    private static GoogleMap map;
    //    ImageView $ChangeView;
    boolean mIsMapPresent = false;
    String mStrUtcFromDate, mStrUtcToDate;
    String mSELECTED_MAP_TYPE = "Normal";
    static double mLatitude = 0;
    static double mLongitude = 0;
    static float vehicle_zoom_level = 15.5F;
    static Context mFuelContext;

    SimpleDateFormat timeFormatFuelFillReport = new SimpleDateFormat(
            "HH:mm:ss");

    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

    View bottomSheet;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;

    private int year, month, day;
    static final int TIME_DIALOG_ID = 1111;
    static final int DATE_DIALOG_ID = 2222;
    private int hour;
    private int minute;
    Spinner spinnerInterval;
    Button mBtnSubmit;
    TextView mTxtFromDate, mTxtEndDate,mTxtstart,mTxtend, mTxtFromTime, mTxtEndTime;
    static String mStartDate;
    static String mEndDate;
    String mStartTime;
    String mStartTimeValue;
    String mStartTimeFuelReport;
    String mEndTimeFuelReport;
    String mEndDateValue;
    String mEndTime;
    String mEndTimeValue;
    String mIntervalSpinnerSelectedValue;
    boolean isFromTime = false, isFromDate = false, isMapPresent = false;
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    ArrayList<String> mIntervalList;
    ArrayList<String> mSpinnerList;
    int mIntervalSpinnerSelectedPos = 0;
    private LineChart mChart;
    LinearLayout mChartLayout;

    ArrayList<Integer> xAxis;

    double mTotalFuelConsume,myvlue, mTotalFuelFill, mTripDistance, mOdoStart, mOdoEnd, mTotalMileage;

    //OSM related
    MapView mapview;
    private IMapController mapController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(
                FuelFillNewReport.this));
        setContentView(R.layout.activity_fuel_fill_new_report);
        cons = new Const();
        mIntervalList = new ArrayList<String>();
        mSpinnerList = new ArrayList<String>();


//        screenArrange();
//        System.out.println("Hi onCreate called ");

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.fuel_report));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        mFuelContext = getApplicationContext();
//        adapter = new TableAdapter(this);

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }


        intervalue = sp.getString("intervalvalue","");
        spinnervalue =sp.getString("spinnervalue","");

        mIntervalSpinnerSelectedValue = intervalue;

//        if (spinnervalue !=null){
//            Log.d("spinnervalue",""+spinnervalue);
//        }
        init();

        mStartDate = Const.getCurrentDate();
//        mEndDate = Const.getTripYesterdayDate2();

//        mStartDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));


        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);

        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);


        mFromHourValue = 00;
        mFromMinuteValue = 00;

//        System.out.println("Hi date is :::" + timeFormat.format(cal.getTime()) + " mFromHourValue " + mFromHourValue + " " + mFromMinuteValue);

//        mStartTimeValue = "12:00 AM";
        mStartTimeValue = timeFormatShow.format(cal.getTime());
//        mStartTime = "12:00:00 am";
        mStartTime = timeFormat.format(cal.getTime());
        mStartTimeFuelReport = timeFormatFuelFillReport.format(cal.getTime());


        final Calendar c = Calendar.getInstance();
        // Current Hour
        hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        minute = c.get(Calendar.MINUTE);
        isFromTime = false;
        updateTime(hour, minute);


        Bundle extras = getIntent().getExtras();
        if (extras == null) {
        } else {

//            in.putExtra("start_date", mStartDate);
//            in.putExtra("start_time", mStartTimeFuelReport);
//            in.putExtra("end_date", mEndDate);
//            in.putExtra("end_time", mEndTimeFuelReport);
//
            mIsLineChartEnabled = extras.getBoolean("is_chart_enabled", false);
//            mStartDate = extras.getString("start_date");
//            mStrFromTime = extras.getString("start_time");
//            mEndDate = extras.getString("end_date");
//            mStrToTime = extras.getString("end_time");
//            mIsFuelChartEnabled = extras.getBoolean("is_chart_enabled", false);
        }


        mIntervalList = intervalSpinnerDataValue();
        mSpinnerList = intervalSpinnerValue();



        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        adapter = new TableAdapter(this);


//        if (savedInstanceState != null) {
//            mStartDate = savedInstanceState.getString("mStartDate");
//            mStartTime = savedInstanceState.getString("mStartTime");
//            mEndDate = savedInstanceState.getString("mEndDate");
//            mEndTime = savedInstanceState.getString("mEndTime");
//            mIntervalSpinnerSelectedValue = savedInstanceState.getString("mIntervalSpinnerSelectedValue");
//
//            System.out.println("Hi orientation changes " + mStartDate + mEndDate + mStartTime);
//            setBottomLayoutData();
//        }

//        setBottomLayoutData();
//
//        if (cd.isConnectingToInternet()) {
////                                mStrFromDate = fromdatevalue.getText().toString().trim();
////                                mStrToDate = todatevalue.getText().toString().trim();
//
//            System.out.println("Hi 222 orientation changes " + mStartDate + mEndDate + mStartTime);
//
//            mFuelFillData.clear();
//            new getKmsSummaryData().execute();
//        } else {
//            Toast.makeText(getApplicationContext(),
//                    "Please check your network connection",
//                    Toast.LENGTH_SHORT).show();
//        }


//        mImgDataViewChange.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////               mIsMapPresent=false;
//                setListData();
//
//
//            }
//        });

        spinnerInterval
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

                {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mIntervalSpinnerSelectedPos = position;

                        SharedPreferences.Editor editor = sp.edit();
                        if (position != 0) {
                            editor.putString("intervalvalue",String.valueOf(mIntervalList.get(position)));
                            editor.putString("spinnervalue",mSpinnerList.get(position));
                            mIntervalSpinnerSelectedValue = String.valueOf(mIntervalList.get(position));
                        } else {
                            mIntervalSpinnerSelectedValue = intervalue;
                        }
                        editor.commit();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

        mToolbar.setNavigationOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(FuelFillNewReport.this, MapMenuActivity.class));
                finish();


            }
        });

//        System.out.println("Hi chart enabled " + Constant.mFuelIsChartView + " " + mIsLineChartEnabled);
        new getKmsSummaryData().execute();

    }

    @Override
    protected void onResume() {
        super.onResume();
//        System.out.println("Hi onResume called ");
        if (Constant.mFuelStoredFromDate != null) {
            mStartDate = Constant.mFuelStoredFromDate;
        }
        if (Constant.mFuelStoredToDate != null) {
            mEndDate = Constant.mFuelStoredToDate;
        }
        if (Constant.mFuelStoredFromTime != null) {
            mStartTime = Constant.mFuelStoredFromTime;
        }
        if (Constant.mFuelStoredToTime != null) {
            mEndTime = Constant.mFuelStoredToTime;
        }
        if (Constant.mFuelInterval != null) {
            mIntervalSpinnerSelectedValue = Constant.mFuelInterval;
        }
        //   mIsLineChartEnabled = Constant.mFuelIsChartView;

//        System.out.println("Hi onresume chart enabled " + Constant.mFuelIsChartView);

//        System.out.println("Hi onResume orientation changes " + mStartDate + mEndDate + mStartTime);
        setBottomLayoutData();

//        if (cd.isConnectingToInternet()) {
////                                mStrFromDate = fromdatevalue.getText().toString().trim();
////                                mStrToDate = todatevalue.getText().toString().trim();
//
////            System.out.println("Hi 222 orientation changes " + mStartDate + mEndDate + mStartTime);
//
//            mFuelFillData.clear();
//            new getKmsSummaryData().execute();
//        } else {
//            Toast.makeText(getApplicationContext(),
//                    "Please check your network connection",
//                    Toast.LENGTH_SHORT).show();
//        }


    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        System.out.println("Hi onRestart called ");
    }

    @Override
    protected void onStart() {
        super.onStart();
//        System.out.println("Hi onStart called ");
    }

    @Override
    protected void onStop() {
        super.onStop();
//        System.out.println("Hi onStop called ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        System.out.println("Hi onDestroy called ");
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the state of item position

        System.out.println("Hi saved instance state " + mIsLineChartEnabled);
        Constant.mFuelStoredFromDate = mStartDate;
        Constant.mFuelStoredToDate = mEndDate;
        Constant.mFuelStoredFromTime = mStartTime;
        Constant.mFuelStoredToTime = mEndTime;
        Constant.mFuelInterval = mIntervalSpinnerSelectedValue;
        Constant.mFuelIsChartView = mIsLineChartEnabled;
//        outState.putString("mStartDate", mStartDate);
//        outState.putString("mStartTime", mStartTime);
//        outState.putString("mEndDate", mEndDate);
//        outState.putString("mEndTime", mEndTime);
        // outState.putString("mIntervalSpinnerSelectedValue", mIntervalSpinnerSelectedValue);
//        System.out.println("Hi save instance called ");
    }

//    @Override
//    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//
//        // Read the state of item position
//        mStartDate = savedInstanceState.getString("mStartDate");thats
//        mStartTime = savedInstanceState.getString("mStartTime");
//        mEndDate = savedInstanceState.getString("mEndDate");
//        mEndTime = savedInstanceState.getString("mEndTime");
//        mEndTime = savedInstanceState.getString("mIntervalSpinnerSelectedValue");
//
//        System.out.println("Hi 000 orientation changes " + mStartDate + mEndDate + mStartTime);
//
//
//    }


    public void init() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        $ChangeView = (ImageView) findViewById(R.id.temperature_data_map_ViewIcon);
        $ChangeView.setOnClickListener(this);

        mChart = (LineChart) findViewById(R.id.chart);
        mChart.zoom(4, 1, 1, 1);
        mChartLayout = (LinearLayout) findViewById(R.id.chart_layout);
//        $ChangeView = (ImageView) findViewById(R.id.fuel_fill_data_map_ViewIcon);

        mMainScrollView = (ScrollView) findViewById(R.id.scrollview_layout);
        mMainScrollView.setVisibility(View.VISIBLE);
//        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);
//        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);


        mFuelDataMapLayout =  findViewById(R.id.temperature_data_map_view_layout);
        mFuelDataMapLayout.setVisibility(View.GONE);

        lv = (ListView) findViewById(R.id.tstoppage_report_listView1);
//        mImgDataViewChange = (ImageView) findViewById(R.id.fuelFill_report_change_data_view);
//        mImgDataViewChange.setVisibility(View.GONE);

        mTxtVehicleName = (TextView) findViewById(R.id.selected_vehicle_txt);
        mTxtVehicleName.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
//        mTxtVehicleNameValue = (TextView) findViewById(R.id.fuelFill_vehicle_name_value_txt);
//        mTxtVehicleNameValue.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);

        mTxtNoRecord = (TextView) findViewById(R.id.report_no_record_txt);

        spinnerInterval = (Spinner) findViewById(R.id.spinner_interval);

        mTxtFromDate = (TextView) findViewById(R.id.txt_start_date_value);
        mTxtFromTime = (TextView) findViewById(R.id.txt_start_time_value);
        mTxtEndDate = (TextView) findViewById(R.id.txt_end_date_value);
        mTxtEndTime = (TextView) findViewById(R.id.txt_end_time_value);
        mTxtstart = (TextView) findViewById(R.id.txt_date_start);
        mTxtend = (TextView) findViewById(R.id.txt_date_end);
        mBtnSubmit = (Button) findViewById(R.id.btn_done);

        mTxtTotalFuelConsume = (TextView) findViewById(R.id.txt_total_fuel_consume);
        mTxtTripDistance = (TextView) findViewById(R.id.txt_trip_distance);
        mTxtTotalFuelFill = (TextView) findViewById(R.id.txt_total_fuel_fill);
        mTxtstartfuel = (TextView) findViewById(R.id.txt_start_fuel);
        mTxtendfuel = (TextView) findViewById(R.id.txt_end_fuel);

        // mTxtTotalMileage = (TextView) findViewById(R.id.txt_total_mileage);
        mTxtOdoStart = (TextView) findViewById(R.id.txt_odo_start);
        mTxtOdoEnd = (TextView) findViewById(R.id.txt_odo_end);

//        mCalendarImage = (ImageView) mToolbar.findViewById(R.id.img_calendar);
//        mImgMapViewClose = (ImageView) mToolbar.findViewById(R.id.img_map_close);
//        mImgMapViewClose.setOnClickListener(this);
//        mCalendarImage.setOnClickListener(this);

        mTxtFromTime.setOnClickListener(this);
        mTxtFromDate.setOnClickListener(this);
        mTxtEndDate.setOnClickListener(this);
        mTxtEndTime.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mTxtTotalFuelFill.setOnClickListener(this);

//        mChart.setVisibility(View.GONE);

        mChartLayout.setVisibility(View.GONE);
        lv.setVisibility(View.VISIBLE);
        Log.d("googlemap","success");
        $ChangeView.setVisibility(View.VISIBLE);
//        mGoogleMapLayout.setVisibility(View.VISIBLE);
//        rl.setVisibility(View.GONE);

        FragmentManager myFragmentManager = getSupportFragmentManager();
        SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                .findFragmentById(R.id.temperature_data_map);
        myMapFragment.getMapAsync(this);

    }

    private void opptionPopUp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

//                mapview.setTileSource(TileSourceFactory.MAPNIK);

                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//                mapview.setTileSource(TileSourceFactory.USGS_SAT);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//                mapview.setTileSource(TileSourceFactory.HIKEBIKEMAP);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//                mapview.setTileSource(TileSourceFactory.USGS_TOPO);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }

//    private void formLineChartNewOld() {
//
//
//        mChart.getDescription().setEnabled(false);
//        mChart.setNoDataText("Loading...");
//        mChart.setOnChartGestureListener(this);
//        mChart.setOnChartValueSelectedListener(this);
//
//        mChart.setTouchEnabled(true);
//        mChart.setDoubleTapToZoomEnabled(false);
//        mChart.setDragEnabled(true);
//        mChart.setScaleXEnabled(true);
//        mChart.setScaleYEnabled(false);
//        mChart.setPinchZoom(false);
////        mChart.zoom(200, 200, 200, 200);
////        mChart.setVisibleXRangeMaximum(10);
//
//        mChart.setDrawGridBackground(false);
//        mChart.getDescription().setEnabled(false);
//        // create a custom MarkerView (extend MarkerView) and specify the layout
//        // to use for it
//        MyMarkerFuelView mv = new MyMarkerFuelView(this, R.layout.custom_routeplayback_marker_fuel_view, getXAxisValues(), "Fuel level", mFuelFillData);
//        mv.setChartView(mChart); // For bounds control
//        mChart.setMarker(mv); // Set the marker to the chart
//
//        XAxis xAxis = mChart.getXAxis();
//        xAxis.enableGridDashedLine(10f, 10f, 0f);
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
////        xAxis.setValueFormatter(new LabelFormatter(getXAxisValues()));
//        xAxis.setGranularity(1f);
//        // xAxis.setGranularityEnabled(true);
//        //  xAxis.setEnabled(false);
//
//        xAxis.setTextSize(11f);
//        xAxis.setTextColor(Color.BLACK);
//        xAxis.setDrawGridLines(false);
//        xAxis.setDrawAxisLine(false);
//
//
//        YAxis leftAxis = mChart.getAxisLeft();
//        leftAxis.removeAllLimitLines();
//        leftAxis.setDrawAxisLine(false);
//        leftAxis.setDrawZeroLine(false);
//        leftAxis.setDrawLimitLinesBehindData(false);
////        mChart.getAxisRight().setEnabled(false);
//
//        YAxis rightAxis = mChart.getAxisRight();
//        rightAxis.removeAllLimitLines();
//        rightAxis.setDrawAxisLine(false);
//        rightAxis.setDrawZeroLine(false);
//        rightAxis.setDrawLimitLinesBehindData(false);
//
//        mChart.setOnChartValueSelectedListener(this);
//        mChart.getLegend().setEnabled(false);
//        mChart.invalidate();
//
//
//        // add data
//        setData();
//
////        mChart.setVisibleXRange(20);
////        mChart.setVisibleYRange(20f, AxisDependency.LEFT);
////        mChart.centerViewTo(20, 50, AxisDependency.LEFT);
//
//        mChart.animateX(2500);
//        //mChart.invalidate();
//
//        // get the legend (only possible after setting data)
//        Legend l = mChart.getLegend();
//
//        // modify the legend ...
//        l.setForm(Legend.LegendForm.LINE);
//        l.setTextSize(11f);
//        l.setTextColor(Color.WHITE);
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setDrawInside(false);
//
////        mChart.setVisibleXRange(10);
////        mChart.invalidate();
//
//        mChart.notifyDataSetChanged();
//
//
//    }

    private void formLineChartNew() {

//        mTxtTotalFuelConsume.setText(mTotalFuelConsume + " ltrs");
//        mTxtTripDistance.setText(mTripDistance + " kms");

        for (int i=0 ; i<mFuelFillData.size(); i++){
            mOdoEnd = mFuelFillData.get(i).getOdoMeterReading();
            mOdoStart = mFuelFillData.get(0).getOdoMeterReading();

        }

        mTxtOdoStart.setText("Odo Start : " + mOdoStart);
        mTxtOdoEnd.setText("Odo End : " + mOdoEnd);
//        mTxtTotalMileage.setText(mTotalMileage + " km/l");

        /*chart*/
//        if (mTotalFuelFill > 0) {
//
//        } else {
//            mTotalFuelFill = 0;
//        }
//
//        if (mTotalFuelFill > 0) {
//            String mText = "<u>" + mTotalFuelFill + "</u>";
//            mTxtTotalFuelFill.setText(Html.fromHtml(mText));
//            mTxtTotalFuelFill.setTextColor(Color.BLUE);
//        } else {
//            mTxtTotalFuelFill.setText("" + mTotalFuelFill);
//            mTxtTotalFuelFill.setTextColor(Color.BLACK);
//        }


        mChart.setOnChartValueSelectedListener(this);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        mChart.setDragDecelerationFrictionCoef(0.9f);


        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setDragEnabled(true);
        mChart.setScaleXEnabled(true);
        mChart.setScaleYEnabled(false);
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);
        mChart.setHighlightPerDragEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
//        mChart.setBackgroundColor(Color.LTGRAY);

        // add data
        Log.d("formlinechart",""+mFuelFillData.size());
        setData();


        mChart.animateX(500);

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.SQUARE);
//        l.setTypeface(mTfLight);
        l.setTextSize(11f);
        l.setTextColor(Color.BLACK);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);



        MyMarkerFuelView mv = new MyMarkerFuelView(this, R.layout.custom_routeplayback_marker_fuel_view, getXAxisValues(), "Fuel level", mFuelFillData);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart

        XAxis xAxis = mChart.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setGranularity(1f);
        xAxis.setEnabled(false);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTypeface(TypefaceUtil.getTextBold(getApplicationContext()));
        leftAxis.setTextColor(Color.rgb(247, 163, 92));
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);


        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setTypeface(TypefaceUtil.getTextBold(getApplicationContext()));
        rightAxis.setTextColor(Color.rgb(135, 206, 250));
        rightAxis.setDrawGridLines(false);
        rightAxis.setDrawZeroLine(false);
        rightAxis.setGranularityEnabled(false);
    }


    private void setData() {
        //ArrayList<Entry> valuesRight = new ArrayList<Entry>();
        String fuelvalue = null,speedvalue = null;
        if (mFuelFillData.size() == 0){
            Log.d("mFuelFillsize","success");
        }
        else if (mFuelFillData.size() > 0) {
            ArrayList<Entry> valuesRight = new ArrayList<Entry>();
            for (int i = 0; i < mFuelFillData.size(); i++) {


                Log.d("newspeed", "" + mFuelFillData.get(i).getNewSpeed());
                fuelvalue = mFuelFillData.get(i).getFuelLtr();
                Log.d("fuelvalue", "" +fuelvalue);
                if (mFuelFillData.get(i).getFuelLtr() == null){
                    Log.d("fuelllll","failure");
                } else{
                    Entry v1e1 = new Entry(i, Float.valueOf(mFuelFillData.get(i).getFuelLtr()));
                    valuesRight.add(v1e1);
                }


            }

            ArrayList<Entry> valuesLeft = new ArrayList<Entry>();
            for (int i = 0; i < mFuelFillData.size(); i++) {
                speedvalue =  mFuelFillData.get(i).getNewSpeed();
                Log.d("speedvalue", "" +speedvalue);
                if (mFuelFillData.get(i).getNewSpeed() == null){
                    Log.d("newsppee","failure");
                }else {
                    Entry v1e1 = new Entry(i, Float.valueOf(mFuelFillData.get(i).getNewSpeed()));
                    valuesLeft.add(v1e1);
                }

            }

            Log.d("speedvalue1", "" +speedvalue);
            Log.d("fuelvalue1", "" +fuelvalue);
            if (speedvalue == null && fuelvalue == null){
                Log.d("linechart","failure");
            } else {
                Log.d("linechart","success");
                LineDataSet set1, set2;

                if (mChart.getData() != null &&
                        mChart.getData().getDataSetCount() > 0) {
                    set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
                    set2 = (LineDataSet) mChart.getData().getDataSetByIndex(1);
                    set1.setValues(valuesRight);
                    set2.setValues(valuesLeft);
                    mChart.getData().notifyDataChanged();
                    mChart.notifyDataSetChanged();
                } else {
                    // create a dataset and give it a type
                    //dataset for right
                    set1 = new LineDataSet(valuesRight, "Fuel");
                    set1.setAxisDependency(YAxis.AxisDependency.RIGHT);
                    set1.setColor(Color.rgb(135, 206, 250));
                    set1.setLineWidth(1.5f);
                    set1.setDrawCircleHole(false);
//                set1.setCircleColor(Color.BLACK);
                    set1.setCircleColor(Color.rgb(135, 206, 250));
                    set1.setCircleRadius(3f);
                    set1.setDrawCircles(true);
                    set1.setValueTextSize(9f);
                    set1.setDrawFilled(true);
                    set1.setFormLineWidth(1f);
//                set1.setDrawValues(false);
                    set1.setFormSize(15.f);
                    set1.setDrawValues(true);

                    if (Utils.getSDKInt() >= 18) {
                        // fill drawable only supported on api level 18 and above
                        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
                        set1.setFillDrawable(drawable);
                    } else {
                        set1.setFillColor(Color.BLACK);
                    }


                    //dataset for left
                    set2 = new LineDataSet(valuesLeft, "Speed");
                    set2.setAxisDependency(YAxis.AxisDependency.LEFT);
                    set2.setColor(Color.rgb(247, 163, 92));
//                set2.setColor(Color.MAGENTA);
                    set2.setLineWidth(1.5f);

                    set2.setDrawCircleHole(false);
//                set2.setCircleColor(Color.BLACK);
                    set2.setCircleColor(Color.rgb(247, 163, 92));
                    set2.setCircleRadius(3f);
                    set2.setDrawCircles(true);
                    set2.setValueTextSize(9f);
                    set2.setDrawFilled(false);

                    set2.setFormLineWidth(1f);
//                set1.setDrawValues(false);
                    set2.setFormSize(15.f);
                    set2.setDrawValues(true);

                    ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                    dataSets.add(set1); // add the datasets

                    // create a data object with the datasets
//                LineData data = new LineData(dataSets);

                    LineData data = new LineData(set1, set2);
                    mChart.setData(data);
                }
            }


//            LineDataSet set1, set2;
//
//            if (mChart.getData() != null &&
//                    mChart.getData().getDataSetCount() > 0) {
//                set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
//                set2 = (LineDataSet) mChart.getData().getDataSetByIndex(1);
//                set1.setValues(valuesRight);
//                set2.setValues(valuesLeft);
//                mChart.getData().notifyDataChanged();
//                mChart.notifyDataSetChanged();
//            } else {
//                // create a dataset and give it a type
//                //dataset for right
//                set1 = new LineDataSet(valuesRight, "Fuel");
//                set1.setAxisDependency(YAxis.AxisDependency.RIGHT);
//                set1.setColor(Color.rgb(135, 206, 250));
//                set1.setLineWidth(1.5f);
//                set1.setDrawCircleHole(false);
////                set1.setCircleColor(Color.BLACK);
//                set1.setCircleColor(Color.rgb(135, 206, 250));
//                set1.setCircleRadius(3f);
//                set1.setDrawCircles(true);
//                set1.setValueTextSize(9f);
//                set1.setDrawFilled(true);
//                set1.setFormLineWidth(1f);
////                set1.setDrawValues(false);
//                set1.setFormSize(15.f);
//                set1.setDrawValues(true);
//
//                if (Utils.getSDKInt() >= 18) {
//                    // fill drawable only supported on api level 18 and above
//                    Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
//                    set1.setFillDrawable(drawable);
//                } else {
//                    set1.setFillColor(Color.BLACK);
//                }
//
//
//                //dataset for left
//                set2 = new LineDataSet(valuesLeft, "Speed");
//                set2.setAxisDependency(YAxis.AxisDependency.LEFT);
//                set2.setColor(Color.rgb(247, 163, 92));
////                set2.setColor(Color.MAGENTA);
//                set2.setLineWidth(1.5f);
//
//                set2.setDrawCircleHole(false);
////                set2.setCircleColor(Color.BLACK);
//                set2.setCircleColor(Color.rgb(247, 163, 92));
//                set2.setCircleRadius(3f);
//                set2.setDrawCircles(true);
//                set2.setValueTextSize(9f);
//                set2.setDrawFilled(false);
//
//                set2.setFormLineWidth(1f);
////                set1.setDrawValues(false);
//                set2.setFormSize(15.f);
//                set2.setDrawValues(true);
//
//                ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//                dataSets.add(set1); // add the datasets
//
//                // create a data object with the datasets
////                LineData data = new LineData(dataSets);
//
//                LineData data = new LineData(set1, set2);
//                mChart.setData(data);
//            }
        }
    }




    private ArrayList<Integer> getXAxisValues() {
        xAxis = new ArrayList<>();

        if (mFuelFillData.size() == 0){

        }else{


            for (int i = 0; i < mFuelFillData.size(); i++) {
//            xAxis.add(Const.getAcReportTime(mFuelFillData.get(i).getStartTime()));
                xAxis.add(i);
            }
        }

        return xAxis;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_calendar_chart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_calendar:
//                storeFavourtite();

                if (!bottomSheetEnabled) {
                    setIntervalSpinnerValue();
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    // setData();
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }

                break;

            case R.id.action_chart:
                //new getfuelmachinery().execute();
                if (mIsLineChartEnabled) {
//                    mImgDataViewChange.setBackgroundResource(R.drawable.bar_chart);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.line_chart));

//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart, getApplicationContext().getTheme()));
//                    } else {
//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart));
//                    }
                    mIsLineChartEnabled = false;
//                    setupBarChart();
//                    mChart.setVisibility(View.GONE);
                    new getKmsSummaryData().execute();
                    // new getfuelmachinery().execute();
                    mChartLayout.setVisibility(View.GONE);
                    lv.setVisibility(View.VISIBLE);
                    mMainScrollView.setVisibility(View.VISIBLE);


                } else {
//                    mImgDataViewChange.setBackgroundResource(R.drawable.data_table);

//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table, getApplicationContext().getTheme()));
//                    } else {
//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table));
//                    }

                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.data_table));

                    mIsLineChartEnabled = true;
                    mMainScrollView.setVisibility(View.GONE);
//                    mChart.setVisibility(View.VISIBLE);
                    mChartLayout.setVisibility(View.VISIBLE);
                    mTxtstart.setVisibility(View.GONE);
                    mTxtend.setVisibility(View.GONE);
                    lv.setVisibility(View.GONE);


//                    setupLineChart();
                    new getfuelmachinery().execute();
                    // new getKmsSummaryData().execute();
                    formLineChartNew();
                }


                break;

            default:
                break;
        }

        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_start_date_value:
                isFromDate = true;
                DialogFragment dialogfragment = new DatePickerDialogTheme();

                dialogfragment.show(getFragmentManager(), "Theme");

                break;
            case R.id.txt_start_time_value:
//                showDialog(DATE_DIALOG_FROMID);
                isFromTime = true;
                //   showDialog(TIME_DIALOG_ID);

                showTimeDialog();
                break;
            case R.id.txt_end_date_value:
//                createNumberDialog(1);
                isFromDate = false;
                DialogFragment dialogfragment1 = new DatePickerDialogTheme1();

                dialogfragment1.show(getFragmentManager (),"Theme");
//                show(1);

                break;
            case R.id.txt_end_time_value:
//                createNumberDialog(0);
                isFromTime = false;
                // showDialog(TIME_DIALOG_ID);

                showTimeDialog();
//                show(0);
                break;
            case R.id.txt_total_fuel_fill:
//                createNumberDialog(0);
                if (mTotalFuelFill > 0) {

                    Intent in = new Intent(FuelFillNewReport.this, FuelFillReport.class);
                    in.putExtra("start_date", mStartDate);
                    in.putExtra("start_time", mStartTimeFuelReport);
                    in.putExtra("end_date", mEndDate);
                    in.putExtra("end_time", mEndTimeFuelReport);
                    in.putExtra("is_chart_enabled", mIsLineChartEnabled);
                    startActivity(in);
                    //  finish();

//                    startActivity(new Intent(FuelFillNewReport.this, FuelFillReport.class));
//                    finish();
                }
//                show(0);
                break;
//            case R.id.img_calendar:
//                if (!bottomSheetEnabled) {
//                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                } else {
//                    // setData();
//                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }
//
//                if (bottomSheetEnabled) {
//                    bottomSheetEnabled = false;
//                } else {
//                    bottomSheetEnabled = true;
//                }
////                break;
//            case R.id.img_map_close:
//
//
//                mFuelDataMapLayout.setVisibility(View.GONE);
////                if (isMapPresent) {
//                isMapPresent = false;
//                lv.setVisibility(View.VISIBLE);
//                mImgMapViewClose.setVisibility(View.GONE);
////                }
//
//
//                break;
//            case R.id.temperature_data_map_ViewIcon:
//                opptionPopUp();
//                break;
            case R.id.btn_done:
//                createNumberDialog(0);
//                show(0);
                Log.d("check","");

                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();
//                    mStoppageData = new ArrayList<StoppageReportDto>();
                    mTxtstart.setVisibility(View.VISIBLE);
                    mTxtend.setVisibility(View.VISIBLE);
                    mTxtstart.setText("Start date : " + mStartDate);
                    mTxtend.setText("End date : " + mEndDate);
                    adapter = null;
                    adapter = new TableAdapter(FuelFillNewReport.this);
                    //     String url = Const.API_URL + "/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    mFuelFillData.clear();
                    lv.setAdapter(null);

//                    String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    // String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&fromTime=" + id_from_time.getText().toString() + "&toDate=" + id_to_date.getText().toString() + "&toTime=" + id_to_time.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                    // String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                    new getKmsSummaryData().execute();
                    new getfuelmachinery().execute();
                    //new getfuelmachinery().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }
//                new CameraReportIamgeListActivtiy.getCameraReport().execute();
                break;
        }

    }

    private void  setIntervalSpinnerValue() {
        ArrayAdapter dataAdapter = new ArrayAdapter(this, R.layout.spinner_row, intervalSpinnerValue());

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        // spinnerFromHour.setPrompt("Select From Hour");
        Log.d("dataAdapter",""+intervalSpinnerDataValue());

        spinnerInterval.setAdapter(dataAdapter);


        int spinnerPosition = dataAdapter.getPosition(spinnervalue);

//set the default according to value
        spinnerInterval.setSelection(spinnerPosition);


        //spinnerInterval.setSelection((mIntervalSpinnerSelectedPos));
        Log.d("dataAdapter1",""+mIntervalSpinnerSelectedPos);
    }

    private ArrayList<String> intervalSpinnerValue() {
        ArrayList<String> mHrList = new ArrayList<String>();
        mHrList.add("Interval");
        mHrList.add("1 mins");
        mHrList.add("2 mins");
        mHrList.add("5 mins");
        mHrList.add("10 mins");
        mHrList.add("15 mins");
        mHrList.add("30 mins");

        return mHrList;
    }

    private ArrayList<String> intervalSpinnerDataValue() {
        ArrayList<String> mHrList = new ArrayList<String>();
        mHrList.add("0");
        mHrList.add("1");
        mHrList.add("2");
        mHrList.add("5");
        mHrList.add("10");
        mHrList.add("15");
        mHrList.add("30");

        return mHrList;
    }

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);
    }


    private class getKmsSummaryData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                //  Const constaccess = new Const();

                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
                Log.d("fromtrim",""+string_from_date);
                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();
                Log.d("totrim",""+string_to_date);

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                Date d = f.parse(string_from_date);

//                System.out.println("The from date after converted  :::::" + d);

                long fromMilliSeconds = d.getTime();
//                System.out.println("The from time in millis :::::" + fromMilliSeconds);
                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
//                long fromMilliSeconds =1470767400000;
//                long toMilliSeconds=1471685033000;


                //  System.out.println("The to time in millis :::::" + toMilliSeconds);


//                result = constaccess
//                        .sendGet(Const.API_URL + "/mobile/getExecutiveReport?groupId=" + mSelectedGroup + "&fromDate=" + mStrFromDate + "&toDate=" + mStrToDate + "&userId=" + mUserId, 30000);
                HttpConfig ht = new HttpConfig();
//                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDropFillReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=1&fromDate=" + mStrFromDate + "&fromTime=" + mStrFromTime + "&toDate=" + mStrToDate + "&toTime=" + mStrToTime + "&fuelDrop=true&fuelFill=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
//                System.out.println("Hi 333 orientation changes " + mStartDate + mEndDate + mStartTime);

//                http://www.gpsvts.net/mobile/getVehicleFuelHistory4Mobile?vehicleId=AKSHYA-TN29BZ1220&interval=1&fromDate=2017-11-22&fromTime=00:00:00&toDate=2017-11-29&toTime=12:07:32&fuelDrop=true&fuelFill=true&fromDateUTC=1511289000000&toDateUTC=1511937452000&userId=AKSHAYATRANS

//                result = ht.httpGet(Const.API_URL + "/mobile/getVehicleFuelHistory4Mobile?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=" + mIntervalSpinnerSelectedValue + "&fuelDrop=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
//                result = ht.httpGet(Const.API_URL + "/mobile/getFuelRawData?vehicleId=" + Constant.SELECTED_VEHICLE_ID  + "&fromTimeUtc=" + fromMilliSeconds + "&toTimeUtc=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
                Log.d("selectspinner1",""+mIntervalSpinnerSelectedValue);
                result = ht.httpGet(Const.API_URL + "/mobile/getFuelRawData?vehicleId=" + Constant.SELECTED_VEHICLE_ID  + "&fromTimeUtc=" + fromMilliSeconds + "&toTimeUtc=" + toMilliSeconds + "&interval=" + mIntervalSpinnerSelectedValue + "&userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mFuelFillData = new ArrayList<>();

            if (result != null && result.length() > 0) {
                String mPrevAdds = null;
                String error = null;
                try {
                    JSONArray jsonArray = new JSONArray(result.trim());
                    //  mFuelFillData.clear();
                    for (int i=0; i<jsonArray.length();i++){
                        JSONObject jsonFuelObject = jsonArray.getJSONObject(i);
                        FuelFillDto f = new FuelFillDto();
                        if (jsonFuelObject.has("dt")) {
                            f.setStartTime(Long.valueOf(jsonFuelObject.getString("dt")));
                        }
                        if (jsonFuelObject.has("fuelLitr")) {
                            f.setFuelLtr(jsonFuelObject.getString("fuelLitr"));
                        }


                        if (jsonFuelObject.has("sp")) {
                            f.setNewSpeed(jsonFuelObject.getString("sp"));
                        }

                        if (jsonFuelObject.has("ignitionStatus")) {
                            f.setIgnitionStatus(jsonFuelObject.getString("ignitionStatus"));
                        }

                        if (jsonFuelObject.has("odoMeterReading")) {
                            f.setOdoMeterReading(jsonFuelObject.getDouble("odoMeterReading"));
                        }

                        if (jsonFuelObject.has("address")) {
                            if (jsonFuelObject.getString("address").equals("P")) {
                                f.setAddress(mPrevAdds);
                            } else {
                                mPrevAdds = jsonFuelObject.getString("address");
                                f.setAddress(jsonFuelObject.getString("address"));
                            }
                        }
                        if (jsonFuelObject.has("error")){
                            Log.d("errorfound",""+jsonFuelObject.getString("error"));
                            if(jsonFuelObject.getString("error").equalsIgnoreCase("No Data Found.")){
//                                mChartLayout.setVisibility(View.GONE);
//                                mChart.setVisibility(View.GONE);
                                // mMainScrollView.setVisibility(View.GONE);
                                lv.setVisibility(View.GONE);
                                mChart.clear();
                                mChart.setData(null);
                                mChart.notifyDataSetChanged();
                                Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                            }
                            if (jsonFuelObject.getString("error").equalsIgnoreCase("This report is not privileged for selected vehicle or group.")){
                                mMainScrollView.setVisibility(View.GONE);
//                                mChartLayout.setVisibility(View.GONE);
//                                mChart.setVisibility(View.GONE);
                                lv.setVisibility(View.GONE);
                                mChart.clear();
                                mChart.setData(null);
                                mChart.notifyDataSetChanged();
                                Toast.makeText(FuelFillNewReport.this, "This report is not privileged for selected vehicle or group.", Toast.LENGTH_SHORT).show();
                            }
                            if (jsonFuelObject.getString("error").equalsIgnoreCase("Please select date range within 3 days.")) {
                                lv.setVisibility(View.GONE);
//                                mChart.setVisibility(View.GONE);
//                                lv.setVisibility(View.GONE);
                                mMainScrollView.setVisibility(View.GONE);
                                mChart.clear();
                                mChart.setData(null);
                                mChart.notifyDataSetChanged();
                                Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                            }                        }






                        mFuelFillData.add(f);
                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("errormsg",""+error);
//                if (error !=null){
//                    lv.setVisibility(View.GONE);
//                    Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
//
//                }
//                else {
//                    setTableLayoutData();
//                }

//                try {
//                    JSONObject jsonObject = new JSONObject(result.trim());
//
//                    if (jsonObject.has("totalFuelConsume")) {
//                        mTotalFuelConsume = jsonObject.getDouble("totalFuelConsume");
//                    }
//                    if (jsonObject.has("tripDistance")) {
//                        mTripDistance = jsonObject.getDouble("tripDistance");
//                    }
//
//                    if (jsonObject.has("totalFuelFill")) {
//                        mTotalFuelFill = jsonObject.getInt("totalFuelFill");
//                    }
//
//                    if (jsonObject.has("openingOdoReading")) {
//                        mOdoStart = jsonObject.getDouble("openingOdoReading");
//                    }
//                    if (jsonObject.has("closingOdoReading")) {
//                        mOdoEnd = jsonObject.getDouble("closingOdoReading");
//                    }
//
//                    if (jsonObject.has("milege")) {
//                        mTotalMileage = jsonObject.getInt("milege");
//                    }
//
//
//                    if (jsonObject.has("history4Mobile")) {
//
////                        if (jsonObject.get("history4Mobile") != null && !jsonObject.get("history4Mobile").equals("null")) {
//                        try {
//                            JSONArray jArrayFuel = jsonObject.getJSONArray("history4Mobile");
//
//                            String mPrevAdds = null;
//                            for (int i = 0; i < jArrayFuel.length(); i++) {
//                                JSONObject jsonFuelObject = jArrayFuel.getJSONObject(i);
//                                FuelFillDto f = new FuelFillDto();
//
//                                if (jsonFuelObject.has("dt")) {
//                                    f.setStartTime(Long.valueOf(jsonFuelObject.getString("dt")));
//                                }
//                                if (jsonFuelObject.has("fuelLitr")) {
//                                    f.setFuelLtr(jsonFuelObject.getString("fuelLitr"));
//                                }
//
//
//                                if (jsonFuelObject.has("newSpeed")) {
//                                    f.setNewSpeed(jsonFuelObject.getString("newSpeed"));
//                                }
//
//                                if (jsonFuelObject.has("ignitionStatus")) {
//                                    f.setIgnitionStatus(jsonFuelObject.getString("ignitionStatus"));
//                                }
//
//                                if (jsonFuelObject.has("odoMeterReading")) {
//                                    f.setOdoMeterReading(jsonFuelObject.getString("odoMeterReading"));
//                                }
//
//                                if (jsonFuelObject.has("address")) {
//                                    if (jsonFuelObject.getString("address").equals("P")) {
//                                        f.setAddress(mPrevAdds);
//                                    } else {
//                                        mPrevAdds = jsonFuelObject.getString("address");
//                                        f.setAddress(jsonFuelObject.getString("address"));
//                                    }
//                                }
//
//                                mFuelFillData.add(f);
//
//                            }
//                        } catch (JSONException e) {
//
//                        } finally {
//
//                        }
//
//
//                    } else {
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }


                setTableLayoutData();

            } else {
                Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(FuelFillNewReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private void setBottomLayoutData() {
        mTxtFromDate.setText(mStartDate);
        mTxtEndDate.setText(mEndDate);
        mTxtFromTime.setText(mStartTimeValue);
        mTxtEndTime.setText(mEndTimeValue);
        setIntervalSpinnerValue();
        mIntervalList = intervalSpinnerDataValue();

        Constant.mFuelStoredFromDate = null;
        Constant.mFuelStoredToDate = null;
        Constant.mFuelStoredFromTime = null;
        Constant.mFuelStoredToTime = null;
        Constant.mFuelInterval = null;
    }

    private void showTimeDialog() {
        if (isFromTime) {
            hour = mFromHourValue;
            minute = mFromMinuteValue;
//            System.out.println("Hi clicked hour minute value " + mFromHourValue + " " + mFromMinuteValue + " :: " + hour + " " + minute);
        } else {
            hour = mToHourValue;
            minute = mToMinuteValue;
//            System.out.println("Hi clicked hour minute value " + mToHourValue + " " + mToMinuteValue + " :: " + hour + " " + minute);
        }


//                hour=20;
//                minute=33;

        TimePickerDialog dialog2 = new TimePickerDialog(this, timePickerListener, hour, minute,
                false);
        dialog2.show();
    }

    private void showDateDialog() {
        if (isFromDate) {

            String[] dateArray = mStartDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        } else {

            String[] dateArray = mEndDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());

        }
//        System.out.println("Hi year " + year + " mo " + month + " da " + day);
//        final Calendar c = Calendar.getInstance();
//        year = c.get(Calendar.YEAR);
//        month = c.get(Calendar.MONTH);
//        day = c.get(Calendar.DAY_OF_MONTH);
        /** set date picker as current date */
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
        dialog.show();
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            if (isFromDate) {
                mStartDate = String.valueOf(mDate2);
                //  mStartTimeValue=String.valueOf(mDate2);
                mTxtFromDate.setText(mDate2);
            } else {
                mEndDate = String.valueOf(mDate2);

                mTxtEndDate.setText(mDate2);
            }
//            mFromDate = String.valueOf(mDate);
//            mFromDateTxtValue = String.valueOf(mDate2);
//            mTxtDate.setText(mDate2);
            // }
        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);


        }

    };


    private void updateTime(int hours, int mins) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, mins);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
//        System.out.println("Hi date is 222:::" + timeFormat.format(cal.getTime()));

//        String aTime = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
        String aTime = timeFormatShow.format(cal.getTime());
//        String aTime2 = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(":00").append(minutes).append(" ").append(timeSet2).toString();

        String aTime2 = timeFormat.format(cal.getTime());
        String aTime3 = timeFormatFuelFillReport.format(cal.getTime());

        SimpleDateFormat hourFormat = new SimpleDateFormat(
                "HH");
        SimpleDateFormat minuteFormat = new SimpleDateFormat(
                "mm");

        if (isFromTime) {
            mStartTime = aTime2;
            mStartTimeValue = aTime;

            mStartTimeFuelReport = aTime3;


            mFromHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
            mFromMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));
            mTxtFromTime.setText(mStartTimeValue);
//            System.out.println("Hi from time value " + mFromHourValue + " " + mFromMinuteValue);
        } else {
            mEndTime = aTime2;
            mEndTimeValue = aTime;
            mEndTimeFuelReport = aTime3;
//            mToHourValue = cal.HOUR_OF_DAY;
//            mToMinuteValue = cal.MINUTE;
            mTxtEndTime.setText(mEndTimeValue);

//            SimpleDateFormat hourFormat = new SimpleDateFormat(
//                    "hh");
//            SimpleDateFormat minuteFormat = new SimpleDateFormat(
//                    "mm");

            mToHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
            mToMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));

//            System.out.println("Hi to time value " + mToHourValue + " " + mToMinuteValue);
        }


    }


    public void  setTableLayoutData() {

//        adapter.setHeader(mHeader);
//        ListView listView = (ListView) findViewById(R.id.executive_report_listView1);
        Log.d("mFuelFillDatasize",""+mFuelFillData.size());
//     if (mFuelFillData.size() == 0 || mFuelFillData.size() == 1){
//         Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
//         if (mIsLineChartEnabled) {
//             // mChart.setVisibility(View.VISIBLE);
//             mChartLayout.setVisibility(View.VISIBLE);
//             lv.setVisibility(View.GONE);
//             mChart.clear();
//             mChart.setData(null);
//             mChart.notifyDataSetChanged();
//         } else {
////                mChart.setVisibility(View.GONE);
//             mChartLayout.setVisibility(View.GONE);
//             lv.setVisibility(View.VISIBLE);
//             lv.setAdapter(null);
//         }
//     } else {
//         if (isHeaderPresent) {
//
//         } else {
//             lv.addHeaderView(adapter.getHeaderView(lv));
//             isHeaderPresent = true;
//         }
//
//
//         lv.setAdapter(adapter);
//         adapter.setData(mFuelFillData);
//
//         adapter.notifyDataSetChanged();
//
//
//         if (mIsLineChartEnabled) {
////                mChart.setVisibility(View.VISIBLE);
//             mChartLayout.setVisibility(View.VISIBLE);
//             lv.setVisibility(View.GONE);
//
//
////                    setupLineChart();
//             //  new getfuelmachinery().execute();
//
//             formLineChartNew();
//         }
//     }

        if (mFuelFillData !=null) {
            if (mFuelFillData.size() == 0){

            }else {

                if (mFuelFillData.size() == 0){

                }
                else if (mFuelFillData.size() > 0) {

                    if (isHeaderPresent) {

                    } else {
                        lv.addHeaderView(adapter.getHeaderView(lv));
                        isHeaderPresent = true;
                    }
                    mMainScrollView.setVisibility(View.VISIBLE);
                    lv.setAdapter(adapter);
                    adapter.setData(mFuelFillData);
                    adapter.notifyDataSetChanged();


                    if (mIsLineChartEnabled) {
//                mChart.setVisibility(View.VISIBLE);
                        mChartLayout.setVisibility(View.VISIBLE);
                        lv.setVisibility(View.GONE);
                        mMainScrollView.setVisibility(View.GONE);


//                    setupLineChart();
                        //  new getfuelmachinery().execute();

                        formLineChartNew();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                    if (mIsLineChartEnabled) {
                        // mChart.setVisibility(View.VISIBLE);
                        mChartLayout.setVisibility(View.GONE);
                        mChart.setVisibility(View.GONE);
                        mMainScrollView.setVisibility(View.GONE);
                        lv.setVisibility(View.GONE);
                        mChart.clear();
                        mChart.setData(null);
                        mChart.notifyDataSetChanged();
                    } else {
//                mChart.setVisibility(View.GONE);
                        mChartLayout.setVisibility(View.GONE);
                        lv.setVisibility(View.VISIBLE);
                        mMainScrollView.setVisibility(View.VISIBLE);
                        lv.setAdapter(null);
                    }
                }
            }
        }


//        if (mTotalFuelValue != null) {
//            mTxtCumulativeFuelValue.setText(mTotalFuelValue + " ltrs");
//        } else {
//            mTxtCumulativeFuelValue.setText("0 ltr");
//        }
//        parseVehicleKms();

    }

    public class TableAdapter extends BaseAdapter {

        // private static final String TAG = "TableAdapter";

        private Context mContext;

        private String[] mHeader;


        private int mCurrentScroll;

        private int[] mColResources = {R.id.fuelfill_date_textView, R.id.fuelfill_fuel_ltr_textView,
                R.id.fuelfill_adds_textView, R.id.fuelfill_speed_textView,
                R.id.fuelfill_odo_distance_textView};

        public TableAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return mFuelFillData != null ? mFuelFillData.size() : 0;

        }

        @Override
        public Object getItem(int position) {
            return mFuelFillData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            Log.d("Count",""+getCount());

            HorizontalScrollView view = null;
//            LinearLayout view = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                view = (HorizontalScrollView) inflater.inflate(
                        R.layout.fuelfill_list_item_table_row_new, parent, false);

//                view = (LinearLayout) inflater.inflate(
//                        R.layout.fuelfill_list_item_table_row_new, parent, false);


//                view.setOnScrollChangeListener(new OnScrollListener() {
//                    @Override
//                    public void onScrollChanged(View view, int scrollX) {
//
//                    }
//                });


//                view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {
//
//                    @Override
//                    public void onScrollChanged(View scrollView, int scrollX) {
//
//                        mCurrentScroll = scrollX;
//                        ListView listView = (ListView) scrollView.getParent();
//                        if (listView == null)
//                            return;
//
//                        for (int i = 0; i < listView.getChildCount(); i++) {
//                            View child = listView.getChildAt(i);
//                            if (child instanceof HorizontalScrollView
//                                    && child != scrollView) {
//                                HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
//                                if (scrollView2.getScrollX() != mCurrentScroll) {
//                                    scrollView2.setScrollX(mCurrentScroll);
//                                }
//                            }
//                        }
//                    }
//                });

            } else {
                view = (HorizontalScrollView) convertView;
//                view = (LinearLayout) convertView;
            }

            view.setScrollX(mCurrentScroll);

            if (position % 2 == 0) {
                view.setBackgroundColor(Color.WHITE);
            } else {
                view.setBackgroundColor(Color.LTGRAY);
            }

            final FuelFillDto data = mFuelFillData.get(position);

//            for (int i = 0; i < mColResources.length; i++) {
            TextView col1 = (TextView) view.findViewById(mColResources[0]);
            col1.setText(cons.getAcReportTime(data.getStartTime()));
            TextView col2 = (TextView) view.findViewById(mColResources[1]);
            col2.setText(String.valueOf(data.getFuelLtr()));
            TextView col3 = (TextView) view.findViewById(mColResources[2]);
            SpannableString content = new SpannableString("Link");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            col3.setText(content);
            // col3.setText(String.valueOf(data.getAddress()));
            col3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFuelDataMapLayout.setVisibility(View.VISIBLE);
                    mMainScrollView.setVisibility(View.GONE);
                    lv.setVisibility(View.GONE);
                    String addres = data.getAddress();

                    if (addres.equalsIgnoreCase("_")){
                        Log.d("addres",""+addres);
                    } else{
                        String[] value1 = addres.split(",");
                        mLatitude = Double.valueOf(value1[0]);
                        mLongitude = Double.valueOf(value1[1]);
                        Log.d("latlng",""+mLatitude);
                        Log.d("latlng1",""+mLongitude);
                    }
                    setupGoogleMap();


//                    mLatitude = Double.valueOf(mFuelFillData.get(position).getLatitude());
//                    mLongitude = Double.valueOf(mFuelFillData.get(position).getLongitude());
                }
            });

            TextView col4 = (TextView) view.findViewById(mColResources[3]);
            col4.setText(String.valueOf(data.getNewSpeed()));

            TextView col5 = (TextView) view.findViewById(mColResources[4]);
            col5.setText(String.valueOf(data.getOdoMeterReading()));


            LinearLayout.LayoutParams col1Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col1Params.width = width * 35 / 100;
//            col1Params.height = height * 7 / 100;
            col1.setLayoutParams(col1Params);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            LinearLayout.LayoutParams col245Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col245Params.width = width * 30 / 100;
//            col245Params.height = height * 7 / 100;
            col2.setLayoutParams(col245Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);

            col4.setLayoutParams(col245Params);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(col245Params);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams col3Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col3Params.width = width * 40 / 100;
//            col3Params.height = height * 7 / 100;
            col3.setLayoutParams(col3Params);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);


            return view;
        }

        public View getHeaderView(ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            HorizontalScrollView view = (HorizontalScrollView) inflater
                    .inflate(R.layout.fuelfill_list_item_table_header_new, parent, false);

//            LinearLayout view = (LinearLayout) inflater
//                    .inflate(R.layout.fuelfill_list_item_table_header_new, parent, false);

//            view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {
//
//                @Override
//                public void onScrollChanged(View scrollView, int scrollX) {
//
//                    mCurrentScroll = scrollX;
//                    ListView listView = (ListView) scrollView.getParent();
//                    if (listView == null)
//                        return;
//
//                    for (int i = 0; i < listView.getChildCount(); i++) {
//                        View child = listView.getChildAt(i);
//                        if (child instanceof HorizontalScrollView
//                                && child != scrollView) {
//                            HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
//                            if (scrollView2.getScrollX() != mCurrentScroll) {
//                                scrollView2.setScrollX(mCurrentScroll);
//                            }
//                        }
//                    }
//                }
//            });

            TextView col1 = (TextView) view.findViewById(R.id.fuelsumm__textView_header);
            TextView col2 = (TextView) view.findViewById(R.id.fuelfill_fuel_ltr_textView_header);
            TextView col3 = (TextView) view.findViewById(R.id.fuelfill_adds_textView_header);
            TextView col4 = (TextView) view.findViewById(R.id.fuelfill_odo_distance_textView_header);
            TextView col5 = (TextView) view.findViewById(R.id.fuelfill_speed_textView_header);
//            SpannableString content = new SpannableString("Link");
//            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//            col3.setText(content);


            LinearLayout.LayoutParams col1Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col1Params.width = width * 35 / 100;
//            col1.setLayoutParams(col1Params);
//            col1.setPadding(width * 2 / 100, 0, 0, 0);
//            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            LinearLayout.LayoutParams col245Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col245Params.width = width * 30 / 100;
//            col245Params.height = height * 7 / 100;
            col2.setLayoutParams(col245Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);

            col4.setLayoutParams(col245Params);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(col245Params);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams col3Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col3Params.width = width * 40 / 100;
//            col3Params.height = height * 7 / 100;
            col3.setLayoutParams(col3Params);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);

            return view;
        }

//        public void setHeader(String[] header) {
//            mHeader = header;
//        }

        public void setData(List<FuelFillDto> data) {
            mFuelFillData = data;
            notifyDataSetChanged();
        }

    }

//    public void setListData() {
//        mImgDataViewChange.setVisibility(View.GONE);
//        mIsMapPresent = false;
//        lv.setVisibility(View.VISIBLE);
//        mFuelDataMapLayout.setVisibility(View.GONE);
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

//
//        if (mIsMapPresent) {
//            System.out.println("The map is present ::::::" + mIsMapPresent);
//            setListData();
//
//           // new getKmsSummaryData().execute();
//        } else {
        startActivity(new Intent(FuelFillNewReport.this, MapMenuActivity.class));
        finish();
//        }
    }


    @Override
    public void onChartGestureStart(MotionEvent me,
                                    ChartTouchListener.ChartGesture
                                            lastPerformedGesture) {

//        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me,
                                  ChartTouchListener.ChartGesture
                                          lastPerformedGesture) {

//        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            // or highlightTouch(null) for callback to onNothingSelected(...)
            mChart.highlightValues(null);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
//        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
//        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
//        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2,
                             float velocityX, float velocityY) {
//        Log.i("Fling", "Chart flinged. VeloX: "
//                + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
//        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
//        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

//        Log.i("Entry selected", e.toString());
//        Log.i("Entry data ", e.getData() + " " + e.getVal() + " " + e.getXIndex());
//        Log.i("LOWHIGH", "low: " + mChart.getLowestVisibleXIndex()
//                + ", high: " + mChart.getHighestVisibleXIndex());
//
//        Log.i("MIN MAX", "xmin: " + mChart.getXChartMin()
//                + ", xmax: " + mChart.getXChartMax()
//                + ", ymin: " + mChart.getYChartMin()
//                + ", ymax: " + mChart.getYChartMax());

//        String mToastMsg = xAxis.get(e.getXIndex()) + "\n" + e.getVal() + " ltrs";
//        final Toast toast = Toast.makeText(getApplicationContext(), mToastMsg, Toast.LENGTH_SHORT);
//        toast.show();
//
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                toast.cancel();
//            }
//        }, 1000);

    }


    @Override
    public void onNothingSelected() {
//        Log.i("Nothing selected", "Nothing selected.");
    }


    public void setupGoogleMap() {

        if (map != null) {
            map.clear();
        }

        View marker = null;

        marker = ((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
        id_vehicle_in_marker.setVisibility(View.GONE);

        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));


//        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//            // Use default InfoWindow frame
//            @Override
//            public View getInfoWindow(Marker arg0) {
//                return null;
//            }

        // Defines the contents of the InfoWindow
        //  @Override
//            public View getInfoContents(Marker arg0) {
//
//                // Getting view from the layout file info_window_layout
//              //  View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);
//
//                // Getting the position from the marker
//                LatLng latLng = arg0.getPosition();
//
////                DisplayMetrics displayMetrics = new DisplayMetrics();
////                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
////                height = displayMetrics.heightPixels;
////                width = displayMetrics.widthPixels;
////                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
////                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
////                        LinearLayout.LayoutParams.WRAP_CONTENT,
////                        LinearLayout.LayoutParams.WRAP_CONTENT);
////                backImageParams.width = width * 80 / 100;
////                //backImageParams.height = height * 10 / 100;
////                backImageParams.gravity = Gravity.CENTER;
////                layout.setLayoutParams(backImageParams);
//
////                // Getting reference to the TextView to set latitude
////                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
////                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//
////                new GetAddressTask(txtContent).execute(mLatitude, mLongitude, 1.0);
//
//
//                StringBuffer addr = new StringBuffer();
//                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//                List<Address> addresses = null;
//                try {
//                    addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                } catch (IllegalArgumentException e2) {
//                    // Error message to post in the log
////                        String errorString = "Illegal arguments " +
////                                Double.toString(params[0]) +
////                                " , " +
////                                Double.toString(params[1]) +
////                                " passed to address service";
////                        e2.printStackTrace();
////                        return errorString;
//                } catch (NullPointerException np) {
//                    // TODO Auto-generated catch block
//                    np.printStackTrace();
//                }
//                // If the reverse geocode returned an address
//                if (addresses != null && addresses.size() > 0) {
//                    // Get the first address
//                    Address address = addresses.get(0);
//                    /*
//                     * Format the first line of address (if available),
//                     * city, and country name.
//                     */
//                    String addressText = null;
//
//                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                        addressText = address.getAddressLine(i);
//                        addr.append(addressText + ",");
//                    }
//                    // Return the text
//                    // return addr.toString();
//                } else {
//                    addr.append("No address found");
//                }
//
//                //txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " \n" + addressValue);
//               // txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
//
//               // return v;
//
//            }
        //});


        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        Marker currentMarker = map.addMarker(markerOption);
        currentMarker.showInfoWindow();



    }
    private class getfuelmachinery extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                //  Const constaccess = new Const();

                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");

//                String now = new SimpleDateFormat("yyyy-MM-dd hh:mm aa").format(mEndDate.trim() + " " + mEndTime.trim());
//
//                SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
//                SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa");
//                String time24 = outFormat.format(inFormat.parse(now));
                Date d = f.parse(string_from_date);

//                System.out.println("The from date after converted  :::::" + d);

                long fromMilliSeconds = d.getTime();
//                System.out.println("The from time in millis :::::" + fromMilliSeconds);
                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
//                long fromMilliSeconds =1470767400000;
//                long toMilliSeconds=1471685033000;


                //  System.out.println("The to time in millis :::::" + toMilliSeconds);


//                result = constaccess
//                        .sendGet(Const.API_URL + "/mobile/getExecutiveReport?groupId=" + mSelectedGroup + "&fromDate=" + mStrFromDate + "&toDate=" + mStrToDate + "&userId=" + mUserId, 30000);
                HttpConfig ht = new HttpConfig();
//                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDropFillReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=1&fromDate=" + mStrFromDate + "&fromTime=" + mStrFromTime + "&toDate=" + mStrToDate + "&toTime=" + mStrToTime + "&fuelDrop=true&fuelFill=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
//                System.out.println("Hi 333 orientation changes " + mStartDate + mEndDate + mStartTime);

//                http://www.gpsvts.net/mobile/getVehicleFuelHistory4Mobile?vehicleId=AKSHYA-TN29BZ1220&interval=1&fromDate=2017-11-22&fromTime=00:00:00&toDate=2017-11-29&toTime=12:07:32&fuelDrop=true&fuelFill=true&fromDateUTC=1511289000000&toDateUTC=1511937452000&userId=AKSHAYATRANS

//                result = ht.httpGet(Const.API_URL + "/mobile/getVehicleFuelHistory4Mobile?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=" + mIntervalSpinnerSelectedValue + "&fuelDrop=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDetailForMachinery?vehicleId=" + Constant.SELECTED_VEHICLE_ID  + "&fromDateTime=" + fromMilliSeconds + "&toDateTime=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mFuelFillData = new ArrayList<>();

            if (result != null && result.length() > 0) {
                String mPrevAdds = null;
                try {
                    JSONObject jsonObject = new JSONObject(result.trim());

                    if (jsonObject.getString("tFuelCon") !=null){
                        mTxtTotalFuelConsume.setText(jsonObject.getString("tFuelCon") + " ltrs");
                        //  mTotalFuelConsume = jsonObject.getDouble("totalFuelConsume");

                    }
                    if (jsonObject.getString("tdistance") !=null){
                        mTxtTripDistance.setText(jsonObject.getString("tdistance") + " kms");
//                    mTripDistance= jsonObject.getDouble("tdistance");
                    }


                    if (jsonObject.getString("sFuelLevel") !=null){
                        mTxtstartfuel.setText(jsonObject.getString("sFuelLevel") + " ltrs");
                    }

                    if (jsonObject.getString("eFuelLevel") !=null){
                        mTxtendfuel.setText(jsonObject.getString("eFuelLevel") + " ltrs");
                    }

                    mTotalFuelFill = jsonObject.getInt("totalFuelFills");

                    if (mTotalFuelFill > 0) {

                    } else {
                        mTotalFuelFill = 0;
                    }

                    if (mTotalFuelFill > 0) {
                        String mText = "<u>" + mTotalFuelFill + "</u>";
                        mTxtTotalFuelFill.setText(Html.fromHtml(mText) + " ltrs");
                        mTxtTotalFuelFill.setTextColor(Color.BLUE);
                    } else {
                        mTxtTotalFuelFill.setText("" + mTotalFuelFill + " ltrs");
                        mTxtTotalFuelFill.setTextColor(Color.BLACK);
                    }




                    Log.d("totalFuelFill",""+jsonObject.getString("totalFuelFills"));
                    Log.d("tdistance",""+jsonObject.getString("tdistance"));
                    Log.d("tFuelCo n",""+jsonObject.getString("tFuelCon"));


                }catch (JSONException e) {
                    e.printStackTrace();
                }

//                try {
//                    JSONObject jsonObject = new JSONObject(result.trim());
//
//                    if (jsonObject.has("totalFuelConsume")) {
//                        mTotalFuelConsume = jsonObject.getDouble("totalFuelConsume");
//                    }
//                    if (jsonObject.has("tripDistance")) {
//                        mTripDistance = jsonObject.getDouble("tripDistance");
//                    }
//
//                    if (jsonObject.has("totalFuelFill")) {
//                        mTotalFuelFill = jsonObject.getInt("totalFuelFill");
//                    }
//
//                    if (jsonObject.has("openingOdoReading")) {
//                        mOdoStart = jsonObject.getDouble("openingOdoReading");
//                    }
//                    if (jsonObject.has("closingOdoReading")) {
//                        mOdoEnd = jsonObject.getDouble("closingOdoReading");
//                    }
//
//                    if (jsonObject.has("milege")) {
//                        mTotalMileage = jsonObject.getInt("milege");
//                    }
//
//
//                    if (jsonObject.has("history4Mobile")) {
//
////                        if (jsonObject.get("history4Mobile") != null && !jsonObject.get("history4Mobile").equals("null")) {
//                        try {
//                            JSONArray jArrayFuel = jsonObject.getJSONArray("history4Mobile");
//
//                            String mPrevAdds = null;
//                            for (int i = 0; i < jArrayFuel.length(); i++) {
//                                JSONObject jsonFuelObject = jArrayFuel.getJSONObject(i);
//                                FuelFillDto f = new FuelFillDto();
//
//                                if (jsonFuelObject.has("dt")) {
//                                    f.setStartTime(Long.valueOf(jsonFuelObject.getString("dt")));
//                                }
//                                if (jsonFuelObject.has("fuelLitr")) {
//                                    f.setFuelLtr(jsonFuelObject.getString("fuelLitr"));
//                                }
//
//
//                                if (jsonFuelObject.has("newSpeed")) {
//                                    f.setNewSpeed(jsonFuelObject.getString("newSpeed"));
//                                }
//
//                                if (jsonFuelObject.has("ignitionStatus")) {
//                                    f.setIgnitionStatus(jsonFuelObject.getString("ignitionStatus"));
//                                }
//
//                                if (jsonFuelObject.has("odoMeterReading")) {
//                                    f.setOdoMeterReading(jsonFuelObject.getString("odoMeterReading"));
//                                }
//
//                                if (jsonFuelObject.has("address")) {
//                                    if (jsonFuelObject.getString("address").equals("P")) {
//                                        f.setAddress(mPrevAdds);
//                                    } else {
//                                        mPrevAdds = jsonFuelObject.getString("address");
//                                        f.setAddress(jsonFuelObject.getString("address"));
//                                    }
//                                }
//
//                                mFuelFillData.add(f);
//
//                            }
//                        } catch (JSONException e) {
//
//                        } finally {
//
//                        }
//
//
//                    } else {
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }


                // setTableLayoutData();
            } else {
                Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
            }

        }
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(FuelFillNewReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    public static class DatePickerDialogTheme extends DialogFragment implements DatePickerDialog.OnDateSetListener{

        private boolean isFromDate;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

//for one

//for two

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_DEVICE_DEFAULT_DARK,this,year,month,day);


            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day){

            year = year;
            month = month;
            day = day;

            TextView mTxtFromDate = (TextView) getActivity().findViewById(R.id.txt_start_date_value);
            //  mTxtFromDate.setText(day + ":" + (month+1) + ":" + year);

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append((month + 1)).append("-").append((day))
                    .append(" ");
            mTxtFromDate.setText(mDate2);
            mStartDate = String.valueOf(mDate2);
//            if (isFromDate) {
//                mStartDate = String.valueOf(mDate2);
//                //  mStartTimeValue=String.valueOf(mDate2);
//                mTxtFromDate.setText(mDate2);
//            } else {
//                mEndDate = String.valueOf(mDate2);
//
//                mTxtEndDate.setText(mDate2);
//            }


        }
    }

    public static class DatePickerDialogTheme1 extends DialogFragment implements DatePickerDialog.OnDateSetListener{

        private boolean isFromDate;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

//for one

//for two

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_DEVICE_DEFAULT_DARK,this,year,month,day);


            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day){

            year = year;
            month = month;
            day = day;

            TextView mTxtEndDate = (TextView) getActivity().findViewById(R.id.txt_end_date_value);
            //  mTxtFromDate.setText(day + ":" + (month+1) + ":" + year);

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append((month + 1)).append("-").append((day))
                    .append(" ");
            mTxtEndDate.setText(mDate2);
            mEndDate = String.valueOf(mDate2);
//            if (isFromDate) {
//                mStartDate = String.valueOf(mDate2);
//                //  mStartTimeValue=String.valueOf(mDate2);
//                mTxtFromDate.setText(mDate2);
//            } else {
//                mEndDate = String.valueOf(mDate2);
//
//                mTxtEndDate.setText(mDate2);
//            }


        }
    }



}
