package com.vamosys.vamos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.vamosys.utils.Constant.EndTimeFuelReport;
import static com.vamosys.utils.Constant.FuelStoredFromDate;
import static com.vamosys.utils.Constant.FuelStoredFromTime;
import static com.vamosys.utils.Constant.FuelStoredToDate;
import static com.vamosys.utils.Constant.FuelStoredToTime;
import static com.vamosys.utils.Constant.StartTimeFuelReport;

public class FuelFillReportSensorActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar mToolbar;
    AppCompatTextView mTxtVehicleName;
    FrameLayout frameLayout;
    TabLayout tabLayout;
    Boolean chart=false;
    String position="0";
    private int year, month, day;
    static final int TIME_DIALOG_ID = 1111;
    static final int DATE_DIALOG_ID = 2222;
    private int hour;
    private int minute;
    Spinner spinnerInterval;
    Button mBtnSubmit;
    static TextView mTxtFromDate;
    static TextView mTxtEndDate;
    TextView mTxtstart;
    TextView mTxtend;
    TextView mTxtFromTime;
    TextView mTxtEndTime;
    static String mStartDate;
    static String mEndDate;
    String mStartTime;
    String mStartTimeValue;
    String mStartTimeFuelReport;
    String mEndTimeFuelReport;
    String mEndDateValue;
    String mEndTime;
    String mEndTimeValue;
    String mIntervalSpinnerSelectedValue;
    public static boolean isFromTime = false, isFromDate = false, isMapPresent = false;
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    ArrayList<String> mIntervalList;
    ArrayList<String> mSpinnerList;
    int mIntervalSpinnerSelectedPos = 0;
    ConnectionDetector cd;
    SharedPreferences sp;
    static Context mFuelContext;
    String intervalue,spinnervalue;

    SimpleDateFormat timeFormatFuelFillReport = new SimpleDateFormat(
            "HH:mm:ss");

    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

    androidx.appcompat.app.AlertDialog dialogs;
    long from,to;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_fill_report_sensor);
        try {
            mToolbar = findViewById(R.id.toolbar);
            mToolbar.setTitle(getResources().getString(R.string.fuel_report));
            mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
            setSupportActionBar(mToolbar);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(FuelFillReportSensorActivity.this, MapMenuActivity.class));
                    finish();
                }
            });
            init();
            sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            cd = new ConnectionDetector(getApplicationContext());
            mFuelContext = getApplicationContext();
            if (sp.getString("ip_adds", "") != null) {
                if (sp.getString("ip_adds", "").trim().length() > 0) {
                    Const.API_URL = sp.getString("ip_adds", "");
                }
            }
            intervalue = sp.getString("intervalvalue", "");
            spinnervalue = sp.getString("spinnervalue", "");

            mIntervalSpinnerSelectedValue = intervalue;
            if (FuelStoredFromDate!=null&& FuelStoredFromDate.length() > 1 && FuelStoredToDate!=null&& FuelStoredToDate.length() > 1 && FuelStoredFromTime!=null&& FuelStoredFromTime.length() > 1 && FuelStoredToTime!=null&& FuelStoredToTime.length() > 1) {
               Log.d("res","fuel_date_stored");
                Log.d("res","fuel_date_stored "+FuelStoredFromDate);
                Log.d("res","fuel_date_stored "+FuelStoredToDate);
                Log.d("res","fuel_date_stored "+FuelStoredFromTime);
                Log.d("res","fuel_date_stored "+FuelStoredToTime);
                Log.d("res","fuel_date_stored "+StartTimeFuelReport);
                Log.d("res","fuel_date_stored "+EndTimeFuelReport);

                mStartDate= FuelStoredFromDate;
                mEndDate= FuelStoredToDate;
                mStartTime= FuelStoredFromTime;
                mEndTime= FuelStoredToTime;
                mStartTimeFuelReport= StartTimeFuelReport;
                mEndTimeFuelReport= EndTimeFuelReport;
            }
            else{
                Log.d("res","fuel_date_inital");
                mStartDate = Const.getCurrentDate();
                mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));

                long timeInMillis = System.currentTimeMillis();
                Calendar cal1 = Calendar.getInstance();
                cal1.setTimeInMillis(timeInMillis);
                Calendar cal = Calendar.getInstance();

                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);


                mFromHourValue = 00;
                mFromMinuteValue = 00;

                mStartTimeValue = timeFormatShow.format(cal.getTime());
//        mStartTime = "12:00:00 am";
                mStartTime = timeFormat.format(cal.getTime());
                mStartTimeFuelReport = timeFormatFuelFillReport.format(cal.getTime());

                final Calendar c = Calendar.getInstance();
                // Current Hour
                hour = c.get(Calendar.HOUR_OF_DAY);
                // Current Minute
                minute = c.get(Calendar.MINUTE);
                isFromTime = false;
                updateTime(hour, minute);
                mIntervalList = intervalSpinnerDataValue();
                mSpinnerList = intervalSpinnerValue();
            }
            setBottomLayoutData();
            from=convertDataTolong(mStartDate.trim(),mStartTime.trim());
            to=convertDataTolong(mEndDate.trim(),mEndTime.trim());
            loadFragment(""+0,false);
        }catch(Exception e){
            Log.d("try","error1 "+e.getMessage());
        }
    }

    private long convertDataTolong(String trim, String trim1) {
        long fromMilliSeconds=0;
        try {
            String string_from_date = trim + " " + trim1;
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
            try {
                Date d = f.parse(string_from_date);
                fromMilliSeconds = d.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }catch(Exception e){
            Log.d("try","error2 "+e.getMessage());
        }

        return fromMilliSeconds;
    }

    private ArrayList<String> intervalSpinnerValue() {
        ArrayList<String> mHrList = new ArrayList<String>();

        try {
            mHrList.add("Interval");
            mHrList.add("1 mins");
            mHrList.add("2 mins");
            mHrList.add("5 mins");
            mHrList.add("10 mins");
            mHrList.add("15 mins");
            mHrList.add("30 mins");
        }catch(Exception e){
            Log.d("try","error3 "+e.getMessage());
        }

        return mHrList;
    }

    private ArrayList<String> intervalSpinnerDataValue() {
        ArrayList<String> mHrList = new ArrayList<String>();
        try {

            mHrList.add("0");
            mHrList.add("1");
            mHrList.add("2");
            mHrList.add("5");
            mHrList.add("10");
            mHrList.add("15");
            mHrList.add("30");

        }catch(Exception e){
            Log.d("try","error4 "+e.getMessage());
        }
        return mHrList;
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the state of item position
        try {
            Constant.mFuelStoredFromDate = mStartDate;
            Constant.mFuelStoredToDate = mEndDate;
            Constant.mFuelStoredFromTime = mStartTime;
            Constant.mFuelStoredToTime = mEndTime;
            Constant.mFuelInterval = mIntervalSpinnerSelectedValue;
            Constant.mFuelIsChartView = chart;
            FuelStoredFromDate=mStartDate;
            FuelStoredToDate=mEndDate;
            FuelStoredFromTime=mStartTime;
            FuelStoredToTime=mEndTime;
            StartTimeFuelReport=mStartTimeFuelReport;
            EndTimeFuelReport=mEndTimeFuelReport;
        }catch(Exception e){
            Log.d("try","error5 "+e.getMessage());
        }

    }

    private void updateTime(int hours, int mins) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, hours);
            cal.set(Calendar.MINUTE, mins);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);

            String aTime = timeFormatShow.format(cal.getTime());

            String aTime2 = timeFormat.format(cal.getTime());
            String aTime3 = timeFormatFuelFillReport.format(cal.getTime());

            SimpleDateFormat hourFormat = new SimpleDateFormat(
                    "HH");
            SimpleDateFormat minuteFormat = new SimpleDateFormat(
                    "mm");

            if (isFromTime) {
                mStartTime = aTime2;
                mStartTimeValue = aTime;

                mStartTimeFuelReport = aTime3;


                mFromHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
                mFromMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));
                mTxtFromTime.setText(mStartTimeValue);
            } else {
                mEndTime = aTime2;
                mEndTimeValue = aTime;
                mEndTimeFuelReport = aTime3;

                mTxtEndTime.setText(mEndTimeValue);

                mToHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
                mToMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));

            }
        }catch(Exception e){
            Log.d("try","error6 "+e.getMessage());
        }



    }
    private void init() {
        try {
            mTxtVehicleName = findViewById(R.id.selected_vehicle_txt);
            mTxtVehicleName.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
            frameLayout=findViewById(R.id.frameLayout);
            tabLayout = findViewById(R.id.tablayout);
            TabLayout.Tab sensor1 = tabLayout.newTab();
            sensor1.setText("Sensor 1");
            tabLayout.addTab(sensor1);

            TabLayout.Tab sensor2 = tabLayout.newTab();
            sensor2.setText("Sensor 2");
            tabLayout.addTab(sensor2);

            tabLayout.getTabAt(0).select();

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    if (FuelStoredFromDate!=null&& FuelStoredFromDate.length() > 1 && FuelStoredToDate!=null&& FuelStoredToDate.length() > 1 && FuelStoredFromTime!=null&& FuelStoredFromTime.length() > 1 && FuelStoredToTime!=null&& FuelStoredToTime.length() > 1) {
                        mStartDate= FuelStoredFromDate;
                        mEndDate= FuelStoredToDate;
                        mStartTime= FuelStoredFromTime;
                        mEndTime= FuelStoredToTime;
                        mStartTimeFuelReport= StartTimeFuelReport;
                        mEndTimeFuelReport= EndTimeFuelReport;
                    }
                    else{
                        mStartDate = Const.getCurrentDate();
                        mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));

                        long timeInMillis = System.currentTimeMillis();
                        Calendar cal1 = Calendar.getInstance();
                        cal1.setTimeInMillis(timeInMillis);
                        Calendar cal = Calendar.getInstance();

                        cal.set(Calendar.HOUR_OF_DAY, 0);
                        cal.set(Calendar.MINUTE, 0);
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);


                        mFromHourValue = 00;
                        mFromMinuteValue = 00;

                        mStartTimeValue = timeFormatShow.format(cal.getTime());
//        mStartTime = "12:00:00 am";
                        mStartTime = timeFormat.format(cal.getTime());
                        mStartTimeFuelReport = timeFormatFuelFillReport.format(cal.getTime());

                        final Calendar c = Calendar.getInstance();
                        // Current Hour
                        hour = c.get(Calendar.HOUR_OF_DAY);
                        // Current Minute
                        minute = c.get(Calendar.MINUTE);
                        isFromTime = false;
                        updateTime(hour, minute);
                        mIntervalList = intervalSpinnerDataValue();
                        mSpinnerList = intervalSpinnerValue();
                    }
                    position=""+tab.getPosition();
                    loadFragment(position,chart);
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    if (FuelStoredFromDate!=null&& FuelStoredFromDate.length() > 1 && FuelStoredToDate!=null&& FuelStoredToDate.length() > 1 && FuelStoredFromTime!=null&& FuelStoredFromTime.length() > 1 && FuelStoredToTime!=null&& FuelStoredToTime.length() > 1) {
                        mStartDate= FuelStoredFromDate;
                        mEndDate= FuelStoredToDate;
                        mStartTime= FuelStoredFromTime;
                        mEndTime= FuelStoredToTime;
                        mStartTimeFuelReport= StartTimeFuelReport;
                        mEndTimeFuelReport= EndTimeFuelReport;
                    }
                    else{
                        mStartDate = Const.getCurrentDate();
                        mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));

                        long timeInMillis = System.currentTimeMillis();
                        Calendar cal1 = Calendar.getInstance();
                        cal1.setTimeInMillis(timeInMillis);
                        Calendar cal = Calendar.getInstance();

                        cal.set(Calendar.HOUR_OF_DAY, 0);
                        cal.set(Calendar.MINUTE, 0);
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);


                        mFromHourValue = 00;
                        mFromMinuteValue = 00;

                        mStartTimeValue = timeFormatShow.format(cal.getTime());
//        mStartTime = "12:00:00 am";
                        mStartTime = timeFormat.format(cal.getTime());
                        mStartTimeFuelReport = timeFormatFuelFillReport.format(cal.getTime());

                        final Calendar c = Calendar.getInstance();
                        // Current Hour
                        hour = c.get(Calendar.HOUR_OF_DAY);
                        // Current Minute
                        minute = c.get(Calendar.MINUTE);
                        isFromTime = false;
                        updateTime(hour, minute);
                        mIntervalList = intervalSpinnerDataValue();
                        mSpinnerList = intervalSpinnerValue();
                    }
                    position=""+tab.getPosition();
                    loadFragment(position,chart);
                }
            });

            mIntervalList = new ArrayList<String>();
            mSpinnerList = new ArrayList<String>();

            showAlertDialog();
        }catch(Exception e){
            Log.d("try","error7 "+e.getMessage());
        }

    }

    private void setBottomLayoutData() {
        try {
            mTxtFromDate.setText(mStartDate);
            mTxtEndDate.setText(mEndDate);
            mTxtFromTime.setText(mStartTime);
            mTxtEndTime.setText(mEndTime);
            setIntervalSpinnerValue();
            mIntervalList = intervalSpinnerDataValue();

            Constant.mFuelStoredFromDate = null;
            Constant.mFuelStoredToDate = null;
            Constant.mFuelStoredFromTime = null;
            Constant.mFuelStoredToTime = null;
            Constant.mFuelInterval = null;
        }catch(Exception e){
            Log.d("try","error8 "+e.getMessage());
        }


    }
    private void loadFragment(String postion,Boolean chat) {
        try {
            FuelStoredFromDate=mStartDate;
            FuelStoredToDate=mEndDate;
            FuelStoredFromTime=mStartTime;
            FuelStoredToTime=mEndTime;
            StartTimeFuelReport=mStartTimeFuelReport;
            EndTimeFuelReport=mEndTimeFuelReport;
            Fragment fragment = new SensorFuelFillFragment(postion,chat,mStartDate,mStartTimeFuelReport,mEndDate,mEndTimeFuelReport,from,to,mIntervalSpinnerSelectedValue);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frameLayout, fragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
        }catch(Exception e){
            Log.d("try","error9 "+e.getMessage());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_calendar_chart, menu);
        return true;
    }
    private void showAlertDialog(){
        Log.d("show alert","show3");
        try {
            androidx.appcompat.app.AlertDialog.Builder builders = new androidx.appcompat.app.AlertDialog.Builder(this,R.style.CustomAlertDialog);
            final View customLayout = LayoutInflater.from(this).inflate(R.layout.dialog_date_time_pick, null);
            mTxtFromDate = customLayout.findViewById(R.id.txt_start_date_value);
            mTxtFromTime = customLayout.findViewById(R.id.txt_start_time_value);
            mTxtEndDate = customLayout.findViewById(R.id.txt_end_date_value);
            mTxtEndTime =customLayout.findViewById(R.id.txt_end_time_value);
            mTxtstart = customLayout.findViewById(R.id.txt_date_start);
            mTxtend = customLayout.findViewById(R.id.txt_date_end);
            mBtnSubmit = customLayout.findViewById(R.id.btn_done);
            spinnerInterval = customLayout.findViewById(R.id.spinner_interval);

            spinnerInterval
                    .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                    {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            try {
                                mIntervalSpinnerSelectedPos = position;
                                SharedPreferences.Editor editor = sp.edit();
                                if (position != 0) {
                                    editor.putString("intervalvalue", String.valueOf(mIntervalList.get(position)));
                                    editor.putString("spinnervalue", mSpinnerList.get(position));
                                    mIntervalSpinnerSelectedValue = String.valueOf(mIntervalList.get(position));
                                    spinnervalue = mSpinnerList.get(position);
                                } else {
                                    mIntervalSpinnerSelectedValue = intervalue;
                                }
                                editor.commit();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

            mTxtFromTime.setOnClickListener(this);
            mTxtFromDate.setOnClickListener(this);
            mTxtEndDate.setOnClickListener(this);
            mTxtEndTime.setOnClickListener(this);
            mBtnSubmit.setOnClickListener(this);

            builders.setView(customLayout);
            dialogs= builders.create();
        }catch(Exception e){
            Log.d("try","error10 "+e.getMessage());
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_calendar:
                setIntervalSpinnerValue();
                dialogs.show();
                break;
            case R.id.action_chart:
                if(chart){
                    chart=false;
                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.line_chart));
                }
                else {
                    chart=true;
                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.data_table));
                }
                loadFragment(position,chart);
                break;
            default:
                break;
        }
        return true;
    }

    private void  setIntervalSpinnerValue() {
        try {
            ArrayAdapter dataAdapter = new ArrayAdapter(this, R.layout.spinner_row, intervalSpinnerValue());
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            Log.d("dataAdapter",""+intervalSpinnerDataValue());
            spinnerInterval.setAdapter(dataAdapter);
            int spinnerPosition = dataAdapter.getPosition(spinnervalue);
            spinnerInterval.setSelection(spinnerPosition);
            Log.d("dataAdapter1",""+mIntervalSpinnerSelectedPos);
        }catch(Exception e){
            Log.d("try","error11 "+e.getMessage());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_start_date_value:
                isFromDate = true;
                DialogFragment dialogfragment = new DatePickerDialogTheme();
                dialogfragment.show(getFragmentManager(), "Theme");
                break;
            case R.id.txt_start_time_value:
                isFromTime = true;
                showTimeDialog();
                break;
            case R.id.txt_end_date_value:
                isFromDate = false;
                DialogFragment dialogfragment1 = new DatePickerDialogTheme();
                dialogfragment1.show(getFragmentManager (),"Theme");
                break;
            case R.id.txt_end_time_value:
                isFromTime = false;
                showTimeDialog();
                break;
            case R.id.btn_done:
                Log.d("fuelData","start "+mStartDate+" "+mEndDate+" "+mStartTime+" "+mEndTime);
                from=convertDataTolong(mStartDate.trim(),mStartTime.trim());
                to=convertDataTolong(mEndDate.trim(),mEndTime.trim());
                dialogs.dismiss();
                loadFragment(position,chart);
                break;
        }
    }
    private void showTimeDialog() {
        try {
            if (isFromTime) {
                hour = mFromHourValue;
                minute = mFromMinuteValue;
            } else {
                hour = mToHourValue;
                minute = mToMinuteValue;
            }
            TimePickerDialog dialog2 = new TimePickerDialog(this, timePickerListener, hour, minute,
                    false);
            dialog2.show();
        }catch(Exception e){
            Log.d("try","error12 "+e.getMessage());
        }
    }
    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;
            updateTime(hour, minute);
        }

    };
    @SuppressLint("ValidFragment")
    public static class DatePickerDialogTheme extends DialogFragment implements DatePickerDialog.OnDateSetListener{
        private boolean isFromDate=FuelFillReportSensorActivity.isFromDate;
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_DEVICE_DEFAULT_DARK,this,year,month,day);
            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day){
            year = year;
            month = month;
            day = day;
            if(isFromDate){

                StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                        .append((month + 1)).append("-").append((day))
                        .append(" ");
                mTxtFromDate.setText(mDate2);
                mStartDate = String.valueOf(mDate2);
            }
            else {

                StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                        .append((month + 1)).append("-").append((day))
                        .append(" ");
                mTxtEndDate.setText(mDate2);
                mEndDate = String.valueOf(mDate2);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FuelFillReportSensorActivity.this, MapMenuActivity.class));
        finish();
    }
}