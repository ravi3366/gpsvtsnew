package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vamosys.adapter.TripSummaryAdapter;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.TripSummaryMobile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;


public class TripSummaryActivity extends AppCompatActivity implements OnClickListener {

    int width, height;
    //    ImageView $BackArrow, mImgChangeDate;
//    TextView
//            $TxtTitle;
    LinearLayout mLayoutList, mLayoutChk;

    List<com.vamosys.model.TripSummaryMobile> mTripSummaryList = new ArrayList<com.vamosys.model.TripSummaryMobile>();

    ListView mLstReportData;
    //    ProgressDialog progressDialog;
    ProgressDialog progressDialog;
    Const cons;
    int currentPage = 0;
    //    String mFromDate, mToDate;
    boolean is_scroll = true;

    Dialog marker_info_dialog;
    TextView id_from_date, id_to_date;
    int mScrollCount = 0;

    private int year, month, day, hour, minute;
    boolean from_date, to_date, onPressed = false;
    static final int DATE_PICKER_ID = 1111;

    //    private int year, month, day;
    static final int TIME_DIALOG_ID = 1111;
    static final int DATE_DIALOG_ID = 2222;
//    private int hour;
//    private int minute;

    ConnectionDetector cd;
    static Timer timer;
    SharedPreferences sp;
    Toolbar mToolbar;
    View bottomSheet;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;
    Button mBtnSubmit;
    TextView mTxtFromDate, mTxtEndDate, mTxtFromTime, mTxtEndTime;
    Spinner spinnerFromSite, spinnerEndSite;
    boolean isFromTime = false, isFromDate = false;
    String mStartDate, mEndDate, mStartTime, mStartTimeValue, mEndTime, mEndTimeValue;

    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "HH:mm:ss");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

//    private DatePickerDialog.OnDateSetListener datepickerListener = new DatePickerDialog.OnDateSetListener() {
//        // when dialog box is closed, below method will be called.
//        @Override
//        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
//            year = selectedYear;
//            month = selectedMonth;
//            day = selectedDay;
//
//
//            if (from_date) {
//                id_from_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
//            } else if (to_date) {
//                id_to_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
//            }
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_summary_main_layout);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("TripSummary(" + Constant.SELECTED_VEHICLE_SHORT_NAME + ")");
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);

        cons = new Const();
        cd = new ConnectionDetector(getApplicationContext());

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        mToolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(TripSummaryActivity.this, MapMenuActivity.class));
                finish();


            }
        });

        intialization();
//        screenArrange();

//        System.out.println("The vehicle id is:::::" + Constant.SELECTED_VEHICLE_ID);

//        $TxtTitle.setText("Trip Summary  (" + Constant.SELECTED_VEHICLE_SHORT_NAME + ")");

        progressDialog = new ProgressDialog(TripSummaryActivity.this,
                AlertDialog.THEME_HOLO_LIGHT);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setProgressDrawable(new ColorDrawable(
                android.graphics.Color.BLUE));
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);

//        mFromDate = cons.GetCurrentDate(Calendar.getInstance());
//        mToDate = cons.GetCurrentDate(Calendar.getInstance());

//        mFromDate = cons.GetFromCurrentDate(0);
//        mToDate = cons.GetToCurrentDate(0);

        mStartDate = cons.GetFromCurrentDate(0);
        mEndDate = cons.GetToCurrentDate(0);

        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        System.out.println("Hi date is :::" + timeFormat.format(cal.getTime()));

//        mStartTimeValue = "12:00 AM";
        mStartTimeValue = timeFormatShow.format(cal.getTime());
//        mStartTime = "12:00:00 am";
        mStartTime = timeFormat.format(cal.getTime());
        final Calendar c = Calendar.getInstance();
        // Current Hour
        hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        minute = c.get(Calendar.MINUTE);
        isFromTime = false;


        //updateTime(hour, minute);

        updateTime(23, 59);

        setBottomLayoutData();

        loadDatas();

        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        mLstReportData.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int pos,
                                    long arg3) {
                // TODO Auto-generated method stub
                Constant.SELECTED_TRIP_FROM_DATE = String.valueOf(mTripSummaryList.get(pos).getfT());
                Constant.SELECTED_TRIP_TO_DATE = String.valueOf(mTripSummaryList.get(pos).gettT());


                Intent i = new Intent(TripSummaryActivity.this, HistoryNewGoogleMapActivity.class);
                i.putExtra("from_date", Constant.SELECTED_TRIP_FROM_DATE);
                i.putExtra("to_date", Constant.SELECTED_TRIP_TO_DATE);
                startActivity(i);


//                startActivity(new Intent(TripSummaryActivity.this,
//                        HistoryActivity.class));
                finish();

            }
        });


//        mLstReportData.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem,
//                                 int visibleItemCount, int totalItemCount) {
//                // int currentFirstVisPos = view.getFirstVisiblePosition();
//                // if (currentFirstVisPos > myLastVisiblePos) {
//                // // scroll down
//                // }
//                // if (currentFirstVisPos < myLastVisiblePos) {
//                // // scroll up
//                // }
//                // myLastVisiblePos = currentFirstVisPos;
//                // Constant.prtMsg("Grid view position is ::::::::::"
//                // + view.getFirstVisiblePosition());
//            }
//
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//                // TODO Auto-generated method stub
//
//                if (scrollState == SCROLL_STATE_IDLE) {
//                    if (mLstReportData.getLastVisiblePosition() >= mLstReportData.getCount() - 1) {
//                        currentPage++;
//                        // // load more list items:
//                        // loadElements(currentPage);
////                        System.out
////                                .println("Load more items called::::::::::::");
//
////                        mFromDate = cons.GetFromCurrentDate(currentPage);
////                        mToDate = cons.GetToCurrentDate(currentPage);
//
////                        mStartDate = cons.GetFromCurrentDate(currentPage);
////                        mEndDate = cons.GetToCurrentDate(currentPage);
//
//                        mStartDate = cons.GetFromDateFromGiven(mStartDate);
//                        mEndDate = cons.GetToDateFromGivenDate(mStartDate);
//
//
//                        if (is_scroll) {
//                            if (cd.isConnectingToInternet()) {
//
//                                loadDatas();
//                            } else {
//                                Toast.makeText(TripSummaryActivity.this,
//                                        "Please check your network connection",
//                                        Toast.LENGTH_SHORT).show();
//
//                            }
//                        }
//                    }
//                }
//
//            }
//        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_calendar, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_calendar:
//                storeFavourtite();

                if (!bottomSheetEnabled) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    // setData();
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }

                break;

            default:
                break;
        }

        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.txt_start_date_value:

                isFromDate = true;
                showDialog(DATE_DIALOG_ID);
                break;
            case R.id.txt_start_time_value:
//                showDialog(DATE_DIALOG_FROMID);
                isFromTime = true;
                showDialog(TIME_DIALOG_ID);
                break;
            case R.id.txt_end_date_value:
//                createNumberDialog(1);
                isFromDate = false;
                showDialog(DATE_DIALOG_ID);
//                show(1);

                break;
            case R.id.txt_end_time_value:
//                createNumberDialog(0);
                isFromTime = false;
                showDialog(TIME_DIALOG_ID);
//                show(0);
                break;

//            case R.id.img_calendar:
//                if (!bottomSheetEnabled) {
//                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                } else {
//                    // setData();
//                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }
//
//                if (bottomSheetEnabled) {
//                    bottomSheetEnabled = false;
//                } else {
//                    bottomSheetEnabled = true;
//                }
//
//
//
//                break;
            case R.id.btn_done:
//                createNumberDialog(0);
//                show(0);
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }
                if (cd.isConnectingToInternet()) {


                    mTripSummaryList.clear();

//                    String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    new GetReportData().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    private void setBottomLayoutData() {
        mTxtFromDate.setText(mStartDate);
        mTxtEndDate.setText(mEndDate);
        mTxtFromTime.setText(mStartTimeValue);
        mTxtEndTime.setText(mEndTimeValue);
    }

    private void loadDatas() {
        // TODO Auto-generated method stub
        // preloaderStart();
        // Handler h = new Handler();
        // h.postDelayed(new Runnable() {
        // @Override
        // public void run() {
        // // TODO Auto-generated method stub
        // // homeProgressDialog.show();
        // new loadScreenData().execute();
        //
        // }
        // }, 200);

//        if (timer != null) {
//            timer.cancel();
//        }
//        timer = new Timer();
//        timer.schedule(new TimerTask() {
//http://188.166.244.126:9000/
//            public void run() {
//                String url = Const.API_URL + "/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + cons.GetCurrentDate(Calendar.getInstance()) + "&toDate=" + cons.GetCurrentDate(Calendar.getInstance());
        if (cd.isConnectingToInternet()) {
//            String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + mFromDate + "&toDate=" + mToDate;
            new GetReportData().execute();
        } else {
            Toast.makeText(TripSummaryActivity.this, "Please check your network connection", Toast.LENGTH_SHORT).show();
        }
//            }
//        }, 1000);

    }


    private void intialization() {
        // TODO Auto-generated method stub


//        $TxtTitle = (TextView) findViewById(R.id.trip_summ_title);

//        $TxtTitle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

//        $BackArrow = (ImageView) findViewById(R.id.trip_summ_view_Back);
//        mImgChangeDate = (ImageView) findViewById(R.id.trip_summ_change_date);

        mLstReportData = (ListView) findViewById(R.id.trip_summ_list_view);
        mTxtFromDate = (TextView) findViewById(R.id.txt_start_date_value);
        mTxtFromTime = (TextView) findViewById(R.id.txt_start_time_value);
        mTxtEndDate = (TextView) findViewById(R.id.txt_end_date_value);
        mTxtEndTime = (TextView) findViewById(R.id.txt_end_time_value);
        mBtnSubmit = (Button) findViewById(R.id.btn_done);
        spinnerFromSite = (Spinner) findViewById(R.id.spinner_from_site);
        spinnerEndSite = (Spinner) findViewById(R.id.spinner_end_site);
        spinnerEndSite.setVisibility(View.GONE);
        spinnerFromSite.setVisibility(View.GONE);
        mTxtFromTime.setOnClickListener(this);
        mTxtFromDate.setOnClickListener(this);
        mTxtEndDate.setOnClickListener(this);
        mTxtEndTime.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);

//        $BackArrow.setOnClickListener(this);
//        mImgChangeDate.setOnClickListener(this);


//        $FRomValue.setText(cons.GetCurrentDate(Calendar.getInstance()));
//        $ToValue.setText(cons.GetCurrentDate(Calendar.getInstance()));


    }

    /**
     * Function to add 0 for time which is lesser that 10. Ex. 08.00, 05.00
     */
    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

//    @Override
//    public void onClick(View v) {
//        // TODO Auto-generated method stub
//        switch (v.getId()) {
//            case R.id.trip_summ_view_Back:
//                startActivity(new Intent(TripSummaryActivity.this, MapMenuActivity.class));
//                finish();
//                break;
//            case R.id.trip_summ_change_date:
//
//                showVehicleInfoDialog();
//
//                break;
//
//        }
//    }


    private class GetReportData extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date d = f.parse(string_from_date);

                long fromMilliSeconds = d.getTime();

                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
//old api
//                String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID.trim() + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID.trim() + "&fromDate=" +
//                        mStartDate.trim() + "&toDate=" + mEndDate.trim() + "&fromDateUTC=" + fromMilliSeconds +
//                        "&toDateUTC=" + toMilliSeconds+&fromTime=0:0:0+&toTime=15:23:23;

                String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID.trim() + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID.trim() + "&fromDate=" +
                        mStartDate.trim() + "&toDate=" + mEndDate.trim() + "&fromDateUTC=" + fromMilliSeconds +
                        "&toDateUTC=" + toMilliSeconds + "&fromTime=" + mStartTime + "&toTime=" + mEndTime;

//                String url = Const.API_URL + "/mobile/getSiteTripReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + mStartDate.trim() + "&fromTime=" + mStartTime.trim()
//                        + "&toDate=" + mEndDate.trim() + "&toTime=" + mEndTime.trim() + "&language=en&site1=" + mFromSite + "&site2=" + mEndSite + "&fromDateUTC=" + fromMilliSeconds +
//                        "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID;


                HttpConfig ht = new HttpConfig();
                return ht.httpGet(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {
            // Log.d(TAG, "GetReportData()" + "result = " + result + "");
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (timer != null) {
                timer.cancel();
            }

            try {


                if (result != null && result.length() > 0) {
//                    System.out.println("The Tripsummary report result is ::::" + result);
                    TripSummaryMobile tripResponse;

                    Gson g = new Gson();

                    tripResponse = g.fromJson(result.trim(),
                            TripSummaryMobile.class);
                    if (tripResponse.getTrip() != null) {

//                        mTripSummaryList = tripResponse.getTrip();

                        List<com.vamosys.model.TripSummaryMobile> mTSList = new ArrayList<com.vamosys.model.TripSummaryMobile>();
                        mTSList.addAll(tripResponse.getTrip());
//                        Collections.sort(mTSList, new DateComparatorOldToLatest());
                        Collections.reverse(mTSList);

                        //  mTripSummaryList.addAll(tripResponse.getTrip());

                        mTripSummaryList.addAll(mTSList);


                        int currentPosition = mLstReportData.getFirstVisiblePosition();
                        TripSummaryAdapter tadpater = new TripSummaryAdapter(TripSummaryActivity.this, mTripSummaryList);
//                    mLstReportData.setAdapter(new TripSummaryAdapter(TripSummaryActivity.this, mTripSummaryList));
                        mLstReportData.setAdapter(tadpater);
                        Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.grow_from_bottom);
                        bottomUp.setDuration(1000);
                        mLstReportData.startAnimation(bottomUp);

//                    int currentPosition = mLstReportData.getFirstVisiblePosition();

//                    grid.setAdapter(new HomeScreenAdapter(HomeScreen.this,
//                            Constant.home_list));
                        // ListViewWrap.setListViewHeightBasedOnChildren(grid);
                        // Setting new scroll position
                        // grid.setSelectionFromTop(currentPosition + 1, 0);
                        mLstReportData.setSelection(currentPosition);
                        tadpater.notifyDataSetChanged();


                    } else {
                        Toast.makeText(getApplicationContext(), "No data found for the date range from " + mStartDate + " to " + mEndDate, Toast.LENGTH_LONG).show();
                    }
//                    setData();
                }


//                mTripSummaryList.addAll(tripResponse.getTrip());

                System.out.println("Hi trip summary list size is " + mTripSummaryList.size());

                int currentPosition = mLstReportData.getFirstVisiblePosition();
                TripSummaryAdapter tadpater = new TripSummaryAdapter(TripSummaryActivity.this, mTripSummaryList);
//                    mLstReportData.setAdapter(new TripSummaryAdapter(TripSummaryActivity.this, mTripSummaryList));
                mLstReportData.setAdapter(tadpater);
                Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.grow_from_bottom);
                bottomUp.setDuration(1000);
                mLstReportData.startAnimation(bottomUp);

//                    int currentPosition = mLstReportData.getFirstVisiblePosition();

//                    grid.setAdapter(new HomeScreenAdapter(HomeScreen.this,
//                            Constant.home_list));
                // ListViewWrap.setListViewHeightBasedOnChildren(grid);
                // Setting new scroll position
                // grid.setSelectionFromTop(currentPosition + 1, 0);
                mLstReportData.setSelection(currentPosition);
                tadpater.notifyDataSetChanged();
//                    //  svReportOption.setVisibility(View.GONE);
                if (mTripSummaryList.size() > 0) {
                } else {
//                    Toast.makeText(getApplicationContext(), "No data found...", Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "No data found for the date range from " + mStartDate + " to " + mEndDate, Toast.LENGTH_LONG).show();
////                    startActivity(new Intent(TripSummaryActivity.this, MapMenuActivity.class));
////                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(TripSummaryActivity.this, MapMenuActivity.class));
        finish();

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            if (isFromDate) {
                mStartDate = String.valueOf(mDate2);
                //  mStartTimeValue=String.valueOf(mDate2);
                mTxtFromDate.setText(mDate2);
            } else {
                mEndDate = String.valueOf(mDate2);

                mTxtEndDate.setText(mDate2);
            }
//            mFromDate = String.valueOf(mDate);
//            mFromDateTxtValue = String.valueOf(mDate2);
//            mTxtDate.setText(mDate2);
            // }
        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:

                // set time picker as current time
                return new TimePickerDialog(this, timePickerListener, hour, minute,
                        false);

            case DATE_DIALOG_ID:
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                /** set date picker as current date */
                DatePickerDialog dialog = new DatePickerDialog(this,
                        datePickerListener, year, month, day);
                // set time picker as current time
                return dialog;

        }
        return null;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);

        }

    };

    private String utilTime(int value) {

        if (value < 10)
            return "0" + String.valueOf(value);
        else
            return String.valueOf(value);
    }

    private void updateTime(int hours, int mins) {


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, mins);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        System.out.println("Hi date is 222:::" + timeFormat.format(cal.getTime()));

//        String aTime = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
        String aTime = timeFormatShow.format(cal.getTime());
//        String aTime2 = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(":00").append(minutes).append(" ").append(timeSet2).toString();

        String aTime2 = timeFormat.format(cal.getTime());


        if (isFromTime) {
            mStartTime = aTime2;
            mStartTimeValue = aTime;
            mTxtFromTime.setText(mStartTimeValue);
        } else {
            mEndTime = aTime2;
            mEndTimeValue = aTime;
            mTxtEndTime.setText(mEndTimeValue);
        }
    }

}