package com.vamosys.vamos;

import android.database.Cursor;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Created by srinath on 23-03-2015.
 */
public class ParsingClass {
    double ododistance, latitude,currlat,currlng, longitude, curr_lat, curr_long, poi_lat, poi_lng;
    String parked_time, totalRows, totalRunningTime, toDateTime, fromDateTime, toDateTimeUTC, fromDateTimeUTC, tripDistance, distance_covered, speed, shortname, direction, vehicleId, mobileno, position="P", parkedTime, lastseen, vehicleType, overspeed="N", geofence="N", overSpeedLimit, address;
    String totalParkedTime, totalMovingTime, totalIdleTime, totalNoDataTime,shorttname;
    int mParkedCount = 0, mOverSpeedCount = 0;
    JSONObject vehiclesJSON;
    JSONArray latLngOld;
    boolean nohistory = true;
    JsonParser historyjsonparser;
    Const cons = new Const();

    static public String getparkedTime(int time) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        String currentDateTimeString = sdf.format(Integer.toString(time));
        return currentDateTimeString;
    }

    public void getVehiclesInfo(Cursor vehicles_cur, String vehicl_id) {
        vehicles_cur.moveToFirst();
        int vehicles_count = vehicles_cur.getCount();
        for (int i = 0; i < vehicles_count; i++) {
            if (!vehicles_cur.getString(1).equals("null")) {
                try {
                    JSONArray vehicles_array = new JSONArray(vehicles_cur.getString(1));
                    for (int j = 0; j < vehicles_array.length(); j++) {
                        JSONObject vehicleinfo = vehicles_array.getJSONObject(j);
                        if (vehicleinfo.getString("vehicleId").equals(vehicl_id)) {
                            vehiclesJSON = vehicleinfo;
                            latitude = Double.parseDouble(vehicleinfo.getString("latitude"));
                            longitude = Double.parseDouble(vehicleinfo.getString("longitude"));
                            distance_covered = Double.toString((vehicleinfo.getDouble("distanceCovered")));
                            speed = Integer.toString(vehicleinfo.getInt("speed"));
                            position = vehicleinfo.getString("position");
                            mobileno = vehicleinfo.getString("mobileNo");
                            vehicleType = vehicleinfo.getString("vehicleType");
                        }
                    }
                } catch (JSONException je) {
                    je.printStackTrace();
                }
            }
            vehicles_cur.moveToNext();
        }
    }


    public void getVehiclelatlnginfo(String curr_vehicle_info){
        try {
            JSONArray vehicles_array = new JSONArray(curr_vehicle_info);
            for (int j = 0; j < vehicles_array.length(); j++) {
                JSONObject vehicleinfo = vehicles_array.getJSONObject(j);

                    currlat = Double.parseDouble(vehicleinfo.getString("lat"));
                    currlng = Double.parseDouble(vehicleinfo.getString("lng"));
                    shorttname = vehicleinfo.getString("shortName");

            }
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void getCurrentVehicleInfo(String curr_vehicle_info) {
        try {
            JSONObject vehicleinfo = new JSONObject(curr_vehicle_info);
            curr_lat = Double.parseDouble(vehicleinfo.getString("latitude"));
            curr_long = Double.parseDouble(vehicleinfo.getString("longitude"));
            ododistance = vehicleinfo.getDouble("odoDistance");
            speed = Integer.toString(vehicleinfo.getInt("speed"));
            position = vehicleinfo.getString("position");
            vehicleId = vehicleinfo.getString("vehicleId");
            distance_covered = Double.toString(vehicleinfo.getDouble("distanceCovered"));
            direction = vehicleinfo.getString("direction");
            shortname = vehicleinfo.getString("shortName");
            overspeed = vehicleinfo.getString("isOverSpeed");
            geofence = vehicleinfo.getString("insideGeoFence");
            vehicleType = vehicleinfo.getString("vehicleType");
            address = vehicleinfo.getString("address");

            if (vehicleinfo.has("latLngOld")) {
                latLngOld = vehicleinfo.getJSONArray("latLngOld");
            }

        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void getVehicleHistory(String curr_vehicle_info) {
        try {
            JSONObject vehicleinfo = new JSONObject(curr_vehicle_info);
            curr_lat = Double.parseDouble(vehicleinfo.getString("lt"));
            curr_long = Double.parseDouble(vehicleinfo.getString("lg"));
            speed = Integer.toString(vehicleinfo.getInt("sp"));
            direction = vehicleinfo.getString("dr");
            position = vehicleinfo.getString("ps");
            lastseen = vehicleinfo.getString("dt");
            distance_covered = Double.toString(vehicleinfo.getDouble("dc"));
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void getPoiInformation(String curr_vehicle_info) {
        try {
            JSONObject vehicleinfo = new JSONObject(curr_vehicle_info);
            poi_lat = Double.parseDouble(vehicleinfo.getString("latitude"));
            poi_lng = Double.parseDouble(vehicleinfo.getString("longitude"));
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void historyParse(String result) {
        JsonFactory f = new MappingJsonFactory();
        try {
            JsonParser jp = f.createJsonParser(result);
            historyjsonparser = jp;
            JsonToken current;
            current = jp.nextToken();
            if (current != JsonToken.START_OBJECT) {
                return;
            }
            while (jp.nextToken() != JsonToken.END_OBJECT) {
                String fieldName = jp.getCurrentName();
                if (fieldName.equals("vehicleId")) {
                    jp.nextToken();
                    vehicleId = jp.getText();
                }
                if (fieldName.equals("overSpeedLimit")) {
                    jp.nextToken();
                    overSpeedLimit = jp.getText();
                }
                if (fieldName.equals("shortName")) {
                    jp.nextToken();
                    shortname = jp.getText();
                }
                if (fieldName.equals("totalRunningTime")) {
                    jp.nextToken();
                    totalRunningTime = Long.toString(jp.getValueAsLong());
                }
                if (fieldName.equals("tripDistance")) {
                    jp.nextToken();
                    tripDistance = jp.getText();
                }
                if (fieldName.equals("fromDateTime")) {
                    jp.nextToken();
                    fromDateTime = jp.getText();
                }
                if (fieldName.equals("toDateTime")) {
                    jp.nextToken();
                    toDateTime = jp.getText();
                }

                if (fieldName.equals("toDateTimeUTC")) {
                    jp.nextToken();
                    toDateTimeUTC = Long.toString(jp.getValueAsLong());
                }

                if (fieldName.equals("fromDateTimeUTC")) {
                    jp.nextToken();
                    fromDateTimeUTC = Long.toString(jp.getValueAsLong());
                }

                if (fieldName.equals("history4Mobile")) {
                    jp.skipChildren();
                }
                if (fieldName.equals("totalRows")) {
                    jp.nextToken();
                    totalRows = jp.getText();
                }

                if (fieldName.equals("odoDistance")) {
                    jp.nextToken();
                    ododistance = Double.valueOf(jp.getText());
                }

//                if (fieldName.equals("totalParkedTime")) {
//                    jp.nextToken();
//
//                    System.out.println("The parked time is :::::" + String.valueOf(jp.getText()));
//
//                    totalParkedTime = cons.getVehicleTime(String.valueOf(jp.getText()));
//                }

//                if (fieldName.equals("totalIdleTime")) {
//                    jp.nextToken();
//                    System.out.println("The totalIdleTime time is :::::" + String.valueOf(jp.getText()));
//                    totalIdleTime = cons.getVehicleTime(String.valueOf(jp.getText()));
//                }
//
//                if (fieldName.equals("totalRunningTime")) {
//                    jp.nextToken();
//                    System.out.println("The totalRunningTime time is :::::" + String.valueOf(jp.getText()));
//                    totalMovingTime = cons.getVehicleTime(String.valueOf(jp.getText()));
//                }
//
//                if (fieldName.equals("totalNoDataTime")) {
//                    jp.nextToken();
//                    System.out.println("The totalNoDataTime time is :::::" + String.valueOf(jp.getText()));
//                    totalNoDataTime = cons.getVehicleTime(String.valueOf(jp.getText()));
//                }

            }

            try {
                JSONObject vehicleinfo = new JSONObject(result);
                totalParkedTime = cons.getVehicleTime(String.valueOf(vehicleinfo.getString("totalParkedTime")));

                totalIdleTime = cons.getVehicleTime(String.valueOf(vehicleinfo.getString("totalIdleTime")));
                totalMovingTime = cons.getVehicleTime(String.valueOf(vehicleinfo.getString("totalRunningTime")));
                totalNoDataTime = cons.getVehicleTime(String.valueOf(vehicleinfo.getString("totalNoDataTime")));

                mParkedCount = (vehicleinfo.getInt("parkingCount"));
                mOverSpeedCount = (vehicleinfo.getInt("overSpeedInstances"));

                vehicleType = (vehicleinfo.getString("vehicleType"));

            } catch (JSONException je) {
                je.printStackTrace();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void historyobjectparsing(JsonNode node) {
        curr_lat = Double.parseDouble(node.get("lt").getValueAsText());
        curr_long = Double.parseDouble(node.get("lg").getValueAsText());
        speed = node.get("sp").getValueAsText();
        direction = node.get("dr").getValueAsText();
        position = node.get("ps").getValueAsText();
        if (position.equalsIgnoreCase("P") || position.equalsIgnoreCase("S")) {
            parked_time = cons.getVehicleTime(node.get("pt").getValueAsText());
        } else {
            parked_time = "-";
        }
        lastseen = node.get("dt").getValueAsText();
        distance_covered = node.get("dc").getValueAsText();
        address=node.get("address").getValueAsText();
    }

    public JSONArray getLatLngOld() {
        return latLngOld;
    }

//    public void setLatLngOld(JSONArray latLngOld) {
//        this.latLngOld = latLngOld;
//    }

    /**
     * Get and Set methods
     */


    public String getVehicleId() {
        return vehicleId;
    }

    public String getvehicleType() {
        return vehicleType;
    }

    public String getshortname() {
        return shortname;
    }

    public String getOverSpeedLimit() {
        return overSpeedLimit;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getpoiLatitude() {
        return poi_lat;
    }

    public double getpoiLongitude() {
        return poi_lng;
    }

    public String getSpeed() {
        return speed;
    }

    public String gettripDistance() {
        return tripDistance;
    }

    public String getfromDateTime() {
        return fromDateTime;
    }

    public String getToDateTimeUTC() {
        return toDateTimeUTC;
    }

    public String getFromDateTimeUTC() {
        return fromDateTimeUTC;
    }

    public String gettoDateTime() {
        return toDateTime;
    }

    public String gettotalRunningTime() {
        return totalRunningTime;
    }

    public String gettotalRows() {
        return totalRows;
    }

    public String getparked_time() {
        return parked_time;
    }

    public boolean getHistoryStatus() {
        return nohistory;
    }

    public JsonParser gethistoryjsonparser() {
        return historyjsonparser;
    }

    public String getparkedTime() {
        return parkedTime;
    }

    public String getMobileno() {
        return mobileno;
    }

    public String getPosition() {
        return position;
    }

    public String getDirection() {
        return direction;
    }

    public String getDistanCovered() {
        return distance_covered;
    }

    public String getodoDistance() {
        return Double.toString(ododistance);
    }

    public JSONObject getVehiclesJSON() {
        return vehiclesJSON;
    }

    public double getCurr_lat() {
        return curr_lat;
    }

    public double getCurr_long() {
        return curr_long;
    }

    public String getlastseen() {
        return lastseen;
    }

    public String getOverSpeed() {
        return overspeed;
    }

    public String getGeoFence() {
        return geofence;
    }

    public String getAddress() {
        return address;
    }

    public double getOdodistance() {
        return ododistance;
    }

    public String getTotalParkedTime() {
        return totalParkedTime;
    }

    public String getTotalMovingTime() {
        return totalMovingTime;
    }

    public String getTotalIdleTime() {
        return totalIdleTime;
    }

    public String getTotalNoDataTime() {
        return totalNoDataTime;
    }

    public int getmOverSpeedCount() {
        return mOverSpeedCount;
    }

    public int getmParkedCount() {
        return mParkedCount;
    }


    public double getCurrlat() {
        return currlat;
    }

    public double getCurrlng() {
        return currlng;
    }

    public String getShorttname() {
        return shorttname;
    }
}
