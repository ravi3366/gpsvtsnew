package com.vamosys.vamos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FuelTheftNewReport extends AppCompatActivity  {
    Toolbar mToolbar;
    AppCompatTextView mTxtVehicleName;
    ConnectionDetector cd;
    SharedPreferences sp;

    FrameLayout frameLayout;
    TabLayout tabLayout;
    String position="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_theft_new_report);
        try {
            mToolbar = findViewById(R.id.toolbar);
            mToolbar.setTitle(getResources().getString(R.string.fuel_theft_report));
            mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
            setSupportActionBar(mToolbar);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(FuelTheftNewReport.this, MapMenuActivity.class));
                    finish();
                }
            });
            init();
            loadFragment(position);
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    private void init() {
        try {
            cd = new ConnectionDetector(getApplicationContext());
            sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

            mTxtVehicleName = findViewById(R.id.selected_vehicle_txt);
            mTxtVehicleName.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
            frameLayout=findViewById(R.id.frameLayout);
            tabLayout = findViewById(R.id.tablayout);
            TabLayout.Tab sensor1 = tabLayout.newTab();
            sensor1.setText("Sensor 1");
            tabLayout.addTab(sensor1);

            TabLayout.Tab sensor2 = tabLayout.newTab();
            sensor2.setText("Sensor 2");
            tabLayout.addTab(sensor2);

            tabLayout.getTabAt(0).select();

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    position=""+tab.getPosition();
                    loadFragment(position);
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    position=""+tab.getPosition();
                    loadFragment(position);
                }
            });
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }


    }

    private void loadFragment(String postion) {
        try {
            Fragment fragment = new SensorFuelTheftFragment(postion);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frameLayout, fragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FuelTheftNewReport.this, MapMenuActivity.class));
        finish();
    }
}