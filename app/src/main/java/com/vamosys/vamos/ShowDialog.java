package com.vamosys.vamos;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

public class ShowDialog extends Activity {

    SharedPreferences sp;
    String value,safemsg,fuelmsg,fueldropmsg,ALERT_MSG;
    String FUELMSG="FuelFilldata";
    String FUELDROPMSG = "Fueldropped";
    String SAFETYMSG = "safetyparking";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
      //  setContentView(R.layout.show_dialog);
        value=getIntent().getStringExtra("vehicle");
        safemsg=getIntent().getStringExtra("safetyname");
        fuelmsg=getIntent().getStringExtra("fuelname");
        fueldropmsg=getIntent().getStringExtra("fueldropname");
        Log.d("safemsg",""+safemsg);
        Log.d("fuelmsg",""+fuelmsg);
        Log.d("fueldropmsg",""+fueldropmsg);

        // Build the dialog
        if (SAFETYMSG.equalsIgnoreCase(safemsg)){
            AlertDialog.Builder Builder=new AlertDialog.Builder(this)
                    .setMessage("Safety Parking Alert is" + value)
                    .setTitle("Alert ")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ShowDialog.this.finish();
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ShowDialog.this.finish();
                        }
                    });
            AlertDialog alertDialog=Builder.create();
            alertDialog.show();
        }
         else if (FUELMSG.equalsIgnoreCase(fuelmsg)){
            AlertDialog.Builder Builder=new AlertDialog.Builder(this)
                    .setMessage("Fuel Filled Alarm of" + value)
                    .setTitle("Alert ")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ShowDialog.this.finish();
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ShowDialog.this.finish();
                        }
                    });
            AlertDialog alertDialog=Builder.create();
            alertDialog.show();
        } else if (FUELDROPMSG.equalsIgnoreCase(fueldropmsg)){
            AlertDialog.Builder Builder=new AlertDialog.Builder(this)
                    .setMessage("Fuel Dropped Alarm of" + value)
                    .setTitle("Alert ")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ShowDialog.this.finish();
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ShowDialog.this.finish();
                        }
                    });
            AlertDialog alertDialog=Builder.create();
            alertDialog.show();
        }



    }



}
