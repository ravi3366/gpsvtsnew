package com.vamosys.vamos;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;
import com.vamosys.utils.TypefaceUtil;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by wifin on 1/24/2016.
 */
public class VehicleHistoryActivity extends FragmentActivity {
    int width, height;
    static final int DATE_PICKER_ID = 1111;
    static final int TIME_DIALOG_ID = 999;

    DBHelper dbhelper;
    SharedPreferences sp;
    ConnectionDetector cd;
    Const cons;

    TextView id_show_history_vehicleid, history_distance_covered_value, history_start_location_value, history_end_location_value, histroy_start_time_value, history_endtime_value, history_timetaken_value, id_history_speed_info_value, id_history_distance_value, id_history_direction_value;
    LinearLayout history_detail_layout, id_history_show_min_layout, history_vehicle_info_layout,is_history_vehicle_layout;
    Button pause_icon, replay_icon, play_icon,mBtnClose;
    SeekBar history_seekbar, speed_seekbar;

    GoogleMap map;
    Dialog marker_info_dialog;
    ProgressDialog progressDialog;
    String historyRepeat;
    final ArrayList<Marker> zoom_location_marker = new ArrayList<Marker>();

    ArrayList<LatLng> points;
    ArrayList<String> speedlist;
    ArrayList<String> vehicle = new ArrayList<String>();
    ArrayList<String> datetimelist;
    ArrayList<String> distancelist;

    private HashMap<Marker, String> parker_mMarkersHashMap;
    Boolean pause = false, delay = true, track = false;
    boolean from_date, to_date, from_time, to_time, seekbarchanged = false, backButtonPressed = false;
    int progressChanged = 0;
    int loopvalue = 0, a, paused_value, map_marker_vechile_position;
    Runnable runnable1;
    private int iLoopDelay;
    private boolean isSpeedChanged = false;
    Handler handler1;
    Dialog current_address__dialog;
    float history_zoom_level = 11.5F;
    final ArrayList<Marker> current_users_location_point = new ArrayList<Marker>();
    private int year, month, day, hour, minute;

//For popup window

    TextView id_histroy_vehicleid, id_history_type_value, id_from_label, id_from_date, id_from_time, id_to_date, id_to_time;
    /**
     * Listener for date picker
     */
    private DatePickerDialog.OnDateSetListener datepickerListener = new DatePickerDialog.OnDateSetListener() {
        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            if (from_date) {
                id_from_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
            } else if (to_date) {
                id_to_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
            }
        }
    };
    /**
     * Listener for Time Picker
     */
    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int selected_hour, int selected_minute) {
            // TODO Auto-generated method stub
            hour = selected_hour;
            minute = selected_minute;
            if (from_time) {
                id_from_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
            } else if (to_time) {
                id_to_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_history_layout);
        dbSetup();
        init();

        dbhelper.deletecurrent_vehicleinfo();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        cons = new Const();

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        opptionPopUp();


        history_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChanged = progress;
                if (pause) {
                    play_icon.setVisibility(View.VISIBLE);
                    pause_icon.setVisibility(View.GONE);
                } else {
                    play_icon.setVisibility(View.GONE);
                    pause_icon.setVisibility(View.VISIBLE);
                }
                replay_icon.setVisibility(View.GONE);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                if (!pause) {
                    seekbarchanged = true;
                }
                handler1.removeCallbacks(runnable1);
                // handler.removeCallbacksAndMessages(null);
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!pause) {
                    Handler seekbarhandler = new Handler();
                    seekbarhandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            marker_loop(progressChanged);
                        }
                    }, 500);
                }
                if (progressChanged == history_seekbar.getMax()) {
                    history_seekbar.setVisibility(View.GONE);
                    replay_icon.setVisibility(View.VISIBLE);
                    id_history_show_min_layout.setVisibility(View.VISIBLE);
                    history_detail_layout.setVisibility(View.VISIBLE);
                    pause_icon.setVisibility(View.GONE);
                    play_icon.setVisibility(View.GONE);
                }
            }
        });


        replay_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id_history_show_min_layout.setVisibility(View.GONE);
                speed_seekbar.setVisibility(View.GONE);
                history_seekbar.setVisibility(View.VISIBLE);
                a = 0;
                pause = false;
                speed_seekbar.setProgress(50);
                marker_loop(0);
                if (points.size() > 1) {
                    replay_icon.setVisibility(View.GONE);
                    pause_icon.setVisibility(View.VISIBLE);
                    play_icon.setVisibility(View.GONE);
                    speed_seekbar.setVisibility(View.GONE);
                    history_seekbar.setVisibility(View.VISIBLE);
                } else {
                    replay_icon.setVisibility(View.GONE);
                    play_icon.setVisibility(View.GONE);
                    pause_icon.setVisibility(View.GONE);
                    speed_seekbar.setVisibility(View.GONE);
                    history_seekbar.setVisibility(View.GONE);
                }
            }
        });
        play_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pause = false;
                marker_loop(progressChanged);
                pause_icon.setVisibility(View.VISIBLE);
                play_icon.setVisibility(View.GONE);
                paused_value = 0;
            }
        });
        pause_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pause = true;
                handler1.removeCallbacks(runnable1);
                // handler.removeCallbacksAndMessages(null);
                if (progressChanged == history_seekbar.getMax()) {
                    history_seekbar.setVisibility(View.GONE);
                    replay_icon.setVisibility(View.VISIBLE);
                    id_history_show_min_layout.setVisibility(View.VISIBLE);
                    history_detail_layout.setVisibility(View.VISIBLE);
                    pause_icon.setVisibility(View.GONE);
                    play_icon.setVisibility(View.GONE);
                } else {
                    pause_icon.setVisibility(View.GONE);
                    play_icon.setVisibility(View.VISIBLE);
                }
            }
        });


        is_history_vehicle_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id_history_show_min_layout.getVisibility() == View.VISIBLE) {
                    id_history_show_min_layout.setVisibility(View.GONE);
                } else {
                    id_history_show_min_layout.setVisibility(View.VISIBLE);
                }
            }
        });

mBtnClose.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(VehicleHistoryActivity.this,VehicleListActivity.class));
        finish();
    }
});


    }

    public void init() {
//        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.vehicle_history_map)).getMap();
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);

        parker_mMarkersHashMap = new HashMap<Marker, String>();

        id_show_history_vehicleid = (TextView) findViewById(R.id.id_show_history_vehicleid);
        history_detail_layout = (LinearLayout) findViewById(R.id.history_detail_layout);
        is_history_vehicle_layout = (LinearLayout) findViewById(R.id.is_history_vehicle_layout);
        id_history_show_min_layout = (LinearLayout) findViewById(R.id.id_history_show_min_layout);

        mBtnClose = (Button) findViewById(R.id.history_close_icon);
       // replay_icon = (Button) findViewById(R.id.replay_icon);

        pause_icon = (Button) findViewById(R.id.pause_icon);
        replay_icon = (Button) findViewById(R.id.replay_icon);
        play_icon = (Button) findViewById(R.id.play_icon);
        history_distance_covered_value = (TextView) findViewById(R.id.history_distance_covered_value);
        history_start_location_value = (TextView) findViewById(R.id.history_start_location_value);
        history_end_location_value = (TextView) findViewById(R.id.history_end_location_value);
        histroy_start_time_value = (TextView) findViewById(R.id.histroy_start_time_value);
        history_endtime_value = (TextView) findViewById(R.id.history_endtime_value);
        history_timetaken_value = (TextView) findViewById(R.id.history_timetaken_value);
        history_seekbar = (SeekBar) findViewById(R.id.history_seekbar);
        speed_seekbar = (SeekBar) findViewById(R.id.speed_seekbar);
        iLoopDelay = speed_seekbar.getProgress();

        id_history_speed_info_value = (TextView) findViewById(R.id.id_history_speed_info_value);
        id_history_distance_value = (TextView) findViewById(R.id.id_history_distance_value);
        id_history_direction_value = (TextView) findViewById(R.id.id_history_direction_value);
        history_vehicle_info_layout = (LinearLayout) findViewById(R.id.history_vehicle_info_layout);

    }

    void dbSetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**AsyncTask Background API Calls*/
    /**
     * Public Methods
     */
    public void marker_loop(int loop) {
        a = loop;
        int value = 0;
        handler1 = new Handler();
        System.out.println("The points size is:::"+points.size());
        for (a = loop; a < points.size(); a++) {

            loopvalue = a;
            show_marker(a, value);
            value++;
        }
    }

    private void opptionPopUp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(VehicleHistoryActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.VISIBLE);
        rg_home.setVisibility(View.GONE);

        RadioButton $Custom = (RadioButton) dialog
                .findViewById(R.id.rb_history_custom);
        RadioButton $Today = (RadioButton) dialog
                .findViewById(R.id.rb_history_today);
        RadioButton $Yesterday = (RadioButton) dialog
                .findViewById(R.id.rb_history_yesterday);
        RadioButton $DayBeforeYesterday = (RadioButton) dialog
                .findViewById(R.id.rb_history_day_before_yesterday);
        RadioButton $LastOneHour = (RadioButton) dialog
                .findViewById(R.id.rb_history_last_one_hour);
        $Custom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                dialog.dismiss();
                showVehicleInfoDialog("Custom");
            }
        });
        $Today.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //  map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                dialog.dismiss();
                showVehicleInfoDialog("Today");
            }
        });
        $Yesterday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                dialog.dismiss();
                showVehicleInfoDialog("Yesterday");
            }
        });

        $DayBeforeYesterday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //  map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                dialog.dismiss();
                showVehicleInfoDialog("Day Before Yesterday");
            }
        });
        $LastOneHour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                dialog.dismiss();
                showVehicleInfoDialog("Last One Hour");
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Custom.setLayoutParams(radioParama);
        $DayBeforeYesterday.setLayoutParams(radioParama);
        $Today.setLayoutParams(radioParama);
        $Yesterday.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $LastOneHour.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Custom.setTextSize(16);
            $Today.setTextSize(16);
            $Yesterday.setTextSize(16);
            $DayBeforeYesterday.setTextSize(16);
            $LastOneHour.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Custom.setTextSize(15);
            $Today.setTextSize(15);
            $Yesterday.setTextSize(15);
            $DayBeforeYesterday.setTextSize(15);
            $LastOneHour.setTextSize(15);

        } else if (width > 260 && width < 500) {
            $Custom.setTextSize(14);
            $Today.setTextSize(14);
            $Yesterday.setTextSize(14);
            $DayBeforeYesterday.setTextSize(14);
            $LastOneHour.setTextSize(14);
        } else if (width <= 260) {
            $Custom.setTextSize(13);
            $Today.setTextSize(13);
            $Yesterday.setTextSize(13);
            $DayBeforeYesterday.setTextSize(13);
            $LastOneHour.setTextSize(13);
        }
    }


    /**
     * Map Related code
     */
    private boolean showVehicleInfoDialog(String mReportType) {

        TextView id_popup_history_labelid, id_popup_history_type_value,
                id_popup_to_time_label, id__popupto_date_label,
                id_popup_from_label,
                id_popup_from_time_label,id_txt_divider1,id_txt_divider2;
        Button id_history_selection_done, id_history_selection_cancel;
//        if (isFromList) {
//            marker_info_dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
//            marker_info_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        } else {
        marker_info_dialog = new Dialog(VehicleHistoryActivity.this);
//        }
        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        marker_info_dialog.setContentView(R.layout.history_popup);
        marker_info_dialog.setCancelable(false);
        //LinearLayout id_date_time_picker_layout;
        LinearLayout id_from_datetime_layout, id_to_datetime_layout, id_history_type_layout;
        ImageView id_history_vehicle_info_icon;
//        final TextView id_histroy_vehicleid, id_history_type_value, id_from_label, id_from_date, id_from_time, id_to_date, id_to_time;
       // Button id_history_selection_done, id_history_selection_cancel;

        id_from_datetime_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id_popup_from_datetime_layout);
        id_to_datetime_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id__popup_to_datetime_layout);
        id_history_type_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id_popup_history_type_labellayout);
       // id_date_time_picker_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id_popup_date_time_picker_layout);

        id_history_vehicle_info_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_popup_history_vehicle_info_icon);
        id_histroy_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_histroy_vehicleid);

        id_txt_divider1 = (TextView) marker_info_dialog.findViewById(R.id.divider_txt_1);
        id_txt_divider2 = (TextView) marker_info_dialog.findViewById(R.id.divider_txt_1);

       // id_history_type_value = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_type_value);
       // id_from_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_label);

       // id_from_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_date);
       // id_from_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_time);
       // id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_date);
       // id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time);
        //id_history_selection_done = (Button) marker_info_dialog.findViewById(R.id.id_popup_history_selection_done);
        //id_history_selection_cancel = (Button) marker_info_dialog.findViewById(R.id.id_popup_history_selection_cancel);


        id_popup_history_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_labelid);
        id_popup_history_type_value = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_type_value);
        id_popup_from_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_label);

        id_from_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_date);
        id_popup_from_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_time_label);
        id_from_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_time);
        id__popupto_date_label = (TextView) marker_info_dialog.findViewById(R.id.id__popupto_date_label);
        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_date);
        id_popup_to_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time_label);
        id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time);

        id_history_selection_done = (Button) marker_info_dialog.findViewById(R.id.id_popup_history_selection_done);
        id_history_selection_cancel = (Button) marker_info_dialog.findViewById(R.id.id_popup_history_selection_cancel);


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
       // int height = metrics.heightPixels;
      //  int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams history_for_layout = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        history_for_layout.width = width * 90 / 100;
        history_for_layout.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        history_for_layout.gravity = Gravity.CENTER;
        history_for_layout.setMargins(width*5/100,0,width*5/100,0);
        id_history_type_layout.setLayoutParams(history_for_layout);

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 80 / 100;
        backImageParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        backImageParams.gravity = Gravity.CENTER;

        id_popup_history_labelid.setLayoutParams(backImageParams);
        id_popup_history_type_value.setLayoutParams(backImageParams);

        LinearLayout.LayoutParams backImageParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams1.width = width * 40 / 100;
        backImageParams1.height = LinearLayout.LayoutParams.WRAP_CONTENT;

        id_popup_from_label.setLayoutParams(backImageParams1);
        id_from_date.setLayoutParams(backImageParams1);
        id_popup_from_time_label.setLayoutParams(backImageParams1);
        id_from_time.setLayoutParams(backImageParams1);

        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);
        id_popup_to_time_label.setLayoutParams(backImageParams1);
        id_to_time.setLayoutParams(backImageParams1);


        if (width >= 600) {
            id_popup_history_labelid.setTextSize(18);
            id_popup_history_type_value.setTextSize(17);
            id_from_date.setTextSize(17);
            id_from_time.setTextSize(17);
            id_to_date.setTextSize(17);
            id_to_time.setTextSize(17);
          //  $TxtOfflineVehicleValue.setTextSize(16);
        } else if (width > 501 && width < 600) {
            id_popup_history_labelid.setTextSize(17);
            id_popup_history_type_value.setTextSize(16);
            id_from_date.setTextSize(16);
            id_from_time.setTextSize(16);
            id_to_date.setTextSize(16);
            id_to_time.setTextSize(16);
           // $TxtOfflineVehicleValue.setTextSize(15);
        } else if (width > 260 && width < 500) {
            id_popup_history_labelid.setTextSize(16);
            id_popup_history_type_value.setTextSize(15);
            id_from_date.setTextSize(15);
            id_from_time.setTextSize(15);
            id_to_date.setTextSize(15);
            id_to_time.setTextSize(15);
           // $TxtOfflineVehicleValue.setTextSize(14);
        } else if (width <= 260) {
            id_popup_history_labelid.setTextSize(15);
            id_popup_history_type_value.setTextSize(14);
            id_from_date.setTextSize(14);
            id_from_time.setTextSize(14);
            id_to_date.setTextSize(14);
            id_to_time.setTextSize(14);
          //  $TxtOfflineVehicleValue.setTextSize(13);
        }


        id_histroy_vehicleid.setText(Constant.SELECTED_VEHICLE_ID);
        if (mReportType.equalsIgnoreCase("Custom")) {
           // id_date_time_picker_layout.setVisibility(View.VISIBLE);
            id_from_datetime_layout.setVisibility(View.VISIBLE);
            id_to_datetime_layout.setVisibility(View.VISIBLE);
            id_txt_divider1.setVisibility(View.VISIBLE);
            id_txt_divider2.setVisibility(View.VISIBLE);

            String today_date = cons.getDateandTime("today");
            id_from_date.setText(today_date);
            id_from_time.setText("00:00:00");
            id_to_date.setText(today_date);
            id_to_time.setText("23:59:00");

        } else if (mReportType.equalsIgnoreCase("Today")) {
            id_popup_history_type_value.setText("Today");
           // id_date_time_picker_layout.setVisibility(View.VISIBLE);
            id_from_datetime_layout.setVisibility(View.GONE);
            id_to_datetime_layout.setVisibility(View.GONE);
            id_txt_divider1.setVisibility(View.GONE);
            id_txt_divider2.setVisibility(View.GONE);
            String today_date = cons.getDateandTime("today");
            id_from_date.setText(today_date);
            id_from_time.setText("00:00:00");
            id_to_date.setText(today_date);
            id_to_time.setText("23:59:00");

        } else if (mReportType.equalsIgnoreCase("Yesterday")) {

            id_popup_history_type_value.setText("Yesterday");
            //id_date_time_picker_layout.setVisibility(View.VISIBLE);
            id_from_datetime_layout.setVisibility(View.GONE);
            id_to_datetime_layout.setVisibility(View.GONE);
            id_txt_divider1.setVisibility(View.GONE);
            id_txt_divider2.setVisibility(View.GONE);
            String yesterday_date = cons.getDateandTime("yesterday");
            id_from_date.setText(yesterday_date);
            id_from_time.setText("00:00:00");
            id_to_date.setText(yesterday_date);
            id_to_time.setText("23:59:00");

        } else if (mReportType.equalsIgnoreCase("Day Before Yesterday")) {

            id_popup_history_type_value.setText("Daybefore");
           // id_date_time_picker_layout.setVisibility(View.VISIBLE);
            id_from_datetime_layout.setVisibility(View.GONE);
            id_to_datetime_layout.setVisibility(View.GONE);
            id_txt_divider1.setVisibility(View.GONE);
            id_txt_divider2.setVisibility(View.GONE);
            String daybeforeyesterday_date = cons.getDateandTime("daybeforeyesterday");
            id_from_date.setText(daybeforeyesterday_date);
            id_from_time.setText("00:00:00");
            id_to_date.setText(daybeforeyesterday_date);
            id_to_time.setText("23:59:00");

        } else if (mReportType.equalsIgnoreCase("Last One Hour")) {
            id_popup_history_type_value.setText("Last one hour");
           // id_date_time_picker_layout.setVisibility(View.VISIBLE);
            id_from_datetime_layout.setVisibility(View.GONE);
            id_to_datetime_layout.setVisibility(View.GONE);
            id_txt_divider1.setVisibility(View.GONE);
            id_txt_divider2.setVisibility(View.GONE);
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int min = cal.get(Calendar.MINUTE);
            int past_hour = cal.get(Calendar.HOUR_OF_DAY) - 1;
            String lastonehour = cons.getDateandTime("today");
            id_from_date.setText(lastonehour);
            id_from_time.setText(new StringBuilder().append(pad(past_hour)).append(":").append(pad(min)).append(":").append("00"));
            id_to_date.setText(lastonehour);
            id_to_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(min)).append(":").append("00"));
        }

        /** listener to handle when history is canceled */
        id_history_selection_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opptionPopUp();
                marker_info_dialog.hide();
            }
        });

        /** listener to handle when history is selected */
        id_history_selection_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    marker_info_dialog.hide();
//                    id_date_time_picker_layout.setVisibility(View.GONE);
//                    id_map_info_layout.setVisibility(View.GONE);
                    if (map != null) {
                        map.clear();
                    }
                    String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&fromTime=" + id_from_time.getText().toString() + "&toDate=" + id_to_date.getText().toString() + "&toTime=" + id_to_time.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                    String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                    new GetVehicleHistory().execute(url);
                    new GetPOIInformation().execute(par);
                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
                    internet_toast.show();
                }
            }
        });

        /** listener to handle when From date is clicked in history selection*/
        id_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                from_date = true;
                to_date = false;
                from_time = false;
                to_time = false;
                showDialog(DATE_PICKER_ID);
            }
        });
        /** listener to handle when From Time is clicked in history selection*/
        id_from_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                from_date = false;
                to_date = false;
                from_time = true;
                to_time = false;
                showDialog(TIME_DIALOG_ID);
            }
        });
        /** listener to handle when To date is clicked in history selection*/
        id_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                from_date = false;
                to_date = true;
                from_time = false;
                to_time = false;
                showDialog(DATE_PICKER_ID);
            }
        });
        /** listener to handle when To Time is clicked in history selection*/
        id_to_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                from_date = false;
                to_date = false;
                from_time = false;
                to_time = true;
                showDialog(TIME_DIALOG_ID);
            }
        });


        marker_info_dialog.show();
        return true;
    }

    /**
     * Function to add 0 for time which is lesser that 10. Ex. 08.00, 05.00
     */
    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }

    /**
     * Date and Time picker code
     */

    public class GetVehicleHistory extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(VehicleHistoryActivity.this);
            progressDialog.show();
        }

        protected void onProgressUpdate(Integer... values) {
            progressDialog.show();
        }

        @Override
        public String doInBackground(String... urls) {
            String response_from_server = "";
            if (urls[0].equals("replay")) {
                response_from_server = sp.getString("historyplaybackdata", "");
            } else {
                try {
                    HttpConfig ht = new HttpConfig();
                    response_from_server = ht.httpGet(urls[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return response_from_server;
        }

        @Override
        public void onPostExecute(String result) {
            if (result != null && !result.isEmpty()) {
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("historyplaybackdata", result);
                editor.commit();
                historyPlayBack(result, progressDialog);
                historyRepeat = result;
            } else {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
//                header_layout.setVisibility(View.VISIBLE);
//                vehicle_info_layout.setVisibility(View.VISIBLE);
//                id_map_info_layout.setVisibility(View.GONE);
//                home_button_functionality();
                startActivity(new Intent(VehicleHistoryActivity.this, VehicleListActivity.class));
                finish();
                Toast.makeText(getApplicationContext(), "No History Data Available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Date and Time picker code
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar calNow = Calendar.getInstance();
        switch (id) {
            case DATE_PICKER_ID:
                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listner to date picker
                return new DatePickerDialog(this, datepickerListener, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH));
            case TIME_DIALOG_ID:
                // set time picker as current time
                return new TimePickerDialog(this, timePickerListener, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false);
        }
        return null;
    }

    public class GetPOIInformation extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        public String doInBackground(String... urls) {
            String response_from_server = "";
            try {

//                System.out.println("The POI url is :::::" + urls[0]);
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(urls[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        public void onPostExecute(String result) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("poidata", result);
            editor.commit();
            drawPoiPoints(result);
        }
    }

    public void drawPoiPoints(String poi_data) {
        if (poi_data != null && poi_data.length() != 0) {
            try {
                JSONObject parentobject = new JSONObject(poi_data);
                if (!parentobject.isNull("geoFence")) {
                    int vehicles_history_length;
                    final ArrayList<LatLng> points = new ArrayList<>();
                    JSONArray vehiclearray = parentobject.getJSONArray("geoFence");
                    vehicles_history_length = vehiclearray.length();
                    for (int z = 0; z < vehicles_history_length; z++) {
                        if (!vehiclearray.get(z).equals(null)) {
                            ParsingClass parse = new ParsingClass();
                            parse.getPoiInformation(vehiclearray.getJSONObject(z).toString());
                            double lat = parse.getpoiLatitude();
                            double lng = parse.getpoiLongitude();
                            LatLng position = new LatLng(lat, lng);

                            System.out.println("The poi points are :::::"+lat +" "+lng);

                            points.add(position);
                            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custompoimarker, null);
                            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleHistoryActivity.this, marker)));
                            Marker currentMarker = map.addMarker(markerOption);
                        }
                    }
                }
            } catch (JSONException je) {
                je.printStackTrace();
            }
        }
    }

    /**
     * Code to make the Marker icon adjustable for multiple screen
     */
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }


    public void historyPlayBack(final String result, final ProgressDialog dialog) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String totalrows = null;
                if (result != null && result.length() != 0) {
                    if (map != null) {
                        map.clear();
                    }
                    //  vehicle_info_layout.setVisibility(View.GONE);
                    // id_refresh_tracking_icon.setVisibility(View.GONE);
                    //  id_refresh_countdowntimer.setVisibility(View.GONE);
                    history_detail_layout.setVisibility(View.VISIBLE);
                    id_history_show_min_layout.setVisibility(View.GONE);
                    pause_icon.setVisibility(View.VISIBLE);
                    play_icon.setVisibility(View.GONE);
                    // header_layout.setVisibility(View.GONE);
                    if (zoom_location_marker != null) {
                        zoom_location_marker.clear();
                    }
                    try {
                        ParsingClass parsingClass = new ParsingClass();
                        parsingClass.historyParse(result);
                        int vehicles_history_length;
                        points = new ArrayList<LatLng>();
                        speedlist = new ArrayList<String>();
                        datetimelist = new ArrayList<String>();
                        distancelist = new ArrayList<String>();
                        if (!points.isEmpty()) {
                            points.clear();
                            //  directionlist.clear();
                            speedlist.clear();
                            datetimelist.clear();
                            distancelist.clear();
                        }
                        String vehicle_short_name;
                        PolylineOptions options = new PolylineOptions().width(8);
                        PolylineOptions overspeed_options = new PolylineOptions().width(4).geodesic(true);
                        vehicle_short_name = parsingClass.getshortname();
                        id_show_history_vehicleid.setText((parsingClass.getshortname().equals("null") ? "No Data" : parsingClass.getshortname()));
                        history_distance_covered_value.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance() + " KMS"));
                        histroy_start_time_value.setText((parsingClass.getfromDateTime().equals("null") ? "No Data" : parsingClass.getfromDateTime()));
                        history_endtime_value.setText((parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));
                        totalrows = parsingClass.gettotalRows();
                        history_timetaken_value.setText(cons.getVehicleTime(parsingClass.gettotalRunningTime()));
                        /** Parsing all the history data*/
                        try {
                            JsonFactory f = new MappingJsonFactory();
                            JsonParser historyjsonParser = f.createJsonParser(result);
                            JsonToken current;
                            current = historyjsonParser.nextToken();
                            if (current != JsonToken.START_OBJECT) {
                                return;
                            }
                            while (historyjsonParser.nextToken() != JsonToken.END_OBJECT) {
                                String fieldName = historyjsonParser.getCurrentName();
                                if (fieldName.equals("history4Mobile")) {
                                    current = historyjsonParser.nextToken();
                                    if (current == JsonToken.START_ARRAY) {
                                        int current_node_index = 0;
                                        while (historyjsonParser.nextToken() != JsonToken.END_ARRAY) {
                                            // read the record into a tree model,
                                            // this moves the parsing position to the end of it
                                            JsonNode node = historyjsonParser.readValueAsTree();
                                            // And now we have random access to everything in the object
                                            if (!node.isNull()) {
                                                parsingClass.historyobjectparsing(node);
                                                double lat = parsingClass.getCurr_lat();
                                                double lng = parsingClass.getCurr_long();
                                                LatLng position = new LatLng(lat, lng);
                                                points.add(position);
                                                MarkerOptions markerOption = new MarkerOptions().position(position);
                                                options.add(position);
                                                options.color(getResources().getColor(R.color.history_polyline_color));
                                                speedlist.add(parsingClass.getSpeed());
                                                datetimelist.add(parsingClass.getlastseen());
                                                distancelist.add(parsingClass.getDistanCovered());
                                                View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
                                                ImageView id_history_marker_icon = (ImageView) marker.findViewById(R.id.id_history_marker_icon);
                                                ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
                                                ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
                                                id_history_marker_icon.setVisibility(View.VISIBLE);
                                                id_custom_marker_icon.setVisibility(View.GONE);
                                                id_vehicle_in_marker.setVisibility(View.GONE);
                                                Marker currentMarker = null;
                                                if (current_node_index == 0) {
                                                    id_history_marker_icon.setImageResource(R.drawable.startflag);
                                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleHistoryActivity.this, marker)));
                                                    currentMarker = map.addMarker(markerOption);
                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + "-" + "," + Double.toString(parsingClass.getCurr_lat()) + "," + Double.toString(parsingClass.getCurr_long()) + "," + "start");
                                                    new GetAddressTask(history_start_location_value).execute(parsingClass.getCurr_lat(), parsingClass.getCurr_long(), 2.0);
                                                } else if (current_node_index == Integer.parseInt(totalrows) - 1) {
                                                    id_history_marker_icon.setImageResource(R.drawable.endflag);
                                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleHistoryActivity.this, marker)));
                                                    currentMarker = map.addMarker(markerOption);
                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + "-" + "," + Double.toString(parsingClass.getCurr_lat()) + "," + Double.toString(parsingClass.getCurr_long()) + "," + "end");
                                                    new GetAddressTask(history_end_location_value).execute(parsingClass.getCurr_lat(), parsingClass.getCurr_long(), 3.0);
                                                } else if (parsingClass.getPosition().equalsIgnoreCase("P")) {
                                                    id_history_marker_icon.setImageResource(R.drawable.id_parker_icon);
                                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleHistoryActivity.this, marker)));
                                                    currentMarker = map.addMarker(markerOption);
                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + parsingClass.getparked_time() + "," + Double.toString(parsingClass.getCurr_lat()) + "," + Double.toString(parsingClass.getCurr_long()) + "," + "parked");
                                                } else {
                                                    markerOption.alpha(0);
                                                    currentMarker = map.addMarker(markerOption);
                                                    currentMarker.setVisible(false);
                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + "-" + "," + "-" + "," + "-" + "," + "idle");
                                                }
                                                zoom_location_marker.add(currentMarker);
                                            }
                                            current_node_index++;
                                        }
                                    }
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                        /** Parsing all the history data*/
                        map.addPolyline(options);
                        //progressDialog.dismiss();
                        if (!zoom_location_marker.isEmpty()) {
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            for (int marker = 0; marker < zoom_location_marker.size(); marker++) {
                                builder.include(zoom_location_marker.get(marker).getPosition());
                            }
                            LatLngBounds bounds = builder.build();
                            int padding = 100; // offset from edges of the map in pixels
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                            map.animateCamera(cu);
                        }
                        pause = false;
                        if (points.size() > 1) {
                            history_seekbar.setMax(points.size());
                            replay_icon.setVisibility(View.GONE);
                            speed_seekbar.setVisibility(View.GONE);
                            history_seekbar.setVisibility(View.VISIBLE);
                        } else {
                            history_seekbar.setMax(points.size());
                            replay_icon.setVisibility(View.GONE);
                            play_icon.setVisibility(View.GONE);
                            pause_icon.setVisibility(View.GONE);
                            speed_seekbar.setVisibility(View.GONE);
                            history_seekbar.setVisibility(View.GONE);
                        }
                        backButtonPressed = false;
                        speed_seekbar.setProgress(50);
                        marker_loop(0);
                    } catch (Exception je) {
                        je.printStackTrace();
                    } finally {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                } else {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(getApplicationContext(), "No History Data Available for the Selected Intervals", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void show_marker(final int loop, int value) {
      //  Toast.makeText(getApplicationContext(),"Hi loop is ::::"+loop,Toast.LENGTH_SHORT).show();
        final int p = loop;
        final View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
        final LatLng getpoints = points.get(loop);
        final String speed = speedlist.get(loop);
        final String datetim = datetimelist.get(loop);
        final String distance = distancelist.get(loop);
        try {
            handler1.postDelayed(runnable1 = new Runnable() {
                @Override
                public void run() {
                    if (pause) {
                        handler1.removeCallbacksAndMessages(null);
                        if (!backButtonPressed && !isSpeedChanged) {
                            backButtonPressed = false;
                            current_address__dialog = new Dialog(VehicleHistoryActivity.this);
                            current_address__dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            current_address__dialog.setContentView(R.layout.custom_marker_dialog);
                            current_address__dialog.setCancelable(true);
                            TextView id_historyplayback_parkedtime_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_parkedtime_text_labelid);
                            TextView id_view_parkedtime = (TextView) current_address__dialog.findViewById(R.id.id_view_parkedtime);
                            TextView id_historyplayback_parkedduration_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_parkedduration_text_labelid);
                            TextView id_view_parkedduration = (TextView) current_address__dialog.findViewById(R.id.id_view_parkedduration);
                            TextView id_historyplayback_addres_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_addres_text_labelid);
                            TextView id_view_address = (TextView) current_address__dialog.findViewById(R.id.id_view_address);
                            id_historyplayback_parkedtime_text_labelid.setVisibility(View.GONE);
                            id_view_parkedtime.setVisibility(View.GONE);
                            id_historyplayback_parkedduration_text_labelid.setVisibility(View.GONE);
                            id_view_parkedduration.setVisibility(View.GONE);
                            id_historyplayback_addres_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
                            id_view_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
                            new GetAddressTask(id_view_address).execute(getpoints.latitude, getpoints.longitude, 6.0);
                            current_address__dialog.show();
                        }
                        return;
                    } else {
                        if (seekbarchanged) {
                            seekbarchanged = false;
                            handler1.removeCallbacksAndMessages(null);
                            return;
                        } else {
                            history_seekbar.setProgress(loop);
                            id_history_speed_info_value.setText(speed);
                            id_history_distance_value.setText(distance);
                            id_history_direction_value.setText(cons.getTimefromserver(datetim));
                            history_vehicle_info_layout.setVisibility(View.VISIBLE);
                            if (current_users_location_point.isEmpty()) {
                                CameraPosition INIT = new CameraPosition.Builder().target(getpoints).zoom(history_zoom_level).build();
                                Marker ad = null;
                                ad = map.addMarker(new MarkerOptions().position(getpoints).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.playback_nav_icon)));
                                current_users_location_point.add(ad);
                            } else {
                                for (int m = 0; m < current_users_location_point.size(); m++) {
                                    Marker removemarker = current_users_location_point.get(m);
                                    removemarker.remove();
                                }
                                current_users_location_point.clear();
                                if (p == points.size() - 1) {
                                    speed_seekbar.setVisibility(View.GONE);
                                    history_seekbar.setVisibility(View.GONE);
                                    replay_icon.setVisibility(View.VISIBLE);
                                    id_history_show_min_layout.setVisibility(View.VISIBLE);
                                    history_detail_layout.setVisibility(View.VISIBLE);
                                    pause_icon.setVisibility(View.GONE);
                                }
                                CameraPosition INIT = new CameraPosition.Builder().target(getpoints).zoom(history_zoom_level).build();
                                Marker ad = null;
                                ad = map.addMarker(new MarkerOptions().position(getpoints).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.playback_nav_icon)));
                                current_users_location_point.add(ad);
                            }
                        }
                    }
                }
            }, (iLoopDelay * 10) * value);
        } catch (Exception e) {
        }
    }


    private class GetAddressTask extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTask(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {
            getlat = params[0];
            getlng = params[1];
            address_type = params[2];
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(params[0], params[1], 1);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(params[0]) +
                        " , " +
                        Double.toString(params[1]) +
                        " passed to address service";
                e2.printStackTrace();
                return errorString;
            } catch (NullPointerException np) {
                // TODO Auto-generated catch block
                np.printStackTrace();
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                String addressText = null;
                StringBuffer addr = new StringBuffer();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressText = address.getAddressLine(i);
                    addr.append(addressText + ",");
                }
                // Return the text
                return addr.toString();
            } else {
                return "No address found";
            }
        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);
            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {
                    /***
                     * 1.0 - vehicles current address when a vehicle is clicked
                     * 2.0 - vehicles origin address in history selection
                     * 3.0 - vehicles destination address in history selection
                     * 6.0 - vehicles current address when pause button is clicked in history selection
                     */

//                    System.out.println("The address is ::::" + result);

                    if (address_type == 1.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("address", result);
                        editor.commit();
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 2.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("vehicle_origin_address", result);
                        editor.commit();
                        history_start_location_value.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 3.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("vehicle_destination_address", result);
                        editor.commit();
                        history_end_location_value.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 4.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("current_vehicle_address", result);
                        editor.commit();
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 5.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 6.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    }
                }
            } else {
                Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
                empty_fav.show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(VehicleHistoryActivity.this,VehicleListActivity.class));
        finish();
    }
}
