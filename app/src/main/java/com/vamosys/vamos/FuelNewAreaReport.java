package com.vamosys.vamos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Area;
import com.anychart.core.ui.Crosshair;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.HoverMode;
import com.anychart.enums.MarkerType;
import com.anychart.enums.ScaleStackMode;
import com.anychart.enums.TooltipDisplayMode;
import com.anychart.graphics.vector.Stroke;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.vamosys.model.FuelFillDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HorizontalScrollView;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.UnCaughtException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FuelNewAreaReport extends AppCompatActivity implements View.OnClickListener, OnChartGestureListener,
        OnChartValueSelectedListener, OnMapReadyCallback {
    static ListView lv;
    TextView mTxtNoRecord, mTxtVehicleName, mTxtTotalFuelConsume, mTxtTripDistance, mTxtTotalFuelFill,mTxtstartfuel,mTxtendfuel,mTxtTotalMileage, mTxtOdoStart, mTxtOdoEnd;
    ImageView mCalendarImage, mImgMapViewClose;
    static LinearLayout mFuelDataMapLayout;
    LinearLayout mGoogleMapLayout;
    boolean isOsmEnabled = true;
    Toolbar mToolbar;
    ImageView $ChangeView;
    String intervalue,spinnervalue;

    //    ImageView mImgDataViewChange;
    ConnectionDetector cd;
    SharedPreferences sp;
    static Const cons;

    static int width;
    static int height;
    boolean isHeaderPresent = false, mIsLineChartEnabled = false;
    private List<FuelFillDto> mFuelFillData = new ArrayList<FuelFillDto>();

    TableAdapter adapter;

    Dialog marker_info_dialog;


    //    String mTotalFuelValue = null;
    private static GoogleMap map;
    //    ImageView $ChangeView;
    boolean mIsMapPresent = false;
    String mStrUtcFromDate, mStrUtcToDate;
    String mSELECTED_MAP_TYPE = "Normal";
    static double mLatitude = 0;
    static double mLongitude = 0;
    static float vehicle_zoom_level = 15.5F;
    static Context mFuelContext;
    String tanksize;

    SimpleDateFormat timeFormatFuelFillReport = new SimpleDateFormat(
            "HH:mm:ss");

    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

    View bottomSheet;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;

    private int year, month, day;
    static final int TIME_DIALOG_ID = 1111;
    static final int DATE_DIALOG_ID = 2222;
    private int hour;
    private int minute;
    Spinner spinnerInterval;
    Button mBtnSubmit;
    TextView mTxtFromDate, mTxtEndDate,mTxtstart,mTxtend, mTxtFromTime, mTxtEndTime;
    String mStartDate, mEndDate, mStartTime, mStartTimeValue, mStartTimeFuelReport, mEndTimeFuelReport, mEndDateValue, mEndTime, mEndTimeValue, mIntervalSpinnerSelectedValue;
    boolean isFromTime = false, isFromDate = false, isMapPresent = false;
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    ArrayList<String> mIntervalList;
    ArrayList<String> mSpinnerList;
    int mIntervalSpinnerSelectedPos = 0;
    private LineChart mChart;
    LinearLayout mChartLayout;
    AnyChartView anyChartView;
    ArrayList<Integer> xAxis;

    double mTotalFuelConsume,myvlue, mTotalFuelFill, mTripDistance, mOdoStart, mOdoEnd, mTotalMileage;

    //OSM related
    MapView mapview;
    private IMapController mapController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(
                FuelNewAreaReport.this));
        setContentView(R.layout.activity_fuel_fill_new_report);
         anyChartView = findViewById(R.id.any_chart_view);
        cons = new Const();
        mIntervalList = new ArrayList<String>();
        mSpinnerList = new ArrayList<String>();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.fuel_report));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        mFuelContext = getApplicationContext();

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
            isOsmEnabled = true;
        } else {
            isOsmEnabled = false;
        }

        intervalue = sp.getString("intervalvalue","");
        spinnervalue =sp.getString("spinnervalue","");

        mIntervalSpinnerSelectedValue = intervalue;

        init();

        mStartDate = Const.getCurrentDate();
        mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));


        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);


        mFromHourValue = 00;
        mFromMinuteValue = 00;
        mStartTimeValue = timeFormatShow.format(cal.getTime());
        mStartTime = timeFormat.format(cal.getTime());
        mStartTimeFuelReport = timeFormatFuelFillReport.format(cal.getTime());

        final Calendar c = Calendar.getInstance();
        // Current Hour
        hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        minute = c.get(Calendar.MINUTE);
        isFromTime = false;
        updateTime(hour, minute);


        Bundle extras = getIntent().getExtras();
        if (extras == null) {
        } else {

            mIsLineChartEnabled = extras.getBoolean("is_chart_enabled", false);

        }


        mIntervalList = intervalSpinnerDataValue();
        mSpinnerList = intervalSpinnerValue();

        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        adapter = new TableAdapter(this);

        spinnerInterval
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

                {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mIntervalSpinnerSelectedPos = position;

                        SharedPreferences.Editor editor = sp.edit();
                        if (position != 0) {
                            editor.putString("intervalvalue",String.valueOf(mIntervalList.get(position)));
                            editor.putString("spinnervalue",mSpinnerList.get(position));
                            mIntervalSpinnerSelectedValue = String.valueOf(mIntervalList.get(position));
                        } else {
                            mIntervalSpinnerSelectedValue = intervalue;
                        }
                        editor.commit();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

        mToolbar.setNavigationOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(FuelNewAreaReport.this, MapMenuActivity.class));
                finish();


            }
        });

//        System.out.println("Hi chart enabled " + Constant.mFuelIsChartView + " " + mIsLineChartEnabled);
        new getKmsSummaryData().execute();



    }

    public void init() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        $ChangeView = (ImageView) findViewById(R.id.temperature_data_map_ViewIcon);
        $ChangeView.setOnClickListener(this);

        mChart = (LineChart) findViewById(R.id.chart);
        mChart.zoom(4, 1, 1, 1);
        mChartLayout = (LinearLayout) findViewById(R.id.chart_layout);
//        $ChangeView = (ImageView) findViewById(R.id.fuel_fill_data_map_ViewIcon);


        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);


        mFuelDataMapLayout = (LinearLayout) findViewById(R.id.temperature_data_map_view_layout);
        mFuelDataMapLayout.setVisibility(View.GONE);

        lv = (ListView) findViewById(R.id.tstoppage_report_listView1);
//        mImgDataViewChange = (ImageView) findViewById(R.id.fuelFill_report_change_data_view);
//        mImgDataViewChange.setVisibility(View.GONE);

        mTxtVehicleName = (TextView) findViewById(R.id.selected_vehicle_txt);
        mTxtVehicleName.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
//        mTxtVehicleNameValue = (TextView) findViewById(R.id.fuelFill_vehicle_name_value_txt);
//        mTxtVehicleNameValue.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);

        mTxtNoRecord = (TextView) findViewById(R.id.report_no_record_txt);

        spinnerInterval = (Spinner) findViewById(R.id.spinner_interval);

        mTxtFromDate = (TextView) findViewById(R.id.txt_start_date_value);
        mTxtFromTime = (TextView) findViewById(R.id.txt_start_time_value);
        mTxtEndDate = (TextView) findViewById(R.id.txt_end_date_value);
        mTxtEndTime = (TextView) findViewById(R.id.txt_end_time_value);
        mTxtstart = (TextView) findViewById(R.id.txt_date_start);
        mTxtend = (TextView) findViewById(R.id.txt_date_end);
        mBtnSubmit = (Button) findViewById(R.id.btn_done);

        mTxtTotalFuelConsume = (TextView) findViewById(R.id.txt_total_fuel_consume);
        mTxtTripDistance = (TextView) findViewById(R.id.txt_trip_distance);
        mTxtTotalFuelFill = (TextView) findViewById(R.id.txt_total_fuel_fill);
        mTxtstartfuel = (TextView) findViewById(R.id.txt_start_fuel);
        mTxtendfuel = (TextView) findViewById(R.id.txt_end_fuel);
        mTxtOdoStart = (TextView) findViewById(R.id.txt_odo_start);
        mTxtOdoEnd = (TextView) findViewById(R.id.txt_odo_end);


        mTxtFromTime.setOnClickListener(this);
        mTxtFromDate.setOnClickListener(this);
        mTxtEndDate.setOnClickListener(this);
        mTxtEndTime.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mTxtTotalFuelFill.setOnClickListener(this);


        mChartLayout.setVisibility(View.GONE);

        lv.setVisibility(View.VISIBLE);
        if (isOsmEnabled) {
            $ChangeView.setVisibility(View.GONE);
            mGoogleMapLayout.setVisibility(View.GONE);
            rl.setVisibility(View.VISIBLE);
            mapview = new MapView(this);
            mapview.setTilesScaledToDpi(true);
            rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                    RelativeLayout.LayoutParams.FILL_PARENT));

            mapview.setBuiltInZoomControls(false);
            mapview.setMultiTouchControls(true);
            mapController = mapview.getController();
            mapController.setZoom(8);
            mapview.getOverlays().clear();
            mapview.invalidate();
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
        } else {
            Log.d("googlemap","success");
            $ChangeView.setVisibility(View.VISIBLE);
            mGoogleMapLayout.setVisibility(View.VISIBLE);
            rl.setVisibility(View.GONE);

            FragmentManager myFragmentManager = getSupportFragmentManager();
            SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                    .findFragmentById(R.id.temperature_data_map);
            myMapFragment.getMapAsync(this);
        }

    }

    private void updateTime(int hours, int mins) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, mins);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        String aTime = timeFormatShow.format(cal.getTime());

        String aTime2 = timeFormat.format(cal.getTime());
        String aTime3 = timeFormatFuelFillReport.format(cal.getTime());

        SimpleDateFormat hourFormat = new SimpleDateFormat(
                "HH");
        SimpleDateFormat minuteFormat = new SimpleDateFormat(
                "mm");

        if (isFromTime) {
            mStartTime = aTime2;
            mStartTimeValue = aTime;

            mStartTimeFuelReport = aTime3;


            mFromHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
            mFromMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));
            mTxtFromTime.setText(mStartTimeValue);
        } else {
            mEndTime = aTime2;
            mEndTimeValue = aTime;
            mEndTimeFuelReport = aTime3;
            mTxtEndTime.setText(mEndTimeValue);

            mToHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
            mToMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));

        }


    }
    private ArrayList<String> intervalSpinnerValue() {
        ArrayList<String> mHrList = new ArrayList<String>();
        mHrList.add("Interval");
        mHrList.add("1 mins");
        mHrList.add("2 mins");
        mHrList.add("5 mins");
        mHrList.add("10 mins");
        mHrList.add("15 mins");
        mHrList.add("30 mins");

        return mHrList;
    }

    private ArrayList<String> intervalSpinnerDataValue() {
        ArrayList<String> mHrList = new ArrayList<String>();
        mHrList.add("0");
        mHrList.add("1");
        mHrList.add("2");
        mHrList.add("5");
        mHrList.add("10");
        mHrList.add("15");
        mHrList.add("30");

        return mHrList;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        System.out.println("Hi onResume called ");
        if (Constant.mFuelStoredFromDate != null) {
            mStartDate = Constant.mFuelStoredFromDate;
        }
        if (Constant.mFuelStoredToDate != null) {
            mEndDate = Constant.mFuelStoredToDate;
        }
        if (Constant.mFuelStoredFromTime != null) {
            mStartTime = Constant.mFuelStoredFromTime;
        }
        if (Constant.mFuelStoredToTime != null) {
            mEndTime = Constant.mFuelStoredToTime;
        }
        if (Constant.mFuelInterval != null) {
            mIntervalSpinnerSelectedValue = Constant.mFuelInterval;
        }

        setBottomLayoutData();

        if (cd.isConnectingToInternet()) {


            mFuelFillData.clear();
            new getKmsSummaryData().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }


    }

    private void setBottomLayoutData() {
        mTxtFromDate.setText(mStartDate);
        mTxtEndDate.setText(mEndDate);
        mTxtFromTime.setText(mStartTimeValue);
        mTxtEndTime.setText(mEndTimeValue);
        setIntervalSpinnerValue();
        mIntervalList = intervalSpinnerDataValue();

        Constant.mFuelStoredFromDate = null;
        Constant.mFuelStoredToDate = null;
        Constant.mFuelStoredFromTime = null;
        Constant.mFuelStoredToTime = null;
        Constant.mFuelInterval = null;

    }

    private void  setIntervalSpinnerValue() {
        ArrayAdapter dataAdapter = new ArrayAdapter(this, R.layout.spinner_row, intervalSpinnerValue());

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Log.d("dataAdapter",""+intervalSpinnerDataValue());

        spinnerInterval.setAdapter(dataAdapter);


        int spinnerPosition = dataAdapter.getPosition(spinnervalue);

//set the default according to value
        spinnerInterval.setSelection(spinnerPosition);


        //spinnerInterval.setSelection((mIntervalSpinnerSelectedPos));
        Log.d("dataAdapter1",""+mIntervalSpinnerSelectedPos);
    }
    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the state of item position

        System.out.println("Hi saved instance state " + mIsLineChartEnabled);
        Constant.mFuelStoredFromDate = mStartDate;
        Constant.mFuelStoredToDate = mEndDate;
        Constant.mFuelStoredFromTime = mStartTime;
        Constant.mFuelStoredToTime = mEndTime;
        Constant.mFuelInterval = mIntervalSpinnerSelectedValue;
        Constant.mFuelIsChartView = mIsLineChartEnabled;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.txt_start_date_value:
                isFromDate = true;
//                showDialog(DATE_DIALOG_ID);

                showDateDialog();

                break;
            case R.id.txt_start_time_value:
//                showDialog(DATE_DIALOG_FROMID);
                isFromTime = true;
                //   showDialog(TIME_DIALOG_ID);

                showTimeDialog();
                break;
            case R.id.txt_end_date_value:
//                createNumberDialog(1);
                isFromDate = false;
//                showDialog(DATE_DIALOG_ID);


                showDateDialog();
//                show(1);

                break;
            case R.id.txt_end_time_value:
//                createNumberDialog(0);
                isFromTime = false;
                // showDialog(TIME_DIALOG_ID);

                showTimeDialog();
//                show(0);
                break;
            case R.id.txt_total_fuel_fill:
//                createNumberDialog(0);
                if (mTotalFuelFill > 0) {

                    Intent in = new Intent(FuelNewAreaReport.this, FuelFillReport.class);
                    in.putExtra("start_date", mStartDate);
                    in.putExtra("start_time", mStartTimeFuelReport);
                    in.putExtra("end_date", mEndDate);
                    in.putExtra("end_time", mEndTimeFuelReport);
                    in.putExtra("is_chart_enabled", mIsLineChartEnabled);
                    startActivity(in);
                    //  finish();

                }
                break;

            case R.id.btn_done:

                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                if (cd.isConnectingToInternet()) {
                    mTxtstart.setVisibility(View.VISIBLE);
                    mTxtend.setVisibility(View.VISIBLE);
                    mTxtstart.setText("Start date : " + mStartDate);
                    mTxtend.setText("End date : " + mEndDate);
                    adapter = null;
                    adapter = new TableAdapter(FuelNewAreaReport.this);
//     String url = Const.API_URL + "/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    mFuelFillData.clear();
                    lv.setAdapter(null);

                    new getKmsSummaryData().execute();
                    //new getfuelmachinery().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }
                new getfuelmachinery().execute();
//                new CameraReportIamgeListActivtiy.getCameraReport().execute();
                break;
        }


    }

    private void showTimeDialog() {
        if (isFromTime) {
            hour = mFromHourValue;
            minute = mFromMinuteValue;
        } else {
            hour = mToHourValue;
            minute = mToMinuteValue;
        }


        TimePickerDialog dialog2 = new TimePickerDialog(this, timePickerListener, hour, minute,
                false);
        dialog2.show();
    }
    private void showDateDialog() {
        if (isFromDate) {

            String[] dateArray = mStartDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        } else {

            String[] dateArray = mEndDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());

        }
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
        dialog.show();
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            if (isFromDate) {
                mStartDate = String.valueOf(mDate2);
                //  mStartTimeValue=String.valueOf(mDate2);
                mTxtFromDate.setText(mDate2);
            } else {
                mEndDate = String.valueOf(mDate2);

                mTxtEndDate.setText(mDate2);
            }
//            mFromDate = String.valueOf(mDate);
//            mFromDateTxtValue = String.valueOf(mDate2);
//            mTxtDate.setText(mDate2);
            // }
        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);


        }

    };


    @Override
    public void onChartGestureStart(MotionEvent me,
                                    ChartTouchListener.ChartGesture
                                            lastPerformedGesture) {

//        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me,
                                  ChartTouchListener.ChartGesture
                                          lastPerformedGesture) {

//        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            // or highlightTouch(null) for callback to onNothingSelected(...)
            mChart.highlightValues(null);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);

    }

    public class TableAdapter extends BaseAdapter {

        // private static final String TAG = "TableAdapter";

        private Context mContext;

        private String[] mHeader;


        private int mCurrentScroll;

        private int[] mColResources = {R.id.fuelfill_date_textView, R.id.fuelfill_fuel_ltr_textView,
                R.id.fuelfill_adds_textView, R.id.fuelfill_speed_textView,
                R.id.fuelfill_odo_distance_textView};

        public TableAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return mFuelFillData != null ? mFuelFillData.size() : 0;

        }

        @Override
        public Object getItem(int position) {
            return mFuelFillData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            Log.d("Count",""+getCount());

            HorizontalScrollView view = null;
//            LinearLayout view = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                view = (HorizontalScrollView) inflater.inflate(
                        R.layout.fuelfill_list_item_table_row_new, parent, false);


            } else {
                view = (HorizontalScrollView) convertView;
//                view = (LinearLayout) convertView;
            }

            view.setScrollX(mCurrentScroll);

            if (position % 2 == 0) {
                view.setBackgroundColor(Color.WHITE);
            } else {
                view.setBackgroundColor(Color.LTGRAY);
            }

            final FuelFillDto data = mFuelFillData.get(position);

//            for (int i = 0; i < mColResources.length; i++) {
            TextView col1 = (TextView) view.findViewById(mColResources[0]);
            col1.setText(cons.getAcReportTime(data.getStartTime()));
            TextView col2 = (TextView) view.findViewById(mColResources[1]);
            col2.setText(String.valueOf(data.getFuelLtr()));
            TextView col3 = (TextView) view.findViewById(mColResources[2]);
            SpannableString content = new SpannableString("Link");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            col3.setText(content);
            // col3.setText(String.valueOf(data.getAddress()));
            col3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFuelDataMapLayout.setVisibility(View.VISIBLE);
                    lv.setVisibility(View.GONE);
                    String addres = data.getAddress();

                    if (addres.equalsIgnoreCase("_")){
                        Log.d("addres",""+addres);
                    } else{
                        String[] value1 = addres.split(",");
                        mLatitude = Double.valueOf(value1[0]);
                        mLongitude = Double.valueOf(value1[1]);
                        Log.d("latlng",""+mLatitude);
                        Log.d("latlng1",""+mLongitude);
                    }

                    if (isOsmEnabled) {
                        setupOsmMap();
                    } else {
                        setupGoogleMap();
                    }

//                    mLatitude = Double.valueOf(mFuelFillData.get(position).getLatitude());
//                    mLongitude = Double.valueOf(mFuelFillData.get(position).getLongitude());
                }
            });

            TextView col4 = (TextView) view.findViewById(mColResources[3]);
            col4.setText(String.valueOf(data.getNewSpeed()));

            TextView col5 = (TextView) view.findViewById(mColResources[4]);
            col5.setText(String.valueOf(data.getOdoMeterReading()));


            LinearLayout.LayoutParams col1Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col1Params.width = width * 35 / 100;
//            col1Params.height = height * 7 / 100;
            col1.setLayoutParams(col1Params);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            LinearLayout.LayoutParams col245Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col245Params.width = width * 30 / 100;
//            col245Params.height = height * 7 / 100;
            col2.setLayoutParams(col245Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);

            col4.setLayoutParams(col245Params);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(col245Params);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams col3Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col3Params.width = width * 40 / 100;
//            col3Params.height = height * 7 / 100;
            col3.setLayoutParams(col3Params);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);


            return view;
        }

        public View getHeaderView(ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            HorizontalScrollView view = (HorizontalScrollView) inflater
                    .inflate(R.layout.fuelfill_list_item_table_header_new, parent, false);


            TextView col1 = (TextView) view.findViewById(R.id.fuelsumm__textView_header);
            TextView col2 = (TextView) view.findViewById(R.id.fuelfill_fuel_ltr_textView_header);
            TextView col3 = (TextView) view.findViewById(R.id.fuelfill_adds_textView_header);
            TextView col4 = (TextView) view.findViewById(R.id.fuelfill_odo_distance_textView_header);
            TextView col5 = (TextView) view.findViewById(R.id.fuelfill_speed_textView_header);
//            SpannableString content = new SpannableString("Link");
//            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//            col3.setText(content);


            LinearLayout.LayoutParams col1Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col1Params.width = width * 35 / 100;
//            col1.setLayoutParams(col1Params);
//            col1.setPadding(width * 2 / 100, 0, 0, 0);
//            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            LinearLayout.LayoutParams col245Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col245Params.width = width * 30 / 100;
//            col245Params.height = height * 7 / 100;
            col2.setLayoutParams(col245Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);

            col4.setLayoutParams(col245Params);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(col245Params);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams col3Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            col3Params.width = width * 40 / 100;
//            col3Params.height = height * 7 / 100;
            col3.setLayoutParams(col3Params);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);

            return view;
        }



        public void setData(List<FuelFillDto> data) {
            mFuelFillData = data;
            notifyDataSetChanged();
        }

    }

    public void setupOsmMap() {

//        if (map != null) {
//            map.clear();
//        }

        if (mapview != null) {
            InfoWindow.closeAllInfoWindowsOn(mapview);
            mapview.getOverlays().clear();
            mapview.invalidate();
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
        }



        View markerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_image_view, null);
        ImageView mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);
        mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
//        animateMarker.setIcon(createDrawableFromViewNew(AcReport.this, markerView));
        org.osmdroid.views.overlay.Marker osmMarker = new org.osmdroid.views.overlay.Marker(mapview);
        osmMarker.setPosition(new GeoPoint(mLatitude, mLongitude));
        osmMarker.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
//        osmMarker.setTitle(Constant.SELECTED_VEHICLE_SHORT_NAME);
        osmMarker.setIcon(Constant.createDrawableFromViewNew(FuelNewAreaReport.this, markerView));
//        InfoWindow infoWindow = new FuelFillNewReport().MyInfoWindow(R.layout.map_info_txt_layout, mapview, addressValue);
//        osmMarker.setInfoWindow(infoWindow);
//        osmMarker.showInfoWindow();

        final GeoPoint newPos = new GeoPoint(mLatitude, mLongitude);

        new Handler(Looper.getMainLooper()).post(
                new Runnable() {
                    public void run() {
                        mapview.getController().setCenter(newPos);
                    }
                }
        );

        mapview.getOverlays().add(osmMarker);
        mapview.invalidate();
    }

    public void setupGoogleMap() {

        if (map != null) {
            map.clear();
        }

        View marker = null;

        marker = ((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
        id_vehicle_in_marker.setVisibility(View.GONE);

        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        Marker currentMarker = map.addMarker(markerOption);
        currentMarker.showInfoWindow();



    }
    private class CustomDataEntry extends ValueDataEntry {
        CustomDataEntry(String x, Number value, Number value2, Number value3, Number value4, Number value5) {
            super(x, value);
            setValue("value2", value2);
            setValue("value3", value3);
            setValue("value4", value4);
            setValue("value5", value5);
        }
    }

    private class getKmsSummaryData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                //  Const constaccess = new Const();

                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
                Log.d("fromtrim",""+string_from_date);
                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();
                Log.d("totrim",""+string_to_date);

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                Date d = f.parse(string_from_date);

//                System.out.println("The from date after converted  :::::" + d);

                long fromMilliSeconds = d.getTime();
//                System.out.println("The from time in millis :::::" + fromMilliSeconds);
                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
//                long fromMilliSeconds =1470767400000;
//                long toMilliSeconds=1471685033000;


                //  System.out.println("The to time in millis :::::" + toMilliSeconds);


//                result = constaccess
//                        .sendGet(Const.API_URL + "/mobile/getExecutiveReport?groupId=" + mSelectedGroup + "&fromDate=" + mStrFromDate + "&toDate=" + mStrToDate + "&userId=" + mUserId, 30000);
                HttpConfig ht = new HttpConfig();
//                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDropFillReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=1&fromDate=" + mStrFromDate + "&fromTime=" + mStrFromTime + "&toDate=" + mStrToDate + "&toTime=" + mStrToTime + "&fuelDrop=true&fuelFill=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
//                System.out.println("Hi 333 orientation changes " + mStartDate + mEndDate + mStartTime);

//                http://www.gpsvts.net/mobile/getVehicleFuelHistory4Mobile?vehicleId=AKSHYA-TN29BZ1220&interval=1&fromDate=2017-11-22&fromTime=00:00:00&toDate=2017-11-29&toTime=12:07:32&fuelDrop=true&fuelFill=true&fromDateUTC=1511289000000&toDateUTC=1511937452000&userId=AKSHAYATRANS

//                result = ht.httpGet(Const.API_URL + "/mobile/getVehicleFuelHistory4Mobile?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=" + mIntervalSpinnerSelectedValue + "&fuelDrop=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
//                result = ht.httpGet(Const.API_URL + "/mobile/getFuelRawData?vehicleId=" + Constant.SELECTED_VEHICLE_ID  + "&fromTimeUtc=" + fromMilliSeconds + "&toTimeUtc=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
                Log.d("selectspinner1",""+mIntervalSpinnerSelectedValue);
                result = ht.httpGet(Const.API_URL + "/mobile/getFuelRawData?vehicleId=" + Constant.SELECTED_VEHICLE_ID  + "&fromTimeUtc=" + fromMilliSeconds + "&toTimeUtc=" + toMilliSeconds + "&interval=" + mIntervalSpinnerSelectedValue + "&userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mFuelFillData = new ArrayList<>();

            if (result != null && result.length() > 0) {
                String mPrevAdds = null;
                String error = null;
                try {
                    JSONArray jsonArray = new JSONArray(result.trim());
                    //  mFuelFillData.clear();
                    for (int i=0; i<jsonArray.length();i++){
                        JSONObject jsonFuelObject = jsonArray.getJSONObject(i);
                        FuelFillDto f = new FuelFillDto();
                        if (jsonFuelObject.has("dt")) {
                            f.setStartTime(Long.valueOf(jsonFuelObject.getString("dt")));
                        }
                        if (jsonFuelObject.has("fuelLitr")) {
                            f.setFuelLtr(jsonFuelObject.getString("fuelLitr"));
                        }
                        if (jsonFuelObject.has("tankSize")){
                            f.setTankSize(jsonFuelObject.getString("tankSize"));
                        }


                        if (jsonFuelObject.has("sp")) {
                            f.setNewSpeed(jsonFuelObject.getString("sp"));
                        }

                        if (jsonFuelObject.has("ignitionStatus")) {
                            f.setIgnitionStatus(jsonFuelObject.getString("ignitionStatus"));
                        }

                        if (jsonFuelObject.has("odoMeterReading")) {
                            f.setOdoMeterReading(jsonFuelObject.getDouble("odoMeterReading"));
                        }

                        if (jsonFuelObject.has("address")) {
                            if (jsonFuelObject.getString("address").equals("P")) {
                                f.setAddress(mPrevAdds);
                            } else {
                                mPrevAdds = jsonFuelObject.getString("address");
                                f.setAddress(jsonFuelObject.getString("address"));
                            }
                        }
                        if (jsonFuelObject.has("error")){
                            Log.d("errorfound",""+jsonFuelObject.getString("error"));
                            if(jsonFuelObject.getString("error").equalsIgnoreCase("No Data Found.")){
                                lv.setVisibility(View.GONE);
                                mChartLayout.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                            }
                            if (jsonFuelObject.getString("error").equalsIgnoreCase("This report is not privileged for selected vehicle or group.")){
                                lv.setVisibility(View.GONE);
                                mChartLayout.setVisibility(View.GONE);
                                Toast.makeText(FuelNewAreaReport.this, "This report is not privileged for selected vehicle or group.", Toast.LENGTH_SHORT).show();
                            }
                        }

                        mFuelFillData.add(f);
                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("errormsg",""+error);



                setTableLayoutData();

            } else {
                Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(FuelNewAreaReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    public void  setTableLayoutData() {

        Log.d("mFuelFillDatasize",""+mFuelFillData.size());

        if (mFuelFillData !=null) {
            if (mFuelFillData.size() == 0){

            }else {

                if (mFuelFillData.size() == 0){

                }
                else if (mFuelFillData.size() > 0) {

                    if (isHeaderPresent) {

                    } else {
                        lv.addHeaderView(adapter.getHeaderView(lv));
                        isHeaderPresent = true;
                    }
                    lv.setAdapter(adapter);
                    adapter.setData(mFuelFillData);
                    adapter.notifyDataSetChanged();


                    if (mIsLineChartEnabled) {
//                mChart.setVisibility(View.VISIBLE);
                        mChartLayout.setVisibility(View.VISIBLE);
                        lv.setVisibility(View.GONE);
                        formLineChartNew();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                    if (mIsLineChartEnabled) {
                        // mChart.setVisibility(View.VISIBLE);
                        mChartLayout.setVisibility(View.VISIBLE);
                        lv.setVisibility(View.GONE);
                        mChart.clear();
                        mChart.setData(null);
                        mChart.notifyDataSetChanged();
                    } else {
//                mChart.setVisibility(View.GONE);
                        mChartLayout.setVisibility(View.GONE);
                        lv.setVisibility(View.VISIBLE);
                        lv.setAdapter(null);
                    }
                }
            }
        }

    }

    private void formLineChartNew() {


        for (int i = 0; i < mFuelFillData.size(); i++) {
            mOdoEnd = mFuelFillData.get(i).getOdoMeterReading();
            mOdoStart = mFuelFillData.get(0).getOdoMeterReading();

        }

        mTxtOdoStart.setText("Odo Start : " + mOdoStart);
        mTxtOdoEnd.setText("Odo End : " + mOdoEnd);

        Cartesian areaChart = AnyChart.area();

        areaChart.animation(true);

        Crosshair crosshair = areaChart.crosshair();
        crosshair.enabled(true);
        // TODO yStroke xStroke in crosshair
        crosshair.yStroke((Stroke) null, null, null, (String) null, (String) null)
                .xStroke("#fff", 1d, null, (String) null, (String) null)
                .zIndex(39d);
        crosshair.yLabel(0).enabled(true);

        areaChart.yScale().stackMode(ScaleStackMode.VALUE);


        // areaChart.title("Unaudited Apple Inc. Revenue by Operating Segments");
        if (mFuelFillData.size() == 0) {

        } else if (mFuelFillData.size() > 0) {
            List<DataEntry> seriesData = null;
            for (int i = 0; i < mFuelFillData.size(); i++) {
                seriesData = new ArrayList<>();
//        seriesData.add(new CustomDataEntry("Q2 2014", 17.982, 10.941, 9.835, 4.047, 2.841));
//        seriesData.add(new CustomDataEntry("Q3 2014", 17.574, 8.659, 6.230, 2.627, 2.242));
//        seriesData.add(new CustomDataEntry("Q4 2014", 19.75, 10.35, 6.292, 3.595, 2.136));
//        seriesData.add(new CustomDataEntry("Q1 2015", 30.6, 17.2, 16.1, 5.4, 5.2));
//        seriesData.add(new CustomDataEntry("Q2 2015", 21.316, 12.204, 16.823, 3.457, 4.210));
//        seriesData.add(new CustomDataEntry("Q3 2015", 20.209, 10.342, 13.23, 2.872, 2.959));
//        seriesData.add(new CustomDataEntry("Q4 2015", 21.773, 10.577, 12.518, 3.929, 2.704));
//        seriesData.add(new CustomDataEntry("Q1 2016", 29.3, 17.9, 18.4, 4.8, 5.4));
                if (mFuelFillData.get(i).getFuelLtr() == null) {
                    Log.d("fuelllll", "failure");
                } else {
                    ValueDataEntry v1e1 = new ValueDataEntry(i, Float.valueOf(mFuelFillData.get(i).getFuelLtr()));
                    Log.d("fueellitree",""+mFuelFillData.get(i).getFuelLtr());
                    seriesData.add(v1e1);

                }
            }

            List<DataEntry> seriesData1 = new ArrayList<DataEntry>();
            for (int i = 0; i < mFuelFillData.size(); i++) {
                if (mFuelFillData.get(i).getNewSpeed() == null){
                    Log.d("newsppee","failure");
                }else {
                    ValueDataEntry v1e2 = new ValueDataEntry(i, Float.valueOf(mFuelFillData.get(i).getNewSpeed()));
                    Log.d("fueelspedd",""+mFuelFillData.get(i).getNewSpeed());
                    seriesData1.add(v1e2);
                }

            }


            Set set = Set.instantiate();
            set.data(seriesData);
            Set set1 = Set.instantiate();
            set.data(seriesData1);
            Mapping series1Data = set.mapAs("{ x: 'x', value: 'value' }");
            Mapping series2Data = set1.mapAs("{ x: 'x', value: 'value2' }");
            Mapping series3Data = set.mapAs("{ x: 'x', value: 'value3' }");
            //   Mapping series4Data = set.mapAs("{ x: 'x', value: 'value4' }");
            //  Mapping series5Data = set.mapAs("{ x: 'x', value: 'value5' }");

            Area series1 = areaChart.area(series1Data);
            series1.name("Fuel");
            series1.stroke("3 #fff");
            series1.hovered().stroke("3 #fff");
            series1.hovered().markers().enabled(true);
            series1.hovered().markers()
                    .type(MarkerType.CIRCLE)
                    .size(4d)
                    .stroke("1.5 #fff");
            series1.markers().zIndex(100d);

//        Area series2 = areaChart.area(series2Data);
//        series2.name("Europe");
//        series2.stroke("3 #fff");
//        series2.hovered().stroke("3 #fff");
//        series2.hovered().markers().enabled(true);
//        series2.hovered().markers()
//                .type(MarkerType.CIRCLE)
//                .size(4d)
//                .stroke("1.5 #fff");
//        series2.markers().zIndex(100d);

            Area series3 = areaChart.area(series2Data);
            series3.name("Speed");
            series3.stroke("3 #fff");
            series3.hovered().stroke("3 #fff");
            series3.hovered().markers().enabled(true);
            series3.hovered().markers()
                    .type(MarkerType.CIRCLE)
                    .size(4d)
                    .stroke("1.5 #fff");
            series3.markers().zIndex(100d);

//        Area series4 = areaChart.area(series4Data);
//        series4.name("Japan");
//        series4.stroke("3 #fff");
//        series4.hovered().stroke("3 #fff");
//        series4.hovered().markers().enabled(true);
//        series4.hovered().markers()
//                .type(MarkerType.CIRCLE)
//                .size(4d)
//                .stroke("1.5 #fff");
//        series4.markers().zIndex(100d);
//
//        Area series5 = areaChart.area(series5Data);
//        series5.name("Rest of Asia Pacific");
//        series5.stroke("3 #fff");
//        series5.hovered().stroke("3 #fff");
//        series5.hovered().markers().enabled(true);
//        series5.hovered().markers()
//                .type(MarkerType.CIRCLE)
//                .size(4d)
//                .stroke("1.5 #fff");
//        series5.markers().zIndex(100d);


            areaChart.legend().enabled(true);
            areaChart.legend().fontSize(13d);
            areaChart.legend().padding(0d, 0d, 20d, 0d);
            areaChart.xAxis(0).title("Speed");
            areaChart.yAxis(0).title("Fuel");
            areaChart.interactivity().hoverMode(HoverMode.BY_X);
            areaChart.tooltip()
//                    .valuePrefix("$")
//                    .valuePostfix(" bln.")
                    .displayMode(TooltipDisplayMode.UNION);
            anyChartView.setZoomEnabled(true);
            anyChartView.setChart(areaChart);

        }







    }
    private void setData() {
        //ArrayList<Entry> valuesRight = new ArrayList<Entry>();
        String fuelvalue = null,speedvalue = null;
        if (mFuelFillData.size() == 0){
            Log.d("mFuelFillsize","success");
        }
        else if (mFuelFillData.size() > 0) {
            ArrayList<Entry> valuesLeft = new ArrayList<Entry>();
            for (int i = 0; i < mFuelFillData.size(); i++) {

                tanksize = mFuelFillData.get(i).getTankSize();
                Log.d("newspeed", "" + mFuelFillData.get(i).getNewSpeed());
                fuelvalue = mFuelFillData.get(i).getFuelLtr();
                Log.d("fuelvalue", "" +fuelvalue);
                if (mFuelFillData.get(i).getFuelLtr() == null){
                    Log.d("fuelllll","failure");
                } else{
                    Entry v1e1 = new Entry(i, Float.valueOf(mFuelFillData.get(i).getFuelLtr()));
                    valuesLeft.add(v1e1);
                }




            }


            ArrayList<Entry> valuesBottom = new ArrayList<Entry>();
            for (int i = 0; i < mFuelFillData.size(); i++) {
//                speedvalue =  mFuelFillData.get(i).getNewSpeed();
//                Log.d("speedvalue", "" +speedvalue);
                if (mFuelFillData.get(i).getTankSize() == null){
                    Log.d("newsppee","failure");
                }else {
                    Entry v1e1 = new Entry(i, Float.valueOf(mFuelFillData.get(i).getTankSize()));
                    valuesBottom.add(v1e1);
                }

            }

            ArrayList<Entry> valuesRight = new ArrayList<Entry>();
            for (int i = 0; i < mFuelFillData.size(); i++) {
                speedvalue =  mFuelFillData.get(i).getNewSpeed();
                Log.d("speedvalue", "" +speedvalue);
                if (mFuelFillData.get(i).getNewSpeed() == null){
                    Log.d("newsppee","failure");
                }else {
                    Entry v1e1 = new Entry(i, Float.valueOf(mFuelFillData.get(i).getNewSpeed()));
                    valuesRight.add(v1e1);
                }

            }

            Log.d("speedvalue1", "" +speedvalue);
            Log.d("fuelvalue1", "" +fuelvalue);
            if (speedvalue == null && fuelvalue == null){
                Log.d("linechart","failure");
            } else {
                Log.d("linechart","success");
                LineDataSet set1, set2;

                if (mChart.getData() != null &&
                        mChart.getData().getDataSetCount() > 0) {
                    set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
                    set2 = (LineDataSet) mChart.getData().getDataSetByIndex(1);
                    set2.setValues(valuesRight);
                    set1.setValues(valuesLeft);
                    mChart.getData().notifyDataChanged();
                    mChart.notifyDataSetChanged();
                } else {
                    // create a dataset and give it a type
                    //dataset for right
                    set1 = new LineDataSet(valuesRight, "Speed");
                    set1.setAxisDependency(YAxis.AxisDependency.RIGHT);
                    set1.setColor(Color.rgb(247, 163, 92));
                    set1.setLineWidth(1.5f);
                    set1.setDrawCircleHole(false);
//                set1.setCircleColor(Color.BLACK);
                    set1.setCircleColor(Color.rgb(247, 163, 92));
                    set1.setCircleRadius(3f);
                    set1.setDrawCircles(true);
                    set1.setValueTextSize(9f);
                    set1.setDrawFilled(true);
                    set1.setFormLineWidth(1f);
//                set1.setDrawValues(false);
                    set1.setFormSize(15.f);
                    set1.setDrawValues(true);

                    if (Utils.getSDKInt() >= 18) {
                        // fill drawable only supported on api level 18 and above
                        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
                        set1.setFillDrawable(drawable);
                    } else {
                        set1.setFillColor(Color.BLACK);
                    }


                    //dataset for left
                    set2 = new LineDataSet(valuesLeft, "Fuel");
                    set2.setAxisDependency(YAxis.AxisDependency.LEFT);
                    set2.setColor(Color.rgb(135, 206, 250));
//                set2.setColor(Color.MAGENTA);
                    set2.setLineWidth(1.5f);
                    set2.setDrawCircleHole(false);
//                set2.setCircleColor(Color.BLACK);
                    //  set1.setFillColor(Color.rgb(135, 206, 250));
                    set2.setCircleColor(Color.rgb(135, 206, 250));
                    set2.setCircleRadius(3f);
                    set2.setDrawCircles(true);
                    set2.setValueTextSize(9f);
                    set2.setDrawFilled(false);

                    set2.setFormLineWidth(1f);
//                set1.setDrawValues(false);
                    set2.setFormSize(15.f);
                    set2.setDrawValues(true);


                    ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                    dataSets.add(set1); // add the datasets

                    // create a data object with the datasets
//                LineData data = new LineData(dataSets);

                    LineData data = new LineData(set1, set2);
                    mChart.setData(data);
                }
            }

        }
    }


    private ArrayList<Integer> getXAxisValues() {
        xAxis = new ArrayList<Integer>();
        String datetime = "";

        if (mFuelFillData.size() == 0){

        } else {


            for (int i = 0; i < mFuelFillData.size(); i++) {
                xAxis.add(i);
            }
        }

        return xAxis;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_calendar_chart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_calendar:
//                storeFavourtite();

                if (!bottomSheetEnabled) {
                    setIntervalSpinnerValue();
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    // setData();
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }

                break;

            case R.id.action_chart:
                //new getfuelmachinery().execute();
                if (mIsLineChartEnabled) {
                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.line_chart));

                    mIsLineChartEnabled = false;
                    new getKmsSummaryData().execute();
                    // new getfuelmachinery().execute();
                    mChartLayout.setVisibility(View.GONE);
                    lv.setVisibility(View.VISIBLE);


                } else {

                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.data_table));

                    mIsLineChartEnabled = true;

                    mChartLayout.setVisibility(View.VISIBLE);
                    mTxtstart.setVisibility(View.GONE);
                    mTxtend.setVisibility(View.GONE);
                    lv.setVisibility(View.GONE);

                    new getfuelmachinery().execute();
                    formLineChartNew();
                }


                break;

            default:
                break;
        }

        return true;
    }

    private class getfuelmachinery extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                //  Const constaccess = new Const();

                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");

//                String now = new SimpleDateFormat("yyyy-MM-dd hh:mm aa").format(mEndDate.trim() + " " + mEndTime.trim());
//
//                SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
//                SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa");
//                String time24 = outFormat.format(inFormat.parse(now));
                Date d = f.parse(string_from_date);

//                System.out.println("The from date after converted  :::::" + d);

                long fromMilliSeconds = d.getTime();
//                System.out.println("The from time in millis :::::" + fromMilliSeconds);
                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
//                long fromMilliSeconds =1470767400000;
//                long toMilliSeconds=1471685033000;


                //  System.out.println("The to time in millis :::::" + toMilliSeconds);


//                result = constaccess
//                        .sendGet(Const.API_URL + "/mobile/getExecutiveReport?groupId=" + mSelectedGroup + "&fromDate=" + mStrFromDate + "&toDate=" + mStrToDate + "&userId=" + mUserId, 30000);
                HttpConfig ht = new HttpConfig();
//                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDropFillReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=1&fromDate=" + mStrFromDate + "&fromTime=" + mStrFromTime + "&toDate=" + mStrToDate + "&toTime=" + mStrToTime + "&fuelDrop=true&fuelFill=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
//                System.out.println("Hi 333 orientation changes " + mStartDate + mEndDate + mStartTime);

//                http://www.gpsvts.net/mobile/getVehicleFuelHistory4Mobile?vehicleId=AKSHYA-TN29BZ1220&interval=1&fromDate=2017-11-22&fromTime=00:00:00&toDate=2017-11-29&toTime=12:07:32&fuelDrop=true&fuelFill=true&fromDateUTC=1511289000000&toDateUTC=1511937452000&userId=AKSHAYATRANS

//                result = ht.httpGet(Const.API_URL + "/mobile/getVehicleFuelHistory4Mobile?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=" + mIntervalSpinnerSelectedValue + "&fuelDrop=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDetailForMachinery?vehicleId=" + Constant.SELECTED_VEHICLE_ID  + "&fromDateTime=" + fromMilliSeconds + "&toDateTime=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mFuelFillData = new ArrayList<>();

            if (result != null && result.length() > 0) {
                String mPrevAdds = null;
                try {
                    JSONObject jsonObject = new JSONObject(result.trim());

                    if (jsonObject.getString("tFuelCon") !=null){
                        mTxtTotalFuelConsume.setText(jsonObject.getString("tFuelCon") + " ltrs");
                        //  mTotalFuelConsume = jsonObject.getDouble("totalFuelConsume");

                    }
                    if (jsonObject.getString("tdistance") !=null){
                        mTxtTripDistance.setText(jsonObject.getString("tdistance") + " kms");
//                    mTripDistance= jsonObject.getDouble("tdistance");
                    }

                    if (jsonObject.getString("sFuelLevel") !=null){
                        mTxtstartfuel.setText(jsonObject.getString("sFuelLevel") + " ltrs");
                    }

                    if (jsonObject.getString("eFuelLevel") !=null){
                        mTxtendfuel.setText(jsonObject.getString("eFuelLevel") + " ltrs");
                    }

                    mTotalFuelFill = jsonObject.getDouble("totalFuelFills");


                    if (mTotalFuelFill > 0) {

                    } else {
                        mTotalFuelFill = 0;
                    }

                    if (mTotalFuelFill > 0) {
                        String mText = "<u>" + mTotalFuelFill + "</u>";
                        Log.d("mTotalFuelFill",""+mTotalFuelFill);
                        String fuelvalue = Double.toString(Double.parseDouble(new DecimalFormat("##.####").format(mTotalFuelFill)));
                        Log.d("mFuelFill",""+fuelvalue);
                        //mTxtTotalFuelFill.setText((int) Double.parseDouble(new DecimalFormat("##.####").format(mTotalFuelFill)));
//                            mTxtTotalFuelFill.setText(new DecimalFormat("##.##").format(i2));
                        mTxtTotalFuelFill.setText(fuelvalue + " ltrs");
                        // mTxtTotalFuelFill.setText(String.valueOf(mTotalFuelFill) + " ltrs");
                        mTxtTotalFuelFill.setTextColor(Color.BLUE);
                    } else {
                        Log.d("mTotalFuelFill1",""+mTotalFuelFill);
//
                        mTxtTotalFuelFill.setText("" + mTotalFuelFill + " ltrs");
                        mTxtTotalFuelFill.setTextColor(Color.BLACK);
                    }




                    Log.d("totalFuelFill",""+jsonObject.getString("totalFuelFills"));
                    Log.d("tdistance",""+jsonObject.getString("tdistance"));
                    Log.d("tFuelCo n",""+jsonObject.getString("tFuelCon"));


                }catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(FuelNewAreaReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(FuelNewAreaReport.this, MapMenuActivity.class));
        finish();
    }


}
