package com.vamosys.vamos;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.vamosys.adapter.ReportDataAdapter;
import com.vamosys.model.ReportData;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;


public class ReportActivity extends Activity implements OnClickListener {

    int width, height;
    ImageView $BackArrow;
    TextView $FromLable, $ToLable, $FRomValue, $ToValue, $FromTimeLable,
            $ToTimeLable, $FRomTimeValue, $ToTimeValue, $TxtTitle, $Done,
            $StopPageMin, $IdleEditMin, $OverSpeedKm, $NoTReacheableMin;
    LinearLayout $LayoutCheck, $LayoutValue, $LayoutIn1, $LayoutIn2, $LayoutIn3, $LayoutIn4, mLayoutList, mLayoutChk;
    EditText $StopPageEdit, $IdleEdit, $OverSpeedEdit, $NoTReacheableEdit;
    CheckBox $StopPageCheck, $IdleCheck, $OverSpeedCheck, $NotReachableCheck,
            $LocationCheck, $SiteCheck;
    ListView mLstReportData;
    ProgressDialog progressDialog;
    Const cons;
    SharedPreferences sp;

    boolean mIsStoppage = true, mIsIdle = true, mIsNotReachable = true, mOverSpeed = true, mIsLocation = false, mSite = false;
    String overspeed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_activity_layout);
        cons = new Const();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        intialization();
        screenArrange();

        $TxtTitle.setText("Reports(" + Constant.SELECTED_VEHICLE_SHORT_NAME + ")");
        $FRomValue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new DatePickerDialog(ReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        $FRomValue.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        $FRomTimeValue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new TimePickerDialog(ReportActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        // TODO Auto-generated method stub
                        $FRomTimeValue.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
                    }
                }, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false).show();
            }
        });
        $ToValue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new DatePickerDialog(ReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        $ToValue.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        $ToTimeValue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new TimePickerDialog(ReportActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        // TODO Auto-generated method stub
                        $ToTimeValue.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
                    }
                }, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false).show();
            }
        });


    }

    private void intialization() {
        // TODO Auto-generated method stub
        $StopPageEdit = (EditText) findViewById(R.id.report_checkStopPageValue);
        $IdleEdit = (EditText) findViewById(R.id.report_checkIdleValue);
        $NoTReacheableEdit = (EditText) findViewById(R.id.report_checkNotReachableValue);
        $OverSpeedEdit = (EditText) findViewById(R.id.report_checkOverSpeedValue);

        $StopPageEdit.setText("5");
        $IdleEdit.setText("5");
        $NoTReacheableEdit.setText("5");
        $OverSpeedEdit.setText(Constant.SELECTED_OVER_SPEED);

        $StopPageCheck = (CheckBox) findViewById(R.id.report_checkStopPageLable);
        $IdleCheck = (CheckBox) findViewById(R.id.report_checkIdleLable);
        $OverSpeedCheck = (CheckBox) findViewById(R.id.report_checkOverSpeedLable);
        $NotReachableCheck = (CheckBox) findViewById(R.id.report_checkNotReachableLable);
        $LocationCheck = (CheckBox) findViewById(R.id.report_checkLocationLable);
        $SiteCheck = (CheckBox) findViewById(R.id.report_checkSiteLable);

        $StopPageCheck.setChecked(true);
        $IdleCheck.setChecked(true);
        $OverSpeedCheck.setChecked(true);
        $NotReachableCheck.setChecked(true);
        $LocationCheck.setChecked(false);
        $SiteCheck.setChecked(false);


        $FromLable = (TextView) findViewById(R.id.report_fromLable);
        $ToLable = (TextView) findViewById(R.id.report_toLable);
        $FRomValue = (TextView) findViewById(R.id.report_fromValue);
        $ToValue = (TextView) findViewById(R.id.report_toValue);
        $FromTimeLable = (TextView) findViewById(R.id.report_fromTimeLable);
        $ToTimeLable = (TextView) findViewById(R.id.report_toTimeLable);
        $FRomTimeValue = (TextView) findViewById(R.id.report_fromTimeValue);
        $ToTimeValue = (TextView) findViewById(R.id.report_toTimeValue);

        $TxtTitle = (TextView) findViewById(R.id.report_title);
        $BackArrow = (ImageView) findViewById(R.id.report_view_Back);
        $Done = (TextView) findViewById(R.id.report_done);
        $LayoutCheck = (LinearLayout) findViewById(R.id.report_checkLableLayout);
        $LayoutValue = (LinearLayout) findViewById(R.id.report_checkValueLayout);

        $LayoutIn1 = (LinearLayout) findViewById(R.id.report_lay1);
        $LayoutIn2 = (LinearLayout) findViewById(R.id.report_lay2);
        $LayoutIn3 = (LinearLayout) findViewById(R.id.report_lay3);
        $LayoutIn4 = (LinearLayout) findViewById(R.id.report_lay4);
        // $LayoutReport = (LinearLayout) findViewById(R.id.test_reportLayout);
        // $LayoutHistroy = (LinearLayout)
        // findViewById(R.id.test_historyLayout);
        $StopPageMin = (TextView) findViewById(R.id.report_txtStopPageMin);
        $IdleEditMin = (TextView) findViewById(R.id.report_txtIdleMin);
        $OverSpeedKm = (TextView) findViewById(R.id.report_txtOverSpeedKm);
        $NoTReacheableMin = (TextView) findViewById(R.id.report_txtNotReachableMin);
        $BackArrow.setOnClickListener(this);
        $Done.setOnClickListener(this);

        mLayoutChk = (LinearLayout) findViewById(R.id.report_chk_layout);
        mLayoutList = (LinearLayout) findViewById(R.id.report_list_layout);
        mLstReportData = (ListView) findViewById(R.id.report_list_view);

        mLayoutList.setVisibility(View.GONE);
        mLayoutChk.setVisibility(View.VISIBLE);
        // $LayoutInfo.setOnClickListener(this);
        // $LayoutLocate.setOnClickListener(this);
        // $LayoutReport.setOnClickListener(this);
        // $LayoutHistroy.setOnClickListener(this);

        $FRomValue.setText(cons.GetCurrentDate(Calendar.getInstance()));
        $ToValue.setText(cons.GetCurrentDate(Calendar.getInstance()));


    }

    /**
     * Function to add 0 for time which is lesser that 10. Ex. 08.00, 05.00
     */
    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.report_view_Back:
                startActivity(new Intent(ReportActivity.this, MapMenuActivity.class));
                finish();
                break;
            case R.id.report_done:
                Constant.SELECTED_OVER_SPEED = $OverSpeedEdit.getText().toString();
                String strData = "&fromDate=" + $FRomValue.getText().toString() + "&fromTime=" + $FRomTimeValue.getText().toString() + ":00&toDate=" + $ToValue.getText().toString() + "&toTime=" + $ToTimeValue.getText().toString() + ":00&stoppage=" + $StopPageCheck.isChecked() + "&stopMints=" + $StopPageEdit.getText().toString() + "&idle=" + $IdleCheck.isChecked() + "&idleMints=" + $IdleEdit.getText().toString() + "&notReachable=" + $NotReachableCheck.isChecked() + "&notReachableMints=" + $NoTReacheableEdit.getText().toString() + "&overspeed=" + $OverSpeedCheck.isChecked() + "&speed=" + $OverSpeedEdit.getText().toString() + "&location=" + $LocationCheck.isChecked() + "&site=" + $SiteCheck.isChecked();
                // new GetReportData.execute(strData);
                new GetReportData().execute(strData);
                break;

        }
    }


    private class GetReportData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(ReportActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String url = Const.API_URL + "mobile/getActionReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + params[0];
                // Log.d(TAG, "GetReportData()" + "url = " + url + "");
                HttpConfig ht = new HttpConfig();
                return ht.httpGet(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {
            // Log.d(TAG, "GetReportData()" + "result = " + result + "");
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            try {
                //result = "[{\"startTime\":1451025700295,\"state\":\"LocationExit\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451025740300,\"state\":\"LocationEntry\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451025790308,\"state\":\"LocationExit\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451029229991,\"state\":\"LocationEntry\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451029268311,\"state\":\"LocationExit\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451111892272,\"state\":\"LocationEntry\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451111962293,\"state\":\"LocationExit\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451115969411,\"state\":\"LocationEntry\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451116008057,\"state\":\"LocationExit\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0}]";
                ArrayList<ReportData> alReportData = new ArrayList<>();
                if (result != null && !result.isEmpty()) {
                    JSONArray mJSONArray = new JSONArray(result);
                    if (mJSONArray != null) {
                        for (int i = 0; i < mJSONArray.length(); i++) {
                            JSONObject mJsonObject = new JSONObject(mJSONArray.getString(i));
                            if (mJsonObject.has("startTime") && mJsonObject.has("state") && mJsonObject.has("address") && mJsonObject.has("fuelConsume")) {
                                //Load listview data
                                ReportData mReportData = new ReportData();
                                mReportData.setStartTime(mJsonObject.getString("startTime"));
                                mReportData.setState(mJsonObject.getString("state"));
                                mReportData.setAddress(mJsonObject.getString("address"));
                                mReportData.setFuelConsume(mJsonObject.getString("fuelConsume"));
                                alReportData.add(mReportData);
                            }
                        }
                    }
                }
                if (alReportData.size() > 0) {

//                    System.out.println("The report data size is ::::" + alReportData.size());

                    mLayoutList.setVisibility(View.VISIBLE);
                    mLayoutChk.setVisibility(View.GONE);
                    // mReportListAdapter.setItemList(alReportData);

                    mLstReportData.setAdapter(new ReportDataAdapter(ReportActivity.this, alReportData));
                    Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.grow_from_bottom);
                    bottomUp.setDuration(1000);
                    mLstReportData.startAnimation(bottomUp);
                    //  svReportOption.setVisibility(View.GONE);
                } else {
                    Toast.makeText(getApplicationContext(), "No data found...", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(ReportActivity.this, MapMenuActivity.class));
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(ReportActivity.this,MapMenuActivity.class));
        finish();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        $BackArrow.setLayoutParams(backImageParams);
        $BackArrow.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 85 / 100;
        headTxtParams.height = height * 10 / 100;
        $TxtTitle.setLayoutParams(headTxtParams);
        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
        $TxtTitle.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtParama.width = width * 50 / 100;
        txtParama.height = height * 6 / 100;
        $FromLable.setLayoutParams(txtParama);
        $FRomValue.setLayoutParams(txtParama);
        $ToValue.setLayoutParams(txtParama);
        $FromTimeLable.setLayoutParams(txtParama);
        $FRomTimeValue.setLayoutParams(txtParama);
        $ToTimeValue.setLayoutParams(txtParama);

        $FromLable.setGravity(Gravity.CENTER | Gravity.LEFT);
        $FRomValue.setGravity(Gravity.CENTER | Gravity.LEFT);
        $ToValue.setGravity(Gravity.CENTER | Gravity.LEFT);
        $FromTimeLable.setGravity(Gravity.CENTER | Gravity.LEFT);
        $FRomTimeValue.setGravity(Gravity.CENTER | Gravity.LEFT);
        $ToTimeValue.setGravity(Gravity.CENTER | Gravity.LEFT);

        $FromLable.setPadding(width * 5 / 100, 0, 0, 0);
        $FRomValue.setPadding(width * 5 / 100, 0, 0, 0);
        $ToValue.setPadding(width * 5 / 100, 0, 0, 0);
        $FromTimeLable.setPadding(width * 5 / 100, 0, 0, 0);
        $FRomTimeValue.setPadding(width * 5 / 100, 0, 0, 0);
        $ToTimeValue.setPadding(width * 5 / 100, 0, 0, 0);

        LinearLayout.LayoutParams txtSecondParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtSecondParama.width = width * 50 / 100;
        txtSecondParama.height = height * 6 / 100;
        txtSecondParama.topMargin = height * 3 / 100;
        $ToLable.setLayoutParams(txtSecondParama);
        $ToTimeLable.setLayoutParams(txtSecondParama);
        $ToLable.setGravity(Gravity.CENTER | Gravity.LEFT);
        $ToTimeLable.setGravity(Gravity.CENTER | Gravity.LEFT);
        $ToLable.setPadding(width * 5 / 100, 0, 0, 0);
        $ToTimeLable.setPadding(width * 5 / 100, 0, 0, 0);

        LinearLayout.LayoutParams checkLayoutParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        checkLayoutParama.width = width * 50 / 100;
        //checkLayoutParama.topMargin=height*2/100;
        $LayoutCheck.setLayoutParams(checkLayoutParama);
        $LayoutValue.setLayoutParams(checkLayoutParama);
        //		 $LayoutInfo.setLayoutParams(mainLayoutParama);
        //		 $LayoutLocate.setLayoutParams(mainLayoutParama);
        //		 $LayoutReport.setLayoutParams(mainLayoutParama);

        LinearLayout.LayoutParams checkParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        checkParama.width = width * 45 / 100;
        checkParama.height = (int) (height * 7 / 100);
        checkParama.leftMargin = width * 5 / 100;
        checkParama.topMargin = height * 2 / 100;
        $StopPageCheck.setLayoutParams(checkParama);
        //$StopPageCheck.setPadding(width*1/100, height*1/100, width*1/100, height*1/100);
        $IdleCheck.setLayoutParams(checkParama);
        $OverSpeedCheck.setLayoutParams(checkParama);
        $NotReachableCheck.setLayoutParams(checkParama);
        $LocationCheck.setLayoutParams(checkParama);

        //		$StopPageCheck.setGravity(Gravity.CENTER | Gravity.LEFT);
        //		$IdleCheck.setGravity(Gravity.CENTER | Gravity.LEFT);
        //		$OverSpeedCheck.setGravity(Gravity.CENTER | Gravity.LEFT);
        //		$NotReachableCheck.setGravity(Gravity.CENTER | Gravity.LEFT);
        //		$LocationCheck.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams ediecheckLayoutParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        ediecheckLayoutParama.width = width * 45 / 100;
        ediecheckLayoutParama.height = height * 7 / 100;
        //		ediecheckLayoutParama.leftMargin = width * 3 / 100;
        ediecheckLayoutParama.topMargin = height * 2 / 100;
        ediecheckLayoutParama.gravity = Gravity.CENTER | Gravity.LEFT;
        $LayoutIn1.setLayoutParams(ediecheckLayoutParama);
        $LayoutIn2.setLayoutParams(ediecheckLayoutParama);
        $LayoutIn3.setLayoutParams(ediecheckLayoutParama);
        $LayoutIn4.setLayoutParams(ediecheckLayoutParama);

        LinearLayout.LayoutParams edieParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        edieParama.width = width * 20 / 100;
        //edieParama.height = height * 8 / 100;
        //edieParama.leftMargin = width * 3 / 100;
        //edieParama.topMargin = height * 2 / 100;
        edieParama.gravity = Gravity.CENTER | Gravity.LEFT;
        $StopPageEdit.setLayoutParams(edieParama);
        $IdleEdit.setLayoutParams(edieParama);
        $OverSpeedEdit.setLayoutParams(edieParama);
        $NoTReacheableEdit.setLayoutParams(edieParama);
        //$SiteCheck.setLayoutParams(checkParama);

        $StopPageEdit.setGravity(Gravity.CENTER | Gravity.LEFT);
        $IdleEdit.setGravity(Gravity.CENTER | Gravity.LEFT);
        $OverSpeedEdit.setGravity(Gravity.CENTER | Gravity.LEFT);
        $NoTReacheableEdit.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams minParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        minParama.width = width * 25 / 100;
        //minParama.height = height * 6 / 100;
        //minParama.topMargin = height * 3 / 100;
        $StopPageMin.setLayoutParams(minParama);
        $IdleEditMin.setLayoutParams(minParama);
        $OverSpeedKm.setLayoutParams(minParama);
        $NoTReacheableMin.setLayoutParams(minParama);

        $StopPageMin.setGravity(Gravity.BOTTOM | Gravity.LEFT);
        $IdleEditMin.setGravity(Gravity.BOTTOM | Gravity.LEFT);
        $OverSpeedKm.setGravity(Gravity.BOTTOM | Gravity.LEFT);
        $NoTReacheableMin.setGravity(Gravity.BOTTOM | Gravity.LEFT);

        $StopPageMin.setPadding(width * 2 / 100, 0, 0, 0);
        $IdleEditMin.setPadding(width * 2 / 100, 0, 0, 0);
        $OverSpeedKm.setPadding(width * 2 / 100, 0, 0, 0);
        $NoTReacheableMin.setPadding(width * 2 / 100, 0, 0, 0);

        LinearLayout.LayoutParams sitecheckParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        sitecheckParama.width = width * 45 / 100;
        sitecheckParama.height = (int) (height * 7 / 100);
        sitecheckParama.topMargin = height * 2 / 100;
        $SiteCheck.setLayoutParams(sitecheckParama);
        $SiteCheck.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams okayButton = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        okayButton.width = width * 50 / 100;
        okayButton.height = (int) (height * 6.5 / 100);
        okayButton.gravity = Gravity.CENTER;
        okayButton.topMargin = height * 3 / 100;
        $Done.setLayoutParams(okayButton);
        $Done.setGravity(Gravity.CENTER);

        if (width >= 600) {
            $FromLable.setTextSize(16);
            $ToTimeLable.setTextSize(16);
            $ToLable.setTextSize(16);
            $FRomValue.setTextSize(16);
            $ToValue.setTextSize(16);
            $FromTimeLable.setTextSize(16);
            $FRomTimeValue.setTextSize(16);
            $ToTimeValue.setTextSize(16);
            $TxtTitle.setTextSize(16);
            $StopPageEdit.setTextSize(16);
            $IdleEdit.setTextSize(16);
            $OverSpeedEdit.setTextSize(16);
            $NoTReacheableEdit.setTextSize(16);
            $SiteCheck.setTextSize(16);
            $LocationCheck.setTextSize(16);
            $NotReachableCheck.setTextSize(16);
            $OverSpeedCheck.setTextSize(16);
            $IdleCheck.setTextSize(16);
            $StopPageCheck.setTextSize(16);
            $Done.setTextSize(16);
            $StopPageMin.setTextSize(14);
            $IdleEditMin.setTextSize(14);
            $OverSpeedKm.setTextSize(14);
            $NoTReacheableMin.setTextSize(14);
        } else if (width > 501 && width < 600) {
            $FromLable.setTextSize(15);
            $ToTimeLable.setTextSize(15);
            $ToLable.setTextSize(15);
            $FRomValue.setTextSize(15);
            $ToValue.setTextSize(15);
            $FromTimeLable.setTextSize(15);
            $FRomTimeValue.setTextSize(15);
            $ToTimeValue.setTextSize(15);
            $TxtTitle.setTextSize(15);
            $StopPageEdit.setTextSize(15);
            $IdleEdit.setTextSize(15);
            $OverSpeedEdit.setTextSize(15);
            $NoTReacheableEdit.setTextSize(15);
            $SiteCheck.setTextSize(15);
            $LocationCheck.setTextSize(15);
            $NotReachableCheck.setTextSize(15);
            $OverSpeedCheck.setTextSize(15);
            $IdleCheck.setTextSize(15);
            $StopPageCheck.setTextSize(15);
            $Done.setTextSize(15);
            $StopPageMin.setTextSize(13);
            $IdleEditMin.setTextSize(13);
            $OverSpeedKm.setTextSize(13);
            $NoTReacheableMin.setTextSize(13);
        } else if (width > 260 && width < 500) {
            $FromLable.setTextSize(14);
            $ToTimeLable.setTextSize(14);
            $ToLable.setTextSize(14);
            $FRomValue.setTextSize(14);
            $ToValue.setTextSize(14);
            $FromTimeLable.setTextSize(14);
            $FRomTimeValue.setTextSize(14);
            $ToTimeValue.setTextSize(14);
            $TxtTitle.setTextSize(14);
            $StopPageEdit.setTextSize(14);
            $IdleEdit.setTextSize(14);
            $OverSpeedEdit.setTextSize(14);
            $NoTReacheableEdit.setTextSize(14);
            $SiteCheck.setTextSize(14);
            $LocationCheck.setTextSize(14);
            $NotReachableCheck.setTextSize(14);
            $OverSpeedCheck.setTextSize(14);
            $IdleCheck.setTextSize(14);
            $StopPageCheck.setTextSize(14);
            $Done.setTextSize(14);
            $StopPageMin.setTextSize(12);
            $IdleEditMin.setTextSize(12);
            $OverSpeedKm.setTextSize(12);
            $NoTReacheableMin.setTextSize(12);
        } else if (width <= 260) {
            $FromLable.setTextSize(13);
            $ToTimeLable.setTextSize(13);
            $ToLable.setTextSize(13);
            $FRomValue.setTextSize(13);
            $ToValue.setTextSize(13);
            $FromTimeLable.setTextSize(13);
            $FRomTimeValue.setTextSize(13);
            $ToTimeValue.setTextSize(13);
            $TxtTitle.setTextSize(13);
            $StopPageEdit.setTextSize(13);
            $IdleEdit.setTextSize(13);
            $OverSpeedEdit.setTextSize(13);
            $NoTReacheableEdit.setTextSize(13);
            $SiteCheck.setTextSize(13);
            $LocationCheck.setTextSize(13);
            $NotReachableCheck.setTextSize(13);
            $OverSpeedCheck.setTextSize(13);
            $IdleCheck.setTextSize(13);
            $StopPageCheck.setTextSize(13);
            $Done.setTextSize(13);
            $StopPageMin.setTextSize(11);
            $IdleEditMin.setTextSize(11);
            $OverSpeedKm.setTextSize(11);
            $NoTReacheableMin.setTextSize(11);
        }
    }
}