package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avenues.lib.testotpappnew.PaymentActivity;
import com.vamosys.adapter.PendingPaymentDueAdapter;
import com.vamosys.model.PaymentDueDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Utility.AvenuesParams;

public class PendingPaymentActivity extends AppCompatActivity {
    Toolbar mToolbar;
    ConnectionDetector cd;
    SharedPreferences sp;
    List<PaymentDueDto> mPaymentDueList = new ArrayList<PaymentDueDto>();
    RecyclerView mRecyclerView;
    Button mBtnOk;
    TextView mTxtTotalDue, mTxtCustomerName;
    LinearLayout mBottomTotalAmntLayout, mNoRecordLayout, mPaymentRecordLayout;

    double mTotalDueAmnt = 0.0;
    String mCustName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_payment);
        init();
        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();

//            System.out.println("Hi data get method calling");

//            mStoppageData.clear();
            new getPaymentDue().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(PendingPaymentActivity.this, VehicleListActivity.class));
                finish();


            }
        });

        mBottomTotalAmntLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PendingPaymentActivity.this, PaymentActivity.class);
                String mGroupName = null;
                if (Constant.SELECTED_GROUP.contains(":")) {
                    String[] str_msg_data_array = Constant.SELECTED_GROUP.split(":");
                    mGroupName = str_msg_data_array[1];
                } else {
                    mGroupName = Constant.SELECTED_GROUP;
                }


                intent.putExtra(AvenuesParams.AMOUNT, String.valueOf(mTotalDueAmnt));

                intent.putExtra(AvenuesParams.BILLING_NAME, mCustName);
                intent.putExtra(AvenuesParams.F_CODE, mGroupName);
                intent.putExtra(AvenuesParams.USER_ID, Constant.SELECTED_USER_ID);

                startActivity(intent);
            }
        });

    }


    private void init() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.payment_pending));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        mBtnOk = (Button) findViewById(R.id.btn_payment_due_okay);
        mTxtTotalDue = (TextView) findViewById(R.id.txt_total_balance);
        mTxtCustomerName = (TextView) findViewById(R.id.txt_customer_name);

        mBottomTotalAmntLayout = (LinearLayout) findViewById(R.id.bottom_total_amount_layout);

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mNoRecordLayout = (LinearLayout) findViewById(R.id.layout_no_record);
        mPaymentRecordLayout = (LinearLayout) findViewById(R.id.layout_payment_data);
    }


    /**
     * AsyncTask Background API Calls
     */
    public class getPaymentDue extends AsyncTask<String, Integer, String> {
        ProgressDialog progressDialog1;

        @Override
        public String doInBackground(String... urls) {


            String response_from_server = "";
            try {

//                   System.out.println("The getPaymentDue url is ::::"+Const.PAYMENT_DUE_API + mUserId);
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(Const.PAYMENT_DUE_API + Constant.SELECTED_USER_ID);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog1 = new ProgressDialog(PendingPaymentActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog1.setMessage("Please Wait...");
            progressDialog1.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog1.setCancelable(true);
            progressDialog1.setCanceledOnTouchOutside(false);
            progressDialog1.show();

        }

        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        public void onPostExecute(String result) {
            progressDialog1.dismiss();
//            System.out.println("The getPaymentDue result  is ::::"+result);

            if (result != null && result.length() > 0) {
                try {
                    JSONObject jObj = new JSONObject(result.trim());
                    if (jObj.has("hist") && !jObj.get("hist").equals(null)) {
                        JSONArray jArray = jObj.getJSONArray("hist");
                        mPaymentDueList.clear();
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject jobj2 = jArray.getJSONObject(i);
                            PaymentDueDto pd = new PaymentDueDto();
                            if (jobj2.has("dueDate")) {
                                pd.setDueDate(jobj2.getString("dueDate"));
                            }
                            if (jobj2.has("dueDays")) {
                                pd.setDueDays(jobj2.getString("dueDays"));
                            }

                            if (jobj2.has("customerName")) {
                                pd.setCustomerName(jobj2.getString("customerName"));
                            }

                            if (jobj2.has("balanceAmount")) {
                                pd.setBalanceAmount(jobj2.getString("balanceAmount"));
                                mTotalDueAmnt = mTotalDueAmnt + Double.parseDouble(jobj2.getString("balanceAmount"));
                            }

                            if (jobj2.has("invoiceLink")) {
                                pd.setInvoiceLink(jobj2.getString("invoiceLink"));
                            }
                            mPaymentDueList.add(pd);
                        }

                    }

                    if (jObj.has("balanceAmount") && jObj.getString("balanceAmount").trim().length() > 0) {
                        mTotalDueAmnt = Double.parseDouble(jObj.getString("balanceAmount"));
                    }

                    if (jObj.has("customerName")) {
                        mCustName = jObj.getString("customerName");
                    }


                    //show alert dialog
                    if (mPaymentDueList.size() > 0) {
                        mPaymentRecordLayout.setVisibility(View.VISIBLE);
                        mNoRecordLayout.setVisibility(View.GONE);
                        //showPaymentDueDialog(mTotalDueAmnt, mCustName);

                        PendingPaymentDueAdapter mAadp = new PendingPaymentDueAdapter(mPaymentDueList, PendingPaymentActivity.this);
                        mRecyclerView.setAdapter(mAadp);
                        mAadp.notifyDataSetChanged();

                        mTxtTotalDue.setText(Utils.get2DecimalValue(String.valueOf(mTotalDueAmnt)));
                        mTxtCustomerName.setText(mCustName);

                    } else {
                        mPaymentRecordLayout.setVisibility(View.GONE);
                        mNoRecordLayout.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    mPaymentRecordLayout.setVisibility(View.GONE);
                    mNoRecordLayout.setVisibility(View.VISIBLE);
                }
            } else {
                mPaymentRecordLayout.setVisibility(View.GONE);
                mNoRecordLayout.setVisibility(View.VISIBLE);
            }

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PendingPaymentActivity.this, VehicleListActivity.class));
        finish();
    }
}
