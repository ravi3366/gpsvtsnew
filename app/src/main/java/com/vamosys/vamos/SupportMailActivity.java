package com.vamosys.vamos;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class SupportMailActivity extends AppCompatActivity {
Button Send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_mail);
        Send=(Button)findViewById(R.id.send);
        Send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromEmail = "nathiya.vamosys@gmail.com";
                String fromPassword = "#vamo123";
                String toEmails = "kokila.vamosys@gmail.com";
                String emailSubject = "User Id";
                String emailBody = "recive mail from nathiya hello hai";
                String UserMailId = "kokila.vamosys@gmail.com";
                new SupportMailActivity.SendMailTask(SupportMailActivity.this).execute(fromEmail,
                        fromPassword, toEmails, emailSubject, emailBody,UserMailId);
//                Intent it = new Intent(Intent.ACTION_SEND);
//                it.putExtra(Intent.EXTRA_EMAIL,"kokila.vamosys@gmail.com");
//                it.putExtra(Intent.EXTRA_SUBJECT,"Testing");
//                it.putExtra(Intent.EXTRA_TEXT,"testing the intent concept");
//                it.setType("message/rfc822");
//                startActivity(Intent.createChooser(it,"Choose Mail App"));

            }
        });

    }
    @SuppressWarnings("rawtypes")
    public class SendMailTask extends AsyncTask {

        private ProgressDialog statusDialog;
        private Activity sendMailActivity;

        public SendMailTask(Activity activity) {
            sendMailActivity = activity;

        }

        @Override
        protected Object doInBackground(Object[] args) {
            try {
                Log.i("SendMailTask", "About to instantiate GMail...");
                publishProgress("Processing input....");
                GMail androidEmail = new GMail(args[0].toString(),
                        args[1].toString(),  args[2].toString(), args[3].toString(),
                        args[4].toString(),args[5].toString());
                publishProgress("Preparing mail message....");
                androidEmail.createEmailMessage();
                publishProgress("Sending email....");
                androidEmail.sendEmail();
                publishProgress("Email Sent.");
                Log.i("SendMailTask", "Mail Sent.");



            } catch (Exception e) {
                publishProgress(e.getMessage());
                Log.e("SendMailTask", e.getMessage(), e);}
            return null;
        }
        @Override
        public void onProgressUpdate(Object... values) {
//            statusDialog.setMessage("sending");
        }
        @Override
        public void onPostExecute(Object result) {
//            statusDialog.dismiss();
        }
    }

    }
