package com.vamosys.vamos;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VehicleTrackingService extends Service {
    SharedPreferences sp;
    double latitude, longitude, api_pass_lat, api_pass_lng;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    DBHelper dbhelper;
    String username;
    Context context = this;

    public VehicleTrackingService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        dbSetup(context);
        WifiManager m_wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        sp = PreferenceManager.getDefaultSharedPreferences(context);

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        //  username = sp.getString("mPhoneNumber", "9940642634");
        cd = new ConnectionDetector(context);
//        System.out.println("tracking_VehicleTrackingService onReceive");
        if (checkNetworkSettings()) {
            Thread splashThread = new Thread() {
                @Override
                public void run() {
                    String response_from_server = null;
                    try {
                        String urlstr = Const.API_URL + "mobile/getSelectedVehicleLocation?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&macid=" + getStdTableValue(context, "macid") + "&appid=" + getStdTableValue(context, "appid");
//                        Date date = new Date(); // *1000 is to convert seconds to milliseconds
//                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // the format of your date
//                        String formattedDate = sdf.format(date);
//                        System.out.println("The Live time is  "+formattedDate);
                        System.out.println("The Live track service url is ::::"+urlstr);

                        response_from_server = sendGet(urlstr, 10000);

                        System.out.println("The Live vehicle live track datas :::" + response_from_server);

                    } catch (Exception e) {
                        e.printStackTrace();
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putBoolean("tracking_exception", true);
                        editor.commit();
                    } finally {
                        if (response_from_server != null && !response_from_server.isEmpty()) {
                            dbhelper.insert_current_vehicle_details(response_from_server);
                        }
                    }
                }
            };
            splashThread.start();
        } else {
            Toast internet_toast = Toast.makeText(context, "Internet not connected", Toast.LENGTH_LONG);
            internet_toast.show();
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * POST and GET Methods
     */
    private String sendGet(String url, int connection_timeout) throws Exception {

        //  System.out.println("The background service url is :::::"+url);

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setConnectTimeout(connection_timeout);
        int responseCode = con.getResponseCode();
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }
    /** POST and GET Methods*/
    /**
     * Methods
     */
    public String getStdTableValue(Context context, String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(context, "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }

    public boolean checkNetworkSettings() {
        isInternetPresent = cd.isConnectingToInternet();
        return isInternetPresent;
    }
    /** Methods*/
    /**
     * Database Initialisation
     */
    void dbSetup(Context context) {
        dbhelper = new DBHelper(context);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /** Database Initialisation*/
}
