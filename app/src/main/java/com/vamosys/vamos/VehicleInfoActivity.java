package com.vamosys.vamos;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.utils.Constant;
import com.vamosys.utils.TypefaceUtil;

public class VehicleInfoActivity extends Activity {
    ImageView info_view_Back, info_id_vehicle_info_icon;
    TextView info_header_txt, info_id_vehicleid, info_id_lastseentime_labelid,
            info_id_ignition_speed, info_id_time_labelid, info_id_view_address,
            info_id_view_lastseentime, info_id_view_lastactive,
            info_id_ignition_labelid, info_id_time,
            info_id_address_text_labelid, info_id_lastseentime_label_separator,
            info_id_speed_labelid, info_id_view_speed,
            info_id_view_spped_lastactive, info_id_speed_labelid_separator, info_odo_label, info_odo_value, info_id_ac_status_label, info_id_ac_status_value;
    LinearLayout info_id_lastseentime_layout, info_id_address_layout,
            info_id_address_label_layout, info_id_ignition_layout,
            info_id_ignition_label_layout, info_id_time_label_layout, info_id_ac_status_layout;
    LinearLayout info_id_speed_layout, info_id_speed_label_layout;
    Const cons;
    DBHelper dbhelper;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_info);
        cons = new Const();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        init();
        dbSetup();
        screenArrange();
        setData();

        info_view_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VehicleInfoActivity.this, MapMenuActivity.class));
                finish();
            }
        });

    }

    void dbSetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(VehicleInfoActivity.this, VehicleListActivity.class));
        finish();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        /* screen arrangements */
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams backImageParams0 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams0.width = width * 45 / 100;
        backImageParams0.height = height * 3 / 100;
        backImageParams0.gravity = Gravity.CENTER;
        backImageParams0.topMargin = (int) (height * 0.01 / 100);
        backImageParams0.bottomMargin = (int) (height * 0.01 / 100);
        backImageParams0.leftMargin = (int) (height * 0.25 / 100);
        backImageParams0.rightMargin = (int) (height * 0.25 / 100);

        info_id_lastseentime_labelid.setLayoutParams(backImageParams0);
        info_odo_label.setLayoutParams(backImageParams0);


        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 90 / 100;
        backImageParams.height = height * 3 / 100;
        backImageParams.gravity = Gravity.CENTER;
        backImageParams.topMargin = (int) (height * 0.01 / 100);
        backImageParams.bottomMargin = (int) (height * 0.01 / 100);
        backImageParams.leftMargin = (int) (height * 0.25 / 100);
        backImageParams.rightMargin = (int) (height * 0.25 / 100);

        // info_id_lastseentime_labelid.setLayoutParams(backImageParams);

        info_id_speed_labelid.setLayoutParams(backImageParams);
        info_id_address_text_labelid.setLayoutParams(backImageParams);


        LinearLayout.LayoutParams backImageParams12 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams12.width = width * 45 / 100;
        backImageParams12.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        backImageParams12.topMargin = (int) (height * 0.01 / 100);
        backImageParams12.bottomMargin = (int) (height * 0.01 / 100);
        backImageParams12.leftMargin = (int) (height * 0.25 / 100);
        backImageParams12.rightMargin = (int) (height * 0.25 / 100);

        info_id_view_lastseentime.setLayoutParams(backImageParams12);
        info_odo_value.setLayoutParams(backImageParams12);


        LinearLayout.LayoutParams backImageParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams1.width = width * 90 / 100;
        backImageParams1.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        backImageParams1.topMargin = (int) (height * 0.01 / 100);
        backImageParams1.bottomMargin = (int) (height * 0.01 / 100);
        backImageParams1.leftMargin = (int) (height * 0.25 / 100);
        backImageParams1.rightMargin = (int) (height * 0.25 / 100);

        // info_id_view_lastseentime.setLayoutParams(backImageParams1);
        info_id_view_address.setLayoutParams(backImageParams1);

        LinearLayout.LayoutParams backImageParams2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams2.width = width * 45 / 100;
        backImageParams2.height = height * 3 / 100;
        backImageParams2.gravity = Gravity.CENTER;

        info_id_ignition_labelid.setLayoutParams(backImageParams2);
        info_id_time_labelid.setLayoutParams(backImageParams2);

        LinearLayout.LayoutParams backImageParams3 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams3.width = width * 40 / 100;
        backImageParams3.height = height * 6 / 100;
        backImageParams3.gravity = Gravity.CENTER;
        backImageParams3.topMargin = (int) (height * 0.01 / 100);
        backImageParams3.bottomMargin = (int) (height * 0.01 / 100);
        backImageParams3.leftMargin = (int) (height * 0.25 / 100);
        backImageParams3.rightMargin = (int) (height * 0.25 / 100);

        info_id_view_speed.setLayoutParams(backImageParams3);
        info_id_view_spped_lastactive.setLayoutParams(backImageParams3);

        LinearLayout.LayoutParams backImageParams4 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        // backImageParams4.width = width * 40 / 100;
        backImageParams4.height = LinearLayout.LayoutParams.MATCH_PARENT;

        backImageParams4.gravity = Gravity.CENTER;
        // backImageParams4.topMargin = (int) (height * 0.01 / 100);
        // backImageParams4.bottomMargin = (int) (height * 0.01 / 100);
        // backImageParams4.leftMargin = (int) (height * 0.25 / 100);
        // backImageParams4.rightMargin = (int) (height * 0.25 / 100);

        info_id_ignition_label_layout.setLayoutParams(backImageParams4);
        info_id_time_label_layout.setLayoutParams(backImageParams4);
    }

    public void init() {
        info_view_Back = (ImageView) findViewById(R.id.info_view_Back);
        info_id_vehicle_info_icon = (ImageView) findViewById(R.id.info_id_vehicle_info_icon);
        info_header_txt = (TextView) findViewById(R.id.info_header_txt);
        info_id_vehicleid = (TextView) findViewById(R.id.info_id_vehicleid);
        info_id_lastseentime_labelid = (TextView) findViewById(R.id.info_id_lastseentime_labelid);
        info_id_view_lastseentime = (TextView) findViewById(R.id.info_id_view_lastseentime);
        info_id_view_lastactive = (TextView) findViewById(R.id.info_id_view_lastactive);
        info_id_speed_labelid = (TextView) findViewById(R.id.info_id_speed_labelid);
        info_id_address_text_labelid = (TextView) findViewById(R.id.info_id_address_text_labelid);
        info_id_view_speed = (TextView) findViewById(R.id.info_id_view_speed);
        info_id_view_address = (TextView) findViewById(R.id.info_id_view_address);
        info_id_time_labelid = (TextView) findViewById(R.id.info_id_time_labelid);
        info_id_ignition_labelid = (TextView) findViewById(R.id.info_id_ignition_labelid);
        info_id_time = (TextView) findViewById(R.id.info_id_time);
        info_id_ignition_speed = (TextView) findViewById(R.id.info_id_ignition_speed);
        info_id_view_spped_lastactive = (TextView) findViewById(R.id.info_id_view_spped_lastactive);
        info_id_speed_labelid_separator = (TextView) findViewById(R.id.info_id_speed_labelid_separator);
        info_id_lastseentime_layout = (LinearLayout) findViewById(R.id.info_id_lastseentime_layout);
        info_id_address_layout = (LinearLayout) findViewById(R.id.info_id_address_layout);
        info_id_address_label_layout = (LinearLayout) findViewById(R.id.info_id_address_label_layout);
        info_id_ignition_layout = (LinearLayout) findViewById(R.id.info_id_ignition_layout);
        info_id_time_label_layout = (LinearLayout) findViewById(R.id.info_id_time_label_layout);
        info_id_ignition_label_layout = (LinearLayout) findViewById(R.id.info_id_ignition_label_layout);
        // info_id_lastseentime_labellayout = (LinearLayout)
        // findViewById(R.id.info_id_lastseentime_labellayout);
        info_id_speed_layout = (LinearLayout) findViewById(R.id.info_id_speed_layout);
        info_id_speed_label_layout = (LinearLayout) findViewById(R.id.info_id_speed_label_layout);

        info_odo_label = (TextView) findViewById(R.id.info_id_odo_labelid);
        info_odo_value = (TextView) findViewById(R.id.info_id_view_odo_lastseentime);

        info_id_ac_status_layout = (LinearLayout) findViewById(R.id.info_id_ac_status_layout);
        info_id_ac_status_label = (TextView) findViewById(R.id.info_id_ac_status_label);
        info_id_ac_status_value = (TextView) findViewById(R.id.info_id_ac_status_value);


        info_id_speed_labelid_separator.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_view_spped_lastactive.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_ignition_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_time.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_ignition_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_time_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_view_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_view_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_address_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_speed_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        info_id_view_lastactive.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_view_lastseentime.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_lastseentime_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        info_odo_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_odo_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        info_id_vehicleid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_header_txt.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        info_id_ac_status_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        info_id_ac_status_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));


    }

    public void setData() {
        try {
            double latitude = Double.parseDouble(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLatitude());
            double longitude = Double.parseDouble(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLongitude());


//            new GetAddressTask(info_id_view_address).execute(latitude, longitude, 1.0);

            new GetAddressTaskNew(info_id_view_address).execute(latitude, longitude, 1.0);

            //  selected_vehicleid = getcurrentmarker.getString("vehicleId");
            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Bus")) {
                info_id_vehicle_info_icon.setImageResource(R.drawable.bus_east);

            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Car")) {

                info_id_vehicle_info_icon.setImageResource(R.drawable.car_east);
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Truck")) {

                info_id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Cycle")) {

                info_id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Jcb")) {

                info_id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Bike")) {

                info_id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
            }else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("HeavyVehicle")) {

                info_id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
            }
            info_id_vehicleid.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getShortName());

            info_odo_value.setText(Constant.SELECTED_ODO_DISTANCE);

            if (Constant.SELECTED_AC_STATUS.trim().equalsIgnoreCase("yes")) {
                info_id_ac_status_value.setText("ON");
            } else {
                info_id_ac_status_value.setText("OFF");
            }


//            try {
//                map_marker_vechile_position = child.indexOf(getcurrentmarker.getString("vehicleId"));
//                if (map_marker_vechile_position == -1) {
//                    child = (ArrayList<String>) childelements.get(0);
//                    map_marker_vechile_position = child.indexOf(getcurrentmarker.getString("vehicleId"));
//                }
//            } catch (Exception e) {
//                vehicle = (ArrayList<String>) childelements.get(0);
//                map_marker_vechile_position = vehicle.indexOf(getcurrentmarker.getString("vehicleId"));
//            }
//            position = map_marker_vechile_position;
            info_id_view_speed.setText(Integer.toString(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getSpeed()) + " KMS / HR");
            info_id_view_lastseentime.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLastSeen());
            info_id_view_spped_lastactive.setText("Speed limit is " + Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getOverSpeedLimit() + " KMS / HR");
            info_id_ignition_speed.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getIgnitionStatus());
            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("P")) {
                info_id_time_labelid.setText("Parked Time");
                info_id_time.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getParkedTime())));
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("M")) {
                info_id_time_labelid.setText("Moving Time");
                info_id_time.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getMovingTime())));
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("U")) {
                info_id_time_labelid.setText("No Data Time");
                info_id_time.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getNoDataTime())));
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("S")) {
                info_id_time_labelid.setText("Idle Time");
                info_id_time.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getIdleTime())));
            } else {
                info_id_time_label_layout.setVisibility(View.GONE);
            }
//            SharedPreferences.Editor editor = sp.edit();
//            editor.putString("current_vehicleID", getcurrentmarker.getString("vehicleId"));
//            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private class GetAddressTask extends AsyncTask<Double, String, String> {
//        TextView currenta_adddress_view;
//        double getlat, getlng;
//        double address_type;
//
//        public GetAddressTask(TextView txtview) {
//            super();
//            currenta_adddress_view = txtview;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        /**
//         * Get a Geocoder instance, get the latitude and longitude
//         * look up the address, and return it
//         *
//         * @return A string containing the address of the current
//         * location, or an empty string if no address can be found,
//         * or an error message
//         * @params params One or more Location objects
//         */
//        @Override
//        protected String doInBackground(Double... params) {
//            getlat = params[0];
//            getlng = params[1];
//            address_type = params[2];
//            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//            List<Address> addresses = null;
//            try {
//                addresses = geocoder.getFromLocation(params[0], params[1], 1);
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            } catch (IllegalArgumentException e2) {
//                // Error message to post in the log
//                String errorString = "Illegal arguments " +
//                        Double.toString(params[0]) +
//                        " , " +
//                        Double.toString(params[1]) +
//                        " passed to address service";
//                e2.printStackTrace();
//                return errorString;
//            } catch (NullPointerException np) {
//                // TODO Auto-generated catch block
//                np.printStackTrace();
//            }
//            // If the reverse geocode returned an address
//            if (addresses != null && addresses.size() > 0) {
//                // Get the first address
//                Address address = addresses.get(0);
//                /*
//                 * Format the first line of address (if available),
//                 * city, and country name.
//                 */
//                String addressText = null;
//                StringBuffer addr = new StringBuffer();
//                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                    addressText = address.getAddressLine(i);
//                    addr.append(addressText + ",");
//                }
//                // Return the text
//                return addr.toString();
//            } else {
//                return "No address found";
//            }
//        }
//
//        protected void onProgressUpdate(String... progress) {
//        }
//
//        protected void onPostExecute(String result) {
//            // sourceedit.setText(result);
//            if (result != null && !result.isEmpty()) {
//                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {
//                    /***
//                     * 1.0 - vehicles current address when a vehicle is clicked
//                     * 2.0 - vehicles origin address in history selection
//                     * 3.0 - vehicles destination address in history selection
//                     * 6.0 - vehicles current address when pause button is clicked in history selection
//                     */
//                    if (address_type == 1.0) {
//                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 5.0) {
//                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 6.0) {
//                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
//                    }
//                }
//            } else {
//                Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
//                empty_fav.show();
//            }
//        }
//    }

    int addressTryCount = 1;

    private class GetAddressTaskNew extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
//        double address_type;

        public GetAddressTaskNew(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {

//            address_type = params[2];
            String addr = null;
            try {
                getlat = params[0];
                getlng = params[1];
                addr = Constant.getAddress(String.valueOf(params[0]), String.valueOf(params[1]),getStdTableValue("appid"),getString(R.string.address_api));
            } catch (Exception e1) {
                e1.printStackTrace();
            }


            if (addr != null && addr.length() > 0) {
                return addr.toString();
            } else {
                return "No address found";
            }

        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);

//            System.out.println("Hi google address address22 " + address_type + " " + result);

            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {

                    currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
//                        history_start_location_value.setText(result.equals("null") ? "No Data" : result);

                    if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                        if (addressTryCount < 3) {
//                                new GetAddressTask(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);
//
//                            } else {
                            new GetAddressTaskNew(currenta_adddress_view).execute(getlat, getlng);
                        }
                        addressTryCount++;
                    }


                }
            } else {
                Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
                empty_fav.show();
            }
        }
    }

    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }

}
