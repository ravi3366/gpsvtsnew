package com.vamosys.vamos;

import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.vamosys.model.HistoryParkingDto;
import com.vamosys.model.ParkinData;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;
import com.vamosys.utils.TypefaceUtil;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import at.markushi.ui.CircleButton;

public class HistoryNewGoogleMapActivity extends FragmentActivity implements OnMapReadyCallback,View.OnClickListener {
    boolean from_date, to_date, from_time, to_time, seekbarchanged = false, backButtonPressed = false;
    private Marker selectedMarker;
    private final Handler mHandler = new Handler();
    boolean isPauseCalled = false, isStopCalled = false;
    int mPopupStartCount = 1, mPopupEndCount = 1, mHistoryStartCount = 1, mHistoryEndCount = 1, mShowAddsPopUpCount = 1;
    int mPopupStartCountNew = 1, mPopupEndCountNew = 1, mHistoryStartCountNew = 1, mHistoryEndCountNew = 1, mShowAddsPopUpCountNew = 1;

    private static int ANIMATE_SPEEED = 3000;
    private static int ANIMATE_SPEEED_TURN = 3000;
    private static int BEARING_OFFSET = 20;
    private GoogleMap map;
    TextView vehicleId,Txt_start_location,Txt_end_location,Txt_no_parking,Txt_No_overspeed,Txt_no_distance_coverd,Txt_timeTaken;
    TextView Txt_end_time,Txt_start_time,Txt_parked_time,Txt_Moving_time,Txt_Idel_time,Txt_Nodata_time,Txt_Speed_limit,Txt_odo_distance;
    String Tagg="checkHistory";
    SlidingUpPanelLayout slidingUpPanelLayout;
    RelativeLayout relativeLayoutBottomSlideup;
    LinearLayout linearlayoutBottomSlideup;
    CardView value_speed,Speed_cardview,Distance_cardview;
    ImageView up_arrow,down_arrow;
    private Dialog dialog;
    private boolean isFromTime;
    private boolean isFromDate;
    TextView Txt_fromDate,Txt_fromTime,Txt_toDate,Txt_toTime;
    String radio_value;
    ProgressDialog progressDialog,progressDialog1;
    double mPopupStartLat = 0.0, mPopupEndLat = 0.0, mPopupStartLng = 0.0, mPopupEndLng = 0.0;
    Handler handler1;
    int progressChanged = 0;
    int currentIndex = 0;
    boolean m3Dmap = true;



    DBHelper dbhelper;
    SharedPreferences sp;
    ConnectionDetector cd;
    Const cons;
    String fromDate,fromTime,toDate,toTime,CustomfromDate,CustomfromTime,CustomtoDate,CustomtoTime;
    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss");
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    private int year, month, day, hour, minute;
    View mCustomViewMarker = null;
    String historyRepeat;
    String mHistoryResult, mSpeedLimit;
    String mParkedTime, mMovingTime, mIdleTime, mNoDataTime, mOdoDistance, mDistanceTravelled;
    Boolean pause = false;

    private HashMap<Marker, MarkerOptions> mMarkersHashMap;
    private HashMap<Marker, Integer> mMarkersPositionHashMap;
    float history_zoom_level = 16.5F;
    private List<Marker> markers = new ArrayList<Marker>();

    List<LatLng> geoPoints = new ArrayList<>();
    List<String> mPosList = new ArrayList<String>();
    List<String> mAddsList = new ArrayList<String>();
    ArrayList<String> speedlist;
    ArrayList<String> vehicle = new ArrayList<String>();
    ArrayList<String> datetimelist;
    ArrayList<String> mIdleTimeList;
    ArrayList<String> distancelist;
    int width, height;

    int speed_value=2,text_vale=1;
    Button speed;
    TextView Txt_change_speed,Txt_change_distance,Txt_change_time;

    CircleButton PlayButton,Live_history,parking_marker,change_view,navigation;
    Double currlat,currlng,startlat,startlng,endlat,endlng,parklat,parklng,Sparklat,Sparklng,Eparklat,Eparklng;
    ArrayList<ParkinData> getParking=new ArrayList<>();
    boolean is_clicked =true;
    Dialog current_address__dialog;
    String mSELECTED_MAP_TYPE = "Normal";
    Boolean Parking_isclicked=true,History_isclicked=true;

    List<HistoryParkingDto> historyParkingDtos = new ArrayList<>();
    List<HistoryParkingDto> parkingdata = new ArrayList<>();
    HashMap<String, Integer> parkingMarker = new HashMap<String, Integer>();
    private HashMap<Marker, MarkerOptions> ParkingMarkerHashMap=new HashMap<Marker, MarkerOptions>();
    //select any option
    Dialog history_alert;
    TextView mTxt_msg1,mTxt_msg2,ok_button;
    ImageView back_arrow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_new_google_map);
        handler1 = new Handler();

        dbSetup();
        inti();
        setUpSlidePanel();
        value_speed.setVisibility(View.GONE);
        Speed_cardview.setVisibility(View.GONE);
        Distance_cardview.setVisibility(View.GONE);
//        relativeLayoutBottomSlideup.setVisibility(View.GONE);
        slidingUpPanelLayout.setPanelHeight(0);
        dbhelper.deletecurrent_vehicleinfo();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        cons = new Const();
        mCustomViewMarker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Constant.SELECTED_TRIP_FROM_DATE = extras.getString("from_date");
            Constant.SELECTED_TRIP_TO_DATE = extras.getString("to_date");
            if (cd.isConnectingToInternet()) {

                // System.out.println("The from date is ::::"+Constant.SELECTED_TRIP_FROM_DATE);
                // System.out.println("The to date is ::::"+Constant.SELECTED_TRIP_TO_DATE);

                String string_from_date = Const.getTripDate(Constant.SELECTED_TRIP_FROM_DATE) + " " + Const.getTripTime(Constant.SELECTED_TRIP_FROM_DATE);
                String string_to_date = Const.getTripDate(Constant.SELECTED_TRIP_TO_DATE) + " " + Const.getTripTime(Constant.SELECTED_TRIP_TO_DATE);

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date d = null, d1 = null;
                try {
                    d = f.parse(string_from_date);

                    // System.out.println("The from time in millis :::::" + fromMilliSeconds);
                    d1 = f.parse(string_to_date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long fromMilliSeconds = d.getTime();
                long toMilliSeconds = d1.getTime();


                String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + Const.getTripDate(Constant.SELECTED_TRIP_FROM_DATE) + "&fromTime=" + Const.getTripTime(Constant.SELECTED_TRIP_FROM_DATE) + "&toDate=" + Const.getTripDate(Constant.SELECTED_TRIP_TO_DATE) + "&toTime=" + Const.getTripTime(Constant.SELECTED_TRIP_TO_DATE) + "&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                new GetVehicleHistory().execute(url);
                new GetPOIInformation().execute(par);
            } else {
                Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
                internet_toast.show();
            }
        }
        else{
            opptionPopUp();
        }
        speed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(speed_value==3){
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"3x",Toast.LENGTH_LONG).show();
                    speed.setText("3X");
                    animator.stopAnimation();
                    if (currentIndex == 0 || (currentIndex == geoPoints.size())) {
                        if (map != null) {
                            map.clear();
                        }
                    }
                    ANIMATE_SPEEED = 500;
                    ANIMATE_SPEEED_TURN = 600;
                    speed_value=1;
                    StartSpeed();
                }
                else if(speed_value==2){
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"2x",Toast.LENGTH_LONG).show();
                    speed.setText("2X");
                    animator.stopAnimation();
                    if (currentIndex == 0 || (currentIndex == geoPoints.size())) {
                        if (map != null) {
                            map.clear();
                        }
                    }
                    ANIMATE_SPEEED = 800;
                    ANIMATE_SPEEED_TURN = 800;
                    speed_value=3;
                    StartSpeed();
                }
                else if(speed_value==1){
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"1x",Toast.LENGTH_LONG).show();
                    speed.setText("1X");
                    animator.stopAnimation();
                    if (currentIndex == 0 || (currentIndex == geoPoints.size())) {
                        if (map != null) {
                            map.clear();
                        }
                    }
                    ANIMATE_SPEEED = 2000;
                    ANIMATE_SPEEED_TURN = 2000;
                    speed_value=2;
                    StartSpeed();
                }
            }
        });
    }
    public void StartSpeed(){
        Handler seekbarhandler = new Handler();
        seekbarhandler.postDelayed(new Runnable() {
            @Override
            public void run() {


//                            System.out.println("The current map zoom level is :::::" + map.getCameraPosition().zoom);

                if (map.getCameraPosition().zoom <= 3.5) {
                    Log.d("history zoom level","history1 "+history_zoom_level);
                    history_zoom_level = history_zoom_level;
                    Log.d("history zoom level","history2 "+history_zoom_level);

                } else {
                    Log.d("history zoom level","history3 "+history_zoom_level);
                    history_zoom_level = (map.getCameraPosition().zoom);
                    Log.d("history zoom level","history4 "+history_zoom_level);
                }
                PlayButton.setImageResource(R.drawable.pause_outline);
                animator.startAnimation(true);
                //marker_loop(progressChanged);
            }
        }, 500);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.setPadding(1, 1, 1, 150);
//        map.getUiSettings().setZoomControlsEnabled(true);
        LatLng latLng=new LatLng(Double.parseDouble(Constant.SELECTED_MAIN_VEHICLE_LAT),Double.parseDouble(Constant.SELECTED_MAIN_VEHICLE_LNG));
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,7.0f));
        mMarkersHashMap = new HashMap<Marker, MarkerOptions>();
        mMarkersPositionHashMap = new HashMap<Marker, Integer>();

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                Log.d("parking marker"," no parking marker");
                if (mMarkersHashMap.containsKey(marker)) {
//                    if (cd.isConnectingToInternet()) {

                    final int marPos = mMarkersPositionHashMap.get(marker);
                    map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        // Use default InfoWindow frame
                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        // Defines the contents of the InfoWindow
                        @Override
                        public View getInfoContents(Marker arg0) {

                            // Getting view from the layout file info_window_layout
                            View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                            // Getting the position from the marker
                            LatLng latLng = arg0.getPosition();

                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            height = displayMetrics.heightPixels;
                            width = displayMetrics.widthPixels;
                            LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                            LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);
                            backImageParams.width = width * 80 / 100;
                            //backImageParams.height = height * 10 / 100;
                            backImageParams.gravity = Gravity.CENTER;
                            layout.setLayoutParams(backImageParams);

                            // Getting reference to the TextView to set latitude
                            TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                            txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                            TextView tx = null;

//                            txtContent = tx;
//                        new GetAddressTask(txtContent).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);

                            StringBuffer addr = new StringBuffer();
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = null;
                            try {
                                addresses = geocoder.getFromLocation(geoPoints.get(marPos).latitude, geoPoints.get(marPos).longitude, 1);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            } catch (IllegalArgumentException e2) {
                                // Error message to post in the log
//                        String errorString = "Illegal arguments " +
//                                Double.toString(params[0]) +
//                                " , " +
//                                Double.toString(params[1]) +
//                                " passed to address service";
//                        e2.printStackTrace();
//                        return errorString;
                            } catch (NullPointerException np) {
                                // TODO Auto-generated catch block
                                np.printStackTrace();
                            }
                            // If the reverse geocode returned an address
                            if (addresses != null && addresses.size() > 0) {
                                // Get the first address
                                Address address = addresses.get(0);
                                /*
                                 * Format the first line of address (if available),
                                 * city, and country name.
                                 */
                                String addressText = null;

                                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                    addressText = address.getAddressLine(i);
                                    addr.append(addressText + ",");
                                }
                                // Return the text
                                // return addr.toString();
                            } else {
                                addr.append("No address found");
                            }


//                            if (mPosList.get(marPos).equalsIgnoreCase("P")) {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(marPos)) + " \n Parked Time : " + mIdleTimeList.get(marPos));
//                            } else if (mPosList.get(marPos).equalsIgnoreCase("S")) {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(marPos)) + " \n Idle Time : " + mIdleTimeList.get(marPos));
//                            } else {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(marPos)));
//                            }

                            String mAddsData = null;
                            if (mAddsList.get(marPos) != null) {
                                mAddsData = mAddsList.get(marPos);
                            }

                            if (mPosList.get(marPos).equalsIgnoreCase("P")) {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(marPos)) + " \n Parked Time : " + mIdleTimeList.get(marPos));
                            } else if (mPosList.get(marPos).equalsIgnoreCase("S")) {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(marPos)) + " \n Idle Time : " + mIdleTimeList.get(marPos));
                            } else {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(marPos)));
                            }


                            if (mSpeedLimit != null && speedlist.get(marPos) != null) {
                                if (Integer.valueOf(speedlist.get(marPos)) > Integer.valueOf(mSpeedLimit)) {
                                    txtContent.setText(txtContent.getText().toString().trim() + " \n Over Speed : "+speedlist.get(marPos));
                                }
                            }

                            return v;

                        }
                    });


                    LatLng markerPos = geoPoints.get(marPos);
//                    MarkerOptions markerOption = new MarkerOptions().position(markerPos);

                    MarkerOptions markerOptions2 = mMarkersHashMap.get(marker);
//                    System.out.println("Marker option laocation is :::::" + markerOptions2.getPosition());
                    Marker marker2 = map.addMarker(markerOptions2);
//                    markerOptions2.get
                    marker2.showInfoWindow();
//                        showVehicleInfoDialog(jSelectedVehicleObject, false);
//                    } else {
//                    Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
//                    internet_toast.show();
//                    }
                    return true;
                }


                return true;
            }
        });
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                marker.hideInfoWindow();
            }
        });


    }
    void dbSetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void inti(){
        back_arrow=(ImageView)findViewById(R.id.back_arrow) ;
        PlayButton=(CircleButton)findViewById(R.id.play_buttons);
        navigation=(CircleButton)findViewById(R.id.nav_buttons);
        Live_history=(CircleButton)findViewById(R.id.live_history);
        parking_marker=(CircleButton)findViewById(R.id.parking_marker);
        change_view=(CircleButton)findViewById(R.id.change_view);
        PlayButton.setOnClickListener(this);
        Live_history.setOnClickListener(this);
        parking_marker.setOnClickListener(this);
        change_view.setOnClickListener(this);
        navigation.setOnClickListener(this);

        PlayButton.setVisibility(View.GONE);
        Live_history.setVisibility(View.GONE);
        parking_marker.setVisibility(View.GONE);
        change_view.setVisibility(View.GONE);
        navigation.setVisibility(View.GONE);


        PlayButton.setImageResource(R.drawable.pause_outline);

        FragmentManager myFragmentManager = getSupportFragmentManager();
        SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                .findFragmentById(R.id.vehicle_history_map);
        myMapFragment.getMapAsync(this);

        vehicleId=(TextView)findViewById(R.id.id_show_history_vehicleid);
        vehicleId.setText(Constant.SELECTED_VEHICLE_ID);
        slidingUpPanelLayout=(SlidingUpPanelLayout)findViewById(R.id.sliding_layout);
        relativeLayoutBottomSlideup=(RelativeLayout) findViewById(R.id.relativeLayoutBottomSlideup);
        linearlayoutBottomSlideup=(LinearLayout)findViewById(R.id.LinearlayoutBottomSlideup);
        up_arrow=(ImageView)findViewById(R.id.up_arrow);
        down_arrow=(ImageView)findViewById(R.id.down_arrow);
        value_speed=(CardView)findViewById(R.id.Value_cardview);
        Speed_cardview=(CardView)findViewById(R.id.Speed_cardview);
        Distance_cardview=(CardView)findViewById(R.id.distance_cardview) ;

        Txt_start_location=(TextView)findViewById(R.id.history_start_location_value);
        Txt_end_location=(TextView)findViewById(R.id.history_end_location_value);
        Txt_no_parking=(TextView)findViewById(R.id.history_parked_count_value);
        Txt_No_overspeed=(TextView)findViewById(R.id.history_over_speed_count_value);
        Txt_no_distance_coverd=(TextView)findViewById(R.id.history_distance_covered_value);
        Txt_timeTaken=(TextView)findViewById(R.id.history_timetaken_value);
        Txt_end_time=(TextView)findViewById(R.id.history_endtime_value);
        Txt_start_time=(TextView)findViewById(R.id.histroy_start_time_value);
        Txt_parked_time=(TextView)findViewById(R.id.history_parked_value);
        Txt_Moving_time=(TextView)findViewById(R.id.history_Moving_value);
        Txt_Idel_time=(TextView)findViewById(R.id.history_idel_value);
        Txt_Nodata_time=(TextView)findViewById(R.id.history_Nodata_value);
        Txt_Speed_limit=(TextView)findViewById(R.id.history_speed_value);
        Txt_odo_distance=(TextView)findViewById(R.id.history_odo_value);
        Txt_change_distance=(TextView)findViewById(R.id.txt_distance_value) ;
        Txt_change_speed=(TextView)findViewById(R.id.Txt_speed_value) ;
        Txt_change_time=(TextView)findViewById(R.id.Txt_date_time_value) ;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        speed=(Button)findViewById(R.id.Btn_speed);
        speed.setVisibility(View.GONE);
        back_arrow.setOnClickListener(this);


    }
    private void changeMapView() {

        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(HistoryNewGoogleMapActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }

    private void setUpSlidePanel()
    {
        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }
            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState == SlidingUpPanelLayout.PanelState.DRAGGING) {

                    if (previousState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                        Log.d(Tagg,"Expanded 1");
                        relativeLayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_crop_background));
                        linearlayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_crop_background));
                        up_arrow.setVisibility(View.VISIBLE);
                        down_arrow.setVisibility(View.GONE);
                    }
                } else if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    Log.d(Tagg,"Expanded 2");

                    relativeLayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_no_crop_background));
                    linearlayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_no_crop_background));
                    up_arrow.setVisibility(View.GONE);
                    down_arrow.setVisibility(View.VISIBLE);


                } else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    Log.d(Tagg,"collapsed");

                    relativeLayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_crop_background));
                    linearlayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_crop_background));
                    up_arrow.setVisibility(View.VISIBLE);
                    down_arrow.setVisibility(View.GONE);
                }
            }
        });
    }
    private void opptionPopUp(){

        dialog = new Dialog(HistoryNewGoogleMapActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.history_custom_dialog);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        Txt_fromDate=(TextView)dialog.findViewById(R.id.select_from_date);
        Txt_toDate=(TextView)dialog.findViewById(R.id.select_to_date);
        Txt_fromTime=(TextView)dialog.findViewById(R.id.Select_from_time);
        Txt_toTime=(TextView)dialog.findViewById(R.id.select_to_time);
        final LinearLayout view_history,custom_layout,radio_layout;
        view_history=(LinearLayout)dialog.findViewById(R.id.view_history) ;
        custom_layout=(LinearLayout)dialog.findViewById(R.id.custom_layout) ;
        radio_layout=(LinearLayout)dialog.findViewById(R.id.radio_button_layout) ;
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.radio_grp);

        rg_history.clearCheck();
        rg_history.setVisibility(View.VISIBLE);
        RadioButton $Custom = (RadioButton) dialog
                .findViewById(R.id.radio_custom);
        RadioButton $Today = (RadioButton) dialog
                .findViewById(R.id.radio_Today);
        RadioButton $Yesterday = (RadioButton) dialog
                .findViewById(R.id.radio_yesterday);
        RadioButton $DayBeforeYesterday = (RadioButton) dialog
                .findViewById(R.id.radio_dayBeforesterday);
        RadioButton $LastOneHour = (RadioButton) dialog
                .findViewById(R.id.radio_lastonehour);
        $Custom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setFromDate();
                setToDate();
                radio_value="Custom";
                radio_layout.setVisibility(View.GONE);
                custom_layout.setVisibility(View.VISIBLE);
                Txt_fromDate.setText(CustomfromDate);
                Txt_fromTime.setText(CustomfromTime);
                Txt_toDate.setText(CustomtoDate);
                Txt_toTime.setText(CustomtoTime);

            }
        });
        $Today.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                radio_value=  "Today"  ;
                String today_date = cons.getDateandTime("today");
                fromDate=today_date;
                fromTime="00:00:00";
                toDate=today_date;
                toTime=cons.getCurrentTime();


            }
        });
        $Yesterday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                radio_value=  "Yesterday";
                String yesterday_date = cons.getDateandTime("yesterday");
                fromDate=yesterday_date;
                fromTime="00:00:00";
                toDate=yesterday_date;
                toTime="23:59:00";

            }
        });

        $DayBeforeYesterday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                radio_value=  "Day Before Yesterday";
                String daybeforeyesterday_date = cons.getDateandTime("daybeforeyesterday");
                fromDate=daybeforeyesterday_date;
                fromTime="00:00:00";
                toDate=daybeforeyesterday_date;
                toTime="23:59:00";

            }
        });
        $LastOneHour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                radio_value= "Last One Hour";
                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int min = cal.get(Calendar.MINUTE);
                int past_hour = cal.get(Calendar.HOUR_OF_DAY) - 1;
                String lastonehour = cons.getDateandTime("today");
                fromDate=lastonehour;
                toDate=lastonehour;
                fromTime=(new StringBuilder().append(pad(past_hour)).append(":").append(pad(min)).append(":").append("00")).toString();
                toTime=(new StringBuilder().append(pad(hour)).append(":").append(pad(min)).append(":").append("00")).toString();
            }
        });
        view_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(radio_value!=null){
                    Log.d("checkHistory","from "+fromDate+" "+fromTime+" to "+toDate+" "+toTime);
                    dialog.cancel();
                    viewMap();
                }else{
                    history_alert=new Dialog(HistoryNewGoogleMapActivity.this);
                    history_alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    history_alert.setContentView(R.layout.history_alert_msg);
                    history_alert.setCanceledOnTouchOutside(false);
                    mTxt_msg2=(TextView)history_alert.findViewById(R.id.Select_msg2);
                    mTxt_msg1=(TextView)history_alert.findViewById(R.id.Select_msg);
                    ok_button=(TextView)history_alert.findViewById(R.id.ok_button);
                    mTxt_msg2.setVisibility(View.GONE);
                    mTxt_msg1.setText("Select any one option !!");
                    ok_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            history_alert.cancel();
//                                startActivity(new Intent(HistoryNewGoogleMapActivity.this,MapMenuActivity.class));
//                                finish();
                        }
                    });
                    history_alert.show();
                }
            }
        });


        Txt_fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromDate=true;
                showDateDialog();

            }
        });
        Txt_fromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromTime=true;
                showTimeDialog();
            }
        });
        Txt_toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromDate=false;
                showDateDialog();
            }
        });
        Txt_toTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromTime=false;
                showTimeDialog();
            }
        });

    }
    private void setFromDate(){
        CustomfromDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        fromDate=Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        mFromHourValue=00;
        mFromMinuteValue=00;
//        CustomfromTime = timeFormat.format(cal.getTime());
        CustomfromTime="00:00:00";
        fromTime="00:00:00";


    }
    private void setToDate(){
        CustomtoDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        toDate=Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        final Calendar c = Calendar.getInstance();
        // Current Hour
        hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        minute = c.get(Calendar.MINUTE);
        isFromTime = false;
        SimpleDateFormat hourFormat = new SimpleDateFormat(
                "HH");
        SimpleDateFormat minuteFormat = new SimpleDateFormat(
                "mm");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        CustomtoTime = "23:59:00";
        toTime="23:59:00";
        mFromHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
        mFromMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));

    }
    private void showTimeDialog() {
        if (isFromTime) {
            hour = mFromHourValue;
            minute = mFromMinuteValue;
        } else {
            hour = mToHourValue;
            minute = mToMinuteValue;
        }

        TimePickerDialog dialog2 = new TimePickerDialog(this, timePickerListeners, hour, minute,
                false);
        dialog2.show();
    }
    private void showDateDialog() {
        if (isFromDate) {
            String[] dateArray = CustomfromDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        } else {
            String[] dateArray = CustomtoDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        }
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
        dialog.show();
    }


    private TimePickerDialog.OnTimeSetListener timePickerListeners = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;
            Log.d("checkhistory","hour "+hour+" minute "+minute);
            if (isFromTime) {
                mFromHourValue = hour;
                mFromMinuteValue = minute;
                fromTime=new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00").toString();
                Txt_fromTime.setText(fromTime);
            }
            else  {
                mToHourValue = hour;
                mToMinuteValue = minute;
                toTime=new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00").toString();
                Txt_toTime.setText(toTime);
            }

        }
    };


    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day));
            if (isFromDate) {
                fromDate = String.valueOf(mDate2);
                Txt_fromDate.setText(fromDate);
            } else {
                toDate = String.valueOf(mDate2);
                Txt_toDate.setText(toDate);
            }

        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }



    private void viewMap(){
        Log.d("checkHistory","viewmap");
        if (cd.isConnectingToInternet()) {
            if (map != null) {
                map.clear();
            }
//            relativeLayoutBottomSlideup.setVisibility(View.VISIBLE);
            String string_from_date = fromDate.toString().trim() + " " + fromTime.toString().trim();
            String string_to_date = toDate.toString().trim() + " " + toTime.toString().trim();
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = null, d1 = null;
            try {
                d = f.parse(string_from_date);

                d1 = f.parse(string_to_date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            long fromMilliSeconds = d.getTime();
            long toMilliSeconds = d1.getTime();

            String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + fromDate.toString() + "&fromTime=" + fromTime.toString() + "&toDate=" + toDate.toString() + "&toTime=" + toTime.toString() + "&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds +
                    "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
            String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
            new GetVehicleHistory().execute(url);
            new GetPOIInformation().execute(par);
        } else {
            Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
            internet_toast.show();
        }
    }
    private void playHistory(){

        is_clicked=true;
        if (geoPoints.size() > 2) {
            PlayButton.setVisibility(View.VISIBLE);
            Live_history.setVisibility(View.VISIBLE);
            parking_marker.setVisibility(View.VISIBLE);
            change_view.setVisibility(View.VISIBLE);
            speed.setVisibility(View.VISIBLE);
            PlayButton.setImageResource(R.drawable.pause_outline);
            navigation.setVisibility(View.VISIBLE);
            slidingUpPanelLayout.setPanelHeight((int) getApplicationContext().getResources().getDimension(R.dimen.size150));
            animator.startAnimation(true);
        } else {
            PlayButton.setVisibility(View.GONE);
            Live_history.setVisibility(View.GONE);
            parking_marker.setVisibility(View.GONE);
            change_view.setVisibility(View.GONE);
            speed.setVisibility(View.GONE);
            navigation.setVisibility(View.GONE);
            slidingUpPanelLayout.setPanelHeight(0);
            Toast.makeText(getApplicationContext(), "Please select other options to view history ", Toast.LENGTH_SHORT).show();
            opptionPopUp();
        }
    }
    private Animator animator= new Animator();

    public void stopAnimation() {
        isStopCalled = false;
        animator.stop();
    }

    public void startAnimation(boolean showPolyLine) {
        if (geoPoints.size() > 2) {


            animator.initialize(showPolyLine);
        } else {
//                replay_icon.setVisibility(View.GONE);
//            play_icon.setVisibility(View.GONE);
//            pause_icon.setVisibility(View.GONE);
        }
    }
    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }
    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case  R.id.back_arrow:
                startActivity(new Intent(HistoryNewGoogleMapActivity.this,MapMenuActivity.class));
                finish();
                break;

            case  R.id.nav_buttons:
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + mPopupStartLat + "," + mPopupStartLng + ""));
                startActivity(intent);
                break;
            case R.id.play_buttons:
                if(is_clicked){
                    Log.d("play clicking","play1");
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"play",Toast.LENGTH_LONG).show();
                    is_clicked=false;
                    animator.stopAnimation();
                    PlayButton.setImageResource(R.drawable.play_outline);
                    if (currentIndex == geoPoints.size()) {
                        currentIndex = 0;
                    }
                }
                else {
                    Log.d("play clicking","play2");
                    PlayButton.setImageResource(R.drawable.pause_outline);
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"pause",Toast.LENGTH_LONG).show();
                    is_clicked=true;
                    animator.startAnimation(true);
                }
                break;
            case R.id.change_view:
//                Toast.makeText(HistoryNewGoogleMapActivity.this,"change",Toast.LENGTH_LONG).show();
                changeMapView();
                break;
            case R.id.live_history:
                if(History_isclicked){
                    Log.d("play clicking","play3");
                    PlayButton.setImageResource(R.drawable.play_outline);
                    Live_history.setColor(getApplicationContext().getResources().getColor(R.color.colorPrimary));
                    Live_history.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_bule));
                    Live_history.setImageResource(R.drawable.white_history);
//                    parking_marker.setImageResource(R.drawable.parking_outline);
//                    parking_marker.setColor(getApplicationContext().getResources().getColor(R.color.white));
//                    parking_marker.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
                    is_clicked=true;
                    new ShowHistory().execute();
                    Log.d("check 1 day history","completed");
                    History_isclicked=false;
                }
                else {
                    Log.d("play clicking","play4");
                    History_isclicked=true;
                    if(!Parking_isclicked){
                        if(map!=null){
                            map.clear();
                        }
                        new ShowParking().execute();
                        animator.startAnimation(true);
                        PlayButton.setImageResource(R.drawable.pause_outline);
                        Live_history.setImageResource(R.drawable.todayhistory);
                        Live_history.setColor(getApplicationContext().getResources().getColor(R.color.white));
                        Live_history.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
                    }
                    else {
                        if(map!=null){
                            map.clear();
                        }
                        drawOldPolyine();
                        animator.startAnimation(true);
                        PlayButton.setImageResource(R.drawable.pause_outline);
                        Live_history.setImageResource(R.drawable.todayhistory);
                        Live_history.setColor(getApplicationContext().getResources().getColor(R.color.white));
                        Live_history.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
                    }

//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"Double history",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.parking_marker:
                if(Parking_isclicked){
//                    animator.stopAnimation();
                    Log.d("play clicking","play5");
                    parking_marker.setColor(getApplicationContext().getResources().getColor(R.color.colorPrimary));
                    parking_marker.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_bule));
                    parking_marker.setImageResource(R.drawable.white_parking);
                    PlayButton.setImageResource(R.drawable.play_outline);
//                    Live_history.setImageResource(R.drawable.todayhistory);
//                    Live_history.setColor(getApplicationContext().getResources().getColor(R.color.white));
//                    Live_history.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
                    is_clicked=true;
                    new ShowParking().execute();
                    Parking_isclicked=false;
                }
                else {
                    Log.d("play clicking","play6");
                    Parking_isclicked=true;
                    if(!History_isclicked){
                        if(map!=null){
                            map.clear();
                        }
                        new ShowHistory().execute();
                        drawOldPolyine();
                        animator.startAnimation(true);
                        PlayButton.setImageResource(R.drawable.pause_outline);
                        parking_marker.setImageResource(R.drawable.parking_outline);
                        parking_marker.setColor(getApplicationContext().getResources().getColor(R.color.white));
                        parking_marker.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
                    }
                    else{
                        if(map!=null){
                            map.clear();
                        }
                        drawOldPolyine();
                        animator.startAnimation(true);
                        PlayButton.setImageResource(R.drawable.pause_outline);
                        parking_marker.setImageResource(R.drawable.parking_outline);
                        parking_marker.setColor(getApplicationContext().getResources().getColor(R.color.white));
                        parking_marker.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
                    }
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"Double parking",Toast.LENGTH_LONG).show();
                }
                break;


        }

    }

    private class ShowParking extends AsyncTask<String,String,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(Tagg,"showprogress");
            ShowProgress();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            animator.stopAnimation();
            animator.startAnimation(true);
            PlayButton.setImageResource(R.drawable.pause_outline);
            PolylineOptions options = new PolylineOptions().width(6).color(getResources().getColor(R.color.black)).geodesic(true);
//            if(map!=null){
//                map.clear();
//            }
            if(historyParkingDtos!=null && historyParkingDtos.size()>0){
                for(int i=0;i<historyParkingDtos.size();i++){
                    HistoryParkingDto h = historyParkingDtos.get(i);
                    HistoryParkingDto h1=null;
                    if(i==0){
                        startlat =h.getLatitude() ;
                        startlng = h.getLongitude();
                    }
                    if(i==historyParkingDtos.size()-1){
                        endlat = h.getLatitude();
                        endlng = h.getLongitude();
                        Log.d("endlatti",""+endlat);
                        Log.d("endlongii",""+endlng);
                    }

                    currlat = h.getLatitude();
                    currlng = h.getLongitude();
                    Log.d("currlatti",""+currlat);
                    Log.d("currlongii",""+currlng);
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currlat, currlng), 9.5f));
                    LatLng position =new LatLng(currlat,currlng);
                    options.add(position);
                }

                Log.d("startlat",""+startlat);
                Log.d("startlng",""+startlng);
                View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.trackingmarker, null);
                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(startlat, startlng));
                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, marker)));
                View Endmarker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.endtrackingmarker, null);
                MarkerOptions EndmarkerOption = new MarkerOptions().position(new LatLng(endlat, endlng));
                EndmarkerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, Endmarker)));
                map.addPolyline(options);
//                map.addMarker(markerOption);
                map.addMarker(EndmarkerOption);
                map.addPolyline(options);
                View Parkmarker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.parking_marker, null);
                int count=0,title=0;
                for(int x=0;x<parkingdata.size();x++){
                    Log.d("parking","parking detail");
                    final HistoryParkingDto h = parkingdata.get(x);
                    if(h.getPosition().equalsIgnoreCase("P")){
                        title=x+1;
                        parklat =h.getLatitude();
                        parklng =h.getLongitude();
                        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                            // Use default InfoWindow frame
                            @Override
                            public View getInfoWindow(Marker arg0) {
                                return null;
                            }

                            // Defines the contents of the InfoWindow
                            @Override
                            public View getInfoContents(Marker arg0) {

                                // Getting view from the layout file info_window_layout
                                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                                // Getting the position from the marker
                                LatLng latLng = arg0.getPosition();

                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                height = displayMetrics.heightPixels;
                                width = displayMetrics.widthPixels;
                                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                backImageParams.width = width * 80 / 100;
                                //backImageParams.height = height * 10 / 100;
                                backImageParams.gravity = Gravity.CENTER;
                                layout.setLayoutParams(backImageParams);

                                // Getting reference to the TextView to set latitude
                                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

                                txtContent.setText(h.getAddress() + " " + cons.getTimefromserver(String.valueOf(h.getDate())) + " \n Parked Time : " + Const.getVehicleTime(h.getParkedTime()));

                                return v;

                            }
                        });
                        MarkerOptions ParkmarkerOption = new MarkerOptions().position(new LatLng(parklat, parklng));
                        ParkmarkerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, Parkmarker)));
//                        map.addMarker(ParkmarkerOption);
                        Log.d("parking","parking detail"+" "+parklat+" "+parklng);
                        count++;
                        Marker currentMarker = map.addMarker(ParkmarkerOption);
                        Log.d("test",currentMarker.getId()+" "+x);
                        parkingMarker.put(currentMarker.getId(),x);
                        ParkingMarkerHashMap.put(currentMarker,ParkmarkerOption);
                        Log.d("test","parking "+currentMarker+" "+ParkmarkerOption);
                        currentMarker.showInfoWindow();
                    }
                    map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(final Marker marker) {
//                            Toast.makeText(HistoryNewGoogleMapActivity.this,""+marker.getId()+" "+parkingMarker.get(marker.getId()),Toast.LENGTH_LONG).show();
                            Log.d("test",""+marker.getId());
                            map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                // Use default InfoWindow frame
                                @Override
                                public View getInfoWindow(Marker arg0) {
                                    return null;
                                }

                                // Defines the contents of the InfoWindow
                                @Override
                                public View getInfoContents(Marker arg0) {
                                    Log.d("test","1 "+marker.getId());
                                    // Getting view from the layout file info_window_layout
                                    View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                                    // Getting the position from the marker
                                    LatLng latLng = arg0.getPosition();

                                    DisplayMetrics displayMetrics = new DisplayMetrics();
                                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                    height = displayMetrics.heightPixels;
                                    width = displayMetrics.widthPixels;
                                    LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                                    LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.WRAP_CONTENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT);
                                    backImageParams.width = width * 80 / 100;
                                    //backImageParams.height = height * 10 / 100;
                                    backImageParams.gravity = Gravity.CENTER;
                                    layout.setLayoutParams(backImageParams);

                                    // Getting reference to the TextView to set latitude
                                    TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                                    txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
                                    if(parkingMarker.get(marker.getId())!=null){
                                        Log.d("test","1 "+marker.getId());
                                        HistoryParkingDto h = parkingdata.get(parkingMarker.get(marker.getId()));
                                        txtContent.setText(h.getAddress() + " " + cons.getTimefromserver(String.valueOf(h.getDate())) + " \n Parked Time : " + Const.getVehicleTime(h.getParkedTime()));
                                    }
                                    else {
                                        Log.d("test","2 "+marker.getId());
                                        HistoryParkingDto h = parkingdata.get(0);
                                        txtContent.setText(h.getAddress() + " " + cons.getTimefromserver(String.valueOf(h.getDate())) + " \n Parked Time : " + Const.getVehicleTime(h.getParkedTime()));
                                    }
                                    return v;

                                }
                            });
                            MarkerOptions markerOptions2 = ParkingMarkerHashMap.get(marker);
                            Marker marker2 = map.addMarker(markerOptions2);
                            marker2.showInfoWindow();
                            return true;
                        }
                    });
                    Log.d("parking","count "+count);
                }

            }
            Log.d(Tagg,"Hideprogress");
            HideProgress();
        }

        @Override
        protected String doInBackground(String... strings) {
            int count=0;
            double lat,lng;
            for(int x=0;x<historyParkingDtos.size();x++){
                Log.d("parking","parking detail");
                HistoryParkingDto h = historyParkingDtos.get(x);
                HistoryParkingDto parkdata= new HistoryParkingDto();
                if(h.getPosition().equalsIgnoreCase("P")){
                    count++;
                    lat=h.getLatitude();
                    lng=h.getLongitude();
                    parkdata.setLatitude(lat);
                    parkdata.setLongitude(lng);
                    parkdata.setPosition(h.getPosition());
                    parkdata.setAddress(h.getAddress());
                    parkdata.setDate(h.getDate());
                    parkdata.setParkedTime(h.getParkedTime());
                    parkingdata.add(parkdata);
                    Log.d("parking","parking detail"+parkdata.getLatitude());
                }
                Log.d("parking","count1 "+count);
            }
            return "show parking";
        }
    }

    private class ShowHistory extends AsyncTask<String,String,String>{
        @Override
        protected String doInBackground(String... strings) {
            for (int i = 0; i < historyParkingDtos.size(); i++) {
                HistoryParkingDto h = historyParkingDtos.get(i);
                if(i==0){
                    startlat =h.getLatitude() ;
                    startlng = h.getLongitude();
                }
                if(i==historyParkingDtos.size()-1){
                    endlat = h.getLatitude();
                    endlng = h.getLongitude();
                    Log.d("endlatti",""+endlat);
                    Log.d("endlongii",""+endlng);
                }
            }

            return "show history";
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowProgress();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            PolylineOptions options = new PolylineOptions().width(6).color(getResources().getColor(R.color.black)).geodesic(true);
//            if(map!=null){
//                map.clear();
//            }
//            animator.stopAnimation();
            animator.startAnimation(true);
            PlayButton.setImageResource(R.drawable.pause_outline);
            if(historyParkingDtos!=null && historyParkingDtos.size()>0){
                for(int i=0;i<historyParkingDtos.size();i++){
                    HistoryParkingDto h = historyParkingDtos.get(i);
                    currlat = h.getLatitude();
                    currlng = h.getLongitude();
                    Log.d("currlatti",""+currlat);
                    Log.d("currlongii",""+currlng);
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currlat, currlng), 9.5f));
                    LatLng position =new LatLng(currlat,currlng);
                    options.add(position);
                }
                Log.d("startlat",""+startlat);
                Log.d("startlng",""+startlng);
                View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.trackingmarker, null);
                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(startlat, startlng));
                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, marker)));
                View Endmarker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.endtrackingmarker, null);
                MarkerOptions EndmarkerOption = new MarkerOptions().position(new LatLng(endlat, endlng));
                EndmarkerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, Endmarker)));
                map.addPolyline(options);
//                map.addMarker(markerOption);
                map.addMarker(EndmarkerOption);
                map.addPolyline(options);
            }
            HideProgress();
        }


    }



    public void drawOldPolyine(){
        Log.d("draw old polyline","draw");
        List<HistoryParkingDto> parkingdata = new ArrayList<>();
        PolylineOptions options = new PolylineOptions().width(6).color(getResources().getColor(R.color.history_bg_color)).geodesic(true);
        if(historyParkingDtos.size()>2){
//            if(map!=null){
//                map.clear();
//            }
            for(int i=0;i<=currentIndex;i++){
                Log.d("draw old polyline1","draw");
                HistoryParkingDto h = historyParkingDtos.get(i);
                HistoryParkingDto parkdata= new HistoryParkingDto();
                currlat = h.getLatitude();
                currlng = h.getLongitude();
                Log.d("currlatti",""+currlat);
                Log.d("currlongii",""+currlng);
                LatLng position =new LatLng(currlat,currlng);
                options.add(position);
                map.addPolyline(options);
                if(h.getPosition().equalsIgnoreCase("P")){
                    MarkerOptions parkingMarker = new MarkerOptions().position(new LatLng(h.getLatitude(),h.getLongitude()));
                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);
                    id_custom_marker_icon.setImageResource(R.drawable.icon_parking);
                    parkingMarker.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));
                    map.addMarker(parkingMarker);
                }
                else if(h.getPosition().equalsIgnoreCase("S")){
                    MarkerOptions IdelMarker = new MarkerOptions().position(new LatLng(h.getLatitude(),h.getLongitude()));
                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);
                    id_custom_marker_icon.setImageResource(R.drawable.icon_idle);
                    IdelMarker.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));
                    map.addMarker(IdelMarker);
                }
                else if(h.getSpeed()>Integer.valueOf(mSpeedLimit)){
                    MarkerOptions speedMarker = new MarkerOptions().position(new LatLng(h.getLatitude(),h.getLongitude()));
                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);
                    id_custom_marker_icon.setImageResource(R.drawable.over_speed_new_icon);
                    speedMarker.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));
                    map.addMarker(speedMarker);
                }
            }
            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.trackingmarker, null);
            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(startlat, startlng));
            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, marker)));
            map.addMarker(markerOption);
            Log.d("parking","total "+parkingdata.size());
            for(int i=0;i<=currentIndex;i++){
                HistoryParkingDto h = historyParkingDtos.get(i);
//                if(h.getPosition().equalsIgnoreCase("P")){
//                    MarkerOptions parkingMarker = new MarkerOptions().position(new LatLng(h.getLatitude(),h.getLongitude()));
//                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
//                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
//                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
//                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
//                    img_mark1.setVisibility(View.GONE);
//                    img_mark2.setVisibility(View.GONE);
//                    img_mark3.setVisibility(View.GONE);
//                    id_custom_marker_icon.setVisibility(View.VISIBLE);
//                    id_custom_marker_icon.setImageResource(R.drawable.icon_parking);
//                    parkingMarker.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));
//                    map.addMarker(parkingMarker);
//                }
//                else if(h.getPosition().equalsIgnoreCase("S")){
//                    MarkerOptions IdelMarker = new MarkerOptions().position(new LatLng(h.getLatitude(),h.getLongitude()));
//                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
//                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
//                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
//                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
//                    img_mark1.setVisibility(View.GONE);
//                    img_mark2.setVisibility(View.GONE);
//                    img_mark3.setVisibility(View.GONE);
//                    id_custom_marker_icon.setVisibility(View.VISIBLE);
//                    id_custom_marker_icon.setImageResource(R.drawable.icon_idle);
//                   IdelMarker.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));
//                    map.addMarker(IdelMarker);
//                }
//                else if(h.getSpeed()<Integer.valueOf(mSpeedLimit)){
//                    MarkerOptions speedMarker = new MarkerOptions().position(new LatLng(h.getLatitude(),h.getLongitude()));
//                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
//                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
//                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
//                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
//                    img_mark1.setVisibility(View.GONE);
//                    img_mark2.setVisibility(View.GONE);
//                    img_mark3.setVisibility(View.GONE);
//                    id_custom_marker_icon.setVisibility(View.VISIBLE);
//                    id_custom_marker_icon.setImageResource(R.drawable.over_speed_new_icon);
//                    speedMarker.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));
//                    map.addMarker(speedMarker);
//                }

            }
//            map.addPolyline(options);
        }
    }

    public class GetVehicleHistory extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(HistoryNewGoogleMapActivity.this);
            progressDialog.show();
        }

        protected void onProgressUpdate(Integer... values) {
            progressDialog.show();
        }

        @Override
        public String doInBackground(String... urls) {
            Log.d(Tagg,"getvehicleHistory");
            String response_from_server = "";
            if (urls[0].equals("replay")) {
                response_from_server = sp.getString("historyplaybackdata", "");
            } else {
                try {
                    HttpConfig ht = new HttpConfig();
                    response_from_server = ht.httpGet(urls[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return response_from_server;
        }

        @Override
        public void onPostExecute(String result) {
            Log.d(Tagg,"getHis on post");
            if (result != null && !result.isEmpty()) {
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("historyplaybackdata", result);
                editor.commit();
//                value_speed.setVisibility(View.VISIBLE);
//                Speed_cardview.setVisibility(View.VISIBLE);
//                Distance_cardview.setVisibility(View.VISIBLE);
                historyData(result);
                historyPlayBack(result, progressDialog);
                historyRepeat = result;
            } else {
                Toast.makeText(getApplicationContext(), "No History Data Available", Toast.LENGTH_SHORT).show();
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                startActivity(new Intent(HistoryNewGoogleMapActivity.this, VehicleListActivity.class));
                finish();
            }
        }
    }
    private  void  ShowProgress(){
        progressDialog1 = new ProgressDialog(HistoryNewGoogleMapActivity.this,
                AlertDialog.THEME_HOLO_LIGHT);
        progressDialog1.setMessage("Please Wait...");
        progressDialog1.setProgressDrawable(new ColorDrawable(
                Color.BLUE));
        progressDialog1.setCancelable(true);
        progressDialog1.setCanceledOnTouchOutside(false);
        progressDialog1.show();
    }
    private  void HideProgress(){
        Log.d(Tagg,"hideprogress");
        if (progressDialog1.isShowing()) {
            progressDialog1.dismiss();
        }
    }

    public  void historyData(String result){
        if(historyParkingDtos!=null){
            historyParkingDtos.clear();
        }
        try {
            JSONObject jsonObject1= new JSONObject(result);
            JSONArray jsonArray=null;
            if(result!=null){
                if(jsonObject1.has("history4Mobile")&&jsonObject1.get("history4Mobile")!=null){
                    jsonArray=jsonObject1.getJSONArray("history4Mobile");
                    JSONObject getData=null;
                    for(int i=0;i<jsonArray.length();i++){
                        getData=jsonArray.getJSONObject(i);
                        HistoryParkingDto h= new HistoryParkingDto();
                        //                                primaryEngine: "OFF"
                        if(getData.has("lt")&&getData.get("lt")!=null){
                            h.setLatitude(getData.getDouble("lt"));
                        }
                        if(getData.has("lg")&&getData.get("lg")!=null){
                            h.setLongitude(getData.getDouble("lg"));
                        }
                        if(getData.has("sp")&&getData.get("sp")!=null){
                            h.setSpeed(getData.getInt("sp"));
                        }
                        if(getData.has("dt")&&getData.get("dt")!=null){
                            h.setDate(getData.getLong("dt"));
                        }
                        if(getData.has("ps")&&getData.get("ps")!=null){
                            h.setPosition(getData.getString("ps"));
                        }
                        if(getData.has("pt")&&getData.get("pt")!=null){
                            h.setParkedTime(getData.getString("pt"));
                        }
                        if(getData.has("address")&&getData.get("address")!=null){
                            h.setAddress(getData.getString("address"));
                        }
                        if(getData.has("primaryEngine")&&getData.get("primaryEngine")!=null){
                            h.setIgnitionStatus(getData.getString("lt"));
                        }

                        historyParkingDtos.add(h);
                    }
                }
            }
            Log.d(Tagg,"historyParkingDtos "+historyParkingDtos.size());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public class GetPOIInformation extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        public String doInBackground(String... urls) {
            String response_from_server = "";
            try {

//                System.out.println("The POI url is :::::" + urls[0]);
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(urls[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        public void onPostExecute(String result) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("poidata", result);
            editor.commit();
            drawPoiPoints(result);
        }

    }
    public void drawPoiPoints(String poi_data) {
        if (poi_data != null && poi_data.length() != 0) {
            try {
                JSONObject parentobject = new JSONObject(poi_data);
                if (!parentobject.isNull("geoFence")) {
                    int vehicles_history_length;
                    final ArrayList<LatLng> points = new ArrayList<>();
                    JSONArray vehiclearray = parentobject.getJSONArray("geoFence");
                    vehicles_history_length = vehiclearray.length();
                    for (int z = 0; z < vehicles_history_length; z++) {
                        if (!vehiclearray.get(z).equals(null)) {
                            ParsingClass parse = new ParsingClass();
                            parse.getPoiInformation(vehiclearray.getJSONObject(z).toString());
                            double lat = parse.getpoiLatitude();
                            double lng = parse.getpoiLongitude();
                            LatLng position = new LatLng(lat, lng);

//                            System.out.println("The poi points are :::::" + lat + " " + lng);

                            points.add(position);
                            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custompoimarker, null);
                            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, marker)));
                            Marker currentMarker = map.addMarker(markerOption);
                        }
                    }
                }
            } catch (JSONException je) {
                je.printStackTrace();
            }
        }
    }
    public void historyPlayBack(final String result, final ProgressDialog dialog) {

        String totalrows = null;
        if (result != null && result.length() != 0) {
            mHistoryResult = result;
            if (map != null) {
                map.clear();
            }

            try {

                if (mMarkersHashMap != null) {
                    mMarkersHashMap.clear();
                }

                if (mMarkersPositionHashMap != null) {
                    mMarkersPositionHashMap.clear();
                }

                ParsingClass parsingClass = new ParsingClass();
                parsingClass.historyParse(result);
                int vehicles_history_length;
                geoPoints = new ArrayList<LatLng>();
                speedlist = new ArrayList<String>();
                datetimelist = new ArrayList<String>();
                distancelist = new ArrayList<String>();
                mIdleTimeList = new ArrayList<String>();
                if (!geoPoints.isEmpty()) {
                    geoPoints.clear();
                    //  directionlist.clear();
                    mPosList.clear();
                    speedlist.clear();
                    datetimelist.clear();
                    distancelist.clear();
                    mIdleTimeList.clear();
                    mAddsList.clear();
                }
                String vehicle_short_name;
                PolylineOptions options = new PolylineOptions().width(8);
                PolylineOptions overspeed_options = new PolylineOptions().width(4).geodesic(true);
                vehicle_short_name = parsingClass.getshortname();
                vehicleId.setText((parsingClass.getshortname().equals("null") ? "No Data" : parsingClass.getshortname()));

                Log.d("vehiclehistory",""+parsingClass.getvehicleType());

                Txt_no_distance_coverd.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance() + " KMS"));


                Txt_start_time.setText(cons.getTripTimefromserver((parsingClass.getFromDateTimeUTC().equals("null") ? "No Data" : parsingClass.getFromDateTimeUTC())));
                Txt_end_time.setText(cons.getTripTimefromserver((parsingClass.getToDateTimeUTC().equals("null") ? "No Data" : parsingClass.getToDateTimeUTC())));

                Txt_No_overspeed.setText(String.valueOf(parsingClass.getmOverSpeedCount()));
                Txt_no_parking.setText(String.valueOf(parsingClass.getmParkedCount()));


                mSpeedLimit = parsingClass.getOverSpeedLimit();
                mOdoDistance = parsingClass.getodoDistance();
                mParkedTime = parsingClass.getTotalParkedTime();
                mIdleTime = parsingClass.getTotalIdleTime();
                mMovingTime = parsingClass.getTotalMovingTime();
                mNoDataTime = parsingClass.getTotalNoDataTime();
                mDistanceTravelled = parsingClass.gettripDistance();
                totalrows = parsingClass.gettotalRows();
                Txt_timeTaken.setText(cons.getVehicleTime(parsingClass.gettotalRunningTime()));
                try {
                    JsonFactory f = new MappingJsonFactory();
                    JsonParser historyjsonParser = f.createJsonParser(result);
                    JsonToken current;
                    current = historyjsonParser.nextToken();
                    if (current != JsonToken.START_OBJECT) {
                        return;
                    }
                    while (historyjsonParser.nextToken() != JsonToken.END_OBJECT) {
                        String fieldName = historyjsonParser.getCurrentName();
                        if (fieldName.equals("history4Mobile")) {
                            current = historyjsonParser.nextToken();
                            if (current == JsonToken.START_ARRAY) {
                                int current_node_index = 0;
                                while (historyjsonParser.nextToken() != JsonToken.END_ARRAY) {
                                    // read the record into a tree model,
                                    // this moves the parsing position to the end of it
                                    JsonNode node = historyjsonParser.readValueAsTree();
                                    // And now we have random access to everything in the object
                                    if (!node.isNull()) {
                                        parsingClass.historyobjectparsing(node);
                                        double lat = parsingClass.getCurr_lat();
                                        double lng = parsingClass.getCurr_long();
                                        LatLng position = new LatLng(lat, lng);
                                        geoPoints.add(position);
                                        mPosList.add(parsingClass.getPosition());
                                        mAddsList.add(parsingClass.getAddress());
                                        MarkerOptions markerOption = new MarkerOptions().position(position);
                                        options.add(position);
                                        options.color(getResources().getColor(R.color.history_polyline_color));
                                        speedlist.add(parsingClass.getSpeed());
                                        datetimelist.add(parsingClass.getlastseen());
                                        distancelist.add(parsingClass.getDistanCovered());
                                        if (parsingClass.getparked_time() != null) {
                                            mIdleTimeList.add(parsingClass.getparked_time());
                                        } else {
                                            mIdleTimeList.add("");
                                        }
                                        Marker currentMarker = null;
                                        if (Integer.parseInt(totalrows) == 1){
                                            mPopupEndLat = parsingClass.getCurr_lat();
                                            mPopupEndLng = parsingClass.getCurr_long();
                                            Log.d("mPopupEndLat",""+mPopupEndLat);
                                            Log.d("mPopupEndLng",""+mPopupEndLng);
                                        }
                                        if (current_node_index == 0) {
                                            mPopupStartLat = parsingClass.getCurr_lat();
                                            mPopupStartLng = parsingClass.getCurr_long();
                                        } else if (current_node_index == Integer.parseInt(totalrows) - 1) {
                                            mPopupEndLat = parsingClass.getCurr_lat();
                                            mPopupEndLng = parsingClass.getCurr_long();
                                            Log.d("parsingend",""+mPopupEndLat);
                                            Log.d("parsingendlng",""+mPopupEndLat);

                                        }

                                    }
                                    current_node_index++;
                                }
                            }
                        }
                    }
                    Log.d(Tagg,"add List "+mAddsList.size());
                    Txt_start_location.setText(mAddsList.get(0));
                    Txt_end_location.setText(mAddsList.get(mAddsList.size()-1));
                    if (mSpeedLimit != null) {
                        Txt_Speed_limit.setText(mSpeedLimit + " Km/hr");
                    } else {
                        Txt_Speed_limit.setText("0 Km/hr");
                    }

                    if (mOdoDistance != null) {
                        Txt_odo_distance.setText(mOdoDistance + " Kms");
                    } else {
                        Txt_odo_distance.setText("0 Km");
                    }

                    Txt_parked_time.setText(mParkedTime);
                    Txt_Idel_time.setText(mIdleTime);
                    Txt_Moving_time.setText(mMovingTime);
                    Txt_Nodata_time.setText(mNoDataTime);
                } catch (IOException e) {
                    e.printStackTrace();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                pause = false;
                backButtonPressed = false;

            } catch (Exception je) {
                je.printStackTrace();
            } finally {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
            addMarkerToMap();
        }
        else {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            Toast.makeText(getApplicationContext(), "No History Data Available for the Selected Intervals", Toast.LENGTH_SHORT).show();
        }

//        newCutomizedHistoryInfoPopupWindow();
        playHistory();
    }
    private class GetAddressTaskNew extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTaskNew(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {

            address_type = params[2];
            String addr = null;
            try {
                addr = Constant.getAddress(String.valueOf(params[0]), String.valueOf(params[1]),getStdTableValue("appid"),getString(R.string.address_api));
            } catch (Exception e1) {
                e1.printStackTrace();
            }


            if (addr != null && addr.length() > 0) {
                return addr.toString();
            } else {
                return "No address found";
            }

        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);

            System.out.println("Hi google address address22 " + address_type + " " + result);

            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {


                    if (address_type == 1.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("address", result);
                        editor.commit();
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 2.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("vehicle_origin_address", result);
                        editor.commit();


                    } else if (address_type == 3.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("vehicle_destination_address", result);
                        editor.commit();


                    } else if (address_type == 4.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("current_vehicle_address", result);
                        editor.commit();
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);

                        if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                            if (mPopupStartCountNew < 5) {
//                                new GetAddressTask(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);
//
//                            } else {
                                new GetAddressTaskNew(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);
                            }
                            mPopupStartCountNew++;
                        }


                    } else if (address_type == 5.0) {

                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("current_vehicle_address", result);
                        editor.commit();

                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);


                        if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                            if (mPopupEndCountNew < 5) {
//                                new GetAddressTask(currenta_adddress_view).execute(mPopupEndLat, mPopupEndLng, 5.0);
//                            } else {
                                new GetAddressTaskNew(currenta_adddress_view).execute(mPopupEndLat, mPopupEndLng, 5.0);
                            }
                            mPopupEndCountNew++;
                        }


                    } else if (address_type == 6.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                        Log.d("check address","6.0.0");
                        if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                            if (mShowAddsPopUpCountNew < 5) {
                                Log.d("check address","6.0.1");
                                new GetAddressTaskNew(currenta_adddress_view).execute(getlat, getlng, 6.0);
                            } else {
                                Log.d("check address","6.0.2");
                            }
                            mShowAddsPopUpCountNew++;
                        }


                    }

                }
            } else {
                Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
                empty_fav.show();
            }
        }
    }
    private class GetAddressTask extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTask(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {
            getlat = params[0];
            getlng = params[1];
            address_type = params[2];
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;

//            Constant.getAddress(String.valueOf(params[0]), String.valueOf(params[1]));

            try {
                addresses = geocoder.getFromLocation(params[0], params[1], 5);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(params[0]) +
                        " , " +
                        Double.toString(params[1]) +
                        " passed to address service";
                e2.printStackTrace();
                return errorString;
            } catch (NullPointerException np) {
                // TODO Auto-generated catch block
                np.printStackTrace();
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                String addressText = null;
                StringBuffer addr = new StringBuffer();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressText = address.getAddressLine(i);
                    addr.append(addressText + ",");
                }
                // Return the text

                if (addr != null && addr.length() > 0) {
                    return addr.toString();
                } else {
                    return "No address found";
                }
            } else {
                return "No address found";
            }
        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);

            System.out.println("Hi geocoder address " + address_type + " " + result);

            if (result != null && !result.isEmpty()) {
//                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {
                /***
                 * 1.0 - vehicles current address when a vehicle is clicked
                 * 2.0 - vehicles origin address in history selection
                 * 3.0 - vehicles destination address in history selection
                 * 6.0 - vehicles current address when pause button is clicked in history selection
                 */

//                    System.out.println("The address is ::::" + result);

                if (address_type == 1.0) {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("address", result);
                    editor.commit();
                    currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                } else if (address_type == 2.0) {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("vehicle_origin_address", result);
                    editor.commit();

                    if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                        if (mHistoryStartCount < 5) {
                            new GetAddressTask(Txt_start_location).execute(mPopupStartLat, mPopupStartLng, 2.0);
                        }
                        mHistoryStartCount++;
                    }

                } else if (address_type == 3.0) {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("vehicle_destination_address", result);
                    editor.commit();

                    if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                        if (mHistoryEndCount < 5) {
                            new GetAddressTask(Txt_end_location).execute(mPopupEndLat, mPopupEndLng, 3.0);
                        }
                        mHistoryEndCount++;
                    }

                } else if (address_type == 4.0) {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("current_vehicle_address", result);
                    editor.commit();

                    if (mPopupStartLat != 0 && mPopupStartLng != 0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                        if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                            if (mPopupStartCount < 5) {
                                new GetAddressTask(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);

                            } else {
                                new GetAddressTaskNew(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);
                            }
                            mPopupStartCount++;
                        }
                    } else {
                        currenta_adddress_view.setText("Start location not available");
                    }


                } else if (address_type == 5.0) {

                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("current_vehicle_address", result);
                    editor.commit();
                    if (mPopupEndLat != 0 && mPopupEndLng != 0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                        if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                            if (mPopupEndCount < 5) {
                                new GetAddressTask(currenta_adddress_view).execute(mPopupEndLat, mPopupEndLng, 5.0);
                            } else {
                                new GetAddressTaskNew(currenta_adddress_view).execute(mPopupEndLat, mPopupEndLng, 5.0);
                            }
                            mPopupEndCount++;
                        }
                    } else {
                        currenta_adddress_view.setText("End location not available");

                    }


                } else if (address_type == 6.0) {
                    currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    Log.d("check address","6.0");
                    if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                        if (mShowAddsPopUpCount < 5) {
                            Log.d("check address","6.1");
                            new GetAddressTask(currenta_adddress_view).execute(getlat, getlng, 6.0);
                        } else {
                            Log.d("check address","6.2");
                            new GetAddressTaskNew(currenta_adddress_view).execute(getlat, getlng, 6.0);
                        }
                        mShowAddsPopUpCount++;
                    }

                }
            }
//            } else {
//                Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
//                empty_fav.show();
//            }
        }
    }
    public void showCurrentAddress() {
        current_address__dialog = new Dialog(HistoryNewGoogleMapActivity.this);
        current_address__dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        current_address__dialog.setContentView(R.layout.custom_marker_dialog);
        current_address__dialog.setCancelable(true);
        TextView id_historyplayback_parkedtime_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_parkedtime_text_labelid);
        TextView id_view_parkedtime = (TextView) current_address__dialog.findViewById(R.id.id_view_parkedtime);
        TextView id_historyplayback_parkedduration_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_parkedduration_text_labelid);
        TextView id_view_parkedduration = (TextView) current_address__dialog.findViewById(R.id.id_view_parkedduration);
        TextView id_historyplayback_addres_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_addres_text_labelid);
        TextView id_view_address = (TextView) current_address__dialog.findViewById(R.id.id_view_address);
        id_historyplayback_parkedtime_text_labelid.setVisibility(View.GONE);
        id_view_parkedtime.setVisibility(View.GONE);
        id_historyplayback_parkedduration_text_labelid.setVisibility(View.GONE);
        id_view_parkedduration.setVisibility(View.GONE);
        id_historyplayback_addres_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_view_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        new GetAddressTask(id_view_address).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);
        current_address__dialog.show();
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }
    public void addMarkerToMap() {
        // play_icon.setVisibility(View.VISIBLE);
        //  pause_icon.setVisibility(View.VISIBLE);
        // replay_icon.setVisibility(View.VISIBLE);
        progressChanged = 5;
        setAnimateTime();
        if (geoPoints.size() > 0) {
            for (int a = 0; a < geoPoints.size(); a++) {

                addMarkerToMap(geoPoints.get(a));

            }
        }
    }
    public void addMarkerToMap(LatLng latLng) {
        Marker marker = map.addMarker(new MarkerOptions().position(latLng)
                .title("title")
                .snippet("snippet"));
        marker.setVisible(false);
        markers.add(marker);

    }

    public void setAnimateTime() {


        // System.out.println("Progress changed is :::::" + progressChanged + " history max size is :::::" + history_seekbar.getMax());
        switch (progressChanged) {

            case 1:
                ANIMATE_SPEEED = 7000;
                ANIMATE_SPEEED_TURN = 7000;
                break;
            case 2:
                ANIMATE_SPEEED = 6000;
                ANIMATE_SPEEED_TURN = 6000;
                break;
            case 3:
                ANIMATE_SPEEED = 5000;
                ANIMATE_SPEEED_TURN = 5000;
                break;
            case 4:
                ANIMATE_SPEEED = 4000;
                ANIMATE_SPEEED_TURN = 4000;
                break;
            case 5:
                ANIMATE_SPEEED = 3000;
                ANIMATE_SPEEED_TURN = 3000;
                break;
            case 6:
                ANIMATE_SPEEED = 2000;
                ANIMATE_SPEEED_TURN = 2000;
                break;
            case 7:
                ANIMATE_SPEEED = 1500;
                ANIMATE_SPEEED_TURN = 1500;
                break;
            case 8:
                ANIMATE_SPEEED = 1000;
                ANIMATE_SPEEED_TURN = 1000;
                break;
            case 9:
                ANIMATE_SPEEED = 500;
                ANIMATE_SPEEED_TURN = 500;
                break;
            case 10:
                ANIMATE_SPEEED = 200;
                ANIMATE_SPEEED_TURN = 200;
                break;
        }


    }

    public class Animator implements Runnable {


        private final Interpolator interpolator = new LinearInterpolator();


        float tilt = 90;
        float zoom = 15.5f;
        boolean upward = true;

        long start = SystemClock.uptimeMillis();

        LatLng endLatLng = null;
        LatLng beginLatLng = null;

        boolean showPolyline = false;

        private Marker trackingMarker;

        public void reset() {
            //  System.out.println("Prabha reset called :::::::::::::");
            //    resetMarkers();
            start = SystemClock.uptimeMillis();
            if (currentIndex == geoPoints.size()) {
                currentIndex = 0;
            } else {

            }
            // System.out.println("the current index in reset method is ::::" + currentIndex);
            endLatLng = getEndLatLng();
            beginLatLng = getBeginLatLng();

        }

        public void stop() {

//            if (trackingMarker != null) {
//                trackingMarker.remove();
//            }

            mHandler.removeCallbacks(animator);

        }

        public void initialize(boolean showPolyLine) {
            // System.out.println("Prabha initialize called :::::::::::::");
            reset();
            if (trackingMarker != null) {
                trackingMarker.remove();
            }
            this.showPolyline = showPolyLine;

            // highLightMarker(0);

            if (showPolyLine) {
                polyLine = initializePolyLine();
            }
            // System.out.println("Initia lize method called ::::" + currentIndex);
            // We first need to put the camera in the correct position for the first run (we need 2 markers for this).....


            LatLng markerPos = geoPoints.get(currentIndex);
            LatLng secondPos = geoPoints.get(currentIndex + 1);
            if (mPosList.get(currentIndex) != null && !mPosList.get(currentIndex).equalsIgnoreCase("M")) {

                map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    // Use default InfoWindow frame
                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    // Defines the contents of the InfoWindow
                    @Override
                    public View getInfoContents(Marker arg0) {

                        // Getting view from the layout file info_window_layout
                        View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                        // Getting the position from the marker
                        LatLng latLng = arg0.getPosition();

                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        height = displayMetrics.heightPixels;
                        width = displayMetrics.widthPixels;
                        LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);
                        backImageParams.width = width * 80 / 100;
                        //backImageParams.height = height * 10 / 100;
                        backImageParams.gravity = Gravity.CENTER;
                        layout.setLayoutParams(backImageParams);

                        // Getting reference to the TextView to set latitude
                        TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                        txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

//                        new GetAddressTask(txtContent).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);

                        StringBuffer addr = new StringBuffer();
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = null;
                        try {
                            addresses = geocoder.getFromLocation(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 1);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        } catch (IllegalArgumentException e2) {
                            // Error message to post in the log
//                        String errorString = "Illegal arguments " +
//                                Double.toString(params[0]) +
//                                " , " +
//                                Double.toString(params[1]) +
//                                " passed to address service";
//                        e2.printStackTrace();
//                        return errorString;
                        } catch (NullPointerException np) {
                            // TODO Auto-generated catch block
                            np.printStackTrace();
                        }
                        // If the reverse geocode returned an address
                        if (addresses != null && addresses.size() > 0) {
                            // Get the first address
                            Address address = addresses.get(0);
                            /*
                             * Format the first line of address (if available),
                             * city, and country name.
                             */
                            String addressText = null;

                            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                addressText = address.getAddressLine(i);
                                addr.append(addressText + ",");
                            }
                            // Return the text
                            // return addr.toString();
                        } else {
                            addr.append("No address found");
                        }


//                        txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                        if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
//                            txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
//                        } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
//                            txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
//                        } else {
//                            txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                        }

                        String mAddsData = null;
                        if (mAddsList.get(currentIndex) != null) {
                            mAddsData = mAddsList.get(currentIndex);
                        }

                        if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                            txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
                        } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                            txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
                        } else {
                            txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
                        }
                        return v;

                    }
                });

                MarkerOptions markerOption = new MarkerOptions().position(markerPos);
                // markerOption.title(parse.getshortname());

                if (currentIndex == 0) {


                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);

//                ImageView img_mark4 = (ImageView) parking_marker.findViewById(R.id.id_vehicle_in_marker);


//                ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
                    id_custom_marker_icon.setImageResource(R.drawable.start);
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));

                }
                else {

//                }

                    if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
//                    markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

                        // markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//                    markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.parking));
//                    ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
//                    id_vehicle_in_marker.setImageResource(R.drawable.parking);
//                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryActivity.this, mCustomViewMarker)));

                        ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                        ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                        ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                        ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                        img_mark1.setVisibility(View.GONE);
                        img_mark2.setVisibility(View.GONE);
                        img_mark3.setVisibility(View.GONE);
                        id_custom_marker_icon.setVisibility(View.VISIBLE);

//                ImageView img_mark4 = (ImageView) parking_marker.findViewById(R.id.id_vehicle_in_marker);


//                ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
//                        id_custom_marker_icon.setImageResource(R.drawable.parking);
                        id_custom_marker_icon.setImageResource(R.drawable.icon_parking);
                        markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));


                    } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                        //markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));

                        ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                        ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                        ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                        ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                        img_mark1.setVisibility(View.GONE);
                        img_mark2.setVisibility(View.GONE);
                        img_mark3.setVisibility(View.GONE);
                        id_custom_marker_icon.setVisibility(View.VISIBLE);

                        id_custom_marker_icon.setImageResource(R.drawable.icon_idle);
                        markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));
                    }
                }
                //  markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryActivity.this, marker)));
                // if (current_vehicle_location_marker.isEmpty()) {


                Marker currentMarker = map.addMarker(markerOption);

//                Marker marker = map.addMarker(new MarkerOptions()
//                        .position(markerPos)
//                        .title("Title")
//                        .snippet("Snippet")
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

//                System.out.println("Marker option inserting laocation is :::::" + markerOption.getPosition());
                mMarkersHashMap.put(currentMarker, markerOption);
                mMarkersPositionHashMap.put(currentMarker, currentIndex);
                currentMarker.showInfoWindow();


            }
            else {

                // markerOption.title(parse.getshortname());

                if (currentIndex == 0) {
                    MarkerOptions markerOption = new MarkerOptions().position(markerPos);
                    map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        // Use default InfoWindow frame
                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        // Defines the contents of the InfoWindow
                        @Override
                        public View getInfoContents(Marker arg0) {

                            // Getting view from the layout file info_window_layout
                            View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                            // Getting the position from the marker
                            LatLng latLng = arg0.getPosition();

                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            height = displayMetrics.heightPixels;
                            width = displayMetrics.widthPixels;
                            LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                            LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);
                            backImageParams.width = width * 80 / 100;
                            //backImageParams.height = height * 10 / 100;
                            backImageParams.gravity = Gravity.CENTER;
                            layout.setLayoutParams(backImageParams);

                            // Getting reference to the TextView to set latitude
                            TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                            txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

//                        new GetAddressTask(txtContent).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);

                            StringBuffer addr = new StringBuffer();
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = null;
                            try {
                                addresses = geocoder.getFromLocation(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 1);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            } catch (IllegalArgumentException e2) {
                                // Error message to post in the log
//                        String errorString = "Illegal arguments " +
//                                Double.toString(params[0]) +
//                                " , " +
//                                Double.toString(params[1]) +
//                                " passed to address service";
//                        e2.printStackTrace();
//                        return errorString;
                            } catch (NullPointerException np) {
                                // TODO Auto-generated catch block
                                np.printStackTrace();
                            }
                            // If the reverse geocode returned an address
                            if (addresses != null && addresses.size() > 0) {
                                // Get the first address
                                Address address = addresses.get(0);
                                /*
                                 * Format the first line of address (if available),
                                 * city, and country name.
                                 */
                                String addressText = null;

                                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                    addressText = address.getAddressLine(i);
                                    addr.append(addressText + ",");
                                }
                                // Return the text
                                // return addr.toString();
                            } else {
                                addr.append("No address found");
                            }


//                        txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                            if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
//                            } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
//                            } else {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                            }

                            String mAddsData = null;
                            if (mAddsList.get(currentIndex) != null) {
                                mAddsData = mAddsList.get(currentIndex);
                            }

                            if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
                            } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
                            } else {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
                            }

                            if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                                if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                                    txtContent.setText(txtContent.getText().toString().trim() + " \n Over Speed : "+speedlist.get(currentIndex));
                                }
                            }

                            return v;

                        }
                    });


                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);

//                ImageView img_mark4 = (ImageView) parking_marker.findViewById(R.id.id_vehicle_in_marker);


//                ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
                    id_custom_marker_icon.setImageResource(R.drawable.start);
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));

                    Marker currentMarker = map.addMarker(markerOption);

//                Marker marker = map.addMarker(new MarkerOptions()
//                        .position(markerPos)
//                        .title("Title")
//                        .snippet("Snippet")
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//                    System.out.println("Marker option inserting laocation is :::::" + markerOption.getPosition());
                    mMarkersHashMap.put(currentMarker, markerOption);
                    mMarkersPositionHashMap.put(currentMarker, currentIndex);
                    currentMarker.showInfoWindow();


                }


            }


            setupCameraPositionForMovement(markerPos, secondPos);

        }

        private void setupCameraPositionForMovement(LatLng markerPos,
                                                    LatLng secondPos) {
            System.out.println("setup camera position called ::::");
            float bearing = bearingBetweenLatLngs(markerPos, secondPos);

            trackingMarker = map.addMarker(new MarkerOptions().position(markerPos)
                    .title("title")
                    .snippet("snippet"));
            trackingMarker.showInfoWindow();
            CameraPosition cameraPosition;
            if (m3Dmap) {
                cameraPosition =
                        new CameraPosition.Builder()
                                .target(markerPos)
                                .bearing(bearing + BEARING_OFFSET)
                                .tilt(90)
                                .zoom(history_zoom_level)
                                .build();


            } else {

                //            if animation not required use this method
                cameraPosition =
                        new CameraPosition.Builder()
                                .target(markerPos)

                                .tilt(90)
                                .zoom(history_zoom_level)
                                .build();


            }


//            .zoom(history_zoom_level)
            map.animateCamera(
                    CameraUpdateFactory.newCameraPosition(cameraPosition),
                    ANIMATE_SPEEED_TURN,
                    new GoogleMap.CancelableCallback() {

                        @Override
                        public void onFinish() {
                            System.out.println("finished camera");
                            animator.reset();
                            Handler handler = new Handler();
                            handler.post(animator);
                        }

                        @Override
                        public void onCancel() {
                            //System.out.println("cancelling camera");
                        }
                    }
            );
        }

        private Polyline polyLine;

        private Polyline initializePolyLine() {
            PolylineOptions rectOptions = new PolylineOptions();
            rectOptions.add(geoPoints.get(currentIndex));
            rectOptions.color(getResources().getColor(R.color.history_polyline_color));

            return map.addPolyline(rectOptions);
        }

        /**
         * Add the marker to the polyline.
         */
        private void updatePolyLine(LatLng latLng) {
            // System.out.println("Prabha updatePolyLine called :::::::::::::");
            List<LatLng> points = polyLine.getPoints();
            points.add(latLng);
            //rectOptions.color(getResources().getColor(R.color.history_polyline_color));
            polyLine.setPoints(points);
        }


        public void stopAnimation() {
            isStopCalled = false;
            animator.stop();
        }

        public void startAnimation(boolean showPolyLine) {
            if (trackingMarker != null) {
                trackingMarker.remove();
            }
            if (geoPoints.size() > 2) {

                //System.out.println("Start animation geo points size " + geoPoints.size());

                animator.initialize(showPolyLine);
            } else {
//                replay_icon.setVisibility(View.GONE);
//                play_icon.setVisibility(View.GONE);
//                pause_icon.setVisibility(View.GONE);
            }
        }


        @Override
        public void run() {
            // System.out.println("Run called :::::");
            // System.out.println("Prabha run called :::::::::::::");
            long elapsed = SystemClock.uptimeMillis() - start;
            double t = interpolator.getInterpolation((float) elapsed / ANIMATE_SPEEED);

//			LatLng endLatLng = getEndLatLng();
//			LatLng beginLatLng = getBeginLatLng();

            double lat = t * endLatLng.latitude + (1 - t) * beginLatLng.latitude;
            double lng = t * endLatLng.longitude + (1 - t) * beginLatLng.longitude;
            LatLng newPosition = new LatLng(lat, lng);

            trackingMarker.setPosition(newPosition);
            trackingMarker.showInfoWindow();

            if (showPolyline) {
//                System.out.println("Prabha updatePolyLine called :::::::::::::");
                updatePolyLine(newPosition);
            }

            // It's not possible to move the marker + center it through a cameraposition update while another camerapostioning was already happening.
            //navigateToPoint(newPosition,tilt,bearing,currentZoom,false);
            //navigateToPoint(newPosition,false);

            if (t < 1) {
                //System.out.println("Prabha t < 1 true called :::::::::::::");
                mHandler.postDelayed(this, 16);
            } else {
                // System.out.println("Prabha t >>>> 1 else called :::::::::::::");
                // System.out.println("Move to next marker.... current = " + currentIndex + " and size = " + geoPoints.size());
                // imagine 5 elements -  0|1|2|3|4 currentindex must be smaller than 4
                if (currentIndex < geoPoints.size() - 2) {

                    //  System.out.println("Prabha currentIndex < geoPoints.size() - 2 called :::::::::::::");

                    currentIndex++;

                    endLatLng = getEndLatLng();
                    beginLatLng = getBeginLatLng();


                    start = SystemClock.uptimeMillis();

                    LatLng begin = getBeginLatLng();
                    LatLng end = getEndLatLng();

                    float bearingL = bearingBetweenLatLngs(begin, end);

                    highLightMarker(currentIndex);


//                    if animation required enable this method

//                    CameraPosition cameraPosition =
//                            new CameraPosition.Builder()
//                                    .target(end) // changed this...
//                                    .bearing(bearingL + BEARING_OFFSET)
//                                    .tilt(tilt)
//                                    .zoom(map.getCameraPosition().zoom)
//                                    .build();

                    CameraPosition cameraPosition;
                    if (m3Dmap) {
                        cameraPosition =
                                new CameraPosition.Builder()
                                        .target(end) // changed this...
                                        .bearing(bearingL + BEARING_OFFSET)
                                        .tilt(tilt)
                                        .zoom(map.getCameraPosition().zoom)
                                        .build();
                    } else {
//                    if animation not required use this method
                        cameraPosition =
                                new CameraPosition.Builder()
                                        .target(end) // changed this...

                                        .tilt(tilt)
                                        .zoom(map.getCameraPosition().zoom)
                                        .build();
                    }


//                    .zoom(history_zoom_level)
                    map.animateCamera(
                            CameraUpdateFactory.newCameraPosition(cameraPosition),
                            ANIMATE_SPEEED_TURN,
                            null
                    );

                    start = SystemClock.uptimeMillis();
                    mHandler.postDelayed(animator, 16);

                } else {

                    // System.out.println("the current index in anim else method is :::::" + currentIndex);

                    currentIndex++;
                    highLightMarker(currentIndex);
                    stopAnimation();
                }

            }
        }


        private LatLng getEndLatLng() {

            // System.out.println("The current index is ::::" + currentIndex);
//if(currentIndex!=geoPoints.size()){
//    cur
//}
//            if(currentIndex+1==geoPoints.size()){
//                currentIndex=currentIndex-2;
//            }

            return geoPoints.get(currentIndex + 1);
        }

        private LatLng getBeginLatLng() {

//            if(currentIndex+1==geoPoints.size()){
//                currentIndex=currentIndex-1;
//            }

            return geoPoints.get(currentIndex);
        }

        private void adjustCameraPosition() {
            //System.out.println("tilt = " + tilt);
            //System.out.println("upward = " + upward);
            //System.out.println("zoom = " + zoom);
            if (upward) {

                if (tilt < 90) {
                    tilt++;
                    zoom -= 0.01f;
                } else {
                    upward = false;
                }

            } else {
                if (tilt > 0) {
                    tilt--;
                    zoom += 0.01f;
                } else {
                    upward = true;
                }
            }
        }
    }
    private void highLightMarker(int index) {
        highLightMarker(markers.get(index));
    }

    private void highLightMarker(Marker marker) {

//        if (geoPoints.size() - 1 == currentIndex) {
//            Log.d("date value", "date and time");
            ParsingClass parsingClass = new ParsingClass();
            parsingClass.historyParse(mHistoryResult);

//            Txt_change_distance.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance()));
//            Txt_change_time.setText((parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));

//            Txt_change_time.setText(cons.getTimefromserver(datetimelist.get(currentIndex)));
//            Log.d("date value", "date and time 1");


//        } else {
            if(distancelist.get(currentIndex)!=null ){

                Txt_change_distance.setText(distancelist.get(currentIndex)+" Kms");
                Distance_cardview.setVisibility(View.VISIBLE);
            }
            if (datetimelist.get(currentIndex)!=null){
                Log.d("date value","1 "+datetimelist.get(currentIndex));
                value_speed.setVisibility(View.VISIBLE);
                Txt_change_time.setText(cons.getTimefromserver(datetimelist.get(currentIndex)));
                Log.d("date value","2 "+cons.getTimefromserver(datetimelist.get(currentIndex)));

            }
//        }

        if(speedlist.get(currentIndex)!=null){
            Speed_cardview.setVisibility(View.VISIBLE);
            Txt_change_speed.setText(speedlist.get(currentIndex)+" Km/hr");
            Log.d("Speed value ",Txt_change_speed.getText().toString());
        }


        if (mPosList.get(currentIndex) != null && !mPosList.get(currentIndex).equalsIgnoreCase("M")) {

            map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                         // Use default InfoWindow frame
                                         @Override
                                         public View getInfoWindow(Marker arg0) {
                                             return null;
                                         }

                                         // Defines the contents of the InfoWindow
                                         @Override
                                         public View getInfoContents(Marker arg0) {

                                             // Getting view from the layout file info_window_layout
                                             View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                                             // Getting the position from the marker
                                             LatLng latLng = arg0.getPosition();

                                             DisplayMetrics displayMetrics = new DisplayMetrics();
                                             getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                             height = displayMetrics.heightPixels;
                                             width = displayMetrics.widthPixels;
                                             LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                                             LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                                     LinearLayout.LayoutParams.WRAP_CONTENT,
                                                     LinearLayout.LayoutParams.WRAP_CONTENT);
                                             backImageParams.width = width * 80 / 100;
                                             //backImageParams.height = height * 10 / 100;
                                             backImageParams.gravity = Gravity.CENTER;
                                             layout.setLayoutParams(backImageParams);

                                             // Getting reference to the TextView to set latitude
                                             TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                                             txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));


                                             StringBuffer addr = new StringBuffer();
                                             Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                             List<Address> addresses = null;
                                             try {
                                                 addresses = geocoder.getFromLocation(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 1);
                                             } catch (IOException e1) {
                                                 e1.printStackTrace();
                                             } catch (IllegalArgumentException e2) {

                                             } catch (NullPointerException np) {
                                                 // TODO Auto-generated catch block
                                                 np.printStackTrace();
                                             }
                                             // If the reverse geocode returned an address
                                             if (addresses != null && addresses.size() > 0) {
                                                 // Get the first address
                                                 Address address = addresses.get(0);
                                                 /*
                                                  * Format the first line of address (if available),
                                                  * city, and country name.
                                                  */
                                                 String addressText = null;

                                                 for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                                     addressText = address.getAddressLine(i);
                                                     addr.append(addressText + ",");
                                                     Log.d("addraddr",""+addr);
                                                 }
                                                 // Return the text
                                                 // return addr.toString();
                                             } else {
                                                 addr.append("No address found");
                                             }


//                    txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));

                                             // System.out.println("The pos is ::::" + mPosList.get(currentIndex) + " and time is ::::::" + mIdleTimeList.get(currentIndex));

//                                             if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
//                                                 txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
//                                             } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
//                                                 txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
//                                             } else {
//                                                 txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                                             }

                                             String mAddsData = null;
                                             if (mAddsList.get(currentIndex) != null) {
                                                 mAddsData = mAddsList.get(currentIndex);
                                             }

                                             if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                                                 txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
                                             } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                                                 txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
                                             } else {
                                                 txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
                                             }


                                             if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                                                 if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                                                     txtContent.setText(txtContent.getText().toString().trim() + " \n Over Speed : "+speedlist.get(currentIndex));
                                                 }
                                             }

                                             return v;

                                         }
                                     }

            );

            MarkerOptions markerOption = new MarkerOptions().position(marker.getPosition());

            if (currentIndex == geoPoints.size() - 1) {
                ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                img_mark1.setVisibility(View.GONE);
                img_mark2.setVisibility(View.GONE);
                img_mark3.setVisibility(View.GONE);
                id_custom_marker_icon.setVisibility(View.VISIBLE);
                id_custom_marker_icon.setImageResource(R.drawable.stop);
                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));

            } else {
                if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {

                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);

                    id_custom_marker_icon.setImageResource(R.drawable.icon_parking);
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));

                } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {

                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);

                    id_custom_marker_icon.setImageResource(R.drawable.icon_idle);
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));

                } else {
                    if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                        if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                            markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

                        }
                    }
                }
            }

            Marker currentMarker = map.addMarker(markerOption);

            mMarkersHashMap.put(currentMarker, markerOption);
            mMarkersPositionHashMap.put(currentMarker, currentIndex);
            currentMarker.showInfoWindow();

        } else {

            if (currentIndex == geoPoints.size() - 1) {
                MarkerOptions markerOption = new MarkerOptions().position(marker.getPosition());

                map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                             // Use default InfoWindow frame
                                             @Override
                                             public View getInfoWindow(Marker arg0) {
                                                 return null;
                                             }

                                             // Defines the contents of the InfoWindow
                                             @Override
                                             public View getInfoContents(Marker arg0) {

                                                 // Getting view from the layout file info_window_layout
                                                 View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                                                 // Getting the position from the marker
                                                 LatLng latLng = arg0.getPosition();

                                                 DisplayMetrics displayMetrics = new DisplayMetrics();
                                                 getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                                 height = displayMetrics.heightPixels;
                                                 width = displayMetrics.widthPixels;
                                                 LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                                                 LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                                         LinearLayout.LayoutParams.WRAP_CONTENT,
                                                         LinearLayout.LayoutParams.WRAP_CONTENT);
                                                 backImageParams.width = width * 80 / 100;
                                                 //backImageParams.height = height * 10 / 100;
                                                 backImageParams.gravity = Gravity.CENTER;
                                                 layout.setLayoutParams(backImageParams);

                                                 // Getting reference to the TextView to set latitude
                                                 TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                                                 txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));


                                                 StringBuffer addr = new StringBuffer();
                                                 Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                                 List<Address> addresses = null;
                                                 try {
                                                     addresses = geocoder.getFromLocation(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 1);
                                                 } catch (IOException e1) {
                                                     e1.printStackTrace();
                                                 } catch (IllegalArgumentException e2) {

                                                 } catch (NullPointerException np) {
                                                     // TODO Auto-generated catch block
                                                     np.printStackTrace();
                                                 }
                                                 // If the reverse geocode returned an address
                                                 if (addresses != null && addresses.size() > 0) {
                                                     // Get the first address
                                                     Address address = addresses.get(0);
                                                     /*
                                                      * Format the first line of address (if available),
                                                      * city, and country name.
                                                      */
                                                     String addressText = null;

                                                     for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                                         addressText = address.getAddressLine(i);
                                                         addr.append(addressText + ",");
                                                     }
                                                     // Return the text
                                                     // return addr.toString();
                                                 } else {
                                                     addr.append("No address found");
                                                 }



                                                 String mAddsData = null;
                                                 if (mAddsList.get(currentIndex) != null) {
                                                     mAddsData = mAddsList.get(currentIndex);
                                                 }
                                                 Log.d("mAddsData",""+mAddsList.get(currentIndex));

                                                 if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                                                     txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
                                                 } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                                                     txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
                                                 } else {
                                                     txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
                                                 }


                                                 if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                                                     if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                                                         txtContent.setText(txtContent.getText().toString().trim() + " \n Over Speed : "+speedlist.get(currentIndex));
                                                     }
                                                 }

                                                 return v;

                                             }
                                         }

                );

                ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                img_mark1.setVisibility(View.GONE);
                img_mark2.setVisibility(View.GONE);
                img_mark3.setVisibility(View.GONE);
                id_custom_marker_icon.setVisibility(View.VISIBLE);
                id_custom_marker_icon.setImageResource(R.drawable.stop);
                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));
                Marker currentMarker = map.addMarker(markerOption);

                mMarkersHashMap.put(currentMarker, markerOption);
                mMarkersPositionHashMap.put(currentMarker, currentIndex);
                currentMarker.showInfoWindow();
            }
            else {
                if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                    if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                        MarkerOptions markerOption = new MarkerOptions().position(marker.getPosition());

                        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                                     // Use default InfoWindow frame
                                                     @Override
                                                     public View getInfoWindow(Marker arg0) {
                                                         return null;
                                                     }

                                                     // Defines the contents of the InfoWindow
                                                     @Override
                                                     public View getInfoContents(Marker arg0) {

                                                         // Getting view from the layout file info_window_layout
                                                         View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                                                         // Getting the position from the marker
                                                         LatLng latLng = arg0.getPosition();

                                                         DisplayMetrics displayMetrics = new DisplayMetrics();
                                                         getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                                         height = displayMetrics.heightPixels;
                                                         width = displayMetrics.widthPixels;
                                                         LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                                                         LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                                                 LinearLayout.LayoutParams.WRAP_CONTENT,
                                                                 LinearLayout.LayoutParams.WRAP_CONTENT);
                                                         backImageParams.width = width * 80 / 100;
                                                         //backImageParams.height = height * 10 / 100;
                                                         backImageParams.gravity = Gravity.CENTER;
                                                         layout.setLayoutParams(backImageParams);

                                                         // Getting reference to the TextView to set latitude
                                                         TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                                                         txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));


                                                         StringBuffer addr = new StringBuffer();
                                                         Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                                         List<Address> addresses = null;
                                                         try {
                                                             addresses = geocoder.getFromLocation(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 1);
                                                         } catch (IOException e1) {
                                                             e1.printStackTrace();
                                                         } catch (IllegalArgumentException e2) {

                                                         } catch (NullPointerException np) {
                                                             // TODO Auto-generated catch block
                                                             np.printStackTrace();
                                                         }
                                                         // If the reverse geocode returned an address
                                                         if (addresses != null && addresses.size() > 0) {
                                                             // Get the first address
                                                             Address address = addresses.get(0);
                                                             /*
                                                              * Format the first line of address (if available),
                                                              * city, and country name.
                                                              */
                                                             String addressText = null;

                                                             for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                                                 addressText = address.getAddressLine(i);
                                                                 addr.append(addressText + ",");
                                                             }
                                                             // Return the text
                                                             // return addr.toString();
                                                         } else {
                                                             addr.append("No address found");
                                                         }


                                                         String mAddsData = null;
                                                         if (mAddsList.get(currentIndex) != null) {
                                                             mAddsData = mAddsList.get(currentIndex);
                                                         }

                                                         if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                                                             txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
                                                         } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                                                             txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
                                                         } else {
                                                             txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
                                                         }

                                                         if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                                                             if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                                                                 txtContent.setText(txtContent.getText().toString().trim() + " \n Over Speed : "+speedlist.get(currentIndex));
                                                             }
                                                         }

                                                         return v;

                                                     }
                                                 }

                        );

                        ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                        ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                        ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                        ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                        img_mark1.setVisibility(View.GONE);
                        img_mark2.setVisibility(View.GONE);
                        img_mark3.setVisibility(View.GONE);
                        id_custom_marker_icon.setVisibility(View.VISIBLE);
                        id_custom_marker_icon.setImageResource(R.drawable.over_speed_new_icon);
                        markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryNewGoogleMapActivity.this, mCustomViewMarker)));
                        Marker currentMarker = map.addMarker(markerOption);


                        mMarkersHashMap.put(currentMarker, markerOption);
                        mMarkersPositionHashMap.put(currentMarker, currentIndex);
                        currentMarker.showInfoWindow();
                    }
                }
            }


        }

        if (currentIndex == geoPoints.size() - 1)

        {


//            play_icon.setVisibility(View.GONE);
//            pause_icon.setVisibility(View.GONE);
            currentIndex = 0;
        }


        this.selectedMarker = marker;
    }
    private Location convertLatLngToLocation(LatLng latLng) {
        Location location = new Location("someLoc");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

    private float bearingBetweenLatLngs(LatLng beginLatLng, LatLng endLatLng) {
        Location beginLocation = convertLatLngToLocation(beginLatLng);
        Location endLocation = convertLatLngToLocation(endLatLng);
        return beginLocation.bearingTo(endLocation);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(HistoryNewGoogleMapActivity.this,MapMenuActivity.class));
        finish();
    }


}
