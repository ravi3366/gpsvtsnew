package com.vamosys.vamos;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;

import java.util.regex.Pattern;


public class DriverDetails extends Activity implements OnClickListener {

    int width, height;
    Button mBtnCall, mBtnMsg,mEdit,mDone;
    LinearLayout mLayoutContent, mLayoutVehicleName, mLayoutDriverName, mLayoutContactNum, mLayoutBtn;
    ImageView mImgVehicleIcon, mImgDriverIcon, mImgCallIcon, mImgBackIcon;
    TextView mTxtVehicleName, mTxtDriverName, mTxtContactNum, mTxtTitle;
    EditText mDriver_names,mDriver_numbers;
    String DriverEditResult;
    String driverName;
    String driverNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_details_main_layout);
        intialization();
        screenArrange();
        setData();

        Log.d("driver detail"," "+Constant.SELECTED_DRIVER_NAME+" "+Constant.SELECTED_DRIVER_CONTACT_NUMBER);
        System.out.println("driver detail"+mTxtDriverName.getText().toString());

        mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog=new Dialog(DriverDetails.this);
                dialog.setContentView(R.layout.activity_edit_driver_detail);
                dialog.setTitle("Driver Detail");
                mDriver_names=(EditText) dialog.findViewById(R.id.Edit_driver_Name);
                mDriver_numbers=(EditText) dialog.findViewById(R.id.Edit_Mobile_Num);
                mDriver_names.setText(Constant.SELECTED_DRIVER_NAME);
                mDriver_numbers.setText(Constant.SELECTED_DRIVER_CONTACT_NUMBER);
                mDone=(Button)dialog.findViewById(R.id.DoneButton1);
                dialog.show();

                mDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        driverName=mDriver_names.getText().toString();
                        driverNo=mDriver_numbers.getText().toString();
                        setDataApi();
                        setData();
                        dialog.cancel();
                    }
                });


            }
        });

    }

    private void setDataApi() {
//        Toast.makeText(getApplicationContext(),"setdata",Toast.LENGTH_LONG).show();
//        http://209.97.163.4:9000/mobile/updateDriverDetails?userId=senthtrqcker&vehicleId=HI&driverName=senth1&driverNo=
        Log.d("driver detail","drive name: "+driverName+" phn "+driverNo);
        String par[] = {Const.API_URL + "mobile/updateDriverDetails?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID +
                "&driverName="+driverName+"&driverNo="+driverNo};
        new setDriverDetail().execute(par);
        Constant.SELECTED_DRIVER_CONTACT_NUMBER=driverNo;
        Constant.SELECTED_DRIVER_NAME=driverName;


    }
    public  class setDriverDetail extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            HttpConfig httpConfig = new HttpConfig();
            try {
                DriverEditResult = httpConfig.httpGet(strings[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("driverdetail"," "+DriverEditResult);
            return DriverEditResult;
            //9936983992

        }
    }
    private void intialization() {
        // TODO Auto-generated method stub
        mBtnCall = (Button) findViewById(R.id.driver_detail_call_btn_layout);
        mBtnMsg = (Button) findViewById(R.id.driver_detail_msg_btn_layout);
        mEdit =(Button) findViewById(R.id.EditButton1);

        mLayoutBtn = (LinearLayout) findViewById(R.id.driver_detail_button_layout);
        mLayoutContent = (LinearLayout) findViewById(R.id.driver_detail_content_layout);
        mLayoutContactNum = (LinearLayout) findViewById(R.id.driver_detail_driver_cont_num_layout);
        mLayoutDriverName = (LinearLayout) findViewById(R.id.driver_detail_driver_name_layout);
        mLayoutVehicleName = (LinearLayout) findViewById(R.id.driver_detail_vehicle_layout);

        mImgBackIcon = (ImageView) findViewById(R.id.driver_detail_view_Back);
        mImgCallIcon = (ImageView) findViewById(R.id.driver_detail_call_icon_layout);
        mImgDriverIcon = (ImageView) findViewById(R.id.driver_detail_driver_icon_layout);
        mImgVehicleIcon = (ImageView) findViewById(R.id.driver_detail_vehicle_icon_layout);

        mTxtTitle = (TextView) findViewById(R.id.driver_detail_title);
        mTxtVehicleName = (TextView) findViewById(R.id.driver_detail_vehicle_name_layout);
        mTxtContactNum = (TextView) findViewById(R.id.driver_detail_call_number_layout);
        mTxtDriverName = (TextView) findViewById(R.id.driver_detail_driver_name_txt_layout);

//        $FRomValue.setText(cons.GetCurrentDate(Calendar.getInstance()));
//        $ToValue.setText(cons.GetCurrentDate(Calendar.getInstance()));

        mImgBackIcon.setOnClickListener(this);
        mBtnCall.setOnClickListener(this);
        mBtnMsg.setOnClickListener(this);


    }

    public void setData() {
        mTxtVehicleName.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
        mTxtDriverName.setText(Constant.SELECTED_DRIVER_NAME);
        mTxtContactNum.setText(Constant.SELECTED_DRIVER_CONTACT_NUMBER);

        if (Constant.SELECTED_VEHICLE_TYPE.toString().equalsIgnoreCase("Bus")) {
            mImgVehicleIcon.setImageDrawable(getResources().getDrawable(R.drawable.bus_east));
        } else if (Constant.SELECTED_VEHICLE_TYPE.toString().equalsIgnoreCase("Truck")) {
            mImgVehicleIcon.setImageDrawable(getResources().getDrawable(R.drawable.truck_east));
        } else if (Constant.SELECTED_VEHICLE_TYPE.toString().equalsIgnoreCase("Car")) {
            mImgVehicleIcon.setImageDrawable(getResources().getDrawable(R.drawable.car_east));
        } else if (Constant.SELECTED_VEHICLE_TYPE.toString().equalsIgnoreCase("Cycle")) {
            mImgVehicleIcon.setImageDrawable(getResources().getDrawable(R.drawable.cycle_east));
        } else if (Constant.SELECTED_VEHICLE_TYPE.toString().equalsIgnoreCase("Jcb")) {
            mImgVehicleIcon.setImageDrawable(getResources().getDrawable(R.drawable.jcb_icon));
        }


    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.driver_detail_view_Back:
                startActivity(new Intent(DriverDetails.this, MapMenuActivity.class));
                finish();
                break;

            case R.id.driver_detail_call_btn_layout:
                if (isValidMobile(Constant.SELECTED_DRIVER_CONTACT_NUMBER)) {
                    callMethod();
                } else {
                    Toast.makeText(DriverDetails.this, "Mobile number is not valid", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.driver_detail_msg_btn_layout:
                if (isValidMobile(Constant.SELECTED_DRIVER_CONTACT_NUMBER)) {
                    sendSMS();
                } else {
                    Toast.makeText(DriverDetails.this, "Mobile number is not valid", Toast.LENGTH_SHORT).show();
                }
                break;


        }
    }

    public void callMethod() {
//        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Constant.SELECTED_DRIVER_CONTACT_NUMBER));
//        startActivity(intent);
//        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Constant.SELECTED_DRIVER_CONTACT_NUMBER));
//        startActivity(callIntent);

//        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Constant.SELECTED_DRIVER_CONTACT_NUMBER));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
//        }

    }

    public void sendSMS() {

        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", Constant.SELECTED_DRIVER_CONTACT_NUMBER, null)));
    }

    private boolean isValidMobile(String phone2) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone2)) {
            if (phone2.length() < 6 || phone2.length() > 13) {
                check = false;
//                txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(DriverDetails.this, MapMenuActivity.class));
        finish();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 12 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mImgBackIcon.setLayoutParams(backImageParams);
        mImgBackIcon.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 85 / 100;
        headTxtParams.height = height * 10 / 100;
        mTxtTitle.setLayoutParams(headTxtParams);
        mTxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mTxtTitle.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams layoutVehicleParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutVehicleParams.width = width;
//        layoutVehicleParams.height = height * 10 / 100;
        mLayoutVehicleName.setLayoutParams(headTxtParams);
        //mTxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mLayoutVehicleName.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams layoutDriverParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutDriverParams.width = width;
//        layoutDriverParams.height = height * 12 / 100;
        mLayoutDriverName.setLayoutParams(layoutDriverParams);
        //mTxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mLayoutDriverName.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams layoutContactParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutContactParams.width = width;
//        layoutContactParams.height = height * 10 / 100;
        mLayoutContactNum.setLayoutParams(layoutContactParams);
        //mTxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mLayoutContactNum.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams layoutButtonParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutButtonParams.width = width;
        layoutButtonParams.height = height * 12 / 100;
        mLayoutBtn.setLayoutParams(layoutButtonParams);
        //mTxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mLayoutBtn.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams txtVehicleNameParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtVehicleNameParams.width = width * 70 / 100;
//        txtVehicleNameParams.height = height *  / 100;
        mTxtVehicleName.setLayoutParams(txtVehicleNameParams);
        mTxtVehicleName.setPadding(width * 2 / 100, 0, 0, 0);
        mTxtVehicleName.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtDriverNameParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtDriverNameParams.width = width * 70 / 100;
//        txtVehicleNameParams.height = height *  / 100;
        mTxtDriverName.setLayoutParams(txtDriverNameParams);
        mTxtDriverName.setPadding(width * 2 / 100, 0, 0, 0);
        mTxtDriverName.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtContactnumParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtContactnumParams.width = width * 70 / 100;
//        txtVehicleNameParams.height = height *  / 100;
        mTxtContactNum.setLayoutParams(txtContactnumParams);
        mTxtContactNum.setPadding(width * 2 / 100, 0, 0, 0);
        mTxtContactNum.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtVehicleIconParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtVehicleIconParams.width = width * 15 / 100;
        txtVehicleIconParams.height = width * 15 / 100;
//        txtVehicleIconParams.setg
        mImgVehicleIcon.setLayoutParams(txtVehicleIconParams);
//        mImgVehicleIcon.setPadding(width * 2 / 100, 0, 0, 0);
//        mImgVehicleIcon.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtDriverIconParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtDriverIconParams.width = width * 15 / 100;
        txtDriverIconParams.height = width * 15 / 100;
//        txtVehicleIconParams.setg
        mImgDriverIcon.setLayoutParams(txtDriverIconParams);
//        mImgDriverIcon.setPadding(width * 2 / 100, 0, 0, 0);

        LinearLayout.LayoutParams txtCallIconParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtCallIconParams.width = width * 15 / 100;
        txtCallIconParams.height = width * 15 / 100;
//        txtVehicleIconParams.setg
        mImgCallIcon.setLayoutParams(txtCallIconParams);
//        mImgCallIcon.setPadding(width * 2 / 100, 0, 0, 0);

        LinearLayout.LayoutParams btnCallParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        btnCallParams.width = width * 35 / 100;
        btnCallParams.height = height * 7 / 100;
        mBtnCall.setLayoutParams(btnCallParams);
//        mBtnCall.setpa
        mBtnCall.setPadding(0, 0, width * 2 / 100, 0);
        mBtnCall.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams btnMsgParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        btnMsgParams.width = width * 35 / 100;
        btnMsgParams.height = height * 7 / 100;
        mBtnMsg.setLayoutParams(btnMsgParams);
        mBtnMsg.setPadding(width * 2 / 100, 0, 0, 0);
        mBtnMsg.setGravity(Gravity.CENTER);


        if (width >= 600) {

            mTxtTitle.setTextSize(16);
            mTxtContactNum.setTextSize(16);
            mTxtDriverName.setTextSize(16);
            mTxtVehicleName.setTextSize(16);

        } else if (width > 501 && width < 600) {
//            mTxtTitle.setTextSize(15);

            mTxtTitle.setTextSize(15);
            mTxtContactNum.setTextSize(15);
            mTxtDriverName.setTextSize(15);
            mTxtVehicleName.setTextSize(15);

        } else if (width > 260 && width < 500) {

//            mTxtTitle.setTextSize(14);

            mTxtTitle.setTextSize(14);
            mTxtContactNum.setTextSize(14);
            mTxtDriverName.setTextSize(14);
            mTxtVehicleName.setTextSize(14);

        } else if (width <= 260) {

//            mTxtTitle.setTextSize(13);

            mTxtTitle.setTextSize(13);
            mTxtContactNum.setTextSize(13);
            mTxtDriverName.setTextSize(13);
            mTxtVehicleName.setTextSize(13);

        }
    }
}