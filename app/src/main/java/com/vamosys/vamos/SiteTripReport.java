package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.vamosys.model.SiteTripreportDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HorizontalScrollView;
import com.vamosys.utils.HttpConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class SiteTripReport extends AppCompatActivity implements View.OnClickListener {

    Toolbar mToolbar;
    ImageView mCalendarImage;
    TableAdapter adapter;
    boolean isHeaderPresent = false;

    ConnectionDetector cd;
    SharedPreferences sp;
    static ListView lv;
    static int width;
    static int height;
    static Const cons;
    //ImageView mImgDateSelector;
    View bottomSheet;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;


    private int year, month, day;
    static final int TIME_DIALOG_ID = 1111;
    static final int DATE_DIALOG_ID = 2222;
    private int hour;
    private int minute;
    Spinner spinnerFromSite, spinnerEndSite;
    Button mBtnSubmit;
    TextView mTxtFromDate, mTxtEndDate, mTxtFromTime, mTxtEndTime;
    boolean isFromTime = false, isFromDate = false;

    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "HH:mm:ss");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");


    private static ArrayList<SiteTripreportDto> mSiteTripReportData = new ArrayList<SiteTripreportDto>();
    SimpleDateFormat dftime = new SimpleDateFormat("kk:mm");
    String mStartDate, mEndDate, mStartTime, mStartTimeValue, mEndTime, mEndTimeValue, mFromSite = "All", mEndSite = "All";
    int mFromSiteSelectedPos = 0, mEndSiteSelectedPos = 0;
    ArrayList<String> mSiteList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_trip_report);
        init();
        screenArrange();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.site_trip_report));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);
        mCalendarImage = (ImageView) mToolbar.findViewById(R.id.img_calendar);
        mCalendarImage.setOnClickListener(this);


//        final Calendar cal1 = Calendar.getInstance();
//        cal1.add(Calendar.DATE, -1);

        mStartDate = Const.getTripYesterdayDate2();
        mEndDate = Const.getTripYesterdayDate2();

//        mStartDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
//        mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));


//        long timeInMillis = System.currentTimeMillis();
//        Calendar cal1 = Calendar.getInstance();
//        cal1.setTimeInMillis(timeInMillis);

//        SimpleDateFormat dateFormat = new SimpleDateFormat(
//                "yyyy-MM-dd hh:mm:ss aa");


        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        System.out.println("Hi date is :::" + timeFormat.format(cal.getTime()));

//        mStartTimeValue = "12:00 AM";
        mStartTimeValue = timeFormatShow.format(cal.getTime());
//        mStartTime = "12:00:00 am";
        mStartTime = timeFormat.format(cal.getTime());
        final Calendar c = Calendar.getInstance();
        // Current Hour
        hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        minute = c.get(Calendar.MINUTE);
        isFromTime = false;


        //updateTime(hour, minute);

        updateTime(23,59);


        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(SiteTripReport.this, MapMenuActivity.class));
                finish();


            }
        });
        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        setBottomLayoutData();
        adapter = new TableAdapter(this);

        if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();
            mSiteTripReportData.clear();
            new getSiteTripReport().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }

        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        spinnerFromSite
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mFromSiteSelectedPos = position;
                        // if (position != 0) {
                        mFromSite = String.valueOf(mSiteList.get(position).trim());
//                        } else {
//                            mFromSite = "";
//                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

        spinnerEndSite
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mEndSiteSelectedPos = position;
//                        if (position != 0) {
                        mEndSite = String.valueOf(mSiteList.get(position).trim());
//                        } else {
//                            mEndSite = "";
//                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

//        mCalendarImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                System.out.println("Hi state " + bottomSheetEnabled);
//
//
//                if (!bottomSheetEnabled) {
//                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                } else {
//                    // setData();
//                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }
//
//                if (bottomSheetEnabled) {
//                    bottomSheetEnabled = false;
//                } else {
//                    bottomSheetEnabled = true;
//                }
//
//
//            }
//        });

    }

    private void init() {
        lv = (ListView) findViewById(R.id.site_trip_report_listView1);
        spinnerFromSite = (Spinner) findViewById(R.id.spinner_from_site);
        spinnerEndSite = (Spinner) findViewById(R.id.spinner_end_site);
        mTxtFromDate = (TextView) findViewById(R.id.txt_start_date_value);
        mTxtFromTime = (TextView) findViewById(R.id.txt_start_time_value);
        mTxtEndDate = (TextView) findViewById(R.id.txt_end_date_value);
        mTxtEndTime = (TextView) findViewById(R.id.txt_end_time_value);
        mBtnSubmit = (Button) findViewById(R.id.btn_done);
        mTxtFromTime.setOnClickListener(this);
        mTxtFromDate.setOnClickListener(this);
        mTxtEndDate.setOnClickListener(this);
        mTxtEndTime.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);

    }

    private void setBottomLayoutData() {
        mTxtFromDate.setText(mStartDate);
        mTxtEndDate.setText(mEndDate);
        mTxtFromTime.setText(mStartTimeValue);
        mTxtEndTime.setText(mEndTimeValue);
    }

    private void screenArrange() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        height = metrics.heightPixels;
        width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.txt_start_date_value:

                isFromDate = true;
                showDialog(DATE_DIALOG_ID);
                break;
            case R.id.txt_start_time_value:
//                showDialog(DATE_DIALOG_FROMID);
                isFromTime = true;
                showDialog(TIME_DIALOG_ID);
                break;
            case R.id.txt_end_date_value:
//                createNumberDialog(1);
                isFromDate = false;
                showDialog(DATE_DIALOG_ID);
//                show(1);

                break;
            case R.id.txt_end_time_value:
//                createNumberDialog(0);
                isFromTime = false;
                showDialog(TIME_DIALOG_ID);
//                show(0);
                break;

            case R.id.img_calendar:
                if (!bottomSheetEnabled) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    // setData();
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }

                if (mSiteList.size() > 0) {

                } else {
                    new getAllSites().execute();
                }

                break;
            case R.id.btn_done:
//                createNumberDialog(0);
//                show(0);
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();
//                    mStoppageData = new ArrayList<StoppageReportDto>();
                    mSiteTripReportData.clear();
                    lv.setAdapter(null);
                    adapter.notifyDataSetChanged();
                    new getSiteTripReport().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }

//                new CameraReportIamgeListActivtiy.getCameraReport().execute();
                break;
        }
    }

    private class getSiteTripReport extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {

                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date d = f.parse(string_from_date);

                long fromMilliSeconds = d.getTime();

                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
//                System.out.println("Date " + string_from_date + " utc " + fromMilliSeconds + " FT: " + mStartTime + " FS : " + mFromSite + " ES :" + mEndSite);

                HttpConfig ht = new HttpConfig();

//                http://gpsvts.net/mobile/getSiteTripReport?vehicleId=vli_ttaipl-TN01X6747&fromDate=2017-06-02&fromTime=0:0:0&toDate=2017-06-05&toTime=23:59:59&language=en&site1=S.P.Koil%20Railway%20Gate&site2=Nissan%20Power%20Train&fromDateUTC=1496341800000&toDateUTC=1496687399000&userId=vantec
//                result = ht.httpGet(Const.API_URL + "/mobile/getSiteTripReport?vehicleId=TN12E2134&fromDate=2017-06-05&fromTime=0:0:0&toDate=2017-06-05&toTime=23:59:59&language=en&site1=All&site2=All&fromDateUTC=1496601000000&toDateUTC=1496687399000&userId=vantec");
//                result = ht.httpGet(Const.API_URL + "/mobile/getSiteTripReport?vehicleId=vli_ttaipl-TN01X6747&fromDate=2017-06-02&fromTime=0:0:0&toDate=2017-06-05&toTime=23:59:59&language=en&site1=S.P.Koil%20Railway%20Gate&site2=Nissan%20Power%20Train&fromDateUTC=1496341800000&toDateUTC=1496687399000&userId=vantec");
                String url = Const.API_URL + "/mobile/getSiteTripReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + mStartDate.trim() + "&fromTime=" + mStartTime.trim()
                        + "&toDate=" + mEndDate.trim() + "&toTime=" + mEndTime.trim() + "&language=en&site1=" + mFromSite + "&site2=" + mEndSite + "&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID;
                url = url.replace(" ", "%20");
                result = ht.httpGet(url);


            } catch (Exception e) {
                System.out.println("Hi exception is ::::" + e.toString());
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
//            System.out.println("The Fuel fill result is ::::" + result);
            if (result != null && result.length() > 0) {
//                System.out.println("The Fuel fill result is ::::" + result);

                JSONArray jArrayFuel = null;
                try {
//                    jsonArray = new JSONArray(result);

                    // for (int i = 0; i < jsonArray.length(); i++) {
//                    JSONObject jsonObject = jsonArray.getJSONObject(1);

                    JSONObject jsonObject = new JSONObject(result.trim());

                    if (jsonObject.has("history")) {
                        if (jsonObject.getJSONArray("history") != null) {
                            jArrayFuel = jsonObject.getJSONArray("history");
                            for (int j = 0; j < jArrayFuel.length(); j++) {
                                JSONArray jArrHistory = jArrayFuel.getJSONArray(j);
                                for (int i = 0; i < jArrHistory.length(); i = i + 2) {

                                    SiteTripreportDto f = new SiteTripreportDto();
                                    JSONObject jsonCheckObject1 = jArrHistory.getJSONObject(i);
                                    if ((jsonCheckObject1.has("tripCount") && jsonCheckObject1.getInt("tripCount") > 0) && (jsonCheckObject1.has("singleTripdistance") && jsonCheckObject1.getDouble("singleTripdistance") > 0)) {
                                        f.setSingleTripdistance(jsonCheckObject1.getDouble("singleTripdistance"));
                                        f.setTripCount(jsonCheckObject1.getInt("tripCount"));
                                        f.setCountData(true);
                                    } else {

                                        if (i < jArrHistory.length()) {
                                            JSONObject jsonStoppageObject1 = jArrHistory.getJSONObject(i);
                                            if (jsonStoppageObject1.has("state")) {
                                                // System.out.println("Hi start state is :::" + jsonStoppageObject1.getString("state"));
                                                f.setStartLocation(jsonStoppageObject1.getString("state"));
                                            }

                                            if (jsonStoppageObject1.has("startTime")) {
                                                f.setStartTime(jsonStoppageObject1.getLong("startTime"));
                                            }
                                        }

                                        if (i + 1 < jArrHistory.length()) {
                                            JSONObject jsonStoppageObject = jArrHistory.getJSONObject(i + 1);

                                            if (jsonStoppageObject.has("stoppageTime")) {
                                                f.setStoppageTime(jsonStoppageObject.getLong("stoppageTime"));
                                            }

                                            if (jsonStoppageObject.has("endTime")) {
                                                f.setEndTime(jsonStoppageObject.getLong("endTime"));
                                            }

                                            if (jsonStoppageObject.has("runningTime")) {
                                                f.setRunningTime(jsonStoppageObject.getLong("runningTime"));
                                            }

                                            if (jsonStoppageObject.has("detentionTime")) {
                                                f.setDetentionTime(jsonStoppageObject.getLong("detentionTime"));
                                            }

                                            if (jsonStoppageObject.has("distanceCovered")) {
                                                f.setDistanceCovered(jsonStoppageObject.getDouble("distanceCovered"));
                                            }

                                            if (jsonStoppageObject.has("avgSpeed")) {
                                                f.setAvgSpeed(jsonStoppageObject.getDouble("avgSpeed"));
                                            }

                                            if (jsonStoppageObject.has("speedViolation")) {
//                                        System.out.println("Hi speedViolation is :::" + jsonStoppageObject.getInt("speedViolation"));
                                                f.setSpeedViolation(jsonStoppageObject.getInt("speedViolation"));
                                            }

                                            if (jsonStoppageObject.has("state")) {

                                                // System.out.println("Hi end state is :::" + jsonStoppageObject.getString("state"));

                                                f.setEndLocation(jsonStoppageObject.getString("state"));
                                            }

                                        }
                                        f.setCountData(false);
                                    }
                                    //  System.out.println("Hi start state is :::" + f.getStartLocation() + " end state ::" + f.getEndLocation());

                                    mSiteTripReportData.add(f);


                                }

                            }
                        }
                    }

//                    if (jsonObject.has("totalFuel")) {
//                        mTotalFuelValue = String.valueOf(jsonObject.getDouble("totalFuel"));
//                    }

                    // }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                setTableLayoutData();
            } else {
                Toast.makeText(getApplicationContext(), "Report not available.Please try again", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(SiteTripReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private class getAllSites extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {


                HttpConfig ht = new HttpConfig();

//                http://gpsvts.net/mobile/getSiteTripReport?vehicleId=vli_ttaipl-TN01X6747&fromDate=2017-06-02&fromTime=0:0:0&toDate=2017-06-05&toTime=23:59:59&language=en&site1=S.P.Koil%20Railway%20Gate&site2=Nissan%20Power%20Train&fromDateUTC=1496341800000&toDateUTC=1496687399000&userId=vantec
//                result = ht.httpGet(Const.API_URL + "/mobile/getSiteTripReport?vehicleId=TN12E2134&fromDate=2017-06-05&fromTime=0:0:0&toDate=2017-06-05&toTime=23:59:59&language=en&site1=All&site2=All&fromDateUTC=1496601000000&toDateUTC=1496687399000&userId=vantec");
                result = ht.httpGet(Const.API_URL + "/mobile/viewSite?userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
//            System.out.println("The Fuel fill result is ::::" + result);
            ArrayList<String> siteList = new ArrayList<String>();
            siteList.add("All");
            if (result != null && result.length() > 0) {
//                System.out.println("The Fuel fill result is ::::" + result);

                JSONArray jArraySites = null;
                try {


                    JSONObject jsonObject = new JSONObject(result.trim());

                    if (jsonObject.has("siteParent")) {
                        jArraySites = jsonObject.getJSONArray("siteParent");
                        for (int j = 0; j < jArraySites.length(); j++) {

                            JSONObject jSiteParentObj = jArraySites.getJSONObject(j);
                            if (jSiteParentObj.has("site")) {
                                JSONArray jArrSiteList = jSiteParentObj.getJSONArray("site");
                                for (int i = 0; i < jArrSiteList.length(); i++) {
                                    JSONObject siteContainObj = jArrSiteList.getJSONObject(i);
                                    if (siteContainObj.has("siteName")) {
                                        if (siteContainObj.getString("siteName") != null) {
                                            siteList.add(siteContainObj.getString("siteName"));
                                        }
                                    }
                                }
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                setTableLayoutData();
            } else {
                Toast.makeText(getApplicationContext(), "Report not available.Please try again", Toast.LENGTH_SHORT).show();
            }
            mSiteList = siteList;

            setSiteSpinnerData();

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(SiteTripReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private void setSiteSpinnerData() {
        ArrayAdapter dataAdapter = new ArrayAdapter(this, R.layout.spinner_row, mSiteList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        // spinnerFromHour.setPrompt("Select From Hour");
        spinnerFromSite.setAdapter(dataAdapter);
        spinnerFromSite.setSelection((mFromSiteSelectedPos));

        spinnerEndSite.setAdapter(dataAdapter);
        spinnerEndSite.setSelection((mEndSiteSelectedPos));
    }

    public void setTableLayoutData() {

//        adapter.setHeader(mHeader);
//        ListView listView = (ListView) findViewById(R.id.executive_report_listView1);

        if (isHeaderPresent) {

        } else {
            lv.addHeaderView(adapter.getHeaderView(lv));
            isHeaderPresent = true;
        }


        lv.setAdapter(adapter);
        adapter.setData(mSiteTripReportData);

        adapter.notifyDataSetChanged();

//        mTxtCumulativeFuelValue.setText(mTotalFuelValue + " ltrs");

//        parseVehicleKms();

    }


    public class TableAdapter extends BaseAdapter {

        // private static final String TAG = "TableAdapter";

        private Context mContext;

        private String[] mHeader;


        private int mCurrentScroll;

        private int[] mColResources = {R.id.site_trip_start_location_textView, R.id.site_trip_start_time_textView,
                R.id.site_trip_end_location_textView, R.id.site_trip_end_time_textView, R.id.site_trip_duration_textView, R.id.site_trip_dist_cov_textView,
                R.id.site_trip_moving_textView, R.id.site_trip_stoppage_textView, R.id.site_trip_av_speed_textView, R.id.site_trip_speed_violations_textView};
        private int[] mColResources2 = {R.id.site_trip_summary_textView, R.id.site_trip_total_count_textView, R.id.site_trip_total_distance_textView};

        public TableAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return mSiteTripReportData != null ? mSiteTripReportData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mSiteTripReportData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            HorizontalScrollView view = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                view = (HorizontalScrollView) inflater.inflate(
                        R.layout.site_trip_list_item_table_row, parent, false);

//                view.setOnScrollChangeListener(new OnScrollListener() {
//                    @Override
//                    public void onScrollChanged(View view, int scrollX) {
//
//                    }
//                });


                view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                    @Override
                    public void onScrollChanged(View scrollView, int scrollX) {

                        mCurrentScroll = scrollX;
                        ListView listView = (ListView) scrollView.getParent();
                        if (listView == null)
                            return;

                        for (int i = 0; i < listView.getChildCount(); i++) {
                            View child = listView.getChildAt(i);
                            if (child instanceof HorizontalScrollView
                                    && child != scrollView) {
                                HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                                if (scrollView2.getScrollX() != mCurrentScroll) {
                                    scrollView2.setScrollX(mCurrentScroll);
                                }
                            }
                        }
                    }
                });

            } else {
                view = (HorizontalScrollView) convertView;
            }

            view.setScrollX(mCurrentScroll);

//            if (position % 2 == 0) {
//                view.setBackgroundColor(Color.WHITE);
//            } else {
//                view.setBackgroundColor(Color.LTGRAY);
//            }

            SiteTripreportDto data = mSiteTripReportData.get(position);

            LinearLayout mContentLayout, mTotalLayout;
            mContentLayout = (LinearLayout) view.findViewById(R.id.site_trip_adap_top_latout);
            mTotalLayout = (LinearLayout) view.findViewById(R.id.site_trip_adap_bottom_latout);

            if (data.isCountData()) {
                mContentLayout.setVisibility(View.GONE);
                mTotalLayout.setVisibility(View.VISIBLE);
                //TextView col1 = (TextView) view.findViewById(mColResources2[0]);
                // col1.setText("Total Count            " + data.getTripCount());
                TextView col2 = (TextView) view.findViewById(mColResources2[1]);
                col2.setText("Total Count  " + data.getTripCount());
                TextView col3 = (TextView) view.findViewById(mColResources2[2]);
                col3.setText("Total Distance " + data.getSingleTripdistance());
            } else {
                mContentLayout.setVisibility(View.VISIBLE);
                mTotalLayout.setVisibility(View.GONE);
//            for (int i = 0; i < mColResources.length; i++) {
                TextView col1 = (TextView) view.findViewById(mColResources[0]);
                col1.setText(data.getStartLocation());

                //System.out.println("Hi start location :::" + data.getStartLocation());

//            col1.setText(cons.getReport24HourTimefromserver(String.valueOf(data.getEndTime() - data.getStoppageTime())));
                TextView col2 = (TextView) view.findViewById(mColResources[1]);
                col2.setText(String.valueOf(cons.getReport24HourTimefromserver(String.valueOf(data.getStartTime()))));
                TextView col3 = (TextView) view.findViewById(mColResources[2]);
//            col3.setText(String.valueOf(data.getAddress()));
                col3.setText(data.getEndLocation());
                //System.out.println("Hi end location :::" + data.getEndLocation());
                TextView col4 = (TextView) view.findViewById(mColResources[3]);
                col4.setText(String.valueOf(cons.getReport24HourTimefromserver(String.valueOf(data.getEndTime()))));
                TextView col5 = (TextView) view.findViewById(mColResources[4]);
                if (data.getDetentionTime() > 0) {
                    col5.setText(cons.getreportTimeValue((data.getDetentionTime())));
                } else {
                    col5.setText("00:00:00");
                }

                TextView col6 = (TextView) view.findViewById(mColResources[5]);


                col6.setText(String.valueOf(data.getDistanceCovered()));


                TextView col7 = (TextView) view.findViewById(mColResources[6]);

                if (data.getRunningTime() > 0) {
                    col7.setText(cons.getreportTimeValue((data.getRunningTime())));
                } else {
                    col7.setText("00:00:00");
                }
//                col7.setText(cons.getreportTimeValue((data.getRunningTime())));
                TextView col8 = (TextView) view.findViewById(mColResources[7]);

                if (data.getStoppageTime() > 0) {
                    col8.setText(cons.getreportTimeValue((data.getStoppageTime())));
                } else {
                    col8.setText("00:00:00");
                }

//                col8.setText(cons.getreportTimeValue((data.getStoppageTime())));
                TextView col9 = (TextView) view.findViewById(mColResources[8]);
                col9.setText(String.valueOf(data.getAvgSpeed()));
                TextView col10 = (TextView) view.findViewById(mColResources[9]);
                col10.setText(String.valueOf(data.getSpeedViolation()));
                LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                text2Params.width = width * 50 / 100;
//            text2Params.height = height * 9 / 100;
                col1.setLayoutParams(text2Params);
                col1.setPadding(width * 2 / 100, 0, 0, 0);
                col1.setGravity(Gravity.CENTER | Gravity.LEFT);

                col3.setLayoutParams(text2Params);
                col3.setPadding(width * 2 / 100, 0, 0, 0);
                col3.setGravity(Gravity.CENTER | Gravity.LEFT);


                LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                vehicleNameParams.width = width * 35 / 100;
//            vehicleNameParams.height = height * 9 / 100;

                col2.setLayoutParams(vehicleNameParams);
                col2.setPadding(width * 2 / 100, 0, 0, 0);
                col2.setGravity(Gravity.CENTER | Gravity.LEFT);

                col4.setLayoutParams(vehicleNameParams);
                col4.setPadding(width * 2 / 100, 0, 0, 0);
                col4.setGravity(Gravity.CENTER | Gravity.LEFT);

                col5.setLayoutParams(vehicleNameParams);
                col5.setPadding(width * 2 / 100, 0, 0, 0);
                col5.setGravity(Gravity.CENTER | Gravity.LEFT);


                LinearLayout.LayoutParams text3Params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                text3Params.width = width * 30 / 100;
//            text3Params.height = height * 9 / 100;

                col6.setLayoutParams(text3Params);
                //   col3.setWidth(width * 75 / 100);
                col6.setPadding(width * 2 / 100, 0, 0, 0);
                col6.setGravity(Gravity.CENTER | Gravity.LEFT);

                col7.setLayoutParams(text3Params);
                //   col3.setWidth(width * 75 / 100);
                col7.setPadding(width * 2 / 100, 0, 0, 0);
                col7.setGravity(Gravity.CENTER | Gravity.LEFT);

                col8.setLayoutParams(text3Params);
                //   col3.setWidth(width * 75 / 100);
                col8.setPadding(width * 2 / 100, 0, 0, 0);
                col8.setGravity(Gravity.CENTER | Gravity.LEFT);


                col9.setLayoutParams(text3Params);
                //   col3.setWidth(width * 75 / 100);
                col9.setPadding(width * 2 / 100, 0, 0, 0);
                col9.setGravity(Gravity.CENTER | Gravity.LEFT);


                col10.setLayoutParams(text3Params);
                //   col3.setWidth(width * 75 / 100);
                col10.setPadding(width * 2 / 100, 0, 0, 0);
                col10.setGravity(Gravity.CENTER | Gravity.LEFT);
            }


            return view;
        }

        public View getHeaderView(ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            HorizontalScrollView view = (HorizontalScrollView) inflater
                    .inflate(R.layout.site_trip_list_item_table_header, parent, false);

//            for (int i = 0; i < mColResources.length; i++) {
//                TextView col = (TextView) view.findViewById(mColResources[i]);
//                col.setText(mHeader[i]);
//            }


            view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                @Override
                public void onScrollChanged(View scrollView, int scrollX) {

                    mCurrentScroll = scrollX;
                    ListView listView = (ListView) scrollView.getParent();
                    if (listView == null)
                        return;

                    for (int i = 0; i < listView.getChildCount(); i++) {
                        View child = listView.getChildAt(i);
                        if (child instanceof HorizontalScrollView
                                && child != scrollView) {
                            HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                            if (scrollView2.getScrollX() != mCurrentScroll) {
                                scrollView2.setScrollX(mCurrentScroll);
                            }
                        }
                    }
                }
            });


            TextView col1 = (TextView) view.findViewById(R.id.site_trip_start_location_textView_header);
            TextView col2 = (TextView) view.findViewById(R.id.site_trip_start_time_textView_header);
            TextView col3 = (TextView) view.findViewById(R.id.site_trip_end_location_textView_header);
            TextView col4 = (TextView) view.findViewById(R.id.site_trip_end_time_textView_header);
            TextView col5 = (TextView) view.findViewById(R.id.site_trip_duration_textView_header);
            TextView col6 = (TextView) view.findViewById(R.id.site_trip_dist_cov_textView_header);
            TextView col7 = (TextView) view.findViewById(R.id.site_trip_moving_textView_header);
            TextView col8 = (TextView) view.findViewById(R.id.site_trip_stoppage_textView_header);
            TextView col9 = (TextView) view.findViewById(R.id.site_trip_av_speed_textView_header);
            TextView col10 = (TextView) view.findViewById(R.id.site_trip_speed_violations_textView_header);
            //  TextView col5 = (TextView) view.findViewById(R.id.link_textView_header);

            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 50 / 100;
            text2Params.height = height * 9 / 100;
            col1.setLayoutParams(text2Params);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            col3.setLayoutParams(text2Params);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 35 / 100;
            vehicleNameParams.height = height * 9 / 100;

            col2.setLayoutParams(vehicleNameParams);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);

            col4.setLayoutParams(vehicleNameParams);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(vehicleNameParams);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text3Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text3Params.width = width * 30 / 100;
            text3Params.height = height * 9 / 100;

            col6.setLayoutParams(text3Params);
            //   col3.setWidth(width * 75 / 100);
            col6.setPadding(width * 2 / 100, 0, 0, 0);
            col6.setGravity(Gravity.CENTER | Gravity.LEFT);

            col7.setLayoutParams(text3Params);
            //   col3.setWidth(width * 75 / 100);
            col7.setPadding(width * 2 / 100, 0, 0, 0);
            col7.setGravity(Gravity.CENTER | Gravity.LEFT);

            col8.setLayoutParams(text3Params);
            //   col3.setWidth(width * 75 / 100);
            col8.setPadding(width * 2 / 100, 0, 0, 0);
            col8.setGravity(Gravity.CENTER | Gravity.LEFT);


            col9.setLayoutParams(text3Params);
            //   col3.setWidth(width * 75 / 100);
            col9.setPadding(width * 2 / 100, 0, 0, 0);
            col9.setGravity(Gravity.CENTER | Gravity.LEFT);


            col10.setLayoutParams(text3Params);
            //   col3.setWidth(width * 75 / 100);
            col10.setPadding(width * 2 / 100, 0, 0, 0);
            col10.setGravity(Gravity.CENTER | Gravity.LEFT);


//            col4.setLayoutParams(text2Params);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

//            col4.setLayoutParams(text2Params);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);


            return view;
        }

//        public void setHeader(String[] header) {
//            mHeader = header;
//        }

        public void setData(ArrayList<SiteTripreportDto> data) {
            mSiteTripReportData = data;
            notifyDataSetChanged();
        }

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            if (isFromDate) {
                mStartDate = String.valueOf(mDate2);
                //  mStartTimeValue=String.valueOf(mDate2);
                mTxtFromDate.setText(mDate2);
            } else {
                mEndDate = String.valueOf(mDate2);

                mTxtEndDate.setText(mDate2);
            }
//            mFromDate = String.valueOf(mDate);
//            mFromDateTxtValue = String.valueOf(mDate2);
//            mTxtDate.setText(mDate2);
            // }
        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:

                // set time picker as current time
                return new TimePickerDialog(this, timePickerListener, hour, minute,
                        false);

            case DATE_DIALOG_ID:
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                /** set date picker as current date */
                DatePickerDialog dialog = new DatePickerDialog(this,
                        datePickerListener, year, month, day);
                // set time picker as current time
                return dialog;

        }
        return null;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);

        }

    };

    private String utilTime(int value) {

        if (value < 10)
            return "0" + String.valueOf(value);
        else
            return String.valueOf(value);
    }


//    private void updateTime(int hours, int mins) {
//
//        String timeSet = "";
//        int hour2 = hours;
////        int mins2=mins;
//        if (hours > 12) {
//            hours -= 12;
//            timeSet = "PM";
//            //timeSet2 = "p.m.";
//        } else if (hours == 0) {
//            hours += 12;
//            timeSet = "AM";
////            timeSet2 = "a.m.";
//        } else if (hours == 12) {
//            timeSet = "PM";
////            timeSet2 = "p.m.";
//        } else {
//            timeSet = "AM";
////            timeSet2 = "a.m.";
//        }
//
//
//        String minutes = "";
//        if (mins < 10)
//            minutes = "0" + mins;
//        else
//            minutes = String.valueOf(mins);
//
//// Append in a StringBuilder
//        String aTime = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
//
//        String aTime2 = new StringBuilder().append(hour2).append(':')
//                .append(minutes).append(":00").toString();
//        if (isFromTime) {
//            mStartTime = aTime2;
//            mStartTimeValue = aTime;
//            mTxtFromTime.setText(mStartTimeValue);
//        } else {
//            mEndTime = aTime2;
//            mEndTimeValue = aTime;
//            mTxtEndTime.setText(mEndTimeValue);
//        }
//    }

    private void updateTime(int hours, int mins) {

//        String timeSet = "", timeSet2 = "";
//        if (hours > 12) {
//            hours -= 12;
//            timeSet = "PM";
//            timeSet2 = "p.m.";
//        } else if (hours == 0) {
//            hours += 12;
//            timeSet = "AM";
//            timeSet2 = "a.m.";
//        } else if (hours == 12) {
//            timeSet = "PM";
//            timeSet2 = "p.m.";
//        } else {
//            timeSet = "AM";
//            timeSet2 = "a.m.";
//        }
//
//
//        String minutes = "";
//        if (mins < 10)
//            minutes = "0" + mins;
//        else
//            minutes = String.valueOf(mins);

// Append in a StringBuilder


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, mins);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        System.out.println("Hi date is 222:::" + timeFormat.format(cal.getTime()));

//        String aTime = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
        String aTime = timeFormatShow.format(cal.getTime());
//        String aTime2 = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(":00").append(minutes).append(" ").append(timeSet2).toString();

        String aTime2 = timeFormat.format(cal.getTime());


        if (isFromTime) {
            mStartTime = aTime2;
            mStartTimeValue = aTime;
            mTxtFromTime.setText(mStartTimeValue);
        } else {
            mEndTime = aTime2;
            mEndTimeValue = aTime;
            mTxtEndTime.setText(mEndTimeValue);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SiteTripReport.this, MapMenuActivity.class));
        finish();
    }

}
