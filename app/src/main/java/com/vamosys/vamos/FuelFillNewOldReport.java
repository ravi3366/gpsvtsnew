package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.maps.GoogleMap;
import com.vamosys.model.FuelFillDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyMarkerView;
import com.vamosys.utils.UnCaughtException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FuelFillNewOldReport extends AppCompatActivity implements View.OnClickListener, OnChartGestureListener,
        OnChartValueSelectedListener {
    static ListView lv;
    TextView mTxtNoRecord, mTxtVehicleName;
    ImageView mCalendarImage, mImgMapViewClose;
    static LinearLayout mFuelDataMapLayout;
    Toolbar mToolbar;

    //    ImageView mImgDataViewChange;
    ConnectionDetector cd;
    SharedPreferences sp;
    static Const cons;

    static int width;
    static int height;
    boolean isHeaderPresent = false, mIsLineChartEnabled = false;
    private List<FuelFillDto> mFuelFillData = new ArrayList<FuelFillDto>();

    TableAdapter adapter;

    Dialog marker_info_dialog;


    //    String mTotalFuelValue = null;
    private static GoogleMap map;
    //    ImageView $ChangeView;
    boolean mIsMapPresent = false;
    String mStrUtcFromDate, mStrUtcToDate;
    String mSELECTED_MAP_TYPE = "Normal";
    static double mLatitude = 0;
    static double mLongitude = 0;
    static float vehicle_zoom_level = 15.5F;
    static Context mFuelContext;

    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

    View bottomSheet;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;

    private int year, month, day;
    static final int TIME_DIALOG_ID = 1111;
    static final int DATE_DIALOG_ID = 2222;
    private int hour;
    private int minute;
    Spinner spinnerInterval;
    Button mBtnSubmit;
    TextView mTxtFromDate, mTxtEndDate, mTxtFromTime, mTxtEndTime;
    String mStartDate, mEndDate, mStartTime, mStartTimeValue, mEndDateValue, mEndTime, mEndTimeValue, mIntervalSpinnerSelectedValue = "1";
    boolean isFromTime = false, isFromDate = false, isMapPresent = false;
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    ArrayList<String> mIntervalList = new ArrayList<String>();
    int mIntervalSpinnerSelectedPos = 0;
    private LineChart mChart;

    ArrayList<String> xAxis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(
                FuelFillNewOldReport.this));
        setContentView(R.layout.activity_fuel_fill_new_report);
        cons = new Const();
        init();
//        screenArrange();
//        System.out.println("Hi onCreate called ");

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.fuel_report));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        mFuelContext = getApplicationContext();
//        adapter = new TableAdapter(this);

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }


        mStartDate = Const.getCurrentDate();
//        mEndDate = Const.getTripYesterdayDate2();

//        mStartDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));


        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);

        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);


        mFromHourValue = 00;
        mFromMinuteValue = 00;

//        System.out.println("Hi date is :::" + timeFormat.format(cal.getTime()) + " mFromHourValue " + mFromHourValue + " " + mFromMinuteValue);

//        mStartTimeValue = "12:00 AM";
        mStartTimeValue = timeFormatShow.format(cal.getTime());
//        mStartTime = "12:00:00 am";
        mStartTime = timeFormat.format(cal.getTime());


        final Calendar c = Calendar.getInstance();
        // Current Hour
        hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        minute = c.get(Calendar.MINUTE);
        isFromTime = false;
        updateTime(hour, minute);

        mIntervalList = intervalSpinnerDataValue();


        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        adapter = new TableAdapter(this);


//        if (savedInstanceState != null) {
//            mStartDate = savedInstanceState.getString("mStartDate");
//            mStartTime = savedInstanceState.getString("mStartTime");
//            mEndDate = savedInstanceState.getString("mEndDate");
//            mEndTime = savedInstanceState.getString("mEndTime");
//            mIntervalSpinnerSelectedValue = savedInstanceState.getString("mIntervalSpinnerSelectedValue");
//
//            System.out.println("Hi orientation changes " + mStartDate + mEndDate + mStartTime);
//            setBottomLayoutData();
//        }

//        setBottomLayoutData();
//
//        if (cd.isConnectingToInternet()) {
////                                mStrFromDate = fromdatevalue.getText().toString().trim();
////                                mStrToDate = todatevalue.getText().toString().trim();
//
//            System.out.println("Hi 222 orientation changes " + mStartDate + mEndDate + mStartTime);
//
//            mFuelFillData.clear();
//            new getKmsSummaryData().execute();
//        } else {
//            Toast.makeText(getApplicationContext(),
//                    "Please check your network connection",
//                    Toast.LENGTH_SHORT).show();
//        }
//        }


//        mImgDataViewChange.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////               mIsMapPresent=false;
//                setListData();
//
//
//            }
//        });

        spinnerInterval
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mIntervalSpinnerSelectedPos = position;
                        if (position != 0) {
                            mIntervalSpinnerSelectedValue = String.valueOf(mIntervalList.get(position));
                        } else {
                            mIntervalSpinnerSelectedValue = "1";
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(FuelFillNewOldReport.this, MapMenuActivity.class));
                finish();


            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
//        System.out.println("Hi onResume called ");
        if (Constant.mFuelStoredFromDate != null) {
            mStartDate = Constant.mFuelStoredFromDate;
        }
        if (Constant.mFuelStoredToDate != null) {
            mStartTime = Constant.mFuelStoredToDate;
        }
        if (Constant.mFuelStoredFromTime != null) {
            mEndDate = Constant.mFuelStoredFromTime;
        }
        if (Constant.mFuelStoredToTime != null) {
            mEndTime = Constant.mFuelStoredToTime;
        }
        if (Constant.mFuelInterval != null) {
            mIntervalSpinnerSelectedValue = Constant.mFuelInterval;
        }
        mIsLineChartEnabled = Constant.mFuelIsChartView;
//        System.out.println("Hi onResume orientation changes " + mStartDate + mEndDate + mStartTime);
        setBottomLayoutData();

        if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();

//            System.out.println("Hi 222 orientation changes " + mStartDate + mEndDate + mStartTime);

            mFuelFillData.clear();
            new getKmsSummaryData().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        System.out.println("Hi onRestart called ");
    }

    @Override
    protected void onStart() {
        super.onStart();
//        System.out.println("Hi onStart called ");
    }

    @Override
    protected void onStop() {
        super.onStop();
//        System.out.println("Hi onStop called ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        System.out.println("Hi onDestroy called ");
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the state of item position


        Constant.mFuelStoredFromDate = mStartDate;
        Constant.mFuelStoredToDate = mStartTime;
        Constant.mFuelStoredFromTime = mEndDate;
        Constant.mFuelStoredToTime = mEndTime;
        Constant.mFuelInterval = mIntervalSpinnerSelectedValue;
        Constant.mFuelIsChartView = mIsLineChartEnabled;
//        outState.putString("mStartDate", mStartDate);
//        outState.putString("mStartTime", mStartTime);
//        outState.putString("mEndDate", mEndDate);
//        outState.putString("mEndTime", mEndTime);
//        outState.putString("mIntervalSpinnerSelectedValue", mIntervalSpinnerSelectedValue);
//        System.out.println("Hi save instance called ");
    }

//    @Override
//    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//
//        // Read the state of item position
//        mStartDate = savedInstanceState.getString("mStartDate");
//        mStartTime = savedInstanceState.getString("mStartTime");
//        mEndDate = savedInstanceState.getString("mEndDate");
//        mEndTime = savedInstanceState.getString("mEndTime");
//        mEndTime = savedInstanceState.getString("mIntervalSpinnerSelectedValue");
//
//        System.out.println("Hi 000 orientation changes " + mStartDate + mEndDate + mStartTime);
//
//
//    }


    public void init() {

        mChart = (LineChart) findViewById(R.id.chart);

//        $ChangeView = (ImageView) findViewById(R.id.fuel_fill_data_map_ViewIcon);
        mFuelDataMapLayout = (LinearLayout) findViewById(R.id.temperature_data_map_view_layout);

        lv = (ListView) findViewById(R.id.tstoppage_report_listView1);
//        mImgDataViewChange = (ImageView) findViewById(R.id.fuelFill_report_change_data_view);
//        mImgDataViewChange.setVisibility(View.GONE);

        mTxtVehicleName = (TextView) findViewById(R.id.selected_vehicle_txt);
        mTxtVehicleName.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
//        mTxtVehicleNameValue = (TextView) findViewById(R.id.fuelFill_vehicle_name_value_txt);
//        mTxtVehicleNameValue.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);

        mTxtNoRecord = (TextView) findViewById(R.id.report_no_record_txt);

        spinnerInterval = (Spinner) findViewById(R.id.spinner_interval);

        mTxtFromDate = (TextView) findViewById(R.id.txt_start_date_value);
        mTxtFromTime = (TextView) findViewById(R.id.txt_start_time_value);
        mTxtEndDate = (TextView) findViewById(R.id.txt_end_date_value);
        mTxtEndTime = (TextView) findViewById(R.id.txt_end_time_value);
        mBtnSubmit = (Button) findViewById(R.id.btn_done);

//        mCalendarImage = (ImageView) mToolbar.findViewById(R.id.img_calendar);
//        mImgMapViewClose = (ImageView) mToolbar.findViewById(R.id.img_map_close);
//        mImgMapViewClose.setOnClickListener(this);
//        mCalendarImage.setOnClickListener(this);

        mTxtFromTime.setOnClickListener(this);
        mTxtFromDate.setOnClickListener(this);
        mTxtEndDate.setOnClickListener(this);
        mTxtEndTime.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);

        mChart.setVisibility(View.GONE);
        lv.setVisibility(View.VISIBLE);

    }

    private void setupLineChart() {
//        mChart.setOnChartValueSelectedListener(FuelFillNewReport.this);
//        CombinedChart mchart = null;

        // no description text
//        mChart.setDescription("");
//        mChart.setNoDataTextDescription("You need to provide data for the chart.");
        mChart.getDescription().setEnabled(false);
        mChart.setNoDataText("No record found");
        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);

        mChart.setTouchEnabled(true);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setDragEnabled(true);
        mChart.setScaleXEnabled(true);
        mChart.setScaleYEnabled(false);
        mChart.setPinchZoom(false);

//        mChart.setDrawGridBackground(false);

        mChart.setDrawGridBackground(false);
        // mChart.setHighlightPerDragEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
//        mChart.setPinchZoom(true);

        // set an alternative background color
//        mChart.setBackgroundColor(Color.LTGRAY);

//        mChart.setTouchEnabled(true);
//        mChart.setDragEnabled(true);
//        mChart.setScaleEnabled(true);

        // add data
//        setData();

//        LineData data = new LineData(getXAxisValues(), getDataSet());
        LineData data = new LineData(getDataSet());
//        data.setGroupSpace(1f);
        data.setValueTextSize(10f);
        mChart.setData(data);
        mChart.setPinchZoom(false);
        mChart.setDoubleTapToZoomEnabled(true);

//        mChart.animateX(2000);
        mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);

        // get the legend (only possible after setting data)
//        Legend l = mChart.getLegend();
//
//        // modify the legend ...
//        // l.setPosition(LegendPosition.LEFT_OF_CHART);
//        l.setForm(Legend.LegendForm.LINE);
        // l.setTypeface(mTfLight);

//        l.setTextSize(11f);
//        l.setTextColor(Color.WHITE);
//        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);

//        l.setYOffset(11f);

        XAxis xAxis = mChart.getXAxis();
        // xAxis.setTypeface(mTfLight);
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis leftAxis = mChart.getAxisLeft();
        //  leftAxis.setTypeface(mTfLight);
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        // leftAxis.setAxisMaximum(200f);
        // leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setSpaceTop(0.0f);
        leftAxis.setSpaceBottom(0.0f);


        //leftAxis.setGranularityEnabled(true);

        YAxis rightAxis = mChart.getAxisRight();
        //  rightAxis.setTypeface(mTfLight);
//        rightAxis.setTextColor(Color.RED);
        //  rightAxis.setAxisMaximum(900);
        //  rightAxis.setAxisMinimum(-200);
//        rightAxis.setDrawGridLines(false);

        rightAxis.setEnabled(false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setDrawAxisLine(false);
        rightAxis.setSpaceTop(0.0f);
        rightAxis.setSpaceBottom(0.0f);

        mChart.getXAxis().setEnabled(false);

//        Legend l = mChart.getLegend();

        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
//        l.setForm(Legend.LegendForm.LINE);

        mChart.setOnChartValueSelectedListener(this);
        mChart.getLegend().setEnabled(false);
        mChart.invalidate();

        // rightAxis.setDrawZeroLine(false);
        // rightAxis.setGranularityEnabled(false);
    }


    private void formLineChartNew() {


        mChart.getDescription().setEnabled(false);
        mChart.setNoDataText("Loading...");
        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);

        mChart.setTouchEnabled(true);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setDragEnabled(true);
        mChart.setScaleXEnabled(true);
        mChart.setScaleYEnabled(false);
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);
        mChart.getDescription().setEnabled(false);
        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_routeplayback_marker_view, getXAxisValues(), "Fuel level");
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart

        XAxis xAxis = mChart.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setValueFormatter(new LabelFormatter(getXAxisValues()));
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setEnabled(false);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines();
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawLimitLinesBehindData(true);
        mChart.getAxisRight().setEnabled(false);

        mChart.setOnChartValueSelectedListener(this);
        mChart.getLegend().setEnabled(false);
        mChart.invalidate();


        // add data
        setData();

//        mChart.setVisibleXRange(20);
//        mChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        mChart.centerViewTo(20, 50, AxisDependency.LEFT);

        mChart.animateX(2500);
        //mChart.invalidate();

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        mChart.notifyDataSetChanged();

    }


    private void setData() {
        if (mFuelFillData.size() > 0) {
            ArrayList<Entry> values = new ArrayList<Entry>();
            for (int i = 0; i < mFuelFillData.size(); i++) {

                Entry v1e1 = new Entry(i, Float.valueOf(mFuelFillData.get(i).getFuelLtr()));
                values.add(v1e1);
            }


            LineDataSet set1;

            if (mChart.getData() != null &&
                    mChart.getData().getDataSetCount() > 0) {
                set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
                set1.setValues(values);
                mChart.getData().notifyDataChanged();
                mChart.notifyDataSetChanged();
            } else {
                // create a dataset and give it a type
                set1 = new LineDataSet(values, "DataSet 1");

                set1.setColor(Color.rgb(135, 206, 250));
                set1.setLineWidth(1.5f);
                set1.setDrawCircleHole(false);
                set1.setCircleColor(Color.BLACK);
                set1.setCircleRadius(3f);
                set1.setDrawCircles(true);
                set1.setValueTextSize(9f);
                set1.setDrawFilled(true);
                set1.setFormLineWidth(1f);
//                set1.setDrawValues(false);
                set1.setFormSize(15.f);
                set1.setDrawValues(true);
                if (Utils.getSDKInt() >= 18) {
                    // fill drawable only supported on api level 18 and above
                    Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
                    set1.setFillDrawable(drawable);
                } else {
                    set1.setFillColor(Color.BLACK);
                }


                ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                dataSets.add(set1); // add the datasets

                // create a data object with the datasets
                LineData data = new LineData(dataSets);

                // set data
                mChart.setData(data);
            }
        }
    }


    private ArrayList<String> getXAxisValues() {
        xAxis = new ArrayList<>();

        for (int i = 0; i < mFuelFillData.size(); i++) {
            xAxis.add(Const.getAcReportTime(mFuelFillData.get(i).getStartTime()));
        }

        return xAxis;
    }

    private ArrayList<ILineDataSet> getDataSet() {


        ArrayList<ILineDataSet> dataSets = null;

        ArrayList<Entry> valueSet1 = new ArrayList<>();


        for (int i = 0; i < mFuelFillData.size(); i++) {

            Entry v1e1 = new Entry(Float.valueOf(mFuelFillData.get(i).getFuelLtr()), i);
            valueSet1.add(v1e1);
        }

//        for (int i = 0; i < 20; i++) {
//
//
//            Entry v1e1;
//            if (i % 2 == 0) {
//                v1e1 = new Entry((float) i + 10, i);
//            } else {
//                v1e1 = new Entry((float) i + 30, i);
//            }
//
//            valueSet1.add(v1e1);
//        }

        LineDataSet lineDataSet = new LineDataSet(valueSet1, "X axis - Date Time Vs Y axis - Fuel(Ltrs)");
//        barDataSet1.setColor(Color.rgb(0, 155, 0));
//        barDataSet1.setFillAlpha(110);
//        barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);

        lineDataSet.setColor(Color.rgb(255, 127, 0));

        // barDataSet1.setFillColor(R.color.black);

//        barDataSet1.setColor(Color.BLACK);
//        lineDataSet.setCircleColor(Color.BLACK);
        lineDataSet.setLineWidth(1.5f);
        lineDataSet.setDrawCircles(false);
        lineDataSet.setAxisDependency(YAxis.AxisDependency.RIGHT);
//        lineDataSet.setCircleRadius(3f);
//        lineDataSet.setDrawCircleHole(false);
//        lineDataSet.setValueTextSize(9f);
//        lineDataSet.setDrawFilled(true);

        // barDataSet1.setLabel("Hello");
//        barDataSet1.;
        // barDataSet1.setBarSpacePercent(50f);
//        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Brand 2");
//        barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);

        return dataSets;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_calendar_chart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_calendar:
//                storeFavourtite();

                if (!bottomSheetEnabled) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    // setData();
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }

                break;

            case R.id.action_chart:
                if (mIsLineChartEnabled) {
//                    mImgDataViewChange.setBackgroundResource(R.drawable.bar_chart);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.line_chart));

//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart, getApplicationContext().getTheme()));
//                    } else {
//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart));
//                    }
                    mIsLineChartEnabled = false;
//                    setupBarChart();
                    mChart.setVisibility(View.GONE);
                    lv.setVisibility(View.VISIBLE);

                } else {
//                    mImgDataViewChange.setBackgroundResource(R.drawable.data_table);

//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table, getApplicationContext().getTheme()));
//                    } else {
//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table));
//                    }

                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.data_table));

                    mIsLineChartEnabled = true;

                    mChart.setVisibility(View.VISIBLE);
                    lv.setVisibility(View.GONE);


//                    setupLineChart();

                    formLineChartNew();
                }


                break;

            default:
                break;
        }

        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_start_date_value:
                isFromDate = true;
//                showDialog(DATE_DIALOG_ID);

                showDateDialog();

                break;
            case R.id.txt_start_time_value:
//                showDialog(DATE_DIALOG_FROMID);
                isFromTime = true;
                //   showDialog(TIME_DIALOG_ID);

                showTimeDialog();
                break;
            case R.id.txt_end_date_value:
//                createNumberDialog(1);
                isFromDate = false;
//                showDialog(DATE_DIALOG_ID);


                showDateDialog();
//                show(1);

                break;
            case R.id.txt_end_time_value:
//                createNumberDialog(0);
                isFromTime = false;
                // showDialog(TIME_DIALOG_ID);

                showTimeDialog();
//                show(0);
                break;
//            case R.id.img_calendar:
//                if (!bottomSheetEnabled) {
//                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                } else {
//                    // setData();
//                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }
//
//                if (bottomSheetEnabled) {
//                    bottomSheetEnabled = false;
//                } else {
//                    bottomSheetEnabled = true;
//                }
////                break;
//            case R.id.img_map_close:
//
//
//                mFuelDataMapLayout.setVisibility(View.GONE);
////                if (isMapPresent) {
//                isMapPresent = false;
//                lv.setVisibility(View.VISIBLE);
//                mImgMapViewClose.setVisibility(View.GONE);
////                }
//
//
//                break;
//            case R.id.temperature_data_map_ViewIcon:
//                opptionPopUp();
//                break;
            case R.id.btn_done:
//                createNumberDialog(0);
//                show(0);
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();
//                    mStoppageData = new ArrayList<StoppageReportDto>();
                    adapter = null;
                    adapter = new TableAdapter(FuelFillNewOldReport.this);
//     String url = Const.API_URL + "/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    mFuelFillData.clear();
                    lv.setAdapter(null);

//                    String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    // String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&fromTime=" + id_from_time.getText().toString() + "&toDate=" + id_to_date.getText().toString() + "&toTime=" + id_to_time.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                    // String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                    new getKmsSummaryData().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }
//                new CameraReportIamgeListActivtiy.getCameraReport().execute();
                break;
        }

    }

    private void setIntervalSpinnerValue() {
        ArrayAdapter dataAdapter = new ArrayAdapter(this, R.layout.spinner_row, intervalSpinnerValue());

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        // spinnerFromHour.setPrompt("Select From Hour");
        spinnerInterval.setAdapter(dataAdapter);
        spinnerInterval.setSelection((mIntervalSpinnerSelectedPos));
    }

    private ArrayList<String> intervalSpinnerValue() {
        ArrayList<String> mHrList = new ArrayList<String>();
        mHrList.add("Interval");
        mHrList.add("1 mins");
        mHrList.add("2 mins");
        mHrList.add("5 mins");
        mHrList.add("10 mins");
        mHrList.add("15 mins");
        mHrList.add("30 mins");

        return mHrList;
    }

    private ArrayList<String> intervalSpinnerDataValue() {
        ArrayList<String> mHrList = new ArrayList<String>();
        mHrList.add("0");
        mHrList.add("1");
        mHrList.add("2");
        mHrList.add("5");
        mHrList.add("10");
        mHrList.add("15");
        mHrList.add("30");

        return mHrList;
    }

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }


    private class getKmsSummaryData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                //  Const constaccess = new Const();

                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                Date d = f.parse(string_from_date);

                //  System.out.println("The from date after converted  :::::" + d);

                long fromMilliSeconds = d.getTime();
                // System.out.println("The from time in millis :::::" + fromMilliSeconds);
                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
//                long fromMilliSeconds =1470767400000;
//                long toMilliSeconds=1471685033000;


                //  System.out.println("The to time in millis :::::" + toMilliSeconds);


//                result = constaccess
//                        .sendGet(Const.API_URL + "/mobile/getExecutiveReport?groupId=" + mSelectedGroup + "&fromDate=" + mStrFromDate + "&toDate=" + mStrToDate + "&userId=" + mUserId, 30000);
                HttpConfig ht = new HttpConfig();
//                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDropFillReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=1&fromDate=" + mStrFromDate + "&fromTime=" + mStrFromTime + "&toDate=" + mStrToDate + "&toTime=" + mStrToTime + "&fuelDrop=true&fuelFill=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
//                System.out.println("Hi 333 orientation changes " + mStartDate + mEndDate + mStartTime);

//                http://www.gpsvts.net/mobile/getVehicleFuelHistory4Mobile?vehicleId=AKSHYA-TN29BZ1220&interval=1&fromDate=2017-11-22&fromTime=00:00:00&toDate=2017-11-29&toTime=12:07:32&fuelDrop=true&fuelFill=true&fromDateUTC=1511289000000&toDateUTC=1511937452000&userId=AKSHAYATRANS

                result = ht.httpGet(Const.API_URL + "/mobile/getVehicleFuelHistory4Mobile?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=" + mIntervalSpinnerSelectedValue + "&fuelDrop=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mFuelFillData = new ArrayList<>();
            System.out.println("The Fuel fill result is ::::" + result);
            if (result != null && result.length() > 0) {
                // System.out.println("The Fuel fill result is ::::" + result);

//                JSONArray jsonArray = null, jArrayFuel = null;
                try {
//                    jsonArray = new JSONArray(result);
//
//                    // for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject(result.trim());

                    if (jsonObject.has("history4Mobile")) {

//                        if (jsonObject.get("history4Mobile") != null && !jsonObject.get("history4Mobile").equals("null")) {
                        try {
                            JSONArray jArrayFuel = jsonObject.getJSONArray("history4Mobile");

                            String mPrevAdds = null;
                            for (int i = 0; i < jArrayFuel.length(); i++) {
                                JSONObject jsonFuelObject = jArrayFuel.getJSONObject(i);
                                FuelFillDto f = new FuelFillDto();

                                if (jsonFuelObject.has("dt")) {
                                    f.setStartTime(Long.valueOf(jsonFuelObject.getString("dt")));
                                }
//                            if (jsonFuelObject.has("latitude")) {
//                                f.setLatitude(jsonFuelObject.getDouble("latitude"));
//                            }
//                            if (jsonFuelObject.has("longitude")) {
//                                f.setLongitude(jsonFuelObject.getDouble("longitude"));
//                            }
                                if (jsonFuelObject.has("fuelLitr")) {
                                    f.setFuelLtr(jsonFuelObject.getString("fuelLitr"));
                                }

                                if (jsonFuelObject.has("address")) {
                                    if (jsonFuelObject.getString("address").equals("P")) {
                                        f.setAddress(mPrevAdds);
                                    } else {
                                        mPrevAdds = jsonFuelObject.getString("address");
                                        f.setAddress(jsonFuelObject.getString("address"));
                                    }
                                }

//                            if (jsonFuelObject.has("fuelFrom")) {
//                                f.setFuelFrom(jsonFuelObject.getDouble("fuelFrom"));
//                            }
//                            if (jsonFuelObject.has("fuelTo")) {
//                                f.setFuelTo(jsonFuelObject.getDouble("fuelTo"));
//                            }
                                mFuelFillData.add(f);

                            }
                        } catch (JSONException e) {

                        } finally {
//                            Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                        }
//                        } else {
//                            Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
//                        }

                    } else {
//                        Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                    }

//                    if (jsonObject.has("totalFuel")) {
//                        mTotalFuelValue = String.valueOf(jsonObject.getDouble("totalFuel"));
//                    }

                    // }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                setTableLayoutData();
            } else {
                Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(FuelFillNewOldReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private void setBottomLayoutData() {
        mTxtFromDate.setText(mStartDate);
        mTxtEndDate.setText(mEndDate);
        mTxtFromTime.setText(mStartTimeValue);
        mTxtEndTime.setText(mEndTimeValue);
        setIntervalSpinnerValue();
        mIntervalList = intervalSpinnerDataValue();

        Constant.mFuelStoredFromDate = null;
        Constant.mFuelStoredToDate = null;
        Constant.mFuelStoredFromTime = null;
        Constant.mFuelStoredToTime = null;
        Constant.mFuelInterval = null;

    }

    private void showTimeDialog() {
        if (isFromTime) {
            hour = mFromHourValue;
            minute = mFromMinuteValue;
//            System.out.println("Hi clicked hour minute value " + mFromHourValue + " " + mFromMinuteValue + " :: " + hour + " " + minute);
        } else {
            hour = mToHourValue;
            minute = mToMinuteValue;
//            System.out.println("Hi clicked hour minute value " + mToHourValue + " " + mToMinuteValue + " :: " + hour + " " + minute);
        }


//                hour=20;
//                minute=33;

        TimePickerDialog dialog2 = new TimePickerDialog(this, timePickerListener, hour, minute,
                false);
        dialog2.show();
    }

    private void showDateDialog() {
        if (isFromDate) {

            String[] dateArray = mStartDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        } else {

            String[] dateArray = mEndDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());

        }
//        System.out.println("Hi year " + year + " mo " + month + " da " + day);
//        final Calendar c = Calendar.getInstance();
//        year = c.get(Calendar.YEAR);
//        month = c.get(Calendar.MONTH);
//        day = c.get(Calendar.DAY_OF_MONTH);
        /** set date picker as current date */
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
        dialog.show();
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            if (isFromDate) {
                mStartDate = String.valueOf(mDate2);
                //  mStartTimeValue=String.valueOf(mDate2);
                mTxtFromDate.setText(mDate2);
            } else {
                mEndDate = String.valueOf(mDate2);

                mTxtEndDate.setText(mDate2);
            }
//            mFromDate = String.valueOf(mDate);
//            mFromDateTxtValue = String.valueOf(mDate2);
//            mTxtDate.setText(mDate2);
            // }
        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);


        }

    };


    private void updateTime(int hours, int mins) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, mins);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
//        System.out.println("Hi date is 222:::" + timeFormat.format(cal.getTime()));

//        String aTime = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
        String aTime = timeFormatShow.format(cal.getTime());
//        String aTime2 = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(":00").append(minutes).append(" ").append(timeSet2).toString();

        String aTime2 = timeFormat.format(cal.getTime());

        SimpleDateFormat hourFormat = new SimpleDateFormat(
                "HH");
        SimpleDateFormat minuteFormat = new SimpleDateFormat(
                "mm");

        if (isFromTime) {
            mStartTime = aTime2;
            mStartTimeValue = aTime;


            mFromHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
            mFromMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));
            mTxtFromTime.setText(mStartTimeValue);
//            System.out.println("Hi from time value " + mFromHourValue + " " + mFromMinuteValue);
        } else {
            mEndTime = aTime2;
            mEndTimeValue = aTime;
//            mToHourValue = cal.HOUR_OF_DAY;
//            mToMinuteValue = cal.MINUTE;
            mTxtEndTime.setText(mEndTimeValue);

//            SimpleDateFormat hourFormat = new SimpleDateFormat(
//                    "hh");
//            SimpleDateFormat minuteFormat = new SimpleDateFormat(
//                    "mm");

            mToHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
            mToMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));

//            System.out.println("Hi to time value " + mToHourValue + " " + mToMinuteValue);
        }


    }


    public void setTableLayoutData() {

//        adapter.setHeader(mHeader);
//        ListView listView = (ListView) findViewById(R.id.executive_report_listView1);
        if (mFuelFillData.size() > 0) {

            if (isHeaderPresent) {

            } else {
                lv.addHeaderView(adapter.getHeaderView(lv));
                isHeaderPresent = true;
            }


            lv.setAdapter(adapter);
            adapter.setData(mFuelFillData);

            adapter.notifyDataSetChanged();


            if (mIsLineChartEnabled) {
                mChart.setVisibility(View.VISIBLE);
                lv.setVisibility(View.GONE);


//                    setupLineChart();

                formLineChartNew();
            }
        } else {
            Toast.makeText(getApplicationContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
            if (mIsLineChartEnabled) {
                mChart.setVisibility(View.VISIBLE);
                lv.setVisibility(View.GONE);
                mChart.clear();
                mChart.setData(null);
                mChart.notifyDataSetChanged();
            } else {
                mChart.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);
                lv.setAdapter(null);
            }
        }


//        if (mTotalFuelValue != null) {
//            mTxtCumulativeFuelValue.setText(mTotalFuelValue + " ltrs");
//        } else {
//            mTxtCumulativeFuelValue.setText("0 ltr");
//        }
//        parseVehicleKms();

    }

    public class TableAdapter extends BaseAdapter {

        // private static final String TAG = "TableAdapter";

        private Context mContext;

        private String[] mHeader;


        private int mCurrentScroll;

        private int[] mColResources = {R.id.fuelfill_date_textView, R.id.fuelfill_fuel_ltr_textView,
                R.id.fuelfill_adds_textView};

        public TableAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return mFuelFillData != null ? mFuelFillData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mFuelFillData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

//            HorizontalScrollView view = null;
            LinearLayout view = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
//                view = (HorizontalScrollView) inflater.inflate(
//                        R.layout.fuelfill_list_item_table_row_new, parent, false);

                view = (LinearLayout) inflater.inflate(
                        R.layout.fuelfill_list_item_table_row_new, parent, false);


//                view.setOnScrollChangeListener(new OnScrollListener() {
//                    @Override
//                    public void onScrollChanged(View view, int scrollX) {
//
//                    }
//                });


//                view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {
//
//                    @Override
//                    public void onScrollChanged(View scrollView, int scrollX) {
//
//                        mCurrentScroll = scrollX;
//                        ListView listView = (ListView) scrollView.getParent();
//                        if (listView == null)
//                            return;
//
//                        for (int i = 0; i < listView.getChildCount(); i++) {
//                            View child = listView.getChildAt(i);
//                            if (child instanceof HorizontalScrollView
//                                    && child != scrollView) {
//                                HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
//                                if (scrollView2.getScrollX() != mCurrentScroll) {
//                                    scrollView2.setScrollX(mCurrentScroll);
//                                }
//                            }
//                        }
//                    }
//                });

            } else {
//                view = (HorizontalScrollView) convertView;
                view = (LinearLayout) convertView;
            }

            view.setScrollX(mCurrentScroll);

            if (position % 2 == 0) {
                view.setBackgroundColor(Color.WHITE);
            } else {
                view.setBackgroundColor(Color.LTGRAY);
            }

            FuelFillDto data = mFuelFillData.get(position);

//            for (int i = 0; i < mColResources.length; i++) {
            TextView col1 = (TextView) view.findViewById(mColResources[0]);

            col1.setText(cons.getAcReportTime(data.getStartTime()));
            TextView col2 = (TextView) view.findViewById(mColResources[1]);
            col2.setText(String.valueOf(data.getFuelLtr()));
            TextView col3 = (TextView) view.findViewById(mColResources[2]);
            col3.setText(String.valueOf(data.getAddress()));


//            TextView col4 = (TextView) view.findViewById(mColResources[3]);
//            col4.setText(String.valueOf(data.getFuelTo()));
//            TextView col5 = (TextView) view.findViewById(mColResources[4]);
//            col5.setText("Location");

//            SpannableString content = new SpannableString("Location");
//            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//            col5.setText(content);


//            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            vehicleNameParams.width = width * 35 / 100;
//            vehicleNameParams.height = height * 7 / 100;
//            col1.setLayoutParams(vehicleNameParams);
//            col1.setPadding(width * 2 / 100, 0, 0, 0);
//            col1.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col3.setLayoutParams(vehicleNameParams);
//
//            col3.setPadding(width * 2 / 100, 0, 0, 0);
//            col3.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            text2Params.width = width * 28 / 100;
//            text2Params.height = height * 7 / 100;
//            col2.setLayoutParams(text2Params);
//            col2.setPadding(width * 2 / 100, 0, 0, 0);
//            col2.setGravity(Gravity.CENTER | Gravity.LEFT);


//            col4.setLayoutParams(text2Params);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col5.setLayoutParams(text2Params);
//            col5.setPadding(width * 2 / 100, 0, 0, 0);
//            col5.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            col5.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mImgDataViewChange.setVisibility(View.VISIBLE);
//                    mIsMapPresent = true;
//                    mFuelDataMapLayout.setVisibility(View.VISIBLE);
//                    lv.setVisibility(View.GONE);
//                    mLatitude = mFuelFillData.get(position).getLatitude();
//                    mLongitude = mFuelFillData.get(position).getLongitude();
//                    setupMap();
//                }
//            });

//            }

            return view;
        }

        public View getHeaderView(ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
//            HorizontalScrollView view = (HorizontalScrollView) inflater
//                    .inflate(R.layout.fuelfill_list_item_table_header_new, parent, false);

            LinearLayout view = (LinearLayout) inflater
                    .inflate(R.layout.fuelfill_list_item_table_header_new, parent, false);

//            for (int i = 0; i < mColResources.length; i++) {
//                TextView col = (TextView) view.findViewById(mColResources[i]);
//                col.setText(mHeader[i]);
//            }


//            view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {
//
//                @Override
//                public void onScrollChanged(View scrollView, int scrollX) {
//
//                    mCurrentScroll = scrollX;
//                    ListView listView = (ListView) scrollView.getParent();
//                    if (listView == null)
//                        return;
//
//                    for (int i = 0; i < listView.getChildCount(); i++) {
//                        View child = listView.getChildAt(i);
//                        if (child instanceof HorizontalScrollView
//                                && child != scrollView) {
//                            HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
//                            if (scrollView2.getScrollX() != mCurrentScroll) {
//                                scrollView2.setScrollX(mCurrentScroll);
//                            }
//                        }
//                    }
//                }
//            });


            TextView col1 = (TextView) view.findViewById(R.id.fuelfill_date_textView_header);
            TextView col2 = (TextView) view.findViewById(R.id.fuelfill_fuel_ltr_textView_header);
            TextView col3 = (TextView) view.findViewById(R.id.fuelfill_adds_textView_header);
//            TextView col4 = (TextView) view.findViewById(R.id.fuelfill_ending_ltr_textView_header);
//            TextView col5 = (TextView) view.findViewById(R.id.link_textView_header);


//            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            vehicleNameParams.width = width * 35 / 100;
//            vehicleNameParams.height = height * 7 / 100;
//            col1.setLayoutParams(vehicleNameParams);
//            col1.setPadding(width * 2 / 100, 0, 0, 0);
//            col1.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col3.setLayoutParams(vehicleNameParams);
//
//            col3.setPadding(width * 2 / 100, 0, 0, 0);
//            col3.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            text2Params.width = width * 28 / 100;
//            text2Params.height = height * 7 / 100;
//            col2.setLayoutParams(text2Params);
//            col2.setPadding(width * 2 / 100, 0, 0, 0);
//            col2.setGravity(Gravity.CENTER | Gravity.LEFT);


//            col4.setLayoutParams(text2Params);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col5.setLayoutParams(text2Params);
//            col5.setPadding(width * 2 / 100, 0, 0, 0);
//            col5.setGravity(Gravity.CENTER | Gravity.LEFT);


            return view;
        }

//        public void setHeader(String[] header) {
//            mHeader = header;
//        }

        public void setData(List<FuelFillDto> data) {
            mFuelFillData = data;
            notifyDataSetChanged();
        }

    }

//    public void setListData() {
//        mImgDataViewChange.setVisibility(View.GONE);
//        mIsMapPresent = false;
//        lv.setVisibility(View.VISIBLE);
//        mFuelDataMapLayout.setVisibility(View.GONE);
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

//
//        if (mIsMapPresent) {
//            System.out.println("The map is present ::::::" + mIsMapPresent);
//            setListData();
//
//           // new getKmsSummaryData().execute();
//        } else {
        startActivity(new Intent(FuelFillNewOldReport.this, MapMenuActivity.class));
        finish();
//        }
    }


    @Override
    public void onChartGestureStart(MotionEvent me,
                                    ChartTouchListener.ChartGesture
                                            lastPerformedGesture) {

//        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me,
                                  ChartTouchListener.ChartGesture
                                          lastPerformedGesture) {

//        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            // or highlightTouch(null) for callback to onNothingSelected(...)
            mChart.highlightValues(null);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
//        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
//        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
//        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2,
                             float velocityX, float velocityY) {
//        Log.i("Fling", "Chart flinged. VeloX: "
//                + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
//        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
//        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

//        Log.i("Entry selected", e.toString());
//        Log.i("Entry data ", e.getData() + " " + e.getVal() + " " + e.getXIndex());
//        Log.i("LOWHIGH", "low: " + mChart.getLowestVisibleXIndex()
//                + ", high: " + mChart.getHighestVisibleXIndex());
//
//        Log.i("MIN MAX", "xmin: " + mChart.getXChartMin()
//                + ", xmax: " + mChart.getXChartMax()
//                + ", ymin: " + mChart.getYChartMin()
//                + ", ymax: " + mChart.getYChartMax());

//        String mToastMsg = xAxis.get(e.getXIndex()) + "\n" + e.getVal() + " ltrs";
//        final Toast toast = Toast.makeText(getApplicationContext(), mToastMsg, Toast.LENGTH_SHORT);
//        toast.show();
//
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                toast.cancel();
//            }
//        }, 1000);

    }


    @Override
    public void onNothingSelected() {
//        Log.i("Nothing selected", "Nothing selected.");
    }


}
