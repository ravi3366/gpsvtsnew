package com.vamosys.vamos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.model.PolylineOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.vamosys.model.HistoryParkingDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.PathOverlay;
import org.osmdroid.views.overlay.Polyline;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import at.markushi.ui.CircleButton;

public class HistoryNewActivity extends FragmentActivity implements View.OnClickListener {

    LinearLayout googleMapLayout;
    RelativeLayout rl;
    MapView mapview;
    private IMapController mapController;

    DBHelper dbhelper;
    SharedPreferences sp;
    ConnectionDetector cd;
    Const cons;
    View mCustomViewMarker = null;

    Dialog dialog;

    int width, height;

    private boolean isFromTime;
    private boolean isFromDate;
    TextView Txt_fromDate,Txt_fromTime,Txt_toDate,Txt_toTime;
    String radio_value;
    ProgressDialog progressDialog,progressDialog1;
    String fromDate,fromTime,toDate,toTime,CustomfromDate,CustomfromTime,CustomtoDate,CustomtoTime;
    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss");
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    private int year, month, day, hour, minute;
    private Dialog history_alert;
    TextView mTxt_msg1,mTxt_msg2,ok_button;
    ImageView back_arrow;

    Double currlat,currlng,startlat,startlng,endlat,endlng,parklat,parklng,Sparklat,Sparklng,Eparklat,Eparklng;

    String Tagg="OSM Map Test";
    SlidingUpPanelLayout slidingUpPanelLayout;
    RelativeLayout relativeLayoutBottomSlideup;
    LinearLayout linearlayoutBottomSlideup;
    CardView value_speed,Speed_cardview,Distance_cardview;
    ImageView up_arrow,down_arrow;

    List<HistoryParkingDto> historyParkingDtos = new ArrayList<>();
    List<GeoPoint> geoPoints = new ArrayList<>();
    List<String> mPosList = new ArrayList<String>();
    List<String> mAddsList = new ArrayList<String>();
    ArrayList<String> speedlist;
    ArrayList<String> vehicle = new ArrayList<String>();
    ArrayList<String> datetimelist;
    ArrayList<String> mIdleTimeList;
    ArrayList<String> distancelist;
    List<HistoryParkingDto> parkingdata = new ArrayList<>();
    HashMap<String, Integer> parkingMarker = new HashMap<String, Integer>();


    TextView vehicleId,Txt_start_location,Txt_end_location,Txt_no_parking,Txt_No_overspeed,Txt_no_distance_coverd,Txt_timeTaken;
    TextView Txt_end_time,Txt_start_time,Txt_parked_time,Txt_Moving_time,Txt_Idel_time,Txt_Nodata_time,Txt_Speed_limit,Txt_odo_distance;

    TextView Txt_change_speed,Txt_change_distance,Txt_change_time;
    CircleButton PlayButton,Live_history,parking_marker,changeView,navigation;

    String mHistoryResult, mSpeedLimit;
    String mParkedTime, mMovingTime, mIdleTime, mNoDataTime, mOdoDistance, mDistanceTravelled;
    double mPopupStartLat = 0.0, mPopupEndLat = 0.0, mPopupStartLng = 0.0, mPopupEndLng = 0.0;

    int currentIndex = 0;

    private Animator animator = new Animator();
    Marker animateMarker;

    private final Handler mHandler = new Handler();
    private boolean is_clicked;

    ImageView mImgMarkerIcon;
    private View markerView;

    ArrayList<GeoPoint> polylinePoints = new ArrayList<GeoPoint>();

    private static int ANIMATE_SPEEED = 3000;
    private static int ANIMATE_SPEEED_TURN = 3000;
    boolean isPauseCalled = false, isStopCalled = false;
    private Button speed;
    private boolean backButtonPressed=false,pause=false;
    int speed_value=2;

    float history_zoom_level = 13.5F;

    Boolean Parking_isclicked=true,History_isclicked=true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        //important! set your user agent to prevent getting banned from the osm servers
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.activity_history_new_google_map);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        dbsetup();
        init();
        slidingUpPanelLayout.setPanelHeight(0);
        value_speed.setVisibility(View.GONE);
        Speed_cardview.setVisibility(View.GONE);
        Distance_cardview.setVisibility(View.GONE);
        dbhelper.deletecurrent_vehicleinfo();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        cons = new Const();
        mCustomViewMarker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        setUpSlidePanel();
        opptionPopUp();

        speed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(speed_value==3){
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"3x",Toast.LENGTH_LONG).show();
                    speed.setText("3X");
                    animator.stopAnimation();
                    if (currentIndex == 0 || (currentIndex == geoPoints.size())) {
                        if (mapview != null) {
                            mapview.getOverlays().clear();
                            mapview.invalidate();
                            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
                        }
                    }
                    ANIMATE_SPEEED = 500;
                    ANIMATE_SPEEED_TURN = 600;
                    speed_value=1;
                    StartSpeed();
                }
                else if(speed_value==2){
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"2x",Toast.LENGTH_LONG).show();
                    speed.setText("2X");
                    animator.stopAnimation();
                    if (currentIndex == 0 || (currentIndex == geoPoints.size())) {
                        if (mapview != null) {
                            mapview.getOverlays().clear();
                            mapview.invalidate();
                            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
                        }
                    }
                    ANIMATE_SPEEED = 800;
                    ANIMATE_SPEEED_TURN = 800;
                    speed_value=3;
                    StartSpeed();
                }
                else if(speed_value==1){
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"1x",Toast.LENGTH_LONG).show();
                    speed.setText("1X");
                    animator.stopAnimation();
                    if (currentIndex == 0 || (currentIndex == geoPoints.size())) {
                        if (mapview != null) {
                            mapview.getOverlays().clear();
                            mapview.invalidate();
                            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
                        }
                    }
                    ANIMATE_SPEEED = 2000;
                    ANIMATE_SPEEED_TURN = 2000;
                    speed_value=2;
                    StartSpeed();
                }
            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case  R.id.back_arrow:
                startActivity(new Intent(HistoryNewActivity.this,MapMenuActivity.class));
                finish();
                break;
            case  R.id.nav_buttons:
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + mPopupStartLat + "," + mPopupStartLng + ""));
                startActivity(intent);
                break;
            case R.id.play_buttons:
                if(is_clicked){
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"play",Toast.LENGTH_LONG).show();
                    is_clicked=false;
                    animator.stopAnimation();
                    PlayButton.setImageResource(R.drawable.play_outline);
//                    showCurrentAddress();
                    if (currentIndex == geoPoints.size()) {
                        currentIndex = 0;
                    }
                }
                else {
                    PlayButton.setImageResource(R.drawable.pause_outline);
//                    Toast.makeText(HistoryNewGoogleMapActivity.this,"pause",Toast.LENGTH_LONG).show();
                    is_clicked=true;
                    animator.startAnimation(true);
                }
                break;

            case R.id.live_history:
                if(History_isclicked){
                    PlayButton.setImageResource(R.drawable.play_outline);
                    Live_history.setColor(getApplicationContext().getResources().getColor(R.color.colorPrimary));
                    Live_history.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_bule));
                    Live_history.setImageResource(R.drawable.white_history);
//                    parking_marker.setImageResource(R.drawable.parking_outline);
//                    parking_marker.setColor(getApplicationContext().getResources().getColor(R.color.white));
//                    parking_marker.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
//                    Parking_isclicked=true;
                    is_clicked=true;
                    new ShowHistory().execute();
                    Log.d("check 1 day history","completed");
                    History_isclicked=false;
                }
                else {
                    History_isclicked=true;
//                    drawOldPolyine();
//                    animator.startAnimation(true);
//                    PlayButton.setImageResource(R.drawable.pause_outline);
//                    Live_history.setImageResource(R.drawable.todayhistory);
//                    Live_history.setColor(getApplicationContext().getResources().getColor(R.color.white));
//                    Live_history.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
////                    Toast.makeText(HistoryNewGoogleMapActivity.this,"Double history",Toast.LENGTH_LONG).show();

                    Log.d("play clicking","play4");
                    History_isclicked=true;
                    if(!Parking_isclicked){
                        if (mapview != null) {
                          mapview.getOverlays().clear();
                          mapview.invalidate();
                          mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
                        }
                        new ShowParking().execute();
                        animator.startAnimation(true);
                        PlayButton.setImageResource(R.drawable.pause_outline);
                        Live_history.setImageResource(R.drawable.todayhistory);
                        Live_history.setColor(getApplicationContext().getResources().getColor(R.color.white));
                        Live_history.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
                    }
                    else {
                        if (mapview != null) {
                            mapview.getOverlays().clear();
                            mapview.invalidate();
                            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
                        }
                        drawOldPolyine();
                        animator.startAnimation(true);
                        PlayButton.setImageResource(R.drawable.pause_outline);
                        Live_history.setImageResource(R.drawable.todayhistory);
                        Live_history.setColor(getApplicationContext().getResources().getColor(R.color.white));
                        Live_history.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
                    }
                }
                break;
            case R.id.parking_marker:
                if(Parking_isclicked){
//                    animator.stopAnimation();
                    parking_marker.setColor(getApplicationContext().getResources().getColor(R.color.colorPrimary));
                    parking_marker.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_bule));
                    parking_marker.setImageResource(R.drawable.white_parking);
                    PlayButton.setImageResource(R.drawable.play_outline);
//                    Live_history.setImageResource(R.drawable.todayhistory);
//                    Live_history.setColor(getApplicationContext().getResources().getColor(R.color.white));
//                    Live_history.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
//                    History_isclicked=true;
                    is_clicked=true;
                    new ShowParking().execute();
                    Parking_isclicked=false;
                }
                else {
//                    Parking_isclicked=true;
//                    drawOldPolyine();
//                    animator.startAnimation(true);
//                    PlayButton.setImageResource(R.drawable.pause_outline);
//                    parking_marker.setImageResource(R.drawable.parking_outline);
//                    parking_marker.setColor(getApplicationContext().getResources().getColor(R.color.white));
//                    parking_marker.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
////                    Toast.makeText(HistoryNewGoogleMapActivity.this,"Double parking",Toast.LENGTH_LONG).show();

                    Log.d("play clicking","play6");
                    Parking_isclicked=true;
                    if(!History_isclicked){
                        if (mapview != null) {
                            mapview.getOverlays().clear();
                            mapview.invalidate();
                            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
                        }
                        new ShowHistory().execute();
                        drawOldPolyine();
                        animator.startAnimation(true);
                        PlayButton.setImageResource(R.drawable.pause_outline);
                        parking_marker.setImageResource(R.drawable.parking_outline);
                        parking_marker.setColor(getApplicationContext().getResources().getColor(R.color.white));
                        parking_marker.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
                    }
                    else{
                        if (mapview != null) {
                            mapview.getOverlays().clear();
                            mapview.invalidate();
                            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
                        }
                        drawOldPolyine();
                        animator.startAnimation(true);
                        PlayButton.setImageResource(R.drawable.pause_outline);
                        parking_marker.setImageResource(R.drawable.parking_outline);
                        parking_marker.setColor(getApplicationContext().getResources().getColor(R.color.white));
                        parking_marker.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.round_ripple_white));
                    }
                }
                break;


        }

    }

    public void drawOldPolyine(){
        Log.d("draw old polyline","draw");
        if(historyParkingDtos.size()>2) {
//            if (mapview != null) {
//                mapview.getOverlays().clear();
//                mapview.invalidate();
//                mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//            }
            ArrayList<GeoPoint> pathPoints = new ArrayList<>();
            Marker StartMarker = new Marker(mapview);
            for (int i = 0; i <= currentIndex; i++) {

                Log.d("draw old polyline1", "draw");
                HistoryParkingDto h = historyParkingDtos.get(i);
                currlat = h.getLatitude();
                currlng = h.getLongitude();
                Log.d("currlatti",""+currlat);
                Log.d("currlongii",""+currlng);
                GeoPoint newPos = new GeoPoint(currlat,currlng);
                pathPoints.add(newPos);
                if(i==0){
                    mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
                    StartMarker.setPosition(newPos);
                    StartMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    StartMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this,markerView));
                    mapview.getOverlays().add(StartMarker);
                    mapview.invalidate();
                }

            }
            Polyline line = new Polyline();   //see note below!
            line.setPoints(pathPoints);
            mapview.getOverlayManager().add(line);
        }
    }


    private class ShowHistory extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            for (int i = 0; i < historyParkingDtos.size(); i++) {
                HistoryParkingDto h = historyParkingDtos.get(i);
                if(i==0){
                    startlat =h.getLatitude() ;
                    startlng = h.getLongitude();
                }
                if(i==historyParkingDtos.size()-1){
                    endlat = h.getLatitude();
                    endlng = h.getLongitude();
                    Log.d("endlatti",""+endlat);
                    Log.d("endlongii",""+endlng);
                }
            }
//            if (mapview != null) {
//                mapview.getOverlays().clear();
//                mapview.invalidate();
//                mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//            }
            return "history";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowProgress();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           ArrayList<GeoPoint> pathPoints = new ArrayList<>();
            Marker stableMarker = new Marker(mapview);
            Marker StartMarker = new Marker(mapview);

//            if (mapview != null) {
//                mapview.getOverlays().clear();
//                mapview.invalidate();
//                mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//            }

//            animator.stopAnimation();
            animator.startAnimation(true);

            if(historyParkingDtos!=null && historyParkingDtos.size()>0){
                for(int i=0;i<historyParkingDtos.size();i++){
                    HistoryParkingDto h = historyParkingDtos.get(i);
                    currlat = h.getLatitude();
                    currlng = h.getLongitude();
                    Log.d("currlatti",""+currlat);
                    Log.d("currlongii",""+currlng);
                    GeoPoint newPos = new GeoPoint(currlat,currlng);
                    pathPoints.add(newPos);
                    if(i==0){
                        mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
                        StartMarker.setPosition(newPos);
                        StartMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                        StartMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this,markerView));
                        mapview.getOverlays().add(StartMarker);
                        mapview.invalidate();
                    }
                    if(i==historyParkingDtos.size()-1){
                        mImgMarkerIcon.setImageResource(R.drawable.marker_icon);
                        stableMarker.setPosition(newPos);
                        stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                        stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this,markerView));
                        mapview.getOverlays().add(stableMarker);
                        mapview.invalidate();
                    }

                }
                Polyline line = new Polyline();   //see note below!
                line.setPoints(pathPoints);
                mapController.setZoom(6.0);
                mapview.getOverlayManager().add(line);

            }

            HideProgress();
        }
    }
    private class ShowParking extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            int count=0;
            double lat,lng;
            for(int x=0;x<historyParkingDtos.size();x++){
                Log.d("parking","parking detail");
                HistoryParkingDto h = historyParkingDtos.get(x);
                HistoryParkingDto parkdata= new HistoryParkingDto();
                if(h.getPosition().equalsIgnoreCase("P")){
                    count++;
                    lat=h.getLatitude();
                    lng=h.getLongitude();
                    parkdata.setLatitude(lat);
                    parkdata.setLongitude(lng);
                    parkdata.setPosition(h.getPosition());
                    parkdata.setAddress(h.getAddress());
                    parkdata.setDate(h.getDate());
                    parkdata.setParkedTime(h.getParkedTime());
                    parkingdata.add(parkdata);
                    Log.d("parking","parking detail"+parkdata.getLatitude());
                }
                Log.d("parking","count1 "+count);
            }
            return "history";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowProgress();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ArrayList<GeoPoint> pathPoints = new ArrayList<>();
            Marker stableMarker = new Marker(mapview);
            Marker StartMarker = new Marker(mapview);

//            if (mapview != null) {
//                mapview.getOverlays().clear();
//                mapview.invalidate();
//                mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//            }

//            animator.stopAnimation();
            animator.startAnimation(true);
            PlayButton.setImageResource(R.drawable.pause_outline);
            if(historyParkingDtos!=null && historyParkingDtos.size()>0){
                for(int i=0;i<historyParkingDtos.size();i++){
                    HistoryParkingDto h = historyParkingDtos.get(i);
                    currlat = h.getLatitude();
                    currlng = h.getLongitude();
                    Log.d("currlatti",""+currlat);
                    Log.d("currlongii",""+currlng);
                    GeoPoint newPos = new GeoPoint(currlat,currlng);
                    pathPoints.add(newPos);
                    if(i==0){
                        mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
                        StartMarker.setPosition(newPos);
                        StartMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                        StartMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this,markerView));
                        mapview.getOverlays().add(StartMarker);
                        mapview.invalidate();
                    }
                    if(i==historyParkingDtos.size()-1){
                        mImgMarkerIcon.setImageResource(R.drawable.marker_icon);
                        stableMarker.setPosition(newPos);
                        stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                        stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this,markerView));
                        mapview.getOverlays().add(stableMarker);
                        mapview.invalidate();
                    }
                }
                Polyline line = new Polyline();   //see note below!
                line.setPoints(pathPoints);
                mapController.setZoom(6.0);
                mapview.getOverlayManager().add(line);
            }

            for(int x=0;x<parkingdata.size();x++){
                Log.d("parking","parking detail");
                final HistoryParkingDto h = parkingdata.get(x);
                if(h.getPosition().equalsIgnoreCase("P")){
                    parklat =h.getLatitude();
                    parklng =h.getLongitude();
                    GeoPoint newPos = new GeoPoint(parklat,parklng);
                    final Marker ParkingMarker = new Marker(mapview);
                    mImgMarkerIcon.setImageResource(R.drawable.icon_parking);
                    ParkingMarker.setPosition(newPos);
                    ParkingMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    ParkingMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this,markerView));
                    Log.d(Tagg,"date "+String.valueOf(h.getDate()));
                    ParkingMarker.setTitle(h.getAddress()+" "+cons.getTimefromserver(String.valueOf(h.getDate())));
                    mapview.getOverlays().add(ParkingMarker);
                    mapview.invalidate();
                    ParkingMarker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker, MapView mapView) {
                            ParkingMarker.showInfoWindow();
                            return true;
                        }
                    });
                }
            }
            HideProgress();
        }
    }
    public void StartSpeed(){
        Handler seekbarhandler = new Handler();
        seekbarhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mapview.getZoomLevelDouble() <= 3.5) {
                    Log.d("history zoom level","history1 "+history_zoom_level);
                    history_zoom_level = history_zoom_level;
                    Log.d("history zoom level","history2 "+history_zoom_level);

                } else {
                    Log.d("history zoom level","history3 "+history_zoom_level);
                    history_zoom_level = (float) mapview.getZoomLevelDouble();
                    Log.d("history zoom level","history4 "+history_zoom_level);
                }
                PlayButton.setImageResource(R.drawable.pause_outline);
                animator.startAnimation(true);
            }
        }, 500);
    }
    private void viewMap(){
        Log.d("checkHistory","viewmap");
        if (cd.isConnectingToInternet()) {

            String string_from_date = fromDate.toString().trim() + " " + fromTime.toString().trim();
            String string_to_date = toDate.toString().trim() + " " + toTime.toString().trim();
            Log.d(Tagg,string_from_date+" & "+string_to_date);
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = null, d1 = null;
            try {
                d = f.parse(string_from_date);

                d1 = f.parse(string_to_date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            long fromMilliSeconds = d.getTime();
            long toMilliSeconds = d1.getTime();

            String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + fromDate.toString() + "&fromTime=" + fromTime.toString() + "&toDate=" + toDate.toString() + "&toTime=" + toTime.toString() + "&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds +
                    "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
            String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
            new GetVehicleHistory().execute(url);
//            new GetPOIInformation().execute(par);
        } else {
            Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
            internet_toast.show();
        }
    }



    public class GetVehicleHistory extends AsyncTask<String,String,String>{
        @Override
        protected String doInBackground(String... urls) {
            Log.d(Tagg,"getvehicleHistory");
            String response_from_server = "";
            if (urls[0].equals("replay")) {
                response_from_server = sp.getString("historyplaybackdata", "");
            } else {
                try {
                    HttpConfig ht = new HttpConfig();
                    response_from_server = ht.httpGet(urls[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return response_from_server;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(HistoryNewActivity.this);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(Tagg,"getHis on post");
            if (result != null && !result.isEmpty()) {
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("historyplaybackdata", result);
                editor.commit();
//                value_speed.setVisibility(View.VISIBLE);
//                Speed_cardview.setVisibility(View.VISIBLE);
//                Distance_cardview.setVisibility(View.VISIBLE);
                slidingUpPanelLayout.setPanelHeight((int) getApplicationContext().getResources().getDimension(R.dimen.size150));
                historyData(result);
                historyPlayBack(result, progressDialog);
            }
            else {
                Toast.makeText(getApplicationContext(), "No History Data Available", Toast.LENGTH_SHORT).show();
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                startActivity(new Intent(HistoryNewActivity.this, VehicleListActivity.class));
                finish();
            }
        }
    }
    public  void historyData(String result){
        if(historyParkingDtos!=null){
            historyParkingDtos.clear();
        }
        try {
            JSONObject jsonObject1= new JSONObject(result);
            JSONArray jsonArray=null;
            if(result!=null){
                if(jsonObject1.has("history4Mobile")&&jsonObject1.get("history4Mobile")!=null){
                    jsonArray=jsonObject1.getJSONArray("history4Mobile");
                    JSONObject getData=null;
                    for(int i=0;i<jsonArray.length();i++){
                        getData=jsonArray.getJSONObject(i);
                        HistoryParkingDto h= new HistoryParkingDto();
                        //                                primaryEngine: "OFF"
                        if(getData.has("lt")&&getData.get("lt")!=null){
                            h.setLatitude(getData.getDouble("lt"));
                        }
                        if(getData.has("lg")&&getData.get("lg")!=null){
                            h.setLongitude(getData.getDouble("lg"));
                        }
                        if(getData.has("sp")&&getData.get("sp")!=null){
                            h.setSpeed(getData.getInt("sp"));
                        }
                        if(getData.has("dt")&&getData.get("dt")!=null){
                            h.setDate(getData.getLong("dt"));
                        }
                        if(getData.has("ps")&&getData.get("ps")!=null){
                            h.setPosition(getData.getString("ps"));
                        }
                        if(getData.has("pt")&&getData.get("pt")!=null){
                            h.setParkedTime(getData.getString("pt"));
                        }
                        if(getData.has("address")&&getData.get("address")!=null){
                            h.setAddress(getData.getString("address"));
                        }
                        if(getData.has("primaryEngine")&&getData.get("primaryEngine")!=null){
                            h.setIgnitionStatus(getData.getString("lt"));
                        }

                        historyParkingDtos.add(h);
                    }
                }
            }
            Log.d(Tagg,"historyParkingDtos "+historyParkingDtos.size());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void historyPlayBack(final String result, final ProgressDialog dialog) {
        String totalrows = null;
        if (result != null && result.length() != 0){
            mHistoryResult = result;
            if (mapview != null) {
                mapview.getOverlays().clear();
                mapview.invalidate();
                mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
            }

            try {

                ParsingClass parsingClass = new ParsingClass();
                parsingClass.historyParse(result);
                geoPoints = new ArrayList<GeoPoint>();
                speedlist = new ArrayList<String>();
                datetimelist = new ArrayList<String>();
                distancelist = new ArrayList<String>();
                mIdleTimeList = new ArrayList<String>();
                if (!geoPoints.isEmpty()) {
                    geoPoints.clear();
                    //  directionlist.clear();
                    mPosList.clear();
                    speedlist.clear();
                    datetimelist.clear();
                    distancelist.clear();
                    mIdleTimeList.clear();
                    mAddsList.clear();
                }
                String vehicle_short_name;
                PolylineOptions options = new PolylineOptions().width(8);
                PolylineOptions overspeed_options = new PolylineOptions().width(4).geodesic(true);
                vehicle_short_name = parsingClass.getshortname();
                vehicleId.setText((parsingClass.getshortname().equals("null") ? "No Data" : parsingClass.getshortname()));

                Log.d("vehiclehistory",""+parsingClass.getvehicleType());

                Txt_no_distance_coverd.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance() + " KMS"));


                Txt_start_time.setText(cons.getTripTimefromserver((parsingClass.getFromDateTimeUTC().equals("null") ? "No Data" : parsingClass.getFromDateTimeUTC())));
                Txt_end_time.setText(cons.getTripTimefromserver((parsingClass.getToDateTimeUTC().equals("null") ? "No Data" : parsingClass.getToDateTimeUTC())));

                Txt_No_overspeed.setText(String.valueOf(parsingClass.getmOverSpeedCount()));
                Txt_no_parking.setText(String.valueOf(parsingClass.getmParkedCount()));


                mSpeedLimit = parsingClass.getOverSpeedLimit();
                mOdoDistance = parsingClass.getodoDistance();
                mParkedTime = parsingClass.getTotalParkedTime();
                mIdleTime = parsingClass.getTotalIdleTime();
                mMovingTime = parsingClass.getTotalMovingTime();
                mNoDataTime = parsingClass.getTotalNoDataTime();
                mDistanceTravelled = parsingClass.gettripDistance();
                totalrows = parsingClass.gettotalRows();
                Txt_timeTaken.setText(cons.getVehicleTime(parsingClass.gettotalRunningTime()));
                try {
                    JsonFactory f = new MappingJsonFactory();
                    JsonParser historyjsonParser = f.createJsonParser(result);
                    JsonToken current;
                    current = historyjsonParser.nextToken();
                    if (current != JsonToken.START_OBJECT) {
                        return;
                    }
                    while (historyjsonParser.nextToken() != JsonToken.END_OBJECT) {
                        String fieldName = historyjsonParser.getCurrentName();
                        if (fieldName.equals("history4Mobile")) {
                            current = historyjsonParser.nextToken();
                            if (current == JsonToken.START_ARRAY) {
                                int current_node_index = 0;
                                while (historyjsonParser.nextToken() != JsonToken.END_ARRAY) {
                                    // read the record into a tree model,
                                    // this moves the parsing position to the end of it
                                    JsonNode node = historyjsonParser.readValueAsTree();
                                    // And now we have random access to everything in the object
                                    if (!node.isNull()) {
                                        parsingClass.historyobjectparsing(node);
                                        double lat = parsingClass.getCurr_lat();
                                        double lng = parsingClass.getCurr_long();
                                        GeoPoint position = new GeoPoint(lat, lng);
                                        geoPoints.add(position);
                                        mPosList.add(parsingClass.getPosition());
                                        mAddsList.add(parsingClass.getAddress());
                                        speedlist.add(parsingClass.getSpeed());
                                        datetimelist.add(parsingClass.getlastseen());
                                        distancelist.add(parsingClass.getDistanCovered());
                                        if (parsingClass.getparked_time() != null) {
                                            mIdleTimeList.add(parsingClass.getparked_time());
                                        } else {
                                            mIdleTimeList.add("");
                                        }
                                        if (Integer.parseInt(totalrows) == 1){
                                            mPopupEndLat = parsingClass.getCurr_lat();
                                            mPopupEndLng = parsingClass.getCurr_long();
                                            Log.d("mPopupEndLat",""+mPopupEndLat);
                                            Log.d("mPopupEndLng",""+mPopupEndLng);
                                        }
                                        if (current_node_index == 0) {
                                            mPopupStartLat = parsingClass.getCurr_lat();
                                            mPopupStartLng = parsingClass.getCurr_long();

                                        } else if (current_node_index == Integer.parseInt(totalrows) - 1) {
                                            mPopupEndLat = parsingClass.getCurr_lat();
                                            mPopupEndLng = parsingClass.getCurr_long();

                                        }

                                    }
                                    current_node_index++;
                                }
                            }
                        }
                    }
                    Log.d(Tagg,"add List "+mAddsList.size());
                    Txt_start_location.setText(mAddsList.get(0));
                    Txt_end_location.setText(mAddsList.get(mAddsList.size()-1));
                    if (mSpeedLimit != null) {
                        Txt_Speed_limit.setText(mSpeedLimit + " Km/hr");
                    } else {
                        Txt_Speed_limit.setText("0 Km/hr");
                    }

                    if (mOdoDistance != null) {
                        Txt_odo_distance.setText(mOdoDistance + " Kms");
                    } else {
                        Txt_odo_distance.setText("0 Km");
                    }

                    Txt_parked_time.setText(mParkedTime);
                    Txt_Idel_time.setText(mIdleTime);
                    Txt_Moving_time.setText(mMovingTime);
                    Txt_Nodata_time.setText(mNoDataTime);
                } catch (IOException e) {
                    e.printStackTrace();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                pause = false;
                backButtonPressed = false;

            } catch (Exception je) {
                je.printStackTrace();
            } finally {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }
        else {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            Toast.makeText(getApplicationContext(), "No History Data Available for the Selected Intervals", Toast.LENGTH_SHORT).show();
        }
        playHistory();
    }

    private void playHistory(){
        is_clicked=true;
        if (geoPoints.size() > 2) {
            PlayButton.setVisibility(View.VISIBLE);
            Live_history.setVisibility(View.VISIBLE);
            parking_marker.setVisibility(View.VISIBLE);
            speed.setVisibility(View.VISIBLE);
            PlayButton.setImageResource(R.drawable.pause_outline);
            navigation.setVisibility(View.VISIBLE);
            slidingUpPanelLayout.setPanelHeight((int) getApplicationContext().getResources().getDimension(R.dimen.size150));
            animator.startAnimation(true);
        } else {
            PlayButton.setVisibility(View.GONE);
            Live_history.setVisibility(View.GONE);
            parking_marker.setVisibility(View.GONE);
            speed.setVisibility(View.GONE);
            navigation.setVisibility(View.GONE);
            slidingUpPanelLayout.setPanelHeight(0);
            Toast.makeText(getApplicationContext(), "Please select other options to view history ", Toast.LENGTH_SHORT).show();
            opptionPopUp();
        }
    }


    public class Animator implements Runnable {
        private final Interpolator interpolator = new LinearInterpolator();
        float tilt = 90;
        float zoom = 15.5f;
        boolean upward = true;

        long start = SystemClock.uptimeMillis();

        GeoPoint endLatLng = null;
        GeoPoint beginLatLng = null;

        boolean showPolyline = false;

        @Override
        public void run() {
            Log.d(Tagg,"run "+currentIndex);
            long elapsed = SystemClock.uptimeMillis() - start;
            double t = interpolator.getInterpolation((float) elapsed / ANIMATE_SPEEED);

            double lat = t * endLatLng.getLatitude() + (1 - t) * beginLatLng.getLatitude();
            double lng = t * endLatLng.getLongitude() + (1 - t) * beginLatLng.getLongitude();

            GeoPoint newPos = new GeoPoint(lat, lng);
            animateMarker.setPosition(newPos);
            mapController.setCenter(newPos);

            if (showPolyline) {
                updatePolyLine(lat, lng);
            }
            if (t < 1) {
                mHandler.postDelayed(this, 16);
            }
            else {
                    if (currentIndex < geoPoints.size() - 2) {

                        currentIndex++;
                        endLatLng = getEndLatLng();
                        beginLatLng = getBeginLatLng();
                        start = SystemClock.uptimeMillis();

                        GeoPoint begin = getBeginLatLng();
                        GeoPoint end = getEndLatLng();

                        highLightMarker(currentIndex);
                        start = SystemClock.uptimeMillis();
                        mHandler.postDelayed(animator, 16);

                    }
                    else {

                        currentIndex++;
                        highLightMarker(currentIndex);
                        stopAnimation();
                    }
                }
        }
        private void highLightMarker(int index) {

            highLightMarker();

        }

        private void highLightMarker() {
            Marker stableMarker = new Marker(mapview);
//            if (geoPoints.size() - 1 == currentIndex) {
                Log.d(Tagg,"set toolbar value 1");
                ParsingClass parsingClass = new ParsingClass();
                parsingClass.historyParse(mHistoryResult);

//                Txt_change_distance.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance()));
//                Txt_change_time.setText((parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));
//            }
//            else {
                Log.d(Tagg,"set toolbar value 2");
            if(distancelist.get(currentIndex)!=null ){

                Txt_change_distance.setText(distancelist.get(currentIndex)+" Kms");
                Distance_cardview.setVisibility(View.VISIBLE);
            }
            if (datetimelist.get(currentIndex)!=null){
                Log.d("date value","1 "+datetimelist.get(currentIndex));
                value_speed.setVisibility(View.VISIBLE);
                Txt_change_time.setText(cons.getTimefromserver(datetimelist.get(currentIndex)));
                Log.d("date value","2 "+cons.getTimefromserver(datetimelist.get(currentIndex)));

            }
//                Txt_change_distance.setText(distancelist.get(currentIndex)+" Kms");
//               Txt_change_time.setText(cons.getTimefromserver(datetimelist.get(currentIndex)));
//            }
//            Txt_change_speed.setText(speedlist.get(currentIndex)+" Km/hr");
            if(speedlist.get(currentIndex)!=null){
                Speed_cardview.setVisibility(View.VISIBLE);
                Txt_change_speed.setText(speedlist.get(currentIndex)+" Km/hr");
                Log.d("Speed value ",Txt_change_speed.getText().toString());
            }

            if (mPosList.get(currentIndex) != null && !mPosList.get(currentIndex).equalsIgnoreCase("M")){
                String mAddsData = null, mMarkerWindowText = null;
                if (mAddsList.get(currentIndex) != null) {
                    mAddsData = mAddsList.get(currentIndex);
                }

                if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                    mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex);
                } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                    mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex);

                } else {
                    mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex));
                }

                if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                    if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                        mMarkerWindowText = mMarkerWindowText + " \n Over Speed";
                    }
                }

                if (currentIndex == geoPoints.size() - 1){
                    stableMarker.setPosition(geoPoints.get(currentIndex));
                    stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    mImgMarkerIcon.setImageResource(R.drawable.stop);
                    stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this, markerView));
                    stableMarker.setSubDescription(mMarkerWindowText);
                    stableMarker.showInfoWindow();
                }
                else{
                    if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {

                        stableMarker.setPosition(geoPoints.get(currentIndex));
                        stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                        mImgMarkerIcon.setImageResource(R.drawable.icon_parking);
                        stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this, markerView));
                        stableMarker.setTitle("Parking");
                        stableMarker.setSubDescription(mMarkerWindowText);
                        stableMarker.showInfoWindow();
                    }
                    else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {

                        stableMarker.setPosition(geoPoints.get(currentIndex));
                        stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                        mImgMarkerIcon.setImageResource(R.drawable.icon_idle);
                        stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this, markerView));
                        stableMarker.setTitle("Idle");
                        stableMarker.setSubDescription(mMarkerWindowText);
                        stableMarker.showInfoWindow();
                    }
                    else {
                        if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                            if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                                mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
                                stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this, markerView));
                            }
                        }
                    }
                }
                mapview.getOverlays().add(stableMarker);
                mapview.invalidate();
            }
            else {

                if (currentIndex == geoPoints.size() - 1) {


                    String mAddsData = null, mMarkerWindowText = null;
                    if (mAddsList.get(currentIndex) != null) {
                        mAddsData = mAddsList.get(currentIndex);
                    }

                    if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {

                        mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex);
                    } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                        mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex);

                    } else {
                        mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex));
                    }


                    if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                        if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                            mMarkerWindowText = mMarkerWindowText + " \n Over Speed";
                        }
                    }
                    System.out.println("The current index is :::::" + currentIndex + " HIIIIIII 1111111");

                    stableMarker.setPosition(geoPoints.get(currentIndex));
                    stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    mImgMarkerIcon.setImageResource(R.drawable.stop);
                    stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this, markerView));
                    stableMarker.setSubDescription(mMarkerWindowText);
                    stableMarker.showInfoWindow();
                    mapview.getOverlays().add(stableMarker);
                    mapview.invalidate();

                } else {
                    if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                        if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {

                            String mAddsData = null, mMarkerWindowText = null;
                            if (mAddsList.get(currentIndex) != null) {
                                mAddsData = mAddsList.get(currentIndex);
                            }

                            if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {

                                mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex);
                            } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                                mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex);

                            } else {
                                mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex));
                            }


                            if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                                if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                                    mMarkerWindowText = mMarkerWindowText + " \n Over Speed";
                                }
                            }
                            System.out.println("The current index is :::::" + currentIndex + " HIIIIIII 1111111");

                            stableMarker.setPosition(geoPoints.get(currentIndex));
                            stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                            mImgMarkerIcon.setImageResource(R.drawable.over_speed_new_icon);
                            stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this, markerView));
                            stableMarker.setSubDescription(mMarkerWindowText);
                            stableMarker.showInfoWindow();
                            mapview.getOverlays().add(stableMarker);
                            mapview.invalidate();


                        }
                    }
                }
            }
            stableMarker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker, MapView mapView) {

                    Toast.makeText(getApplicationContext(), "Marker clicked ", Toast.LENGTH_SHORT).show();

                    return false;
                }
            });
            if (currentIndex == geoPoints.size() - 1)

            {
                currentIndex = 0;
            }

        }

        public void reset(){
            start = SystemClock.uptimeMillis();
            if (currentIndex == geoPoints.size()) {
                currentIndex = 0;
            } else {

            }
            System.out.println("the current index in reset method is ::::" + currentIndex);
            endLatLng = getEndLatLng();
            beginLatLng = getBeginLatLng();

        }

        public void startAnimation(boolean showPolyLine) {
            if(geoPoints.size()>2){
                Log.d(Tagg,"startAnnimation "+geoPoints.size());
                animator.initialize(showPolyLine);
            }
            else{
                Log.d(Tagg,"NoAnnimation "+geoPoints.size());
                Toast.makeText(HistoryNewActivity.this,"No Data availabel",Toast.LENGTH_LONG).show();
            }
        }

        private void initialize(boolean showPolyLine) {
            Log.d(Tagg,"initialize polyline "+geoPoints.size());
            reset();
            this.showPolyline = showPolyLine;
            Marker stableMarker = new Marker(mapview);

            if (showPolyLine) {
                updatePolyLine(geoPoints.get(currentIndex).getLatitude(), geoPoints.get(currentIndex).getLongitude());
            }

            GeoPoint markerPos = geoPoints.get(currentIndex);
            GeoPoint secondPos = geoPoints.get(currentIndex + 1);

            if (mPosList.get(currentIndex) != null && !mPosList.get(currentIndex).equalsIgnoreCase("M")) {
                Log.d(Tagg,"initialize polyline Moving");
                String mAddsData = null, mMarkerWindowText = null;
                if (mAddsList.get(currentIndex) != null) {
                    mAddsData = mAddsList.get(currentIndex);
                }
                if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {

                    mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex);
                } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                    mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex);

                } else {
                    mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex));
                }


                if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                    if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                        mMarkerWindowText = mMarkerWindowText + " \n Over Speed";
                    }
                }
                if (currentIndex == 0) {
                    mImgMarkerIcon.setImageResource(R.drawable.start);
                    stableMarker.setPosition(geoPoints.get(currentIndex));
                    stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this,markerView));
                    stableMarker.setTitle("Start");
                    stableMarker.setSubDescription(mMarkerWindowText);
                    stableMarker.showInfoWindow();
                }
                else {

                    if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                        stableMarker.setPosition(geoPoints.get(currentIndex));
                        stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                        mImgMarkerIcon.setImageResource(R.drawable.icon_parking);
                        stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this, markerView));
                        stableMarker.setTitle("Parking");
                        stableMarker.setSubDescription(mMarkerWindowText);
                        stableMarker.showInfoWindow();

                    } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                        stableMarker.setPosition(geoPoints.get(currentIndex));
                        stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                        mImgMarkerIcon.setImageResource(R.drawable.icon_idle);
                        stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this, markerView));
                        stableMarker.setTitle("Idle");
                        stableMarker.setSubDescription(mMarkerWindowText);
                        stableMarker.showInfoWindow();

                    }
                }
                mapview.getOverlays().add(stableMarker);
                mapview.invalidate();
            }

            else {
                Log.d(Tagg,"initialize polyline not Moving");

                if (currentIndex == 0) {


                    String mAddsData = null, mMarkerWindowText = null;
                    if (mAddsList.get(currentIndex) != null) {
                        mAddsData = mAddsList.get(currentIndex);
                    }

                    if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                        mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex);
                    } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                        mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex);
                    } else {
                        mMarkerWindowText = mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex));
                    }


                    if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                        if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                            mMarkerWindowText = mMarkerWindowText + " \n Over Speed";
                        }
                    }

                    stableMarker.setPosition(geoPoints.get(currentIndex));
                    stableMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    mImgMarkerIcon.setImageResource(R.drawable.start);
                    stableMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this, markerView));
                    stableMarker.setTitle("Start");
                    stableMarker.setSubDescription(mMarkerWindowText);
                    stableMarker.showInfoWindow();
                    mapview.getOverlays().add(stableMarker);
                    mapview.invalidate();

                }

            }

            setupCameraPositionForMovement(markerPos, secondPos);

        }

        private void setupCameraPositionForMovement(GeoPoint markerPos, GeoPoint secondPos) {
            Log.d(Tagg,"setUpCameraPosition");
            animateMarker.setPosition(markerPos);
            animateMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            mapview.getOverlays().add(animateMarker);
            mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
            animateMarker.setIcon(createDrawableFromViewNew(HistoryNewActivity.this, markerView));
            mapview.invalidate();
//            mapController.setCenter(markerPos);

            try {
                animator.reset();
                Handler handler = new Handler();
                handler.post(animator);
            } catch (Exception e) {
                e.printStackTrace();
            }



        }
        private void updatePolyLine(double lat,double lng){
            Log.d(Tagg,"update polyline");
            GeoPoint newPos = new GeoPoint(lat, lng);
            polylinePoints.add(newPos);
            Polyline myPath = new Polyline();
            myPath.setColor(getResources().getColor(R.color.history_polyline_color));
            myPath.setPoints(polylinePoints);
//            mapController.setZoom(16.0);
            mapview.getOverlays().add(myPath);
            mapview.invalidate();
        }

        public void stopAnimation() {
            Log.d(Tagg,"stopAnnimation "+geoPoints.size());
            isStopCalled = false;
            animator.stop();

        }
        public void stop() {

            if (animateMarker != null) {
                animateMarker.remove(mapview);
            }
            mHandler.removeCallbacks(animator);

        }

        private GeoPoint getEndLatLng() {
            return geoPoints.get(currentIndex + 1);
        }
        private GeoPoint getBeginLatLng() {
            return geoPoints.get(currentIndex);
        }


    }



    private Location convertLatLngToLocation(GeoPoint latLng) {
        Location location = new Location("someLoc");
        location.setLatitude(latLng.getLatitude());
        location.setLongitude(latLng.getLongitude());
        return location;
    }

    private float bearingBetweenLatLngs(GeoPoint beginLatLng, GeoPoint endLatLng) {
        Location beginLocation = convertLatLngToLocation(beginLatLng);
        Location endLocation = convertLatLngToLocation(endLatLng);
        return beginLocation.bearingTo(endLocation);
    }


    public static Drawable createDrawableFromViewNew(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        System.out.println("Hi view width " + displayMetrics.widthPixels + " " + view.getMeasuredWidth() + " height " + displayMetrics.heightPixels + " " + view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return new BitmapDrawable(bitmap);
    }

    private void opptionPopUp(){

        dialog = new Dialog(HistoryNewActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.history_custom_dialog);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        Txt_fromDate=(TextView)dialog.findViewById(R.id.select_from_date);
        Txt_toDate=(TextView)dialog.findViewById(R.id.select_to_date);
        Txt_fromTime=(TextView)dialog.findViewById(R.id.Select_from_time);
        Txt_toTime=(TextView)dialog.findViewById(R.id.select_to_time);
        final LinearLayout view_history,custom_layout,radio_layout;
        view_history=(LinearLayout)dialog.findViewById(R.id.view_history) ;
        custom_layout=(LinearLayout)dialog.findViewById(R.id.custom_layout) ;
        radio_layout=(LinearLayout)dialog.findViewById(R.id.radio_button_layout) ;
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.radio_grp);

        rg_history.clearCheck();
        rg_history.setVisibility(View.VISIBLE);
        RadioButton $Custom = (RadioButton) dialog
                .findViewById(R.id.radio_custom);
        RadioButton $Today = (RadioButton) dialog
                .findViewById(R.id.radio_Today);
        RadioButton $Yesterday = (RadioButton) dialog
                .findViewById(R.id.radio_yesterday);
        RadioButton $DayBeforeYesterday = (RadioButton) dialog
                .findViewById(R.id.radio_dayBeforesterday);
        RadioButton $LastOneHour = (RadioButton) dialog
                .findViewById(R.id.radio_lastonehour);
        $Custom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setFromDate();
                setToDate();
                radio_value="Custom";
                radio_layout.setVisibility(View.GONE);
                custom_layout.setVisibility(View.VISIBLE);
                Txt_fromDate.setText(CustomfromDate);
                Txt_fromTime.setText(CustomfromTime);
                Txt_toDate.setText(CustomtoDate);
                Txt_toTime.setText(CustomtoTime);

            }
        });
        $Today.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                radio_value=  "Today"  ;
                String today_date = cons.getDateandTime("today");
                fromDate=today_date;
                fromTime="00:00:00";
                toDate=today_date;
                toTime=cons.getCurrentTime();


            }
        });
        $Yesterday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                radio_value=  "Yesterday";
                String yesterday_date = cons.getDateandTime("yesterday");
                fromDate=yesterday_date;
                fromTime="00:00:00";
                toDate=yesterday_date;
                toTime="23:59:00";

            }
        });

        $DayBeforeYesterday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                radio_value=  "Day Before Yesterday";
                String daybeforeyesterday_date = cons.getDateandTime("daybeforeyesterday");
                fromDate=daybeforeyesterday_date;
                fromTime="00:00:00";
                toDate=daybeforeyesterday_date;
                toTime="23:59:00";

            }
        });
        $LastOneHour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                radio_value= "Last One Hour";
                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int min = cal.get(Calendar.MINUTE);
                int past_hour = cal.get(Calendar.HOUR_OF_DAY) - 1;
                String lastonehour = cons.getDateandTime("today");
                fromDate=lastonehour;
                toDate=lastonehour;
                fromTime=(new StringBuilder().append(pad(past_hour)).append(":").append(pad(min)).append(":").append("00")).toString();
                toTime=(new StringBuilder().append(pad(hour)).append(":").append(pad(min)).append(":").append("00")).toString();
            }
        });
        view_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(radio_value!=null){
                    Log.d("checkHistory","from "+fromDate+" "+fromTime+" to "+toDate+" "+toTime);
                    dialog.cancel();
                    viewMap();
                }else{
                    history_alert=new Dialog(HistoryNewActivity.this);
                    history_alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    history_alert.setContentView(R.layout.history_alert_msg);
                    history_alert.setCanceledOnTouchOutside(false);
                    mTxt_msg2=(TextView)history_alert.findViewById(R.id.Select_msg2);
                    mTxt_msg1=(TextView)history_alert.findViewById(R.id.Select_msg);
                    ok_button=(TextView)history_alert.findViewById(R.id.ok_button);
                    mTxt_msg2.setVisibility(View.GONE);
                    mTxt_msg1.setText("Select any one option !!");
                    ok_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            history_alert.cancel();
//                                startActivity(new Intent(HistoryNewGoogleMapActivity.this,MapMenuActivity.class));
//                                finish();
                        }
                    });
                    history_alert.show();
                }
            }
        });


        Txt_fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromDate=true;
                showDateDialog();

            }
        });
        Txt_fromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromTime=true;
                showTimeDialog();
            }
        });
        Txt_toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromDate=false;
                showDateDialog();
            }
        });
        Txt_toTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromTime=false;
                showTimeDialog();
            }
        });

    }

    private void setFromDate(){
        CustomfromDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        fromDate=Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        mFromHourValue=00;
        mFromMinuteValue=00;
        CustomfromTime="00:00:00";
        fromTime="00:00:00";


    }
    private void setToDate(){
        CustomtoDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        toDate=Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        final Calendar c = Calendar.getInstance();
        // Current Hour
        hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        minute = c.get(Calendar.MINUTE);
        isFromTime = false;
        SimpleDateFormat hourFormat = new SimpleDateFormat(
                "HH");
        SimpleDateFormat minuteFormat = new SimpleDateFormat(
                "mm");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        CustomtoTime = "23:59:00";
        toTime="23:59:00";
        mFromHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
        mFromMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));

    }
    private void showTimeDialog() {
        if (isFromTime) {
            hour = mFromHourValue;
            minute = mFromMinuteValue;
        } else {
            hour = mToHourValue;
            minute = mToMinuteValue;
        }

        TimePickerDialog dialog2 = new TimePickerDialog(this, timePickerListeners, hour, minute,
                false);
        dialog2.show();
    }
    private void showDateDialog() {
        if (isFromDate) {
            String[] dateArray = CustomfromDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        } else {
            String[] dateArray = CustomtoDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        }
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
        dialog.show();
    }


    private TimePickerDialog.OnTimeSetListener timePickerListeners = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;
            Log.d("checkhistory","hour "+hour+" minute "+minute);
            if (isFromTime) {
                mFromHourValue = hour;
                mFromMinuteValue = minute;
                fromTime=new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00").toString();
                Txt_fromTime.setText(fromTime);
            }
            else  {
                mToHourValue = hour;
                mToMinuteValue = minute;
                toTime=new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00").toString();
                Txt_toTime.setText(toTime);
            }

        }
    };


    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day));
            if (isFromDate) {
                fromDate = String.valueOf(mDate2);
                Txt_fromDate.setText(fromDate);
            } else {
                toDate = String.valueOf(mDate2);
                Txt_toDate.setText(toDate);
            }

        }
    };
    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }


    private  void  ShowProgress(){
        progressDialog1 = new ProgressDialog(HistoryNewActivity.this,
                AlertDialog.THEME_HOLO_LIGHT);
        progressDialog1.setMessage("Please Wait...");
        progressDialog1.setProgressDrawable(new ColorDrawable(
                Color.BLUE));
        progressDialog1.setCancelable(true);
        progressDialog1.setCanceledOnTouchOutside(false);
        progressDialog1.show();
    }
    private  void HideProgress(){
        Log.d(Tagg,"hideprogress");
        if (progressDialog1.isShowing()) {
            progressDialog1.dismiss();
        }
    }

    private void init() {
        vehicleId=(TextView)findViewById(R.id.id_show_history_vehicleid);
        vehicleId.setText(Constant.SELECTED_VEHICLE_ID);
        googleMapLayout=(LinearLayout)findViewById(R.id.google_map_layout);
        googleMapLayout.setVisibility(View.GONE);
        rl=(RelativeLayout)findViewById(R.id.map_view_relativelayout);
        rl.setVisibility(View.VISIBLE);
        mapview = new MapView(this);
        mapview.setTilesScaledToDpi(true);
        rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                RelativeLayout.LayoutParams.FILL_PARENT));
        mapview.setBuiltInZoomControls(false);
        mapview.setMultiTouchControls(true);

        mapview.setBuiltInZoomControls(false);
        mapview.setMultiTouchControls(true);
        mapController = mapview.getController();
        mapController.setZoom(9);

        GeoPoint newPos = new GeoPoint(22.4209204,79.7445752);
        mapController.setCenter(newPos);

        mapview.getOverlays().clear();
        mapview.invalidate();
        mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));

        animateMarker = new Marker(mapview);

        animateMarker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker, MapView mapView) {
                return true;
            }
        });

        markerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_image_view, null);
        mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);

        slidingUpPanelLayout=(SlidingUpPanelLayout)findViewById(R.id.sliding_layout);
        relativeLayoutBottomSlideup=(RelativeLayout) findViewById(R.id.relativeLayoutBottomSlideup);
        linearlayoutBottomSlideup=(LinearLayout)findViewById(R.id.LinearlayoutBottomSlideup);
        up_arrow=(ImageView)findViewById(R.id.up_arrow);
        down_arrow=(ImageView)findViewById(R.id.down_arrow);
        value_speed=(CardView)findViewById(R.id.Value_cardview);
        Speed_cardview=(CardView)findViewById(R.id.Speed_cardview);
        Distance_cardview=(CardView)findViewById(R.id.distance_cardview) ;
        Txt_start_location=(TextView)findViewById(R.id.history_start_location_value);
        Txt_end_location=(TextView)findViewById(R.id.history_end_location_value);
        Txt_no_parking=(TextView)findViewById(R.id.history_parked_count_value);
        Txt_No_overspeed=(TextView)findViewById(R.id.history_over_speed_count_value);
        Txt_no_distance_coverd=(TextView)findViewById(R.id.history_distance_covered_value);
        Txt_timeTaken=(TextView)findViewById(R.id.history_timetaken_value);
        Txt_end_time=(TextView)findViewById(R.id.history_endtime_value);
        Txt_start_time=(TextView)findViewById(R.id.histroy_start_time_value);
        Txt_parked_time=(TextView)findViewById(R.id.history_parked_value);
        Txt_Moving_time=(TextView)findViewById(R.id.history_Moving_value);
        Txt_Idel_time=(TextView)findViewById(R.id.history_idel_value);
        Txt_Nodata_time=(TextView)findViewById(R.id.history_Nodata_value);
        Txt_Speed_limit=(TextView)findViewById(R.id.history_speed_value);
        Txt_odo_distance=(TextView)findViewById(R.id.history_odo_value);
        Txt_change_distance=(TextView)findViewById(R.id.txt_distance_value) ;
        Txt_change_speed=(TextView)findViewById(R.id.Txt_speed_value) ;
        Txt_change_time=(TextView)findViewById(R.id.Txt_date_time_value) ;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        speed=(Button)findViewById(R.id.Btn_speed);
        PlayButton=(CircleButton)findViewById(R.id.play_buttons);
        PlayButton.setImageResource(R.drawable.play_outline);
        Live_history =(CircleButton)findViewById(R.id.live_history);
        parking_marker=(CircleButton)findViewById(R.id.parking_marker);
        changeView=(CircleButton)findViewById(R.id.change_view);
        back_arrow=(ImageView)findViewById(R.id.back_arrow);
        navigation=(CircleButton)findViewById(R.id.nav_buttons);
        back_arrow.setOnClickListener(this);
        PlayButton.setOnClickListener(this);
        Live_history.setOnClickListener(this);
        parking_marker.setOnClickListener(this);
        navigation.setOnClickListener(this);
        changeView.setVisibility(View.GONE);
        speed.setVisibility(View.GONE);
        PlayButton.setVisibility(View.GONE);
        Live_history.setVisibility(View.GONE);
        parking_marker.setVisibility(View.GONE);
        navigation.setVisibility(View.GONE);
    }

    private void setUpSlidePanel() {
        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }
            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState == SlidingUpPanelLayout.PanelState.DRAGGING) {

                    if (previousState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                        Log.d(Tagg,"Expanded 1");
                        relativeLayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_crop_background));
                        linearlayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_crop_background));
                        up_arrow.setVisibility(View.VISIBLE);
                        down_arrow.setVisibility(View.GONE);
                    }
                } else if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    Log.d(Tagg,"Expanded 2");

                    relativeLayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_no_crop_background));
                    linearlayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_no_crop_background));
                    up_arrow.setVisibility(View.GONE);
                    down_arrow.setVisibility(View.VISIBLE);


                } else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    Log.d(Tagg,"collapsed");

                    relativeLayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_crop_background));
                    linearlayoutBottomSlideup.setBackground(getResources().getDrawable(R.drawable.drawable_swipeup_crop_background));
                    up_arrow.setVisibility(View.VISIBLE);
                    down_arrow.setVisibility(View.GONE);
                }
            }
        });
    }
    private void dbsetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(HistoryNewActivity.this,MapMenuActivity.class));
        finish();

    }


}
