package com.vamosys.utils;


import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class GoogleAddressConverter {

//    private final static Log log = LogFactory.getLog(GoogleAddressConverter.class);
//    public static final String Null = "null";
//
//    public static void main(String arg[]) throws Exception {
////		System.out.println("1");
//        HttpClient hc = new HttpClient();
//        GetMethod gm = new GetMethod("https://maps.google.com/maps/api/geocode/json?latlng=" + 22.562342 + "," + 88.319783);
//        int status = hc.executeMethod(gm);
//        InputStream is = gm.getResponseBodyAsStream();
//        String jsonAddr = getStringFromInputStream(is);
//        String faddr1 = new GoogleAddressConverter().getAddress(jsonAddr);
//        String faddr2 = new GoogleAddressConverter().getAddressWithPremises(jsonAddr);
////		System.out.println("i am here"+"\n"+faddr1);
//        if (faddr1.contains("sta:")) {
////			System.out.println("2");
//            faddr1 = new GoogleAddressConverter().convertAddress(faddr1);
//            faddr2 = new GoogleAddressConverter().convertAddress(faddr2);
//        }
//        System.out.println("faddr1  " + faddr1);
//        System.out.println("faddr2  " + faddr2);
//    }

    private String sta;
    private String rot;

    private String loc;
    private String ad1;
    private String ad2;
    private String con;
    private String pin;

    public String getSta() {
        return sta;
    }

    public void setSta(String sta) {
        this.sta = sta.trim();
    }

    public String getAddress(JSONObject jsonAddr) {
        try {
            GoogleGeoCodeResponse gprep = new Gson().fromJson(jsonAddr.toString(), GoogleGeoCodeResponse.class);
            results[] r = gprep.results;
            if (r == null || r.length <= 0)
                return null;
            address_component[] adr = r[0].address_components;
            for (address_component results : adr) {
                String[] a = results.types;
                ArrayList<String> v = new ArrayList<String>(Arrays.asList(a));
                if (v.contains(GoogleGeoCodeResponse.type1)) {
                    setSta(results.short_name);
                } else if (v.contains(GoogleGeoCodeResponse.type2)) {
                    setRot(results.short_name);
                } else if (v.contains(GoogleGeoCodeResponse.type3)) {
                    setLoc(results.short_name);

                } else if (v.contains(GoogleGeoCodeResponse.type4)) {
                    setAd1(results.long_name);
                } else if (v.contains(GoogleGeoCodeResponse.type5)) {
                    setAd2(results.long_name);
                } else if (v.contains(GoogleGeoCodeResponse.type6)) {
                    setCon(results.long_name);
                } else if (v.contains(GoogleGeoCodeResponse.type7)) {
                    setPin(results.short_name);
                }
            }
            return toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public String getRot() {
        return rot;
    }

    public void setRot(String rot) {
        this.rot = rot;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getAd1() {
        return ad1;
    }

    public void setAd1(String ad1) {
        this.ad1 = ad1;
    }

    public String getAd2() {
        return ad2;
    }

    public void setAd2(String ad2) {
        this.ad2 = ad2;
    }

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    @Override
    public String toString() {
        return "sta:" + sta + " rot:" + rot + " loc:" + loc + " ad1:" + ad1 + " ad2:" + ad2 + " con:" + con + " pin:"
                + pin;
    }

}
