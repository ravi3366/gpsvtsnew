package com.vamosys.utils;

public class GoogleGeoCodeResponse {

	public String status;
	public com.vamosys.utils.results[] results;

	public GoogleGeoCodeResponse() {

	}
	protected static final String type1   = "street_address";
	protected static final String type1_1 = "premise";
	protected static final String type1_2 = "street_number";
	protected static final String type2   = "route";
	protected static final String type3   = "locality";
	protected static final String type3_1 = "sublocality_level_1";
	protected static final String type4   = "administrative_area_level_2";
	protected static final String type5   = "administrative_area_level_1";
	protected static final String type6   = "country";
	protected static final String type7   = "postal_code";
}

class results {
	public String formatted_address;
	public com.vamosys.utils.geometry geometry;
	public String[] types;
	public address_component[] address_components;
}

class geometry {
	public com.vamosys.utils.bounds bounds;
	public String location_type;
	public com.vamosys.utils.location location;
	public com.vamosys.utils.bounds viewport;
}

class bounds {

	public location northeast;
	public location southwest;
}

class location {
	public String lat;
	public String lng;
}

class address_component {
	public String long_name;
	public String short_name;
	public String[] types;
}