package com.vamosys.utils;

import java.util.ArrayList;


public class TripSummaryMobile {

    //	private final static Log log = LogFactory.getLog(TripSummaryMobile.class);
    protected String vehicleId;
    protected String vehicleName;
    protected String error;
    private ArrayList<com.vamosys.model.TripSummaryMobile> trip = null;


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public TripSummaryMobile() {

    }

    public ArrayList<com.vamosys.model.TripSummaryMobile> getTrip() {
        return trip;
    }

    public void setTrip(ArrayList<com.vamosys.model.TripSummaryMobile> trip) {
        this.trip = trip;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public TripSummaryMobile(String vehicleId, long fromTime, long toTime, String vehicleName) {
        this.vehicleId = vehicleId;
        this.vehicleName = vehicleName;

    }

}