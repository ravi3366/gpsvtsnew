package com.vamosys.utils;


import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.vamosys.model.FuelFillDto;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.List;

public class MyMarkerFuelView extends MarkerView {

    private TextView tvContent;
    private ArrayList<Integer> mXAxisData;
    private String mAlertText;
    private List<FuelFillDto> mFuelFillData;

    public MyMarkerFuelView(Context context, int layoutResource, ArrayList<Integer> xAxisValue, String mAlertText, List<FuelFillDto> mFuelFillData) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
        mXAxisData = xAxisValue;
        this.mFuelFillData = mFuelFillData;
        this.mAlertText = mAlertText;
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

//        if (e instanceof CandleEntry) {
//
//            CandleEntry ce = (CandleEntry) e;
//
//        tvContent.setText(mXAxisData.get((int) e.getX()) + "\n " + mAlertText + " : " + Utils.formatNumber(e.getY(), 0, true));

        if (getMarkerText(Integer.valueOf(mXAxisData.get((int) e.getX()))) != null) {
            tvContent.setText(getMarkerText(Integer.valueOf(mXAxisData.get((int) e.getX()))));
        }

//        } else {


//            tvContent.setText("" + Utils.formatNumber(e.getY(), 0, true));
//        }

        super.refreshContent(e, highlight);
    }

    public String getMarkerText(int mXaxisValue) {
        String mReturnText = null;

        if (mFuelFillData.size() > mXaxisValue) {
            FuelFillDto f = mFuelFillData.get(mXaxisValue);
            if (f != null) {
                mReturnText = Const.getAcReportTime(f.getStartTime())+"\n";
                mReturnText += "Fuel :" + f.getFuelLtr() + " ltrs\n";
                mReturnText += "Speed : " + f.getNewSpeed() + " Kmph\n";
                mReturnText += "Ignition : " + f.getIgnitionStatus() + "\n";
                mReturnText += "Odo : " + f.getOdoMeterReading();
            }
        }


        return mReturnText;
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
