package com.vamosys.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class NotificationsPreferenceManager {

    private String TAG = NotificationsPreferenceManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "Notifications";
    private static final String KEY_NOTIFICATIONS = "notifications";

    // Constructor
    public NotificationsPreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void addNotification(String notification) {



        // get old notifications
        String oldNotifications = getNotifications();

        if (oldNotifications != null) {
            oldNotifications += "|" + notification;
        } else {
            oldNotifications = notification;
        }

        editor.putString(KEY_NOTIFICATIONS, oldNotifications);
        editor.commit();
    }

    public void updateNotificationsNull() {
        editor.putString(KEY_NOTIFICATIONS, "");
        editor.commit();
    }

    public String getNotifications() {
        return pref.getString(KEY_NOTIFICATIONS, null);
    }

//    public void clear() {
//        editor.clear();
//        editor.commit();
//    }
}
