package com.vamosys.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import androidx.core.graphics.drawable.DrawableCompat;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.vamosys.model.NotificationFilterDto;
import com.vamosys.model.PushNotificationDto;
import com.vamosys.model.VehicleData;
import com.vamosys.services.UploadAddressTask;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osmdroid.tileprovider.MapTileProviderBasic;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.views.overlay.TilesOverlay;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class Constant {

    public static int longposition;
    public static int ScreenWidth;
    public static int ScreenHeight;
    public static boolean Firstclick = false;
    public static List<PushNotificationDto> mPushDataList = new ArrayList<PushNotificationDto>();
    public static List<PushNotificationDto> mPushDataListAdapter = new ArrayList<PushNotificationDto>();

    public static List<NotificationFilterDto> mNotificationFilterList = new ArrayList<NotificationFilterDto>();

    public static String mDbUserId;

    public static List<String> mUserGroupList = new ArrayList<String>();

    public static List<String> mSupportDbList = new ArrayList<String>();

    public static long db_refresh_rate = 0, db_history_interval = 0;
    public static String FuelStoredFromDate = null, FuelStoredToDate = null, FuelStoredFromTime = null, FuelStoredToTime = null,StartTimeFuelReport=null,EndTimeFuelReport=null;
    public static String TFuelStoredFromDate = null, TFuelStoredToDate = null, TFuelStoredFromTime = null, TFuelStoredToTime = null,StartTimeReport=null,EndTimeReport=null;


    public static String mFuelStoredFromDate = null, mFuelStoredToDate = null, mFuelStoredFromTime = null, mFuelStoredToTime = null, mFuelInterval = null;
    public static boolean mFuelIsChartView = false;

    public static String SELECTED_VEHICLE_ID, SELECTED_USER_ID, SELECTED_USER_PWD, SELECTED_GROUP, SELECTED_VEHICLE_SHORT_NAME, SELECTED_ODO_DISTANCE, SELECTED_AC_STATUS;

    public static String SELECTED_TRIP_FROM_DATE, SELECTED_TRIP_TO_DATE, SELECTED_DRIVER_NAME, SELECTED_DRIVER_CONTACT_NUMBER, SELECTED_VEHICLE_TYPE, SELECTED_VEHICLE_ADDRESS;

    public static String SELECTED_MAIN_VEHICLE_LAT, SELECTED_MAIN_VEHICLE_LNG;

    public static String SELECTED_USER_LAT, SELECTED_USER_LNG;

    public static String SELECTED_OVER_SPEED="60";

    public static VehicleData SELECTED_VEHICLE_LOCATION_OBJECT = new VehicleData();

    public static List<VehicleData> SELECTED_VEHICLE_LOCATION_LIST_OBJECT = new ArrayList<VehicleData>();

    public static JSONObject SELECTED_VEHICLE_JSON_OBJECT;
    public static List<VehicleData> VEHILCE_LIST_DATA = new ArrayList<VehicleData>();

    public static int gcm_count = 1;

    public static boolean mIsFirstTime = false;

    public static boolean mUserLocationNearby = false, mUserLocationNearbyActivityCalled = false;
    ;

    public static String SELECTED_EMP_ID, SELECTED_EMP_PWD;

    public static boolean is_logged_in = false, SELECTED_EMP_NOTI_ENABLE_STATUS = true;

    public static ImageView vehicle_direction_in_map(String vehicle_type, String vehicle_direction, ImageView id_vehicle_in_marker) {
        Log.d("consvehicles", "" + vehicle_type);
        //Log.i("vehicle_direction", vehicle_direction);
        if (vehicle_type.equals("Bus")) {
            if (vehicle_direction.equals("N")) {
                id_vehicle_in_marker.setImageResource(R.drawable.bus_north);
            } else if (vehicle_direction.equals("S")) {
                id_vehicle_in_marker.setImageResource(R.drawable.bus_south);
            } else if (vehicle_direction.equals("E")) {
                id_vehicle_in_marker.setImageResource(R.drawable.bus_east);
            } else if (vehicle_direction.equals("W")) {
                id_vehicle_in_marker.setImageResource(R.drawable.bus_west);
            } else if (vehicle_direction.equals("NE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.bus_ne);
            } else if (vehicle_direction.equals("NW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.bus_nw);
            } else if (vehicle_direction.equals("SE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.bus_se);
            } else if (vehicle_direction.equals("SW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.bus_sw);
            } else if (vehicle_direction.equals("null")) {
                id_vehicle_in_marker.setImageResource(R.drawable.bus_east);
            }
        } else if (vehicle_type.equals("Car")) {
            if (vehicle_direction.equals("N")) {
                id_vehicle_in_marker.setImageResource(R.drawable.car_north);
            } else if (vehicle_direction.equals("S")) {
                id_vehicle_in_marker.setImageResource(R.drawable.car_south);
            } else if (vehicle_direction.equals("E")) {
                id_vehicle_in_marker.setImageResource(R.drawable.car_east);
            } else if (vehicle_direction.equals("W")) {
                id_vehicle_in_marker.setImageResource(R.drawable.car_west);
            } else if (vehicle_direction.equals("NE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.car_ne);
            } else if (vehicle_direction.equals("NW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.car_nw);
            } else if (vehicle_direction.equals("SE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.car_se);
            } else if (vehicle_direction.equals("SW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.car_sw);
            } else if (vehicle_direction.equals("null")) {
                id_vehicle_in_marker.setImageResource(R.drawable.car_east);
            }
        } else if (vehicle_type.equals("Cycle")) {
            if (vehicle_direction.equals("N")) {
                id_vehicle_in_marker.setImageResource(R.drawable.cycle_north);
            } else if (vehicle_direction.equals("S")) {
                id_vehicle_in_marker.setImageResource(R.drawable.cycle_south);
            } else if (vehicle_direction.equals("E")) {
                id_vehicle_in_marker.setImageResource(R.drawable.cycle_east);
            } else if (vehicle_direction.equals("W")) {
                id_vehicle_in_marker.setImageResource(R.drawable.cycle_west);
            } else if (vehicle_direction.equals("NE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.cycle_ne);
            } else if (vehicle_direction.equals("NW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.cycle_nw);
            } else if (vehicle_direction.equals("SE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.cycle_se);
            } else if (vehicle_direction.equals("SW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.cycle_sw);
            } else if (vehicle_direction.equals("null")) {
                id_vehicle_in_marker.setImageResource(R.drawable.cycle_east);
            }
        } else if (vehicle_type.equals("Truck")) {
            if (vehicle_direction.equals("N")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_north);
            } else if (vehicle_direction.equals("S")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_south);
            } else if (vehicle_direction.equals("E")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            } else if (vehicle_direction.equals("W")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_west);
            } else if (vehicle_direction.equals("NE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_ne);
            } else if (vehicle_direction.equals("NW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_nw);
            } else if (vehicle_direction.equals("SE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_se);
            } else if (vehicle_direction.equals("SW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_sw);
            } else if (vehicle_direction.equals("null")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            }
        } else if (vehicle_type.equalsIgnoreCase("Jcb")) {
            id_vehicle_in_marker.setImageResource(R.drawable.jcb_icon);
        } else if (vehicle_type.equalsIgnoreCase("Bike")) {
            id_vehicle_in_marker.setImageResource(R.drawable.vehicle_bike);
        } else if (vehicle_type.equals("Ambulance")) {
            if (vehicle_direction.equals("N")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("S")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("E")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("W")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("NE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("NW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("SE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("SW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("null")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            }
        } else if (vehicle_type.equals("HeavyVehicle")) {
            if (vehicle_direction.equals("N")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            } else if (vehicle_direction.equals("S")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            } else if (vehicle_direction.equals("E")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            } else if (vehicle_direction.equals("W")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            } else if (vehicle_direction.equals("NE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            } else if (vehicle_direction.equals("NW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            } else if (vehicle_direction.equals("SE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            } else if (vehicle_direction.equals("SW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            } else if (vehicle_direction.equals("null")) {
                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
            }
        } else if (vehicle_type.equals("Bike")) {
            if (vehicle_direction.equals("N")) {
                id_vehicle_in_marker.setImageResource(R.drawable.vehicle_bike);
            } else if (vehicle_direction.equals("S")) {
                id_vehicle_in_marker.setImageResource(R.drawable.vehicle_bike);
            } else if (vehicle_direction.equals("E")) {
                id_vehicle_in_marker.setImageResource(R.drawable.vehicle_bike);
            } else if (vehicle_direction.equals("W")) {
                id_vehicle_in_marker.setImageResource(R.drawable.vehicle_bike);
            } else if (vehicle_direction.equals("NE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.vehicle_bike);
            } else if (vehicle_direction.equals("NW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.vehicle_bike);
            } else if (vehicle_direction.equals("SE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.vehicle_bike);
            } else if (vehicle_direction.equals("SW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.vehicle_bike);
            } else if (vehicle_direction.equals("null")) {
                id_vehicle_in_marker.setImageResource(R.drawable.vehicle_bike);
            }
        }else if (vehicle_type.equals("Ambulance")) {
            if (vehicle_direction.equals("N")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("S")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("E")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("W")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("NE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("NW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("SE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("SW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            } else if (vehicle_direction.equals("null")) {
                id_vehicle_in_marker.setImageResource(R.drawable.ambulance);
            }
        }
        else if (vehicle_type.equals("Van")) {
            if (vehicle_direction.equals("N")) {
                id_vehicle_in_marker.setImageResource(R.drawable.van1);
            } else if (vehicle_direction.equals("S")) {
                id_vehicle_in_marker.setImageResource(R.drawable.van1);
            } else if (vehicle_direction.equals("E")) {
                id_vehicle_in_marker.setImageResource(R.drawable.van1);
            } else if (vehicle_direction.equals("W")) {
                id_vehicle_in_marker.setImageResource(R.drawable.van1);
            } else if (vehicle_direction.equals("NE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.van1);
            } else if (vehicle_direction.equals("NW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.van1);
            } else if (vehicle_direction.equals("SE")) {
                id_vehicle_in_marker.setImageResource(R.drawable.van1);
            } else if (vehicle_direction.equals("SW")) {
                id_vehicle_in_marker.setImageResource(R.drawable.van1);
            } else if (vehicle_direction.equals("null")) {
                id_vehicle_in_marker.setImageResource(R.drawable.van1);
            }
        }
        return id_vehicle_in_marker;
    }


    public static Drawable setVehicleImage(String vehicle_position, Context context, String vehicleType) {
//        Log.d("GPSVTS", "setVehicleImage() called with: " + "vehicle_position = [" + vehicle_position + "]");

//        Drawable drawable;
        Log.d("consvehtyp", "" + vehicleType);
        if (vehicle_position.equalsIgnoreCase("P")) {

            if (vehicleType.equals("Bus")) {
                Log.d("consvehtyp1", "" + vehicleType);
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.bus_east), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equals("Car")) {
                Log.d("consvehtyp12", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.car_east), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equals("Truck")) {
                Log.d("consvehtyp13", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equals("Cycle")) {
                Log.d("consvehtyp14", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.cycle_east), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equalsIgnoreCase("Jcb")) {
                Log.d("consvehtyp15", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.jcb_icon), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equalsIgnoreCase("Bike")) {
                Log.d("consvehtyp16", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.vehicle_bike), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equalsIgnoreCase("Ambulance")) {
                Log.d("consvehtyp17", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.ambulance), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equalsIgnoreCase("HeavyVehicle")) {
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.black));

            }else if (vehicleType.equalsIgnoreCase("Van")) {
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.van1), context.getResources().getColor(R.color.black));

            }

//            return setVehicleDrawable(drawable, context.getResources().getColor(R.color.black));
        } else if (vehicle_position.equalsIgnoreCase("M")) {

//            if (mIgnitionStatus != null && mIgnitionStatus.trim().equalsIgnoreCase("OFF")) {
//                return setVehicleDrawable(drawable, context.getResources().getColor(R.color.grey));
//            } else {
//            return setVehicleDrawable(drawable, context.getResources().getColor(R.color.green));



            if (vehicleType.equals("Bus")) {
                Log.d("consvehtyp1", "" + vehicleType);
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.bus_east), context.getResources().getColor(R.color.green));

            } else if (vehicleType.equals("Car")) {
                Log.d("consvehtyp12", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.car_east), context.getResources().getColor(R.color.green));

            } else if (vehicleType.equals("Truck")) {
                Log.d("consvehtyp13", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.green));

            } else if (vehicleType.equals("Cycle")) {
                Log.d("consvehtyp14", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.cycle_east), context.getResources().getColor(R.color.green));

            } else if (vehicleType.equalsIgnoreCase("Jcb")) {
                Log.d("consvehtyp15", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.jcb_icon), context.getResources().getColor(R.color.green));

            } else if (vehicleType.equalsIgnoreCase("Bike")) {
                Log.d("consvehtyp16", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.vehicle_bike), context.getResources().getColor(R.color.green));

            } else if (vehicleType.equalsIgnoreCase("Ambulance")) {
                Log.d("consvehtyp17", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.ambulance), context.getResources().getColor(R.color.green));

            } else if (vehicleType.equalsIgnoreCase("HeavyVehicle")) {
                Log.d("consvehtyp18", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.green));

            }else if (vehicleType.equalsIgnoreCase("Van")) {
                Log.d("consvehtyp19", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.van1),context.getResources().getColor(R.color.green));

            }


//            }
        } else if (vehicle_position.equalsIgnoreCase("U")) {
//            return setVehicleDrawable(drawable, context.getResources().getColor(R.color.red));


            if (vehicleType.equals("Bus")) {
                Log.d("consvehtyp1", "" + vehicleType);
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.bus_east), context.getResources().getColor(R.color.red));

            } else if (vehicleType.equals("Car")) {
                Log.d("consvehtyp12", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.car_east), context.getResources().getColor(R.color.red));

            } else if (vehicleType.equals("Truck")) {
                Log.d("consvehtyp13", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.red));

            } else if (vehicleType.equals("Cycle")) {
                Log.d("consvehtyp14", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.cycle_east), context.getResources().getColor(R.color.red));

            } else if (vehicleType.equalsIgnoreCase("Jcb")) {
                Log.d("consvehtyp15", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.jcb_icon), context.getResources().getColor(R.color.red));

            } else if (vehicleType.equalsIgnoreCase("Bike")) {
                Log.d("consvehtyp16", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.vehicle_bike), context.getResources().getColor(R.color.red));

            } else if (vehicleType.equalsIgnoreCase("Ambulance")) {
                Log.d("consvehtyp17", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.ambulance), context.getResources().getColor(R.color.red));

            } else if (vehicleType.equalsIgnoreCase("HeavyVehicle")) {
                Log.d("consvehtyp18", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.red));

            }else if (vehicleType.equalsIgnoreCase("Van")) {
                Log.d("consvehtyp19", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.van1),context.getResources().getColor(R.color.red));

            }

        } else if (vehicle_position.equalsIgnoreCase("S") ) {

//            return setVehicleDrawable(drawable, context.getResources().getColor(R.color.yellow));



            if (vehicleType.equals("Bus")) {
                Log.d("consvehtyp1", "" + vehicleType);
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.bus_east), context.getResources().getColor(R.color.yellow));

            } else if (vehicleType.equals("Car")) {
                Log.d("consvehtyp12", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.car_east), context.getResources().getColor(R.color.yellow));

            } else if (vehicleType.equals("Truck")) {
                Log.d("consvehtyp13", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.yellow));

            } else if (vehicleType.equals("Cycle")) {
                Log.d("consvehtyp14", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.cycle_east), context.getResources().getColor(R.color.yellow));

            } else if (vehicleType.equalsIgnoreCase("Jcb")) {
                Log.d("consvehtyp15", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.jcb_icon), context.getResources().getColor(R.color.yellow));

            } else if (vehicleType.equalsIgnoreCase("Bike")) {
                Log.d("consvehtyp16", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.vehicle_bike), context.getResources().getColor(R.color.yellow));

            } else if (vehicleType.equalsIgnoreCase("Ambulance")) {
                Log.d("consvehtyp17", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.ambulance), context.getResources().getColor(R.color.yellow));

            } else if (vehicleType.equalsIgnoreCase("HeavyVehicle")) {
                Log.d("consvehtyp18", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.yellow));

            }else if (vehicleType.equalsIgnoreCase("Van")) {
                Log.d("consvehtyp19", "" + vehicleType);
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.van1),context.getResources().getColor(R.color.yellow));
            }
        }
        else if( vehicle_position.equalsIgnoreCase("N")){
            if (vehicleType.equals("Bus")) {
                Log.d("consvehtyp1", "" + vehicleType);
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.bus_east), context.getResources().getColor(R.color.grey));

            } else if (vehicleType.equals("Car")) {
                Log.d("consvehtyp12", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.car_east), context.getResources().getColor(R.color.grey));

            } else if (vehicleType.equals("Truck")) {
                Log.d("consvehtyp13", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.grey));

            } else if (vehicleType.equals("Cycle")) {
                Log.d("consvehtyp14", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.cycle_east), context.getResources().getColor(R.color.grey));

            } else if (vehicleType.equalsIgnoreCase("Jcb")) {
                Log.d("consvehtyp15", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.jcb_icon), context.getResources().getColor(R.color.grey));

            } else if (vehicleType.equalsIgnoreCase("Bike")) {
                Log.d("consvehtyp16", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.vehicle_bike), context.getResources().getColor(R.color.grey));

            } else if (vehicleType.equalsIgnoreCase("Ambulance")) {
                Log.d("consvehtyp17", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.ambulance), context.getResources().getColor(R.color.grey));

            } else if (vehicleType.equalsIgnoreCase("HeavyVehicle")) {
                Log.d("consvehtyp18", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.grey));

            }else if (vehicleType.equalsIgnoreCase("Van")) {
                Log.d("consvehtyp19", "" + vehicleType);
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.van1),context.getResources().getColor(R.color.grey));
            }
        }
        else {

//            return setVehicleDrawable(drawable, context.getResources().getColor(R.color.grey));


            if (vehicleType.equals("Bus")) {
                Log.d("consvehtyp1", "" + vehicleType);
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.bus_east), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equals("Car")) {
                Log.d("consvehtyp12", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.car_east), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equals("Truck")) {
                Log.d("consvehtyp13", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equals("Cycle")) {
                Log.d("consvehtyp14", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.cycle_east), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equalsIgnoreCase("Jcb")) {
                Log.d("consvehtyp15", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.jcb_icon), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equalsIgnoreCase("Bike")) {
                Log.d("consvehtyp16", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.vehicle_bike), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equalsIgnoreCase("Ambulance")) {
                Log.d("consvehtyp17", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.ambulance), context.getResources().getColor(R.color.black));

            } else if (vehicleType.equalsIgnoreCase("HeavyVehicle")) {
                Log.d("consvehtyp18", "" + vehicleType);

                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.truck_east), context.getResources().getColor(R.color.black));

            }else if (vehicleType.equalsIgnoreCase("Van")) {
                Log.d("consvehtyp19", "" + vehicleType);
                return setVehicleDrawable(context.getResources().getDrawable(R.drawable.van1),context.getResources().getColor(R.color.black));

            }

        }
        return null;
    }

    private static Drawable setVehicleDrawable(Drawable d, int color) {
        Drawable wrappedDrawable = DrawableCompat.wrap(d);
        DrawableCompat.setTint(wrappedDrawable, color);
        return wrappedDrawable;
    }

//    public static void readFile(String mLocText) {
//        String sdcard = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GPSVTS/log/";
//
////Get the text file
//        File file = new File(sdcard, "gpsLocation.txt");
//
//
//        if (file.exists()) {
//
////Read text from file
//            StringBuilder text = new StringBuilder();
//
//            try {
//                BufferedReader br = new BufferedReader(new FileReader(file));
//                String line;
//
//                while ((line = br.readLine()) != null) {
//                    text.append(line);
//                    text.append('\n');
//                }
//                br.close();
//            } catch (IOException e) {
//                //You'll need to add proper error handling here
//                e.printStackTrace();
//            }
//
//            System.out.println("File exists and previous error log is :::" + text);
//
//            writeAFile(text + "\n" + mLocText);
//
//        } else {
//            writeAFile(mLocText);
//        }
//    }

    public static void writeAFile(String temp) {


        try {

            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                String sdpath = Environment.getExternalStorageDirectory().getPath();
                File dirpath = new File(sdpath, "/GPSVTS/log");
                if (!dirpath.exists()) {
                    dirpath.mkdirs();
                }
                String filename = dirpath.getAbsolutePath() + "/" + System.currentTimeMillis() + "_error_log.txt";
                File tempfile = new File(filename);
                if (tempfile.exists())
                    tempfile.delete();
                else
                    tempfile.createNewFile();

                temp = "###" + temp;


                FileOutputStream fOut = new FileOutputStream(tempfile);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.append(temp.toString());
                myOutWriter.close();
                fOut.close();


            }

        } catch (Exception e) {
        }

    }

    private static void updateAddressToServer(JSONObject obj, String latitude, String longitude, String appId) {
        GoogleAddressConverter g = new GoogleAddressConverter();
        g.getAddress(obj);


        String faddr = g.getAddress(obj);

        // faddr = faddr.replaceAll("[^\\x00-\\x7F]", "");

        if (faddr != null) {

            faddr = faddr.replaceAll("[^\\x00-\\x7F]", "");


            UploadAddressTask upload = new UploadAddressTask();

//            System.out.println("Hi before encoding the address data is " + faddr);

            try {
                faddr = URLEncoder.encode(faddr, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            String url = faddr + "&lattitude=" + latitude + "&longitude=" + longitude + "&status=mob" + "&appId=" + appId;

            upload.execute(url);
        }
    }

    public static void getAddressForServer(String latitude, String longitude) {
        try {

            JSONObject jsonObj = HttpConfig.getJSONfromURL("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + ","
                    + longitude + "&sensor=true");

            //updateAddressToServer(jsonObj, latitude, longitude);
        } catch (Exception e) {

        }
    }

    public static String getAddress(String latitude, String longitude, String appid,String apikey) {

//        System.out.println("Hi get address called " + latitude + longitude);

        String Address1 = "", Address2 = "", City = "", State = "", Country = "", County = "", PIN = "";
        Address1 = "";
        Address2 = "";
        City = "";
        State = "";
        Country = "";
        County = "";
        PIN = "";

        try {

//            JSONObject jsonObj = HttpConfig.getJSONfromURL("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + ","
//                    + longitude + "&key=" +apikey+ "&sensor=true");


            JSONObject jsonObj = HttpConfig.getJSONfromURL("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + ","
                    + longitude + "&key=" +apikey);


            updateAddressToServer(jsonObj, latitude, longitude, appid);

            System.out.println("Hi address is " + jsonObj);

            String Status = jsonObj.getString("status");
            if (Status.equalsIgnoreCase("OK")) {
                JSONArray Results = jsonObj.getJSONArray("results");
                JSONObject zero = Results.getJSONObject(0);
                JSONArray address_components = zero.getJSONArray("address_components");

                for (int i = 0; i < address_components.length(); i++) {
                    JSONObject zero2 = address_components.getJSONObject(i);
                    String long_name = zero2.getString("long_name");
                    JSONArray mtypes = zero2.getJSONArray("types");
                    String Type = mtypes.getString(0);

                    if (TextUtils.isEmpty(long_name) == false || !long_name.equals(null) || long_name.length() > 0 || long_name != "") {
                        if (Type.equalsIgnoreCase("street_number")) {
                            Address1 = long_name + " ";
                        } else if (Type.equalsIgnoreCase("route")) {
                            Address1 = Address1 + long_name;
                        } else if (Type.equalsIgnoreCase("sublocality")) {
                            Address2 = long_name;
                        } else if (Type.equalsIgnoreCase("locality")) {
                            // Address2 = Address2 + long_name + ", ";
                            City = long_name;
                        } else if (Type.equalsIgnoreCase("administrative_area_level_2")) {
                            County = long_name;
                        } else if (Type.equalsIgnoreCase("administrative_area_level_1")) {
                            State = long_name;
                        } else if (Type.equalsIgnoreCase("country")) {
                            Country = long_name;
                        } else if (Type.equalsIgnoreCase("postal_code")) {
                            PIN = long_name;
                        }
                    }

                    // JSONArray mtypes = zero2.getJSONArray("types");
                    // String Type = mtypes.getString(0);
                    // Log.e(Type,long_name);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        StringBuffer result = new StringBuffer();

        if (Address1 != null && !Address1.isEmpty()) {
            result.append(Address1 + ",");
        }

        if (Address2 != null && !Address2.isEmpty()) {
            result.append(Address2 + ",");
        }

        if (City != null && !City.isEmpty()) {
            result.append(City + ",");
        }

        if (County != null && !County.isEmpty()) {
            result.append(County + ",");
        }

        if (State != null && !State.isEmpty()) {
            result.append(State + ",");
        }

        if (Country != null && !Country.isEmpty()) {
            result.append(Country + ",");
        }

        if (PIN != null && !PIN.isEmpty()) {
            result.append(PIN);
        }

//        System.out.println("Hi returning address is " + result.toString());
        return result.toString();

    }

    public static Drawable createDrawableFromViewNew(Context context, View view) {


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        System.out.println("Hi view width " + displayMetrics.widthPixels + " " + view.getMeasuredWidth() + " height " + displayMetrics.heightPixels + " " + view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.createBitmap(250, 250, Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.crea(96, 96, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
//        return bitmap;


        return new BitmapDrawable(bitmap);
    }

    public static TilesOverlay getTilesOverlay(Context context) {

//        System.out.println("Hi tiles load called ");

        final MapTileProviderBasic tileProvider = new MapTileProviderBasic(context);
//		final ITileSource tileSource = new XYTileSource("FietsRegionaal",  3, 18, 256, ".png",
//				new String[] { "http://overlay.openstreetmap.nl/openfietskaart-rcn/" });
        final ITileSource tileSource = new XYTileSource("FietsRegionaal", 1, 18, 256, ".png",
                new String[]{Const.OSM_URL});

//        System.out.println("Hi osm url "+tileSource.get);
        tileProvider.setTileSource(tileSource);
        final TilesOverlay tilesOverlay = new TilesOverlay(tileProvider, context);
        tilesOverlay.setLoadingBackgroundColor(Color.TRANSPARENT);
//        System.out.println("Hi overlay tiles " + tilesOverlay);
        return tilesOverlay;
    }

//    private void stripUnderlines(TextView textView) {
//        Spannable s = new SpannableString(textView.getText());
//        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
//        for (URLSpan span: spans) {
//            int start = s.getSpanStart(span);
//            int end = s.getSpanEnd(span);
//            s.removeSpan(span);
//            span = new URLSpanNoUnderline(span.getURL());
//            s.setSpan(span, start, end, 0);
//        }
//        textView.setText(s);
//    }

}
