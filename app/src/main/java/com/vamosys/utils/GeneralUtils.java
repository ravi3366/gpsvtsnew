package com.vamosys.utils;

import android.text.TextUtils;

/**
 * Created by Smart Team.
 */
public class GeneralUtils {
    public static boolean isNumeric(String string) {
        return TextUtils.isDigitsOnly(string);
    }
}
