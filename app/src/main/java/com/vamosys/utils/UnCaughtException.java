package com.vamosys.utils;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Date;

public class UnCaughtException implements
        UncaughtExceptionHandler {

    private Context context;
//    private static Context context1;
//    SharedPreferences pref;
//    String mEmpCode = null;
//    Activity act;

    public UnCaughtException(Context ctx) {
        context = ctx;
//        context1 = ctx;
//        act = activity;


    }

    private StatFs getStatFs() {
        File path = Environment.getDataDirectory();
        return new StatFs(path.getPath());
    }

    private long getAvailableInternalMemorySize(StatFs stat) {
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return availableBlocks * blockSize;
    }

    private long getTotalInternalMemorySize(StatFs stat) {
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return totalBlocks * blockSize;
    }

//    private void addInformation(StringBuilder message) {
//        message.append("Locale: ").append(Locale.getDefault()).append('\n');
//        try {
//            PackageManager pm = context.getPackageManager();
//            PackageInfo pi;
//            pi = pm.getPackageInfo(context.getPackageName(), 0);
//            message.append("Version: ").append(pi.versionName).append('\n');
//            message.append("Package: ").append(pi.packageName).append('\n');
//        } catch (Exception e) {
//            Log.e("CustomExceptionHandler", "Error", e);
//            message.append("Could not get Version information for ").append(
//                    context.getPackageName());
//        }
//        message.append("Empcode : ").append(mEmpCode).append('\n');
//        message.append("Phone Model: ").append(android.os.Build.MODEL)
//                .append('\n');
//        message.append("Android Version: ")
//                .append(android.os.Build.VERSION.RELEASE).append('\n');
//        message.append("Board: ").append(android.os.Build.BOARD).append('\n');
//        message.append("Brand: ").append(android.os.Build.BRAND).append('\n');
//        message.append("Device: ").append(android.os.Build.DEVICE).append('\n');
//        message.append("Host: ").append(android.os.Build.HOST).append('\n');
//        message.append("ID: ").append(android.os.Build.ID).append('\n');
//        message.append("Model: ").append(android.os.Build.MODEL).append('\n');
//        message.append("Product: ").append(android.os.Build.PRODUCT)
//                .append('\n');
//        message.append("Type: ").append(android.os.Build.TYPE).append('\n');
//        StatFs stat = getStatFs();
//        message.append("Total Internal memory: ")
//                .append(getTotalInternalMemorySize(stat)).append('\n');
//        message.append("Available Internal memory: ")
//                .append(getAvailableInternalMemorySize(stat)).append('\n');
//    }

    public void uncaughtException(Thread t, Throwable e) {
        try {
            StringBuilder report = new StringBuilder();
            Date curDate = new Date();
            report.append("Error Report collected on : ")
                    .append(curDate.toString()).append('\n').append('\n');
            report.append("Informations :").append('\n');
//            addInformation(report);
            report.append('\n').append('\n');
            report.append("Stack:\n");
            final Writer result = new StringWriter();
            final PrintWriter printWriter = new PrintWriter(result);
            e.printStackTrace(printWriter);
            report.append(result.toString());
            printWriter.close();
            report.append('\n');
            report.append("**** End of current Report ***");
            Log.e(UnCaughtException.class.getName(),
                    "Error while sendErrorMail" + report);
            // sendErrorMail(report);

            System.exit(0);

            Constant.writeAFile(report.toString());
        } catch (Throwable ignore) {
            Log.e(UnCaughtException.class.getName(),
                    "Error while sending error e-mail", ignore);
        }
    }


//    public void readFile(String mLocText) {
//        String sdcard = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GPSVTS/log/";
//
////Get the text file
//        File file = new File(sdcard, "gpsLocation.txt");
//
//
//        if (file.exists()) {
//
////Read text from file
//            StringBuilder text = new StringBuilder();
//
//            try {
//                BufferedReader br = new BufferedReader(new FileReader(file));
//                String line;
//
//                while ((line = br.readLine()) != null) {
//                    text.append(line);
//                    text.append('\n');
//                }
//                br.close();
//            } catch (IOException e) {
//                //You'll need to add proper error handling here
//            }
//
//            writeAFile(text + "\n" + mLocText);
//
//        } else {
//            writeAFile(mLocText);
//        }
//    }

//    public void writeAFile(String temp) {
//
//
//        try {
//
//            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
//                String sdpath = Environment.getExternalStorageDirectory().getPath();
//                File dirpath = new File(sdpath, "/GPSVTS/log/");
//                if (!dirpath.exists()) {
//                    dirpath.mkdirs();
//                }
//                String filename = dirpath.getAbsolutePath() + "/" +System.currentTimeMillis()+"_error_log.txt";
//                File tempfile = new File(filename);
//                if (tempfile.exists())
//                    tempfile.delete();
//                else
//                    tempfile.createNewFile();
//
//                temp = "###" + temp;
//
//
//                FileOutputStream fOut = new FileOutputStream(tempfile);
//                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
//                myOutWriter.append(temp.toString());
//                myOutWriter.close();
//                fOut.close();
//
//
//            }
//
//        } catch (Exception e) {
//        }
//
//    }


}