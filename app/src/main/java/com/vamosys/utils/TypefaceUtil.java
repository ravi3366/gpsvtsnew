package com.vamosys.utils;

import android.content.Context;
import android.graphics.Typeface;

public class TypefaceUtil {
    /**
     * Using reflection to override default typeface
     * NOTICE: DO NOT FORGET TO SET TYPEFACE FOR APP THEME AS DEFAULT TYPEFACE WHICH WILL BE OVERRIDDEN
     *
     * @param context to work with assets
     */
    public static Typeface getMyFont(Context context) {
        Typeface customFontTypeface = null;
        try {
            customFontTypeface = Typeface.createFromAsset(context.getAssets(), "OpenSans-Semibold.ttf");
        } catch (Exception e) {
        }
        return customFontTypeface;
    }
    public static Typeface getTitleFont(Context context) {
        Typeface appnamefont = null;
        try {
            appnamefont = Typeface.createFromAsset(context.getAssets(), "OpenSans-Semibold.ttf");
        } catch (Exception e) {
        }
        return appnamefont;
    }

    public static Typeface getTextBold(Context context) {
        Typeface appnamefont = null;
        try {
            appnamefont = Typeface.createFromAsset(context.getAssets(), "OpenSans-Bold.ttf");
        } catch (Exception e) {
        }
        return appnamefont;
    }
}