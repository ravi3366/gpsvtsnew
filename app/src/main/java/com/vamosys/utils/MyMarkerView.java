package com.vamosys.utils;


import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.vamosys.vamos.R;

import java.util.List;

public class MyMarkerView extends MarkerView {

    private TextView tvContent;
    private List<String> mXAxisData;
    private String mAlertText;


    public MyMarkerView(Context context, int layoutResource, List<String> xAxisValue, String mAlertText) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
        mXAxisData = xAxisValue;

        this.mAlertText = mAlertText;
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

//        if (e instanceof CandleEntry) {
//
//            CandleEntry ce = (CandleEntry) e;
//
        tvContent.setText(mXAxisData.get((int) e.getX()) + "\n " + mAlertText + " : " + Utils.formatNumber(e.getY(), 0, true));
//        } else {

//            tvContent.setText("" + Utils.formatNumber(e.getY(), 0, true));
//        }

        super.refreshContent(e, highlight);
    }


    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
