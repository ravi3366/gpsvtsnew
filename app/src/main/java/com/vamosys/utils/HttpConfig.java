package com.vamosys.utils;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpConfig {

    public String httpGet(String url) throws Exception {

        //  Toast.makeText(NavigationActivity.this, "The url is :::"+url, Toast.LENGTH_SHORT).show();
        System.out.println("Url in http config :::" + url);

//        url = URLEncoder.encode(url, "utf-8");

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        con.setConnectTimeout(1 * 1000);
        //add request header
        int responseCode = con.getResponseCode();
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println("Res str is :::111" + response.toString());
        return response.toString();
    }


    public String httpDomainVerify(String url) throws Exception {

        System.out.println("Url in http config :::" + url);
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        con.setConnectTimeout(30 * 1000);
        //add request header
        int responseCode = con.getResponseCode();


        return String.valueOf(responseCode);
    }


    public static JSONObject getJSONfromURL(String url) {

//        System.out.println("Hi address fetch url is " + url);
        Log.d("url","url adress "+url);
        // initialize
        InputStream is = null;
        String result = "";
        JSONObject jObject = null;

        // http post
        try {

            URL obj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            // optional default is GET
//            URL url = new URL(url);

            // Open a HTTP  connection to  the URL
//            conn = (HttpsURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setConnectTimeout(30 * 1000);


//            HttpClient httpclient = new DefaultHttpClient();
//            HttpPost httppost = new HttpPost(url);
//            HttpResponse response = httpclient.execute(httppost);
//            HttpEntity entity = response.getEntity();


            is = conn.getInputStream();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        // convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        try {
            jObject = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }
//        System.out.println("Hi returning json address is " + jObject);
        return jObject;
    }


}
