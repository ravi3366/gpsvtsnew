package com.vamosys.utils;


import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class PiechartLabelFormatter implements IValueFormatter {

//    private final String[] mLabels = {"Parking", "Moving", "Idle", "No Data"};
    private DecimalFormat mFormat;
    ArrayList<String> entries;

    public PiechartLabelFormatter(ArrayList<String> entries) {
        mFormat = new DecimalFormat("###,###,##0");
        this.entries = entries;
    }

//    @Override
//    public String getFormattedValue(float value, AxisBase axis) {
//        return mLabels[((int) value)];
//    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
//        Log.i("Hi label 22", String.valueOf(entry) + " " +  entries.size()+" index "+dataSetIndex );

//        Log.i("Hi label ", String.valueOf(entry.getData()) + " " +  entries.size() + " " + entries.get(dataSetIndex));


//        if (!((int) entry.getData() < 0) && (entries.size() < (int) entry.getData())) {
//            return entries.get(dataSetIndex) + " \n" + mFormat.format(value);
        return  mFormat.format(value);
//        }
//        return "";

    }
}
