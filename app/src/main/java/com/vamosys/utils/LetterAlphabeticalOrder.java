package com.vamosys.utils;

import com.vamosys.model.Site;
import com.vamosys.model.TripSummaryMobile;
import com.vamosys.vamos.Const;

import java.util.Comparator;

public class LetterAlphabeticalOrder implements Comparator {


    @Override
    public int compare(Object o1, Object o2) {
        Site dd1 = (Site) o1;
        Site dd2 = (Site) o2;

        String s1 = null, s2 = null;

        s1 = dd1.getSiteName();
        s2 = dd2.getSiteName();

        return s1.compareToIgnoreCase(s2);
    }
}
