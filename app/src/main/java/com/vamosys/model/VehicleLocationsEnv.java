package com.vamosys.model;

public class VehicleLocationsEnv{
	
	private String rowId;
	private String group;
	private String zoomLevel;	
	private String latitude;
	private String longitude;	
	private String distance;
	private String totalVehicles;
	private String online;
	private String alerts;
	private String attention;
	private int topSpeed;
	private String topSpeedVehicleId;
	private int totalMovingVehicles;
	private int totalIdleVehicles;
	private int totalParkedVehicles;
	private int totalNoDataVehicles;
	private String allowBooking;
	private String allowedVehicles;
	private String userType;  // owner, driver, public
	private String supportDetails;
	
	private VehicleData vehicleLocations[];
	
	public String getSupportDetails() {
		return supportDetails;
	}
	public void setSupportDetails(String supportDetails) {
		this.supportDetails = supportDetails;
	}
	public int getTopSpeed() {
		return topSpeed;
	}
	public void setTopSpeed(int topSpeed) {
		this.topSpeed = topSpeed;
	}
	public String getTopSpeedVehicleId() {
		return topSpeedVehicleId;
	}
	public void setTopSpeedVehicleId(String topSpeedVehicleId) {
		this.topSpeedVehicleId = topSpeedVehicleId;
	}
	public int getTotalMovingVehicles() {
		return totalMovingVehicles;
	}
	public void setTotalMovingVehicles(int totalMovingVehicles) {
		this.totalMovingVehicles = totalMovingVehicles;
	}
	public int getTotalIdleVehicles() {
		return totalIdleVehicles;
	}
	public void setTotalIdleVehicles(int totalIdleVehicles) {
		this.totalIdleVehicles = totalIdleVehicles;
	}
	public int getTotalParkedVehicles() {
		return totalParkedVehicles;
	}
	public void setTotalParkedVehicles(int totalParkedVehicles) {
		this.totalParkedVehicles = totalParkedVehicles;
	}
	public int getTotalNoDataVehicles() {
		return totalNoDataVehicles;
	}
	public void setTotalNoDataVehicles(int totalNoDataVehicles) {
		this.totalNoDataVehicles = totalNoDataVehicles;
	}
	
	
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getTotalVehicles() {
		return totalVehicles;
	}
	public void setTotalVehicles(String totalVehicles) {
		this.totalVehicles = totalVehicles;
	}
	public String getOnline() {
		return online;
	}
	public void setOnline(String online) {
		this.online = online;
	}
	public String getAlerts() {
		return alerts;
	}
	public void setAlerts(String alerts) {
		this.alerts = alerts;
	}
	public String getAttention() {
		return attention;
	}
	public void setAttention(String attention) {
		this.attention = attention;
	}


	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public VehicleData[] getVehicleLocations() {
		return vehicleLocations;
	}
	public void setVehicleLocations(VehicleData vl[]) {
		this.vehicleLocations = vl;
	}
	public String getZoomLevel() {
		return zoomLevel;
	}
	public void setZoomLevel(String zoomLevel) {
		this.zoomLevel = zoomLevel;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getRowId() {
		return rowId;
	}
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}


	public String getAllowBooking() {
		return allowBooking;
	}
	public void setAllowBooking(String allowBooking) {
		this.allowBooking = allowBooking;
	}
	public String getAllowedVehicles() {
		return allowedVehicles;
	}
	public void setAllowedVehicles(String allowedVehicles) {
		this.allowedVehicles = allowedVehicles;
	}
}
