package com.vamosys.model;

public class VehicleStructure {
    String latitude;
    String longitude;
    String vehicleid;
    String shortname;
    public VehicleStructure(String latitude, String longitude, String vehicleid, String shortname) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.vehicleid = vehicleid;
        this.shortname = shortname;
    }
    public VehicleStructure() {
        this.latitude = "";
        this.longitude = "";
        this.vehicleid = "";
        this.shortname = "";
    }
    //getters
    public String getlatitude() {
        return latitude;
    }
    public String getlongitude() {
        return longitude;
    }
    public String getvehicleid() {
        return vehicleid;
    }
    public String getshortname() {
        return shortname;
    }
    //setters
    public void setlatitude(String latitude) {
        this.latitude = latitude;
    }
    public void setlongitude(String longitude) {
        this.longitude = longitude;
    }
    public void setvehicleid(String vehicleid) {
        this.vehicleid = vehicleid;
    }
    public void setshortname(String shortname) {
        this.shortname = shortname;
    }
}

