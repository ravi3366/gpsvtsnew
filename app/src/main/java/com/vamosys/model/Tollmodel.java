package com.vamosys.model;

public class Tollmodel {
    private String shortName;

    private String error;



    private TollgateDto history[];

    public String getShortName() {
        return shortName;
    }
    public TollgateDto[] getHistory() {
        return history;
    }

    public void setHistory(TollgateDto[] history) {
        this.history = history;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
