package com.vamosys.model;


public class SiteTripreportDto {

    private String startLocation;
    private String endLocation;
    private long startTime;
    private long endTime;
    private double avgSpeed;
    private int speedViolation;
    private double distanceCovered;
    private long runningTime;
    private long detentionTime;
    private long stoppageTime;

    private boolean isCountData;
    private double singleTripdistance;
    private int tripCount;


    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public double getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(double avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    public int getSpeedViolation() {
        return speedViolation;
    }

    public void setSpeedViolation(int speedViolation) {
        this.speedViolation = speedViolation;
    }

    public double getDistanceCovered() {
        return distanceCovered;
    }

    public void setDistanceCovered(double distanceCovered) {
        this.distanceCovered = distanceCovered;
    }

    public long getRunningTime() {
        return runningTime;
    }

    public void setRunningTime(long runningTime) {
        this.runningTime = runningTime;
    }

    public long getDetentionTime() {
        return detentionTime;
    }

    public void setDetentionTime(long detentionTime) {
        this.detentionTime = detentionTime;
    }

    public long getStoppageTime() {
        return stoppageTime;
    }

    public void setStoppageTime(long stoppageTime) {
        this.stoppageTime = stoppageTime;
    }

    public boolean isCountData() {
        return isCountData;
    }

    public void setCountData(boolean countData) {
        isCountData = countData;
    }

    public double getSingleTripdistance() {
        return singleTripdistance;
    }

    public void setSingleTripdistance(double singleTripdistance) {
        this.singleTripdistance = singleTripdistance;
    }

    public int getTripCount() {
        return tripCount;
    }

    public void setTripCount(int tripCount) {
        this.tripCount = tripCount;
    }
}
