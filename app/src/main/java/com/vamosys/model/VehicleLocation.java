package com.vamosys.model;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class VehicleLocation{

	protected int rowId;
	protected String latitude;
	protected String longitude;
	protected int speed;
	protected long date;
	protected String alert;
	protected String direction="N";
	protected String position;
	protected double distanceCovered;
	protected double odoDistance=0.0d;
	protected String status;
	protected String altitude;
	protected String color;  //color of status -- indicator
	protected String lastSeen;
	protected String ignitionStatus;
	protected String insideGeoFence="N";
	protected String isOverSpeed="N";
	protected String address;
	protected long parkedTime;
	protected long movingTime;
	protected long idleTime;
	protected long noDataTime;
	protected double tankSize;

    public String getExpiryStatus() {
        return expiryStatus;
    }

    public void setExpiryStatus(String expiryStatus) {
        this.expiryStatus = expiryStatus;
    }

    protected String expiryStatus;
	protected String alertDateTime;
	protected double fuelConsume;
	protected double fuelLitre;



	//protected double temperature;
	protected long lastComunicationTime;

//	public double getTemperature() {
//		return temperature;
//	}
//
//	public void setTemperature(double temperature) {
//		this.temperature = temperature;
//	}

	public double getTankSize() {
		return tankSize;
	}

	public void setTankSize(double tankSize) {
		this.tankSize = tankSize;
	}



	public double getFuelConsume() {
		return fuelConsume;
	}

	public void setFuelConsume(double fuelConsume) {
		this.fuelConsume = fuelConsume;
	}

	public long getLastComunicationTime() {
		return lastComunicationTime;
	}

	public void setLastComunicationTime(long lastComunicationTime) {
		this.lastComunicationTime = lastComunicationTime;
	}

	public double getFuelLitre() {
		return fuelLitre;
	}

	public void setFuelLitre(double fuelLitre) {
		this.fuelLitre = fuelLitre;
	}

	public String getAlertDateTime() {
		return alertDateTime;
	}
	public void setAlertDateTime(String alertDateTime) {
		this.alertDateTime = alertDateTime;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}

	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public String getAlert() {
		return alert;
	}
	public void setAlert(String alert) {
		this.alert = alert;
	}
	public int getRowId() {
		return rowId;
	}
	public void setRowId(int rowId) {
		this.rowId = rowId;
	}
	public double getDistanceCovered() {
		return distanceCovered;
	}
	public void setDistanceCovered(double distanceCovered) {
		
		this.distanceCovered = new BigDecimal(distanceCovered).setScale(2, RoundingMode.HALF_UP).doubleValue() ;
	}
	public double getOdoDistance() {
		return odoDistance;
	}
	public void setOdoDistance(double odoDistance) {
		this.odoDistance = new BigDecimal(odoDistance).setScale(2, RoundingMode.HALF_UP).doubleValue() ;
	}
	/**
	 * @return the altitude
	 */
	public String getAltitude() {
		return altitude;
	}
	/**
	 * @param altitude the altitude to set
	 */
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the lastSeen
	 */
	public String getLastSeen() {
		return lastSeen;
	}
	/**
	 * @param lastSeen the lastSeen to set
	 */
	public void setLastSeen(String lastSeen) {
		this.lastSeen = lastSeen;
	}
	/**
	 * @return the ignitionStatus
	 */
	public String getIgnitionStatus() {
		return ignitionStatus;
	}
	/**
	 * @param ignitionStatus the ignitionStatus to set
	 */
	public void setIgnitionStatus(String ignitionStatus) {
		this.ignitionStatus = ignitionStatus;
	}
	/**
	 * @return the insideGeoFence
	 */
	public String getInsideGeoFence() {
		return insideGeoFence;
	}
	/**
	 * @param insideGeoFence the insideGeoFence to set
	 */
	public void setInsideGeoFence(String insideGeoFence) {
		this.insideGeoFence = insideGeoFence;
	}
	/**
	 * @return the isOverSpeed
	 */
	public String getIsOverSpeed() {
		return isOverSpeed;
	}
	/**
	 * @param isOverSpeed the isOverSpeed to set
	 */
	public void setIsOverSpeed(String isOverSpeed) {
		this.isOverSpeed = isOverSpeed;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	public long getMovingTime() {
		return movingTime;
	}
	public void setMovingTime(long movingTime) {
		this.movingTime = movingTime;
	}
	public long getIdleTime() {
		return idleTime;
	}
	public void setIdleTime(long idleTime) {
		this.idleTime = idleTime;
	}
	public long getNoDataTime() {
		return noDataTime;
	}
	public void setNoDataTime(long noDataTime) {
		this.noDataTime = noDataTime;
	}
	/**
	 * @return the parkedTime
	 */
	public long getParkedTime() {
		return parkedTime;
	}
	/**
	 * @param parkedTime the parkedTime to set
	 */
	public void setParkedTime(long parkedTime) {
		this.parkedTime = parkedTime;
	}
	@Override
	public String toString() {
		return "VehicleLocation [rowId=" + rowId + ", latitude=" + latitude + ", longitude=" + longitude + ", speed="
				+ speed + ", date=" + date + ", alert=" + alert + ", direction=" + direction + ", position=" + position
				+ ", distanceCovered=" + distanceCovered + ", odoDistance=" + odoDistance + ", status=" + status
				+ ", altitude=" + altitude + ", color=" + color + ", lastSeen=" + lastSeen + ", ignitionStatus="
				+ ignitionStatus + ", tankSize="+ tankSize + ", insideGeoFence=" + insideGeoFence + ", isOverSpeed=" + isOverSpeed + "]";
	}
	
}
