package com.vamosys.model;

/**
 * Created by Smart Team.
 */
public class ReportData {
    private String startTime;
    private String state;
    private String address;
    private String fuelConsume;
    public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getFuelConsume() {
        return fuelConsume;
    }
    public void setFuelConsume(String fuelConsume) {
        this.fuelConsume = fuelConsume;
    }
}
