package com.vamosys.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ExecutiveReportData {

	

	private int rowId;
	private String date;
	private String vehicleId;
	private String shortName;
	private String fcode;
	private int topSpeed;
	private int overSpeedInstances;
	private double distanceMonth;
	private double distanceYest;
	private double distanceWeek;
	private double odoOpeningReading;
	private double odoClosingReading;
	private double distanceToday;
	private int parkingCount;
	private String topSpeedGeoLocation;
	private String address;
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	private String topSpeedTime;
	private long totalParkedTime;
	private long totalRunningTime;
	private long totalIdleTime;
	private long totalNoDataTime;
	
	

	public double getDistanceYest() {
		return Double.parseDouble(String.format("%.2f",distanceYest));
	}

	public void setDistanceYest(double distanceYest) {
		this.distanceYest = distanceYest;
	}

	public String getTopSpeedTime() {
		return topSpeedTime;
	}

	public long getTotalParkedTime() {
		return totalParkedTime;
	}
	public void setTotalParkedTime(long totalParkedTime) {
		this.totalParkedTime = totalParkedTime;
	}
	public long getTotalRunningTime() {
		return totalRunningTime;
	}
	public void setTotalRunningTime(long totalRunningTime) {
		this.totalRunningTime = totalRunningTime;
	}
	public void setTopSpeedTime(String topSpeedTime) {
		this.topSpeedTime = topSpeedTime;
	}

	
	
	
	public int getRowId() {
		return rowId;
	}
	public void setRowId(int rowId) {
		this.rowId = rowId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getFcode() {
		return fcode;
	}
	public void setFcode(String fcode) {
		this.fcode = fcode;
	}
	public int getTopSpeed() {
		return topSpeed;
	}
	public void setTopSpeed(int topSpeed) {
		this.topSpeed = topSpeed;
	}
	public int getOverSpeedInstances() {
		return overSpeedInstances;
	}
	public void setOverSpeedInstances(int overSpeedInstances) {
		this.overSpeedInstances = overSpeedInstances;
	}
	
	public double getDistanceToday() {
		return distanceToday;
	}
	public void setDistanceToday(double distanceToday) {
		
		this.distanceToday= new BigDecimal(distanceToday).setScale(2, RoundingMode.HALF_UP).doubleValue();

	}
	public int getParkingCount() {
		return parkingCount;
	}
	public void setParkingCount(int parkingCount) {
		this.parkingCount = parkingCount;
	}
	public String getTopSpeedGeoLocation() {
		return topSpeedGeoLocation;
	}
	public void setTopSpeedGeoLocation(String topSpeedGeoLocation) {
		this.topSpeedGeoLocation = topSpeedGeoLocation;
	}
	public long getTotalIdleTime() {
		return totalIdleTime;
	}

	public void setTotalIdleTime(long totalIdleTime) {
		this.totalIdleTime = totalIdleTime;
	}

	public long getTotalNoDataTime() {
		return totalNoDataTime;
	}

	public void setTotalNoDataTime(long totalNoDataTime) {
		this.totalNoDataTime = totalNoDataTime;
	
	}


	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public double getOdoOpeningReading() {
		return odoOpeningReading;
	}

	public void setOdoOpeningReading(double odoOpeningReading) {
		
		this.odoOpeningReading = new BigDecimal(odoOpeningReading).setScale(2, RoundingMode.HALF_UP).doubleValue() ;;
	}

	public double getOdoClosingReading() {
		return odoClosingReading;
	}

	public void setOdoClosingReading(double odoClosingReading) {
		this.odoClosingReading = new BigDecimal(odoClosingReading).setScale(2, RoundingMode.HALF_UP).doubleValue() ;
	}

	@Override
	public String toString() {
		return "ExecutiveReportData [rowId=" + rowId + ", date=" + date + ", vehicleId=" + vehicleId + ", fcode="
				+ fcode + ", topSpeed=" + topSpeed + ", overSpeedInstances=" + overSpeedInstances
				+ ", odoOpeningReading=" + odoOpeningReading + ", odoClosingReading=" + odoClosingReading
				+ ", distanceToday=" + distanceToday + ", parkingCount=" + parkingCount + ", topSpeedGeoLocation="
				+ topSpeedGeoLocation + ", address=" + address + ", topSpeedTime=" + topSpeedTime
				+ ", totalParkedTime=" + totalParkedTime + ", totalRunningTime=" + totalRunningTime
				+ ", totalIdleTime=" + totalIdleTime + ", totalNoDataTime=" + totalNoDataTime + ", openingOdoReading="
				 +"]";
	}

	public double getDistanceMonth() {
		return Double.parseDouble(String.format("%.2f", distanceMonth));
	}

	public void setDistanceMonth(double distanceMonth) {
		this.distanceMonth = distanceMonth;
	}

	public double getDistanceWeek() {
		return Double.parseDouble(String.format("%.2f",distanceWeek));
	}

	public void setDistanceWeek(double distanceWeek) {
		this.distanceWeek = distanceWeek;
	}

	
}
