package com.vamosys.model;

import java.util.List;

public class UserLocationNearestVehicle {
	
	private String group;
	private String error;

	private List<NearestVehicle> nearestVehicles;

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public List<NearestVehicle> getNearestVehicles() {
		return nearestVehicles;
	}

	public void setNearestVehicles(List<NearestVehicle> nearestVehicles) {
		this.nearestVehicles = nearestVehicles;
	}
}
