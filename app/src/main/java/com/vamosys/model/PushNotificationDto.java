package com.vamosys.model;

public class PushNotificationDto {
	
	public String id;
	public String timeStamp;
	public String msgContent;
	public boolean alreadySelected;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	public boolean isAlreadySelected() {
		return alreadySelected;
	}
	public void setAlreadySelected(boolean alreadySelected) {
		this.alreadySelected = alreadySelected;
	}
	

}
