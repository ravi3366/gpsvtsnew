package com.vamosys.model;

public class TollgateDto {


    private String address;
    private String tollgateName;
    private double lat;
    private double lng;
    private String date;
    private long time;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private String error;

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    private String shortName;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTollgateName() {
        return tollgateName;
    }

    public void setTollgateName(String tollgateName) {
        this.tollgateName = tollgateName;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
//    @Override
//    public String toString() {
//        return "TollgateDto [address=" + address + ", tollgateName=" + tollgateName + ", lat=" + lat + ", lng="
//                + lng + ", date=" + date + ", time=" + time
//                + "]";
//    }




}
