package com.vamosys.model;

/*
 * @author prabhakaran
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHandler extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "GPSVTS";

    private static final int DATABASE_VERSION = 5;

    private static String DB_PATH_PREFIX = "/data/data/";
    private static String DB_PATH_SUFFIX = "/databases/";
    private static String DB_PATH = "";

    public static final String TABLE_NOTIFICATIONS = "location";

    public static final String TABLE_USER = "user";

    public static final String TABLE_LOGO = "app_logo";

    public static final String TABLE_LAT_LNG = "lat_lng_data";


    public static final String CREATE_TABLE_NOTIFICATIONS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NOTIFICATIONS
            + " (id integer primary key autoincrement,"
            + "msg_data text , "
            + "time_stamp text" + ");";

    public static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS "
            + TABLE_USER
            + " (id integer primary key autoincrement,"
            + "user_id text , " + "group_list text , "
            + "refresh_rate text , " + "history_interval text , "
            + "support_adds text , "
            + "time text" + ");";

    public static final String CREATE_TABLE_LOGO = "CREATE TABLE IF NOT EXISTS "
            + TABLE_LOGO
            + " (id integer primary key autoincrement,"

            + "app_logo_image BLOB NOT NULL" + ");";


    public static final String CREATE_TABLE_LAT_LNG = "CREATE TABLE IF NOT EXISTS "
            + TABLE_LAT_LNG
            + " (id integer primary key autoincrement,"
            + "latitude text , " + "longitude text , "
            + "date_time text" + ");";


//	public static Mydatabase mydata;
//
//	public static SQLiteDatabase sqldb;
//
//	Context context;

//	public static class Mydatabase extends SQLiteOpenHelper {
//
//		public Mydatabase(Context context, String name, CursorFactory factory,
//				int version) {
//			super(context, name, factory, version);
//		}
//
//		public void onCreate(SQLiteDatabase db) {
//			// TODO Auto-generated method stub
//
//
//
//		}
//
//		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//			// TODO Auto-generated method stub
//			db.execSQL("Drop table if exists " + TABLE_NOTIFICATIONS);
//			db.execSQL("Drop table if exists " + TABLE_USER);
//			db.execSQL("Drop table if exists " + TABLE_LOGO);
//
//
//			onCreate(db);
//		}
//	}

    public DataBaseHandler(Context c) {
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static String getDatabasePath(Context aContext, String databaseName) {
        DB_PATH = DB_PATH_PREFIX + aContext.getPackageName() + DB_PATH_SUFFIX
                + databaseName;
        return DB_PATH;
    }

//	public DataBaseHandler open() {
//		mydata = new Mydatabase(context, DB_NAME, null, DB_VERSION);
//		sqldb = mydata.getWritableDatabase();
//		return this;
//
//	}
//
//	public SQLiteDatabase getDatabaseObj() {
//		return sqldb;
//	}
//
//	public void close() {
//		mydata.close();
//	}

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_NOTIFICATIONS);
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_LOGO);
        db.execSQL(CREATE_TABLE_LAT_LNG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table if exists " + TABLE_NOTIFICATIONS);
        db.execSQL("Drop table if exists " + TABLE_USER);
        db.execSQL("Drop table if exists " + TABLE_LOGO);
        db.execSQL("Drop table if exists " + TABLE_LAT_LNG);

        onCreate(db);
    }

}
