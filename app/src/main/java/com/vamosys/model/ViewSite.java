package com.vamosys.model;

import java.util.Set;


public class ViewSite {

	String error;
	
	SiteParent[] siteParent;
	Set<String> orgIds;
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	
	
		
	
	
	
	public SiteParent[] getSiteParent() {
		return siteParent;
	}
	public void setSiteParent(SiteParent[] siteParent) {
		this.siteParent = siteParent;
	}
	public Set<String> getOrgIds() {
		return orgIds;
	}
	public void setOrgIds(Set<String> orgIds) {
		this.orgIds = orgIds;
	}
	
}
