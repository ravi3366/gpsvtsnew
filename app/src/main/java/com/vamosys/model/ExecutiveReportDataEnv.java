package com.vamosys.model;

public class ExecutiveReportDataEnv {
	private ExecutiveReportData[] execReportData;
	
	private String error;
	private long consolidateRunningTime;
	private long consolidatedParkedTime;
	private long consolidatedNoDataTime;
	private long consolidatedIdleTime;
	private String distanceCoveredAnalytics;
	

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public ExecutiveReportData[] getExecReportData() {
		return execReportData;
	}

	public void setExecReportData(ExecutiveReportData[] execReportData) {
		this.execReportData = execReportData;
	}

	public long getConsolidateRunningTime() {
		return consolidateRunningTime;
	}

	public void setConsolidateRunningTime(long consolidateRunningTime) {
		this.consolidateRunningTime = consolidateRunningTime;
	}

	public long getConsolidatedParkedTime() {
		return consolidatedParkedTime;
	}

	public void setConsolidatedParkedTime(long consolidatedParkedTime) {
		this.consolidatedParkedTime = consolidatedParkedTime;
	}

	public long getConsolidatedNoDataTime() {
		return consolidatedNoDataTime;
	}

	public void setConsolidatedNoDataTime(long consolidatedNoDataTime) {
		this.consolidatedNoDataTime = consolidatedNoDataTime;
	}

	public long getConsolidatedIdleTime() {
		return consolidatedIdleTime;
	}

	public void setConsolidatedIdleTime(long consolidatedIdleTime) {
		this.consolidatedIdleTime = consolidatedIdleTime;
	}

	/**
	 * @return the distanceCoveredAnalytics
	 */
	public String getDistanceCoveredAnalytics() {
		return distanceCoveredAnalytics;
	}

	/**
	 * @param distanceCoveredAnalytics the distanceCoveredAnalytics to set
	 */
	public void setDistanceCoveredAnalytics(String distanceCoveredAnalytics) {
		this.distanceCoveredAnalytics = distanceCoveredAnalytics;
	}
	
}
