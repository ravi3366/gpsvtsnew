package com.vamosys.model;

import java.text.DecimalFormat;


public class TripSummaryMobile {


    protected double dt;
    protected long fT = 0, tT = 0;
    protected double sLat;
    protected double sLng;
    protected double eLat;
    protected double eLng;
    protected String sLoc;
    protected String eLoc;


    public String getsLoc() {
        return sLoc;
    }


    public void setsLoc(String sLoc) {
        this.sLoc = sLoc;
    }


    public String geteLoc() {
        return eLoc;
    }


    public void seteLoc(String eLoc) {
        this.eLoc = eLoc;
    }


    public double getDt() {
        return dt;
    }


    public void setDt(double dt) {
        this.dt = Double.parseDouble(new DecimalFormat("##.##").format(dt));
    }


    public long getfT() {
        return fT;
    }


    public void setfT(long fT) {
        this.fT = fT;
    }


    public long gettT() {
        return tT;
    }


    public void settT(long tT) {
        this.tT = tT;
    }


    public double getsLat() {
        return sLat;
    }


    public void setsLat(double sLat) {
        this.sLat = sLat;
    }


    public double geteLat() {
        return eLat;
    }


    public void seteLat(double eLat) {
        this.eLat = eLat;
    }


    public double getsLng() {
        return sLng;
    }


    public void setsLng(double sLng) {
        this.sLng = sLng;
    }


    public double geteLng() {
        return eLng;
    }


    public void seteLng(double eLng) {
        this.eLng = eLng;
    }


}