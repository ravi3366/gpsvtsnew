package com.vamosys.model;


public class FmsDto {

private String documentType;
    private String dueDate;
    private String amount;
    private String companyName;
    private String description;

    public String getDocumentName() {
        return DocumentName;
    }

    public void setDocumentName(String documentName) {
        DocumentName = documentName;
    }

    private String notifyInterval;
    private String monthInterval;
    private String DocumentName;

    private String odometerDue;
    public String getOdometerDue() {
        return odometerDue;
    }

    public void setOdometerDue(String odometerDue) {
        this.odometerDue = odometerDue;
    }


    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotifyInterval() {
        return notifyInterval;
    }

    public void setNotifyInterval(String notifyInterval) {
        this.notifyInterval = notifyInterval;
    }

    public String getMonthInterval() {
        return monthInterval;
    }

    public void setMonthInterval(String monthInterval) {
        this.monthInterval = monthInterval;
    }
}
