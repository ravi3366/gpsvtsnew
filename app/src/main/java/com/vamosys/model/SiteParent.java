package com.vamosys.model;

public class SiteParent {

	String orgId;
	Site[] site;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public Site[] getSite() {
		return site;
	}

	public void setSite(Site[] site) {
		this.site = site;
	}


	//	public String getOrgId() {
//		return orgId;
//	}
//
//	public void setOrgId(String orgId) {
//		this.orgId = orgId;
//	}
//
//	public Site[] getSite() {
//		return site;
//	}
//
//	public void setSite(Site[] site) {
//		this.site = site;
//	}
}
